<?php namespace App\Http\Controllers;

use App\Gallery;
use App\User;
use App\Role;
use DB;
use App\CreditBundle;
use App\Activity;
use App\Classification;
use App\Profile;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Post;
use Yajra\Datatables\Datatables;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Job;
use App\CreditsDebitCredit;
use Khill\Lavacharts\Lavacharts;

class DashboardController extends Controller
{

    protected $requested_dashboard_route;
    protected $default_jobs_route;
    protected $default_messages_route;
    protected $default_creditbundles_route;
    protected $default_vendorcredits_route;
    protected $default_myshop_route;
    protected $default_blog_route;
    protected $default_links_route;
    protected $default_calendar_route;
    protected $default_profile_route;
    protected $default_advertise_route;
    protected $default_reviews_route;
    protected $default_leads_route;
    protected $default_mstats_route;
    protected $default_activitylog_route;
    protected $default_activityreports_route;
    protected $default_users_route;
    protected $default_tracker_route;
    protected $default_creditreports_route;
    protected $default_vendorcategories_route;
    ///////////////////////////////////////////////////////////////
    ////////////////// START OF RESTFUL FUNCTIONS /////////////////
    ///////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->hasUserRole('vendor')) {
            $portfolio = Gallery::where('name','Portfolio')->where('created_by',$user->id)->first();

            //create new portfolio
            if (!$portfolio) {
                $gallery = new Gallery;

                //save new gallery
                $gallery->name = 'Portfolio';
                $gallery->description = 'Portfolio showing works by '.$user->first_name;
                $gallery->gallery_image = ($user->profile->profilePic);
                $gallery->created_by = $user->id;
                $gallery->published = 1;
                $gallery->save();
                return redirect('portfolio/' . $user->id);
            }
            elseif ($portfolio->images->count() == 0) {
                return redirect('portfolio/' . $user->id);
            }
        }
        $page_data = [
            'balance'            => ( CreditsDebitCredit::total_balance($user->id) ),
            'default_jobs_route' => ( $this->default_jobs_route )
                ? $this->default_jobs_route
                : ( ( !$user || !$user->can_bid_for_job )
                    ? url('jobs/events/')
                    : route('jobs.show', 'all') ),

            'default_messages_route'      => ( $this->default_messages_route )
                ? $this->default_messages_route
                : route('messages'),
            'default_profile_route'       => ( $this->default_profile_route )
                ? $this->default_profile_route
                : route('users.show', Auth::user()->id),
            'default_vendorcredits_route' => ( $this->default_vendorcredits_route )
                ? $this->default_vendorcredits_route
                : route('vendor-credits'),
            'default_creditbundles_route' => ( ( $this->default_creditbundles_route )
                ? $this->default_creditbundles_route
                : route('admin.bundles.index')

            ),

            'default_myshop_route' => ( $this->default_myshop_route )
                ? $this->default_myshop_route
                : route('shop.admin.myshop.index'),

            'default_blog_route' => ( $this->default_blog_route )
                ? $this->default_blog_route
                : route('admin.post.index'),
            'default_links_route' => ( $this->default_links_route )
                ? $this->default_links_route
                : route('admin.links'),

            'default_advertise_route'       => ( $this->default_advertise_route )
                ? $this->default_advertise_route
                : route('advertise'),
            'default_reviews_route'         => ( $this->default_reviews_route )
                ? $this->default_reviews_route
                : route('reviews'),
            'default_leads_route'           => ( $this->default_leads_route )
                ? $this->default_leads_route
                : route('admin.leads'),
            'default_mstats_route'          => ( $this->default_mstats_route )
                ? $this->default_mstats_route
                : route('admin.messages'),
            'default_activitylog_route'     => ( $this->default_activitylog_route )
                ? $this->default_activitylog_route
                : route('users.log'),
            'default_calendar_route' => ( $this->default_calendar_route )
                ? $this->default_calendar_route
                : route('booking.calendar'),
            'default_users_route'           => ( $this->default_users_route )
                ? $this->default_users_route
                : route('users.index'),
            'default_vendorcategories_route'           => ( $this->default_vendorcategories_route )
                ? $this->default_vendorcategories_route
                : route('classification.index'),

            'default_tracker_route'       => ( $this->default_tracker_route )
                ? $this->default_tracker_route
                : config('tracker.stats_base_uri'),
            'default_creditreports_route' => ( $this->default_creditreports_route )
                ? $this->default_creditreports_route
                : route('credits.under_hundred'),

            'hide_layout' => true,
        ];
        if (Auth::user()->hasUserRole('administrator')) {
            return view('dashboard.home-o', $page_data);
        }
        return view('dashboard.home', $page_data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param         $id
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id, Request $request)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {

    }

    ///////////////////////////////////////////////////////////////
    ////////////////// END OF RESTFUL FUNCTIONS ///////////////////
    ///////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////

    /**
     * @param Request $request
     *
     * @return array
     */
    public function getUserStats(Request $request)
    {   
        $user = Auth::user();
        $total_messages = Thread::forUser($user->id)->count();
        $unread_message = count($user->threadsWithNewMessages()); 
        $from = ( date('Y-m-d H:i:s', strtotime('yesterday')) );
        $to = ( date('Y-m-d H:i:s', strtotime('today')) );
        $new_jobs = Job::where('created_at', '>=', $from)->count();
        $active_jobs = Job::where('user_id',$user->id)->count();
        $balance = CreditsDebitCredit::total_balance($user->id);
        $reviews = ( number_format(( $user->profile->reviews()->sum('rating') ), 1) );
        $total = Job::sum('budget');

        $first_job = Job::where('user_id',$user->id)->where('event_date', '>', date('Y-m-d'))->orderBy('event_date','DESC')->first();
        
        if (isset($first_job->event_date)) {
            $days_to_event = $this->dateDifference(date('Y-m-d'),$first_job->event_date);
        }
        else {
            $days_to_event = 0;
        }

        $x = round($total);
        $x_number_format = number_format($x);
        $x_array = explode(',', $x_number_format);
        $x_parts = array( 'K', 'M', 'B', 'T' );
        $x_count_parts = count($x_array) - 1;
        $x_display = $x;
        $x_display = $x_array[ 0 ] . ( (int)$x_array[ 1 ][ 0 ] !== 0 ? '.' . $x_array[ 1 ][ 0 ] : '' );
        $x_display .= $x_parts[ $x_count_parts - 1 ];

        $user_stats = [
            'jobs' => [
                'unread' => 0,
                'active' => $active_jobs,
                'total'  => 0,
                'new'    => $new_jobs,
                'value'  => $x_display,
                'days_to_event'=>$days_to_event,
            ],

            'messages' => [
                'unread' => $unread_message,
                'total'  => $total_messages,
            ],

            'shop' => [
                'products'       => 0,
                'categories'     => 0,
                'sub_categories' => 0,
                'orders'         => 0,
            ],

            'reviews'  => [
                'total' => $reviews,
            ],
            'credits'  => [
                'balance' => $balance,
            ],
            'rankings' => [
                'day'   => 0,
                'week'  => 0,
                'month' => 0,
                'year'  => 0,
            ],
        ];


        return $user_stats;
    }

    public function analytics(Request $request)
    {

    }

    function dateDifference($startDate, $endDate) 
    { 
            $startDate = strtotime($startDate); 
            $endDate = strtotime($endDate); 
            if ($startDate === false || $startDate < 0 || $endDate === false || $endDate < 0 || $startDate > $endDate) 
                return 0; 
                
            $years = date('Y', $endDate) - date('Y', $startDate); 
            
            $endMonth = date('m', $endDate); 
            $startMonth = date('m', $startDate); 
            
            // Calculate months 
            $months = $endMonth - $startMonth; 

            if ($years < 0) 
                return 0; 
            
            // Calculate the days 
            $offsets = array(); 
            if ($years > 0) 
                $offsets[] = $years . (($years == 1) ? ' year' : ' years'); 
            if ($months > 0) 
                $offsets[] = $months . (($months == 1) ? ' month' : ' months'); 
            $offsets = count($offsets) > 0 ? '+' . implode(' ', $offsets) : 'now'; 

            $days = $endDate - strtotime($offsets, $startDate); 
            $days = date('z', $days);    
                        
            return $days; 
    } 
    /**
     * Reroute non ajax requests to the ajax home page
     *
     * @param Request $request
     *
     * @return Response
     */
    public function routeNonAjax(Request $request)
    {
        $url = $request->url();
        $getQueryString = ( $request->getQueryString() ) ? '?' . $request->getQueryString() : '';

        $this->requested_dashboard_route = $url . '' . $getQueryString;

        return $this->index();
    }
}