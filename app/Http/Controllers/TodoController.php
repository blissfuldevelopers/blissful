<?php

namespace App\Http\Controllers;

use App\FileEntry;
use App\Todo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todos = Todo::where('user_id', Auth::id())->paginate(7);
        return view('todo.index', compact('todos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = $request->all();

        $this->validate($request, ['title' => 'required', 'reminder' => 'required']);

        Todo::create([
            'title' => $request->get('title'),
            'reminder' => $request->get('reminder'),
            'user_id' => Auth::user()->id,
        ]);
        return Response::json($task);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Todo::find($id);

        return Response::json($task);

    }

    public function view($id)
    {
        $task = Todo::find($id);
        $entries = FileEntry::where('user_id', Auth::id())->paginate(7);
        $subtasks = Todo::where('parent_id', $id)->get();
        return view('todo.show', compact('task', 'entries', 'subtasks'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $task = Todo::findOrFail($id);
        $task->update($requestData);

        return Response::json($task);
    }
    public function status(Request $request, $id)
    {
        $task = Todo::findOrFail($id);
        $task->status = !$task->status;
        $task->update();

        return redirect()->back();
    }
    public function add_note(Request $request, $id)
    {
        $task = Todo::findOrFail($id);
        $task->description = $request->get('description');
        $task->update();

        return redirect()->back();
    }

    public function subtask(Request $request)
    {
        Todo::create([
            'title' => $request->get('title'),
            'parent_id' => $request->get('parent_id'),
            'user_id' => Auth::user()->id,
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Todo::destroy($id);

        return Response::json($task);
    }
}
