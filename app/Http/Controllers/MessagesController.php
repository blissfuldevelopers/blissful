<?php namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Thread;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Laracasts\Flash\Flash;
use App\Classes\AfricasTalkingGateway;
use App\Activity;
use Illuminate\Support\Facades\Config;
use App\Traits\CaptchaTrait;
use Response;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Jobs\SendMultipleLeads;
use App\Logic\User\UserRepository;
use App\Logic\Mailers\UserMailer;
use App\Bid;
use App\Ranking;
use App\Events\JobStatusChanged;
use App\BreakdownDetails;


class MessagesController extends Controller
{
    use CaptchaTrait;

    /**
     * @var Pusher
     */
    protected $userMailer;
    protected $userRepository;
    protected $is_ajax = false;


    public function __construct(UserMailer $userMailer, UserRepository $userRepository, Request $request)
    {
        $this->is_ajax = $request->ajax();

        $this->userMailer = $userMailer;
        $this->userRepository = $userRepository;
    }

    /**
     * Show all of the message threads to the user
     *
     * @return mixed
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        // All threads that user is participating in
        $currentUserId = Auth::user()->id;
        $threads = Thread::forUser($currentUserId)->orderBy('updated_at', 'DESC')->get();
        $threads = $this->paginate_results($request, $threads);

        return $this->view('messenger.index', compact('threads', 'currentUserId'));
    }

    /**
     * Shows a message thread
     *
     * @param $id
     *
     * @return mixed
     */
    public function show($id)
    {
        $thread = null;
        $users = null;

        if (Auth::check()) {
            try {
                $thread = Thread::findOrFail($id);
                $participantIds = $thread->participantsUserIds();
                // don't show the current user in list
                $users = User::whereNotIn('id', $thread->participantsUserIds())->get();

                //hack for missing threads
                // if ($participantIds->isEmpty()) {
                //     $id = $thread->id + 1;
                //     $thread = Thread::findOrFail($id);
                //     $participantIds = $thread->participantsUserIds();
                //     // don't show the current user in list
                //     $users = User::whereNotIn('id', $thread->participantsUserIds())->get();
                // }


                    if (in_array(Auth::user()->id, $participantIds) || Auth::user()->hasUserRole('administrator')) {

                        if (in_array(Auth::user()->id, $participantIds)) {
                            $userId = Auth::user()->id;
                            $thread->markAsRead($userId);
                        }

                        $bid = Bid::where('thread_id', $thread->id)->first();

                        if($bid){
                            $breakdown_categories = [];
                            // $details = BreakdownDetails::where('bid_id','=',$bid->id)->get();
                            foreach ($bid->details()->groupBy('cat_id')->get() as $detail) {

                                array_push($breakdown_categories, $detail->classification);
                            }

                            if (isset($bid->job->user->id) && Auth::user()->id == $bid->job->user->id && $bid->status == 0) {
                                $bid->status = 4;
                                $bid->save();

                                $ranking = new Ranking;
                                $ranking->storeRankingForVendor($bid->user_id, 'bid was opened', 4);

//                                        event(new JobStatusChanged($bid));
                            }

                            foreach ($bid->user->classifications as $classification) {

                                $user_classifs[]= $classification->name;
                            }

                            if ($bid->job) {
                                foreach ($bid->job->classifications as $classification) {
                                    $job_classifs[]= $classification->name;
                                }
                            }
                            if (isset($job_classifs)) {
                               $results = array_intersect($user_classifs, $job_classifs);
                            }
                            else{
                                $results = [];
                            }

                            switch ($bid->status) {
                                        case 0:
                                            $status = 'New';
                                            break;
                                        case 1:
                                            $status = 'Shortlisted';
                                            break;
                                        case 2:
                                            $status = 'Accepted';
                                            break;
                                        case 3:
                                            $status = 'Declined';
                                            break;
                                        case 4:
                                            $status = 'In Review';
                                            break;
                            }
                            return $this->view('messenger.show-for-bid', compact('thread', 'users','bid','results','status','breakdown_categories'));
                        }
                        else{
                            return $this->view('messenger.show', compact('thread', 'users'));
                        }


                    }
            }
            catch (ModelNotFoundException $e) {
//                Session::flash('error_message', 'The message with ID: ' . $id . ' was not found.');

//            return redirect('messages'); //what's with all these redirects? Just don't show the message
            }
        }


    }

    /**
     * Creates a new message thread
     *
     * @return mixed
     */
    public function create()
    {
        // $users = User::where('id', '!=', Auth::id())->get();
        $users = User::lists('first_name', 'id');


        return $this->view('messenger.create', compact('users'));
    }

    /**
     * Stores a new message thread
     *
     * @return mixed
     */
    public function store(UserRepository $userRepository,UserMailer $userMailer)
    {
        $input = Input::all();

        if (Auth::check()) {
            $validator = Validator::make($input, User::$contact_rules);
            $id = Auth::user()->id;
        }
        else {
            $validator = Validator::make($input, User::$contact_rules, User::$contact_messages);
            $id = false;
        }

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->with('status', 'alert')
                ->with('errorsZurb', 'Validation failed, ')
                ->withErrors($validator)
                ->withInput();
        }
        if ($this->captchaCheck() == false) {
            return redirect()->back()
                             ->with('status', 'alert')
                             ->with('errorsZurb', 'Wrong Captcha')
                             ->withInput();
        }
        $this->dispatch(new SendMultipleLeads($id, $input,$userRepository,$userMailer));


        if (!Auth::check()) {

            $user = User::where('email', $input[ 'sender_mail' ])->first();
            $date = new Carbon;
            $date->modify('-8 minutes');
            if ($user->created_at >= $date) {
               return redirect()
                ->back()
                ->with('status', 'success')
                ->with('messageZurb', 'Message sent successfully, An email has been sent to '.$user->email.' to confirm your account');
            }
        }
        
        return redirect()
            ->back()
            ->with('status', 'success')
            ->with('messageZurb', 'Message Sent Successfully');
    }

    /**
     * Adds a new message to a current thread
     *
     * @param $id
     *
     * @return mixed
     */
    public function update($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        }
        catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');

            return redirect('messages');
        }

        $thread->activateAllParticipants();

        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::id(),
                'body'      => Input::get('message'),
            ]
        );

        // Add replier as a participant
        $participant = Participant::firstOrCreate(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
            ]
        );
        $participant->last_read = new Carbon;
        $participant->save();

        // Recipients
        // if (Input::has('recipients')) {
        //     $thread->addParticipants(Input::get('recipients'));
        // }
        foreach ($thread->participantsUserIds() as $participantID) {
            if ($participantID != Auth::user()->id) {

                $recipient = User::findOrFail($participantID);
            }
        }

        //email notification
        $link = 'login';
        $data = [
            'first_name'   => $recipient->first_name,
            'link'         => $link,
            'contact_name' => Auth::user()->first_name,
            'subject'      => $thread->subject,
            'email'        => $recipient->email
        ];

        $this->userMailer->emailNotification($recipient->email, $data);

        //SMS notification
        if (!empty( $recipient->profile->phone )) {
            $username = env('AT_ID');

            $apikey = env('AT_SECRET');

            $recipients = $recipient->profile->phone;

            $message = "You have a message on blissful.co.ke";

            $gateway = new AfricasTalkingGateway($username, $apikey);

            try {
                // Thats it, hit send and we'll take care of the rest.
                $results = $gateway->sendMessage($recipients, $message);
                foreach ($results as $result) {
                    // status is either "Success" or "error message"
                    Flash::message('Number:' . $result->number);
                    Flash::message('Status:' . $result->status);
                    Flash::message('MessageId:' . $result->messageId);
                    Flash::message('Cost:' . $result->cost);
                }
            }
            catch (AfricasTalkingGatewayException $e) {
                Flash::message('Encountered an error while sending: ' . $e->getMessage());
            }
        }
         return redirect()->route('messages.show', $thread->id);


    }

    /////////////////////////////////
    /////////////////////////////////

    /**
     * @param Request $request
     * @param         $results
     * @param array   $defaults
     *
     * @return array|Paginator
     */
    public function paginate_results(Request $request, $results, $defaults = [ ])
    {
        $results_data = [ ];
        $paginated_data = [ ];

        $limit = ( Input::has('limit') ) ? Input::get('limit')
            : array_get($defaults, 'limit', 10);

        if ($limit != -1) {
            $offset = ( Input::has('page') ) ? ( Input::get('page') - 1 ) * $limit : 0;
        }

        $page = ( Input::has('page') ) ? Input::get('page')
            : array_get($defaults, 'page', 1);


        foreach ($results as $result) {
            $results_data [] = $result;
        }
        $itemsForCurrentPage = ( $limit != -1 ) ? array_slice($results_data, $offset, $limit, true) : $results_data;

        $paginated_data = ( $limit != -1 ) ? new Paginator($itemsForCurrentPage, count($results), $limit, $page, [
            'path'  => $request->url(),
            'query' => $request->query(),
        ]) : $results;

        return $paginated_data;
    }

    /**
     * @param $user
     *
     * @return bool
     */
    public function UserThread($user)
    {
        $threads = null;
        $responseThread = [ ];
        $user = is_object($user) ? $user : User::find($user);

        if (!$user) {
            return false;
        }

        // All threads that user is participating in
        $threads = Thread::forUser($user->id)
                         ->orderBy('updated_at', 'DESC')
                         ->orderBy('id', 'DESC')
                         ->get();

        if ($user->admin || $user->vendor) {
            $bids = ( $user->bid );
            foreach ($bids as $bid) {
                if ($bid->thread) {
                    $responseThread[] = $bid->thread;
                }
            }
        }

        $threads = $threads->merge($responseThread);

        return $threads->values()->all();
    }


    /**
     * Mark a specific thread as read, for ajax use
     *
     * @param $id
     */
    public function read($id)
    {
        $thread = Thread::find($id);
        if (!$thread) {
            abort(404);
        }

        $thread->markAsRead(Auth::id());
    }


    /**
     * Get the number of unread threads, for ajax use
     *
     * @return array
     */
    public function unread()
    {
        $redis = Redis::connection();
        $count = Auth::user()->newMessagesCount();
        $redis->publish('message', $count);

        return [ 'msg_count' => $count ];
    }

    public function messages()
    {
        $from = ( Input::has('from') ) ? date('Y-m-d H:i:s', strtotime(Input::get('from'))) : date('Y-m-d H:i:s', strtotime('yesterday'));
        $to = ( Input::has('to') ) ? date('Y-m-d H:i:s', strtotime(Input::get('to'))) : date('Y-m-d H:i:s', strtotime('now'));
        $messages = Message::with('user', 'participants', 'thread')->whereBetween('created_at', array( $from, $to ))->latest()->paginate(10);

        return  $this->view('admin.activities.messages', compact('messages'))->with('from', $from)->with('to', $to);
    }

    public function threads()
    {
        $from = ( Input::has('from') ) ? date('Y-m-d H:i:s', strtotime(Input::get('from'))) : date('Y-m-d H:i:s', strtotime('yesterday'));
        $to = ( Input::has('to') ) ? date('Y-m-d H:i:s', strtotime(Input::get('to'))) : date('Y-m-d H:i:s', strtotime('now'));
        $threads = Thread::whereBetween('created_at', array( $from, $to ))->latest()->paginate(10);
        $leads = Thread::whereBetween('created_at', array( $from, $to ))->orderBy('created_at', 'DESC')->get();
        $bids = Bid::with('thread')->whereBetween('created_at', array( $from, $to ))->orderBy('created_at', 'DESC')->get();

        return $this->view('admin.activities.threads', compact('threads'))
                    ->with('from', $from)
                    ->with('to', $to)
                    ->with('bids', $bids);

    }


    /**
     * @param       $view
     * @param array $data
     *
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $data = [ ])
    {
        if ($this->is_ajax) {
            return view($view, $data);
        }

        return view('layouts.non-ajax-layout', $data)
            ->with('content', $view);

    }
}
