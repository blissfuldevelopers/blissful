<?php namespace App\Http\Controllers;

use App\User;
use App\Classification;
use App\Profile;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Support\Facades\Auth;
use App\Post;

class UserController extends Controller
{
    protected $default_jobs_route;

    public function getHome()
    {
        $responseThreads = [ ];
        $threads = [ ];
        $user = Auth::user();
        $currentUserId = $user->id;
        // All threads that user is participating in
        $all = Thread::forUser($currentUserId)->orderBy('updated_at', 'DESC')->get();
        $classifications = Classification::lists('name', 'id');
        //get jobs user has posted
        $jobs = ( $user->job );
        $responseThread = [ ];

        foreach ($jobs as $job) {
            foreach ($job->bids as $bid) {
                if ($bid->thread) {
                    $responseThread [] = $bid->thread;
                }
            }
        }

        if (!empty( $responseThread )) {
            $threads = $all->diff($responseThread);
            $responseThreads = collect($responseThread)->sortByDesc('updated_at');
        }
        else{
            $threads=Thread::forUser($currentUserId)->orderBy('updated_at', 'DESC')->get();
        }

        $posts = Post::all();

        if (is_null($posts)) {
            $posts = [ ];
        }
        foreach ($posts as $post) {

        }

        $default_jobs_route = route('jobs.show', 'mine');

        return view('panels.user.home', compact('threads', 'currentUserId'))
            ->with('responseThreads', $responseThreads)
            ->with('post', $post)
            ->with('classifications', $classifications)
            ->with('default_jobs_route', $default_jobs_route)
            ->with('profile', Profile::find($currentUserId));
    }
}