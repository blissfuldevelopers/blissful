<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use Auth;
use ResponseManager;
use Session;
use App\User;
use App\Models\ShopProduct;
use Mail;
use Request;
use Hash;
use Config;
use JWT;
use GuzzleHttp;

class LoginController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (Auth::check()) {
            if (Auth::user()->hasRole('administrator') || Auth::user()->hasRole('vendor')) {
                return view('admin');
            }
            else {
                return redirect('/shop');
            }
        }
        else {
            return view('login');
        }
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        $message = 'Logout successful';

        return Response()->json(ResponseManager::getResult('', 10, $message));
    }

    public function doLogin()
    {
        $input = Request::all();

        Auth::attempt(array(
                          'email'    => $input[ 'email' ],
                          'password' => $input[ 'password' ] )
            , true);

        if (Auth::check()) {
            $message = 'Success';

            return Response()->json(ResponseManager::getResult('', 10, $message));
        }
        else {
            $message = 'Username or password is incorrect';

            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    public function logginuser()
    {
        if (Auth::check()) {
            $user = User::find(Auth::User()->id);
            $user[ 'type' ] = 'user';
            if (Auth::user()->hasRole('administrator')) {
                $user[ 'type' ] = 'admin';
            }
            if (Auth::user()->hasRole('vendor')) {
                $user[ 'type' ] = 'vendor';
            }
            $message = 'Success';

            return Response()->json(ResponseManager::getResult($user, 10, $message));
        }
        else {
            $message = 'Please login';

            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    public function forgotPassword()
    {
        $input = Request::all();
        $data[ 'code' ] = substr(md5(rand(0, 1000000)), 0, 7);
        $user = User::where('email', $input[ 'email' ]);
        $exist = $user->count();
        if ($exist == 0) {
            $message = 'Email id is not register with us.';

            return Response()->json(ResponseManager::getError('', 10, $message));
        }
        $update = $user->update($data);
        if ($update) {
            $data[ 'email' ] = $input[ 'email' ];
            Mail::send('emails.password', $data, function ($message) use ($data) {
                $message->to($data[ 'email' ])->subject('Password Reset!');
            });
            $message = 'Password reset link sent successfully to your email. Please check your mail';

            return Response()->json(ResponseManager::getResult($user, 10, $message));
        }
        else {
            $message = 'Error in sending email, please try again.';

            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    public function resetPassword()
    {
        $input = Request::all();
        $code = $input[ 'code' ];
        //Chk code exist

        $user = User::where('code', $code);
        $exist = $user->count();
        if ($exist == 0) {
            $message = 'Reset link could be expired, please try again';

            return Response()->json(ResponseManager::getError('', 10, $message));
        }
        $input[ 'code' ] = '';
        $input[ 'password' ] = Hash::make($input[ 'password' ]);
        $updateUser = $user->update($input);
        if ($updateUser) {
            $message = 'Your password reset properly, you can log in with your new password';

            return Response()->json(ResponseManager::getResult($user, 10, $message));
        }
        else {
            $message = 'Problems with the password reset, try again.';

            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

}
