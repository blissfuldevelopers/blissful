<?php

namespace App\Http\Controllers;

use App\DataTables\ReminderDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateReminderRequest;
use App\Http\Requests\UpdateReminderRequest;
use App\Repositories\ReminderRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;
use Illuminate\Http\Request;

class ReminderController extends AppBaseController
{
    /** @var  ReminderRepository */
    private $reminderRepository;
    protected $is_ajax = false;
    protected $task_type = 'reminder';

    public function __construct(ReminderRepository $reminderRepo, Request $request)
    {
        $this->is_ajax = $request->ajax();
        $this->reminderRepository = $reminderRepo;
    }

    /**
     * Display a listing of the Reminder.
     *
     * @param ReminderDataTable $reminderDataTable
     *
     * @return Response
     */
    public function index()
    {
        $reminders = $this->reminderRepository->paginate(10);

        return $this->view('reminders.index')
                    ->with('reminders', $reminders);
    }

    /**
     * Show the form for creating a new Reminder.
     *
     * @return Response
     */
    public function create()
    {
        return $this->view('reminders.create');
    }

    /**
     * Store a newly created Reminder in storage.
     *
     * @param CreateReminderRequest $request
     *
     * @return Response
     */
    public function store(CreateReminderRequest $request)
    {
        $input = $request->all();

        $input[ 'task_type' ] = $this->task_type;
        $reminder = $this->reminderRepository->create($input);

        Flash::success('Reminder saved successfully.');

        return redirect(route('reminders.index'));
    }

    /**
     * Display the specified Reminder.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $reminder = $this->reminderRepository->findWithoutFail($id);

        if (empty( $reminder )) {
            Flash::error('Reminder not found');

            return redirect(route('reminders.index'));
        }

        return $this->view('reminders.show')->with('reminder', $reminder);
    }

    /**
     * Show the form for editing the specified Reminder.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $reminder = $this->reminderRepository->findWithoutFail($id);

        if (empty( $reminder )) {
            Flash::error('Reminder not found');

            return redirect(route('reminders.index'));
        }

        return $this->view('reminders.edit')->with('reminder', $reminder);
    }

    /**
     * Update the specified Reminder in storage.
     *
     * @param  int                  $id
     * @param UpdateReminderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReminderRequest $request)
    {
        $reminder = $this->reminderRepository->findWithoutFail($id);

        if (empty( $reminder )) {
            Flash::error('Reminder not found');

            return redirect(route('reminders.index'));
        }
        $input = $request->all();
        $input[ 'task_type' ] = $this->task_type;
        $reminder = $this->reminderRepository->update($input, $id);

        Flash::success('Reminder updated successfully.');

        return redirect(route('reminders.index'));
    }

    /**
     * Remove the specified Reminder from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $reminder = $this->reminderRepository->findWithoutFail($id);

        if (empty( $reminder )) {
            Flash::error('Reminder not found');

            return redirect(route('reminders.index'));
        }

        $this->reminderRepository->delete($id);

        Flash::success('Reminder deleted successfully.');

        return redirect(route('reminders.index'));
    }

    /**
     * @param       $view
     * @param array $data
     *
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $data = [ ])
    {
        if ($this->is_ajax) {
            return view($view, $data);
        }

        return view('layouts.non-ajax-layout', $data)
            ->with('content', $view);

    }
}
