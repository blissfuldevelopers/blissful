<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classification;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Intervention\Image\Facades\Image;
use Input;
use Illuminate\Support\Facades\Response;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Job;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class ClassificationController extends Controller
{
    protected $is_ajax = false;

    public function __construct(Request $request)
    {
        $this->is_ajax = $request->ajax();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $classifications = Classification::whereNull('parent_id')->paginate(15);

        return view('admin.classification.index', compact('classifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.classification.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'description' => 'required', 'meta_title' => 'required', 'primary_keyword' => 'required', 'page_content' => 'required', 'priority' => 'required',]);

        $thumbnail = $request->file('thumbnail_path');
        $thumbnailname = uniqid() . $thumbnail->getClientOriginalName();
        $thumb = Image::make($thumbnail->getRealPath())->resize(320, 160);

        $image_thumb = $thumb->stream();
        $s3 = \Storage::disk('s3');
        $filePath = '/img/categories/' . $thumbnailname;
        $s3->put($filePath, $image_thumb->__toString(), 'public');

        $coverimg = $request->file('cover_image_path');
        $covername = uniqid() . $coverimg->getClientOriginalName();
        $thumb = Image::make($coverimg->getRealPath())->resize(1400, 120);

        $image_thumb = $thumb->stream();
        $s3 = \Storage::disk('s3');
        $filePath = '/img/categories/' . $covername;
        $s3->put($filePath, $image_thumb->__toString(), 'public');

        $classification = new Classification;
        $classification->name = $request->input('name');
        $classification->slug = str_slug($request->input('name'), "-");
        $classification->meta_title = $request->input('meta_title');
        $classification->primary_keyword = $request->input('primary_keyword');
        $classification->page_content = $request->input('page_content');
        $classification->priority = $request->input('priority');
        $classification->border_color = $request->input('border_color');
        $classification->meta_description = $request->input('meta_description');
        $classification->description = $request->input('description');
        $classification->thumbnail_path = ('/img/categories/' . $thumbnailname);
        $classification->cover_image_path = ('/img/categories/' . $covername);
        $classification->save();
        Session::flash('flash_message', 'Classification successfully added!');

        return redirect('classification');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $classification = Classification::findOrFail($id);

        return view('admin.classification.show', compact('classification'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $classification = Classification::findOrFail($id);

        return view('admin.classification.edit', compact('classification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', 'description' => 'required', 'meta_title' => 'required', 'primary_keyword' => 'required', 'page_content' => 'required', 'priority' => 'required',]);

        $classification = Classification::findOrFail($id);
        if (Input::hasFile('thumbnail_path')) {
            $thumbnail = $request->file('thumbnail_path');
            $thumbnailname = uniqid() . $thumbnail->getClientOriginalName();
            $thumb = Image::make($thumbnail->getRealPath())->resize(320, 160);

            $image_thumb = $thumb->stream();
            $s3 = \Storage::disk('s3');
            $filePath = '/img/categories/' . $thumbnailname;
            $s3->put($filePath, $image_thumb->__toString(), 'public');
        }
        if (Input::hasFile('cover_image_path')) {
            $coverimg = $request->file('cover_image_path');
            $covername = uniqid() . $coverimg->getClientOriginalName();
            $thumb = Image::make($coverimg->getRealPath())->resize(1400, 120);

            $image_thumb = $thumb->stream();
            $s3 = \Storage::disk('s3');
            $filePath = '/img/categories/' . $covername;
            $s3->put($filePath, $image_thumb->__toString(), 'public');
        }

        $classification->name = $request->input('name');
        $classification->meta_title = $request->input('meta_title');
        $classification->primary_keyword = $request->input('primary_keyword');
        $classification->page_content = $request->input('page_content');
        $classification->priority = $request->input('priority');
        $classification->border_color = $request->input('border_color');
        $classification->meta_description = $request->input('meta_description');
        $classification->description = $request->input('description');
        $classification->bid_credits = $request->input('bid_credits');
        if (Input::hasFile('thumbnail_path')) {
            $classification->thumbnail_path = ('/img/categories/' . $thumbnailname);
        }
        if (Input::hasFile('thumbnail_path')) {
            $classification->cover_image_path = ('/img/categories/' . $covername);
        }
        $classification->update();

        Session::flash('flash_message', 'Classification successfully updated!');

        return redirect('classification');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {

        DB::table('classifications')->where('parent_id', '=', $id)->orWhere('id', '=', $id)->delete();

        Flash::success('Category Deleted Successfully');

        return redirect()->back();
    }

    public function add_root_parent()
    {
        $classifications = Classification::all();

        return view('admin.classification.subcategory.create', compact('classifications'));
    }

    public function store_root_parent(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'description' => 'required']);

        $parent = Classification::findOrFail($request->input('parent_id'));

        $classification = new Classification;
        $classification->name = $request->input('name');

        if (empty($parent->root_parent_id)) {
            $classification->root_parent_id = $request->input('parent_id');
        } else {
            $classification->root_parent_id = $parent->root_parent_id;
        }

        $classification->parent_id = $request->input('parent_id');
        $classification->description = $request->input('description');
        $classification->slug = str_slug($parent->name, "-") . '-' . str_slug($request->input('name'), "-");
        $classification->save();

        Flash::success('Added successfully');

        return redirect()->back();
    }

    public function add_parent()
    {
        $classifications = Classification::whereNull('parent_id')->get();

        return view('admin.classification.sub-subcategory.create', compact('classifications'));
    }

    public function store_parent(Request $request)
    {

        $this->validate($request, ['name' => 'required', 'description' => 'required']);

        $parent = Classification::findOrFail($request->input('parent_id'));

        $classification = new Classification;
        $classification->name = $request->input('name');
        $classification->description = $request->input('description');
        $classification->root_parent_id = $request->input('root_parent_id');
        $classification->parent_id = $request->input('parent_id');
        $classification->slug = str_slug($parent->name, "-") . '-' . str_slug($request->input('name'), "-");

        $classification->save();

        Flash::success('Added successfully');

        return redirect()->back();
    }

    public function categoryDropDownData()
    {

        $cat_id = Input::get('cat_id');


        $children = Classification::where('parent_id', '=', $cat_id)
            ->orderBy('name', 'asc')
            ->get();

        return Response::json($children);


    }

    public function edit_root_parent($id)
    {
        $classification = Classification::findOrFail($id);
        $categories = Classification::whereNull('parent_id')->get();

        return view('admin.classification.subcategory.edit', compact('classification', 'categories'));
    }

    public function update_root_parent($id, Request $request)
    {
        $request->all();

        $this->validate($request, ['name' => 'required', 'description' => 'required']);

        $parent = Classification::findOrFail($request->input('root_parent_id'));

        $classification = Classification::findOrFail($id);
        $classification->name = $request->input('name');
        $classification->parent_id = $request->input('root_parent_id');
        $classification->root_parent_id = $request->input('root_parent_id');
        $classification->description = $request->input('description');
        $classification->slug = str_slug($parent->name, "-") . '-' . str_slug($request->input('name'), "-");

        $classification->update();

        Flash::success('Updated successfully');

        return redirect()->back();
    }

    public function edit_parent($id)
    {
        $classification = Classification::findOrFail($id);
        $classes = Classification::whereNull('parent_id')->get();
        $classes_1 = Classification::all();

        return view('admin.classification.sub-subcategory.edit', compact('classification', 'classes', 'classes_1'));
    }

    public function update_parent($id, Request $request)
    {
        $request->all();

        $this->validate($request, ['name' => 'required', 'description' => 'required']);

        $parent = Classification::findOrFail($request->input('root_parent_id'));

        $classification = Classification::findOrFail($id);
        $classification->name = $request->input('name');
        $classification->parent_id = $request->input('parent_id');
        $classification->root_parent_id = $request->input('root_parent_id');
        $classification->description = $request->input('description');
        $classification->slug = str_slug($parent->name, "-") . '-' . str_slug($request->input('name'), "-");

        $classification->update();

        Flash::success('Updated successfully');

        return redirect()->back();
    }

    public function classification_bids(Request $request)
    {

        $classifications = Classification::whereNull('parent_id')->get();

        foreach ($classifications as $classification) {
            $slug = $classification->slug;
            $arrays = User::with('classifications')->where('test_account', '=', 0)
                ->whereHas('classifications', function ($query) use ($slug) {
                    $query->where('slug', '=', $slug);
                })->orderBy('ranking', 'DESC')->orderBy('last_login', 'DESC')->get();
        }


        $input = $request->all();
        $classifications = Classification::whereNull('parent_id')->get();

        if (empty($input) || !array_key_exists('classification', $input) || $input['classification'] == 'all') {
            $users = User::join('bids', 'bids.user_id', '=', 'users.id')
                ->groupBy('users.id')
                ->get(['users.id', 'users.first_name', DB::raw('count(bids.id) as bids')])
                ->sortByDesc('bids');
            $users = $this->paginate_results($request, $users);
            $total_jobs = Job::join('bids', 'bids.job_id', '=', 'jobs.id')
                ->get([DB::raw('count(DISTINCT jobs.id) as jobs'), DB::raw('count(bids.id) as bids')]);
            $total_jobs = $total_jobs[0];

            return view('admin.classification.classification_bids', compact('total_jobs', 'users', 'classifications'));
        }

        $slug = $input['classification'];
        $selected = Classification::whereSlug($slug)->first();
        $users = Job::where('classification', '=', $selected->name)
            ->join('bids', 'bids.job_id', '=', 'jobs.id')
            ->join('users', 'users.id', '=', 'bids.user_id')
            ->groupBy('users.id')
            ->get(['users.id', 'users.first_name', DB::raw('count(bids.id) as bids')])
            ->sortByDesc('bids');
        $users = $this->paginate_results($request, $users);

        $total_jobs = Job::where('classification', '=', $selected->name)
            ->join('bids', 'bids.job_id', '=', 'jobs.id')
            ->get([DB::raw('count(DISTINCT jobs.id) as jobs'), DB::raw('count(bids.id) as bids')]);
        $total_jobs = $total_jobs[0];

        return view('admin.classification.classification_bids', compact('total_jobs', 'users', 'classifications', 'selected'));
    }

    public function paginate_results(Request $request, $results, $defaults = [])
    {
        $results_data = [];
        $paginated_data = [];

        $limit = (Input::has('limit')) ? Input::get('limit')
            : array_get($defaults, 'limit', 10);

        if ($limit != -1) {
            $offset = (Input::has('page')) ? (Input::get('page') - 1) * $limit : 0;
        }

        $page = (Input::has('page')) ? Input::get('page')
            : array_get($defaults, 'page', 1);


        foreach ($results as $result) {
            $results_data [] = $result;
        }
        $itemsForCurrentPage = ($limit != -1) ? array_slice($results_data, $offset, $limit, true) : $results_data;

        $paginated_data = ($limit != -1) ? new Paginator($itemsForCurrentPage, count($results), $limit, $page, [
            'path' => $request->url(),
            'query' => $request->query(),
        ]) : $results;

        return $paginated_data;
    }

    public function category_options()
    {
        $classifications = Classification::whereNull('parent_id')->paginate(15);

        return view('admin.classification.options', compact('classifications'));
    }

}
