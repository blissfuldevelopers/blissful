<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\PostFormFields;
use App\Http\Requests;
use App\Http\Requests\PostCreateRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Http\Controllers\Controller;
use App\Post;
use Input;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{
    protected $is_ajax = false;

    public function __construct(Request $request)
    {
        $this->is_ajax = $request->ajax();
    }

    /**
     * Display a listing of the posts.
     */
    public function index()
    {
        return $this->view('admin.post.index')
                    ->withPosts(Post::orderBy('created_at','DESC')->paginate(10));
    }

    /**
     * Show the new post form
     */
    public function create()
    {
        $data = $this->dispatch(new PostFormFields());

        return $this->view('admin.post.create', $data);
    }

    /**
     * Store a newly created Post
     *
     * @param PostCreateRequest $request
     */
    public function store(PostCreateRequest $request)
    {
        $post = Post::create($request->postFillData());
        $post->syncCategories($request->get('categories', [ ]));
        if (Input::hasFile('page_image')) {
            $file = $request->file('page_image');
            $filename = $file->getClientOriginalName();
            $file->move('uploads/', $filename);
            $thumb = Image::make('uploads/' . $filename);

            $image_thumb = $thumb->stream();
            $s3 = \Storage::disk('s3');
            $filePath = 'uploads/' . $filename;
            $s3->getDriver()->put($filePath, $image_thumb->__toString(),[ 
                
                                                                            'visibility' => 'public',
                                                                            'CacheControl' => 'max-age=2592000',
                                                                            'Expires' => gmdate("D, d M Y H:i:s T",strtotime("+3 years"))

                                                                        ]);

            unlink(public_path('uploads/' . $filename));
        }

        return redirect()
            ->route('admin.post.index')
            ->withSuccess('New Post Successfully Created.');
    }

    /**
     * Show the post edit form
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $data = $this->dispatch(new PostFormFields($id));
        $content_raw = ($data['content']);
        return $this->view('admin.post.edit', $data)->with('content_raw',$content_raw);
    }

    /**
     * Update the Post
     *
     * @param PostUpdateRequest $request
     * @param int               $id
     */
    public function update(PostUpdateRequest $request, $id)
    {
        $post = Post::findOrFail($id);
        $post->fill($request->postFillData());
        $post->save();
        $post->syncCategories($request->get('categories', [ ]));
        if (Input::hasFile('page_image')) {
            $file = $request->file('page_image');
            $filename = $file->getClientOriginalName();
            $file->move('uploads/', $filename);

            $thumb = Image::make('uploads/' . $filename);

            $image_thumb = $thumb->stream();
            $s3 = \Storage::disk('s3');
            $filePath = 'uploads/' . $filename;
            $s3->getDriver()->put($filePath, $image_thumb->__toString(),[ 
                
                                                                            'visibility' => 'public',
                                                                            'CacheControl' => 'max-age=2592000',
                                                                            'Expires' => gmdate("D, d M Y H:i:s T",strtotime("+3 years"))

                                                                        ]);


            unlink(public_path('uploads/' . $filename));
        }

        if ($request->action === 'continue') {
            return redirect()
                ->back()
                ->withSuccess('Post saved.');
        }

        return redirect()
            ->route('admin.post.index')
            ->withSuccess('Post saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->categories()->detach();
        $post->delete();

        return redirect()
            ->route('admin.post.index')
            ->withSuccess('Post deleted.');
    }


    /**
     * @param       $view
     * @param array $data
     *
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function view($view, $data = [ ])
    {
        if ($this->is_ajax) {
            return view($view, $data);
        }

        return view('layouts.non-ajax-layout', $data)
            ->with('content', $view);
    }
}
