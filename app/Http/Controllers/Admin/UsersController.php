<?php

namespace App\Http\Controllers\Admin;

use App\Credit;
use App\Description;
use Illuminate\Http\Request;
use App\User;
use App\Profile;
use App\Role;
use App\Locations;
use Illuminate\Support\Facades\Auth;
use App\Gallery;
use App\Http\Requests;
use App\Classification;
use App\Http\Controllers\Controller;
use Input, Hash, Validator;
use Intervention\Image\Facades\Image;
use App\Logic\Mailers\UserMailer;
use App\Traits\CaptchaTrait;
use App\Activity;
use Yajra\Datatables\Datatables;
use App\CreditsDebitCredit;
use Mixpanel;
Use App\Meta;
use Carbon\Carbon;
use Illuminate\Contracts\Filesystem\Filesystem;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB;
use App\Models\Boost;
use App\UserTokens;

class UsersController extends Controller
{
    use CaptchaTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $userMailer;
    protected $is_ajax = false;

    public function __construct(UserMailer $userMailer, Request $request)
    {
        $this->userMailer = $userMailer;
        $this->is_ajax = $request->ajax();
    }

    public function index()
    {
        if (!Auth::check()) {
            return redirect()->back();
        } elseif (Auth::user()->hasuserRole('administrator')) {
            $users = User::with('profile')->orderBy('updated_at', 'DESC')->withTrashed()->get();

            return $this->view('admin.users.index')->with('users', $users);
        }

        return redirect()->back();
    }

    public function getUsers()
    {
        $users = User::with('profile');

        return Datatables::of($users)
            ->addColumn('action', '
                                <a class="btn btn-success btn-xs" href="/reset/first/{{$id}}">Join request</a>
                                <a class="btn btn-success btn-xs" href="/users/{{$id}}/edit">Edit</a>
                                <a class="btn btn-xs" href="/vendor/gallery/{{$id}}">Edit Galleries</a>
                                <a href="/admin/users/delete/{{$id}}" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-trash"></span></a>
                                ')
            ->addColumn('role', function ($user) {
                foreach ($user->roles as $role) {
                    return $role->name;
                }

            })
            ->make(true);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classifications = Classification::whereNull('parent_id')->lists('name', 'id')->all();

        $locations = Locations::all();

        return $this->view('admin.users.create')->with('classifications', $classifications)->with('locations', $locations);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        set_time_limit(0);
        $input = Input::all();

        if ($this->captchaCheck() == false) {
            return redirect()->back()
                ->withErrors(['Wrong Captcha'])
                ->withInput();
        } else {
            $validation = Validator::make($input, User::$rules);
            if ($validation->passes()) {

                $token = str_random(30);

                $user = new User;
                $user->email = $input['email'];
                $user->first_name = ucfirst($input['first_name']);
                $user->password = Hash::make($input['password']);
                $user->token = $token;
                $user->slug = str_slug($input['first_name'], "-");
                if (Auth::check()) {
                    if (Auth::user()->hasUserRole('administrator')) {
                        $user->verified = 1;
                        $user->added_by_admin = 1;
                    }
                }
                $user->save();

                //save featured image
                $featured = $request->file('file');
                $featuredname = uniqid() . $featured->getClientOriginalName();

                $thumb = Image::make($featured->getRealPath())->resize(240, 160);

                $image_thumb = $thumb->stream();
                $s3 = \Storage::disk('s3');
                $filePath = 'gallery/images/featured-images/' . $featuredname;
                $s3->put($filePath, $image_thumb->__toString(), 'public');

                //create profile
                $profile = $user->Profile ?: new Profile;
                $profile->user_id = $user->id;
                $profile->location = $input['location'];
                $profile->phone = $input['phone'];
                $profile->profilePic = $filePath;
                $profile->description = $input['description'];
                $user->profile()->save($profile);

                //Assign Role
                $role = Role::whereName('vendor')->first();
                $user->assignRole($role);
                $profile->save();

                //create Portfolio
                $gallery = new Gallery;

                //save new gallery
                $gallery->name = ('Portfolio');
                $gallery->description = ('A showcase of some of the work done by the vendor');
                $gallery->gallery_image = $user->profile->profilePic;
                $gallery->created_by = $user->id;
                $gallery->published = 1;
                $gallery->save();

                //credit the vendor
                CreditsDebitCredit::creditUser($user->id, 300, 0, 0);

                //Category
                $user->classifications()->attach($input['classifications_list']);

                Activity::create([
                    'subject_id' => $user->id,
                    'subject_type' => 'App\User',
                    'name' => 'vendor_registered',
                    'user_id' => $user->id,
                ]);


                if (!Auth::check()) {
                    $data = [
                        'first_name' => $user->first_name,
                        'token' => $token,
                        'subject' => 'Verify your email address',
                        'email' => $user->email,
                    ];

                    $this->userMailer->verify($user->email, $data);

                    $user->send_sms_verification($user->id);

                    return $this->view('auth.verify-mobile',compact('user'));

                } elseif (Auth::user()->hasUserRole('administrator')) {
                    $id = $user->id;

                    // return redirect('gallery/edit/' . $id)
                    return redirect('portfolio/' . $id);
                }
            } else {
                return redirect()->route('users.create')
                    ->withInput()
                    ->withErrors($validation)
                    ->withInput(Input::except('password'))
                    ->with('message', 'There were validation errors.');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        if (!Auth::check() || Auth::user()->id != $id && !Auth::user()->hasUserRole('administrator')) {
            return;
        }
        $user = User::findorfail($id);
        $classifications = Classification::whereNull('parent_id')->lists('name', 'id')->all();
        $credits = CreditsDebitCredit::total_balance($id);
        $selected = $user->featured_classifications->lists('name', 'id')->all();
        $boosts = Boost::whereNull('deleted_at')->get();
        $selected_boosts = $user->boosts->lists('name', 'id')->all();
        $credit_history = Credit::where('credits.user_id', $id)
            ->join('credit_bundles', function ($join) {
                $join
                    ->on('credits.credit_bundle_id', '=', 'credit_bundles.id');
            })
            ->leftjoin('shop_product', function ($join) {
                $join
                    ->on('credit_bundles.product_id', '=', 'shop_product.id');
            })
            ->orderBy('credits.created_at', 'DESC')
            ->get([
                'shop_product.name',
                'credits.quantity',
                'shop_product.sale_price',
                'credits.created_at'
            ]);

        return $this->view('admin.users.show', compact('user', 'credits', 'classifications', 'selected', 'credit_history', 'boosts', 'selected_boosts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request){

        
        $user = User::with('profile')->find($id);
        if (!Auth::check() || Auth::user()->id != $id && !Auth::user()->hasUserRole('administrator')) {
            return;
        }
        //is this a user or vendor account?
        if (!$user->hasuserRole('vendor')) {

            //check if can edit account
            if (Auth::user()->id == $id || Auth::user()->hasUserRole('administrator')) {
                return $this->view('panels.vendor.edit', compact('user'));
            }

        } elseif (Auth::user()->id == $id || Auth::user()->hasUserRole('administrator')) {
            $profile = $user->profile;
            $selected = $user->classifications->lists('id')->all();
            $classifications = Classification::whereNull('parent_id')->orderBy('priority', 'ASC')->get();
            $locations = Locations::all();
            $boosts = Boost::whereNull('deleted_at')->get();
            $selected_boosts = $user->boosts->lists('id')->all();

            return $this->view('admin.users.edit', compact(
                'locations', 'user', 'profile', 'classifications', 'selected', 'boosts', 'selected_boosts'));
        } else {
            return redirect('500');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $user = User::find($id);
        if (!Auth::check() || Auth::user()->id != $id && !Auth::user()->hasUserRole('administrator')) {
            return;
        }
        $rules = array(
            'first_name' => 'required',
            'email' => 'required',
            'location' => 'required',
            'phone' => 'required',
            'description' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            // get the error messages from the validator
            $messages = $validator->messages();

            // redirect our user back to the form with the errors from the validator
            return redirect()->back()
                ->withErrors($validator);
        }

        $input = $request->all();
        $profile = $user->Profile ?: new Profile;
        $user->email = $input['email'];
        $user->first_name = ucfirst($input['first_name']);

        //check if the user is trying to update password
        if ($request->has('password')) {
            $user->password = Hash::make($input['password']);
        }
        //save user details
        $user->save();

        //profile stuff

        $profile->location = $input['location'];

        //check if  they're trying to update profile image
        if (Input::hasFile('file')) {
            $file = $input['file'];
            $filename = uniqid() . $file->getClientOriginalName();
            $thumb = Image::make($file->getRealPath())->resize(240, 160);

            $image_thumb = $thumb->stream();
            $s3 = \Storage::disk('s3');
            $filePath = 'gallery/images/featured-images/' . $filename;
            $s3->getDriver()->put($filePath, $image_thumb->__toString(),[ 
                                                                            'visibility' => 'public',
                                                                            'CacheControl' => 'max-age=2592000',
                                                                            'Expires' => gmdate("D, d M Y H:i:s T",strtotime("+3 years"))

                                                                        ]);

            $profile->profilePic = ('gallery/images/featured-images/' . $filename);
        }
        $profile->phone = $input['phone'];
        $profile->description = $input['description'];
        $user->profile()->save($profile);

        //sync classifications
        $user->classifications()->sync($input['classifications_list']);

        //sync boosts
        if ($request->has('boosts_list')) {
            $user->boosts()->sync($input['boosts_list']);
        }


        Activity::create([
            'subject_id' => $user->id,
            'subject_type' => 'App\User',
            'name' => 'updated_profile',
            'user_id' => $user->id,
        ]);


        $success = true;
        return redirect()->route('users.edit', ['id' => $user->id])
                ->with('status', 'success')
                ->with('message','Profile updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $user = User::find($id);
        $name = $user->first_name;
        $user->delete();

        return redirect()->route('users.index')->with('message', 'User ' . $name . ' has been deleted successfully')
            ->with('status', 'success');
    }
    public function restore($id)
    {

        $user = User::withTrashed()->find($id);
        $name = $user->first_name;
        $user->restore();

        return redirect()->route('users.index')->with('message', 'User ' . $name . ' has been restored successfully')
            ->with('status', 'success');
    }

    public function portfolioShow($id)
    {
        if (Auth::user()->hasUserRole('administrator')) {
            return $this->view('admin.users.portfolio')->with('id', $id);
        } elseif (Auth::user()->hasUserRole('vendor') && Auth::user()->id == $id) {
            return $this->view('admin.users.portfolio')->with('id', $id);
        } else {
            return redirect()->back();
        }

    }

    public function portfolio(Request $request, $id)
    {

        $user = User::find($id);
        foreach ($user->galleries as $gallery) {
            if ($gallery->name == 'Portfolio') {
                $g_id = $gallery->id;
            }
        }
        //save portfolio images
        $file = $request->file('file');
        $filename = uniqid() . $file->getClientOriginalName();

        //move file to save location
        $file->move('gallery/images', $filename);
        //save thumbnail to path
        $image = Image::make('gallery/images/' . $filename)->resize(640, 350);

        $image = $image->stream();
        $s3 = \Storage::disk('s3');
        $filePath = 'gallery/images/compressed/' . $filename;
        $s3->put($filePath, $image->__toString(), 'public');

        $thumb = Image::make('gallery/images/' . $filename)->resize(240, 160);

        $thumb = $thumb->stream();
        $s3 = \Storage::disk('s3');
        $filePath = 'gallery/images/thumbs/' . $filename;
        $s3->put($filePath, $thumb->__toString(), 'public');

        $gall = Gallery::find($g_id);

        //save image details to db
        $image = $gall->images()->create([
            'gallery_id' => $g_id,
            'file_name' => $filename,
            'file_size' => $file->getClientSize(),
            'file_mime' => $file->getClientMimeType(),
            'file_path' => 'gallery/images/compressed/' . $filename,
            'created_by' => $id,
            'caption' => 'No Caption Added',
        ]);
        unlink(public_path('gallery/images/' . $filename));

        return redirect()->route('users.index');
    }

    public function sendVerify($id)
    {
        $user = User::find($id);
        $token = str_random(30);
        $user->verified = 0;
        $user->token = $token;
        $user->save();

        $data = [
            'first_name' => $user->first_name,
            'token' => $token,
            'subject' => 'Verify your email address',
            'email' => $user->email,
        ];

        $this->userMailer->verify($user->email, $data);

        return redirect()->route('users.index');
    }

    public function returnLog()
    {
        $latestActivities = Activity::with('user')->latest()->limit(100)->get();

        return view('admin.users.log')->with('latestActivities', $latestActivities);
    }

    public function vendorListings()
    {
        $users = User::all();
        foreach ($users as $user) {
            if ($user->hasUserRole('vendor')) {

            }
        }
    }

    private function view($view, $data = [])
    {
        if ($this->is_ajax) {
            return view($view, $data);
        }

        return view('layouts.non-ajax-layout', $data)
            ->with('content', $view);

    }

    public function status_update($id, Request $request)
    {
        $user = User::find($id);

        $user->verified = $request['verified'] ?: 0;
        $user->added_by_admin = $request['added_by_admin'] ?: 0;
        $user->test_account = $request['test_account'] ?: 0;
        $user->save();

        $selected = $user->featured_classifications->lists('name', 'id')->all();

        if ($request->has('classifications_list')) {
            $user->featured_classifications()->sync($request['classifications_list']);
        } elseif (!$request->has('classifications_list' && $selected)) {
            $user->featured_classifications()->detach();
        }


        Flash::success('Successfully updated!');

        return redirect()->route('users.show', $user->id);
    }

    public function options_edit($id)
    {
        $user = User::find($id);
        $classifications = Classification::whereNull('parent_id')->orderBy('priority', 'ASC')->get();
        $selected = $user->classifications->lists('id')->all();
        $descriptions = Description::where('user_id', $user->id);
        return $this->view('admin.users.options', compact('classifications', 'selected','user','descriptions'));

    }
    public function options_update($id, Request $request)
    {
        $user = User::find($id);
        $user->classifications()->sync($request['classifications']);

        Flash::success('Successfully updated!');

        return redirect()->route('categories.edit', $user->id);
    }
    public function search(Request $request)
    {

        $keyword = $request->input('term');
        if ($keyword) {
            $results = User::where('first_name', 'like', "$keyword%")->orWhere('last_name', 'like', "$keyword%")->get();

            foreach ($results as $key => $value) {
                $list[$key]['id'] = $value->id;
                $list[$key]['text'] = $value->first_name;
            }
            if (isset($list)) {
                return $list;
            }
        }


    }
    

    public function process_sms_verification($user_id,Request $request){
        
        $user = User::where('id',$user_id)->first();
        $token_id = $request->input('token');

        $match_criteria = [ 'user_id'=>$user->id, 'token'=>$token_id];
        $token = UserTokens::where($match_criteria)->first();
       
        if (is_null($token)) {
            Flash::warning('The code input is incorrect, please try again');
            return $this->view('auth.verify-mobile', compact('user'));
        }
        if ($request->has('password')) {
            $user->password = Hash::make($request->input('password'));
        }

        
        //execute valid token
        if (strtotime($token->created_at) < strtotime($token->created_at->modify('+30 days'))) {

            //mark user as verified
            $user->verified = 1;
            $user->save();

            //delete token
            $token->delete();
            return redirect()->route('auth.login')
            ->with('status', 'success')
            ->with('message', 'Account successfully verified. Login below');

        }
        Flash::warning('The code input is incorrect, please try again');
            return $this->view('auth.verify-mobile', compact('user'));;
    }
//    public function my_test()
//    {
//        
//        $user = User::with('meta')->findorfail($id);
//        if ($user->hasMeta('expiration_date') && $user->hasMeta('first_login')) {
//
//                foreach ($user->meta->where('meta_key', 'first_login') as $meta) {
//                    $first_login_date = strtotime($meta->meta_value);
//                }
//
//                foreach ($user->meta->where('meta_key', 'expiration_date') as $meta) {
//                    $expiration_date = strtotime($meta->meta_value);
//                }
//
//                if ((strtotime(Carbon::now()) - $expiration_date) / 86400 >= 45) {
//                    return ($expiration_date - $first_login_date) / 86400 ;
//                } else {
//                    return 'still have .....days';
//                }
//            } elseif (!$user->hasMeta('expiration_date')) {
//                //
//            }
//    }
}