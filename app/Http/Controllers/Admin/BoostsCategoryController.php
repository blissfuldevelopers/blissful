<?php

namespace App\Http\Controllers\Admin;

use App\Models\BoostCategory;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Boost;
use Illuminate\Http\Request;
use Session;

class BoostsCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $boostcategories = BoostCategory::paginate(25);

        return view('admin.boostcategories.index', compact('boostcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $categories = BoostCategory::whereNull('deleted_at')->get();

        return view('admin.boostcategories.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        return $this->save_boost_category($request);


    }

    /**
     * @param Request $request
     * @param null    $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function save_boost_category(Request $request, $id = null)
    {
        $this->validate($request, BoostCategory::$rules);

        $boostcategory = BoostCategory::saveBoostCategory($request, $id);

        if($boostcategory) {
            Session::flash('flash_message', 'Boost successfully added!');

            return redirect(route('admin.boostcategories.index'));
        }

        Session::flash('flash_message', 'Had trouble saving the boost');

        return Redirect::back()
                       ->withErrors($boostcategory);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $boostcategory = BoostCategory::findOrFail($id);

        return view('admin.boostcategories.show', compact('boostcategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $boostcategory = BoostCategory::findOrFail($id);
        $categories = BoostCategory::whereNull('deleted_at')->get();

        return view('admin.boostcategories.edit', [
            'boostcategory' => $boostcategory,
            'categories'    => $categories,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int                     $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $boostcategory = BoostCategory::findOrFail($id);
        $boostcategory->update($requestData);

        Session::flash('flash_message', 'Boost Category updated!');

        return redirect(route('admin.boostcategories.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        BoostCategory::destroy($id);

        Session::flash('flash_message', 'Boost deleted!');

        return redirect(route('admin.boostcategories.index'));
    }
}
