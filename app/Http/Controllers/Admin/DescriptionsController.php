<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Description;
use App\Models\Option;
use App\User;
use Illuminate\Http\Request;
use Session;

class DescriptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index($id)
    {
        $user = User::find($id);
        $descriptions = Description::where('user_id', $id)->paginate(25);

        return view('admin.descriptions.index', compact('descriptions','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create($id)
    {
        $user = User::find($id);
        $options = Option::all();
        return view('admin.descriptions.create', compact('user','options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request, $id)
    {
        
        $requestData = $request->all();
        $requestData['user_id'] = $id;
        Description::create($requestData);

        Session::flash('flash_message', 'Description added!');

        return redirect('user/options/'.$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $description = Description::findOrFail($id);

        return view('admin.descriptions.show', compact('description'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $description = Description::findOrFail($id);
        $user = User::find($description->user_id);
        $options = Option::all();
        return view('admin.descriptions.edit', compact('description','user','options'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $description = Description::findOrFail($id);
        $description->update($requestData);

        Session::flash('flash_message', 'Description updated!');

        return redirect('user/options/'.$description->user_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $description = Description::findOrFail($id);
        $user = User::find($description->user_id);
        Description::destroy($id);

        Session::flash('flash_message', 'Description deleted!');

        return redirect('user/options/'.$user->id);
    }
}
