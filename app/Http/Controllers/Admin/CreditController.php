<?php

namespace App\Http\Controllers\Admin;

use App\CreditsDebitCredit;
use App\User;
use Illuminate\Http\Request;

use App\CreditBundle;
use App\Http\Requests;
use App\Credit;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;

class CreditController extends Controller
{
    public function index()
    {
        $credit_bundles = CreditBundle::all();
        return view('bundles', compact('credit_bundles'));

    }

    public function purchase($id)
    {
        // credit
        Credit::create(
            [
                'credit_bundle_id' => $id,
                'user_id' => Auth::user()->id,
                'expiry_date' => new Carbon,
            ]
        );

        CreditsDebitCredit::create(
            [
                'user_id' => Auth::user()->id,
            ]
        );
        return redirect('bundles/home/');
    }
    public function create($id)
    {
        $user= User::findorfail($id);
        return view('admin.creditbundles.credit-vendor',compact('user'));

    }
    public function store(Request $request)
    {
        // credit vendor
        CreditsDebitCredit::create(
            [
                'user_id' => $request->input('user_id'),
                'debit' => 0,
                'credit' => $request->input('credit'),
                'object_id' => 0,
                'object_name' => 'credits'
            ]
        );
        $users = User::with('profile')->latest()->get();

        return view('admin.users.index')->with('users', $users);

    }
}
