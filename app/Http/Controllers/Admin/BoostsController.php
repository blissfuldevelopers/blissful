<?php

namespace App\Http\Controllers\Admin;

use App\Models\BoostCategory;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Boost;
use Illuminate\Http\Request;
use Session;

class BoostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $boosts = Boost::paginate(25);

        return view('admin.boosts.index', compact('boosts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $categories = BoostCategory::whereNull('deleted_at')->get();


        return view('admin.boosts.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        return $this->save_boost($request);
    }

    /**
     * @param Request $request
     * @param null    $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function save_boost(Request $request, $id = null)
    {
        $this->validate($request, Boost::$rules);

        $boost = Boost::saveBoost($request, $id);

        if($boost) {
            Session::flash('flash_message', 'Boost successfully added!');

            return redirect(route('admin.boosts.index'));
        }

        Session::flash('flash_message', 'Had trouble saving the boost');

        return Redirect::back()
                       ->withErrors($boost);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $boost = Boost::findOrFail($id);

        return view('admin.boosts.show', compact('boost'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $boost = Boost::findOrFail($id);

        return view('admin.boosts.edit', compact('boost'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int                     $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $boost = Boost::findOrFail($id);
        $boost->update($requestData);

        Session::flash('flash_message', 'Boost updated!');

        return redirect(route('admin.boosts.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Boost::destroy($id);

        Session::flash('flash_message', 'Boost deleted!');

        return redirect(route('admin.boosts.index'));
    }
}
