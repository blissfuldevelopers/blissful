<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CreditBundle;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;


class CreditBundlesController extends Controller
{
    protected $is_ajax = false;

    public function __construct(Request $request)
    {
        $this->is_ajax = $request->ajax();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $creditbundles = CreditBundle::orderBy('id', 'desc')
                                     ->paginate(15);

        return $this->view('admin.creditbundles.index', compact('creditbundles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return $this->view('admin.creditbundles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        return $this->save_credit_bundle($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $creditbundles = CreditBundle::orderBy('id', 'desc')->paginate(15);

        return $this->view('admin.creditbundles.vendor-credits', compact('creditbundles'));
    }

    public function vendor_credits()
    {
        $creditbundles = CreditBundle::orderBy('id', 'desc')->paginate(15);

        return $this->view('admin.creditbundles.vendor-credits', compact('creditbundles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $creditbundle = CreditBundle::findOrFail($id);

        return $this->view('admin.creditbundles.edit', compact('creditbundle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        return $this->save_credit_bundle($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        CreditBundle::destroy($id);

        Session::flash('flash_message', 'CreditBundles successfully deleted!');

        return redirect('admin/bundles');
    }


    /**
     * @param Request $request
     * @param null    $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function save_credit_bundle(Request $request, $id = null)
    {
        $this->validate($request, CreditBundle::$input_validation);

        $credit = CreditBundle::saveCreditBundle($request, $id);

        if ($credit) {
            Session::flash('flash_message', 'CreditBundles successfully added!');

            return redirect('admin/bundles');
        }

        Session::flash('flash_message', 'Had trouble saving the bundle');

        return Redirect::back()
                       ->withErrors($credit);
    }

    /**
     * @param       $view
     * @param array $data
     *
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $data = [ ])
    {
        if ($this->is_ajax) {
            return view($view, $data);
        }

        return view('layouts.non-ajax-layout', $data)
            ->with('content', $view);

    }
}
