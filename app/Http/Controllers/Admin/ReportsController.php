<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CreditsDebitCredit;
use DB;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Database\Eloquent\Collection;
use Input;
use Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\User;
use App\Job;
use App\Bid;
use App\Jobs\SendSMS;
use App\Jobs\SendEmail;
use Laracasts\Flash\Flash;
use App\Role;

class ReportsController extends Controller
{

    protected $is_ajax = false;

    public function __construct(Request $request)
    {
        $this->is_ajax = $request->ajax();

    }
    public function user_stats(Request $request ,$id){

        if (Auth::check() && Auth::user()->id != $id) {
            return ;
        }

        $credits_report = $this->user_credits_purchased($id);
        $credits_report = $this->paginate_results($request,$credits_report);

        $jobs = Job::where('user_id',$id)->paginate(10);
        $bids = Bid::where('user_id',$id)->paginate(10);
        return view('admin.activities.user-reports',compact('credits_report','jobs','bids'));

    }
    public function getNotifications(){
        $roles = Role::all();
        return view('pages.notify-users',compact('roles'));
    }
    public function postNotifications( Request $request){
        $input = $request->all();

        if (in_array("sms", $input['notification_type'])) {
            $this->dispatch(new SendSMS($input));
        }
        if (in_array("email", $input['notification_type'])) {

            $this->dispatch(new SendEmail($input));
        }

        Flash::message('notification sent successfully');

        $roles = Role::all();
        return view('pages.notify-users',compact('roles'));
    }
    public function vendor_balance(Request $request, $user_id = null)
    {
//        $report = CreditsDebitCredit::credit_summary()->paginate(100);

        $report = CreditsDebitCredit::credit_history_table_sql($user_id);
        $report = $this->paginate_results($request, $report);
        return $this->view('admin.activities.credit-reports', ['data' => $report]);
    }

    public function user_credit_history(Request $request, $user_id = null)
    {
//        $report = CreditsDebitCredit::credit_summary()->paginate(100);
        $report = CreditsDebitCredit::credit_history_table_sql($user_id);
        $report = $this->paginate_results($request, $report);
        return $this->view('admin.activities.credit-reports', ['data' => $report]);
    }

    public function purchased_credits(Request $request)
    {
        $report = CreditsDebitCredit::purchased_credit_table_sql();
        $report = $this->paginate_results($request, $report);
        return $this->view('admin.activities.credit-reports', ['data' => $report]);
    }

    public function under_hundred(Request $request, $defaults = [])
    {
        $report = DB::Select("SELECT
                                  users.first_name,
                                  users.email,
                                  profiles.phone,
                                  (SUM(IFNULL(cdc.credit, 0)) - SUM(IFNULL(cdc.debit, 0))) AS credit_balance,
                                  cdc.user_id,
                                  ran_out
                                FROM credits_debit_credits cdc
                                  JOIN users ON cdc.user_id = users.id
                                  JOIN profiles ON users.id = profiles.user_id
                                  JOIN (SELECT
                                          bids.user_id         AS bid_user_id,
                                          max(bids.created_at) AS ran_out
                                        FROM bids
                                        GROUP BY bids.user_id) bids ON cdc.user_id = bids.bid_user_id
                                GROUP BY users.id
                                HAVING credit_balance < 100
                                ORDER BY credit_balance DESC");

        if (isset($request->csv)) {
            $data = [];
            foreach ($report as $result) {
                $data[] = (array)$result;
                #or first convert it and then change its properties using
                #an array syntax, it's up to you
            }
            Excel::create('Under hundred', function ($excel) use ($data) {
                $excel->sheet('vendors', function ($sheet) use ($data) {
                    $sheet->fromArray($data);

                });
            })->download('csv');
        }
//        $report = $this->paginate_results($request, $report);

        return $this->view('admin.activities.credit-reports', ['data' => $report]);

    }

    public function ranked_vendors(Request $request)
    {

        $input = $request->all();
        if (empty($input) || !array_key_exists('type', $input)|| $input['type'] == 'all'){
            $users = DB::table('users')
                ->groupBy('users.id')
                ->join('rankings', function ($join){
                    $join->on('users.id', '=', 'rankings.user_id');
                    })
                ->orderBy('points','DESC')
                ->get(['users.id as user_id','users.first_name as name',DB::raw('sum(rankings.ranking) as points')]);
            $report = $this->paginate_results($request, $users);
        }
        elseif($input['type'] == 'top vendors'){
           $users =  DB::table('users')
                        ->join('user_period_rank', function ($join) {
                            $join->on('users.id', '=', 'user_period_rank.user_id');
                        })
                        ->orderBy('period','DESC')
                        ->orderBy('points','DESC')
                        ->whereNull('user_period_rank.cat_id')
                        ->where('points','>=',15)
                        ->where('user_period_rank.ranking','<=',20)
                        ->get([DB::raw('users.id as user_id'),DB::raw('users.first_name as name'),DB::raw('user_period_rank.points as points'),'user_period_rank.created_at as period']);
                        $report = $this->paginate_results($request, $users);
        }
        elseif($input['type'] == 'recommended vendors'){
           $users =  DB::table('users')
                        ->join('user_period_rank', function ($join) {
                            $join->on('users.id', '=', 'user_period_rank.user_id');
                        })
                        ->join('classifications', function ($join) {
                            $join->on('user_period_rank.cat_id', '=', 'classifications.id');
                        })
                        ->orderBy('classifications.id','ASC')
                        ->orderBy('period','DESC')
                        ->orderBy('points','DESC')
                        ->whereNotNull('user_period_rank.cat_id')
                        ->where('points','>=',15)
                        ->where('user_period_rank.ranking','<=',3)
                        ->get([DB::raw('users.id as user_id'),DB::raw('users.first_name as name'),'classifications.name as category',DB::raw('user_period_rank.points as points'),'user_period_rank.created_at as period']);
                        $report = $this->paginate_results($request, $users);
        }
        return $this->view('admin.activities.ranked-vendors', ['data' => $report]);
    }

    public function free_credit_expiry(Request $request)
    {
       $report = DB::Select("select users.first_name, users.id as user_id, date_add(date(meta.login_date), INTERVAL 45 DAY) as expiry_date
                                from credits_debit_credits cdc
                                  join (select count(id) as count, user_id from credits_debit_credits where credit > 0 group by user_id having count(credit) = 1) cdc_count on cdc_count.user_id = cdc.user_id
                                  join (select min(id), meta_value as login_date, object_id from meta_table where meta_key = 'login' group by object_id) meta on meta.object_id = cdc.user_id
                                  left join (select id, meta_value, object_id from meta_table where meta_key = 'first_credits_expired' and object_type = 'App\User' group by object_id) meta_first_credits_expired on meta.object_id = cdc.user_id
                                  join(select id,first_name from users) users on users.id = cdc.user_id
                                where
                                  cdc.credit > 0
                                  and date(meta.login_date) <= date_sub(curdate(), interval 45 day)
                                  and (cdc.object_name is null or cdc.object_name != 'credits')
                                  and meta_first_credits_expired.id is null");
        $report = $this->paginate_results($request, $report);
        return $this->view('admin.activities.credit-reports', ['data' => $report]);
    }

    /**
     * @param       $view
     * @param array $data
     *
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $data = [])
    {
        if ($this->is_ajax) {
            return view($view, $data);
        }

        return view('layouts.non-ajax-layout', $data)
            ->with('content', $view);

    }

    /**
     * @param Request $request
     * @param         $results
     * @param array $defaults
     *
     * @return array|Paginator
     */
    public function paginate_results(Request $request, $results, $defaults = [])
    {
        $results_data = [];
        $paginated_data = [];

        $limit = (Input::has('limit')) ? Input::get('limit')
            : array_get($defaults, 'limit', 10);

        if ($limit != -1) {
            $offset = (Input::has('page')) ? (Input::get('page') - 1) * $limit : 0;
        }

        $page = (Input::has('page')) ? Input::get('page')
            : array_get($defaults, 'page', 1);


        foreach ($results as $result) {
            $results_data [] = $result;
        }
        $itemsForCurrentPage = ($limit != -1) ? array_slice($results_data, $offset, $limit, true) : $results_data;

        $paginated_data = ($limit != -1) ? new Paginator($itemsForCurrentPage, count($results), $limit, $page, [
            'path' => $request->url(),
            'query' => $request->query(),
        ]) : $results;

        return $paginated_data;
    }
    public function jobs_reports(Request $request){

            if ($request->input('category') == 'all') {
                $data = Job::all();
            }
            else{
                $data = Job::where('classification','=',$request->input('category'))->get();
            }


            Excel::create('Jobs created', function ($excel) use ($data) {
                $excel->sheet('jobs', function ($sheet) use ($data) {
                    $sheet->fromArray($data);

                });
            })->download('csv');

    }
    public function job_credits_reports(){

         $credits_within_period = DB::Select("select DATE_FORMAT(created_at,'%b %Y') as period, sum(credit) as credit, sum(debit) debit,  (sum(credit) - sum(debit)) as credit_balance
            from credits_debit_credits
            group by period
            order by created_at");
         $jobs_within_period = DB::Select("select DATE_FORMAT(created_at,'%b %Y') as period, count(*) jobs
            from jobs
            group by period
            order by created_at");

        //join is too much work
        foreach ($jobs_within_period as $key => $jobs) {
            foreach ($credits_within_period as $credits) {

                if ($credits->period == $jobs->period) {

                    $jobs->credits = $credits->credit;
                    $jobs->debit = $credits->debit;
                    $jobs->credit_balance = $credits->credit_balance;
                }
            }
        }
         
        $data = [];
        foreach ($jobs_within_period as $result) {
            $data[] = (array)$result;
            #or first convert it and then change its properties using
            #an array syntax, it's up to you
        }

        Excel::create('Jobs created vs Credits', function ($excel) use ($data) {
                $excel->sheet('jobsVcredits', function ($sheet) use ($data) {
                    $sheet->fromArray($data);

                });
            })->download('csv');

    }
    public function user_credits_purchased($id)
    {
        $result_ = DB::table('credits')
            ->join('users', 'users.id', '=', 'credits.user_id')
            ->join('credit_bundles', 'credit_bundles.id', '=', 'credits.credit_bundle_id')
            ->select(
                'user_id',
                DB::raw("concat(users.first_name,' ',users.last_name) name"),
                'credit_bundles.credits',
                'quantity',
                'credits.created_at'
            )
            ->where('credits.user_id',$id)
            ->get();

        return $result_;
    }
}