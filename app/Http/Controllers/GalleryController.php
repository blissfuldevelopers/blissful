<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Tag;
use App\Link;
use App\Video;
use Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Cohensive\Embed\Facades\Embed;
use App\Activity;
use Mixpanel;
use App\Review;
use App\Ranking;
class GalleryController extends Controller
{
    // public function __construct(){
    // 	$this->middleware('auth');
    // }

    public function viewGalleryList()
    {   
        if (Auth::check()) {
            $galleries = Gallery::where('created_by', Auth::user()->id)->get();

            return view('gallery.gallery',compact('galleries'));
        }
        
        return redirect('login');
    }

    public function AdminViewGallery($id)
    {
        $galleries = Gallery::where('created_by', $id)->get();

        return view('gallery.gallery')
            ->with('galleries', $galleries)->with('id', $id);
    }

    public function saveGallery(Request $request)
    {
        //validate through rules
        $validator = Validator::make($request->all(), [
            'gallery_name' => 'required|min:3',
            'gallery_description' => 'required|min:3',

        ]);

        //action if fail
        if ($validator->fails()) {
            return redirect('gallery/list')
                ->withErrors($validator)
                ->withInput();

        }

        $file = $request->file('file');
        $filename = uniqid() . $file->getClientOriginalName();
        $thumb = Image::make($file->getRealPath())->resize(240, 160);

        $image_thumb = $thumb->stream();
        $s3 = \Storage::disk('s3');
        $filePath = 'gallery/images/gallerythumbs/' . $filename;
        $s3->getDriver()->put($filePath, $image_thumb->__toString(),[ 
                
                                                                            'visibility' => 'public',
                                                                            'CacheControl' => 'max-age=2592000',
                                                                            'Expires' => gmdate("D, d M Y H:i:s T",strtotime("+3 years"))

                                                                        ]);

        $gallery = new Gallery;
        //save new gallery
        $gallery->name = $request->input('gallery_name');
        $gallery->description = $request->input('gallery_description');
        $gallery->gallery_image = ('gallery/images/gallerythumbs/' . $filename);
        if (Auth::user()->hasRole('administrator')) {
            $gallery->created_by = $request->user_id;
        } else {
            $gallery->created_by = Auth::user()->id;

            $ranking = new Ranking;
            $ranking->storeRankingForVendor(Auth::user()->id, 'created gallery', 2);
        }
        $gallery->published = 1;
        $gallery->save();

        Activity::create([
            'subject_id' => $gallery->id,
            'subject_type' => 'App\Gallery',
            'name' => 'created_gallery',
            'user_id' => Auth::user()->id,
        ]);

        return redirect()->back();

    }

    public function viewGalleryPics($id)
    {

        $gallery = Gallery::findorfail($id);
        $created_by = $gallery->created_by;
        
        if (Auth::check() && Auth::user()->id == $created_by || Auth::user()->hasRole('administrator')) {

            return view('gallery.gallery-view',compact('gallery'));

        } else {
            abort(403);
        }

    }

    public function doImageUpload(Request $request)
    {
        //get the file from post
        $file = $request->file('file');

        //set filename

        $filename = uniqid() . $file->getClientOriginalName();

        //move file to save location
        $file->move('gallery/images', $filename);
        //save thumbnail to path
        $image = Image::make('gallery/images/' . $filename)->resize(640, 350);

        $image_thumb = $image->stream();
        $s3 = \Storage::disk('s3');
        $filePath = 'gallery/images/compressed/' . $filename;
        $s3->put($filePath, $image_thumb->__toString(), 'public');

        $thumb = Image::make('gallery/images/' . $filename)->resize(240, 160);

        $image_thumb = $thumb->stream();
        $s3 = \Storage::disk('s3');
        $filePath = 'gallery/images/thumbs/' . $filename;
        $s3->put($filePath, $image_thumb->__toString(), 'public');

        //select gallery
        $gallery = Gallery::find($request->input('gallery_id'));
        //save image details to db
        $image = $gallery->images()->create([
            'gallery_id' => $request->input('gallery_id'),
            'file_name' => $filename,
            'file_size' => $file->getClientSize(),
            'file_mime' => $file->getClientMimeType(),
            'file_path' => 'gallery/images/compressed/' . $filename,
            'created_by' => $gallery->created_by,
            'caption' => 'No Caption Added',
        ]);
        unlink(public_path('gallery/images/' . $filename));
        return redirect()->back();


    }

    public function videoUploads(Request $request)
    {


        $file = $request->file('file');

        $filename = uniqid() . $file->getClientOriginalName();
        $file->move('gallery/video', $filename);

        //select gallery
        $gallery = Gallery::find(($request->input('gallery_id')));

        //save image details to db
        $video = $gallery->videos()->create([
            'gallery_id' => $request->input('gallery_id'),
            'file_name' => $filename,
            'file_size' => $file->getClientSize(),
            'file_mime' => $file->getClientMimeType(),
            'file_path' => 'gallery/video/' . $filename,
            'created_by' => Auth::user()->id,
            'file_title' => $request->input('vid_title'),
            'caption' => $request->input('vid_description'),
        ]);

        return redirect()->back();
    }

    public function LinkUploads(Request $request)
    {
        $link = $request->input('vid_link');
        $gallery = Gallery::find($request->input('gallery_id'));
        $link = $gallery->links()->create([
            'gallery_id' => $request->input('gallery_id'),
            'links' => $link,
            'created_by' => Auth::user()->id,
            'caption' => 'No Caption Added',
        ]);
        return redirect()->back();


    }

    public function deleteGallery($id)
    {
        //load gallery
        $currentGallery = Gallery::findorfail($id);
        //authentication
        if ($currentGallery->created_by === !Auth::user()->id) {
            abort('404', 'You are not allowed to view this gallery');
        }
        //get images
        $images = $currentGallery->images();
        //delete images
        // foreach ($currentGallery->images() as $image) {
        //     unlink(public_path($image->file_path));
        //     unlink(public_path('gallery/images/thumbs' . $image->file_name));
        // }
        //delete db records
        $currentGallery->images()->delete();
        $currentGallery->videos()->delete();
        $currentGallery->links()->delete();
        //delete gallery
        $currentGallery->delete();

        return redirect()->back();
    }

    public function addIdea($id)
    {
        $user = (Auth::user()->id);

        dd($id);

    }

    public function editGallery($id)
    {
        $galleries = Gallery::where('created_by', Auth::user()->id)->get();
        $edits = Gallery::where('id', $id)->get();


        return view('gallery.gallery-edit')->with('galleries', $galleries)->with('edits', $edits);
    }

    public function updateGallery(Request $request, $id)
    {

        $input = Input::all();
        $gallery = Gallery::find($id);


        if (Input::hasFile('file')) {
            $file = $input['file'];
            $filename = uniqid() . $file->getClientOriginalName();
            $thumb = Image::make($file->getRealPath())->resize(240, 160);

            $image_thumb = $thumb->stream();
            $s3 = \Storage::disk('s3');
            $filePath = 'gallery/images/gallerythumbs/' . $filename;
            $s3->put($filePath, $image_thumb->__toString(), 'public');

            if ($gallery->name == 'Portfolio') {
                $gallery->description = ucfirst($input['gallery_description']);
                $gallery->gallery_image = ('gallery/images/gallerythumbs/' . $filename);
                $gallery->save();
                return redirect('gallery/edit/' . $id)->with('message', 'Changes Saved');
            }
            $gallery->name = ucfirst($input['gallery_name']);
            $gallery->description = ucfirst($input['gallery_description']);
            $gallery->gallery_image = ('gallery/images/gallerythumbs/' . $filename);
            $gallery->save();
            return redirect('gallery/edit/' . $id)->with('message', 'Changes Saved');
        }
        if ($gallery->name == 'Portfolio') {
            $gallery->description = ucfirst($input['gallery_description']);
            $gallery->save();
            return redirect('gallery/edit/' . $id)->with('message', 'Changes Saved');
        } else {
            $gallery->name = ucfirst($input['gallery_name']);
            $gallery->description = ucfirst($input['gallery_description']);
            $gallery->save();
        }

        return redirect('gallery/edit/' . $id);

    }


}
