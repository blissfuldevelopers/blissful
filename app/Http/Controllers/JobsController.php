<?php

namespace App\Http\Controllers;

use App\Events\JobWasCreated;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Job;
use App\Bid;
use App\Locations;
use App\Classification;
use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;
use App\Credit;
use App\Activity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use DB;
use Laracasts\Flash\Flash;
use App\Logic\User\UserRepository;
use Illuminate\Support\Facades\Redirect;
use Mixpanel;
use App\Password;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use App\EventModel;
use Illuminate\Support\Facades\Response;
use App\Models\Event;
use App\Logic\Mailers\UserMailer;
use DateTime;
use App\Jobs\SendSMS;
use App\Jobs\SendEmail;
use App\JobLocations;
use Cmgmyr\Messenger\Models\Thread;

class JobsController extends Controller
{
    private $default_jobs_route;
    private $requested_jobs_route;

//    public function __construct()
//    {
//        $this->middleware('job', ['except' => 'index']);
//    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    protected $userMailer;
    protected $userRepository;


    public function __construct(UserRepository $userRepository, UserMailer $userMailer)
    {
        $this->userRepository = $userRepository;
        $this->userMailer = $userMailer;
    }

    public function index()
    {
        $user = Auth::user();

        $page_data['default_jobs_route'] = ($this->default_jobs_route) ? $this->default_jobs_route
            : ((!$user || !$user->can_bid_for_job)
                ? 'mine'
                : 'all');


        $page_data['requested_jobs_route'] = ($this->requested_jobs_route == $page_data['default_jobs_route']) ? '' : $this->requested_jobs_route;

        return view('jobs.home', $page_data);
    }

    /**
     * Reroute non ajax requests to the ajax home page
     *
     * @param Request $request
     *
     * @return Response
     */
    public function routeNonAjax(Request $request)
    {
        $url = $request->url();
        $getQueryString = ($request->getQueryString()) ? '?' . $request->getQueryString() : '';

        $this->requested_jobs_route = $url . '' . $getQueryString;

        return $this->index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function jobIntro()
    {
        return view('jobs.partials.job-intro');
    }

    public function create()
    {
        //TODO - add the Event if there's a event=$id

        $classifications = Classification::whereNull('parent_id')->orderBy('priority', 'ASC')->get();

        $locations = Locations::all();
        $selected = 0;

        if(Input::has('option')) {
            $selected = Classification::findOrFail(Input::get('option'));
        }

        return view('jobs.fullscreen-create', compact('classifications', 'locations', 'selected'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $job_class = ' ';
        $token = 'final';
        //check if logged in
        if(Auth::check()) {
            $user = Auth::user();
        } else {

            $user = User::where('email', $input['email'])->first();

            if (is_null($user)) {
                $user = $this->register_user($input);
            }
            

        }

        if(empty($user->profile->phone)) {
            $profile = $user->profile;
            $profile->phone = $input['phone'];
            $user->profile()->save($profile);
        }

        if(!isset($request->phone)) {
            $request->merge(array('phone' => $user->profile->phone));
            $input['phone'] = $user->profile->phone;
        }

        foreach($input['classification'] as $key => $value) {
            $sub_categories = [];
            $classification = Classification::where('slug', '=', $value)->first();

            $cat_id = $classification->id;

            foreach($input as $key => $value) {
                $exp_key = explode('-', $key);

                if(count($exp_key) > 1 && $exp_key[1] == $cat_id) {

                    $input[$exp_key[0]] = $value;
                    unset($input[$key]);
                    $request->merge(array($exp_key[0] => $value));

                    if("level" == substr($exp_key[0], 0, 5)) {
                        foreach($value as $val) {
                            array_push($sub_categories, $val);
                        }
                    }

                }

            }
            
            $this->validate($request, Job::$inputRules);
            $input['subcategory'] = $sub_categories;
            $input['user_id'] = ($user) ? $user->id : 0;
            $input['classification_id'] = $classification->id;
            $job = Job::save_job($input);
            $job->classifications()->attach($input['subcategory']);
            $job_class = $job_class." ,".$job->category->name;
            if (array_key_exists('invitee', $input)) {
                $job->invitees()->attach($input['invitee']);

                $data = [
                    'user_id'   =>  $input['invitee'],
                    'message'   =>  'You have received an invite to submit a quotation on blissful.co.ke. Act fast and secure this gig, sending a quotation for this job is absolutely free.',
                    'title'     =>  'Invite to submit a quotation'
                ];
                $this->dispatch(new SendSMS($data));
                $this->dispatch(new SendEmail($data));
            }

        }
        

        if($job) {

            //send mail to tech
            $data = [
                'email'   =>  'support@hugeafrica.com',
                'message'   =>  'A user has requested for '.$job_class.' <br> Name: '.$user->first_name.'<br> Email: '.$user->email.'<br> Phone: '.$user->profile->phone,
                'title'     =>  'New Job created'
            ];
            $this->dispatch(new SendEmail($data));
            
            if (array_key_exists('city_name', $input)) {
                $location_input = [
                    'name'      =>  $input['city_name'],
                    'address'   =>  $input['event_location'],
                    'lat'       =>  $input['lat'],
                    'lng'       =>  $input['lng'],
                    'job_id'    =>  $job->id
                ];
                $location = $job->location ? $job->location : new JobLocations;
                $location->fill($location_input);
                $location->save();
            }
            
            

            if ($user->verified == 0) {
                
                return view('auth.verify-mobile', compact('user'));
            }
            else{
                return view('jobs.job-success');
            }
        }

        else{

           Flash::warning('Had trouble creating the quotation request!');

            return Redirect::back()
            ->withErrors($job); 
        }
        

    }

    /**
     * Display the specified resource.
     *
     * @param         $id
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id, Request $request)
    {
        $selected_category = false;
        $selected_location = false;
        $job = false;
        $page_data = [];
        $user = Auth::user();
        $input = $request->all();
        $use_bootstrap_version = (isset($input['bootstrap'])) ? true : false;
        $locations = Locations::all();

        //these are classifications to allow for sorting

        if($user->hasUserRole('administrator')) {
            $classifications = Classification::all();
        } else {
            $classifications = $user->classifications;
        }

        DB::enableQueryLog();
        if(is_numeric($id)) {
            $jobs_query = Job::where('id', $id);

        } else {
            if(!$user) {
                $jobs_query = Job::orderBy('expiry_date','desc');
            } else {
                switch(strtolower(trim($id))) {
                    case 'mine':
                        $jobs_query = Job::where('user_id', $user->id)->orderBy('expiry_date','desc');
                        break;

                    case 'vendors':
                        break;

                    case 'all':
                    default:
                        $jobs_query = Job::whereNotNull('event_date');
                        break;

                    case 'index':
                    case 'home':
                        return $this->index();
                        break;

                }
            }

            // $jobs_query->where('event_date', '>=', date('Y-m-d H:i:s', strtotime('NOW')));


            $jobs_query = Job::filter_jobs_query_by_user_type($jobs_query, $user);
            if(Input::has('date')) {

                $order = $request->input('date');
                $jobs_query->orderBy('created_at', $order);
            
            } else {
                $jobs_query->orderBy('created_at','desc');
            }

            if(Input::has('category')) {
                $selected_category = Input::get('category');
                if ($selected_category != "all") {
                    $in_array['classification_id'] = $selected_category;
                }
                
                // array_push($in_array, array('classification' => $value));
                // $jobs_query->where('classification', '=', Input::get('category'));
            }
            if(isset ($in_array)) {
                $jobs_query->where($in_array);
            }

            if(Input::has('location')) {
                $selected_location = Input::get('location');
                $jobs_query->Where('event_location', 'like', '%' . $selected_location . '%');
                // array_push($in_array, array('event_location' => $value));
                // $jobs_query->where('event_location', '=', Input::get('location'));
            }


        }
        
        $jobs = $jobs_query->get();
        $page_data = [
            'can_bid'               => ($user && $user->can_bid_for_job),
            'total_jobs'            => count($jobs),
            'use_bootstrap_version' => $use_bootstrap_version,
        ];

        if(count($jobs) == 1 && is_numeric($id)) {
            $page_data['job'] = (isset($jobs[0])) ? $jobs[0] : $jobs;


            //log viewing this Job
            Activity::create([
                                 'subject_id'   => $page_data['job']->id,
                                 'subject_type' => 'App\Job',
                                 'name'         => 'viewed_job',
                                 'user_id'      => (Auth::check()) ? Auth::user()->id : 0,
                             ]);

            if($jobs[0]->user_can_view_bids) {

                $raw_bids = $jobs[0]->bids();
                $bids = $raw_bids->paginate(10);
                $bid_stats = [];

                $bid_group = $raw_bids->select('status', DB::raw('count(*) as total'))
                                      ->groupBy('status')
                                      ->get();

                foreach($bid_group as $value) {
                    if($value->status == 0) {
                        $bid_stats['unread'] = $value->total;
                    }
                    if($value->status == 1) {
                        $bid_stats['shortlisted'] = $value->total;
                    }
                    if($value->status == 3) {
                        $bid_stats['declined'] = $value->total;
                    }

                }


                if($user && $jobs[0]->user_id == $user->id) {
                    return view('bids.status-view', $page_data)->with('bids', $bids);
                }
                $accepted_bid = $raw_bids->where('status', 2)->get();

                return view('bids.create', $page_data, compact('bids', 'accepted_bid', 'bid_stats'));
            }
            if($use_bootstrap_version) {
                return view('bids.create-dash', $page_data);

            }
            $raw_bids = $jobs[0]->bids();
            $bids = $raw_bids->paginate(10);
            $bid_stats = [];

            $bid_group = $raw_bids->select('status', DB::raw('count(*) as total'))
                                  ->groupBy('status')
                                  ->get();

            foreach($bid_group as $value) {
                if($value->status == 0) {
                    $bid_stats['unread'] = $value->total;
                }
                if($value->status == 1) {
                    $bid_stats['shortlisted'] = $value->total;
                }
                if($value->status == 3) {
                    $bid_stats['declined'] = $value->total;
                }

            }
            $accepted_bid = $raw_bids->where('status', 2)->get();

            //set client suggestions in a string
            $user_sub_classifs = [];

            foreach ($page_data['job']->classifications as $k => $classif) {

                if ($classif->parent_id != $classif->root_parent_id) {
                    if (array_key_exists($classif->parent_id, $user_sub_classifs)) {
                         $old = $user_sub_classifs[$classif->parent_id];
                         $new = $old.', '.$classif->name;
                         $user_sub_classifs[$classif->parent_id] = $new;
                    }
                    else{
                        $user_sub_classifs[$classif->parent_id] = $classif->name; 
                       
                    }
                    
                }
                foreach ($user_sub_classifs as $key => $value) {
                    foreach ($page_data['job']->classifications as $ke => $val) {
                        if ($val->id == $key ) {
                            $val->suggested = $value;
                        }
                    }
                }
                
            }
            

            
            return view('bids.create', $page_data, compact('bids', 'accepted_bid', 'bid_stats'));
        }
        // if ($user->hasUserRole('user')) {
        //     $events = [];

        //     foreach ($jobs as $job) {
        //         $events[] = \Calendar::event(
        //         $job->title, //event title
        //         true, //full day event?
        //         new \DateTime($job->event_date), //start time (you can also use Carbon instead of DateTime)
        //         new \DateTime($job->event_date), //end time (you can also use Carbon instead of DateTime)
        //         $job->id, //optionally, you can specify an event ID
        //         Auth::user()->id
        //         );
        //     }


        //     // $eloquentEvent = EventModel::first(); //EventModel implements MaddHatter\LaravelFullcalendar\Event
        //     $calendar = \Calendar::addEvents($events) //add an array with addEvents
        //     ->setOptions([ //set fullcalendar options
        //             'firstDay' => 1
        //         ]);

        //     return view('jobs.calendar', compact('calendar','jobs'));


        // }

        //handle the multiple jobs using pagination
        $limit = (Input::has('limit')) ? Input::get('limit') : 10;
        if($limit != -1) {
            $offset = (Input::has('page')) ? (Input::get('page') - 1) * $limit : 0;
        }

        $page = (Input::has('page')) ? Input::get('page') : 1;

        $jobs_data = [];
        foreach($jobs as $job) {
            $jobs_data [] = $job;
        }
        $itemsForCurrentPage = ($limit != -1) ? array_slice($jobs_data, $offset, $limit, true) : $jobs_data;
        $page_data['jobs'] = ($limit != -1) ? new Paginator($itemsForCurrentPage, count($jobs), $limit, $page, [
            'path'  => $request->url(),
            'query' => $request->query(),
        ]) : $jobs;

        $page_data['offset'] = $offset;

        //log viewing the Jobs Board Page
        Activity::create([
                             'subject_id'   => 6,
                             'subject_type' => 'App\Page',
                             'name'         => 'read',
                             'user_id'      => (Auth::check()) ? Auth::user()->id : 0,
                         ]);

        return view('jobs.show-dash', $page_data)
            ->with('selected_category', $selected_category)
            ->with('selected_location', $selected_location)
            ->with('categories', $classifications)
            ->with('locations', $locations);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $job_classifications = [];
        $job = Job::findOrFail($id);
        $locations = Locations::all();
        $classifications = Classification::all();

        $classification_data = Classification::where('name', '=', $job->category->name)->first();
        foreach($job->classifications as $key => $value) {
            array_push($job_classifications, $value->id);
        }

        return view('jobs.edit')->with('job', $job)->with('locations', $locations)->with('classifications', $classifications)->with('classification_data', $classification_data)->with('job_classifications', $job_classifications);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $no_validate = false;
        if($request->has('in_review') || $request->has('completed')) {
            $no_validate = true;

        }
        if(!$no_validate) {
            $this->validate($request, ['title' => 'required', 'description' => 'required', 'classification_id' => 'required', 'budget' => 'required', 'event_date' => 'required', 'event_location' => 'required',]);
        }


        $job = Job::findOrFail($id);
        $job->update($request->all());
        $input = $request->all();

        if($request->has('sub_classifications')) {
            $job->classifications()->sync($request->input('sub_classifications'));
        }
        if (array_key_exists('city_name', $input)) {
            $location_input = [
                'name'      =>  $input['city_name'],
                'address'   =>  $input['event_location'],
                'lat'       =>  $input['latitude'],
                'lng'       =>  $input['longitude'],
                'job_id'    =>  $job->id
            ];
            $location = $job->location ? $job->location : new JobLocations;
            $location->fill($location_input);
            $location->save();
        }
        if($request->has('in_review') && $request->input('in_review') == 1) {

            $user = $job->user;

            $data = [
                'first_name'   => $user->first_name,
                'subject'      => 'Request for more information on quotation request',
                'email'        => $user->email,
                'mail_content' => $request->input('mail_content'),
            ];

            $this->userMailer->inReview($data);

            return redirect()->route('jobs.edit', $job->id)->with('status', 'success')
            ->with('message', 'Request successfully sent!');
        }


        return redirect()->route('jobs.edit', $job->id)->with('status', 'success')
            ->with('message', 'Request successfully edited!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Job::destroy($id);

        return redirect('jobs/all')->with('status', 'success')
            ->with('message', 'Request successfully deleted!');
    }
    public function restore($id)
    {
        Job::withTrashed()->find($id)->restore();

        return Redirect('jobs/all')->with('status', 'success')
            ->with('message', 'Request successfully restored!');
    }


    public function jobsBoard()
    {
//        $jobs = Job::with(['user'])->latest()->paginate(10);
//
//        return view('jobs.jobs-board', compact('jobs'));
    }


    public function viewJob($id)
    {
//        $job = Job::findOrFail($id);
//
//        return view('jobs.view-job', compact('job'));
    }

    public function bidJob(Request $request)
    {
//        $this->validate($request, ['bid_details']);
//
//        $bid = new Bid;
//        $bid->bid_details = $request['bid_details'];
//        $bid->job_id = $request['job_id'];
//        $bid->user_id = Auth::user()->id;
//        $bid->save();
//
//
//        Session::flash('flash_message', 'Bid successful!');
//
//        return redirect('vendr/jobs');
    }

    public function jobSuccess($token)
    {
        if(Auth::check() || $token == 'final') {
            return redirect('dashboard');
        }

        $password = Password::where('token', $token)->first();
        $user = User::where('email', $password->email)->first();

        return view('jobs.new-user')->with('user', $user)->with('token', $token);
    }

    public function jobPosted()
    {

        return view('jobs.job-success');

    }

    public function childClass(Request $request)
    {

        $id = $request['option'];


        $classification_data = Classification::where('id',$id)->first();

        return view('jobs.child-classes')->with('classification_data', $classification_data);


    }

    public function viewEvents()
    {

        $user = Auth::user();
        $jobs = [];
        if($user->hasUserRole('user')) {
            $events = Event::where(function ($q) use ($user) {
                $q->where('user_id', '=', $user->id);
            })->paginate(6);
            $jobs = Job::where(function ($q) use ($user) {
                $q->where('event_id', '=', 0)
                  ->where('user_id', '=', $user->id);
            })->paginate(4);

            return view('jobs.calendar', compact('events'), compact('jobs'));
        } else {
            return redirect('jobs/all');
        }
    }

    public function viewEventJobs($event_id, Request $request)
    {
        $jobs = Job::where(function ($q) use ($event_id) {
            $q->where('event_id', '=', $event_id);
        })->get();

        $limit = (Input::has('limit')) ? Input::get('limit') : 10;
        if($limit != -1) {
            $offset = (Input::has('page')) ? (Input::get('page') - 1) * $limit : 0;
        }

        $page = (Input::has('page')) ? Input::get('page') : 1;

        $jobs_data = [];
        foreach($jobs as $job) {
            $jobs_data [] = $job;
        }
        $itemsForCurrentPage = ($limit != -1) ? array_slice($jobs_data, $offset, $limit, true) : $jobs_data;
        $page_data['jobs'] = ($limit != -1) ? new Paginator($itemsForCurrentPage, count($jobs), $limit, $page, [
            'path'  => $request->url(),
            'query' => $request->query(),
        ]) : $jobs;

        $page_data['offset'] = $offset;


        return view('jobs.show-dash', $page_data);

    }

    public function returnBidStatus($job_id, $status)
    {
        $job = Job::where('id', $job_id)->first();
        $bids = Bid::where(function ($q) use ($job_id, $status) {
            $q->where('job_id', '=', $job_id)
              ->where('status', '=', $status);
        })->paginate(10);
        switch($status) {
            case 0:
                $state = 'applications';
                break;
            case 1:
                $state = 'shortlisted';
                break;
            case 2:
                $state = 'accepted';
                break;
            case 3:
                $state = 'declined';
                break;
            case 4:
                $state = 'in review';
                break;
        }

        return view('bids.status-view')->with('bids', $bids)->with('job', $job)->with('state', $state);
    }

    public function user_number_blade()
    {
        $user = Auth::user();

        return view('jobs.new-user-number', compact('user'));

    }
    public function register_user($input){


        $random = substr(md5(rand()), 0, 8);

        // if the user has no last name, try extract from first name.
        if (!array_key_exists('last_name', $input)) {

           $exp_key = explode(' ', $input['first_name']);

           if (array_key_exists(0, $exp_key)) {
               $input['first_name'] = $exp_key[0];
           }
           if (array_key_exists(1, $exp_key)) {
               $input['last_name'] = $exp_key[1];
           }
           else{
                $input['last_name'] = " ";
           }
        }
        if (!array_key_exists('password',$input)) {
            $input['password'] = $random;
        }
        $data = [
            'first_name' => $input['first_name'],
            'last_name' => $input['last_name'],
            'email' => $input['email'],
            'verified' => 0,
            'password' => $input['password'],
            'messenger' => 0,
            'phone' => $input['phone'],

        ];

        $user = $this->userRepository->register($data);


        //save phone number
        $profile = $user->profile;
        $profile->phone = $input['phone'];
        $user->profile()->save($profile);
        
        return $user;
    }
    public function extend_expiry($id){

        $job = Job::findOrFail($id);

        // calculating new expiry date
        $now = date('Y-m-d h:i:s');
        $date = DateTime::createFromFormat('Y-m-d h:i:s', $now);

        //set expiry date to 2 weeks from now
        $date->modify('+2 weeks');

        $job->expiry_date = date_format($date, 'Y-m-d h:i:s');
        $job->save();
        $job_classifications = [];

        //stuff to send it back with
        $locations = Locations::all();
        $classifications = Classification::all();

        $classification_data = Classification::where('name', '=', $job->category->name)->first();
        foreach($job->classifications as $key => $value) {
            array_push($job_classifications, $value->id);
        }

        flash::message('Job Updated Successfully');
        return view('jobs.edit')->with('job', $job)->with('locations', $locations)->with('classifications', $classifications)->with('classification_data', $classification_data)->with('job_classifications', $job_classifications);
        
    }
    public function fetch_requests(){

        $user = Auth::user();

        if ($user->hasUserRole('vendor')) {

            $jobs_query = Job::whereNotNull('event_date')->orderBy('created_at','DESC');
            $jobs_query = Job::filter_jobs_query_by_user_type($jobs_query, $user);
            $jobs = $jobs_query->get()->take(5);
        }
        elseif ($user->hasUserRole('user')) {


            $bids = Bid::whereHas('job', function ($query) use ($user) {
                                $query->where('user_id',$user->id);
                            })->get()->take(5);
            return view('jobs.partials.job-partials',compact('bids'));
        }
        else{
            $jobs = Job::all();
        }

        return view('jobs.partials.job-partials',compact('jobs'));
    }
}
