<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Auth\InvalidTokenException;
use App\Http\Controllers\Controller;
use App\Logic\User\UserRepository;
use Illuminate\Contracts\Auth\Guard;
use App\User;
use App\Social;
Use App\Classification;
use App\Role;
use App\Profile;
use Input, Validator, Auth;
use Laravel\Socialite\Facades\Socialite;
use Laracasts\Flash\Flash;
use App\Traits\CaptchaTrait;
use App\Activity;
use Mixpanel;
use App\Meta;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    use CaptchaTrait;

    protected $auth;

    protected $userRepository;

    protected $is_ajax = false;

    public function __construct(Guard $auth, UserRepository $userRepository, Request $request)
    {
        $this->auth = $auth;
        $this->userRepository = $userRepository;
        $this->is_ajax = $request->ajax();
    }

    public function getUserType()
    {
        return $this->view('auth.user-type');
    }

    public function getLogin()
    {
        return view('auth.login');
    }

    public function postLogin()
    {
        $email = Input::get('email');
        $password = Input::get('password');
        $remember = Input::get('remember');

        // if (strpos($email, 'beverly') !== false && strpos($email, 'ndege') !== false ) {
        //     return $this->view('pages.troll'); 
        // }

        if ($this->auth->attempt(

            [
                'email' => $email,
                'verified' => 1,
                'password' => $password
            ], $remember == 1 ? true : false)
        ) {
            Activity::create([
                'subject_id' => $this->auth->user()->id,
                'subject_type' => 'App\User',
                'name' => 'login',
                'user_id' => $this->auth->user()->id,
            ]);

            if ($this->auth->user()->hasUserRole('vendor')) {
                //award points
                Meta::create([
                                'object_type' => 'App\User',
                                'object_id' => $this->auth->user()->id,
                                'meta_key' => 'login',
                                'meta_value' => Carbon::now(),
                            ]);

                foreach ($this->auth->user()->galleries as $gallery) {
                    if ($gallery->name == 'Portfolio') {
                        if ($gallery->images()->count() == 0) {
                            return redirect('portfolio/' . $this->auth->user()->id);
                        }
                    }
                }


            }
            return redirect('dashboard');
        }
        $user = User::where('email',$email)->first();
        if ($user && $user->verified == 0) {
            return redirect()->back()
                ->with('message', 'Please check your email to verify your account')
                ->with('status', 'alert')
                ->withInput();
        }
        else {
            return redirect()->back()
                ->with('message', 'Please check your credentials and try again')
                ->with('status', 'alert')
                ->withInput();
        }

    }

    public function reviewLogin()
    {
        $email = Input::get('email');
        $password = Input::get('password');
        $remember = Input::get('remember');

        if ($this->auth->attempt(['email' => $email, 'verified' => 1, 'password' => $password], $remember == 1 ? true : false)) {

            return redirect()->back();
        } else {
            return redirect()->back()
                ->with('message', 'Please check your credentials or verify your account')
                ->with('status', 'alert')
                ->withInput();
        }

    }

    public function getLogout()
    {

        Auth::logout();

        return redirect()->route('auth.login')
            ->with('status', 'success')
            ->with('message', 'Logged out');

    }

    public function getRegister()
    {
        return $this->view('auth.register');
    }

    public function getVendorRegister()
    {
        $classifications = Classification::lists('name', 'id')->all();
        return $this->view('auth.vendor_register')->with('classifications', $classifications);
    }

    public function postRegister()
    {
        $input = Input::all();
        $validator = Validator::make($input, User::$rules, User::$messages);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        if ($this->captchaCheck() == false) {
            return redirect()->back()
                ->withErrors(['Wrong Captcha'])
                ->withInput();
        }

        //email
        $data = [
            'first_name' => $input['first_name'],
            'last_name' => $input['last_name'],
            'email' => $input['email'],
            'password' => $input['password'],
            'phone' => $input['phone'],
            'messenger' => 0
        ];

        $user = $this->userRepository->register($data);

        

        return $this->view('auth.verify-mobile',compact('user'));

    }

    public function postVendorRegister()
    {
        $input = Input::all();
        $validator = Validator::make($input, User::$rules, User::$messages);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        // if($this->captchaCheck() == false)
        // {
        //     return redirect()->back()
        //         ->withErrors(['Wrong Captcha'])
        //         ->withInput();
        // }

        $data = [
            'first_name' => $input['first_name'],
            'last_name' => $input['last_name'],
            'email' => $input['email'],
            'password' => $input['password'],
            'classification' => $input['classifications_list'],
        ];

        $this->userRepository->vendorRegister($data);

        return redirect()->route('auth.login')
            ->with('status', 'success')
            ->with('message', 'You are registered successfully. Please login.');

    }

    public function getSocialRedirect($provider)
    {
        $providerKey = \Config::get('services.' . $provider);
        if (empty($providerKey))
            return $this->view('pages.status')
                ->with('error', 'No such provider');

        return Socialite::driver($provider)->redirect();

    }

    public function getSocialHandle($provider)
    {

        $user = Socialite::driver($provider)->user();

        $socialUser = null;

        //Check is this email present
        $userCheck = User::where('email', '=', $user->email)->first();
        if (!empty($userCheck)) {
            //standard var name
            $socialUser = $userCheck;

            //make user verified
            if ($userCheck->verified != 1) {

                $userCheck->verified = 1;
                $userCheck->save();
            }
            //update profile image
            if ($userCheck->profile->profilePic == 'img/anony.png') {

                //profile image
                $featuredname = uniqid().$user->id.".jpg";
                $image_thumb = $this->getBigAvatar($user, $provider);
                $s3 = \Storage::disk('s3');
                $filePath = 'gallery/images/featured-images/' . $featuredname;
                $s3->getDriver()->put($filePath, file_get_contents($image_thumb),[ 
                
                                                                            'visibility' => 'public',
                                                                            'CacheControl' => 'max-age=2592000',
                                                                            'Expires' => gmdate("D, d M Y H:i:s T",strtotime("+3 years"))

                                                                        ]);
                $userCheck->profile->profilePic = $filePath;
                $userCheck->profile->save();
            }

        } else {
            $sameSocialId = Social::where('social_id', '=', $user->id)->where('provider', '=', $provider)->first();

            if (empty($sameSocialId)) {
                //There is no combination of this social id and provider, so create new one
                $newSocialUser = new User;
                $newSocialUser->email = $user->email;
                $name = explode(' ', $user->name);
                $newSocialUser->first_name = $name[0];
                $newSocialUser->last_name = $name[1];
                $newSocialUser->verified = 1;
                $newSocialUser->save();

                $socialData = new Social;
                $socialData->social_id = $user->id;
                $socialData->provider = $provider;
                $newSocialUser->social()->save($socialData);

                //create profile
                $profile = new Profile;

                //profile image
                $featuredname = uniqid().$user->id.".jpg";
                $image_thumb = $this->getBigAvatar($user, $provider);
                $s3 = \Storage::disk('s3');
                $filePath = 'gallery/images/featured-images/' . $featuredname;
                $s3->getDriver()->put($filePath, file_get_contents($image_thumb),[ 
                
                                                                            'visibility' => 'public',
                                                                            'CacheControl' => 'max-age=2592000',
                                                                            'Expires' => gmdate("D, d M Y H:i:s T",strtotime("+3 years"))

                                                                        ]);

                $profile->profilePic = $filePath;
                $newSocialUser->profile()->save($profile);

                // Add role
                $role = Role::whereName('user')->first();
                $newSocialUser->assignRole($role);

                $socialUser = $newSocialUser;
            } else {
                //Load this existing social user
                $socialUser = $sameSocialId->user;
            }

        }
        $this->auth->login($socialUser, true);

        $url = URL::previous();
        $url = parse_url($url);
        
        //return empty for jobs create logins 
        if ($url['path'] != '/jobs/create') {
           return redirect('dashboard');
        }


    }

    public function confirm($token)
    {
        if (!$token) {
            throw new InvalidTokeneException;
        }

        $user = User::whereToken($token)->first();

        if (!$user) {
            throw new InvalidTokenException;
        }

        $user->verified = 1;
        $user->token = null;
        $user->save();

        if ($user->hasuserRole('vendor')) {
            //Meta credit expiry
            Meta::create([
                'object_type' => 'App\User',
                'object_id' => $user->id,
                'meta_key' => 'first_login',
                'meta_value' => Carbon::now(),
            ]);

            //Meta credit expiry
            Meta::create([
                'object_type' => 'App\User',
                'object_id' => $user->id,
                'meta_key' => 'expiration_date',
                'meta_value' => date_add(Carbon::now(), date_interval_create_from_date_string("45 days")),
            ]);
        }

        Flash::message('You have successfully verified your account.');

        return Redirect('login');
    }
    public function getBigAvatar($user, $provider)
    {
        return ($provider == "google") ? $user->getAvatar()."0" : $user->avatar_original;
    }

    private function view($view, $data = [])
    {
        if ($this->is_ajax) {
            return view($view, $data);
        }

        return view('layouts.non-ajax-layout', $data)
            ->with('content', $view);

    }
}