<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Logic\User\UserRepository;
use App\User;
use App\Password;
use Validator, Input, Hash,Auth;
use Mixpanel;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;

class PasswordResetController extends Controller
{

    protected $is_ajax = false;
    
    public function __construct(Guard $auth,Request $request)
    {   
        $this->auth = $auth;
        $this->is_ajax = $request->ajax();

    }

    public function getPasswordReset()
    {   

        return view('auth.password-reset');
    }

    public function postPasswordReset(UserRepository $userRepository,Request $request)
    {   

        $input = Input::all();

        if ($request->has('phone')) {
            $user = $this->resetViaPhone($input);
        }
        else{
            $rules = [
            'email' => 'email|required'
            ];
                
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $user = $this->resetViaEmail($input);
        }
        

        if (empty($user)) {
            return redirect()->back()
                ->with('status', 'alert')
                ->with('message', 'User with this email does not exist. Please try again, or contact customer care for assistance.');
        }
        if (array_key_exists('slug', $input) && $user->slug != $input['slug']) {
            return redirect()->back()
                ->with('status', 'alert')
                ->with('message', 'Email input does not match that of the registered business. Please try again, or contact customer care for assistance.');
        }
        $userRepository->resetPassword($user);

        return redirect()->back()
            ->with('status', 'success')
            ->with('message', 'Reset successful. Check your email for the activation link');

    }

    public function postFirstPass(UserRepository $userRepository, $id)
    {
        $user = User::findorfail($id);
        $userRepository->joinRequest($user);
        return redirect('users')
            ->with('status', 'success')
            ->with('message', 'Request Sent!');
    }

    public function getPasswordResetForm($token)
    {
        return view('auth.password-reset-form', compact('token'));
    }

    public function postPasswordResetForm($token, Request $request)
    {   
        
        $rules = [
            'password' => 'required|min:6|max:20',
            'password_confirmation' => 'required|same:password'
        ];

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator);
        }

        $password = Password::where('token', '=', $token)->first();
        if (empty($password)) {
            return view('pages.status')
                ->with('error', 'Reset token is invalid');
        }

        $user = User::where('email', '=', $password->email)->first();
        $user->password = Hash::make(Input::get('password'));
        $user->verified = 1;
        $user->save();

        $password->delete();


        //is it from jobs?
        if ($request->has('from_jobs')) {
            //try log them in
            $password = Input::get('password');
            $remember = true;

            $this->auth->attempt(
            [
                'email' => $user->email,
                'verified' => 1,
                'password' => $password
            ], $remember == 1 ? true : false);

            return redirect('dashboard');
        }
        return redirect()->route('auth.login')
            ->with('status', 'success')
            ->with('message', 'Password reset successful. Login below');
       
    }
    public function claimAccount(Request $request){

        $slug = $request->input('slug');

        $user = User::where('slug',$slug)->first();

        $email = $this->hideEmail($user->email);

        $phone = $this->hidePhone($user->profile->phone);

        return $this->view('auth.claim-business',compact('user','email','phone'));

    }
    public function resetViaPhone($input){

        $rules = [
            'phone' => 'phone|required'
        ];
            
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $phone = $input['phone'];
        $user = User::where('phone', '=', $phone)->first();

        return $user;
    }

    public function resetViaEmail($input){
        

        $email = $input['email'];
        $user = User::where('email', '=', $email)->first();

        return $user;
    }

    public function hideEmail($email){

        $start = "";
        $end = "";
        $prop=2;
        $domain = substr(strrchr($email, "@"), 1);
        $mailname=str_replace($domain,'',$email);
        $name_l=strlen($mailname);
        $domain_l=strlen($domain);
            for($i=0;$i<=$name_l/$prop-1;$i++)
            {
            $start.='x';
            }

            for($i=0;$i<=$domain_l/$prop-1;$i++)
            {
            $end.='x';
            }

        return substr_replace($mailname, $start, 2, $name_l/$prop).substr_replace($domain, $end, 2, $domain_l/$prop);
    }

    public function hidePhone($number) {

        $maskingCharacter = 'X';
        return substr($number, 0, 4) . str_repeat($maskingCharacter, strlen($number) - 6) . substr($number, -2);

    }
     private function view($view, $data = [ ])
    {
        if ($this->is_ajax) {
            return view($view, $data);
        }
        
        return view('layouts.non-ajax-layout', $data)
            ->with('content', $view);

    }

}