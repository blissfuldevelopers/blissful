<?php

namespace App\Http\Controllers;

use Intervention\Image\Facades\Image;
use App\Profile;
Use App\User;
use App\Classification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Hash;
use App\Activity;
use Mixpanel;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $is_ajax = false;

    public function __construct(Request $request )
    {
        $this->is_ajax = $request->ajax();
    }
    public function getVendorProfile($id)
    {
        return view('panels.vendor.profile')->with('profile', Profile::find($id));
    }

    public function editVendorProfile($id)
    {
        if (Auth::user()->id == $id || Auth::user()->hasUserRole('administrator')) {
            return view('panels.vendor.edit')->with('user', User::find($id));
        } elseif (Auth::user()->hasUserRole('vendor')) {
            return redirect()->back();
        }
        return redirect()->back();

    }

    public function updateVendorProfile($id)
    {

        $input = Input::all();
        $user = User::find($id);
        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            // get the error messages from the validator
            $messages = $validator->messages();

            // redirect our user back to the form with the errors from the validator
            return redirect()->back()
                ->withErrors($validator);
        } else {
            if (Input::hasFile('file')) {
                $file = $input['file'];
                $filename = uniqid() . $file->getClientOriginalName();
                $thumb = Image::make($file->getRealPath())->resize(240, 160);

                $image_thumb = $thumb->stream();
                $s3 = \Storage::disk('s3');
                $filePath = 'gallery/images/featured-images/' . $filename;
                $s3->getDriver()->put($filePath, $image_thumb->__toString(),[ 
                
                                                                            'visibility' => 'public',
                                                                            'CacheControl' => 'max-age=2592000',
                                                                            'Expires' => gmdate("D, d M Y H:i:s T",strtotime("+3 years"))

                                                                        ]);

                $profile = $user->Profile ?: new Profile;
                $user->first_name = ucfirst($input['first_name']);
                $user->last_name = ucfirst($input['last_name']);
                if (Input::has("password")) {
                    $user->password = Hash::make($input['password']);
                }
                $user->save();


                $profile->profilePic = 'gallery/images/featured-images/' . $filename;
                $profile->phone = $input['phone'];
                $user->profile()->save($profile);
                Activity::create([
                    'subject_id' => $user->id,
                    'subject_type' => 'App\User',
                    'name' => 'updated_profile',
                    'user_id' => $user->id,
                ]);

                return $this->view('panels.vendor.edit')->with('user', User::find($id))->with('status', 'success')
                ->with('message', 'Profile successfully updated');
            } else {
                $profile = $user->Profile ?: new Profile;
                $user->first_name = ucfirst($input['first_name']);
                $user->last_name = ucfirst($input['last_name']);
                if (Input::has("password")) {
                    $user->password = Hash::make($input['password']);
                }
                $user->save();
                $profile->phone = $input['phone'];
                $user->profile()->save($profile);
                Activity::create([
                    'subject_id' => $user->id,
                    'subject_type' => 'App\User',
                    'name' => 'updated_profile',
                    'user_id' => $user->id,
                ]);

                return $this->view('panels.vendor.edit')->with('user', User::find($id))->with('status', 'success')
                ->with('message', 'Profile successfully updated');
            }

        }

    }
    private function view($view, $data = [ ])
    {
        if ($this->is_ajax) {
            return view($view, $data);
        }

        return view('layouts.non-ajax-layout', $data)
            ->with('content', $view);

    }

}