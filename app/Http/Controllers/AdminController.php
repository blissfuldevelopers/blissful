<?php namespace App\Http\Controllers;

use App\Feedback;
use App\Meta;
use App\User;
use DB;
use App\CreditBundle;
use App\Activity;
use App\Classification;
use App\Profile;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Post;
use Laracasts\Flash\Flash;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator;
use App\Traits\CaptchaTrait;
use App\Logic\Mailers\UserMailer;

class AdminController extends Controller
{
    use CaptchaTrait;

    protected $is_ajax = false;

    public function __construct(Request $request)
    {
        $this->is_ajax = $request->ajax();

    }

    public function getHome()
    {

        $currentUserId = Auth::user()->id;
        // All threads that user is participating in
        $threads = Thread::forUser($currentUserId)->get();
        $users = User::all();
        $vendors = [];
        $clients = [];
        foreach ($users as $user) {
            if ($user->hasuserRole('vendor')) {
                array_push($vendors, $user);
            }
            if ($user->hasuserRole('user')) {
                array_push($clients, $user);
            }

        }
        // $vendors=Role::with('user')->where('name', 'vendor')->get();
        // dd($vendors);
        $vendors_count = count($vendors);
        $clients_count = count($clients);

        //Vendor page view activity
//        $activity = Activity::with(['user', 'subject'])->latest()->get();

//          $activities =  DB::table('activities')
//            ->join('users', function($join){
//                $join->on('users.id', '=', 'activities.subject_id');
//            })
//            ->where('activities.name', 'read')
//            ->where('activities.subject_type', 'vendor')
//            ->groupBy('activities.subject_id')
//            ->orderBy('page_views', 'DESC')
//            ->get([
//            'users.first_name as vendor_name',
//            DB::raw('count(users.id) page_views'),
//            ]);
//        foreach ($activities as $activity) {
//            $page_views[]=$activity->page_views;
//
//        }
//        $vendor_view=(array_sum($page_views));

        ///user registration activity
        $date = date('Y-m-d H:i:s', strtotime('yesterday'));

        $vendor_new = DB::table('users')
            ->join('role_user', function ($join) {
                $join->on('users.id', '=', 'role_user.user_id');
            })
            ->join('roles', function ($join) {
                $join->on('roles.id', '=', 'role_user.role_id');
            })
            ->where('roles.name', '=', 'vendor')
            ->where('users.created_at', '>=', $date)
            ->get();
        $vendor_no = count($vendor_new);
        // $vendors=User::hasRole('vendor')->orderBy('created_at','DESC');
        $quantity = count($users);
        $classifications = Classification::lists('name', 'id');
        $posts = Post::orderBy('id', 'desc')
            ->take(1)
            ->get();

        foreach ($posts as $post) {

        }
        $creditbundles = CreditBundle::orderBy('id', 'desc')
            ->paginate(15);
        $messages = Message::with('user', 'participants', 'thread')->latest()->paginate(10);
        $activity = Activity::with(['user', 'subject'])->latest()->paginate(20);

        return view('panels.admin.home', compact('threads', 'currentUserId'))
            ->with('vendors_count', $vendors_count)
            ->with('clients_count', $clients_count)
            ->with('vendor_no', $vendor_no)
            ->with('vendor_new', $vendor_new)
            ->with('post', $post)
            ->with('classifications', $classifications)
            ->with('profile', Profile::find($currentUserId))
            ->with('quantity', $quantity)
            ->with('activities_report_url', route('activities-report'))
            ->with('creditbundles', $creditbundles)
            ->with('messages', $messages)
            ->with('activity', $activity);

    }

    public function TopVendors(Request $request)
    {
        $from = (Input::has('from')) ? date('Y-m-d H:i:s', strtotime(Input::get('from'))) : date('Y-m-d H:i:s', strtotime('yesterday'));
        $to = (Input::has('to')) ? date('Y-m-d H:i:s', strtotime(Input::get('to'))) : date('Y-m-d H:i:s', strtotime('today'));
        $limit = (Input::has('limit')) ? Input::get('limit') : 10;
        $offset = (Input::has('page')) ? (Input::get('page') - 1) * $limit : 0;
        $page = (Input::has('page')) ? Input::get('page') : 1;
        DB::enableQueryLog();
        ///Vendor page view activity
        $activity = Activity::all();

        $activities = DB::table('activities')
            ->join('users', function ($join) {
                $join->on('users.id', '=', 'activities.subject_id');
            })
            ->where('activities.name', 'read')
            ->where('activities.subject_type', 'vendor')
            ->where('activities.created_at', '>=', $from)
            ->where('activities.created_at', '<=', $to)
            ->groupBy('activities.subject_id')
            ->orderBy('page_views', 'DESC')
            // ->take($limit)
            // ->offset($offset)
            ->get([
                'users.first_name as vendor_name',
                DB::raw('count(users.id) page_views'),
                'activities.created_at as date',
            ]);

        // echo '<pre>';
        // print_r(DB::getQueryLog());
        // echo '</pre>';

        $page_data = [];
        // foreach ($activities as $activity) {
        //     $page_data []= $activity;
        // }
        //dd($activity);
        // $page_data = Datatables::of($activities)->make(true);
        $page_data = new Paginator($page_data, count($page_data), $limit, $page, [
            'path' => $request->url(),
            'query' => $request->query(),
        ]);

        return View('panels.admin.partials.admin-reports-tables')->with('report_data', $page_data);

    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function allActivities(Request $request)
    {
        $page_data = [];
        $activities_data = [];
        $limit = (Input::has('limit')) ? Input::get('limit') : 10;
        if ($limit != -1) {
            $offset = (Input::has('page')) ? (Input::get('page') - 1) * $limit : 0;
        }

        $page = (Input::has('page')) ? Input::get('page') : 1;
        $data_format = (Input::has('format')) ? Input::get('format') : 'table';
        $report_name = 'Activities';

        $input = Input::all();
        $activities_ = Activity::allActivitiesQuery($input);


        $activities = $activities_->get();

        foreach ($activities as $activity) {
            $activities_data [] = $activity;
        }

        $itemsForCurrentPage = ($limit != -1) ? array_slice($activities_data, $offset, $limit, true) : $activities_data;


        $sub_title_report_name = 'Showing ' . count($itemsForCurrentPage) . ' ' . $report_name . ' items';

        $page_data = [
            'data' => ($limit != -1) ? new Paginator($itemsForCurrentPage, count($activities), $limit, $page, [
                'path' => $request->url(),
                'query' => $request->query(),
            ]) : $activities_data,
            'count' => count($limit),
            'total' => count($activities_data),
            'report_name' => $report_name,
            'sub_title_report_name' => $sub_title_report_name,
            'input' => $input,
        ];

        switch ($data_format) {
            case 'json':
                $page_data['report_data'] = $itemsForCurrentPage;

                return response()->json($page_data);
                break;

            case 'table':
            case 'html':
            default:
                return View('panels.admin.partials.admin-reports-tables', $page_data);
                break;
        }
    }


    public function viewCategoryList()
    {

        $categories = Category::all();

        return View('pages.vendors')->with('categories', $categories);
    }

    public function analytics($id)
    {


        $credit_transactions = DB::Select("select concat(users.first_name,' ',users.last_name) as vendor_name, shop_product.name as credit_bundle, credits.quantity as credits_purchased, credits.created_at as purchase_date,credit_bundles.credits as credit_amount
            from users
            join credits on users.id = credits.user_id
            join credit_bundles on credits.credit_bundle_id = credit_bundles.id
            join shop_product on credit_bundles.product_id = shop_product.id");

        $credits_purchased = 0;
        foreach ($credit_transactions as $object) {
            $credits_purchased += isset($object->credit_amount) ? $object->credit_amount : 0;
        }


        $users_bought_credits = DB::Select("select users.first_name as vendor_name, sum(credits.quantity) as credits_purchased
            from users join credits on users.id = credits.user_id
            group by users.id");
        $vendors_purchased = count($users_bought_credits);

        $users_bought_more = DB::Select("select concat(users.first_name,' ',users.last_name) as vendor_name, credits.quantity as credits_purchased
            from users
            join credits on users.id = credits.user_id
            join credit_bundles on credits.credit_bundle_id = credit_bundles.id
            group by users.id
            having count(credits.id) > 1");
        $vendors_bought_more = count($users_bought_more);

        $users_bought_once = DB::Select("select concat(users.first_name,' ',users.last_name) as vendor_name, credits.quantity as credits_purchased
            from users
            join credits on users.id = credits.user_id
            join credit_bundles on credits.credit_bundle_id = credit_bundles.id
            group by users.id
            having count(credits.id) = 1");
        $vendors_bought_once = count($users_bought_once);

        $users_under_hundred = DB::Select("select concat(users.first_name,' ',users.last_name) as vendor_name, (sum(credits_debit_credits.credit) - sum(credits_debit_credits.debit)) as credit_balance
            from users
            join credits_debit_credits on credits_debit_credits.user_id = users.id
            group by users.id
            having credit_balance < 100");
        $vendors_under_hundred = count($users_under_hundred);


        $total_credits_on_network = DB::Select("select sum(credit) credit, sum(debit) debit, (sum(credit) - sum(debit)) as credit_balance from credits_debit_credits;
            ");
        foreach ($total_credits_on_network as $key => $value) {
            $credits_on_network = $value->credit_balance;
            $credits_spent = $value->debit;
        }
        $x = round($credits_on_network);
        $x_number_format = number_format($x);
        $x_array = explode(',', $x_number_format);
        $x_parts = array('K', 'M', 'B', 'T');
        $x_count_parts = count($x_array) - 1;
        $credits_on_network = $x;
        $credits_on_network = $x_array[0] . ((int)$x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
        $credits_on_network .= $x_parts[$x_count_parts - 1];

        $credits_within_period = DB::Select("select DATE_FORMAT(created_at,'%b %Y') as period, sum(credit) as credit, sum(debit) debit,  (sum(credit) - sum(debit)) as credit_balance
            from credits_debit_credits
            group by period
            order by created_at");


        $sent_messages = DB::Select("select DATE_FORMAT(created_at,'%U %Y') period, count(*) total
            from messages
            group by period
            order by created_at");

        $message_number = 0;
        foreach ($sent_messages as $object) {
            $message_number += isset($object->total) ? $object->total : 0;
        }
        $sent_leads = DB::Select("select DATE_FORMAT(created_at,'%U %Y') period, count(*) total
            from threads
            group by period
            order by created_at");

        $leads_number = 0;
        foreach ($sent_leads as $object) {
            $leads_number += isset($object->total) ? $object->total : 0;
        }


        $jobs_created = DB::Select("select DATE_FORMAT(created_at,'%U %Y') period, count(id) total
            from jobs
            group by period
            order by created_at");
        $jobs_number = 0;
        foreach ($jobs_created as $object) {
            $jobs_number += isset($object->total) ? $object->total : 0;
        }


        $bids_made = DB::Select("select DATE_FORMAT(created_at,'%U %Y') period, count(id) total
            from bids
            group by period
            order by created_at");

        $grouped_bids_made = DB::Select("select DATE_FORMAT(created_at,'%U %Y') period,status, count(id) total
            from bids
            group by status, period
            order by created_at");

        $bids_number = 0;
        foreach ($bids_made as $object) {
            $bids_number += isset($object->total) ? $object->total : 0;
        }
        $vendors = DB::table('users')
            ->join('role_user', function ($join) {
                $join->on('users.id', '=', 'role_user.user_id');
            })
            ->join('roles', function ($join) {
                $join->on('roles.id', '=', 'role_user.role_id');
            })
            ->where('roles.name', '=', 'vendor')
            ->get();

        $new_vendors = DB::table('users')
            ->join('role_user', function ($join) {
                $join->on('users.id', '=', 'role_user.user_id');
            })
            ->join('roles', function ($join) {
                $join->on('roles.id', '=', 'role_user.role_id');
            })
            ->where('roles.name', '=', 'vendor')
            ->select('users.created_at', DB::raw("DATE_FORMAT(users.created_at,' %U %Y ') as period,count(*) as total"))
            ->groupBy('period')
            ->orderBy('users.created_at', 'ASC')
            ->get();

        $vendor_number = count($vendors);

        $users = DB::table('users')
            ->join('role_user', function ($join) {
                $join->on('users.id', '=', 'role_user.user_id');
            })
            ->join('roles', function ($join) {
                $join->on('roles.id', '=', 'role_user.role_id');
            })
            ->where('roles.name', '=', 'user')
            ->get();
        $new_users = DB::table('users')
            ->join('role_user', function ($join) {
                $join->on('users.id', '=', 'role_user.user_id');
            })
            ->join('roles', function ($join) {
                $join->on('roles.id', '=', 'role_user.role_id');
            })
            ->where('roles.name', '=', 'user')
            ->select('users.created_at', DB::raw("DATE_FORMAT(users.created_at,' %U %Y ') as period,count(*) as total"))
            ->groupBy('period')
            ->orderBy('users.created_at', 'ASC')
            ->get();
        $user_number = count($users);

        $vendors_not_logged = DB::table('users')
            ->join('role_user', function ($join) {
                $join->on('users.id', '=', 'role_user.user_id');
            })
            ->join('roles', function ($join) {
                $join->on('roles.id', '=', 'role_user.role_id');
            })
            ->where('roles.name', '=', 'vendor')
            ->where('users.last_login', '=', '0000-00-00 00:00:00')
            ->get();


        $vendors_per_category = DB::table('classifications')
            ->join('classification_user', function ($join) {
                $join->on('classifications.id', '=', 'classification_user.classification_id');
            })
            ->select('name', DB::raw('count(*) as total'))
            ->groupBy('name')
            ->orderBy('total', 'DESC')
            ->get();
        $category_number = count($vendors_per_category);

        $jobs_per_category = DB::table('jobs')
            ->select('classification', DB::raw('count(*) as total'))
            ->groupBy('classification')
            ->orderBy('total', 'DESC')
            ->get();
        $active_jobs = DB::table('jobs')
            ->where('event_date', '>=', date('Y-m-d H:i:s', strtotime('NOW')))
            ->get();
        $active_jobs_number = count($active_jobs);

        if ($id == "all") {
            $dash_stats = [
                'jobs' => [
                    'total' => $jobs_number,
                    'active' => $active_jobs_number,
                    'bids' => $bids_number,
                ],

                'users' => [
                    'vendor_number' => $vendor_number,
                    'user_number' => $user_number,
                ],

                'credits' => [
                    'credits_purchased' => $credits_purchased,
                    'vendors_purchased' => $vendors_purchased,
                    'credits_on_network' => $credits_on_network,
                    'credits_spent' => $credits_spent,
                ],

                'messages' => [
                    'total_messages' => $message_number,
                    'total_leads' => $leads_number,
                ],

                'categories' => [
                    'total' => $category_number,
                ],
            ];


            return $dash_stats;
        } else {

            $data = $$id;
            $multiple_spline_list = ["bids_made"];
            $period_spline_list = array("new_vendors", "new_users", "sent_messages", "sent_leads", "jobs_created");
            $doughnut_list = array("jobs_per_category", "vendors_per_category");


            if (in_array($id, $multiple_spline_list)) {

                $votes = \Lava::DataTable();

                $results = array_merge($grouped_bids_made, $data);
                foreach ($results as $result) {
                    if (!isset($merged_results[$result->period])) {
                        $merged_results[$result->period] = [];
                        $merged_results[$result->period]['status-0'] = 0;
                        $merged_results[$result->period]['status-1'] = 0;
                        $merged_results[$result->period]['status-2'] = 0;
                        $merged_results[$result->period]['status-3'] = 0;

                    }

                    if (isset($result->status)) {
                        $merged_results[$result->period]['status-' . $result->status] = $result->total;
                    }

                }

                $votes->addStringColumn('Results')
                    ->addNumberColumn('In Review')
                    ->addNumberColumn('Shortlisted')
                    ->addNumberColumn('Accepted')
                    ->addNumberColumn('Declined');
                foreach ($merged_results as $key => $result) {
                    $votes->addRow([$key, $result['status-0'], $result['status-1'], $result['status-2'], $result['status-3']]);
                }

                $data['votes'] = \Lava::LineChart('Votes', $votes, [
                    'title' => 'Bids_made',
                    'pointSize' => 5
                ]);
                $type = 'line';
                return $this->view('admin.stats.main', $data)->with('type', $type);
            }


            if (in_array($id, $period_spline_list)) {

                $votes = \Lava::DataTable();

                $votes->addStringColumn('Results')
                    ->addNumberColumn(str_replace("_", " ", $id));
                foreach ($data as $object) {
                    $votes->addRow([$object->period, $object->total]);
                }

                $data['votes'] = \Lava::LineChart('Votes', $votes, [
                    'title' => strtoupper(str_replace("_", " ", $id)),
                    'pointSize' => 5
                ]);
                $type = 'line';
                return $this->view('admin.stats.main', $data)->with('type', $type);
            }

            if (in_array($id, $doughnut_list)) {

                $votes = \Lava::DataTable();

                $votes->addStringColumn('Results')
                    ->addNumberColumn(str_replace("_", " ", $id));
                foreach ($data as $object) {
                    if ($id == 'jobs_per_category') {
                        $votes->addRow([$object->classification, $object->total]);
                    } else {
                        $votes->addRow([$object->name, $object->total]);
                    }
                }
                $data['votes'] = \Lava::BarChart('Votes', $votes);
                $type = 'bar';
                return $this->view('admin.stats.main', $data)->with('type', $type);
            }
        }
    }

    public function getChart($id)
    {

        return $this->view('admin.stats.main')->with('type', $id);
    }

    /**
     * @param       $view
     * @param array $data
     *
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $data = [])
    {
        if ($this->is_ajax) {
            return view($view, $data);
        }

        return view('layouts.non-ajax-layout', $data)
            ->with('content', $view);

    }

    public function index()
    {
        return view('admin.index');
    }

    public function feedback(Request $request)
    {
        $input = Input::all();
        $validator = Validator::make($input, Feedback::$rules, Feedback::$messages);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        if ($this->captchaCheck() == false) {
            return redirect()->back()
                ->withErrors(['Wrong Captcha'])
                ->withInput();
        }
        $feedback = new Feedback;
        $feedback->comment = $input['comment'];
        $feedback->rating = $input['rating'];
        $feedback->save();

        //Send Email

        $userMailer = new UserMailer;

        $data = [
            'user_name' => 'Blissful Support Team',
            'to' => ' happy@blissful.co.ke',
            'subject' => 'Blissful User Feedback',
            'text' => $input['comment'] . ' Rating: '.$input['rating'],
        ];

        $userMailer->simpleEmail($data);

        Flash::success('Your feedback was received successfully!');

        return redirect('/');
    }

    public function rating($rating, $token)
    {
        $meta = DB::table('meta_table')->where('meta_value', '=', $token)->get();

        if ($meta) {

            Feedback::create([
                'rating' => $rating,
                'comment' => 'email recommendation',
            ]);
            Activity::create([
                'subject_id'=> (Auth::check()) ? Auth::user()->id : 0,
                'subject_type'=>'App\Feedback',
                'name'=>'recommended_blissful',
                'user_id'=> (Auth::check()) ? Auth::user()->id : 0,
            ]);
            DB::table('meta_table')->where('meta_value', '=', $token)->delete();

            Flash::success('Thank you for rating blissful!');

            return redirect('/');

        } else {

            Flash::warning('Recommendation token does not exist!');

            return redirect('/');
        }

    }
}