<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use ResponseManager;
use Request;
use Response;
use App\Models\ShopProduct;
use Cart;
use Illuminate\Support\Facades\Auth;
use App\Activity;
use App\Models\ShopCategory;
class CartController extends Controller
{

    protected $user_id;

    public function __construct(Request $request)
    {
        $this->_user_id();
    }

    private function _user_id()
    {
        $this->user = Auth::User();

        if ($this->user) {
            $this->user_id = $this->user->id;
        }
        else {

            if (!session('temp_guest_user_id')) {
                $ip = get_ip_address();
                session([ 'temp_guest_user_id' => uniqid('0' . '_' . $ip) ]);
            }

            $this->user_id = session('temp_guest_user_id');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $carts = Cart::instance('Cart')->content()->toArray();
        $products = [ ];
        $total = 0;
        $categories = ShopCategory::orderBy('id', 'desc')->get();
        foreach ($carts as $ct) {
            $product = ShopProduct::with([ 'category', 'subCategory' ])
                                  ->where('id', $ct[ 'id' ])
                                  ->first();

            if ($product) {
                $product = $product->toArray();

                $product[ 'qty' ] = $ct[ 'qty' ];
                $price = $product[ 'price' ];

                if ($product[ 'sale_price' ]) {
                    $price = $product[ 'sale_price' ];
                }

                $product[ 'price' ] = $price;
                $product[ 'total' ] = $price * $ct[ 'qty' ];
                $product[ 'rowid' ] = $ct[ 'rowid' ];
                $total += $product[ 'total' ];

                array_push($products, $product);

            }
            else {

                Cart::instance('Cart')
                    ->remove($ct[ 'rowid' ]);
            }
        }

        return view('shop.cart')->with([    'categories'=>$categories,
                                           'cod_products'   => $products,
                                           'cod_total'      => $total,
                                           'cart_total' => Cart::instance('Cart')->total(),
                                       ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $id = Request::get('product');

        if (!$id) {

            return response()->json([ 'flag' => false ]);
        }

        $addToCart = $this->addToCart($id);

        return response()->json([ 'flag' => $addToCart ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {

        $addToCart = $this->addToCart($id);

        return response()->json([ 'flag' => $addToCart ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        return $this->update($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        $input = Request::all();
        Cart::instance('Cart')->update($id, $input);

        return response()->json([ 'flag' => true ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $delete = Cart::instance('Cart')->remove($id);

        return response()->json([ 'flag' => true ]);
    }

    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////

    private function addToCart($product_id)
    {
        $detail = ShopProduct::find($product_id);
        if ($detail) {
            $detail = $detail->toArray();
            $price = $detail[ 'price' ];
            if ($detail[ 'sale_price' ]) {
                $price = $detail[ 'sale_price' ];
            }

            Activity::create([
                'subject_id'   => $product_id,
                'subject_type' => 'App\Models\ShopProduct',
                'name'         => 'added_cart_item',
                'user_id'      => ( Auth::check() ) ? Auth::user()->id : 0,
            ]);
            Cart::instance('Cart')->add([
                                            'id'      => $detail[ 'id' ],
                                            'name'    => $detail[ 'name' ],
                                            'qty'     => 1,
                                            'price'   => $price,
                                            'options' => [
                                                'dis'     => $detail[ 'discount' ],
                                                'user_id' => $this->user_id,
                                            ],

                                        ]);

            return true;
        }

        return false;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function emptyCart()
    {
        $user_id = Auth::user()->id;

        $delete = Cart::instance('Cart')->remove();

        return response()->json([ 'flag' => true ]);
    }

    public function cartCount()
    {   
        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $count = Cart::instance('Cart')->count(true, $user_id);
        }
        else{
            $count = Cart::instance('Cart')->count();
        }
        if ($count > 0 ) {
            echo $count;
        }
        
    }


}
