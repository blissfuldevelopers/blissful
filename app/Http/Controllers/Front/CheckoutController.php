<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Request;
use Response;
use App\Models\ShopProduct;
use App\Models\ShopOrder;
use Cart;
use Auth;
use App\Activity;
use App\Libraries\pesapal;
use App\Models\ShopCategory;
use App\Meta;

class CheckoutController extends Controller
{
    private $use_live_pesapal = true;

    protected $user;
    protected $user_id;


    public function __construct()
    {
        $this->user = Auth::User();

        PesaPal::$use_live_pesapal = (bool)$this->use_live_pesapal;
        PesaPal::init(); //setup PesaPal settings we need

        $this->_user_id();

    }

    private function _user_id()
    {
        $this->user = Auth::User();

        if ($this->user) {
            $this->user_id = $this->user->id;
        } else {

            if (!session('temp_guest_user_id')) {
                $ip = get_ip_address();
                session(['temp_guest_user_id' => uniqid('0' . '_' . $ip)]);
            }

            $this->user_id = session('temp_guest_user_id');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (!Auth::check()) {
            return redirect('/register');
        }

        $carts = Cart::instance('Cart')
            ->content()
            ->toArray();

        if (count($carts) == 0) {
            return redirect('/shop/cart');
        }

        $input = Request::all();
        $categories = ShopCategory::orderBy('id', 'desc')->get();

        // split products into those payable via COD and those by Pesapal
        $cart_details = static::process_cart_items_to_checkout($carts);

        // if the user has products in cart that are payable via COD and Pesapal

        if (count($cart_details['pesapal_products']) > 0 && count($cart_details['pesapal_products']) > 0) {

            //get required details
            $pesapal_iframe_details = $this->pesapal_iframe($cart_details['pesapal_total'], $cart_details['pesapal_products'], $input);
            $cod_iframe_details = $this->cod_iframe($cart_details['cod_total'], $cart_details['cod_products'], $input);

            //redirect to view
            return view('shop.cart')
                ->with([
                    'categories' => $categories,
                    'pesapal_products' => $cart_details['pesapal_products'],
                    'cod_products' => $cart_details['cod_products'],
                    'pesapal_total' => $cart_details['pesapal_total'],
                    'cod_total' => $cart_details['cod_total'],
                    'flag' => 0,
                    'title' => 'Checkout',
                    'pesapal_iframe_src' => $pesapal_iframe_details,
                    'cod_iframe_src' => $cod_iframe_details,
                ]);

        }

        if (count($cart_details['pesapal_products']) > 0) {

            //get required details
            $pesapal_iframe_details = $this->pesapal_iframe($cart_details['pesapal_total'], $cart_details['pesapal_products'], $input);

            //redirect to view
            return view('shop.cart')
                ->with([
                    'categories' => $categories,
                    'pesapal_products' => $cart_details['pesapal_products'],
                    'pesapal_total' => $cart_details['pesapal_total'],
                    'flag' => 0,
                    'title' => 'Checkout',
                    'pesapal_iframe_src' => $pesapal_iframe_details,
                ]);

        }

        if (count($cart_details['cod_products']) > 0) {

            //get required details
            $cod_iframe_details = $this->cod_iframe($cart_details['cod_total'], $cart_details['cod_products'], $input);

            //redirect to view
            return view('shop.cart')
                ->with([
                    'categories' => $categories,
                    'cod_products' => $cart_details['cod_products'],
                    'cod_total' => $cart_details['cod_total'],
                    'flag' => 0,
                    'title' => 'Checkout',
                    'cod_iframe_src' => $cod_iframe_details,
                ]);

        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if (!Auth::check()) {
            return redirect('/register');
        }

        $carts = Cart::instance('Cart')
            ->content()
            ->toArray();

        if (count($carts) == 0) {
            return redirect('/shop/cart');
        }

        $input = Request::all();
        $categories = ShopCategory::orderBy('id', 'desc')->get();

        // split products into those payable via COD and those by Pesapal
        $cart_details = static::process_cart_items_to_checkout($carts);

        // if the user has products in cart that are payable via COD and Pesapal

        if (count($cart_details['pesapal_products']) > 0 && count($cart_details['pesapal_products']) > 0) {

            //get required details
            $pesapal_iframe_details = $this->pesapal_iframe($cart_details['pesapal_total'], $cart_details['pesapal_products'], $input);
            $cod_iframe_details = $this->cod_iframe($cart_details['cod_total'], $cart_details['cod_products'], $input);

            //redirect to view
            return view('shop.cart')
                ->with([
                    'categories' => $categories,
                    'pesapal_products' => $cart_details['pesapal_products'],
                    'cod_products' => $cart_details['cod_products'],
                    'pesapal_total' => $cart_details['pesapal_total'],
                    'cod_total' => $cart_details['cod_total'],
                    'flag' => 0,
                    'title' => 'Checkout',
                    'pesapal_iframe_src' => $pesapal_iframe_details,
                    'cod_iframe_src' => $cod_iframe_details,
                ]);

        }

        if (count($cart_details['pesapal_products']) > 0) {

            //get required details
            $pesapal_iframe_details = $this->pesapal_iframe($cart_details['pesapal_total'], $cart_details['pesapal_products'], $input);

            //redirect to view
            return view('shop.cart')
                ->with([
                    'categories' => $categories,
                    'pesapal_products' => $cart_details['pesapal_products'],
                    'pesapal_total' => $cart_details['pesapal_total'],
                    'flag' => 0,
                    'title' => 'Checkout',
                    'pesapal_iframe_src' => $pesapal_iframe_details,
                ]);

        }

        if (count($cart_details['cod_products']) > 0) {

            //get required details
            $cod_iframe_details = $this->cod_iframe($cart_details['cod_total'], $cart_details['cod_products'], $input);

            //redirect to view
            return view('shop.cart')
                ->with([
                    'categories' => $categories,
                    'cod_products' => $cart_details['cod_products'],
                    'cod_total' => $cart_details['cod_total'],
                    'flag' => 0,
                    'title' => 'Checkout',
                    'cod_iframe_src' => $cod_iframe_details,
                ]);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $detail = ShopProduct::find($id);
        if ($detail) {
            $detail = $detail->toArray();
            $price = $detail['price'];

            if ($detail['sale_price']) {
                $price = $detail['sale_price'];
            }

            Cart::instance('Cart')->add([
                'id' => $detail['id'],
                'name' => $detail['name'],
                'qty' => 1,
                'price' => $price,
                'options' => [
                    'dis' => $detail['discount'],
                    'user_id' => $this->user_id,
                ],
            ]);

            return response()
                ->json(['flag' => true]);
        } else {

            return response()
                ->json(['flag' => false]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {

//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {

    }

    ///////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
    ////////////////////////////// End of Restful Functions ///////
    ///////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////


    /**
     * @param null $carts
     *
     * @return array
     */
    private static function process_cart_items_to_checkout($carts = null)
    {
        $pesapal_products = [];
        $cod_products = [];

        $pesapal_total = 0;
        $cod_total = 0;

        if ($carts) {
            foreach ($carts as $ct) {
                $product = ShopProduct::with(['category', 'subCategory'])
                    ->where('id', $ct['id'])
                    ->first();


                if ($product) {

                    $product = $product->toArray();
                    $product['qty'] = $ct['qty'];
                    $price = $product['price'];
                    if ($product['sale_price']) {
                        $price = $product['sale_price'];
                    }
                    $product['price'] = $price;
                    $product['total'] = $price * $ct['qty'];
                    $product['rowid'] = $ct['rowid'];

                    //separate pesapal products from COD products

                    if ($product['payment_method'] == 0) {  //these go through pesapal
                        $pesapal_total += $product['total'];
                        array_push($pesapal_products, $product);
                    } elseif ($product['payment_method'] == 1) { //these are for Cash on delivery
                        $cod_total += $product['total'];
                        array_push($cod_products, $product);
                    }

                } else {

                    Cart::instance('Cart')->remove($ct['rowid']);
                }
            }
        }

        return [
            'cod_products' => $cod_products,
            'pesapal_products' => $pesapal_products,
            'pesapal_total' => $pesapal_total,
            'cod_total' => $cod_total,

        ];
    }

    /**
     * TODO Cleanup the way the cart products are fetched in an array to bulk fetch and looping over results
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function finalStep()
    {
        $user_id = Auth::user()->id;

        $final_step = PesaPal::paymentFinalStep(Request::all(), $user_id);

        return redirect($final_step['redirect'])
            ->with('status', $final_step['message_status'])
            ->with('messageZurb', $final_step['messageZurb']);

    }

    /**
     * return the pesapal iframe details (if everything is fine) else return the checkout page
     *
     * @param int $total
     * @param array $products
     * @param array $input
     *
     * @return $this
     */
    private function pesapal_iframe($total = 0, $products = [], $input = [])
    {
        $input['email'] = $email = (isset($input['email'])) ? $input['email']
            : (isset($this->user->email)) ? $this->user->email : null;

        $input['phone'] = $phonenumber = (isset($input['phone'])) ? $input['phone']
            : ((@$this->user->profile->phone) ? @$this->user->profile->phone : '##########'); //ONE of email or phonenumber is required

        if (!$phonenumber && !$email) {

            if (empty($input)) {
                //add errors
            }
            if (!$phonenumber && !$email) {
                //add errors

            }

            return view('checkout')
                ->with([
                    'products' => $products,
                    'total' => $total,
                    'flag' => 1,
                    'phone_number' => $phonenumber,
                ]);
        }


        //pesapal params


        //get form details
        $input['total'] = $amount = (isset($input['total'])) ? $input['total'] : $total; //format amount to 2 decimal places
        $amount = number_format($amount, 2);


        $input['user_id'] = (!isset($input['user_id']))
            ? (($this->user) ? $this->user->id : 0)
            : 0;

        //update ShopOrder
        $delete = ShopOrder::where('pesapal_transaction_tracking_id', '')
            ->where('user_id', $this->user->id)
            ->delete();

        $input['status'] = 'CHECKOUT';
        $order = ShopOrder::create($input, $products);
        $prefix = '';

        foreach ($products as $product) {

            $array_desc[] = $prefix . $product['name'];
        }
        $desc = implode(',', $array_desc);
        $reference = $order['id']; //unique order id of the transaction, generated by merchant

        $first_name = preg_replace('/[^A-Za-z0-9\. -]/', '', $this->user->first_name);
        $last_name = preg_replace('/[^A-Za-z0-9\. -]/', '', $this->user->last_name);

        //post transaction to pesapal
        $iframe_src = PesaPal::iframe($amount, $desc, $reference, $first_name, $last_name, $email, $phonenumber);

        return $iframe_src;
        return view('shop.cart')
            ->with([
                'categories' => $categories,
                'products' => $products,
                'total' => $total,
                'flag' => 0,
                'title' => 'Checkout',
                'iframe_src' => $iframe_src,
            ]);
    }

    /**
     *
     *return COD details
     *
     * @param  int $total
     * @param  int $products
     * @param  int $input
     *
     * @return Response
     */

    public function cod_iframe($total = 0, $products = [], $input = [])
    {


        $input['email'] = $email = (isset($input['email'])) ? $input['email']
            : (isset($this->user->email)) ? $this->user->email : null;

        $input['phone'] = $phonenumber = (isset($input['phone'])) ? $input['phone']
            : ((@$this->user->profile->phone) ? @$this->user->profile->phone : '##########'); //ONE of email or phonenumber is required

        if (!$phonenumber && !$email) {

            if (empty($input)) {
                //add errors
            }
            if (!$phonenumber && !$email) {
                //add errors

            }

            return view('checkout')
                ->with([
                    'products' => $products,
                    'total' => $total,
                    'flag' => 1,
                    'phone_number' => $phonenumber,
                ]);
        }

        //get form details
        $input['total'] = $amount = (isset($input['total'])) ? $input['total'] : $total; //format amount to 2 decimal places
        $amount = number_format($amount, 2);


        $input['user_id'] = (!isset($input['user_id']))
            ? (($this->user) ? $this->user->id : 0)
            : 0;
        $input['status'] = 'CHECKOUT';

        $cod_iframe = ShopOrder::create($input, $products);
        return $cod_iframe;

    }

    /**
     *
     */
    public function pespalIpn()
    {
        $input = Request::all();

        return PesaPal::ipn($input);
    }

    /**
     * Save order details for users on placing order
     */
    public function save_user_order_details()
    {
        $input = Request::all();
        $user = Auth::user();

        //save delivery town
        $delivery_town = Meta::create([
            'object' => 'App\User',
            'object_id' => $user->id,
            'meta_key' => 'delivery_town',
            'meta_value' => $input['town'],
        ]);
        //save delivery address
        $delivery_address = Meta::create([
            'object' => 'App\User',
            'object_id' => $user->id,
            'meta_key' => 'delivery_address',
            'meta_value' => $input['address'],
        ]);

        //update user details

        $user->first_name = ucfirst($input['first_name']);
        $user->last_name = ucfirst($input['last_name']);
        $user->profile->phone = ($input['phone']);

        $user->save();
        $user->profile->save();


        //update order

        $ShopOrder = ShopOrder::findorfail($input['shop_order_id']);
        $ShopOrder->status = 'FILLED DETAILS';
        $total = $ShopOrder->total;

        if ($input['town'] == 'Nairobi') {
            $total = $total + 300;
            $ShopOrder->shipping_fees = 300;
        } else {
            $total = $total + 500;
            $ShopOrder->shipping_fees = 500;
        }

        $ShopOrder->total = $total;
        $ShopOrder->save();
        return view('shop.partials.order-confirmation', compact('input', 'ShopOrder'));

    }

    /**
     * Confirm order on COD
     */
    public function cod_order_confirmation($id)
    {

        $ShopOrder = ShopOrder::findorfail($id);
        $ShopOrder->status = 'ORDER RECIEVED';
        $ShopOrder->save();

        //empty cart
        Cart::instance('Cart')->destroy();

        return view('shop.partials.order-placed', compact('ShopOrder'));
    }

}
