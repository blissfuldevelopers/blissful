<?php

namespace App\Http\Controllers\Front;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use ResponseManager;
use Illuminate\Http\Request;
use Response;
use Input;
use App\Models\ShopProduct;
use App\Models\ShopCategory;
use Cookie;
use App\Activity;
use Illuminate\Support\Facades\Auth;
use App\Models\ShopSubcategory;
class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('shop.shop-deal-view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($product_slug)
    {
        $product = ShopProduct::where('product_slug', $product_slug)->get();
        $product = ShopProduct::filterByUserRole($product, null, true);
        if (empty($product)) {
            abort(404);
        }
//        if (!$product) {
//            $message = 'Could not find the shop item';
//
//            return view('shop.shop-deal-view')
//                ->with([
//                           'data'     => '',
//                           'similar'  => '',
//                           'title'    => 'Invalid Product',
//                           'subtitle' => $message,
//                       ]);
//        }

        if (isset( $_COOKIE[ 'recent' ] )) {
            $ids = unserialize($_COOKIE[ 'recent' ]);
            array_unshift($ids, $product->id);
            $ids = array_unique($ids);
            setcookie("recent", serialize($ids), time() + 86400, '/');
        }
        else {
            setcookie("recent", serialize([ $product->id ]), time() + 86400, '/');
        }

        $similar = ShopProduct::fetchShopProductQuery()
                              ->where('id', '!=', $product->id)
                              ->where('category_id', $product->category_id)
                              ->orderBy('created_at', 'DESC')
                              ->take(4)
                              ->get();
        $categories = ShopCategory::orderBy('id', 'desc')->get();

        $title = $product->name;
        $subtitle = html_entity_decode($product->description);

        Activity::create([
            'subject_id'   => $product->id,
            'subject_type' => 'App\Models\ShopProduct',
            'name'         => 'viewed_shop_product',
            'user_id'      => ( Auth::check() ) ? Auth::user()->id : 0,
        ]);

        return view('shop.shop-product-view')
            ->with([
                       'product'  => $product,
                       'similar'  => $similar,
                       'title'    => $title,
                       'subtitle' => $subtitle,
                       'categories' =>  $categories,
                   ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {

    }

    ///////////////////////////////

    public function home(Request $request)
    {
        $input = $request->all();
        $limit = ( Input::has('limit') ) ? Input::get('limit') : 100;
        $subtitle = '';
        $ids = [ ];
        $home_products = [];
        if (isset( $_COOKIE[ 'recent' ] )) {
            $ids = unserialize($_COOKIE[ 'recent' ]);
        }


        $products = ShopProduct::fetchShopProducts()
        ->whereHas('category', function ($query) {
            $query->where('package',0);
          })
        ->paginate($limit);
        $products = ShopProduct::filterByUserRole($products);

        $recent_products = ShopProduct::fetchRecentProducts($ids)->get();
        $recent_products = ShopProduct::filterByUserRole($recent_products);
        $categories = ShopCategory::where('package', 0)->orderBy('id', 'desc')->get();
        $array_categories = $categories->toArray();
        if(!empty($array_categories)){
            $randKeys = array_rand($array_categories,2);

            foreach($randKeys as $key){
                $home_products[] = $array_categories[$key];
            }

        }
        else{
            $randKeys = array_rand($recent_products,2);

            foreach($randKeys as $key){
                $home_products[] = $recent_products[$key];
            }

        }

        Activity::create([
            'subject_id'   => 6,
            'subject_type' => 'App\Page',
            'name'         => 'read',
            'user_id'      => (Auth::check()) ? Auth::user()->id : 0,
        ]);

        return view('shop.shop-home')->with([   
                                                'home_products' => $home_products,
                                                'recent'   => $recent_products,
                                                'products' => $products,
                                                'categories'=>$categories,
                                            ]);
    }

    public function catProduct($category_slug)
    {
        $category = ShopCategory::with('subcategories')->where('category_slug', $category_slug)->get();
        $category = ShopCategory::filterByUserRole($category, null, true);
        if (empty($category)) {
            abort(404);
        }
        $subtitle = ( $category )
            ? html_entity_decode($category->name . ' &bull; ' . $category->description)
            : 'Category not found';

        $products = ShopProduct::fetchShopProductQuery()
                               ->where('category_id', $category->id)
                               ->orderBy('created_at', 'DESC')
                               ->get();

        $products = ShopProduct::filterByUserRole($products);
        $categories = ShopCategory::where('package', 0)->orderBy('id', 'desc')->get();
        Activity::create([
            'subject_id'   => $category->id,
            'subject_type' => 'App\Models\ShopCategory',
            'name'         => 'viewed_shop_category',
            'user_id'      => ( Auth::check() ) ? Auth::user()->id : 0,
        ]);
        return view('shop.shop-home')
            ->with([ 'products' => $products,
                     'cat'      => $category,
                     'title'    => $category->name,
                     'subtitle' => $subtitle,
                     'categories'=>$categories,
                   ]);
    }
    public function subCatProduct($subcategory_slug)
    {
        $subcategory = ShopSubcategory::wheresubcategory_slug($subcategory_slug)->first();
        $subtitle = ( $subcategory )
            ? html_entity_decode($subcategory->name . ' &bull; ' . $subcategory->description)
            : 'Category not found';

        $products = ShopProduct::fetchShopProductQuery()
            ->where('sub_category_id', $subcategory->id)
            ->orderBy('created_at', 'DESC')
            ->get();

        $products = ShopProduct::filterByUserRole($products);
        $categories = ShopCategory::where('package', 0)->orderBy('id', 'desc')->get();
        Activity::create([
            'subject_id'   => $subcategory->id,
            'subject_type' => 'App\Models\ShopSubcategory',
            'name'         => 'viewed_shop_subcategory',
            'user_id'      => ( Auth::check() ) ? Auth::user()->id : 0,
        ]);
        return view('shop.shop-home')
            ->with([ 'products' => $products,
                'cat'      => $subcategory,
                'title'    => $subcategory->name,
                'subtitle' => $subtitle,
                'categories'=>$categories,
            ]);
    }

}
