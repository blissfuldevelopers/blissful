<?php

namespace App\Http\Controllers\AdminShop;

use App\Http\Controllers\Controller;
use ResponseManager;
use Illuminate\Http\Request;
use Response;
use App\Models\ShopSubcategory;
use App\Models\ShopProduct;
use App\Models\ShopCategory;

class SubCategoryController extends AdminShopController
{
    protected $is_ajax = false;

    public function __construct(Request $request)
    {
//        $this->middleware('shopVendor');
        $this->is_ajax = $request->ajax();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $subcategories = ShopSubcategory::with('category')->orderBy('id', 'desc')->paginate(15);
        
        if (count($subcategories) > 0) {
            return $this->view('shop.admin.subcategories.subcategory-show', ['subcategories' => $subcategories]);
        } else {
            $message = 'Error';
            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories = ShopCategory::get();
        return $this->view('shop.admin.subcategories.subcategory-create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validation = ShopSubcategory::validate($input);
        if ($validation->fails()) {
            $message = $validation->messages()->first();
            return Response()->json(ResponseManager::getError('', 10, $message));
        }
        $input['subcategory_slug'] = str_slug($input[ 'name' ], "-");
        $subcategory = ShopSubcategory::create($input);
        if ($subcategory) {
//            $data = ShopSubcategory::with('category')->where('id', $subcategory['id'])->first()->toArray();
//            $message = 'Sub Category Added Success';
//            return Response()->json(ResponseManager::getResult($data, 10, $message));
            return redirect('shop/admin/subcategory');
        } else {
            $message = 'Error while add Sub Category';
            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $subcategory = ShopSubcategory::find($id);
        if ($subcategory) {
            $message = 'SubCategory Added Success';
            return Response()->json(ResponseManager::getResult($subcategory->toArray(), 10, $message));
        } else {
            $message = 'Invalid SubCategory';
            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {

        $subcategory = ShopSubcategory::with('category')->where('id', $id)->first();
        $categories = ShopCategory::get();
        return $this->view('shop.admin.subcategories.subcategory-edit', [ 'subcategory' => $subcategory,'categories'=> $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();
        $validation = ShopSubcategory::validateUpdate($input, $id);
        if ($validation->fails()) {
            $message = $validation->messages()->first();
            return Response()->json(ResponseManager::getError('', 10, $message));
        }

        $subcategory = ShopSubcategory::findorfail($id);
        $subcategory->update($input);
        if ($subcategory) {
//            $data = ShopSubcategory::with('category')->where('id', $id)->first()->toArray();
//            $message = 'Update Successful';
//            return Response()->json(ResponseManager::getResult($data, 10, $message));
            return redirect('shop/admin/subcategory');
        } else {
            $message = 'Error while update category';
            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {

        $existPrd = ShopProduct::where('category_id', $id)->count();
        if ($existPrd) {
            $message = 'Sub Category exist in products. Please remove it first.';
            return Response()->json(ResponseManager::getError('', 10, $message));
        }

        $delete = ShopSubcategory::where('id', $id)->delete();
        if ($delete) {
//            $message = 'Delete SubCategory Success';
//            return Response()->json(ResponseManager::getResult($delete, 10, $message));
            return redirect('shop/admin/subcategory');
        } else {
            $message = 'Error while delete SubCategory';
            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    public function getSubCategory($id)
    {
        $subcategory = ShopSubcategory::where('category_id', $id)->get()->toArray();
        if (count($subcategory) > 0) {
            $message = 'Success';
            return Response()->json(ResponseManager::getResult($subcategory, 10, $message));
        } else {
            $message = 'No Sub Category found for category';
            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

}
