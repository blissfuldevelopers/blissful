<?php

namespace App\Http\Controllers\AdminShop;

use App\ProductImage;
use ResponseManager;
use Illuminate\Http\Request;
use App\Models\ShopProduct;
use App\Models\ShopCategory;
use App\Models\ShopSubcategory;
use App\Locations;
use Intervention\Image\Facades\Image;
use Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Laracasts\Flash\Flash;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Contracts\Filesystem\Filesystem;
class ProductsController extends AdminShopController
{
    protected $is_ajax = false;

    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $products = ShopProduct::orderBy('id', 'desc')->get();
           
//        $products = ShopProduct::filterByUserRole($products);
        return $this->view('shop.admin.products.products-show')->with('products',$products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories = ShopCategory::get();
        $subcategories = ShopSubcategory::with('category')->get();
        $locations = Locations::all();
        return $this->view('shop.admin.products.product-create', ['categories' => $categories, 'subcategories' => $subcategories, 'locations' => $locations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validation = ShopProduct::validate($input);

        if ($validation->fails()) {
            $message = $validation->messages()->first();

            return Response()->json(ResponseManager::getError('', 10, $message));
        }

//        if (array_key_exists('picture', $input)) {
//            $img = $input[ 'picture' ];
//            list( $type, $data ) = explode(';', $img);
//            list( , $data ) = explode(',', $data);
//            $data = base64_decode($data);
//            $c = uniqid(rand(), true);
//            $md5c = md5($c);
//            $path = 'uploads/product-service/' . $md5c . '.png';
//            file_put_contents($path, $data);
//            $input[ 'picture' ] = $path;
//        }
        if ($request->hasFile('picture')) {
            $coverimg = $request->file('picture');
            $covername = uniqid() . $coverimg->getClientOriginalName();
            $thumb = Image::make($coverimg->getRealPath())->resize(600, 400);

            $image_thumb = $thumb->stream();
            $s3 = \Storage::disk('s3');
            $filePath = 'uploads/product-service/' . $covername;
            $s3->getDriver()->put($filePath, $image_thumb->__toString(),[ 
                
                                                                            'visibility' => 'public',
                                                                            'CacheControl' => 'max-age=2592000',
                                                                            'Expires' => gmdate("D, d M Y H:i:s T",strtotime("+3 years"))

                                                                        ]);

            $input['picture'] = 'uploads/product-service/' . $covername;
        }
        $input['seller_id'] = Auth::user()->id;
        $input['sale_price'] = $input['price'] - $input['discount'];
        $input['product_slug'] = str_slug($input['name'], "-");
        $product = ShopProduct::create($input);
        if ($product) {
//            $data = ShopProduct::fetchShopProducts()->where('id', $product['id'])->first()->toArray();
//            $message = 'Product Added Success';
//
//            return Response()->json(ResponseManager::getResult($data, 10, $message));
             Flash::success('Product created successfully');
            return redirect()->route('shop.admin.products.index');
        } else {
            $message = 'Error while add category';

            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $product = ShopProduct::find($id);
        if ($product) {
            $message = 'Product Added Success';

            return Response()->json(ResponseManager::getResult($product->toArray(), 10, $message));
        } else {
            $message = 'Invalid Product';

            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $product = ShopProduct::with('category')->where('id', $id)->first();
        $categories = ShopCategory::get();
        $subcategories = ShopSubcategory::with('category')->get();
        $locations = Locations::all();
        return $this->view('shop.admin.products.product-edit', ['product' => $product, 'categories' => $categories, 'subcategories' => $subcategories, 'locations' => $locations]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();
        $validation = ShopProduct::validateUpdate($input, $id);
        if ($validation->fails()) {
            $message = $validation->messages()->first();

            return Response()->json(ResponseManager::getError('', 10, $message));
        }

//        if (array_key_exists('picture', $input)) {
//            $img = $input['picture'];
//            list($type, $data) = explode(';', $img);
//            list(, $data) = explode(',', $data);
//            $data = base64_decode($data);
//            $c = uniqid(rand(), true);
//            $md5c = md5($c);
//            $path = 'uploads/product-service/' . $md5c . '.png';
//            file_put_contents($path, $data);
//            $input['picture'] = $path;
//        }
        if ($request->hasFile('picture')) {
            $coverimg = $request->file('picture');
            $covername = uniqid() . $coverimg->getClientOriginalName();
            $thumb = Image::make($coverimg->getRealPath())->resize(600, 400)->save('uploads/product-service/' . $covername, 86);

            $image_thumb = $thumb->stream();
            $s3 = \Storage::disk('s3');
            $filePath = 'uploads/product-service/' . $covername;
            $s3->getDriver()->put($filePath, $image_thumb->__toString(),[ 
                
                                                                            'visibility' => 'public',
                                                                            'CacheControl' => 'max-age=2592000',
                                                                            'Expires' => gmdate("D, d M Y H:i:s T",strtotime("+3 years"))

                                                                        ]);
            
            $input['picture'] = 'uploads/product-service/' . $covername;
        }
        $input['sale_price'] = $input['price'] - $input['discount'];
        $product = ShopProduct::findorfail($id);
        $product->update($input);
        if ($product) {
            $data = ShopProduct::fetchShopProducts()->where('id', $id)->first()->toArray();
            $message = 'Update Successful';
             Flash::success('Product updated successfully');
            return redirect()->route('shop.admin.products.index');
//            return Response()->json(ResponseManager::getResult($data, 10, $message));
        } else {
            $message = 'Error while update category';

            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {

        $delete = ShopProduct::where('id', $id)->delete();
        if ($delete) {
            $message = 'Delete Product Success';
            return redirect()->route('shop.admin.products.index');
//            return Response()->json(ResponseManager::getResult($delete, 10, $message));
        } else {
            $message = 'Error while delete Product';

            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    public function paginate()
    {
        $input = Request::all();
        $page = ($input['page'] * 1) - 1;
        $limit = $input['limit'] * 1;

        $products = ShopProduct::fetchShopProducts();
        if (Auth::user()->hasRole('vendor')) {
            $products = $products->where('seller_id', Auth::User()->id);
        }
        $products = $products->take($limit)->skip($page * $limit);
        if ($page == 0) {
            $data['count'] = $products->count();
        }
        $data['list'] = $products->get()->toArray();
        if (count($data['list']) > 0) {
            $message = 'Success';

            return Response()->json(ResponseManager::getResult($data, 10, $message));
        } else {
            $message = 'No more user found';

            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    public function images($id)
    {
        $product = ShopProduct::find($id);
        return view('shop.admin.products.images', ['product' => $product]);
    }

    public function upload(Request $request)
    {
        //get the file from post
        $file = $request->file('file');

        //set filename

        $filename = uniqid() . $file->getClientOriginalName();

        //move file to save location
        $file->move('uploads/products', $filename);

        //save thumbnail to path
        $image = Image::make('uploads/products/' . $filename)->resize(600, 400)->save('uploads/products/full_size/' . $filename);
        $thumb = Image::make('uploads/products/' . $filename)->resize(240, 160)->save('uploads/products/icon_size/' . $filename, 80);

        //select gallery
        $product = ShopProduct::find($request->input('product_id'));

        //save image details to db
        $image = $product->images()->create([
            'product_id' => $request->input('product_id'),
            'file_name' => $filename,
            'file_size' => $file->getClientSize(),
            'file_mime' => $file->getClientMimeType(),
            'file_path' => 'uploads/products/full_size/' . $filename,
        ]);

        unlink(public_path('uploads/products/' . $filename));

        return redirect()->back();
    }
    public function deleteImage($id)
    {
        $currentImage = ProductImage::findorfail($id);
        $product = $currentImage->product;
        $deleteThumb = unlink(public_path('uploads/products/icon_size/' . $currentImage->file_name));
        $deleteImage = unlink(public_path($currentImage->file_path));
        $currentImage->delete();

        return view('shop.admin.products.images', ['product' => $product]);

    }

    public function paginate_results(Request $request, $results, $defaults = [ ])
    {
        $results_data = [ ];
        $paginated_data = [ ];

        $limit = ( Input::has('limit') ) ? Input::get('limit')
            : array_get($defaults, 'limit', 10);

        if ($limit != -1) {
            $offset = ( Input::has('page') ) ? ( Input::get('page') - 1 ) * $limit : 0;
        }

        $page = ( Input::has('page') ) ? Input::get('page')
            : array_get($defaults, 'page', 1);


        foreach ($results as $result) {
            $results_data [] = $result;
        }
        $itemsForCurrentPage = ( $limit != -1 ) ? array_slice($results_data, $offset, $limit, true) : $results_data;

        $paginated_data = ( $limit != -1 ) ? new Paginator($itemsForCurrentPage, count($results), $limit, $page, [
            'path'  => $request->url(),
            'query' => $request->query(),
        ]) : $results;

        return $paginated_data;
    }

}
