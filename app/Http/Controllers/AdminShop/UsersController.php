<?php

namespace App\Http\Controllers\AdminShop;

use App\Http\Controllers\Controller;
use ResponseManager;
use Request;
use Response;
use Hash;
use App\User;

class UsersController extends Controller {

    public function __construct() {
        $this->middleware('shopVendor');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $category = User::get()->toArray();
        if (count($category) > 0) {
            $message = 'Success';
            return Response()->json(ResponseManager::getResult($category, 10, $message));
        } else {
            $message = 'Error';
            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $input = Request::all();
        $validation = User::validate($input);

        if ($validation->fails()) {
            $message = $validation->messages()->first();
            return Response()->json(ResponseManager::getError('', 10, $message));
        }
        $input['password'] = Hash::make($input['password']);
        if (array_key_exists('photo', $input)) {
            $img = $input['photo'];
            list($type, $data) = explode(';', $img);
            list(, $data) = explode(',', $data);
            $data = base64_decode($data);
            $c = uniqid(rand(), true);
            $md5c = md5($c);
            $path = '../public/uploads/' . $md5c . '.png';
            $url = '../uploads/' . $md5c . '.png';
            file_put_contents($path, $data);
            $input['photo'] = $url;
        }

        $category = User::create($input);
        if ($category) {
            $message = 'User Added Success';
            return Response()->json(ResponseManager::getResult($category, 10, $message));
        } else {
            $message = 'Error while add category';
            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $category = User::find($id);
        if ($category) {
            $message = 'User Added Success';
            return Response()->json(ResponseManager::getResult($category->toArray(), 10, $message));
        } else {
            $message = 'Invalid User';
            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $input = Request::all();
        $validation = User::validateProfile($input, $id);
        if ($validation->fails()) {
            $message = $validation->messages()->first();
            return Response()->json(ResponseManager::getError('', 10, $message));
        }

        if (array_key_exists('photo', $input)) {
            $img = $input['photo'];
            list($type, $data) = explode(';', $img);
            list(, $data) = explode(',', $data);
            $data = base64_decode($data);
            $c = uniqid(rand(), true);
            $md5c = md5($c);
            $path = '../public/uploads/' . $md5c . '.png';
            $url = '../uploads/' . $md5c . '.png';
            file_put_contents($path, $data);
            $input['photo'] = $url;
        }

        $category = User::where('id', $id)->update($input);
        if ($category) {
            $message = 'Update Successful';
            return Response()->json(ResponseManager::getResult($input, 10, $message));
        } else {
            $message = 'Error while update category';
            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {

        $delete = User::where('id', $id)->delete();
        if ($delete) {
            $message = 'Delete User Success';
            return Response()->json(ResponseManager::getResult($delete, 10, $message));
        } else {
            $message = 'Error while delete User';
            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    public function paginateUser() {
        $input = Request::all();
        $page = ($input['page'] * 1) - 1;
        $limit = $input['limit'] * 1;

        $users = User::take($limit)->skip($page * $limit);
        if ($page == 0) {
            $data['count'] = $users->count();
        }

        $data['list'] = $users->get()->toArray();
        if (count($data['list']) > 0) {
            $message = 'Success';
            return Response()->json(ResponseManager::getResult($data, 10, $message));
        } else {
            $message = 'No more user found';
            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    public function getVendors() {
        $users = User::with('type')->get()->toArray();
        $data = [];
        foreach ($users as $user) {
            if ($user['type']) {
                if ($user['type'][0]['name'] == 'vendor') {
                    $data[] = $user;
                }
            }
        }
        if (count($data) > 0) {
            $message = 'Success';
            return Response()->json(ResponseManager::getResult($data, 10, $message));
        } else {
            $message = 'No more user found';
            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

}
