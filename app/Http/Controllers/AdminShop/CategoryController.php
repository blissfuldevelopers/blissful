<?php

namespace App\Http\Controllers\AdminShop;

use ResponseManager;
use Illuminate\Http\Request;
use App\Models\ShopCategory;
use App\Models\ShopProduct;
use App\Models\ShopSubcategory;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Laracasts\Flash\Flash;
use Illuminate\Contracts\Filesystem\Filesystem;
class CategoryController extends AdminShopController
{
    protected $is_ajax = false;

    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $categories = ShopCategory::orderBy('id', 'desc')->paginate(15);

        return $this->view('shop.admin.categories.categories-show', [ 'categories' => $categories ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return $this->view('shop.admin.categories.category-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validation = ShopCategory::validate($input);
        if ($validation->fails()) {
            $message = $validation->messages()->first();

            return Response()->json(ResponseManager::getError('', 10, $message));
        }
        if ($request->hasFile('header_image')) {
            $coverimg = $request->file('header_image');
            $covername = uniqid() . $coverimg->getClientOriginalName();
            $thumb = Image::make($coverimg->getRealPath())->resize(782, 280);

            $image_thumb = $thumb->stream();
            $s3 = \Storage::disk('s3');
            $filePath = 'uploads/category-images/' . $covername;
            $s3->getDriver()->put($filePath, $image_thumb->__toString(),[ 
                
                                                                            'visibility' => 'public',
                                                                            'CacheControl' => 'max-age=2592000',
                                                                            'Expires' => gmdate("D, d M Y H:i:s T",strtotime("+3 years"))

                                                                        ]);

            $input['header_image'] = 'uploads/category-images/' . $covername;
        }
        if ($request->hasFile('sub_header_image')) {
            $coverimg = $request->file('sub_header_image');
            $covername = uniqid() . $coverimg->getClientOriginalName();
            $thumb = Image::make($coverimg->getRealPath())->resize(391, 177);

            $image_thumb = $thumb->stream();
            $s3 = \Storage::disk('s3');
            $filePath = 'uploads/category-images/' . $covername;
            $s3->getDriver()->put($filePath, $image_thumb->__toString(),[ 
                
                                                                            'visibility' => 'public',
                                                                            'CacheControl' => 'max-age=2592000',
                                                                            'Expires' => gmdate("D, d M Y H:i:s T",strtotime("+3 years"))

                                                                        ]);

            $input['sub_header_image'] = 'uploads/category-images/' . $covername;
        }
        $input['category_slug'] = str_slug($input[ 'name' ], "-");
        $category = ShopCategory::create($input);
        if ($category) {
//            $message = 'Category Added Success';
//            return Response()->json(ResponseManager::getResult($category, 10, $message));
             Flash::success('Category created successfully');
            return redirect()->route('shop.admin.category.index');
        }
        else {
            $message = 'Error while add category';
             Flash::warning($message);
            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $category = ShopCategory::find($id);
        if ($category) {
            $message = 'Category Added Success';

            return Response()->json(ResponseManager::getResult($category->toArray(), 10, $message));
        }
        else {
            $message = 'Invalid Category';

            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $category = ShopCategory::find($id);

        return $this->view('shop.admin.categories.category-edit', [ 'category' => $category ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();
        $validation = ShopCategory::validateUpdate($input, $id);
        if ($validation->fails()) {
            $message = $validation->messages()->first();

            return Response()->json(ResponseManager::getError('', 10, $message));
        }
        if ($request->hasFile('header_image')) {
            $coverimg = $request->file('header_image');
            $covername = uniqid() . $coverimg->getClientOriginalName();
            $thumb = Image::make($coverimg->getRealPath())->resize(782, 280);

            $image_thumb = $thumb->stream();
            $s3 = \Storage::disk('s3');
            $filePath = 'uploads/category-images/' . $covername;
            $s3->getDriver()->put($filePath, $image_thumb->__toString(),[ 
                
                                                                            'visibility' => 'public',
                                                                            'CacheControl' => 'max-age=2592000',
                                                                            'Expires' => gmdate("D, d M Y H:i:s T",strtotime("+3 years"))

                                                                        ]);

            $input['header_image'] = 'uploads/category-images/' . $covername;
        }
        if ($request->hasFile('sub_header_image')) {
            $coverimg = $request->file('sub_header_image');
            $covername = uniqid() . $coverimg->getClientOriginalName();
            $thumb = Image::make($coverimg->getRealPath())->resize(391, 177);

            $image_thumb = $thumb->stream();
            $s3 = \Storage::disk('s3');
            $filePath = 'uploads/category-images/' . $covername;
            $s3->put($filePath, $image_thumb->__toString(), 'public');

            $input['sub_header_image'] = 'uploads/category-images/' . $covername;
        }
        $category = ShopCategory::findorfail($id);
        $category->update($input);

        if ($category) {
//            $message = 'Update Successful';
//
//            return Response()->json(ResponseManager::getResult($input, 10, $message));
             Flash::success('Category updated successfully');
            return redirect()->route('shop.admin.category.index');
        }
        else {
            $message = 'Error while updating category';
             Flash::warning($message);
            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $existPrd = ShopProduct::where('category_id', $id)->count();
        if ($existPrd) {
            $message = 'Category exist in products. Please remove it first.';

            return Response()->json(ResponseManager::getError('', 10, $message));
        }
        $existSubCat = ShopSubcategory::where('category_id', $id)->count();
        if ($existSubCat) {
            $message = 'Category exist in subcategory. Please remove it first.';

            return Response()->json(ResponseManager::getError('', 10, $message));
        }

        $delete = ShopCategory::where('id', $id)->delete();
        if ($delete) {
//            $message = 'Delete Category Success';
//
//            return Response()->json(ResponseManager::getResult($delete, 10, $message));
            return redirect()->route('shop.admin.category.index');
        }
        else {
            $message = 'Error while delete Category';

            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

//    public function getCategory($id) {
//        $nominations = Category::where('contest_id', $id)->get()->toArray();
//        if (count($nominations) > 0) {
//            $message = 'Success';
//            return Response()->json(ResponseManager::getResult($nominations, 10, $message));
//        } else {
//            $message = 'No nomination found for category';
//            return Response()->json(ResponseManager::getError('', 10, $message));
//        }
//    }

    public function categoryDropDownData()
    {

        $cat_id = Input::get('cat_id');


        $subcategories = ShopSubcategory::where('category_id', '=', $cat_id)
                                        ->orderBy('name', 'asc')
                                        ->get();

        return Response::json($subcategories);


    }
}