<?php

namespace App\Http\Controllers\AdminShop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminShopController extends Controller
{
    protected $is_ajax = false;
    protected $default_shop_products_route;
    protected $default_shop_categories_route;
    protected $default_shop_subcategories_route;
    protected $default_shop_orders_route;

    public function __construct(Request $request)
    {
//        $this->middleware('shopVendor');
        $this->is_ajax = $request->ajax();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $page_data = [
            'default_shop_products_route' => ( $this->default_shop_products_route )
                ? $this->default_shop_products_route
                : route('shop.admin.products.index'),

            'default_shop_categories_route' => ( $this->default_shop_categories_route )
                ? $this->default_shop_categories_route
                : route('shop.admin.category.index'),

            'default_shop_subcategories_route' => ( $this->default_shop_subcategories_route )
                ? $this->default_shop_subcategories_route
                : route('shop.admin.subcategory.index'),
            'default_shop_orders_route' => ( $this->default_shop_orders_route )
                ? $this->default_shop_orders_route
                : route('shop.admin.orders.index'),

            'hide_layout' => true,
        ];

        return $this->view('shop.admin.dashboard-tabs', $page_data);
    }

    /**
     * @param       $view
     * @param array $data
     *
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function view($view, $data = [ ])
    {
        if ($this->is_ajax) {
            return view($view, $data);
        }

        return view('layouts.non-ajax-layout', $data)
            ->with('content', $view);
    }

}
