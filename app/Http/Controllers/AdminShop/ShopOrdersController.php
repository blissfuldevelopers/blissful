<?php

namespace App\Http\Controllers\AdminShop;

use ResponseManager;
use Illuminate\Http\Request;
use App\Models\ShopProduct;
use App\Models\ShopOrder;
use Auth;

class ShopOrdersController extends AdminShopController
{
    protected $is_ajax = false;

    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $orders = ShopOrder::with('details')->orderBy('id', 'desc')->paginate(15);

//        $products = ShopProduct::filterByUserRole($products);

        return $this->view('shop.admin.orders.order-index', ['orders' => $orders]);
    }


    public function show($id)
    {
        $order = ShopOrder::with('details')->find($id);
        if ($order) {
            return $this->view('shop.admin.orders.order-show', ['order' => $order]);
        } else {
            $message = 'Invalid Product';

            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {

        $delete = ShopOrder::where('id', $id)->delete();
        if ($delete) {
            $message = 'Delete Product Success';
            return redirect()->route('shop.admin.orders.index');
//            return Response()->json(ResponseManager::getResult($delete, 10, $message));
        } else {
            $message = 'Error while delete Product';

            return Response()->json(ResponseManager::getError('', 10, $message));
        }
    }


}
