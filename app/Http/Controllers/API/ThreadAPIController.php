<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateThreadAPIRequest;
use App\Http\Requests\API\UpdateThreadAPIRequest;
use Cmgmyr\Messenger\Models\Thread;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use App\Repositories\ThreadRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Controller\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;


/**
 * Class ThreadController
 *
 * @package App\Http\Controllers\API
 */
class ThreadAPIController extends AppBaseController
{
    /** @var  ThreadRepository */
    private $threadRepository;

    function __construct(ThreadRepository $threadRepo)
    {

        $this->threadRepository = $threadRepo;
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @SWG\Get(
     *      path="/threads",
     *      summary="Get a listing of the Threads.",
     *      tags={"Thread"},
     *      description="Get all Threads",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Thread")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->threadRepository->pushCriteria(new RequestCriteria($request));
        $this->threadRepository->pushCriteria(new LimitOffsetCriteria($request));
        $threads = $this->threadRepository->paginate(10);

        return $this->sendResponse($threads->toArray(), "Threads retrieved successfully");
    }

    /**
     * @param CreateThreadAPIRequest $request
     *
     * @return Response
     *
     * @SWG\Post(
     *      path="/threads",
     *      summary="Store a newly created Thread in storage",
     *      tags={"Thread"},
     *      description="Store Thread",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Thread that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Thread")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Thread"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateThreadAPIRequest $request)
    {
        $input = $request->all();

        $threads = $this->threadRepository->create($input);

        return $this->sendResponse($threads->toArray(), "Thread saved successfully");
    }

    /**
     * @param int $id
     *
     * @return Response
     *
     * @SWG\Get(
     *      path="/threads/{id}",
     *      summary="Display the specified Thread",
     *      tags={"Thread"},
     *      description="Get Thread",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Thread",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Thread"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Thread $thread */
        $thread = $this->threadRepository->find($id);

        if (empty( $thread )) {
            return Response::json(ResponseUtil::makeError("Thread not found"), 400);
        }

        return $this->sendResponse($thread->toArray(), "Thread retrieved successfully");
    }

    /**
     * @param int                    $id
     * @param UpdateThreadAPIRequest $request
     *
     * @return Response
     *
     * @SWG\Put(
     *      path="/threads/{id}",
     *      summary="Update the specified Thread in storage",
     *      tags={"Thread"},
     *      description="Update Thread",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Thread",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Thread that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Thread")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Thread"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateThreadAPIRequest $request)
    {
        $input = $request->all();

        /** @var Thread $thread */
        $thread = $this->threadRepository->find($id);

        if (empty( $thread )) {
            return Response::json(ResponseUtil::makeError("Thread not found"), 400);
        }

        $thread = $this->threadRepository->update($input, $id);

        return $this->sendResponse($thread->toArray(), "Thread updated successfully");
    }

    /**
     * @param int $id
     *
     * @return Response
     *
     * @SWG\Delete(
     *      path="/threads/{id}",
     *      summary="Remove the specified Thread from storage",
     *      tags={"Thread"},
     *      description="Delete Thread",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Thread",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Thread $thread */
        $thread = $this->threadRepository->find($id);

        if (empty( $thread )) {
            return Response::json(ResponseUtil::makeError("Thread not found"), 400);
        }

        $thread->delete();

        return $this->sendResponse($id, "Thread deleted successfully");
    }
}
