<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCronMessageAPIRequest;
use App\Http\Requests\API\UpdateCronMessageAPIRequest;
use App\Libraries\blissful\blissnotify;
use App\Models\CronMessage;
use App\Repositories\CronMessageRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Controller\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Libraries\blissful\blissmessage;

/**
 * Class CronMessageController
 * @package App\Http\Controllers\API
 */

class CronMessageAPIController extends AppBaseController
{
    /** @var  CronMessageRepository */
    private $cronMessageRepository;

    public function __construct(CronMessageRepository $cronMessageRepo)
    {
        $this->cronMessageRepository = $cronMessageRepo;
    }



    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/cronMessages",
     *      summary="Get a listing of the CronMessages.",
     *      tags={"CronMessage"},
     *      description="Get all CronMessages",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CronMessage")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->cronMessageRepository->pushCriteria(new RequestCriteria($request));
        $this->cronMessageRepository->pushCriteria(new LimitOffsetCriteria($request));
        $cronMessages = $this->cronMessageRepository->paginate(10);

        return $this->sendResponse($cronMessages->toArray(), 'CronMessages retrieved successfully');
    }

    /**
     * @param CreateCronMessageAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/cronMessages",
     *      summary="Store a newly created CronMessage in storage",
     *      tags={"CronMessage"},
     *      description="Store CronMessage",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CronMessage that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CronMessage")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CronMessage"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCronMessageAPIRequest $request)
    {
        $input = $request->all();

        $cronMessages = $this->cronMessageRepository->create($input);

        return $this->sendResponse($cronMessages->toArray(), 'CronMessage saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/cronMessages/{id}",
     *      summary="Display the specified CronMessage",
     *      tags={"CronMessage"},
     *      description="Get CronMessage",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CronMessage",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CronMessage"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CronMessage $cronMessage */
        $cronMessage = $this->cronMessageRepository->find($id);

        if (empty($cronMessage)) {
            return Response::json(ResponseUtil::makeError('CronMessage not found'), 400);
        }

        return $this->sendResponse($cronMessage->toArray(), 'CronMessage retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCronMessageAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/cronMessages/{id}",
     *      summary="Update the specified CronMessage in storage",
     *      tags={"CronMessage"},
     *      description="Update CronMessage",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CronMessage",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CronMessage that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CronMessage")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CronMessage"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCronMessageAPIRequest $request)
    {
        $input = $request->all();

        /** @var CronMessage $cronMessage */
        $cronMessage = $this->cronMessageRepository->find($id);

        if (empty($cronMessage)) {
            return Response::json(ResponseUtil::makeError('CronMessage not found'), 400);
        }

        $cronMessage = $this->cronMessageRepository->update($input, $id);

        return $this->sendResponse($cronMessage->toArray(), 'CronMessage updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/cronMessages/{id}",
     *      summary="Remove the specified CronMessage from storage",
     *      tags={"CronMessage"},
     *      description="Delete CronMessage",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CronMessage",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var CronMessage $cronMessage */
        $cronMessage = $this->cronMessageRepository->find($id);

        if (empty($cronMessage)) {
            return Response::json(ResponseUtil::makeError('CronMessage not found'), 400);
        }

        $cronMessage->delete();

        return $this->sendResponse($id, 'CronMessage deleted successfully');
    }
}
