<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCronScheduleAPIRequest;
use App\Http\Requests\API\UpdateCronScheduleAPIRequest;
use App\Models\CronSchedule;
use App\Repositories\CronScheduleRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Controller\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CronScheduleController
 * @package App\Http\Controllers\API
 */

class CronScheduleAPIController extends AppBaseController
{
    /** @var  CronScheduleRepository */
    private $cronScheduleRepository;

    public function __construct(CronScheduleRepository $cronScheduleRepo)
    {
        $this->cronScheduleRepository = $cronScheduleRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/cronSchedules",
     *      summary="Get a listing of the cron_schedules.",
     *      tags={"CronSchedule"},
     *      description="Get all CronSchedules",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CronSchedule")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->cronScheduleRepository->pushCriteria(new RequestCriteria($request));
        $this->cronScheduleRepository->pushCriteria(new LimitOffsetCriteria($request));
        $cronSchedules = $this->cronScheduleRepository->all();

        return $this->sendResponse($cronSchedules->toArray(), 'CronSchedules retrieved successfully');
    }

    /**
     * @param CreateCronScheduleAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/cronSchedules",
     *      summary="Store a newly created CronSchedule in storage",
     *      tags={"CronSchedule"},
     *      description="Store CronSchedule",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CronSchedule that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CronSchedule")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CronSchedule"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCronScheduleAPIRequest $request)
    {
        $input = $request->all();

        $cronSchedules = $this->cronScheduleRepository->create($input);

        return $this->sendResponse($cronSchedules->toArray(), 'CronSchedule saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/cronSchedules/{id}",
     *      summary="Display the specified CronSchedule",
     *      tags={"CronSchedule"},
     *      description="Get CronSchedule",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CronSchedule",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CronSchedule"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CronSchedule $cronSchedule */
        $cronSchedule = $this->cronScheduleRepository->find($id);

        if (empty($cronSchedule)) {
            return Response::json(ResponseUtil::makeError('CronSchedule not found'), 400);
        }

        return $this->sendResponse($cronSchedule->toArray(), 'CronSchedule retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCronScheduleAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/cronSchedules/{id}",
     *      summary="Update the specified CronSchedule in storage",
     *      tags={"CronSchedule"},
     *      description="Update CronSchedule",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CronSchedule",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CronSchedule that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CronSchedule")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CronSchedule"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCronScheduleAPIRequest $request)
    {
        $input = $request->all();

        /** @var CronSchedule $cronSchedule */
        $cronSchedule = $this->cronScheduleRepository->find($id);

        if (empty($cronSchedule)) {
            return Response::json(ResponseUtil::makeError('CronSchedule not found'), 400);
        }

        $cronSchedule = $this->cronScheduleRepository->update($input, $id);

        return $this->sendResponse($cronSchedule->toArray(), 'CronSchedule updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/cronSchedules/{id}",
     *      summary="Remove the specified CronSchedule from storage",
     *      tags={"CronSchedule"},
     *      description="Delete CronSchedule",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CronSchedule",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var CronSchedule $cronSchedule */
        $cronSchedule = $this->cronScheduleRepository->find($id);

        if (empty($cronSchedule)) {
            return Response::json(ResponseUtil::makeError('CronSchedule not found'), 400);
        }

        $cronSchedule->delete();

        return $this->sendResponse($id, 'CronSchedule deleted successfully');
    }
}
