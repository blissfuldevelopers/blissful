<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCronRecipientAPIRequest;
use App\Http\Requests\API\UpdateCronRecipientAPIRequest;
use App\Models\CronRecipient;
use App\Repositories\CronRecipientRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Controller\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CronRecipientController
 * @package App\Http\Controllers\API
 */

class CronRecipientAPIController extends AppBaseController
{
    /** @var  CronRecipientRepository */
    private $cronRecipientRepository;

    public function __construct(CronRecipientRepository $cronRecipientRepo)
    {
        $this->cronRecipientRepository = $cronRecipientRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/cronRecipients",
     *      summary="Get a listing of the CronRecipients.",
     *      tags={"CronRecipient"},
     *      description="Get all CronRecipients",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CronRecipient")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->cronRecipientRepository->pushCriteria(new RequestCriteria($request));
        $this->cronRecipientRepository->pushCriteria(new LimitOffsetCriteria($request));
        $cronRecipients = $this->cronRecipientRepository->all();

        return $this->sendResponse($cronRecipients->toArray(), 'CronRecipients retrieved successfully');
    }

    /**
     * @param CreateCronRecipientAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/cronRecipients",
     *      summary="Store a newly created CronRecipient in storage",
     *      tags={"CronRecipient"},
     *      description="Store CronRecipient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CronRecipient that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CronRecipient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CronRecipient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCronRecipientAPIRequest $request)
    {
        $input = $request->all();

        $cronRecipients = $this->cronRecipientRepository->create($input);

        return $this->sendResponse($cronRecipients->toArray(), 'CronRecipient saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/cronRecipients/{id}",
     *      summary="Display the specified CronRecipient",
     *      tags={"CronRecipient"},
     *      description="Get CronRecipient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CronRecipient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CronRecipient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CronRecipient $cronRecipient */
        $cronRecipient = $this->cronRecipientRepository->find($id);

        if (empty($cronRecipient)) {
            return Response::json(ResponseUtil::makeError('CronRecipient not found'), 400);
        }

        return $this->sendResponse($cronRecipient->toArray(), 'CronRecipient retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCronRecipientAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/cronRecipients/{id}",
     *      summary="Update the specified CronRecipient in storage",
     *      tags={"CronRecipient"},
     *      description="Update CronRecipient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CronRecipient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CronRecipient that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CronRecipient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CronRecipient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCronRecipientAPIRequest $request)
    {
        $input = $request->all();

        /** @var CronRecipient $cronRecipient */
        $cronRecipient = $this->cronRecipientRepository->find($id);

        if (empty($cronRecipient)) {
            return Response::json(ResponseUtil::makeError('CronRecipient not found'), 400);
        }

        $cronRecipient = $this->cronRecipientRepository->update($input, $id);

        return $this->sendResponse($cronRecipient->toArray(), 'CronRecipient updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/cronRecipients/{id}",
     *      summary="Remove the specified CronRecipient from storage",
     *      tags={"CronRecipient"},
     *      description="Delete CronRecipient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CronRecipient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var CronRecipient $cronRecipient */
        $cronRecipient = $this->cronRecipientRepository->find($id);

        if (empty($cronRecipient)) {
            return Response::json(ResponseUtil::makeError('CronRecipient not found'), 400);
        }

        $cronRecipient->delete();

        return $this->sendResponse($id, 'CronRecipient deleted successfully');
    }
}
