<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCronTaskAPIRequest;
use App\Http\Requests\API\UpdateCronTaskAPIRequest;
use App\Models\CronTask;
use App\Repositories\CronTaskRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Controller\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CronTaskController
 * @package App\Http\Controllers\API
 */

class CronTaskAPIController extends AppBaseController
{
    /** @var  CronTaskRepository */
    private $cronTaskRepository;

    public function __construct(CronTaskRepository $cronTaskRepo)
    {
        $this->cronTaskRepository = $cronTaskRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/cronTasks",
     *      summary="Get a listing of the cron_tasks.",
     *      tags={"CronTask"},
     *      description="Get all CronTasks",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CronTask")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->cronTaskRepository->pushCriteria(new RequestCriteria($request));
        $this->cronTaskRepository->pushCriteria(new LimitOffsetCriteria($request));
        $cronTasks = $this->cronTaskRepository->all();

        return $this->sendResponse($cronTasks->toArray(), 'CronTasks retrieved successfully');
    }

    /**
     * @param CreateCronTaskAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/cronTasks",
     *      summary="Store a newly created CronTask in storage",
     *      tags={"CronTask"},
     *      description="Store CronTask",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CronTask that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CronTask")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CronTask"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCronTaskAPIRequest $request)
    {
        $input = $request->all();

        $cronTasks = $this->cronTaskRepository->create($input);

        return $this->sendResponse($cronTasks->toArray(), 'CronTask saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/cronTasks/{id}",
     *      summary="Display the specified CronTask",
     *      tags={"CronTask"},
     *      description="Get CronTask",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CronTask",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CronTask"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CronTask $cronTask */
        $cronTask = $this->cronTaskRepository->find($id);

        if (empty($cronTask)) {
            return Response::json(ResponseUtil::makeError('CronTask not found'), 400);
        }

        return $this->sendResponse($cronTask->toArray(), 'CronTask retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCronTaskAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/cronTasks/{id}",
     *      summary="Update the specified CronTask in storage",
     *      tags={"CronTask"},
     *      description="Update CronTask",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CronTask",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CronTask that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CronTask")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CronTask"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCronTaskAPIRequest $request)
    {
        $input = $request->all();

        /** @var CronTask $cronTask */
        $cronTask = $this->cronTaskRepository->find($id);

        if (empty($cronTask)) {
            return Response::json(ResponseUtil::makeError('CronTask not found'), 400);
        }

        $cronTask = $this->cronTaskRepository->update($input, $id);

        return $this->sendResponse($cronTask->toArray(), 'CronTask updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/cronTasks/{id}",
     *      summary="Remove the specified CronTask from storage",
     *      tags={"CronTask"},
     *      description="Delete CronTask",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CronTask",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var CronTask $cronTask */
        $cronTask = $this->cronTaskRepository->find($id);

        if (empty($cronTask)) {
            return Response::json(ResponseUtil::makeError('CronTask not found'), 400);
        }

        $cronTask->delete();

        return $this->sendResponse($id, 'CronTask deleted successfully');
    }
}
