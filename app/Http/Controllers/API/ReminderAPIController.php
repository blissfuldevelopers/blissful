<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateReminderAPIRequest;
use App\Http\Requests\API\UpdateReminderAPIRequest;
use App\Models\Reminder;
use App\Repositories\ReminderRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Controller\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Libraries\blissful\blissreminder;
/**
 * Class ReminderController
 * @package App\Http\Controllers\API
 */

class ReminderAPIController extends AppBaseController
{
    /** @var  ReminderRepository */
    private $reminderRepository;

    public function __construct(ReminderRepository $reminderRepo)
    {
        $this->reminderRepository = $reminderRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reminders",
     *      summary="Get a listing of the Reminders.",
     *      tags={"Reminder"},
     *      description="Get all Reminders",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Reminder")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->reminderRepository->pushCriteria(new RequestCriteria($request));
        $this->reminderRepository->pushCriteria(new LimitOffsetCriteria($request));
        $reminders = $this->reminderRepository->all();

        return $this->sendResponse($reminders->toArray(), 'Reminders retrieved successfully');
    }

    /**
     * @param CreateReminderAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/reminders",
     *      summary="Store a newly created Reminder in storage",
     *      tags={"Reminder"},
     *      description="Store Reminder",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Reminder that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Reminder")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Reminder"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateReminderAPIRequest $request)
    {
        $input = $request->all();

        $reminders = $this->reminderRepository->create($input);

        return $this->sendResponse($reminders->toArray(), 'Reminder saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/reminders/{id}",
     *      summary="Display the specified Reminder",
     *      tags={"Reminder"},
     *      description="Get Reminder",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Reminder",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Reminder"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Reminder $reminder */
        $reminder = $this->reminderRepository->find($id);

        if (empty($reminder)) {
            return Response::json(ResponseUtil::makeError('Reminder not found'), 400);
        }

        return $this->sendResponse($reminder->toArray(), 'Reminder retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateReminderAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/reminders/{id}",
     *      summary="Update the specified Reminder in storage",
     *      tags={"Reminder"},
     *      description="Update Reminder",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Reminder",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Reminder that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Reminder")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Reminder"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateReminderAPIRequest $request)
    {
        $input = $request->all();

        /** @var Reminder $reminder */
        $reminder = $this->reminderRepository->find($id);

        if (empty($reminder)) {
            return Response::json(ResponseUtil::makeError('Reminder not found'), 400);
        }

        $reminder = $this->reminderRepository->update($input, $id);

        return $this->sendResponse($reminder->toArray(), 'Reminder updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/reminders/{id}",
     *      summary="Remove the specified Reminder from storage",
     *      tags={"Reminder"},
     *      description="Delete Reminder",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Reminder",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Reminder $reminder */
        $reminder = $this->reminderRepository->find($id);

        if (empty($reminder)) {
            return Response::json(ResponseUtil::makeError('Reminder not found'), 400);
        }

        $reminder->delete();

        return $this->sendResponse($id, 'Reminder deleted successfully');
    }
}
