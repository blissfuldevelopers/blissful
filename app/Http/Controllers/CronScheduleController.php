<?php

namespace App\Http\Controllers;

use App\DataTables\CronScheduleDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCronScheduleRequest;
use App\Http\Requests\UpdateCronScheduleRequest;
use App\Repositories\CronScheduleRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;
use Illuminate\Http\Request;

class CronScheduleController extends AppBaseController
{
    /** @var  CronScheduleRepository */
    private $cronScheduleRepository;
    protected $is_ajax = false;

    public function __construct(CronScheduleRepository $cronScheduleRepo, Request $request)
    {
        $this->is_ajax = $request->ajax();
        $this->cronScheduleRepository = $cronScheduleRepo;
    }

    /**
     * Display a listing of the CronSchedule.
     *
     * @param CronScheduleDataTable $cronScheduleDataTable
     *
     * @return Response
     */
    public function index(CronScheduleDataTable $cronScheduleDataTable)
    {
        return $cronScheduleDataTable->render('cron_schedules.index');
    }

    /**
     * Show the form for creating a new CronSchedule.
     *
     * @return Response
     */
    public function create()
    {
        return $this->view('cron_schedules.create');
    }

    /**
     * Store a newly created CronSchedule in storage.
     *
     * @param CreateCronScheduleRequest $request
     *
     * @return Response
     */
    public function store(CreateCronScheduleRequest $request)
    {
        $input = $request->all();

        $cronSchedule = $this->cronScheduleRepository->create($input);

        Flash::success('CronSchedule saved successfully.');

        return redirect(route('cron_schedules.index'));
    }

    /**
     * Display the specified CronSchedule.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cronSchedule = $this->cronScheduleRepository->findWithoutFail($id);

        if (empty( $cronSchedule )) {
            Flash::error('CronSchedule not found');

            return redirect(route('cron_schedules.index'));
        }

        return $this->view('cron_schedules.show')->with('cronSchedule', $cronSchedule);
    }

    /**
     * Show the form for editing the specified CronSchedule.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cronSchedule = $this->cronScheduleRepository->findWithoutFail($id);

        if (empty( $cronSchedule )) {
            Flash::error('CronSchedule not found');

            return redirect(route('cron_schedules.index'));
        }

        return $this->view('cron_schedules.edit')->with('cronSchedule', $cronSchedule);
    }

    /**
     * Update the specified CronSchedule in storage.
     *
     * @param  int                      $id
     * @param UpdateCronScheduleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCronScheduleRequest $request)
    {
        $cronSchedule = $this->cronScheduleRepository->findWithoutFail($id);

        if (empty( $cronSchedule )) {
            Flash::error('CronSchedule not found');

            return redirect(route('cron_schedules.index'));
        }

        $cronSchedule = $this->cronScheduleRepository->update($request->all(), $id);

        Flash::success('CronSchedule updated successfully.');

        return redirect(route('cron_schedules.index'));
    }

    /**
     * Remove the specified CronSchedule from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $cronSchedule = $this->cronScheduleRepository->findWithoutFail($id);

        if (empty( $cronSchedule )) {
            Flash::error('CronSchedule not found');

            return redirect(route('cron_schedules.index'));
        }

        $this->cronScheduleRepository->delete($id);

        Flash::success('CronSchedule deleted successfully.');

        return redirect(route('cron_schedules.index'));
    }

    /**
     * @param       $view
     * @param array $data
     *
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $data = [ ])
    {
        if ($this->is_ajax) {
            return view($view, $data);
        }

        return view('layouts.non-ajax-layout', $data)
            ->with('content', $view);

    }
}
