<?php

namespace App\Http\Controllers;


use App\Classification;
use Illuminate\Http\Request;
use App\Logic\Mailers\UserMailer;
use App\Activity;
use Auth;
use Mixpanel;
use App\Subscriptions;
use Validator;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use App\Models\ShopCategory;
use App\Models\ShopProduct;
use App\Post;
use Carbon\Carbon;
use Jenssegers\Agent\Agent;
use DB;
use App\User;

class PagesController extends Controller
{

    protected $userMailer;
    protected $is_ajax = false;

    public function __construct(UserMailer $userMailer, Request $request)
    {
        $this->userMailer = $userMailer;
        $this->is_ajax = $request->ajax();
    }

    public function index()
    {
        $classifications = Classification::whereNull('parent_id')->orderBy('priority', 'ASC')->get();

        $top_vendors = User::with('classifications')->where(function ($q) {
            $q->where('verified', '=', 1)
                ->orWhere('added_by_admin', '=', 1);
        })->where('test_account', '=', 0)->orderBy('site_rank', 'ASC')->orderBy('last_login', 'DESC')->limit(3)->get();

        
        $vendors = User::with('classifications')->where(function ($q) {
            $q->where('verified', '=', 1)
                ->orWhere('added_by_admin', '=', 1);
        })->where('test_account', '=', 0)
            ->whereHas('classifications', function ($query) {
                $query->where('slug', '=', 'venue');
            })->orderBy('site_rank', 'ASC')->orderBy('last_login', 'DESC')->limit(8)->get();
        
        // $shop_products = ShopProduct::fetchShopProducts()
        //                             ->where('type', 1)
        //                             ->whereHas('category', function ($query) {
        //                                 $query->where('package', 0);
        //                             })->get();
        // $filtered_products = ShopProduct::filterByUserRole($shop_products);

        // if ($filtered_products) {
        //     shuffle($filtered_products);

        //     if (count($filtered_products) == 1) {
        //         $products[] = $filtered_products [ 0 ];
        //     }
        //     else {
        //         $array_rand_input = ( count($filtered_products) < 5 ) ? count($filtered_products) : 5;
        //         $product_keys = array_rand($filtered_products, $array_rand_input);
        //         foreach ($product_keys as $key) {
        //             $products[] = $filtered_products[ $key ];
        //         }
        //     }
        // }

        // $packages = ShopProduct::fetchShopProducts()
        //                        ->whereHas('category', function ($query) {
        //                            $query->where('package', 1);
        //                        })
        //                        ->orderBy(DB::raw('RAND()'))
        //                        ->limit(3)
        //                        ->get();

        // $package_category = ShopCategory::where('package', 1)
        //                                 ->orderBy(DB::raw('RAND()'))
        //                                 ->limit(1)
        //                                 ->get();

        // $posts = Post::with('categories')
        //              ->where('published_at', '<=', Carbon::now())
        //              ->where('is_draft', 0)
        //              ->orderBy('published_at', 'desc')
        //              ->limit(2)
        //              ->get();

        Activity::create([
                             'subject_id'   => 1,
                             'subject_type' => 'App\Page',
                             'name'         => 'read',
                             'user_id'      => ( Auth::check() ) ? Auth::user()->id : 0,
                         ]);


        return View('page.index', compact('classifications', 'top_vendors','vendors'));
    }

    public function getContact()
    {
        Activity::create([
                             'subject_id'   => 2,
                             'subject_type' => 'App\Page',
                             'name'         => 'read',
                             'user_id'      => ( Auth::check() ) ? Auth::user()->id : 0,
                         ]);

        return view('pages.contact');
    }

    public function getContactAgain()
    {
        return redirect('contact');
    }

    public function sendContactInfo(Request $request)
    {
        $data = $request->only('name', 'email', 'phone');

        $data[ 'messageLines' ] = explode("\n", $request->get('message'));

        $this->userMailer->contact($data);

        return back()
            ->withSuccess('Thank you for your message. It has been sent.');
    }

    public function getTerms()
    {
        Activity::create([
                             'subject_id'   => 4,
                             'subject_type' => 'App\Page',
                             'name'         => 'read',
                             'user_id'      => ( Auth::check() ) ? Auth::user()->id : 0,
                         ]);

        return view('pages.terms');
    }

    public function getAbout()
    {
        Activity::create([
                             'subject_id'   => 3,
                             'subject_type' => 'App\Page',
                             'name'         => 'read',
                             'user_id'      => ( Auth::check() ) ? Auth::user()->id : 0,
                         ]);

        return view('pages.about');
    }

    public function getPolicy()
    {
        Activity::create([
                             'subject_id'   => 5,
                             'subject_type' => 'App\Page',
                             'name'         => 'read',
                             'user_id'      => ( Auth::check() ) ? Auth::user()->id : 0,
                         ]);

        return view('pages.policy');
    }

    public function getAdvertise()
    {
        // Activity::create([
        //     'subject_id'   => 2,
        //     'subject_type' => 'App\Page',
        //     'name'         => 'read',
        //     'user_id'      => (Auth::check()) ? Auth::user()->id : 0,
        // ]);

        return $this->view('pages.advertise');
    }

    public function getReviews()
    {
        // Activity::create([
        //     'subject_id'   => 2,
        //     'subject_type' => 'App\Page',
        //     'name'         => 'read',
        //     'user_id'      => (Auth::check()) ? Auth::user()->id : 0,
        // ]);

        return $this->view('pages.reviews');
    }

    public function landing($slug)
    {


        $cat = ShopCategory::wherecategory_slug($slug)->first();
        if (is_null($cat)) {
            return View('landings.landing-page');
        }
        $products = ShopProduct::fetchShopProductQuery()
                               ->where('category_id', $cat->id)
                               ->orderBy('created_at', 'DESC')
                               ->get();

        return View('landings.honeymoons-landing-page')->with('cat', $cat)->with('products', $products);


    }

    public function saveSubscription(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, Subscriptions::$input_rules);
        $subscription = ShopProduct::where('id', $input[ 'product_id' ])->first();

        $input[ 'package_name' ] = $subscription->name;
        $input[ 'package_category' ] = $subscription->category->name;
        $subscription = Subscriptions::create([
                                                  'user_name'  => $input[ 'user_name' ],
                                                  'email'      => $input[ 'email' ],
                                                  'product_id' => $input[ 'product_id' ],
                                                  'number'     => $input[ 'number' ],
                                              ]);
        $this->userMailer->Subscriptions($input);

        return "Your request has been received, we will contact you soon :)";
    }

    public function viewSubscriptions()
    {

        $to = ( Input::has('to') ) ? date('Y-m-d H:i:s', strtotime(Input::get('to'))) : date('Y-m-d H:i:s', strtotime('now'));

        if (Input::has('from')) {

            $from = ( Input::has('from') ) ? date('Y-m-d H:i:s', strtotime(Input::get('from'))) : date('Y-m-d H:i:s', strtotime('yesterday'));
            $subscriptions = Subscriptions::whereBetween('created_at', array( $from, $to ))->latest()->paginate(10);

            return $this->view('admin.activities.honeymoon-subscriptions')->with('subscriptions', $subscriptions)->with('to', $to)->with('from', $from);
        }
        else {
            $subscriptions = Subscriptions::latest()->paginate(10);
        }

        return $this->view('admin.activities.honeymoon-subscriptions')->with('subscriptions', $subscriptions)->with('to', $to);
    }

    private function view($view, $data = [ ])
    {
        if ($this->is_ajax) {

            return view($view, $data);
        }

        return view('layouts.non-ajax-layout', $data)
            ->with('content', $view);

    }
}