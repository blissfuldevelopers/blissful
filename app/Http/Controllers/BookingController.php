<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\User;
use App\Jobs\SendEmail;
use Illuminate\Http\Request;

use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BookingController extends Controller
{

    protected $is_ajax = false;

    public function __construct(Request $request)
    {
        $this->is_ajax = $request->ajax();
    }
    public function calendar()
    {
        return view('bookings.index');
    }

    public function api()
    {
        if (Auth::user()->hasuserRole('administrator')) {
            $events = DB::table('bookings')->select('id', 'name', 'title', 'start_time as start', 'end_time as end')->get();
        } elseif (Auth::user()->hasuserRole('vendor')) {
            $events = DB::table('bookings')->select('id', 'name', 'title', 'start_time as start', 'end_time as end')->where('vendor_id', Auth::user()->id)->get();
        } else {
            $events = DB::table('bookings')->select('id', 'name', 'title', 'start_time as start', 'end_time as end')->where('user_id', Auth::user()->id)->get();
        }

        foreach ($events as $event) {
            $event->title = $event->title . ' - ' . $event->name;
            $event->url = url('booking/events/' . $event->id);
        }
        return $events;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->hasuserRole('administrator')) {
            $data = [
                'events' => Booking::orderBy('start_time')->get()
            ];
        } elseif (Auth::user()->hasuserRole('vendor')) {
            $data = [
                'events' => Booking::where('vendor_id', Auth::user()->id)->orderBy('start_time')->get()
            ];
        } else {
            $data = [
                'events' => Booking::where('user_id', Auth::user()->id)->orderBy('start_time')->get()
            ];
        }

        return view('bookings.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bookings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->all();
        $this->validate($request, [
            'name' => 'required|min:5|max:15',
            'title' => 'required|min:5|max:100',
            'email' => 'required|email',
            'phone' => 'required'
        ]);

        $user = User::where('email',$request->input('email'))->first();
        $vendor = User::where('id',$request->input('vendor_id'))->first();

        $user_id = isset($user->id) ? $user->id : 0;

        $time = explode(" - ", $request->input('time'));

        $booking = new Booking;
        $booking->name = $request->input('name');
        $booking->vendor_id = $request->input('vendor_id');
        $booking->phone = $request->input('phone');
        $booking->email = $request->input('email');
        $booking->user_id = $user_id;
        $booking->booking_ref_no = "B" . date('ymdHis') . str_pad(1906, 3);
        $booking->booking_status = 1;
        $booking->booking_status_description = 'Pending';
        $booking->title = $request->input('title');
        $booking->start_time = $this->change_date_format($time[0]);
        $booking->end_time = $this->change_date_format($time[1]);
        $booking->save();

        //check if we have the phone
        $phone = isset($user->profile->phone) && !empty($user->profile->phone) && $user->profile->phone != $request->input('phone') ? $user->profile->phone.' / '.$request->input('phone') : $request->input('phone');
        $name = isset($user->first_name) ? $user->first_name : $request->input('name');

        $message = 'Details: <br> Name:'.$name.'<br> Email:'.$request->input('email').'<br> Phone:'.$phone.'<br>Venue:'.$vendor->first_name.'<br> Date:'.$this->change_date_format($time[0]). 'To: '.$this->change_date_format($time[1]);

        $data = [
            'email'        => 'happy@blissful.co.ke',
            'title'   => 'New venue booking for '.$vendor->first_name,
            'message'      => $message,
        ];

        //dispatch email to admin
        dispatch( New SendEmail($data));

        $request->session()->flash('success', 'The event was successfully saved!');

        return view('bookings.success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Booking::findOrFail($id);

        $first_date = new DateTime($event->start_time);
        $second_date = new DateTime($event->end_time);
        $difference = $first_date->diff($second_date);

        $data = [
            'event' => $event,
            'duration' => $this->format_interval($difference)
        ];

        return view('bookings.view', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Booking::findOrFail($id);
        $event->start_time = $this->change_date_format_fullcalendar($event->start_time);
        $event->end_time = $this->change_date_format_fullcalendar($event->end_time);

        $data = [
            'event' => $event
        ];

        return view('bookings.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->has('status')) {

            $event = Booking::findOrFail($id);
            $event->booking_status = $request->input('status');
            if ($request->input('status') == 2) {
                $event->booking_status_description = 'Booked';
            } elseif ($request->input('status') == 3) {
                $event->booking_status_description = 'Declined';
            }
            $event->save();
            return redirect('booking/events/' . $event->id);
        } else {
            $this->validate($request, [
                'name' => 'required|min:5|max:15',
                'title' => 'required|min:5|max:100'
            ]);

            $time = explode(" - ", $request->input('time'));

            $event = Booking::findOrFail($id);
            $event->name = $request->input('name');
            $event->title = $request->input('title');
            $event->start_time = $this->change_date_format($time[0]);
            $event->end_time = $this->change_date_format($time[1]);
            $event->save();
            return redirect('booking/events');
        }
    }

    public function status_update(Request $request, $id)
    {
        $event = Booking::findOrFail($id);
        $event->booking_status = $request->input('name');
        $event->booking_status_description = $request->input('title');
        $event->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Booking::find($id);
        $event->delete();

        return redirect('booking/events');
    }

    public function change_date_format($date)
    {
        $time = DateTime::createFromFormat('d/m/Y H:i:s', $date);
        return $time->format('Y-m-d H:i:s');
    }

    public function change_date_format_fullcalendar($date)
    {
        $time = DateTime::createFromFormat('Y-m-d H:i:s', $date);
        return $time->format('d/m/Y H:i:s');
    }

    public function format_interval(\DateInterval $interval)
    {
        $result = "";
        if ($interval->y) {
            $result .= $interval->format("%y year(s) ");
        }
        if ($interval->m) {
            $result .= $interval->format("%m month(s) ");
        }
        if ($interval->d) {
            $result .= $interval->format("%d day(s) ");
        }
        if ($interval->h) {
            $result .= $interval->format("%h hour(s) ");
        }
        if ($interval->i) {
            $result .= $interval->format("%i minute(s) ");
        }
        if ($interval->s) {
            $result .= $interval->format("%s second(s) ");
        }

        return $result;
    }

    public function check_availability($id, Request $request)
    {
        $available = DB::table('bookings')
            ->select('id', 'name', 'title', 'start_time as start', 'end_time as end')
            ->where('start_time', '>=', date('Y-d-m H:i:s', strtotime($request->input('start_date'))))
            ->where('start_time', '<', date('Y-d-m H:i:s', strtotime($request->input('end_date'))))
            ->orWhere('start_time', '<=', date('Y-d-m H:i:s', strtotime($request->input('start_date'))))
            ->where('end_time', '>', date('Y-d-m H:i:s', strtotime($request->input('start_date'))))
            ->orWhere('end_time', '>', date('Y-d-m H:i:s', strtotime($request->input('start_date'))))
            ->where('end_time', '<=', date('Y-d-m H:i:s', strtotime($request->input('end_date'))))
            ->get();
        if (count($available) == 0) {
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }
}
