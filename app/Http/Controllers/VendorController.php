<?php
namespace App\Http\Controllers;

use App\Classification;
use App\Gallery;
use App\Models\Boost;
use App\Profile;
use App\Review;
use App\User;
use App\Locations;
use App\CreditsDebitCredit;
use Illuminate\Http\Request;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Post;
use App\Logic\Mailers\UserMailer;
use App\Activity;
use DB;
use Illuminate\Pagination\LengthAwarePaginator as paginator;
use Laracasts\Flash\Flash;
use Mixpanel;
use App\Ranking;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Option;

class VendorController extends Controller
{

    protected $userMailer;
    protected $default_jobs_route;

    public function __construct(UserMailer $userMailer)
    {
        $this->userMailer = $userMailer;
    }

    public function getHome()
    {

        return redirect('dashboard');
        $currentUserId = Auth::user()->id;
        $user = Auth::user();

        // All threads that user is participating in
        $threads = Thread::forUser($currentUserId)->orderBy('updated_at', 'DESC')->get();
        $classifications = Classification::lists('name', 'id');
        $galleries = Gallery::where('created_by', Auth::user()->id)->get();

        $bids = ($user->bid);
        $responseThread = [];

        foreach ($bids as $bid) {
            if ($bid->thread) {
                $responseThread[] = $bid->thread;
            }
        }
        if (!empty($responseThread)) {
            $threads = (!empty($responseThread)) ? $threads->diff($responseThread) : [];
        }

        $posts = Post::all();
        if (is_null($posts)) {
            $posts = [];
        }
        foreach ($posts as $post) {

        }
        $balance = CreditsDebitCredit::total_balance($currentUserId);

        $default_jobs_route = ($this->default_jobs_route) ? $this->default_jobs_route
            : ((!$user || !$user->can_bid_for_job)
                ? route('jobs.show', 'mine')
                : route('jobs.show', 'all'));

        return view('panels.vendor.home', compact('threads', 'currentUserId'))
            ->with('post', $post)->with('classifications', $classifications)
            ->with('profile', Profile::find($currentUserId))
            ->with('galleries', $galleries)
            ->with('default_jobs_route', $default_jobs_route)
            ->with('balance', $balance);
    }

    public function viewCategoryList()
    {

        $classifications = Classification::all();

        return View('pages.vendors')->with('classifications', $classifications);

    }

    public function viewClassification($slug)
    {
        $cities = locations::all();
        $border_color = ('#92BFE6');

        $classification = Classification::whereSlug($slug)->first();

        if (is_null($classification)) {
            abort(404);
        }

        $arrays = User::with('classifications')->where(function ($q) {
            $q->where('verified', '=', 1)
                ->orWhere('added_by_admin', '=', 1);
        })->where('test_account', '=', 0)
            ->whereHas('classifications', function ($query) use ($slug) {
                $query->where('slug', '=', $slug);
            })->orderBy('site_rank', 'ASC')->orderBy('last_login', 'DESC')->paginate(15);

        $featured = User::with('classifications')->where(function ($q) {
            $q->where('priority', '=', 1);
        })->where('test_account', '=', 0)
            ->whereHas('classifications', function ($query) use ($slug) {
                $query->where('slug', '=', $slug);
            })->orderBy('last_login', 'DESC')->paginate(12);


            Activity::create([
            'subject_id' => $classification->id,
            'subject_type' => 'App\Classification',
            'name' => 'viewed_categories',
            'user_id' => (Auth::check()) ? Auth::user()->id : 0,
        ]);
        $view = 'pages.vendor-classification';

        if ($slug == 'venue') {
            $view = 'pages.venue-vendors';
        }

        return View($view)
            ->with('cities', $cities)
            ->with('arrays', $arrays)
            ->with('featured', $featured)
            ->with('border_color', $border_color)
            ->with('classification', $classification);
    }

    public function showVendor($slug ,Request $request)
    {
        $input = $request->all();
        
        $date = '';
        if (array_key_exists('date', $input)) {
            $date = $input['date'];
        }
        $user = User::whereSlug($slug)->first();
        if (is_null($user)) {
            abort(404);
        }
        $created_by = $user->id;
        $portfolio = [];
        $galleries = Gallery::where('created_by', $created_by)->get();
        foreach ($galleries as $key => $gallery) {
            if ($gallery->name == 'Portfolio') {
                array_push($portfolio, $gallery);
                unset($galleries[$key]);
            }
        }
        $reviews = $user->profile->reviews()
            ->with('user')
            ->approved()
            ->notSpam()
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        Activity::create([
                             'subject_id'   => $user->id,
                             'subject_type' => 'App\Profile',
                             'name'         => 'viewed_vendor',
                             'user_id'      => ( Auth::check() ) ? Auth::user()->id : 0,
                         ]);
        if (!Auth::check() || Auth::user()->hasUserRole('user')) {
            $ranking = new Ranking;
            $ranking->storeRankingForVendor($user->id, 'profile viewed', 1);
        }
        $recommended= $user->UsersWithSimilarClassificationAs($user->id)->orderByRaw("RAND()")->limit(4)->get();

        $similars = [];
        
        foreach ($recommended as $value) {
            $similars [] = User::where('id', $value->id)->get();
        }
        $locations = Locations::all();

        $classifications = $user->classifications()->whereNull('parent_id')->get();
          
        return View('pages.vendor-show', array(
            'portfolio'         => $portfolio,
            'galleries'         => $galleries,
            'reviews'           => $reviews,
            'user'              => $user,
            'similars'          => $similars,
            'locations'         => $locations,
            'classifications'   => $classifications,
            'date'              => $date
        ));

    }

    public function multipleVendors($id)
    {
        $user = User::findorfail($id);
        $recommended = $user->UsersWithSimilarClassificationAs($user->id)->orderByRaw("RAND()")->limit(9)->get();
        foreach ($recommended as $value) {
            $multipleVendors [] = User::where('id', $value->id)->first();

        }

        return view('pages.multiple-vendors')->with('multipleVendors', $multipleVendors);

    }

    public function showVendorAgain($slug)
    {
        return redirect('vendors/' . $slug);
    }

    public function saveReview($slug)
    {

        $input = array(
            'comment' => Input::get('comment'),
            'rating' => Input::get('rating'),
            'slug' => Input::get('slug'),
        );

        // instantiate Rating model
        $review = new Review;

        // Validate that the user's input corresponds to the rules specified in the review model
        $validator = Validator::make($input, $review->getCreateRules());

        // If input passes validation - store the review in DB, otherwise return to product page with error message
        if ($validator->passes()) {
            $review->storeReviewForVendor($slug, $input['comment'], $input['rating']);

            return redirect('vendors/' . $slug . '#reviews-anchor')
                ->with('review_posted', true);
        }

        return redirect('vendors/' . $slug . '#reviews-anchor')
            ->withErrors($validator)
            ->withInput();
    }

    public function sendAdvert(Request $request)
    {
        $data = $request->only('name', 'email', 'phone');

        $this->userMailer->advert($data);
        $success = true;
        return view('pages.advertise')->with('success', $success);
    }

    public function reviewRequest()
    {
        $input = Input::all();
        $data = [
            'contact_name' => Auth::user()->first_name,
            'slug' => Auth::user()->slug,
            'email' => $input['email'],
            'subject' => 'Request for a review from ' . Auth::user()->first_name . ' on blissful.co.ke',
        ];
        $this->userMailer->emailReview($data['email'], $data);
        $success = true;
        return view('pages.reviews')->with('success', $success);
    }
    public function venueSearch(Request $request){

        $input = $request->all();
        $users = [];

        $user_query = User::join('profiles', 'users.id', '=', 'profiles.user_id')
            ->join('user_locations', 'users.id', '=', 'user_locations.user_id')
            ->where('test_account',0)
            ->whereHas('classifications', function ($query){
                    $query->where('slug', '=', 'venue');
        });

        if (array_key_exists('lat', $input)) {
            $user_query = $this->getNearby($user_query,$input['lat'], $input['lng']);
        }
        if (array_key_exists('type', $input)) {
            $id = $input['type'];

            if ($id != 'any') {
                $user_query = $user_query->whereHas('classifications', function ($query) use ($id){
                    $query->where('classifications.id', '=', $id);
                });
            }
            
        }
        if (array_key_exists('date', $input)) {
            //first convert date
            $date = str_replace('/', '-', $input['date']);
            $newDate = date("Y-m-d", strtotime($date));
            
            $user_query = $user_query->leftjoin('bookings', 'users.id', '=', 'bookings.vendor_id')
                                    ->whereNull('bookings.date')
                                    ->orWhere('bookings.date','!=',$newDate)
                                    ->groupBy('users.id');

        }
        if (array_key_exists('min_capacity', $input)) {
            $c_id = Option::where('name','Capacity')->first()->id;
            $min_cap = $input['min_capacity'];
            $max_cap = $input['max_capacity'];

            $user_query->whereHas('descriptions', function ($query) use ($c_id,$min_cap,$max_cap){
                $query->where('option_id', '=', $c_id);
                $query->whereBetween('value', [$min_cap, $max_cap]);
            });

        }
        if (array_key_exists('min_budget', $input)) {
            $c_id = Option::where('name','Price')->first()->id;
            $min_price = $input['min_budget'];
            $max_price = $input['max_budget'];

            $user_query->whereHas('descriptions', function ($query) use ($c_id,$min_price,$max_price){
                $query->where('option_id', '=', $c_id);
                $query->whereBetween('value', [$min_price, $max_price]);
            });

        }
        if (isset($user_query)) {
            $users = $user_query->get();
        }
        
       
        $vendors = $this->paginate_results($request,$users);

        return view('pages.partials.venue-search',compact('vendors','input'));
    }
    public function socialProof($type, $number, $cookie_name)
    {

        if ($number == "first" && $type == "vendor_show") {
            // unset($_COOKIE[$cookie_name]);
            // dd($_COOKIE[$cookie_name]);
            if (!isset($_COOKIE[$cookie_name])) {
                $number = (rand(2, 10));
                $message = "people have contacted this vendor in the last";
                $day_number = (rand(2, 10)) . " days";
                $time_set = date('Y-m-d H:i:s');
                $result_set = ($number . "," . $message . "," . $day_number . "," . $time_set . ",");
                setcookie($cookie_name, $result_set, time() + (86400), "/"); // 86400 = 1 day

            } else {
                $pieces = explode(",", $_COOKIE[$cookie_name]);
                $number = $pieces[0];
                $message = $pieces[1];
                $day_number = $pieces[2];
                $time_set = $pieces[3];
                $result_set = ($number . "," . $message . "," . $day_number . "," . $time_set . ",");
                if (time() - strtotime($time_set) > 10800) {
                    $number = $pieces[0] + (rand(0, 2));

                }
                setcookie($cookie_name, $result_set, time() + (86400), "/"); // 86400 = 1 day

            }
        }

        if ($number == "second" && $type == "vendor_show") {
            if (!isset($_COOKIE[$cookie_name])) {
                $number = (rand(20, 100));
                $message = "users have viewed this vendor in the last ";
                $day_number = (rand(2, 24)) . " hours";
                $time_set = date('Y-m-d H:i:s');
                $result_set = ($number . "," . $message . "," . $day_number . "," . $time_set . ",");
                setcookie($cookie_name, $result_set, time() + (86400), "/"); // 86400 = 1 day

            } else {
                $pieces = explode(",", $_COOKIE[$cookie_name]);
                $number = $pieces[0] + (rand(0, 3));
                $message = $pieces[1];
                $day_number = $pieces[2];
                $time_set = $pieces[3];
                if (time() - strtotime($time_set) > 300) {
                    $number = $pieces[0] + (rand(1, 8));
                }
                if (time() - strtotime($time_set) > 3599) {
                    $number = $pieces[0] + (rand(1, 15));
                    $day_number = $pieces[2] + 1;
                }
                $result_set = ($number . "," . $message . "," . $day_number . "," . $time_set . ",");
                setcookie($cookie_name, $result_set, time() + (86400), "/"); // 86400 = 1 day
            }
        }

        if ($number == "first" && $type == "jobs_create") {
            // unset($_COOKIE[$cookie_name]);
            if (!isset($_COOKIE[$cookie_name])) {
                $number = (rand(1, 14));
                $message = "quotation requests sent in the last ";
                $day_number = (rand(1, 24)) . " hours";
                $time_set = date('Y-m-d H:i:s');
                $result_set = ($number . "," . $message . "," . $day_number . "," . $time_set . ",");
                setcookie($cookie_name, $result_set, time() + (86400), "/"); // 86400 = 1 day

            } else {
                $pieces = explode(",", $_COOKIE[$cookie_name]);
                $number = $pieces[0];
                $message = $pieces[1];
                $day_number = $pieces[2];
                $time_set = $pieces[3];
                if (time() - strtotime($time_set) > 300) {
                    $number = $pieces[0] + (rand(0, 2));
                }
                if (time() - strtotime($time_set) > 3599) {
                    $number = $pieces[0] + (rand(1, 6));
                    $day_number = $pieces[2] + 1;
                }
                $result_set = ($number . "," . $message . "," . $day_number . "," . $time_set . ",");
                setcookie($cookie_name, $result_set, time() + (86400), "/"); // 86400 = 1 day
            }
        }
        if ($number == "first" && $type == "jobs_board") {
            // unset($_COOKIE[$cookie_name]);
            if (!isset($_COOKIE[$cookie_name])) {
                $number = (rand(1, 14));
                $message = "bids sent in the last ";
                $strings = array(
                    '24',
                    '48',
                );
                $key = array_rand($strings);
                $day_number = ($strings[$key]) . " hours";
                $time_set = date('Y-m-d H:i:s');
                $result_set = ($number . "," . $message . "," . $day_number . "," . $time_set . ",");
                setcookie($cookie_name, $result_set, time() + (86400), "/"); // 86400 = 1 day
            } else {
                $pieces = explode(",", $_COOKIE[$cookie_name]);
                $number = $pieces[0];
                $message = $pieces[1];
                $day_number = $pieces[2];
                $time_set = $pieces[3];
                if (time() - strtotime($time_set) > 300) {
                    $number = $pieces[0] + (rand(0, 2));
                }
                if (time() - strtotime($time_set) > 3599) {
                    $number = $pieces[0] + (rand(1, 6));
                }
                $result_set = ($number . "," . $message . "," . $day_number . "," . $time_set . ",");
                setcookie($cookie_name, $result_set, time() + (86400), "/"); // 86400 = 1 day
            }
        }

        if ($number == "first" && $type == "home") {
            // unset($_COOKIE[$cookie_name]);
            if (!isset($_COOKIE[$cookie_name])) {
                $number = (rand(47, 120));
                $message = "vendors have been contacted in the last";
                $day_number = (rand(3, 12)) . " hours";
                $time_set = date('Y-m-d H:i:s');
                $result_set = ($number . "," . $message . "," . $day_number . "," . $time_set . ",");
                setcookie($cookie_name, $result_set, time() + (86400), "/"); // 86400 = 1 day

            } else {
                $pieces = explode(",", $_COOKIE[$cookie_name]);
                $number = $pieces[0];
                $message = $pieces[1];
                $day_number = $pieces[2];
                $time_set = $pieces[3];

                if (time() - strtotime($time_set) > 800) {
                    $number = $pieces[0] + (rand(0, 2));
                }

                if (time() - strtotime($time_set) > 3600) {
                    $number = $pieces[0] + (rand(1, 8));
                    $day_number = $pieces[2] + 1;
                }
                $result_set = ($number . "," . $message . "," . $day_number . "," . $time_set . ",");
                setcookie($cookie_name, $result_set, time() + (86400), "/"); // 86400 = 1 day

            }
        }

        if ($number == "second" && $type == "home") {
            // unset($_COOKIE[$cookie_name]);
            if (!isset($_COOKIE[$cookie_name])) {
                $number = (rand(5, 20));
                $message = "new vendors added in ";
                $day_number = (rand(2, 3)) . " days";
                $time_set = date('Y-m-d H:i:s');
                $result_set = ($number . "," . $message . "," . $day_number . "," . $time_set . ",");
                setcookie($cookie_name, $result_set, time() + (86400), "/"); // 86400 = 1 day

            } else {
                $pieces = explode(",", $_COOKIE[$cookie_name]);
                $number = $pieces[0];
                $message = $pieces[1];
                $day_number = $pieces[2];
                $time_set = $pieces[3];

                if (time() - strtotime($time_set) > 5000) {
                    $number = $pieces[0] + (rand(0, 2));

                }
                $result_set = ($number . "," . $message . "," . $day_number . "," . $time_set . ",");
                setcookie($cookie_name, $result_set, time() + (86400), "/"); // 86400 = 1 day

            }
        }


        $data = [
            'result' => [
                'number' => $number,
                'message' => $message,
                'day_number' => $day_number,

            ]
        ];

        return $data;
    }
    public static function getNearby($user_query,$lat, $lng, $type = 'cities', $limit = 50, $distance = 15, $unit = 'km')
    {   

        // $results = DB::select(DB::raw('SELECT *, ( 6371 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $lng . ') ) + sin( radians(' . $lat .') ) * sin( radians(lat) ) ) ) AS distance FROM locations HAVING distance < ' . $distance . ' ORDER BY distance') );
        // dd($results);
        // return $results;
        // $nearby = DB::select("SELECT *, ( 6371 * acos( cos( radians(-4.0435) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(39.6682) ) + sin( radians(-4.0435) ) * sin( radians( lat ) ) ) ) AS distance FROM locations HAVING distance < 180 ORDER BY distance LIMIT 0 , 20;");
        // dd($nearby);
       
        $nearby = $user_query->selectRaw("*,
                         ( 6371 * acos( cos( radians(?) ) *
                           cos( radians( user_locations.lat ) )
                           * cos( radians( user_locations.lng ) - radians(?)
                           ) + sin( radians(?) ) *
                           sin( radians( user_locations.lat ) ) )
                         ) AS distance", [$lat, $lng, $lat])
        ->having("distance", "<", $distance)
        ->orderBy("distance");
        return $nearby;
    }
    public function paginate_results(Request $request, $results, $defaults = [])
    {
        $results_data = [];
        $paginated_data = [];

        $limit = (Input::has('limit')) ? Input::get('limit')
            : array_get($defaults, 'limit', 12);

        if ($limit != -1) {
            $offset = (Input::has('page')) ? (Input::get('page') - 1) * $limit : 0;
        }

        $page = (Input::has('page')) ? Input::get('page')
            : array_get($defaults, 'page', 1);


        foreach ($results as $result) {
            $results_data [] = $result;
        }
        $itemsForCurrentPage = ($limit != -1) ? array_slice($results_data, $offset, $limit, true) : $results_data;

        $paginated_data = ($limit != -1) ? new Paginator($itemsForCurrentPage, count($results), $limit, $page, [
            'path' => $request->url(),
            'query' => $request->query(),
        ]) : $results;

        return $paginated_data;
    }

    /**
     * Calculates the great-circle distance between two points, with
     * the Haversine formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
      // convert from degrees to radians
      $latFrom = deg2rad($latitudeFrom);
      $lonFrom = deg2rad($longitudeFrom);
      $latTo = deg2rad($latitudeTo);
      $lonTo = deg2rad($longitudeTo);

      $latDelta = $latTo - $latFrom;
      $lonDelta = $lonTo - $lonFrom;

      $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
        cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
      return $angle * $earthRadius;
    }
}