<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Tag;
use App\Image;
use App\Video;
use App\User;
use App\Profile;
use App\Classification;
use DB;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use TomLingham\Searchy\Facades\Searchy;
use Cohensive\Embed\Facades\Embed;
use Illuminate\Pagination\LengthAwarePaginator as paginator;
use App\Locations;

class TagsController extends Controller
{


    public function outputTags(Request $request)
    {

        $article = Image::find($request->input('image_id'));

        $tags = $request->input('tags');
        $currentTags = array_filter($tags, 'is_numeric');
        $newTags = array_diff($tags, $currentTags);


        foreach ($newTags as $newTag) {
            if ($tag = Tag::create([ 'name' => $newTag ]))
                $currentTags[] = $tag->id;

        }

        print_r($currentTags);


        $article->tags()->sync($currentTags);

        $gallery = Image::find($request->input('image_id'));
        $desc = ( $request->input('caption') );

        $image = $gallery->update([ 'caption' => $request->input('caption') ]);

        return redirect()->back();

    }


    public function viewInspirationPage()
    {


        $tags = Tag::lists('id');
        $images = [ ];


        foreach ($tags as $tag) {

            $image = Tag::find($tag)->image()->get();
            array_push($images, $image);
        }


        return view('gallery.Board')
            ->with('images', $images);


    }

    public function searchInspirationPage(Request $request)
    {


        $tagSearch = ( $request->input('tagSearch') ); //get input
        $arrs = explode(" ", $tagSearch); //input as array

        foreach ($arrs as $arr) { //loop through array

            $tagIDs[] = Searchy::tags('name')->query($arr)->select('id')->get();

            foreach ($tagIDs as $value) {
                $vals = $value->id;
                dd($vals);
            }
        }
        $images = [ ];


        // $sift = array_shift($vals);


        foreach ($vals as $val) {
            $image = Tag::find($val)->image()->get(); //get image details
            array_push($images, $image);

        }

        return view('gallery.Board')
            ->with('images', $images);
    }


    public function deleteImage($id)
    {


        $currentImage = Image::findorfail($id);
        // $deleteThumb = unlink(public_path('gallery/images/thumbs/' . $currentImage->file_name));
        // $deleteImage = unlink(public_path($currentImage->file_path));
        $currentImage->delete();

        return redirect()->back();

    }

    public function mainSearch(Request $request)

    {
        $mainSearch = ( $request->input('find') );
        $cities = locations::all();
        $border_color = ('#92BFE6');

        $results = Searchy::users('first_name')->query($mainSearch)->select('id')->getQuery()->take(15)->get();
        if (count($results) > 0) {
            foreach ($results as $result) {
                $id = $result->id;
                $currentUser = User::findorfail($id);
                if ($currentUser->hasUserRole('vendor')) {
                    $arrays[] = $currentUser;
                    $currents[] = $currentUser->classifications;
                }
            }

            foreach ($currents as $current) {
                foreach ($current as $curr) {
                    $clasfs[] = ( $curr->id );
                }
            }


            foreach ($clasfs as $count) {

                $classification = Classification::findorfail($count);
                $class_name[] = $classification->name;
            }
            $counts = array_count_values($class_name);


            $border_color = ( '#92BFE6' );
            
            return View('pages.search')->with('cities', $cities)->with('counts', $counts)->with('arrays', $arrays)->with('border_color', $border_color)->with('mainSearch', $mainSearch);
        }
        else {

            $border_color = ( '#92BFE6' );

            return redirect()->back()->with('cities', $cities)->with('border_color', $border_color)->with('status', 'alert')
                             ->with('searchErrors', 'No results');
        }
    }


    public function autoComplete(Request $request)
    {
        $mainSearch = ( $request->input('search') );
    }

    public function fetchSubCategory($class_id,$mainSearch){

        $cities = locations::all();
        $border_color = ('#92BFE6');
        $classification = Classification::whereSlug($class_id)->first();

        $features = $classification->users;

        $featured =[];
        foreach ($features as $feat) {
            if ($feat->priority == 1) {
                array_push($featured, $feat);
            }
        }
        
        $arrays = $classification->users()->whereHas('profile', function ($query) use ($mainSearch) 
                                                                    {
                                                                        $query->where('location', $mainSearch);
                                                                    })->paginate(15); 
        
        if ($arrays->total() == 0 && $mainSearch == "all" ) {
            $arrays = $classification->users()->paginate(15);
        }

        elseif($arrays->total() == 0 && $mainSearch != "all"){
            $border_color=('#92BFE6');
            return redirect()->back()->with('mainSearch',$mainSearch)->with('status', 'alert')->with('errorsZurb', 'No results');
        }
        return View('pages.vendor-classification')->with('featured',$featured)->with('cities',$cities)->with('arrays',$arrays)->with('mainSearch',$mainSearch)->with('classification',$classification);
           
                
            
    }
        public function fetchSearchCategory(Request $request){

            $cities = locations::all();
            $border_color = ('#92BFE6');
            $class_id=($request->input('locate'));
            $mainSearch=($request->input('search'));
            $results = Searchy::users('first_name')->query($mainSearch)->select('id')->get();
           
            $arrays=[];
            $currents=[];
            if (count($results) > 0) {
                foreach ($results as $result) {
                    $id=$result->id;
                    $currentUser= User::findorfail($id);
                        if ($currentUser->hasUserRole('vendor')) {
                            if ($currentUser->profile->location == $class_id) 
                                {
                                    $currents[]=$currentUser->classifications;
                                array_push($arrays, $currentUser);
                                }
                        }
                    
                }   
                    if (count($currents) > 0) 
                    {
                        foreach ($currents as $current) {
                            foreach ($current as $curr) {
                                $clasfs[]=($curr->id);                        }
                        }
                            


                foreach ($clasfs as $count) {

                    $classification = Classification::findorfail($count);
                    $class_name[] = $classification->name;
                }
                $counts = array_count_values($class_name);
            }

            if (count($arrays) > 0) {
                $border_color = ( '#92BFE6' );

                return View('pages.search')->with('cities', $cities)->with('counts', $counts)->with('arrays', $arrays)->with('border_color', $border_color)->with('mainSearch', $mainSearch);
            }
            else {
                $border_color = ( '#92BFE6' );

                return redirect()->back()->with('cities', $cities)->with('border_color', $border_color)->with('status', 'alert')
                                 ->with('errorsZurb', 'No results');
            }
        }
    }

    public function refineSearch($mainSearch, $category)
    {
        $cities = locations::all();
        $border_color = ('#92BFE6');
        $results = Searchy::users('first_name')->query($mainSearch)->select('id')->get();
        $class_name = [ ];
        foreach ($results as $result) {
            $id = $result->id;
            $currentUser = User::findorfail($id);
            if ($currentUser->hasUserRole('vendor')) {
                foreach ($currentUser->classifications as $classes) {
                    if ($classes->name == $category) {
                        $arrays[] = $currentUser;
                        $class_n = $classes->name;
                        array_push($class_name, $class_n);
                    }
                }
            }
        }

        $counts = array_count_values($class_name);

        $border_color = ( '#92BFE6' );

        return View('pages.search')->with('cities', $cities)->with('counts', $counts)->with('arrays', $arrays)->with('border_color', $border_color)->with('mainSearch', $mainSearch);
    }


}



