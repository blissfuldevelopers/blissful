<?php

namespace App\Http\Controllers;

use App\DataTables\CronTaskDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCronTaskRequest;
use App\Http\Requests\UpdateCronTaskRequest;
use App\Repositories\CronTaskRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;
use Illuminate\Http\Request;

class CronTaskController extends AppBaseController
{
    /** @var  CronTaskRepository */
    private $cronTaskRepository;
    protected $is_ajax = false;

    public function __construct(CronTaskRepository $cronTaskRepo, Request $request)
    {
        $this->is_ajax = $request->ajax();
        $this->cronTaskRepository = $cronTaskRepo;
    }

    /**
     * Display a listing of the CronTask.
     *
     * @param CronTaskDataTable $cronTaskDataTable
     *
     * @return Response
     */
    public function index(CronTaskDataTable $cronTaskDataTable)
    {
        return $cronTaskDataTable->render('cron_tasks.index');
    }

    /**
     * Show the form for creating a new CronTask.
     *
     * @return Response
     */
    public function create()
    {
        return $this->view('cron_tasks.create');
    }

    /**
     * Store a newly created CronTask in storage.
     *
     * @param CreateCronTaskRequest $request
     *
     * @return Response
     */
    public function store(CreateCronTaskRequest $request)
    {
        $input = $request->all();

        $cronTask = $this->cronTaskRepository->create($input);

        Flash::success('CronTask saved successfully.');

        return redirect(route('cron_tasks.index'));
    }

    /**
     * Display the specified CronTask.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cronTask = $this->cronTaskRepository->findWithoutFail($id);

        if (empty( $cronTask )) {
            Flash::error('CronTask not found');

            return redirect(route('cron_tasks.index'));
        }

        return $this->view('cron_tasks.show')->with('cronTask', $cronTask);
    }

    /**
     * Show the form for editing the specified CronTask.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cronTask = $this->cronTaskRepository->findWithoutFail($id);

        if (empty( $cronTask )) {
            Flash::error('CronTask not found');

            return redirect(route('cron_tasks.index'));
        }

        return $this->view('cron_tasks.edit')->with('cronTask', $cronTask);
    }

    /**
     * Update the specified CronTask in storage.
     *
     * @param  int                  $id
     * @param UpdateCronTaskRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCronTaskRequest $request)
    {
        $cronTask = $this->cronTaskRepository->findWithoutFail($id);

        if (empty( $cronTask )) {
            Flash::error('CronTask not found');

            return redirect(route('cron_tasks.index'));
        }

        $cronTask = $this->cronTaskRepository->update($request->all(), $id);

        Flash::success('CronTask updated successfully.');

        return redirect(route('cron_tasks.index'));
    }

    /**
     * Remove the specified CronTask from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $cronTask = $this->cronTaskRepository->findWithoutFail($id);

        if (empty( $cronTask )) {
            Flash::error('CronTask not found');

            return redirect(route('cron_tasks.index'));
        }

        $this->cronTaskRepository->delete($id);

        Flash::success('CronTask deleted successfully.');

        return redirect(route('cron_tasks.index'));
    }

    /**
     * @param       $view
     * @param array $data
     *
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $data = [ ])
    {
        if ($this->is_ajax) {
            return view($view, $data);
        }

        return view('layouts.non-ajax-layout', $data)
            ->with('content', $view);

    }
}
