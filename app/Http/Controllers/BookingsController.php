<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Bookings;
use App\Jobs\SendEmail;

class BookingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookings = Bookings::all();
        return view('bookings.index',compact('bookings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $user = User::where('email',$input['email'])->first();
        $vendor = User::where('id',$input['vendor_id'])->first();
        
        $user_id = isset($user->id) ? $user->id : 0;

        $date = str_replace('/', '-', $input['date']);
        $newDate = date("Y-m-d", strtotime($date));

        $input['date']      = $newDate;
        $input['user_id'] = $user_id;

        $booking = Bookings::create($input);

        //check if we have the phone 
        $phone = isset($user->profile->phone) && !empty($user->profile->phone) && $user->profile->phone != $input['phone'] ? $user->profile->phone.' / '.$input['phone'] : $input['phone'];
        $name = isset($user->first_name) ? $user->first_name : $input['name'];

        $message = 'Details: <br> Name:'.$name.'<br> Email:'.$input['email'].'<br> Phone:'.$phone.'<br>Venue:'.$vendor->first_name.'<br> Date:'.$input['date'];

        $data = [
              'email'        => 'happy@blissful.co.ke',
              'title'   => 'New venue booking for '.$vendor->first_name,
              'message'      => $message,
            ];

        //dispatch email to admin
        dispatch( New SendEmail($data));

        return view('bookings.success'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking = Bookings::where('id',$id)->first();

        return view('bookings.show',compact('booking'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $booking = Bookings::where('id',$id)->first();

        return view('bookings.show',compact('booking'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
