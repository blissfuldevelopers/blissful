<?php
namespace App\Http\Controllers;

use App\Jobs\BlogIndexData;
use App\Http\Requests;
use App\Post;
use App\Category;
use App\Activity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Mixpanel;
use Carbon\Carbon;
class BlogController extends Controller
{
  public function index(Request $request)
  {
    $category = $request->get('category');

    $data = $this->dispatch(new BlogIndexData($category));
     
    $layout = $category ? Category::layout($category) : 'blog.layouts.index';

    Activity::create([
        'subject_id'=> (Auth::check()) ? Auth::user()->id : 0,
        'subject_type'=>'App\User',
        'name'=>'viewed_blog',
        'user_id'=> (Auth::check()) ? Auth::user()->id : 0,
    ]);

    return view($layout, $data);
  }

  public function showPost($slug, Request $request)
  {   

    try {
         $post = Post::with('categories')->whereSlug($slug)->firstOrFail();
    } catch(ModelNotFoundException $e) {
        return redirect('/blog');
    }
   

    $category = $request->get('category');
    if ($category) {
        $category = Category::whereCategory($category)->firstOrFail();
    }
    else{
      $category = $post->categories()->first();
    }
      if (isset($category)) {
        $related = Post::where('published_at', '<=', Carbon::now())
                    ->where('id','!=',$post->id)
                     ->whereHas('categories', function ($q) use ($category) {
                         $q->where('category', '=', $category->category);
                     })
                     ->where('is_draft', 0)
                     ->get();
        if ($related->count() > 3) {
          $related = $related->random(3);
        }
      }
        
        
      Activity::create([
          'subject_id'=> $post->id,
          'subject_type'=>'App\Post',
          'name'=>'viewed_post',
          'user_id'=> (Auth::check()) ? Auth::user()->id : 0,
      ]);

    return view($post->layout, compact('post', 'category', 'slug','related'));
  }
}