<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Locations;
use App\Classification;
use Mixpanel;
use Illuminate\Support\Facades\Auth;
class OnboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkRedirect(){
         if(!isset($_COOKIE['job_intro'])) {
            return redirect('/jobs/intro');
         }
         else{
            return redirect('/jobs/create');
         }
    }
    public function jobIntro(){
        setcookie("job_intro", "Viewed intro", time() + (86400*365), "/");

        return view('jobs.partials.job-intro');
    }
    public function jobcreate(){
        //TODO - add the Event if there's a event=$id

        $classifications = Classification::whereNull('parent_id')->orderBy('priority', 'ASC')->get();

        $locations = Locations::all();



        return view('jobs.fullscreen-create')
            ->with('classifications', $classifications)
            ->with('locations', $locations);

    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
