<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classification;
use DateTime;
use App\Job;
use App\Bid;

class BudgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   
        $input = $request->all();

        $classification = Classification::findorfail($input['classification_id']);
        $job_ids = [];
        $classification_options = Null;

        if (isset($input['classifications_children']) || isset($input['classifications_parent'])) {
            $classification_options = isset($input['classifications_children']) ? $input['classifications_children'] : $input['classifications_parent'] ;
        }
        
        
        $event_date = isset($input['event_date']) ? $input['event_date'] : date("Y-m-d");

        //does the classification exist?
        if (!isset($classification)) {
            return false;
        }
        
        if (is_null($classification_options)) {
                if (isset($input['location'])) {
                    $results = Job::where('classification_id',$classification->id)
                                    ->where('event_location',$input['location'])
                                    ->get();
                            
                //if there aren't enough results
                if (!$results || $results->count() <= 10) {
                    $additional_results = Job::where('classification_id','=',$classification->id)
                                                ->orderby('created_at','DESC')
                                                ->limit(10 - $results->count())
                                                ->get();

                    //merge the extra results
                    $results = $results->merge($additional_results);
                }
                
            }

        }

        else {
            if (isset($input['location'])) {
                $results = Job::where('classification_id',$classification->id)
                                ->whereHas('classifications', function ($query) use ($classification_options) {
                                    $query->wherein('classifications.id', $classification_options);
                                })
                                ->where('event_location',$input['location'])
                                ->get();
                
                //if there aren't enough results
                if (!$results || $results->count() <= 10) {
                    $additional_results = Job::where('classification_id','=',$classification->id)
                                                ->whereHas('classifications', function ($query) use ($classification_options) {
                                                    $query->wherein('classifications.id', $classification_options);
                                                })
                                                ->orderby('created_at','DESC')
                                                ->limit(10 - $results->count())
                                                ->get();

                    //merge the extra results
                    $results = $results->merge($additional_results);
                }
            }
        }

        //no results at all
        if ($results->count() < 1 && isset($input['classifications_parent'] )) {

            $classification_options = $input['classifications_parent'];

            $results = Job::where('classification_id','=',$classification->id)
                                                ->whereHas('classifications', function ($query) use ($classification_options) {
                                                    $query->wherein('classifications.id', $classification_options);
                                                })
                                                ->orderby('created_at','DESC')
                                                ->limit(10 - $results->count())
                                                ->get();
           
        }
        elseif ($results->count() < 1) {
            
            $results = Job::where('classification_id',$classification->id)->orderby('created_at','DESC')->get();
           
        }
        //if still no results, get the trashed ones too
        if ($results->count() < 1) {
            $results = Job::withTrashed()->where('classification_id',$classification->id)->orderby('created_at','DESC')->get();
        }

        foreach ($results as $key => $result) {

            //to be used for comparing bids
            array_push($job_ids, $result->id);
            $result_date = $result->event_date;
            //check if jobs are within range
            $in_range = $this->check_in_range($event_date,$result_date);

            ($in_range ? $result->weight = 1 : $result->weight = 1.03);
            
            ((isset($input['guest_number']) && !empty($result->guest_number)) ? $result = $this->adjust_budget($result,$input['guest_number']) : '');
            
        }
        
        if ($job_ids) {
            //get all bids for the jobs analysed
            $bid_results = Bid::wherein('job_id',$job_ids)->get();

            foreach ($bid_results as $key => $bid_result) {

                $result_date =  $bid_result->job->event_date;
                //check if bids are for jobs that are in range
                $in_range = $this->check_in_range($event_date,$result_date);

                ($in_range ? $bid_result->weight = 1 : $bid_result->weight = 1.03);

                //adjust budget according to guest number
                ((isset($input['guest_number']) && !empty($bid_result->job->guest_number)) ? $bid_result = $this->adjust_budget($bid_result,$input['guest_number']) : $bid_result->budget = $bid_result->estimated_price);
            }

            $bid_results->merge($results);
        }

        $estimate_budget = $this->calculate_weighted_average($results);
        
        return view('jobs.budget-estimate',compact('estimate_budget','classification'));
        // return $estimate_budget;
        


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function check_in_range($event_date,$result_date)
    {

        

        //create some range values
        $start_date = date_format(DateTime::createFromFormat('Y-m-d', $event_date)->modify('-6 weeks'), 'Y-m-d');
        $end_date = date_format(DateTime::createFromFormat('Y-m-d', $event_date)->modify('+6 weeks'), 'Y-m-d');
        
        // Convert to timestamp
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $user_ts = strtotime($result_date);
        
      // Check that user date is between start & end
      return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }
    /**
     * Level out the budget for jobs with higher or lower number of guests
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function adjust_budget($result,$guest_number){
        // 
        if ($result->guest_number != $guest_number) {


            $job_range = explode("-", str_replace("+", "",$result->guest_number));
            $estimate_range = explode("-",str_replace("+", "",$guest_number));

            
            $job_range_median = $this->calculate_median($job_range);
            $estimate_range_median = $this->calculate_median($estimate_range);
            
            if ($result->budget > 0 ) {
                
                $adjusted_budget = ($result->budget/$job_range_median) * $estimate_range_median;
            
                //make adjusted budget the new budget
                $result->budget = round($adjusted_budget);
            }
            

        }
        return $result;
    }
    public function calculate_median($range_array){

        if (!(array_key_exists(1, $range_array))) {
            return $range_array[0];
        }
        //get medians
        foreach (range($range_array[0],$range_array[1]) as $number) {
            $arr[] = $number;
        }

        $count = count($arr); //total numbers in array
        $middleval = floor(($count-1)/2); // find the middle value, or the lowest middle value

        if($count % 2) { // odd number, middle is the median
            $median = $arr[$middleval];
        } else { // even number, calculate avg of 2 medians
            $low = $arr[$middleval];
            $high = $arr[$middleval+1];
            $median = (($low+$high)/2);
        }
        return $median;
    }

    public function calculate_weighted_average($results){

        $total = 0;
        $count = 0;
        $estimate = 0;

        foreach($results as $key=>$result) {

            //only include where budget exists
            if ($result->budget > 0) {
                $total += $result->weight * $result->budget;
                $count += $result->weight;
            }
          
        }

        if ($count > 0) {
            $estimate = round($total / $count,-3);
        }
        
        
        return $estimate;
    }
}
