<?php

namespace App\Http\Controllers;

use App\Events\JobStatusChanged;
use App\Http\Requests;
use App\Bid;
use App\Job;
use App\Activity;
use Illuminate\Http\Request;
use Session;
use App\CreditsDebitCredit;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Ranking;
use App\BreakdownDetails;
use App\Jobs\SendMessages;

class BidsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user = Auth::user();
        foreach ($user->job as $item) {
            $bids[] = $item->bids;
        }

        $page_data = compact('bids');

        return view('bids.index', $page_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $user = Auth::user();
        $jobs_redirect = (!$user || !$user->can_bid_for_job)
            ? 'mine'
            : (!$request->job) ? 'all' : $request->job;

        return redirect()->route('jobs.show', [$jobs_redirect]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        // $this->validate($request, Bid::$inputRules);
        $user = Auth::user();

        $input = $request->all();

        //add classifications selected by user
        if (isset($input['vendor_classification'])) {
            $classification = $user->classifications->find($input['vendor_classification']);
            if ($classification && $classification->count() > 1) {

                $user->classifications()->detach($input['vendor_classification']);
            }
            
            if (!$user->classifications->contains($input['vendor_classification'])) {
                $user->classifications()->attach($input['vendor_classification']);
            }
        }
        
        if (isset($input['action']) && $input['action'] == 'change_status') {
            return $this->change_bid_status($input);
        }

        return $this->bid_for_job($input);
    }

    /**
     * @param $input
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function change_bid_status(Request $request)
    {
        $input = $request->all();

        $bid = Bid::findorfail($input['id']);
        $status = $input['status'];
        //set status
        $bid->status = $status;
        //get job
        $job = $bid->job;

        //fire event
//        event(new JobStatusChanged($bid));
        //set completed 
        if ($status == 2) {
            $job->completed = 1;
            $job->save();

            $ranking = new Ranking;
            $ranking->storeRankingForVendor($bid->user_id, 'bid accepted', 10);

        }
        if ($status == 3) {
            $ranking = new Ranking;
            $ranking->storeRankingForVendor($bid->user_id, 'bid declined', 0);

        }

        if ($bid->save()) {
            Flash::success('Successfully updated status!');
        } else {
            Flash::warning('Error: Could not update status. Please try again');
        }

        return redirect()->route('messages.show', $bid->thread->id);
    }

    /**
     * @param $input
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function bid_for_job($input)
    {
        $route = route('jobs.index');
        $user = Auth::user();
        $use_bootstrap_version = (isset($input['bootstrap'])) ? true : false;
        $input['user_id'] = ($user) ? $user->id : 0;
        $job = Job::findorfail($input['job_id']);

        if (!$job->user_can_bid) {
            $route = route('jobs.show', $input['job_id']) . (($use_bootstrap_version) ? '?use_bootstrap_version=true' : '');

            return redirect($route);

        }

        if (!empty(CreditsDebitCredit::total_balance($user->id))
            && CreditsDebitCredit::total_balance($user->id) >= $job->credits
        ) {

            $bid = Bid::bid_for_job($input);
            if ($bid) {

                $group_dets = [];

                foreach($input as $key => $value) {
                    $exp_key = explode('-', $key);
                    //check if there's a file to save
                    if(count($exp_key) > 1 && $exp_key[0] == "file") {

                        $bid->attachments()->attach($value);
                    }
                    elseif (count($exp_key) > 1 && $exp_key[0] == "item_name") {
                        $group_name = $exp_key[1]."-".$exp_key[2];
                        array_push($group_dets, $group_name);
                    }

                }
                $bid_details_array = [];
                //please clean this someday. adds details to bid
                foreach ($group_dets as $k => $v) {

                    $group_key = explode('-', $v);
                    $group_input = [];
                    foreach ($input as $key => $value) {
                        $exp_key = explode('-', $key);
                        if (count($exp_key) > 1 && $exp_key[1] == $group_key[0] && $exp_key[2] == $group_key[1] ) {
                            $group_input[$exp_key[0]] = $value;
                        }
                    }
                    $group_input['cat_id']  = $group_key[0];
                    $group_input['user_id'] = Auth::user()->id;
                    $group_input['bid_id']  = $bid->id;

                    $bid_details = new BreakdownDetails;
                    $bid_details->fill($group_input);
                    $bid_details->save();
                    array_push($bid_details_array, $bid_details);

                }
                
                

                $ranking = new Ranking;
                $ranking->storeRankingForVendor($user->id, 'bid for job', 5);
                
                $route = route('jobs.show', $bid->job_id);

                return redirect($route)->with('message', 'Request sent successfully')
                ->with('status', 'success');

                //prep input for send
                $messageAttributes = array_merge([
                                        'bid_id'    => $bid->id,
                                        'bid_accepted' => ( isset( $input[ 'bid_accepted' ] ) ) ? $input[ 'bid_accepted' ] : 0,
                                        'thread_id'    => ( isset( $bid->thread->id ) ) ? $bid->thread->id: 0,
                                     ], (array)$input);
                 //dispatch this job to queue
                dispatch(new SendMessages($messageAttributes));

                Flash::success('Bid placed successfully');
                return redirect($route)->with('success', $success);
            }
            Flash::warning('Bid not placed, please retry!');
            $route = route('jobs.show', $input['job_id']);

            return redirect($route)
                ->withErrors($bid);
        } else {
            $route = route('jobs.show', $input['job_id']);

            return redirect($route)->with('message', 'Not enough credits to bid for this job. Please Purchase some credits')
                ->with('status', 'alert');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $bid = Bid::findOrFail($id);

        return view('bids.show', compact('bid'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bid = Bid::findOrFail($id);

        return view('bids.edit', compact('bid'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $bid = Bid::findOrFail($id);
        $bid->update($request->all());

        Session::flash('flash_message', 'Bids successfully updated!');

        return redirect('bids');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Bid::destroy($id);

        Session::flash('flash_message', 'Bids successfully deleted!');

        return redirect('bids');
    }


}
