<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\FileEntry;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;

class FileEntryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $entries = FileEntry::all();

        return view('fileentries.index', compact('entries'));
    }

    public function add()
    {

        $file = Request::file('filefield');
        $extension = $file->getClientOriginalExtension();
        Storage::disk('local')->put($file->getFilename() . '.' . $extension, File::get($file));
        $entry = new FileEntry();
        $entry->user_id = Auth::user()->id;
        $entry->mime = $file->getClientMimeType();
        $entry->original_filename = $file->getClientOriginalName();
        $entry->filename = $file->getFilename() . '.' . $extension;

        $entry->save();

        return redirect()->back();

    }
    public function saveS3(Request $request){
        $file = $request->file('file');
        $filename = $file->getClientOriginalName();
        $filePath = 'attachments/' . $filename;
        $extension = $file->getClientOriginalExtension();
        $file_size = $file->getSize();
        $readable_size = $this->formatBytes($file_size);

        //fileEntry details
        $entry = new FileEntry();
        $entry->user_id = Auth::user()->id;
        $entry->mime = $file->getClientMimeType();
        $entry->original_filename = $filename;
        $entry->file_path = $filePath;
        $entry->file_size = $file_size;
        $entry->readable_size = $readable_size;
        $entry->filename = $file->getFilename() . '.' . $extension;

        $entry->save();

        //save to s3
        $s3 = Storage::disk('s3');
        $s3->getDriver()->put($filePath, file_get_contents($file->getRealPath()),[ 
                                                                'visibility' => 'public',
                                                                'CacheControl' => 'max-age=2592000',
                                                                'Expires' => gmdate("D, d M Y H:i:s T",strtotime("+3 years"))

                                                            ]);
        return $entry;
    }
    public function get($filename)
    {
        $entry = FileEntry::where('filename', '=', $filename)->firstOrFail();
        $file = Storage::disk('local')->get($entry->filename);

        return (new Response($file, 200))
            ->header('Content-Type', $entry->mime);

    }

    public function destroy($id)
    {
        $entry = FileEntry::findOrFail($id);
        Storage::delete($entry->filename);
        return redirect()->back();
    }
     /**
     * Format bytes to kb, mb, gb, tb
     *
     * @param  integer $size
     * @param  integer $precision
     * @return integer
     */
    public static function formatBytes($size, $precision = 2)
    {
        if ($size > 0) {
            $size = (int) $size;
            $base = log($size) / log(1024);
            $suffixes = array(' bytes', ' KB', ' MB', ' GB', ' TB');

            return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
        } else {
            return $size;
        }
    }
}
