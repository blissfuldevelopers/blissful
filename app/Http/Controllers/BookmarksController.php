<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Bookmarks;
use Auth;

class BookmarksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if (Auth::user()->hasUserRole('administrator')) {
            $bookmarks = Bookmarks::all();
            return view('bookmarks.index',compact('bookmarks'));
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        $vendor = User::where('id',$input['vendor_id'])->first();
        $bookmark = Bookmarks::where('user_id',$input['user_id'])->where('vendor_id',$input['vendor_id'])->get();
        if ($bookmark->count() > 0) {
            return redirect()->back()->with('status', 'alert')
            ->with('message', 'You have already bookmarked this vendor');
        }
        elseif ($vendor && $bookmark->count() == 0) {
            $booking = Bookmarks::create($input);
            return redirect()->route('vendor.show',$vendor->slug)->with('status', 'success')
            ->with('message', 'Bookmark successfully saved');
        }
        else{
            return redirect()->back()->with('status', 'alert')
            ->with('message', 'Error while saving bookmark');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Auth::user()->id == $id || Auth::user()->hasUserRole('administrator')) {
            $bookmarks = Bookmarks::where('user_id',$id)->get();
            return view('bookmarks.index',compact('bookmarks'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $booking = Bookmarks::where('id',$id)->first();

        if (Auth::user()->id == $booking->user_id || Auth::user()->hasUserRole('administrator')) {
            $booking->delete();
            return redirect()->back()->with('status', 'alert')
            ->with('message', 'Bookmark removed succesfully');
        }
        
    }
}
