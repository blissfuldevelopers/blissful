<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Contracts\Filesystem\Filesystem;
use App\UserAds;
use Laracasts\Flash\Flash;


class UserAdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        return view('admin.ads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        //add file inputs to array
        $files['full_img_src'] = $request->file('full_img_src');
        $files['mobile_img_src'] = $request->file('mobile_img_src');

        //save to s3
        foreach ($files as $key => $file) {

            $filename = uniqid() . $file->getClientOriginalName();
            $thumb = Image::make($file->getRealPath());

            $image_thumb = $thumb->stream();
            $s3 = \Storage::disk('s3');
            $filePath = 'gallery/images/' . $filename;
            $s3->put($filePath, $image_thumb->__toString(), 'public');
            //set img_src
            $input[$key] = $filePath;
        }
        $userAd = UserAds::create($input);

        Flash::success('Ad created successfully');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.ads.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
