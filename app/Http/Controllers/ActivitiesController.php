<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Activity;

class ActivitiesController extends Controller
{
	protected $is_ajax = false;

	public function __construct(Request $request)
    {
        $this->is_ajax = $request->ajax();

    }

    public function show()
    {

        $activity = Activity::with(['user','subject'])->latest()->paginate(20);


        return $this->view('admin.activities.activities', compact('activity'));
    }
    private function view($view, $data = [ ])
    {
        if ($this->is_ajax) {
            return view($view, $data);
        }

        return view('layouts.non-ajax-layout', $data)
            ->with('content', $view);

    }
}
