<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where all API routes are defined.
|
*/


Route::resource("users", "UserAPIController");

Route::resource("threads", "ThreadAPIController");
Route::resource("messages", "ThreadAPIController");

Route::resource("events", "EventAPIController");

Route::resource('cron_messages', 'CronMessageAPIController');

Route::resource('cron_recipients', 'CronRecipientAPIController');

Route::resource('cron_tasks', 'CronTaskAPIController');

Route::resource('cron_schedules', 'CronScheduleAPIController');

Route::resource('reminders', 'ReminderAPIController');