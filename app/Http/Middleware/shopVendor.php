<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use ResponseManager;

class shopVendor
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return Response()->json(ResponseManager::getError('', 401, 'You are not authorized to access this. Plese login.'));
        } else if (!Auth::user()->hasRole('administrator') && !Auth::user()->hasRole('vendor')) {
            return Response()->json(ResponseManager::getError('', 401, 'You are not authorized to access this. Plese login.'));
        }
        return $next($request);
    }

}
