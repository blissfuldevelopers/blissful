<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\Libraries\blissful\blissanalytics;

class TrackAnalytics
{

    /**
     * Create a new filter instance.
     *
     * @param  Guard $auth
     *
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $segments = $request->segments();
        $track = true;
        if ($segments && count($segments)) {
            $last_part = $segments[ count($segments) - 1 ];
            $track = ( !strpos($last_part, '.') && !in_array($last_part, config('blissanalytics.do_not_track_url_part')) );
        }

        if ($track) {

            $user_id = ( !$this->auth->guest() ) ? $this->auth->user()->id : 0;
            $email = ( !$this->auth->guest() ) ? $this->auth->user()->email : 'guest@blissful.co.ke';

            $arguments = [
                'client_id' => $email,
                'user_id'   => $user_id,
                'url'       => $request->url(),
                'uip'       => $request->ip(),
                'ua'        => $request->header('User-Agent'),
            ];

            BlissAnalytics::send_ga_measurement_protocol(json_encode($arguments));
        }

        return $next($request);
    }

}
