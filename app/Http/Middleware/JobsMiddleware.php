<?php

namespace App\Http\Middleware;

use Closure;

class JobsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $url = $request->url();
        $url_parts = explode('/', $url);
        $last_part = $url_parts[ count($url_parts) - 1 ];

        if ($request->method() == 'POST') {
            return $next($request);
        }
        // if (in_array($last_part, [ 'create', 'child-class','final'] ) || is_numeric($last_part)) {
        //     return $next($request);
        // }
        // if (!$request->ajax() && !strpos($last_part, '.')
        //     && !in_array($last_part, [ 'index', 'home', 'jobs' ])
        // ) {

        //     //redirect to function that'll handle ajaxify
        //     return app('App\Http\Controllers\JobsController')->routeNonAjax($request);
        // }
        // elseif ($request->ajax() && in_array($last_part, [ 'all', 'index', 'home', 'jobs' ])) {

        //     return app('App\Http\Controllers\JobsController')->show('all', $request);
        // }

        return $next($request);
    }
}
