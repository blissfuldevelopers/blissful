<?php

use Illuminate\Support\Facades\Redis;


//Home
Route::get('/', ['as' => 'home', 'uses' => 'PagesController@index']);

//Calendar
Route::get('/booking/calendar', ['as'=>'booking.calendar', 'uses'=>'BookingController@calendar']);

Route::resource('/booking/events', 'BookingController');

Route::get('/booking/api', ['as'=>'booking.api', 'uses'=>'BookingController@api']);

Route::post('ajaxify/check_availability/{id}', 'BookingController@check_availability');

Route::group(['middleware' => 'auth:all'], function () {
    Route::resource("bookmarks", "BookmarksController");
    Route::resource("/user-location", "LocationsController");
    Route::resource("/events", "EventController");
    Route::resource('/reminders', 'ReminderController');
    Route::get('/fetch/jobs','JobsController@fetch_requests');
    Route::get('/user/reports/{id}', 'Admin\ReportsController@user_stats');
});

//push notifications devices
Route::resource('/devices', 'DevicesController');

//bookings
Route::post('/bookings/save', ['as' => 'bookings.save', 'uses' => 'BookingController@store']);
Route::group(['middleware' => 'auth:administrator'], function () {
    Route::resource('/bookings', 'BookingsController');
});


//feedback
Route::post('/feedback', 'AdminController@feedback');

//options
Route::get('/options', 'Admin\ClassificationController@category_options');

//search venues
Route::get('/venues/search','VendorController@venueSearch');

// Added by Gaurang Ghinaiya
Route::get('/shop', 'Front\ProductController@home');
Route::get('/shop/product/{product_slug}', 'Front\ProductController@show');
Route::resource('/shop/cart', 'Front\CartController');
Route::resource('/shop/checkout', 'Front\CheckoutController');
Route::get('/shop/finalstep/{id?}', 'Front\CheckoutController@finalStep');
Route::get('/shop/pespalipn/{id?}', 'Front\CheckoutController@pespalIpn');
Route::post('/shop/save-user-order-details', 'Front\CheckoutController@save_user_order_details');
Route::get('/shop/cod-order-confirmation/{id}', 'Front\CheckoutController@cod_order_confirmation');
Route::get('/shop/cart-count', 'Front\CartController@cartCount');
Route::get('/shop/category/{category_slug}', 'Front\ProductController@catProduct');
Route::get('/shop/subcategory/{subcategory_slug}', 'Front\ProductController@subCatProduct');

Route::group(['prefix' => '/shop/admin', 'as' => 'shop.admin.', 'middleware' => 'auth:administrator'], function () {
    Route::resource('/home', 'AdminShop\AdminShopController', [
        'names' => [
            'index' => 'myshop.index',
            'create' => 'myshop.create',
            'store' => 'myshop.store',
            'show' => 'myshop.show',
            'edit' => 'myshop.edit',
            'update' => 'myshop.update',
            'destroy' => 'myshop.destroy',
        ],
    ]);

    Route::resource('/category', 'AdminShop\CategoryController', [
        'names' => [
            'index' => 'category.index',
            'create' => 'category.create',
            'store' => 'category.store',
            'show' => 'category.show',
            'edit' => 'category.edit',
            'update' => 'category.update',
            'destroy' => 'category.destroy',
        ],
    ]);

    Route::resource('/subcategory', 'AdminShop\SubcategoryController', [
        'names' => [
            'index' => 'subcategory.index',
            'create' => 'subcategory.create',
            'store' => 'subcategory.store',
            'show' => 'subcategory.show',
            'edit' => 'subcategory.edit',
            'update' => 'subcategory.update',
            'destroy' => 'subcategory.destroy',
        ],
    ]);
//    Route::get('/get-sub-category/{id}', 'AdminShop\SubcategoryController@getSubCategory');

//    Route::resource('/users', 'AdminShop\UsersController');
//    Route::post('/user/paginate', 'AdminShop\UsersController@paginateUser');

    Route::resource('/products', 'AdminShop\ProductsController', [
        'names' => [
            'index' => 'products.index',
            'create' => 'products.create',
            'store' => 'products.store',
            'show' => 'products.show',
            'edit' => 'products.edit',
            'update' => 'products.update',
            'destroy' => 'products.destroy',
        ],
    ]);
    Route::resource('/orders', 'AdminShop\ShopOrdersController', [
        'names' => [
            'index' => 'orders.index',
            'show' => 'orders.show',
            'edit' => 'orders.edit',
            'update' => 'orders.update',
            'destroy' => 'orders.destroy',
        ],
    ]);

//    Route::post('/product/paginate', 'AdminShop\ProductsController@paginate');
});
Route::get('shop/admin/product/images/{id}', ['as' => 'products.images', 'uses' => 'AdminShop\ProductsController@images']);
Route::post('shop/admin/product/images/{id}', ['as' => 'products.upload', 'uses' => 'AdminShop\ProductsController@upload']);
Route::get('shop/admin/product/images/delete/{id}', ['as' => 'products.delete', 'uses' => 'AdminShop\ProductsController@deleteImage']);
// EOF of Gaurang Ghinaiya
Route::get('onboard/jobs', 'OnboardController@checkRedirect');
Route::get('jobs/intro', 'OnboardController@jobIntro');
//forum
Route::get('forum', '\Riari\Forum\Frontend\Http\ControllersCategoryController@index');
//landing-pages

Route::get('/welcome/{slug}', ['as' => 'landing-pages', 'uses' => 'PagesController@landing']);
Route::get('/packages/{slug}', ['as' => 'landing-pages', 'uses' => 'PagesController@landing']);

Route::group(['middleware' => 'job',], function () {
//jobs
    Route::get('jobs/status/{job_id}/{status}', 'JobsController@returnBidStatus');
    Route::get('jobs/events/', 'JobsController@viewEvents');
    Route::get('jobs/events/{id}', 'JobsController@viewEventJobs');
    Route::get('jobs/child-class', 'JobsController@childClass');
    Route::get('jobs/create', 'JobsController@create');
    Route::get('jobs/{id}', 'JobsController@show');
    Route::post('jobs/{id}', 'JobsController@show');
    Route::resource('jobs', 'JobsController');
    Route::resource('bids', 'BidsController');
    Route::get('vendr/jobs', 'JobsController@jobsBoard');
    Route::get('jobs/create/success/{token}', 'JobsController@jobSuccess');
    Route::get('jobs/get/user_number_blade', 'JobsController@user_number_blade');
    Route::get('jobs/get/job_posted', 'JobsController@jobPosted');
    Route::get('jobs/restore/{id}', 'JobsController@restore');

//bids
    Route::get('job/bid/{id}', ['as' => 'job.view', 'uses' => 'JobsController@viewJob']);
    Route::post('job/bid/{id}', ['as' => 'job.bid', 'uses' => 'JobsController@bidJob']);
    Route::post('bids/change-status', ['as' => 'bids.change_status', 'uses' => 'BidsController@change_bid_status']);
});

//budget tool
Route::resource('budget', 'BudgetController');
Route::post('bids/status/{id}', 'JobsController@status');

Route::group(['middleware' => 'auth:all'], function () {
    //dashboard
    Route::resource('dashboard', 'DashboardController');
    Route::get('/dashstats', ['as' => 'dashstats', 'uses' => 'DashboardController@getUserStats']);
});


//contact
Route::get('contact', ['as' => 'contact', 'uses' => 'PagesController@getContact']);
Route::get('contact-us', ['as' => 'contact-us', 'uses' => 'PagesController@getContactAgain']);
Route::post('contact', 'PagesController@sendContactInfo');
Route::get('advertise', ['as' => 'advertise', 'uses' => 'PagesController@getAdvertise']);
Route::get('reviews', ['as' => 'reviews', 'uses' => 'PagesController@getReviews']);

//Terms & conditions
Route::get('terms-and-conditions', ['as' => 'terms', 'uses' => 'PagesController@getTerms']);

//Policy
Route::get('privacy-policy', ['as' => 'policy', 'uses' => 'PagesController@getPolicy']);

//About
Route::get('about', ['as' => 'about', 'uses' => 'PagesController@getAbout']);
//Blog
Route::get('/blog', ['as' => 'blog', 'uses' => 'BlogController@index']);
Route::get('/blog/{slug}', 'BlogController@showPost');

//Honeymoon Subscribe
Route::post('/honeymoon-subscription/create', 'PagesController@saveSubscription');
Route::get('honeymoon-subscription', ['middleware' => 'auth:administrator', 'uses' => 'PagesController@viewSubscriptions']);

//Admin
Route::group(['namespace' => 'Admin', 'middleware' => 'auth:administrator'], function () {
    Route::resource('admin/boosts', 'BoostsController');
    Route::resource('admin/boostcategories', 'BoostsCategoryController');
    Route::get('admin/page', 'AdminController@getHome');
    Route::resource('admin/post', 'PostController', ['except' => 'show']);
    Route::resource('admin/category', 'CategoryController', ['except' => 'show']);
    Route::get('admin/upload', 'UploadController@index');
    Route::post('admin/upload/file', 'UploadController@uploadFile');
    Route::delete('admin/upload/file', 'UploadController@deleteFile');
    Route::post('admin/upload/folder', 'UploadController@createFolder');
    Route::delete('admin/upload/folder', 'UploadController@deleteFolder');
    Route::resource('classification', 'ClassificationController');

});
Route::group(['middleware' => 'auth:administrator'], function () {
    Route::get('/sms/get', 'Admin\ReportsController@getNotifications'); 
    Route::get('/sms/post', 'Admin\ReportsController@postNotifications'); 
    Route::get('admin/jobs/reports', 'Admin\ReportsController@jobs_reports');
    Route::get('admin/jobs-credits/reports', 'Admin\ReportsController@job_credits_reports');
    Route::get('admin/classifications/subcategory', ['as' => 'subcategory.create', 'uses' =>'Admin\ClassificationController@add_root_parent']);
    Route::post('admin/classifications/subcategory', ['as' => 'subcategory.store', 'uses' => 'Admin\ClassificationController@store_root_parent']);
    Route::get('admin/classifications/sub-subcategory', 'Admin\ClassificationController@add_parent');
    Route::post('admin/classifications/sub-subcategory', ['as' => 'sub-subcategory.store', 'uses' => 'Admin\ClassificationController@store_parent']);
    Route::get('classification/category/{id}', ['as' => 'parent.edit', 'uses' => 'Admin\ClassificationController@edit_root_parent']);
    Route::post('classification/category/{id}', ['as' => 'parent.update', 'uses' => 'Admin\ClassificationController@update_root_parent']);
    Route::get('classification/subcategory/{id}', ['as' => 'child.edit', 'uses' => 'Admin\ClassificationController@edit_parent']);
    Route::post('classification/subcategory/{id}', ['as' => 'child.update', 'uses' => 'Admin\ClassificationController@update_parent']);
    Route::get('classification/get/bids', ['as' => 'classification.bids', 'uses' => 'Admin\ClassificationController@classification_bids']);
    Route::post('user-status/edit/{id}', ['as' => 'status.update', 'uses' => 'Admin\UsersController@status_update']);
    Route::get('users/search', ['as' => 'users.search', 'uses' => 'Admin\UsersController@search']);
    Route::resource('user-ads', 'UserAdsController');
    Route::get('jobs/update/extend_expiry/{id}', 'JobsController@extend_expiry');
});
Route::group(['middleware' => 'auth:all'], function () {
    Route::get('portfolio/{id}', ['as' => 'portfolio.get', 'uses' => 'Admin\UsersController@portfolioShow']);
    Route::post('portfolio/{id}', ['as' => 'portfolio', 'uses' => 'Admin\UsersController@portfolio']);
});

Route::resource('users', 'Admin\UsersController');
Route::get('user/options/{id}', ['as' => 'options.all', 'uses' => 'Admin\DescriptionsController@index']);
Route::get('user/options/{id}/create', ['as' => 'options.create', 'uses' => 'Admin\DescriptionsController@create']);
Route::post('user/options/{id}/create', ['as' => 'options.store', 'uses' => 'Admin\DescriptionsController@store']);
Route::get('user/options/{id}/edit', ['as' => 'options.edit', 'uses' => 'Admin\DescriptionsController@edit']);
Route::get('user/options/{id}/edit', ['as' => 'options.edit', 'uses' => 'Admin\DescriptionsController@edit']);
Route::post('user/options/{id}/edit', ['as' => 'options.update', 'uses' => 'Admin\DescriptionsController@update']);
Route::get('user/options/{id}/delete', ['as' => 'options.delete', 'uses' => 'Admin\DescriptionsController@destroy']);
Route::resource('admin/options', 'Admin\OptionsController');
Route::get('user/categories/{id}', ['as' => 'categories.all', 'uses' => 'Admin\UsersController@options_edit']);
Route::post('user/categories/{id}', ['as' => 'categories.update', 'uses' => 'Admin\UsersController@options_update']);

Route::get('plot/{id}', ['as' => 'admin.plot', 'middleware' => 'auth:administrator', 'uses' => 'AdminController@getChart']);
Route::get('admin/analytics/{id}', ['as' => 'admin.analytics', 'middleware' => 'auth:administrator', 'uses' => 'AdminController@analytics']);
Route::get('send-verify/{id}', ['as' => 'users.verify', 'middleware' => 'auth:administrator', 'uses' => 'Admin\UsersController@sendVerify']);
Route::post('verify/sms/{user_id}', ['as' => 'users.verify-sms', 'uses' => 'Admin\UsersController@process_sms_verification']);

//Auth
$a = 'auth.';
Route::get('/register/user-type', ['as' => $a . 'user-type', 'uses' => 'Auth\AuthController@getUserType']);
Route::get('/login', ['as' => $a . 'login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('/login', ['as' => $a . 'login-post', 'uses' => 'Auth\AuthController@postLogin']);
Route::post('/reviewLogin', ['as' => $a . 'login-review', 'uses' => 'Auth\AuthController@reviewLogin']);
Route::get('/register', ['as' => $a . 'register', 'uses' => 'Auth\AuthController@getRegister']);
Route::post('/register', ['as' => $a . 'register-post', 'uses' => 'Auth\AuthController@postRegister']);
Route::get('/vendor-register', ['as' => $a . 'vendor-register', 'uses' => 'Admin\UsersController@create']);
Route::post('/vendorRegister', ['as' => $a . 'vendorRegister-post', 'uses' => 'Auth\AuthController@postVendorRegister']);
Route::get('/password', ['as' => $a . 'password', 'uses' => 'Auth\PasswordResetController@getPasswordReset']);
Route::get('/claim-business', ['as' => $a . 'claim-account', 'uses' => 'Auth\PasswordResetController@claimAccount']);
Route::post('/password', ['as' => $a . 'password-post', 'uses' => 'Auth\PasswordResetController@postPasswordReset']);
Route::get('/password/{token}', ['as' => $a . 'reset', 'uses' => 'Auth\PasswordResetController@getPasswordResetForm']);
Route::post('/password/{token}', ['as' => $a . 'reset-post', 'uses' => 'Auth\PasswordResetController@postPasswordResetForm']);

//Social Login
$s = 'social.';
Route::get('/social/redirect/{provider}', ['as' => $s . 'redirect', 'uses' => 'Auth\AuthController@getSocialRedirect']);
Route::get('/social/handle/{provider}', ['as' => $s . 'handle', 'uses' => 'Auth\AuthController@getSocialHandle']);


Route::group(['prefix' => 'admin', 'middleware' => 'auth:administrator'], function () {
    $a = 'admin.';
    Route::get('/', ['as' => $a . 'home', 'uses' => 'AdminController@getHome']);
    Route::get('/vendor/report', 'AdminController@TopVendors');
    Route::get('/activities/report', ['as' => 'activities-report', 'uses' => 'AdminController@allActivities']);
    Route::get('/userlist', 'Admin\UsersController@getUsers');
    Route::get('/users/delete/{id}', 'Admin\UsersController@destroy');
    Route::get('/users/restore/{id}', 'Admin\UsersController@restore');
    Route::get('/links', ['as' => 'admin.links', 'uses' => 'AdminController@index']);
});

Route::group(['prefix' => 'vndor', 'middleware' => 'auth:vendor'], function () {
    $a = 'vendor.';
    Route::get('/', ['as' => $a . 'home', 'uses' => 'VendorController@getHome']);
});

Route::group(['prefix' => 'user', 'middleware' => 'auth:user'], function () {
    $a = 'user.';
    Route::get('/', ['as' => $a . 'home', 'uses' => 'UserController@getHome']);
});

Route::group(['middleware' => 'auth:all'], function () {
    $a = 'authenticated.';
    Route::get('/logout', ['as' => $a . 'logout', 'uses' => 'Auth\AuthController@getLogout']);
});

//Confirm email
Route::get('register/verify/{confirmationCode}', ['as' => 'confirmation_path', 'uses' => 'Auth\AuthController@confirm']);

//Email rating
Route::get('/email/{rating}/{token}', ['as' => 'email_rating', 'uses' => 'AdminController@rating']);

//Messaging
Route::group(['prefix' => 'messages'], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
    Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    Route::get('{id}/read', ['as' => 'messages.read', 'uses' => 'MessagesController@read']);
    Route::get('unread', ['as' => 'messages.unread', 'uses' => 'MessagesController@unread']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
    Route::put('/send/{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
});

//Gallery
Route::group(['prefix' => 'gallery', 'before' => 'auth'], function () {
    Route::get('/list', ['as' => 'gallery.list', 'uses' => 'GalleryController@viewGalleryList']);
});

//search
Route::get('/search', ['as' => 'search', 'uses' => 'TagsController@mainSearch']);
Route::get('/find/{class_id}/{mainSearch}', ['as' => 'categoryLocation', 'uses' => 'TagsController@fetchSubCategory']);
Route::get('/find', 'TagsController@fetchSearchCategory');
Route::get('/search/{mainSearch}/{category}', 'TagsController@refineSearch');
Route::get('/vendorlistings', 'Admin\UsersController@vendorListings');

Route::group(['middleware' => 'auth:all',], function () {
    Route::resource('gallery/save', 'GalleryController@saveGallery');
    Route::get('gallery/delete/{id}', 'GalleryController@deleteGallery');
    Route::get('gallery/view-vid/{id}', 'GalleryController@viewGallerypics');
    Route::resource('video/do-upload', 'GalleryController@videoUploads');
    Route::resource('video/link-upload', 'GalleryController@LinkUploads');
    Route::get('gallery/edit/{id}', 'GalleryController@editGallery');
    Route::post('gallery/update/{id}', 'GalleryController@updateGallery');
    Route::post('gallery/output-tag', 'TagsController@outputTags');
});
//Admin Gallery Stuff
Route::group(['middleware' => 'auth:administrator',], function () {
    Route::resource('locations', 'Admin\LocationsController');
    Route::get('vendor/gallery/{id}', 'GalleryController@AdminViewGallery');
    Route::get('/vendor/credit-vendor/{id}', 'Admin\CreditController@create');
    Route::post('/vendor/credit-vendor/{id}', ['as' => 'vendor.credit-vendor', 'uses' => 'Admin\CreditController@store']);
});
//Gallery routes for both admin and vendor
Route::post('image/do-upload', 'GalleryController@doImageUpload');
Route::get('gallery/view/{id}', 'GalleryController@viewGallerypics');
Route::get('gallery/videos', 'TagsController@getVideos');
Route::get('image/delete/{id}', 'TagsController@deleteImage');


Route::resource('gallery/inspiration', 'TagsController@viewInspirationPage');
Route::resource('gallery/search', 'TagsController@searchInspirationPage');
Route::post('/idea/add', 'GalleryController@addIdea');


//vendors
Route::get('/{slug}', ['as' => 'vendor.view', 'uses' => 'VendorController@viewClassification']);
Route::get('/vendor/{slug}', ['as' => 'vendor.show', 'uses' => 'VendorController@showVendorAgain']);
Route::get('/vendors/{slug}', ['as' => 'vendor.show', 'uses' => 'VendorController@showVendor']);
Route::post('/vendor/{slug}/', ['as' => 'vendor.review', 'middleware' => 'auth:all', 'uses' => 'VendorController@saveReview']);
Route::post('/send-advert', ['middleware' => 'auth:all', 'uses' => 'VendorController@sendAdvert']);
Route::get('/contact/getvendors/{id}', 'VendorController@multipleVendors');
Route::get('/socials/{type}/{number}/{cookie_name}', 'VendorController@socialProof');


//profile
Route::group(['before' => 'auth'], function () {
    Route::get('profile/{id}', ['as' => 'profile', 'before' => 'auth', 'uses' => 'ProfileController@getVendorProfile']);
    Route::get('profile/edit/{id}', ['as' => 'edit.profile', 'before' => 'auth', 'uses' => 'ProfileController@editVendorProfile']);
    Route::post('profile/edit/{id}', ['as' => 'update.profile', 'before' => 'auth', 'uses' => 'ProfileController@updateVendorProfile']);
});

Route::post('/review-request', ['as' => 'review-request', 'before' => 'auth', 'uses' => 'VendorController@reviewRequest']);
Route::get('/reset/first/{id}', ['as' => 'firstPass', 'uses' => 'Auth\PasswordResetController@postFirstPass']);

//Activity Tracker
Route::get('/admin/log', ['as' => 'users.log', 'middleware' => 'auth:administrator', 'uses' => 'ActivitiesController@show']);
Route::post('/admin/messages', ['as' => 'admin.messages', 'middleware' => 'auth:administrator', 'uses' => 'MessagesController@messages']);
Route::get('/admin/messages', ['as' => 'admin.messages', 'middleware' => 'auth:administrator', 'uses' => 'MessagesController@messages']);
Route::get('/admin/users/{id}', ['middleware' => 'auth:administrator', 'uses' => 'Admin\UsersController@show']);
Route::post('/admin/leads', ['as' => 'admin.leads', 'middleware' => 'auth:administrator', 'uses' => 'MessagesController@threads']);
Route::get('/admin/leads', ['as' => 'admin.leads', 'middleware' => 'auth:administrator', 'uses' => 'MessagesController@threads']);

Route::group(['middleware' => 'auth:administrator'], function () {
    Route::resource('admin/bundles', 'Admin\CreditBundlesController');
});
Route::get('/shop/credits', ['as' => 'vendor-credits', 'middleware' => 'auth:vendor', 'uses' => 'Admin\CreditBundlesController@vendor_credits']);

Route::group(['prefix' => 'admin', 'middleware' => 'auth:administrator'], function () {
    Route::get('/reports/credits/', 'Admin\ReportsController@vendor_balance');
    Route::get('/reports/credits/user/{id}', 'Admin\ReportsController@vendor_balance');
    Route::get('/reports/credits/purchased', 'Admin\ReportsController@purchased_credits');
    Route::get('/reports/credits/under_hundred', ['as' => 'credits.under_hundred', 'uses' => 'Admin\ReportsController@under_hundred']);
    Route::get('/reports/rankings/', 'Admin\ReportsController@ranked_vendors');
    Route::get('/reports/free_credit_expiry/', 'Admin\ReportsController@free_credit_expiry');
    Route::resource('cron_tasks', 'CronTaskController', [
        'names' => [
            'create' => 'cron_tasks.create',
            'store' => 'cron_tasks.store',
            'index' => 'cron_tasks.index',
            'show' => 'cron_tasks.show',
            'edit' => 'cron_tasks.edit',
            'update' => 'cron_tasks.update',
            'destroy' => 'cron_tasks.destroy',
        ],
    ]);
    Route::resource('cron_schedules', 'CronScheduleController', [
        'names' => [
            'create' => 'cron_schedules.create',
            'store' => 'cron_schedules.store',
            'index' => 'cron_schedules.index',
            'show' => 'cron_schedules.show',
            'edit' => 'cron_schedules.edit',
            'update' => 'cron_schedules.update',
            'destroy' => 'cron_schedules.destroy',
        ],
    ]);

});
Route::get('api/category-dropdown', 'AdminShop\CategoryController@categoryDropDownData');
Route::get('api/classification-dropdown', 'Admin\ClassificationController@categoryDropDownData');

//Todo
Route::group(['prefix' => 'user', 'middleware' => 'auth:all'], function () {
    Route::resource('/todo', 'TodoController');
    Route::get('todo/view/{id}', ['as' => 'todo.view', 'uses' => 'TodoController@view']);
    Route::post('todo/status/{id}', ['as' => 'todo.status', 'uses' => 'TodoController@status']);
    Route::post('todo/note/{id}', ['as' => 'todo.note', 'uses' => 'TodoController@add_note']);
    Route::post('todo/add-subtask/', ['as' => 'todo.subtask', 'uses' => 'TodoController@subtask']);
});

//FileEntry
Route::group(['middleware' => 'auth:all'], function () {
    Route::post('fileentry/saveS3', ['as' => 'saveS3', 'uses' => 'FileEntryController@saveS3']);
});
Route::get('fileentry', 'FileEntryController@index');
Route::get('fileentry/get/{filename}', ['as' => 'getentry', 'uses' => 'FileEntryController@get']);
Route::post('fileentry/add', ['as' => 'addentry', 'uses' => 'FileEntryController@add']);
Route::get('fileentry/download/{filename}', ['as' => 'downloadentry', 'uses' => 'FileEntryController@download']);

/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
*/
Route::get('/api', function () {
    return view('api.welcome');
});

Route::group(['prefix' => 'api', 'namespace' => 'API', 'middleware' => 'auth:administrator'], function () {
    Route::group(['prefix' => 'v1'], function () {
        require config('infyom.laravel_generator.path.api_routes');
    });
});