<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BreakdownDetails extends Model
{
        /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'breakdown_details';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['item_name','unit_price','description','quantity','bid_id','user_id','cat_id'];

    public function Classification()
    {
        return $this->belongsTo('App\Classification', 'cat_id');
    }
}
