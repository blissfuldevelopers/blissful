<?php

namespace App;

use App\Services\Markdowner;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Post extends Model
{

    protected $dates = ['published_at'];

    protected $fillable = [
    'title', 'subtitle', 'content_raw', 'page_image', 'meta_description',
    'layout', 'is_draft', 'published_at',];

    /**
     * The many-to-many relationship between posts and categories.
     *
     * @return BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category', 'post_category_pivot');
    }

    /**
     * Set the title attribute and automatically the slug
     *
     * @param string $value
     */
    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;

        if (! $this->exists) {
            $this->setUniqueSlug($value, '');
        }
    }

    /**
     * Recursive routine to set a unique slug
     *
     * @param string $title
     * @param mixed $extra
     */
    protected function setUniqueSlug($title, $extra)
    {
        $slug = str_slug($title.'-'.$extra);

        if (static::whereSlug($slug)->exists()) {
            $this->setUniqueSlug($title, $extra + 1);
            return;
        }

        $this->attributes['slug'] = $slug;
    }

    /**
     * Set the HTML content automatically when the raw content is set
     *
     * @param string $value
     */
    public function setContentRawAttribute($value)
    {
        $markdown = new Markdowner();

        $this->attributes['content_raw'] = $value;
        $this->attributes['content_html'] = $markdown->toHTML($value);
    }

    /**
     * Sync category relation adding new categories as needed
     *
     * @param array $categories
     */
    public function syncCategories(array $categories)
    {
      Category::addNeededCategories($categories);

      if (count($categories)) {
        $this->categories()->sync(
          Category::whereIn('category', $categories)->lists('id')->all()
        );
        return;
      }

      $this->categories()->detach();
    }

    /**
	   * Return the date portion of published_at
	   */
	  public function getPublishDateAttribute($value)
	  {
	    return $this->published_at->format('M-j-Y');
	  }

	  /**
	   * Return the time portion of published_at
	   */
	  public function getPublishTimeAttribute($value)
	  {
	    return $this->published_at->format('g:i A');
	  }

	  /**
	   * Alias for content_raw
	   */
	  public function getContentAttribute($value)
	  {
	    return $this->content_raw;
	  }

    // Add the 4 methods below to the class
  /**
   * Return URL to post
   *
   * @param category $category
   * @return string
   */
  public function url(Category $category = null)
  {
    $url = url('blog/'.$this->slug);
    if ($category) {
      $url .= '?category='.urlencode($category->category);
    }

    return $url;
  }

  /**
   * Return array of category links
   *
   * @param string $base
   * @return array
   */
  public function categoryLinks($base = '/blog?category=%category%')
  {
    $categories = $this->categories()->lists('category');
    $return = [];
    foreach ($categories as $category) {
      $url = str_replace('%category%', urlencode($category), $base);
      $return[] = '<a href="'.$url.'">'.e($category).'</a>';
    }
    return $return;
  }

  /**
   * Return next post after this one or null
   *
   * @param category $category
   * @return Post
   */
  public function newerPost(Category $category = null)
  {
    $query =
      static::where('published_at', '>', $this->published_at)
        ->where('published_at', '<=', Carbon::now())
        ->where('is_draft', 0)
        ->orderBy('published_at', 'asc');
    if ($category) {
      $query = $query->whereHas('categories', function ($q) use ($category) {
        $q->where('category', '=', $category->category);
      });
    }

    return $query->first();
  }

  /**
   * Return older post before this one or null
   *
   * @param category $category
   * @return Post
   */
  public function olderPost(Category $category = null)
  {
    $query =
      static::where('published_at', '<', $this->published_at)
        ->where('is_draft', 0)
        ->orderBy('published_at', 'desc');
    if ($category) {
      $query = $query->whereHas('categories', function ($q) use ($category) {
        $q->where('category', '=', $category->category);
      });
    }

    return $query->first();
  }

}
