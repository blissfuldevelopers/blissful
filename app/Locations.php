<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locations extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'locations';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','address','lat','lng','user_id'];

    public function jobs()
    {
        return $this->hasMany('App\Job', 'event_location');
    }
    public function user()
    {
        return $this->hasOne('App\User');
    }


}
