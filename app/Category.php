<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  protected $fillable = [
    'category', 'title', 'subtitle', 'page_image', 'meta_description',
    'reverse_direction',
  ];

  /**
   * The many-to-many relationship between categories and posts.
   *
   * @return BelongsToMany
   */
  public function posts()
  {
    return $this->belongsToMany('App\Post', 'post_category_pivot');
  }

  /**
   * Add any categories needed from the list
   *
   * @param array $categories List of categories to check/add
   */
  public static function addNeededCategories(array $categories)
  {
    if (count($categories) === 0) {
      return;
    }

    $found = static::whereIn('category', $categories)->lists('category')->all();

    foreach (array_diff($categories, $found) as $category) {
      static::create([
        'category' => $category,
        'title' => $category,
        'subtitle' => 'Subtitle for '.$category,
        'page_image' => '',
        'meta_description' => '',
        'reverse_direction' => false,
      ]);
    }
  }
  /**
   * Return the index layout to use for a category
   *
   * @param string $category
   * @param string $default
   * @return string
   */
  public static function layout($category, $default = 'blog.layouts.index')
  {
      $layout = static::whereCategory($category)->pluck('layout');

      return $layout ?: $default;
  }
}
