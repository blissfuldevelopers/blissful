<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['location', 'description', 'phone','profilePic','user_id'];
    
    public function getClassificationListAttribute(){
        return $this->classifications->lists('id')->toArray();
    }

    public function user(){

        return $this->belongsTo('App\User');

    }

    public function reviews()
    {
        return $this->hasMany('App\Review','profile_id','user_id');
    }
    public function recalculateRating($rating)
    {
        $reviews = $this->reviews()->notSpam()->approved();
        $avgRating = $reviews->avg('rating');
        $this->rating_cache = round($avgRating,1);
        $this->rating_count = $reviews->count();
        $this->save();
    }

}
