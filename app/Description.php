<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Description extends Model
{
    use SoftDeletes;
    protected $table = 'descriptions';

    /**
     * The database primary key value.
     *
     * @var string
     */
    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    protected $fillable = ['classification_id', 'option_id','description_detail','user_id'];

    public function classification(){

        return $this->belongsTo('App\Classification');

    }
    public function option()
    {
        return $this->belongsTo('App\Models\Option', 'option_id');
    }
}
