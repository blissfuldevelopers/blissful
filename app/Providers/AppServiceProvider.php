<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Event;
use App\Events\UserWasCreated;
use App\User;
use App\Models\ShopCategory;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
//        User::created(function ($user) {
//            Event::fire(new UserWasCreated($user));
//        });
        $category = ShopCategory::get()->toArray();
        view()->share('category', $category);

        Blade::directive('truncateHTML', function($expression){

            list($string, $limit) = explode(',',str_replace(['(',')',' '], '', $expression));
            $end='...';
            $with_html_count = strlen($string);
            $without_html_count = strlen(strip_tags($string));
            $html_tags_length = $with_html_count-$without_html_count;

            return $string;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}
