<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bookmarks extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id';
	   /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bookmarks';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','vendor_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function vendor(){
    	return $this->belongsTo('App\User', 'vendor_id');
    }
}
