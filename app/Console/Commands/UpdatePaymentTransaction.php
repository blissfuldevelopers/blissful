<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Libraries\pesapal;

class UpdatePaymentTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payments:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Pending Payment By Querying the Gateway\'s QueryStatus URL';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->pending_pesapal();
    }


    public function pending_pesapal()
    {
        $status = pesapal::updateAllPendingOrders();
    }
}
