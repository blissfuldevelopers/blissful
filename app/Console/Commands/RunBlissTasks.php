<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Libraries\blissful\blisstask;

class RunBlissTasks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blisstasks:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run any / all Bliss tasks that have been defined in DB according to the schedule defined in DB';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Sending Scheduled Reminders');
        BlissTask::run_tasks_in_db();
        $this->info('Scheduled Reminders sent (or set in the cron_messages table)...');
    }
}
