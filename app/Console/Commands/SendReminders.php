<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Libraries\blissful\blissreminder;

class SendReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminders:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert any reminders that are meant to be sent within the next 30 mins in the cron_messages table with the correct send datetime';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Sending Scheduled Reminders');
        BlissReminder::send_reminders_in_db();
        $this->info('Scheduled Reminders sent (or set in the cron_messages table)...');
    }
}
