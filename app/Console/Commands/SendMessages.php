<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Libraries\blissful\blissmessage;

class SendMessages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'messages:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send any unsent messages in cron_messages table';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Sending Cron Messages');
        BlissMessage::sendCronMessages();
        $this->info('Should be done sending them...');
    }
}
