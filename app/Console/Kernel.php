<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\UpdatePaymentTransaction::class,
        \App\Console\Commands\SendMessages::class,
        \App\Console\Commands\RunBlissTasks::class,
//        \App\Console\Commands\SendReminders::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //let it be the first thing to run
        //to send any messages created by other tasks
        // and make sure at least it works
        // $schedule->command('messages:send')
        //          ->everyMinute()
        //          ->withoutOverlapping();

        $schedule->command('payments:update')
                 ->everyFiveMinutes() //to make sure it doesn't overlap with anything currently running
                 ->withoutOverlapping();

//        $schedule->command('reminders:send')
//                 ->cron('0,15 * * * * *')
//                 ->withoutOverlapping();

        $schedule->command('blisstasks:run')
                 ->everyMinute()
                 ->withoutOverlapping();
    }


}
