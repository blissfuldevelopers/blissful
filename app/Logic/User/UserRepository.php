<?php namespace App\Logic\User;

use App\Logic\Mailers\UserMailer;
use App\Role;
use App\User;
use App\Password;
use App\Profile;
use Hash, Carbon\Carbon;
use Laracasts\Flash\Flash;
use App\Activity;
use Mixpanel;
use App\Jobs\fetchUserImage;
class UserRepository {

    protected $userMailer;

    public function __construct( UserMailer $userMailer )
    {
        $this->userMailer = $userMailer;
    }

    public function register( $data )
    {
            
                $token = str_random(30);
                $user = new User;
                $user->email            = $data['email'];
                $user->first_name       = ucfirst($data['first_name']);
                $user->last_name        = ucfirst($data['last_name']);
                $user->password         = Hash::make($data['password']);
                $user->token            = $token;
                (array_key_exists('messenger', $data) && $data['messenger'] ==1) ?  $user->verified = 1 : '';
                $user->save();

                

                //profile
                $profile = new Profile;
                $profile->user_id=$user->id;
                $profile->phone = $data['phone'];
                $profile->save();

                //dispatch this to fetch the user img
                dispatch(new fetchUserImage($user->id));
                
                //Assign Role
                $role = Role::whereName('user')->first();
                $user->assignRole($role);


            if(!$data['messenger'] ==1) {

                $data = [
                    'first_name'    => $user->first_name,
                    'token'         => $token,
                    'subject'       => 'Verify your email address',
                    'email'         => $user->email,
                ];

                $this->userMailer->verify($user->email, $data);
            }
            else{

                

                $data = [
                    'first_name'    => $data['first_name'],
                    'token'         => $token,
                    'subject'       => 'Hello & welcome to blissful',
                    'email'         => $data['email'],
                ];

                $this->userMailer->verify_messaged($data['email'], $data);

            }

            //send verification SMS
            $user->send_sms_verification($user->id);

            //log this activity

            Activity::create([
                'subject_id'=> $user->id,
                'subject_type'=>'App\User',
                'name'=>'user_registered',
                'user_id'=> $user->id,
            ]);

            return $user;

    }

    public function resetPassword( User $user  )
    {
        $token = sha1(mt_rand());
        $password = new Password;
        $password->email = $user->email;
        $password->token = $token;
        $password->created_at = Carbon::now();
        $password->save();

        $data = [
            'first_name'    => $user->first_name,
            'token'         => $token,
            'subject'       => 'Password Reset Link',
            'email'         => $user->email,
        ];

        $this->userMailer->passwordReset($user->email, $data);
    }
    public function joinRequest( User $user  )
    {
        $token = sha1(mt_rand());
        $password = new Password;
        $password->email = $user->email;
        $password->token = $token;
        $password->created_at = Carbon::now();
        $password->save();

        $data = [
            'first_name'    => $user->first_name,
            'token'         => $token,
            'subject'       => 'Introducing a New & Improved Blissful - Take Your Business To The Next Level',
            'email'         => $user->email,
        ];

        $this->userMailer->joinRequest($user->email, $data);
    }
}