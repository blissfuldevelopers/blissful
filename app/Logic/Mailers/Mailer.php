<?php namespace App\Logic\Mailers;

abstract class Mailer
{

    public function sendTo($email, $subject, $fromEmail, $view, $data = [ ])
    {
        \Mail::queue($view, $data, function ($message) use ($email, $subject, $fromEmail) {

            if (!is_array($fromEmail)) {
                $message->from($fromEmail, 'Joy from Blissful');
            }
            else {
                $message->from($fromEmail[ 0 ], $fromEmail[ 1 ]);
            }

            $message->to($email)->subject($subject);
        });
    }


}