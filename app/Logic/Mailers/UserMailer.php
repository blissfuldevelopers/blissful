<?php namespace App\Logic\Mailers;

class UserMailer extends Mailer
{
    protected $fromEmail = 'happy@blissful.co.ke';

    public function verify($email, $data)
    {
        $view = 'emails.verify-email';
        $subject = $data[ 'subject' ];
        $fromEmail = 'happy@blissful.co.ke';

        $this->sendTo($email, $subject, $this->fromEmail, $view, $data);
    }

    public function verify_messaged($email, $data)
    {
        $view = 'emails.verify-messaged';
        $subject = $data[ 'subject' ];


        $this->sendTo($email, $subject, $this->fromEmail, $view, $data);
    }

    public function passwordReset($email, $data)
    {
        $view = 'emails.password-reset';
        $subject = $data[ 'subject' ];


        $this->sendTo($email, $subject, $this->fromEmail, $view, $data);
    }

    public function joinRequest($email, $data)
    {
        $view = 'emails.Join';
        $subject = $data[ 'subject' ];


        $this->sendTo($email, $subject, $this->fromEmail, $view, $data);
    }

    public function emailNotification($email, $data)
    {
        $view = 'emails.email-notification';
        $subject = $data[ 'subject' ];


        $this->sendTo($email, $subject, $this->fromEmail, $view, $data);
    }
    public function daily_jobs($data)
    {
        $view = 'emails.daily-jobs';
        $subject = $data[ 'subject' ];


        $this->sendTo($data[ 'email' ], $subject, $this->fromEmail, $view, $data);
    }

    public function emailReview($email, $data)
    {
        $view = 'emails.request-review';
        $subject = $data['subject'];


        $this->sendTo($email, $subject, $this->fromEmail, $view, $data);
    }

    public function contact($data)
    {
        $view = 'emails.contact';
        $subject = 'Blissful Message: ' . $data[ 'name' ];
        $fromEmail = $data[ 'email' ];


        $this->sendTo($this->fromEmail, $subject, $fromEmail, $view, $data);
    }

    public function advert($data)
    {
        $view = 'emails.advert';
        $subject = 'Blissful Message: ' . $data[ 'name' ];
        $fromEmail = $data[ 'email' ];


        $this->sendTo($this->fromEmail, $subject, $fromEmail, $view, $data);
    }

    public function simpleEmail($data)
    {
        $view = 'emails.simple-email';
        $data[ 'text' ] = ( !isset( $data[ 'text' ] ) && isset( $data[ 'message' ] ) ) ? $data[ 'message' ]
            : ( ( isset( $data[ 'text' ] ) ) ? $data[ 'text' ] : '' );

        $this->sendTo($data[ 'to' ], $data[ 'subject' ], $this->fromEmail, $view, $data);
    }
      public function Subscriptions($data)
    {
        $view = 'emails.honeymoon-subscription';
        $subject = 'Blissful Subscription: ' . $data[ 'user_name' ];
        $fromEmail = $data[ 'email' ];


        $this->sendTo($this->fromEmail, $subject, $fromEmail, $view, $data);
    }
     public function inReview($data)
    {
        $view = 'emails.jobsReviews';
        $subject = $data[ 'subject' ];
        $fromEmail = $data[ 'email' ];
        

        $this->sendTo($fromEmail, $subject, $this->fromEmail, $view, $data);
    }
    public function errorsEmail($data)
    {
        $view = 'emails.errors-email';
        $data[ 'text' ] = ( !isset( $data[ 'text' ] ) && isset( $data[ 'message' ] ) ) ? $data[ 'message' ]
            : ( ( isset( $data[ 'text' ] ) ) ? $data[ 'text' ] : '' );

        $this->sendTo($data[ 'to' ], $data[ 'subject' ], $this->fromEmail, $view, $data);
    }
    public function recommendEmail($data)
    {
        $view = 'emails.recommend';
        $subject = $data[ 'subject' ];


        $this->sendTo($data[ 'email' ], $subject, $this->fromEmail, $view, $data);
    }
    public function newsletterEmail($data)
    {
        $view = 'emails.newsletter';
        $subject = $data[ 'subject' ];

        $this->sendTo($data[ 'email' ], $subject, $this->fromEmail, $view, $data);
    }
}