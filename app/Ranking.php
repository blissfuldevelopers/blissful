<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Ranking extends Model

{
    protected $table = 'rankings';

    protected $fillable = ['ranking', 'details', 'user_id'];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function storeRankingForVendor($id, $details, $ranking)
    {
        $user = User::findorfail($id);

        $rankings = $user->Ranking ?: new Ranking;
        $rankings->user_id = $id;
        $rankings->details = $details;
        $rankings->ranking = $ranking;
        $user->rankings()->save($rankings);

        //recalculate rankings
        $user->recalculateRanking($ranking);
    }
}
