<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;

class ShopOrder extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shop_order';
    protected $primaryKey = 'id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'pesapal_transaction_tracking_id',
        'pesapal_merchant_reference',
        'pesapal_notification_type',
        'total',
        'phone',
        'status',
        'user_id',
        'shipping_fees',
    ];
    protected $hidden = [ 'created_at', 'updated_at' ];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public static function validate($data)
    {


        $rule = array(
            'name'        => 'required|unique:shop_category,name,NULL,id,category_id,' . $data[ 'category_id' ],
            'category_id' => 'required',
        );
        $messages = array(
            'required' => 'The :attribute field is required.',
            'unique'   => 'The :attribute already Exist.',
        );


        $data = Validator::make($data, $rule, $messages);

        return $data;
    }

    public static function validateUpdate($data, $id)
    {

        $rule = array(
            'name'        => 'required|unique:shop_category,name,' . $id . ',id,category_id,' . $data[ 'category_id' ],
            'category_id' => 'required',
        );

        $messages = array(
            'required' => 'The :attribute field is required.',
            'unique'   => 'The :attribute already Exist.',
        );

        $data = Validator::make($data, $rule, $messages);

        return $data;
    }

    public static function create(array $attributes = [ ], $products = null)
    {
        if (empty( $attributes )) {
            return false;
        }


        //check if this order
        $shop_order = parent::create($attributes);


        $attributes[ 'order_id' ] = $shop_order->id;

        foreach ($products as $product) {
            ShopOrderDetail::create($attributes, $product);
        }

        return $shop_order;
    }


    public function category()
    {
        return $this->hasOne('App\Models\ShopCategory', 'id', 'category_id');
    }

    public function details()
    {
        return $this->hasMany('App\Models\ShopOrderDetail', 'order_id', 'id');
    }

    public function credits()
    {
        return $this->hasMany('App\Credit');
    }

    public function user()
    {

        return $this->belongsTo('App\User');

    }
}
