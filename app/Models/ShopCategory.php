<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Validator;
use App\Models\ShopCategoryUserRole;
use Illuminate\Support\Facades\Auth;

class ShopCategory extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shop_category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'description','header_image','header_text','sub_header_image','sub_header_text','sub_header_text','sub_header_sub_text', 'category_slug','package'];

    protected $hidden = [ 'created_at', 'updated_at' ];
    protected $appends = [
        'for_user',
        'for_vendor',
        'for_admin',
    ];

    public function getForUserAttribute()
    {
        if ($this->hasUserRole('user')) {
            return $this->attributes[ 'for_user' ] = true;
        }

        if ($this->user_roles && count($this->user_roles) >= 1) {
            return $this->attributes[ 'for_user' ] = false;
        }

        return $this->attributes[ 'for_user' ] = true;

    }

    public function getForVendorAttribute()
    {
        return $this->attributes[ 'for_vendor' ] = (bool)$this->hasUserRole('vendor');
    }

    public function getForAdminAttribute()
    {
        return $this->attributes[ 'for_admin' ] = true;
    }


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public static function validate($data)
    {


        $rule = array(
            'name' => 'required|unique:shop_category',
            'description' => 'required'
        );

        $messages = array(
            'required' => 'The :attribute field is required.',
            'unique'   => 'The :attribute already Exist.',
        );


        $data = Validator::make($data, $rule, $messages);

        return $data;
    }

    public static function validateUpdate($data, $id)
    {

        $rule = array(
            'name' => 'required|unique:shop_category,name,' . $id . ',id',
        );

        $messages = array(
            'required' => 'The :attribute field is required.',
            'unique'   => 'The :attribute already Exist.',
        );

        $data = Validator::make($data, $rule, $messages);

        return $data;
    }

    public function subcategories()
    {
        return $this->hasMany('App\Models\ShopSubcategory','category_id','id');
    }

    public function user_roles()
    {
        return $this->belongsToMany('App\Models\ShopCategoryUserRole', 'role_shop_category', 'category_id', 'role_id')
                    ->withTimestamps();
    }

    public function user_type()
    {
        return $this->belongsToMany('App\Models\ShopCategoryUserRole', 'role_shop_category', 'category_id', 'role_id');
    }

    public function hasUserRole($name)
    {
        $name = ( is_numeric($name) ) ? ShopCategoryUserRole::findOrFail($name) : $name;

        foreach ($this->user_roles as $role) {
            if ($role->name == $name)
                return true;
        }

        return false;
    }

    public function assignUserRole($role)
    {

        $role = ( !is_numeric($role) ) ? ShopCategoryUserRole::whereName('user')->first() : $role;

        return $this->user_roles()->attach($role);
    }

    public function removeUserRole($role)
    {
        $role = ( !is_numeric($role) ) ? ShopCategoryUserRole::whereName('user')->first() : $role;

        return $this->user_roles()->detach($role);
    }

    /**
     * @param      $collection
     * @param null $user
     * @param bool $return_first
     *
     * @return mixed
     */
    public static function filterByUserRole($collection, $user = null, $return_first = false)
    {
        $user = ( !$user ) ? Auth::user() : $user;

        $filtered = $collection->filter(function ($model) use ($user) {

            if ($user) {
                if (!$user->can('view.shop')) {
                    return false;
                }

                if ($user->admin) {
                    return true;
                }

                if ($user->vendor) {
                    return $model->for_user || $model->for_vendor;
                }
            }

            return $model->for_user;
        });

        if ($return_first) {
            return $filtered->first();

        }

        return $filtered->all();
    }
}
