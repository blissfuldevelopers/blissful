<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cron\CronExpression;
use Carbon\Carbon;
use DB;

/**
 * @SWG\Definition(
 *      definition="CronSchedule",
 *      required={cron_task_id, repeat_start, repeat_stop, cron_min, cron_hour, cron_day_of_month, cron_month,
 *      cron_day_of_week},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cron_task_id",
 *          description="cron_task_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cron_min",
 *          description="cron_min",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cron_hour",
 *          description="cron_hour",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cron_day_of_month",
 *          description="cron_day_of_month",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cron_month",
 *          description="cron_month",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cron_day_of_week",
 *          description="cron_day_of_week",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class CronSchedule extends Model
{
    use SoftDeletes;

    public $table = 'cron_schedules';


    protected $dates = [ 'repeat_start', 'repeat_stop', 'deleted_at' ];


    public $fillable = [
        'cron_task_id',
        'repeat_start',
        'repeat_stop',
        'cron_min',
        'cron_hour',
        'cron_day_of_month',
        'cron_month',
        'cron_day_of_week',
        'created_by',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'cron_task_id'      => 'integer',
        'cron_min'          => 'string',
        'cron_hour'         => 'string',
        'cron_day_of_month' => 'string',
        'cron_month'        => 'string',
        'cron_day_of_week'  => 'string',
        'created_by'        => 'integer',
    ];

    protected static $db_attributes = [
        'cron_task_id',
        'repeat_start',
        'repeat_stop',
        'cron_min',
        'cron_hour',
        'cron_day_of_month',
        'cron_month',
        'cron_day_of_week',
        'created_by',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'cron_task_id' => 'required|exists:cron_task,id',
        'repeat_start' => 'required|date|after:yesterday',
        'repeat_stop'  => 'date|after:repeat_start',
        'repeat'       => 'in:@yearly,@annually,@monthly,@weekly,@daily,@hourly,@custom',
    ];

    protected $appends = [
        'cron_expression',
        'next_cron_run_date',
    ];

    protected static $cron_mappings = [
        '@yearly'   => '0 0 1 1 *',
        '@annually' => '0 0 1 1 *',
        '@monthly'  => '0 0 1 * *',
        '@weekly'   => '0 0 * * 0',
        '@daily'    => '0 0 * * *',
        '@hourly'   => '0 * * * *',
    ];

    public function getCronExpressionAttribute()
    {
        return $this->attributes[ 'cron_expression' ] = $this->cron_min . ' ' . $this->cron_hour . ' ' . $this->cron_day_of_month . ' ' . $this->cron_month . ' ' . $this->cron_day_of_week;
    }

    public function getNextCronRunDateAttribute()
    {
        $expression = CronExpression::factory($this->cron_expression);

        return $this->attributes[ 'next_cron_run_date' ] = $expression->getNextRunDate('now', 0, true)->format('Y-m-d H:i:s');
    }

    /**
     * @param        $date
     * @param string $expression
     *
     * @return string
     */
    public static function updateCronMappingForDate($date, $expression = '* * * * *')
    {
        if (!in_array($expression, static::$cron_mappings)) {
            return $expression; //not handling custom / complicated expressions...
        }
        $strtotime = strtotime($date);


        $from_date_cron_exp = [
            'min'          => intval(date('i', $strtotime)),
            'hour'         => date('G', $strtotime),
            'day_of_month' => date('j', $strtotime),
            'month'        => date('n', $strtotime),
            'day_of_week'  => date('w', $strtotime),
        ];

        list( $cron_exp[ 'min' ], $cron_exp[ 'hour' ], $cron_exp[ 'day_of_month' ], $cron_exp[ 'month' ], $cron_exp[ 'day_of_week' ] ) = explode(' ', $expression);

        $mapping = array_search($expression, static::$cron_mappings);
        //change the 0s & other numbers to the date's actual numbers
        //since * means ALL - we don't want to change those
        //  use this CRON format to guide you
        //      minute [0-59], hour [0-23], day of month, month [1-12|JAN-DEC], day of week
        switch ($mapping) {
            case  '@yearly': //0 0 1 1 *
            case  '@annually':
                $cron_exp[ 'min' ] = $from_date_cron_exp[ 'min' ];
                $cron_exp[ 'hour' ] = $from_date_cron_exp[ 'hour' ];
                $cron_exp[ 'day_of_month' ] = $from_date_cron_exp[ 'day_of_month' ];
                $cron_exp[ 'month' ] = $from_date_cron_exp[ 'month' ];
                break;
            case  '@monthly': //0 0 1 * *
                $cron_exp[ 'min' ] = $from_date_cron_exp[ 'min' ];
                $cron_exp[ 'hour' ] = $from_date_cron_exp[ 'hour' ];
                $cron_exp[ 'day_of_month' ] = $from_date_cron_exp[ 'day_of_month' ];
                break;
            case  '@weekly' : //0 0 * * 0
                $cron_exp[ 'min' ] = $from_date_cron_exp[ 'min' ];
                $cron_exp[ 'hour' ] = $from_date_cron_exp[ 'hour' ];
                $cron_exp[ 'day_of_week' ] = $from_date_cron_exp[ 'day_of_week' ];
                break;
            case  '@daily': //0 0 * * *
                $cron_exp[ 'min' ] = $from_date_cron_exp[ 'min' ];
                $cron_exp[ 'hour' ] = $from_date_cron_exp[ 'hour' ];
                break;
            case  '@hourly' : //0 * * * *
                $cron_exp[ 'min' ] = $from_date_cron_exp[ 'min' ];
                break;
        }

        return implode(' ', $cron_exp);
    }

    /**
     * @param array $attributes
     *
     * @return static
     */
    public static function create(array $attributes = [ ])
    {
        $attributes[ 'repeat_start' ] = date('Y-m-d H:i:s', strtotime($attributes[ 'repeat_start' ]));

        if (!isset( $attributes[ 'repeat' ] )
            || ( isset( $attributes[ 'repeat' ] ) && $attributes[ 'repeat' ] == '' )
        ) {
            //set the repeat to be daily (once)
            //AND set the stop date to be the end of that reminder's date
            $attributes[ 'repeat' ] = '@daily';
            $attributes[ 'repeat_stop' ] = date('Y-m-d', strtotime($attributes[ 'repeat_start' ])) . ' 59:59:59';
        }

        $expression = CronExpression::factory($attributes[ 'repeat' ]);

        if (isset( static::$cron_mappings[ $attributes[ 'repeat' ] ] )) {
            //then it's not a custom time
            // so have it run for the mapping from repeat_start
//            $expression->getNextRunDate($attributes[ 'repeat_start' ])->format('Y-m-d H:i:s');
            $expression = static::updateCronMappingForDate($attributes[ 'repeat_start' ], $expression);
        }

        list( $attributes[ 'cron_min' ], $attributes[ 'cron_hour' ], $attributes[ 'cron_day_of_month' ], $attributes[ 'cron_month' ], $attributes[ 'cron_day_of_week' ] ) = explode(' ', $expression);


        $attributes = array_only($attributes, static::$db_attributes);

        return parent::create($attributes);
    }


    /**
     * @param string $expression
     * @param int    $max_extra_mins
     *
     * @return bool
     */
    private static function isDue($expression = '', $max_extra_mins = 0)
    {
        $date = Carbon::now();

        if (CronExpression::factory($expression)->isDue($date->toDateTimeString())) {
            return true;
        }

        $max_extra_mins = ( $max_extra_mins > 60 ) ? 60 : $max_extra_mins; //just to keep within healthy time

        if ($max_extra_mins) {
            for ($i = 1; $i <= $max_extra_mins; $i++) {
                $new_date = $date->addMinutes($i);
                if (CronExpression::factory($expression)->isDue($new_date->toDateTimeString())) {
                    return true;
                }
            }
        }

        return false;
    }


    /**
     * Get all of the events on the schedule that are due.
     *
     * @param     $collection
     * @param int $extra_mins
     *
     * @return array
     */
    public static function dueEvents($collection, $extra_mins = 0)
    {
        $filtered = collect($collection)->filter(function ($model) use ($extra_mins) {
            return static::isDue($model->cron_expression, $extra_mins);
        });

        return $filtered->all();
    }

    /**
     * Scope a query to only get reminders.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeReminder($query)
    {
        return $query
            ->join('cron_tasks', function ($join) {
                $join->on('cron_schedules.cron_task_id', '=', 'cron_tasks.id');
            })
            ->join('cron_task_to_user', function ($join) {
                $join->on('cron_task_to_user.cron_task_id', '=', 'cron_tasks.id');
            })
            ->where('cron_tasks.type', '=', 'reminder')
            ->whereNull('cron_tasks.deleted_at')
            ->whereNull('cron_schedules.deleted_at')
            ->addSelect('cron_tasks.id as task_id')
            ->addSelect('cron_schedules.id as schedule_id')
            ->addSelect('cron_tasks.name as reminder')
            ->addSelect('cron_schedules.repeat_start')
            ->addSelect('cron_schedules.repeat_stop')
            ->withcronexpression();
    }

    /**
     * Scope a query to only get tasks.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTask($query)
    {
        return $query
            ->join('cron_tasks', function ($join) {
                $join->on('cron_schedules.cron_task_id', '=', 'cron_tasks.id');
            })
            ->where('cron_tasks.type', '=', 'task')
            ->whereNull('cron_tasks.deleted_at')
            ->whereNull('cron_schedules.deleted_at')
            ->addSelect('cron_tasks.id as task_id')
            ->addSelect('cron_schedules.id as schedule_id')
            ->addSelect('cron_tasks.name as task_name')
            ->addSelect('cron_schedules.repeat_start')
            ->addSelect('cron_schedules.repeat_stop')
            ->withcronexpression();
    }

    /**
     * Scope a query get the cron expression.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithcronexpression($query)
    {
        return $query->addSelect('cron_schedules.cron_min')
                     ->addSelect('cron_schedules.cron_hour')
                     ->addSelect('cron_schedules.cron_day_of_month')
                     ->addSelect('cron_schedules.cron_month')
                     ->addSelect('cron_schedules.cron_day_of_week');
    }


    public static function scopeWithusersdetails($query)
    {
        return $query
            ->join('users', function ($join) {
                $join->on('cron_task_to_user.user_id', '=', 'users.id');
            })
            ->join('profiles', function ($join) {
                $join->on('profiles.user_id', '=', 'users.id');
            })
            ->whereNull('users.deleted_at')
            ->addSelect('users.first_name')
            ->addSelect('users.last_name')
            ->addSelect('users.email')
            ->addSelect('profiles.phone');
    }

    public function message()
    {
        return $this->belongsTo('App\Models\CronTask', 'cron_task_id');
    }
}
