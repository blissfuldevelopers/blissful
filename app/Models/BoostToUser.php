<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BoostToUser extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'boost_user';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'boost_id', 'start_date', 'stop_date', 'order_id'];

    public static $rules = [
        'user_id'    => 'required',
        'start_date' => 'required|date|after:yesterday',
        'stop_date'  => 'required|date|after:start_date',
    ];

    /**
     * @param $boost
     * @param $user_id
     *
     * @return static
     */
    public static function addBoostToUser($ShopOrderDetail, $user_id)
    {
        $boost = $ShopOrderDetail->product->boost;

        $time_in_seconds = (int)($boost->days * 24 * 60 * 60);

        $date_interval = new \DateInterval('PT' . $time_in_seconds . 'S');

        if($last_boost = static::check_max_active_boost($boost->id, $user_id)) {

            $next_boost = strtotime($last_boost) + 1;
            $next_boost_date = date('Y-m-d H:i:s', $next_boost);

            $start_date = $next_boost_date;
            $stop_date = new \DateTime($next_boost_date);
            $stop_date->add($date_interval);
            $stop_date_string = $stop_date->format('Y-m-d H:i:s');
        } else {

            $start_date = date('Y-m-d H:i:s');
            $stop_date = new \DateTime();
            $stop_date->add($date_interval);
            $stop_date_string = $stop_date->format('Y-m-d H:i:s');
        }

        $attributes = [
            'user_id'    => $user_id,
            'boost_id'   => $boost->id,
            'order_id'   => $ShopOrderDetail->order->id,
            'start_date' => $start_date,
            'stop_date'  => $stop_date_string,
        ];

        return static::updateOrCreate([
                                          'user_id'  => $user_id,
                                          'boost_id' => $boost->id,
                                          'order_id' => $ShopOrderDetail->order->id,
                                      ], $attributes);

    }

    /**
     * @param $boost_id
     * @param $user_id
     *
     * @return mixed
     */
    public static function check_max_active_boost($boost_id, $user_id)
    {

        return static::where('user_id', $user_id)
                     ->where('boost_id', $boost_id)
                     ->where('stop_date', '>', \DB::raw('NOW()'))
                     ->max('stop_date');
    }
}
