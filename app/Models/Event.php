<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Event",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="desc",
 *          description="desc",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Event extends Model
{
    use SoftDeletes;

    public $table = "events";


    protected $dates = [ 'start', 'end', 'deleted_at' ];


    public $fillable = [
        "title",
        "start",
        "end",
        "user_id",
        "fullday",
        "desc",
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "desc" => "string",
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title'   => 'required',
        'start'   => 'required',
        'end'     => 'required',
        'fullday' => 'required',
    ];

    /**
     * Get the comments for the blog post.
     */
    public function jobs()
    {
        return $this->hasMany('App\Job');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
