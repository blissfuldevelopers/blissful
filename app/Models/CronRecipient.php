<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="CronRecipient",
 *      required={message_id},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="message_id",
 *          description="message_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="mobile_number",
 *          description="mobile_number",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="sent",
 *          description="sent",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class CronRecipient extends Model
{
    use SoftDeletes;

    public $table = 'cron_recipients';


    protected $dates = [ 'deleted_at' ];


    public $fillable = [
        'message_id',
        'mobile_number',
        'email',
        'sent',
    ];

    public static $db_attributes = [
        'message_id',
        'mobile_number',
        'email',
        'sent',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'message_id'    => 'integer',
        'mobile_number' => 'integer',
        'email'         => 'string',
        'sent'          => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'message_id'    => 'required|numeric|exists:cron_messages,id',
        'mobile_number' => 'numeric',
        'email'         => 'email',
        'sent'          => 'boolean',
    ];

    public function message()
    {
        return $this->belongsTo('App\Models\CronMessage', 'message_id');
    }

    public static function create(array $attributes = [ ])
    {
        foreach ($attributes as $key => $value) {
            if (!is_array($value)) {
                if ($key == 'mobile_number') {
                    $attributes[ $key ] = clean_mobile_number($value);
                }

                $attributes[ $key ] = ( $value === "<<NOW>>" )
                    ? date('Y-m-d H:i:s')
                    : ( ( $value === "<<NULL>>" ) ? "NULL" : $value );
            }
        }

        return parent::create($attributes);
    }
}
