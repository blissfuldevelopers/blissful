<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopProduct extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shop_product';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'description',
        'category_id',
        'sub_category_id',
        'location',
        'price',
        'seller_id',
        'picture',
        'highlights',
        'qty',
        'tnc',
        'tag',
        'status',
        'sale_price',
        'discount',
        'type',
        'expiry_date',
        'product_slug',
        'payment_method',
    ];
    public static $db_attributes = [
        'id',
        'name',
        'description',
        'category_id',
        'sub_category_id',
        'location',
        'price',
        'seller_id',
        'picture',
        'highlights',
        'qty',
        'tnc',
        'tag',
        'status',
        'sale_price',
        'discount',
        'type',
        'expiry_date',
        'payment_method',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
    ];


    public function category()
    {
        return $this->hasOne('App\Models\ShopCategory', 'id', 'category_id');
    }

    public function subCategory()
    {
        return $this->hasOne('App\Models\ShopSubcategory', 'id', 'sub_category_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'seller_id');
    }

    public function credit_bundle()
    {
        return $this->hasOne('App\CreditBundle', 'product_id');
    }

    public function boost()
    {
        return $this->hasOne('App\Models\Boost', 'product_id');
    }

    public function subscriptions_type()
    {
        return $this->hasOne('App\Subscriptions_type', 'product_id');
    }

    public function orders()
    {
        return $this->hasMany('App\ShopOrderDetail', 'product_id');
    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public static function validate($data)
    {

        $rule = array(
            'name'        => 'required',
            'category_id' => 'required',
            'type'        => 'required',
        );

        $messages = array(
            'required' => 'The :attribute field is required.',
            'unique'   => 'The :attribute already Exist.',
        );


        $data = Validator::make($data, $rule, $messages);

        return $data;
    }

    public static function validateUpdate($data, $id)
    {

        $rule = array(
            'name'        => 'required',
            'category_id' => 'required',
            'type'        => 'required',
        );

        $messages = array(
            'required' => 'The :attribute field is required.',
            'unique'   => 'The :attribute already Exist.',
        );

        $data = Validator::make($data, $rule, $messages);

        return $data;
    }

    public static function fetchShopProducts()
    {
        return ShopProduct::fetchShopProductQuery()
                          ->orderBy('created_at', 'DESC');
    }

    public static function fetchRecentProducts($ids, $take = 4)
    {
        return static::fetchShopProductQuery()
                     ->whereIn('id', $ids)
                     ->take($take);

    }

    public static function fetchShopProductQuery()
    {
        return ShopProduct::with(['category', 'subCategory', 'user']);
    }

    /**
     * @param      $collection
     * @param null $user
     * @param bool $return_first
     *
     * @return mixed
     */
    public static function filterByUserRole($collection, $user = null, $return_first = false)
    {
        $user = (!$user) ? Auth::user() : $user;

        $filtered = $collection->filter(function ($model) use ($user) {
            if($user) {
                if(!$user->can('view.shop')) {
                    return false;
                }

                if($user->admin) {
                    return true;
                }

                if($user->vendor) {
                    return $model->category->for_user || $model->category->for_vendor;
                }
            }

            return $model->category->for_user;
        });

        if($return_first) {
            return $filtered->first();

        }

        return $filtered->all();
    }

    public function images()
    {
        return $this->hasMany('App\ProductImage', 'product_id');
    }
}
