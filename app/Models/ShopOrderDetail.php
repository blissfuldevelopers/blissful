<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Validator;

class ShopOrderDetail extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shop_order_detail';
    protected $primaryKey = 'id';

    public function category()
    {
        return $this->hasOne('App\Models\ShopCategory', 'id', 'category_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\ShopOrder');
    }

    public function product()
    {
        return $this->hasOne('App\Models\ShopProduct', 'id', 'product_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'id', 'order_id', 'name', 'price', 'discount_price', 'discount', 'qty', 'total', 'product_id' ];
    protected $hidden = [ 'created_at', 'updated_at' ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public static function validate($data)
    {


        $rule = array(
            'name'        => 'required|unique:shop_order_detail,name,NULL,id,category_id,' . $data[ 'category_id' ],
            'category_id' => 'required',
        );
        $messages = array(
            'required' => 'The :attribute field is required.',
            'unique'   => 'The :attribute already Exist.',
        );


        $data = Validator::make($data, $rule, $messages);

        return $data;
    }

    public static function validateUpdate($data, $id)
    {

        $rule = array(
            'name'        => 'required|unique:shop_order_detail,name,' . $id . ',id,category_id,' . $data[ 'category_id' ],
            'category_id' => 'required',
        );

        $messages = array(
            'required' => 'The :attribute field is required.',
            'unique'   => 'The :attribute already Exist.',
        );

        $data = Validator::make($data, $rule, $messages);

        return $data;
    }

    public static function create(array $attributes = [ ], $product = null)
    {
        if (empty( $attributes )) {
            return false;
        }

        if ($product) {
            $product = (object)$product;
            $attributes[ 'product_id' ] = $product->id;
            $attributes[ 'name' ] = $product->name;
            $attributes[ 'price' ] = $product->price;
            $attributes[ 'qty' ] = ( !isset( $attributes[ 'qty' ] ) ) ? $product->qty : $attributes[ 'qty' ];
            $attributes[ 'price' ] = $product->price;
            $attributes[ 'discount' ] = $product->discount;
            $attributes[ 'sale_price' ] = $product->sale_price;
        }

        $attributes[ 'discount_price' ] = ( isset( $attributes[ 'discount' ] ) )
            ? ( ( $attributes[ 'price' ] * $attributes[ 'discount' ] ) / 100 )
            : 0;

        $attributes[ 'total' ] = ( $attributes[ 'price' ] - $attributes[ 'discount_price' ] ) * $attributes[ 'qty' ];

        if (isset( $attributes[ 'id' ] )) {
            $attributes[ 'product_id' ] = $attributes[ 'id' ];
            unset( $attributes[ 'id' ] );
        }

        return parent::create($attributes);
    }

}
