<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopCategoryUserRole extends Model
{
    protected $table = 'roles';
    protected $primaryKey = 'id';

    public function shop_categories()
    {
        return $this->belongsToMany('App\Models\ShopCategory', 'role_shop_category', 'category_id', 'role_id');
    }
}