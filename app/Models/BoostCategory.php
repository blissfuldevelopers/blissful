<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BoostCategory extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'boost_category';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'parent_id'];

    public static $rules = [
        'name' => 'required|string',
    ];

    public static $db_attributes = [
        'name', 'parent_id',
    ];

    public function boosts()
    {
        return $this->belongsToMany('App\Models\Boost', 'boosts', 'category_id')->withTimestamps();
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\BoostCategory', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\BoostCategory', 'parent_id', 'id');
    }

    /**
     * @param $request
     * @param $id
     *
     * @return bool|null
     */
    public static function saveBoostCategory($request, $id)
    {
        $attributes = $request->all();
        if(empty($attributes)) {
            return false;
        }

        $attributes['id'] = (isset($attributes['id'])) ? $attributes['id'] : $id;

        $_attributes = array_merge((array)$attributes, [
            'name'      => $attributes['name'],
            'parent_id' => (isset($attributes['parent_id'])) ? $attributes['parent_id'] : null,
        ]);

        $_attributes = array_only($_attributes, static::$db_attributes);

        $boost_category = (isset($attributes['id']) && $attributes['id']) ? static::findOrFail($attributes['id']) : null;

        if($boost_category) {
            if(!isset($attributes['parent_id'])) {
                $attributes['parent_id'] = $boost_category->id;
            }
            $boost_category->update($_attributes);

            return $boost_category;
        }

        $boost_category = parent::updateOrCreate($_attributes, $_attributes);

        if($boost_category && !isset($attributes['parent_id'])) {
            $boost_category->parent_id = $boost_category->id;
            $boost_category->save();
        }

        return $boost_category;
    }
}
