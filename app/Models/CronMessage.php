<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\CronRecipient;
use Illuminate\Support\Facades\Auth;
use Log;

/**
 * @SWG\Definition(
 *      definition="CronMessage",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="message",
 *          description="message",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="sent",
 *          description="sent",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class CronMessage extends Model
{
    use SoftDeletes;

    public static $db_attributes = [
        'subject',
        'message',
        'send_date',
        'sent',
        'created_by',
        'thread_id',
    ];
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'message'   => 'required',
        'sent'      => 'boolean',
        'send_date' => 'required|date|after:yesterday',
    ];
    public static $default_cronmessage_details = [
        'subject'    => 'Blissful - New Message',
        'message'    => '',
        'send_date'  => '<<NOW>>',
        'created_by' => null,
        'thread_id' => null,
    ];
    public static $default_recipient_details = [
        'message_id'    => null,
        'mobile_number' => null,
        'email'         => null,
        'sent'          => 0,
    ];
    public $table = 'cron_messages';
    public $fillable = [
        'subject',
        'message',
        'send_date',
        'sent',
        'created_by',
        'thread_id',
    ];
    protected $dates = [ 'deleted_at' ];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'subject'    => 'string',
        'message'    => 'string',
        'sent'       => 'integer',
        'created_by' => 'integer',
    ];

    public static function create(array $attributes = [ ])
    {
        $recipient_details = ( isset( $attributes[ 'recipient_details' ] ) ) ? $attributes[ 'recipient_details' ] : null;
        $thread_id = ( isset( $attributes[ 'thread_id' ] ) ) ? $attributes[ 'thread_id' ] : null;
        if (!$recipient_details) {
            //first check if the recipient details have been defined in another way
            if (isset( $attributes[ 'mobile_numbers' ] )) {
                foreach ($attributes[ 'mobile_numbers' ] as $mobile_number) {
                    $attributes[ 'recipient_details' ][] = [ 'mobile_number' => clean_mobile_number($mobile_number) ];
                }
            }

            if (isset( $attributes[ 'emails' ] )) {
                foreach ($attributes[ 'emails' ] as $email) {
                    $attributes[ 'recipient_details' ][] = [ 'email' => $email ];
                }
            }

            //if still nothing...
            if (!isset( $attributes[ 'recipient_details' ] )) {
                return false;
            }

            return false;
        }


        if (!isset( $attributes[ 'created_by' ] )) {
            $attributes[ 'created_by' ] = ( Auth::user() ) ? Auth::user()->id : 0;
        }

        $attributes = array_merge(static::$default_cronmessage_details, (array)$attributes);

        foreach ($attributes as $key => $value) {
            if (!is_array($value)) {
                $attributes[ $key ] = ( $value === "<<NOW>>" )
                    ? date('Y-m-d H:i:s')
                    : ( ( $value === "<<NULL>>" ) ? "NULL" : $value );
            }
        }

        $attributes = array_only($attributes, static::$db_attributes);

        Log::info('CronMessage create() attributes ' . serialize($attributes));

        $cron_message = parent::create($attributes);

        Log::info('CronMessage create() result ' . serialize($cron_message));

        if ($cron_message) {

            $insert_data = [ ];

            foreach ($recipient_details as $detail) {

                $insert_data[] = new CronRecipient(array_only(array_merge(static::$default_recipient_details, [
                    'message_id'    => $cron_message->id,
                    'mobile_number' => ( isset( $detail[ 'mobile_number' ] ) ) ? clean_mobile_number($detail[ 'mobile_number' ]) : '',
                    'email'         => ( isset( $detail[ 'email' ] ) ) ? $detail[ 'email' ] : '',
                    'sent'          => 0,
                ]), CronRecipient::$db_attributes));
            }

            Log::info('CronMessage saving recipients result ' . serialize($insert_data));

            return $cron_message->recipients()->saveMany($insert_data);
        }

        return false;
    }

    public function recipients()
    {
        return $this->hasMany('App\Models\CronRecipient', 'message_id');
    }
}
