<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="AnalysisBudgetsToBids",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="desc",
 *          description="desc",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class AnalysisBudgetsToBids extends Model
{
    use SoftDeletes;

    public $table = "analysis_budgets_to_bids";


    protected $dates = [ ];


    public $fillable = [
        "job_id",
        "category_id",
        "location_id",
        "budget_amount",
        "bid_amount",
        "jobs_closed",
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "desc" => "string",
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'job_id'        => 'required|integer',
        'category_id'   => 'integer',
        'location_id'   => 'integer',
        'budget_amount' => 'required|integer',
        'bid_amount'    => 'integer',
        'jobs_closed'   => 'integer',
    ];

    /**
     * Get the comments for the blog post.
     */
    public function jobs()
    {
        return $this->hasMany('App\Job', 'job_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
