<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bookings';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','date','email','phone','user_id','vendor_id','title','booking_ref_no','start_time','end_time','booking_status','booking_description'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function vendor(){
        return $this->belongsTo('App\User', 'vendor_id');
    }
}
