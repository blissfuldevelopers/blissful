<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\CronTask;
use App\Models\CronSchedule;
use Illuminate\Support\Facades\Auth;
use DB;

/**
 * @SWG\Definition(
 *      definition="CronTaskToUser",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cron_task_id",
 *          description="cron_task_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class CronTaskToUser extends Model
{
    use SoftDeletes;

    public $table = 'cron_task_to_user';


    protected $dates = [ 'deleted_at' ];


    public $fillable = [
        'user_id',
        'cron_task_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id'      => 'integer',
        'cron_task_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'task_name'    => 'required|string',
        'task_type'    => 'required|string',
        'user_id'      => 'numeric|exits:users,id',
        'cron_task_id' => 'numeric',
        'repeat_start' => 'required|date|after:yesterday',
        'repeat_stop'  => 'date|after:repeat_start',
        'repeat'       => 'in:@yearly,@annually,@monthly,@weekly,@daily,@hourly,@custom',
    ];



    public function task()
    {
        return $this->belongsTo('App\Models\CronTask', 'cron_task_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }


    /**
     * @param array $attributes
     *
     * @return static
     */
    public static function create(array $attributes = [ ])
    {
        if (!isset( $attributes[ 'created_by' ] )) {
            $attributes[ 'created_by' ] = ( isset( $attributes[ 'user_id' ] ) ) ? $attributes[ 'user_id' ] : Auth::user()->id;
        }

        if (!isset( $attributes[ 'user_id' ] )) {
            $attributes[ 'user_id' ] = $attributes[ 'created_by' ];
        }

        //create the cronTask
        $task_attributes = array_merge($attributes, [
            'name' => $attributes[ 'task_name' ],
            'type' => $attributes[ 'task_type' ],
        ]);
        $task = CronTask::create($task_attributes);
        $attributes[ 'cron_task_id' ] = $task->id;

        //create the schedule
        $schedule = CronSchedule::create($attributes);

        //insert the data into this
        return parent::create($attributes);
    }

    /**
     * @return mixed
     */
    public static function fetchReminders()
    {
        $query = static::reminder();

        return $query;
    }

    /**
     * Scope a query to only get reminders.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeReminder($query)
    {
        return $query
            ->join('cron_tasks', function ($join) {
                $join->on('cron_task_to_user.cron_task_id', '=', 'cron_tasks.id');
            })
            ->leftJoin('cron_schedules', function ($join) {
                $join->on('cron_schedules.cron_task_id', '=', 'cron_tasks.id');
            })
            ->where('cron_tasks.type', '=', 'reminder')
            ->addSelect('cron_tasks.name as reminder')
            ->addSelect('cron_schedules.repeat_start')
            ->addSelect('cron_schedules.repeat_stop')
            ->addSelect(DB::raw("concat(cron_schedules.cron_min,' ',cron_schedules.cron_hour,' ',cron_schedules.cron_day_of_month,' ',cron_schedules.cron_month,' ',cron_schedules.cron_year) as cron_schedule"));
    }
}
