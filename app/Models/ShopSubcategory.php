<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Validator;
use Illuminate\Support\Facades\Auth;
class ShopSubcategory extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shop_subcategory';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'description', 'category_id','subcategory_slug' ];
    protected $hidden = [ 'created_at', 'updated_at' ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public static function validate($data)
    {


        $rule = array(
            'name'        => 'required|unique:shop_subcategory,name,NULL,id,category_id,' . $data[ 'category_id' ],
            'category_id' => 'required',
        );
        $messages = array(
            'required' => 'The :attribute field is required.',
            'unique'   => 'The :attribute already Exist.',
        );


        $data = Validator::make($data, $rule, $messages);

        return $data;
    }

    public static function validateUpdate($data, $id)
    {

        $rule = array(
            'name'        => 'required|unique:shop_subcategory,name,' . $id . ',id,category_id,' . $data[ 'category_id' ],
            'category_id' => 'required',
        );

        $messages = array(
            'required' => 'The :attribute field is required.',
            'unique'   => 'The :attribute already Exist.',
        );

        $data = Validator::make($data, $rule, $messages);

        return $data;
    }

    public function category()
    {
        return $this->belongsTo('App\Models\ShopCategory', 'category_id','id');
    }
    public static function filterByUserRole($collection, $user = null, $return_first = false)
    {
        $user = ( !$user ) ? Auth::user() : $user;

        $filtered = $collection->filter(function ($model) use ($user) {

            if ($user) {
                if (!$user->can('view.shop')) {
                    return false;
                }

                if ($user->admin) {
                    return true;
                }

                if ($user->vendor) {
                    return $model->for_user || $model->for_vendor;
                }
            }

            return $model->for_user;
        });

        if ($return_first) {
            return $filtered->first();

        }

        return $filtered->all();
    }
}
