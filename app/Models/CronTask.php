<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="CronTask",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="type",
 *          description="type",
 *          type="string"
 *      )
 * )
 */
class CronTask extends Model
{
    use SoftDeletes;

    public $table = 'cron_tasks';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = [ 'repeat_start', 'repeat_stop', 'deleted_at' ];


    public $fillable = [
        'name',
        'type',
    ];

    protected static $db_attributes = [
        'name',
        'type',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'   => 'integer',
        'name' => 'string',
        'type' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'type' => 'required',
    ];

    public function schedules()
    {
        return $this->hasMany('App\Models\CronSchedule', 'cron_task_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\CronTaskToUser', 'cron_task_to_user', 'cron_task_id', 'user_id')->withTimestamps();
    }


    public static function create(array $attributes = [ ])
    {
        $attributes = array_only($attributes, static::$db_attributes);

        return parent::create($attributes);
    }

}
