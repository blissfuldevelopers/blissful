<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ShopProduct;
use App\Models\ShopCategory;
use App\Models\ShopSubcategory;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class Boost extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'boosts';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'days', 'sale_price', 'cost', 'product_id', 'category_id', 'published'];

    public static $rules = [
        'name'        => 'required|string',
        'description' => 'required|string',
        'days'        => 'required',
        'sale_price'  => 'required',
        'cost'        => 'required',
        'highlights'  => 'string',
        'tnc'         => 'string',
        'published'   => 'boolean',
    ];

    public static $db_attributes = [
        'product_id',
        'name',
        'description',
        'days',
        'sale_price',
        'cost',
        'category_id',
        'published',
    ];

    public function categories()
    {
        return $this->hasMany('App\Models\BoostCategory', 'category_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\ShopProduct', 'product_id');
    }
    public function user()
    {
        return $this->belongsToMany('App\User');
    }

    /**
     * @param $request
     *
     * @return array
     */
    private static function uploadImage($request)
    {
        $covername = null;
        $filePath = null;
        $s3 = null;

        if($request->hasFile('cover_image_path')) {

            $coverimg = $request->file('cover_image_path');
            $covername = uniqid() . $coverimg->getClientOriginalName();
            $thumb = Image::make($coverimg->getRealPath())->resize(600, 400);
            $image_thumb = $thumb->stream();
            $s3 = \Storage::disk('s3');
            $filePath = 'uploads/products/full_size' . $covername;

            $s3 = $s3->put($filePath, $image_thumb->__toString(), 'public');
        }

        return [
            'covername' => $covername,
            'filePath'  => $filePath,
            's3'        => $s3,
        ];
    }

    /**
     * @param      $request
     * @param null $boost_id
     *
     * @return bool
     */
    public static function saveBoost($request, $boost_id = null)
    {
        $attributes = $request->all();
        if(empty($attributes)) {
            return false;
        }

        $attributes['id'] = (isset($attributes['id'])) ? $attributes['id'] : $boost_id;

        $product_id = static::createShopProduct($request, $attributes);

        $_attributes = array_merge((array)$attributes, [
            'product_id'  => $product_id,
            'name'        => $attributes['name'],
            'days'        => $attributes['days'],
            'sale_price'  => $attributes['sale_price'],
            'cost'        => $attributes['cost'],
            'description' => $attributes['description'],
            'published'   => (bool)$attributes['published'],
            'category_id' => (isset($attributes['category_id'])) ? $attributes['category_id'] : '',
        ]);
        $_attributes = array_only($_attributes, static::$db_attributes);

        $boost = (isset($attributes['id']) && $attributes['id']) ? static::findOrFail($attributes['id']) : null;
        if($boost) {
            $boost->update($_attributes);

            return $boost;
        }

        return parent::updateOrCreate($_attributes, $_attributes);

    }

    /**
     * @param $request
     * @param $attributes
     *
     * @return null
     */
    private static function createShopProduct($request, $attributes)
    {
        $product_id = null;

        $uploadImage = static::uploadImage($request);

        $boost_price = $attributes['sale_price'];
        $cost = $attributes['cost'];
        $boost = (isset($attributes['id'])) ? static::findOrFail($attributes['id']) : null;
        $discount = (($boost_price - $cost) < 0)
            ? ($boost_price - $cost) * -1
            : ($boost_price - $cost);

        $product_attributes = array_merge((array)$attributes, [
            'price'       => $boost_price,
            'sale_price'  => $cost,
            'discount'    => $discount,
            'status'      => (isset($attributes['published']) && $attributes['published']) ? 'active' : 'Inactive',
            'seller_id'   => (Auth::user()) ? Auth::user()->id : 0,
            'qty'         => 1,
            'description' => $request['description'],
            'highlights'  => $request['highlights'],
            'tnc'         => $request['tnc'],
            'picture'     => $uploadImage['filePath'],
        ]);

        $product_attributes = array_only($product_attributes, ShopProduct::$db_attributes);

        if($boost) {
            $product = ShopProduct::findOrFail($boost->product_id);
            $product->update($product_attributes);
            $product_id = $boost->product_id;
        } else {
            //first create (or update) the product
            $product = ShopProduct::updateOrCreate($product_attributes, $product_attributes);
            $product_id = $product->id;

        }

        $product_category_attr = [
            'name'          => 'Blissful Boosts',
            'category_slug' => 'blissful_boosts',
        ];
        $product_category = ShopCategory::updateOrCreate($product_category_attr, $product_category_attr);

        if($product_category->wasRecentlyCreated) {
            $product_category->assignUserRole('vendor');
        }

        $product->category_id = $product_category->id;

        $product_subcategory_attr = [
            'name'             => 'Visibility/Intelligence boosts',
            'subcategory_slug' => 'visibility-intelligence-boosts',
            'category_id'      => $product->category_id,

        ];
        $product_subcategory = ShopSubcategory::updateOrCreate($product_subcategory_attr, $product_subcategory_attr);

        $product->sub_category_id = $product_subcategory->id;
        $product->product_slug = str_slug($product->name, "-");
        $product->save();


        return $product_id;
    }
}
