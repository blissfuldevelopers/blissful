<?php
/**
 * Created by PhpStorm.
 * User: davidhenia
 * Date: 17/04/2016
 * Time: 13:51
 */

namespace App\Libraries\blissful;

use Log;
use Crypt;
use Ixudra\Curl\Facades\Curl;

class BlissAnalysisData
{
    private static $keys = [ ];
    public static $post_array = [

    ];

    public static function init()
    {

    }

    /**
     * Function run daily by cron to populate analysis_budget_bids table with:
     * analysis_job_bids, -category id, -user budget, -vendor bids, -job id, -location id, -event_date, -closed job,
     * -created at, -updated at, -credits_used
     *
     * Cron Jobs table for job_id missing in this analysis_budget_bids
     * - data goes to Analysis of budget vs bids
     */
    public static function populate_analysis_budget_bids()
    {
        $analysis_budget_bids_sql = \DB::select("
                select
                    jobs.id as job_id, classifications.id as category_id, locations.id as location_id,
                    jobs.budget as budget_amount, bids.estimated_price as bid_amount,
                    sum(bids.bid_accepted) as jobs_closed
                from jobs
                left join bids on bids.job_id = jobs.id
                left join locations on locations.name = jobs.event_location
                left join classifications on classifications.name = jobs.classification
                group by job_id, category_id, budget_amount, bid_amount, location_id
                order by job_id
                ");

        //
        Log::info(count($analysis_budget_bids_sql) . ' budget items found ');

        $insert = null;
        foreach ($analysis_budget_bids_sql as $key => $obj) {
            $attributes = [
                'job_id'        => (int)$obj->job_id,
                'category_id'   => (int)$obj->category_id,
                'location_id'   => (int)$obj->location_id,
                'budget_amount' => (int)$obj->budget_amount,
                'bid_amount'    => (int)$obj->bid_amount,
                'jobs_closed'   => (int)$obj->jobs_closed,
            ];
            $data = $attributes;
            $insert[ $key ] = \App\Models\AnalysisBudgetsToBids::updateOrCreate($attributes, $data);
        }

        Log::info('populate_analysis_budget_bids insert complete');

    }


}