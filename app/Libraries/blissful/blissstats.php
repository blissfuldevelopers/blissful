<?php
/**
 * Created by PhpStorm.
 * User: davidhenia
 * Date: 17/04/2016
 * Time: 13:51
 */

namespace App\Libraries\blissful;

use Log;
use Crypt;
use DB;
use App\Logic\Mailers\UserMailer;

class BlissStats
{
    private static $stats_data = [];
    private static $fromEmail = [
        'happy@blissful.co.ke',
        'Blissful Team',
    ];
    private static $EmailView = 'emails.bliss-stats';
    private static $toEmail = [
        'dmainah@gmail.com',
    ];

    /**
     *
     */
    private static function init()
    {

    }


    /**
     *
     */
    public static function run()
    {
        Log::info('Bliss stats: Starting');
        static::init();

        //get data
        static::get_data();
        if(empty(static::$stats_data)) {
            Log::info('Bliss stats: No data found');

            return;
        }

        //get emails to send stats to
        static::get_to_emails();
        if(empty(static::$toEmail)) {
            Log::info('Bliss stats: No emails found');

            return;
        }

        $userMailer = new UserMailer;
        $subject = 'Blissful Statistics - ' . date('dS, M-Y');


        Log::info('Bliss stats: Emailing ' . count(static::$toEmail) . ' recipients');

        foreach(static::$toEmail as $to) {
            $userMailer->sendTo($to, $subject, static::$fromEmail, static::$EmailView, [
                'stats_data' => static::$stats_data,
            ]);
        }

        Log::info('Bliss stats: Complete');
    }

    /**
     *
     */
    private static function get_to_emails()
    {
        $results = DB::select("
                        select distinct email from users
                        join role_user on users.id = role_user.user_id
                        join roles on roles.id = role_user.role_id and roles.name = 'administrator'
                        where users.deleted_at IS NULL
                  ");

        $emails = [];
        foreach($results as $row) {
            $emails [] = $row->email;
        }

        static::$toEmail = array_unique(array_merge(static::$toEmail, $emails));
    }


    /**
     *
     */
    private static function get_data()
    {
        Log::info('Bliss stats: Fetching data');

        Log::info('Bliss stats: Fetching yesterday_active_users');
        static::yesterday_active_users();

        Log::info('Bliss stats: Fetching weekly_active_users');
        static::weekly_active_users();

        Log::info('Bliss stats: Fetching monthly_active_users');
        static::monthly_active_users();

        Log::info('Bliss stats: Fetching users_joined(1)');
        static::users_joined(1); //yesterday

        Log::info('Bliss stats: Fetching users_joined(7)');
        static::users_joined(7); //week

        Log::info('Bliss stats: Fetching users_joined(30)');
        static::users_joined(30); //month

        Log::info('Bliss stats: Fetching users_joined(-1) //All Time');
        static::users_joined(-1); //All time

        Log::info('Bliss stats: Fetching jobs_and_bids_created(1)');
        static::jobs_and_bids_created(1); //yesterday

        Log::info('Bliss stats: Fetching jobs_and_bids_created(7)');
        static::jobs_and_bids_created(7); //week

        Log::info('Bliss stats: Fetching jobs_and_bids_created(30)');
        static::jobs_and_bids_created(30); //month

        Log::info('Bliss stats: Fetching jobs_and_bids_created(60)');
        static::jobs_and_bids_created(60); //60 days

        Log::info('Bliss stats: Fetching jobs_and_bids_created(-1) //All time');
        static::jobs_and_bids_created(-1); //All Time


        Log::info('Bliss stats: Completed fetching data');
    }


    /**
     *
     */
    private static function yesterday_active_users()
    {
        static::active_users(1);
    }


    /**
     *
     */
    private static function weekly_active_users()
    {
        static::active_users(7);

    }


    /**
     *
     */
    private static function monthly_active_users()
    {
        static::active_users(30);
    }

    /**
     * @param int $period_days
     */
    private static function active_users($period_days = 30)
    {

        $where_created_at = "";
        if($period_days <> -1 && is_numeric($period_days)) {
            $where_created_at = "AND users.last_login > date_sub(curdate(), interval {$period_days} day)";
        }

        $results = DB::select("
                    SELECT IFNULL(COUNT(DISTINCT users.id), 0) as number_of_users, roles.name as role
                     from users
                     join role_user on users.id = role_user.user_id
                     join roles on roles.id = role_user.role_id
                     WHERE
                     users.deleted_at IS NULL
                     {$where_created_at}
                     group by roles.id
                     union
                     SELECT IFNULL(COUNT(DISTINCT users.id), 0) as number_of_users, 'all_users' as role
                     from users
                     WHERE
                     users.deleted_at IS NULL
                     {$where_created_at}
                     ");

        foreach($results as $row) {
            static::$stats_data['active_users'][static::period_key($period_days)][$row->role] = $row->number_of_users;
        }
    }


    /**
     * @param int $period_days
     */
    private static function users_joined($period_days = 1)
    {
        $where_created_at = "";
        if($period_days <> -1 && is_numeric($period_days)) {
            $where_created_at = "AND users.created_at > date_sub(curdate(), interval {$period_days} day)";
        }

        $results = DB::select("
                    SELECT IFNULL(COUNT(DISTINCT users.id), 0) as number_of_users, roles.name as role
                     from users
                     join role_user on users.id = role_user.user_id
                     join roles on roles.id = role_user.role_id
                     WHERE
                      users.deleted_at IS NULL
                      {$where_created_at}
                     group by roles.id
                     union
                     SELECT IFNULL(COUNT(DISTINCT users.id), 0) as number_of_users, 'all_users' as role
                     from users
                     WHERE
                      users.deleted_at IS NULL
                      {$where_created_at}
                     ");

        foreach($results as $row) {
            static::$stats_data['users_joined'][static::period_key($period_days)][$row->role] = $row->number_of_users;
        }

    }


    /**
     * @param int $period_days
     */
    private static function jobs_and_bids_created($period_days = 1)
    {
        $where_jobs_created_at = "";
        $where_bids_created_at = "";

        if($period_days <> -1 && is_numeric($period_days)) {
            $where_jobs_created_at = "AND jobs.created_at > date_sub(curdate(), INTERVAL {$period_days} day)";
            $where_bids_created_at = "AND bids.created_at > date_sub(curdate(), INTERVAL {$period_days} day)";
        }

        $results = DB::select("
                        select
                          IFNULL(COUNT(DISTINCT jobs.id), 0) jobs,  IFNULL(COUNT(DISTINCT bids.id), 0) bids, '_Total' as classification
                        from jobs
                        left join bids on jobs.id = bids.job_id and bids.deleted_at IS NULL {$where_bids_created_at}
                        where
                        jobs.deleted_at IS NULL
                        {$where_jobs_created_at}

                        union

                        select
                          IFNULL(COUNT(DISTINCT jobs.id), 0) jobs,  IFNULL(COUNT(DISTINCT bids.id), 0) bids, parent_classifications.name as classification
                        from classifications
                        join classifications parent_classifications on parent_classifications.id = classifications.root_parent_id
                        left JOIN jobs ON jobs.classification_id = classifications.id  AND jobs.deleted_at IS NULL {$where_jobs_created_at}
                        left join bids on jobs.id = bids.job_id and bids.deleted_at IS NULL {$where_bids_created_at}
                        where
                          classifications.deleted_at is null
                        GROUP BY parent_classifications.id
                        order by classification
                     ");

        foreach($results as $row) {
            static::$stats_data['jobs_created'][static::period_key($period_days)][$row->classification] = $row->jobs;
            static::$stats_data['bids_created'][static::period_key($period_days)][$row->classification] = $row->bids;
        }

    }


    /**
     * @param int $period_days
     */
    private static function bids_viewed_by_user_after_creating_job($period_days = 1)
    {

        $results = DB::select("

                     ");

        foreach($results as $row) {
            static::$stats_data['bids_viewed_by_user_after_creating_job'][static::period_key($period_days)][$row->role] = $row->number_of_users;
        }

    }


    /**
     * @param $period
     *
     * @return string
     */
    private static function period_key($period)
    {
        if(!is_numeric($period)) {
            Log::info($period);

            return 'number';
        }

        switch($period) {
            case -1:
                return 'all_time';
                break;

            case 1:
                return 'yesterday';
                break;

            default:
                return 'last_' . $period . '_days';
                break;
        }
    }


}