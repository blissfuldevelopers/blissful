<?php
/**
 * Created by PhpStorm.
 * User: davidhenia
 * Date: 17/04/2016
 * Time: 13:51
 */

namespace App\Libraries\blissful;

use App\CreditsDebitCredit;
use Log;
use Crypt;
use Ixudra\Curl\Facades\Curl;

class BlissShop
{


    public static function init()
    {

    }

    /**
     * @param      $ShopOrderDetail
     * @param null $shop_order
     * @param null $user_id
     *
     * @return mixed|string
     */
    public static function performSpecialProductActivities($ShopOrderDetail, $shop_order = null, $user_id = null)
    {
        static::init();
        $message = '';
        $success = false;

        if ($ShopOrderDetail->product) {

            $shop_order = (!$shop_order) ? $ShopOrderDetail->order : $shop_order;
            $user_id = (!$user_id) ? $ShopOrderDetail->order->user_id : $user_id;

            if ($ShopOrderDetail->product->credit_bundle) {

                Log::info('Crediting user: ' . $user_id . ' for order: ' . $shop_order->id);
                //add these credits to the user's credit_debit_credit
                $success = \App\Credit::addProductCreditsToUser($ShopOrderDetail->product, $shop_order, $ShopOrderDetail->qty);

                //Notify User
                $total_credits = ($ShopOrderDetail->product->credit_bundle->credits * $ShopOrderDetail->qty);

                $message = ($success)
                    ? str_replace(['{total_credits}'], $total_credits, \App\Credit::$successfully_credited_message)
                    : \App\Credit::$failed_credited_message;


            } elseif ($ShopOrderDetail->product->boost) {

                Log::info('Blissful boost package : ' . $user_id . ' for order: ' . $shop_order->id);

                //boost credits
                If ($ShopOrderDetail->product->boost->name == 'Intelligence Boost - Basic') {
                    CreditsDebitCredit::creditUser($user_id, 300, $ShopOrderDetail->product->boost->id, 'boost_credits');
                } elseif ($ShopOrderDetail->product->boost->name == 'Intelligence Boost - Gold') {
                    CreditsDebitCredit::creditUser($user_id, 450, $ShopOrderDetail->product->boost->id, 'boost_credits');
                }
                //add the boost to the user's boost_to_user
                $success = \App\Models\BoostToUser::addBoostToUser($ShopOrderDetail, $user_id);

                $message = "You've successfully added " . $ShopOrderDetail->product->boost->name;
            }
        }

        return [
            'success' => $success,
            'message' => $message,
        ];

    }


}