<?php
/**
 * Created by PhpStorm.
 * User: davidhenia
 * Date: 17/04/2016
 * Time: 13:51
 */

namespace App\Libraries\blissful;

use Log;
use Crypt;
use Ixudra\Curl\Facades\Curl;

class BlissAnalytics
{
    private static $keys = array();
    public static $post_array = [
        'v'   => 1,
        'an'  => 'Blissful',
        'aid' => 'hugeafrica', //Application ID
    ];

    public static function init()
    {

        static::$post_array = array_merge(static::$post_array, [
            'tid' => config('blissanalytics.google_tracking_id'), // UA-XXXXXX-X
        ]);
    }

    /**
     * @param $arguments
     */
    public static function send_ga_measurement_protocol($arguments)
    {
        static::init();

        $arguments_obj = json_decode($arguments);

        $pagename_and_event = static::get_pagename_and_event_from_url($arguments_obj->url);
        $arguments_obj->pagename = $pagename_and_event[ 'pagename' ];
        $arguments_obj->event = $pagename_and_event[ 'event' ];
        $arguments_obj->event_value = $pagename_and_event[ 'event_value' ];

        static::send_event_data($arguments_obj);
        static::send_pageview_data($arguments_obj);
    }


    /**
     * @param array $post_array
     */
    protected static function send_event_data($arguments_obj)
    {

        $post_array = array_merge(static::$post_array, [
            't'   => 'event', // type=event
            'cid' => ( $arguments_obj->client_id ), //Anonymous Client ID.
            'uid' => ( $arguments_obj->user_id ), //user ID
            'ec'  => $arguments_obj->pagename, // Event Category (Required)
            'ea'  => $arguments_obj->event, //Event Action (Required)
            'el'  => $arguments_obj->pagename, //Event Label
            'ev'  => $arguments_obj->event_value, //Event Value
        ]);

        return static::send($post_array);
    }

    /**
     * @param array $post_array
     */
    protected static function send_pageview_data($arguments_obj)
    {


        $post_array = array_merge(static::$post_array, [
            't'   => 'pageview', // Hit Type.
            'cid' => ( $arguments_obj->client_id ), //Anonymous Client ID.
            'uid' => ( $arguments_obj->user_id ), //user ID
            'uip' => $arguments_obj->uip,
            'ua'  => $arguments_obj->ua, //user agent
            'dh'  => 'blissful.co.ke', //Document hostname.
            'dp'  => $arguments_obj->url, //Document Page.
            'dt'  => $arguments_obj->pagename, //Document Title.
        ]);


        return static::send($post_array);
    }


    /**
     * @param $url
     *
     * @return string
     */
    private static function get_pagename_and_event_from_url($url)
    {
        $page_name = '';
        $event = 'index';
        $event_value = '';
        $url_parts = explode('/', $url);
        $last_part = $url_parts[ count($url_parts) - 1 ];
        $second_last_part = ( count($url_parts) > 1 ) ? $url_parts[ count($url_parts) - 2 ] : '';

        //make the typical actions events
        if (in_array($last_part, [ 'index', 'create', 'store', 'show', 'edit', 'update', 'destroy', 'all' ])) {
            $event = $last_part;
            $last_part = ''; // for purposes of the page_name
        }
        elseif (is_numeric($last_part)) {
            $last_part = '';
            $event_value = $last_part;
        }

        if (is_numeric($second_last_part)) {
            $event_value = $second_last_part;
            $second_last_part = ( isset( $url_parts[ count($url_parts) - 3 ] ) )
                ? $url_parts[ count($url_parts) - 3 ] : $second_last_part;
        }


        //remove the blissful part in pagename if the last part isn't empty
        if ($last_part <> '' && starts_with(strtolower($second_last_part), 'blissful')) {
            $second_last_part = '';
        }


        $page_name = camel_case($second_last_part) . ' ' . camel_case($last_part);

        return [
            'pagename'    => 'Blissful | ' . ucwords(strtolower($page_name)),
            'event'       => camel_case($event),
            'event_value' => camel_case($event_value),
        ];
    }


    /**
     * @param array  $post_array
     * @param string $curl_method
     *
     * @return mixed|null
     */
    private static function send($post_array = [ ], $curl_method = 'POST')
    {
        // Log::info('Google Analytics item ' . serialize($post_array));
        $response = null;
        $url = 'https://www.google-analytics.com/collect?payload_data';

        if (strtolower(trim($curl_method)) == 'post') {
            $response = Curl::to($url)
                            ->withData($post_array)
                            ->post();
        }
        else {

            $_get_data = [ ];
            foreach ($post_array as $key => $value) {
                $_get_data [] = $key . '=' . urlencode($value);
            }
            $final_post_data = implode('&', $_get_data);

            $curl_get_url = $url . '&' . $final_post_data;
            // Log::info('Google Analytics CURL ' . $curl_get_url);


            $response = Curl::to($curl_get_url)->get();

        }

        // Log::info('Google Analytics response ' . serialize($response));

        return $response;
    }


}