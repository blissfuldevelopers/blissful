<?php
/**
 * Created by PhpStorm.
 * User: davidhenia
 * Date: 17/04/2016
 * Time: 13:51
 */

namespace App\Libraries\blissful;

use App\Jobs\Job;
use DB;
use Log;
use Cache;
use Carbon\Carbon;
use App\Libraries\blissful\blissnotify;
use App\Models\CronSchedule;
use App\Models\CronTaskToUser;
use App\Models\ShopOrder;
use App\User;
use App\Logic\Mailers\UserMailer;
use App\Bid;
use App\Meta;

/**
 * Class BlissTask
 *  Tasks are defined in the cron_tasks table
 *     with column (type) = task
 *     and column (name) = name of the function without 'task_'
 *     scheduled task: blisstasks:run calls BlissTask::run_tasks_in_db()
 *      which looks for any valid task
 *      and runs it
 *
 * @package App\Libraries\blissful
 */
class BlissTask
{
    public static $create_defaults = [
        'task_name'    => '',
        'task_type'    => 'task',
        'user_id'      => null,
        'cron_task_id' => null,
        'repeat_start' => null,
        'repeat_stop'  => null,
        'repeat'       => null,
    ];

    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    /////////////////   TASKS
    ////////////////////////  Any function with prefix 'task_' is assumed to be a task
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////

    /////////////////   FOR GENERAL / ALL / NOT-SPECIFIC
    /**
     *
     * @return bool
     */
    private static function task_inform_users_to_complete_shop_checkout()
    {
        $message = "Dear {user_name}, it seems you haven't completed purchasing {shop_items} items. How may we help you? Contact us at: 0775987555";

        //get the relevant general / any user who hasn't finished checking out  & their contact details

        $incomplete_shop_orders = ShopOrder::where('shop_order.status', 'CHECKOUT')
                                           ->leftJoin('shop_order_detail', function ($join) {
                                               $join
                                                   ->on('shop_order_detail.order_id', '=', 'shop_order.id');
                                           })
                                           ->join('users', function ($join) {
                                               $join
                                                   ->on('users.id', '=', 'shop_order.user_id');
                                           })
                                           ->join('profiles', function ($join) {
                                               $join
                                                   ->on('users.id', '=', 'profiles.user_id');
                                           })
                                           ->whereNull('shop_order.deleted_at')
                                           ->get([
                                                     'shop_order.id',
                                                     'shop_order.created_at as order_date',
                                                     'users.id as user_id',
                                                     'users.first_name',
                                                     'users.last_name',
                                                     'users.email',
                                                     'profiles.phone',
                                                     'shop_order_detail.name as shop_order_detail_name',
                                                     'shop_order_detail.qty',
                                                 ]);
        if(!$incomplete_shop_orders) {
            return false;
        }

        $user_items = [];
        $user_incomplete_shop_order = [];
        foreach($incomplete_shop_orders as $incomplete_shop_order) {
            $user_incomplete_shop_order[$incomplete_shop_order->user_id] = $incomplete_shop_order;
            $user_items[$incomplete_shop_order->user_id] [] = $incomplete_shop_order->shop_order_detail_name . " ({$incomplete_shop_order->qty})";
        }

        if(empty($user_incomplete_shop_order)) {
            return false;
        }

        foreach($user_incomplete_shop_order as $user_id => $_incomplete_shop_order) {
            $shop_items = implode(', ', $user_items[$user_id]);
            $user_name = $_incomplete_shop_order->first_name . ' ' . $_incomplete_shop_order->last_name;

            $message_details = [
                'subject' => 'Pending Items in your Shopping Cart',
                'message' => str_replace(['{user_name}', '{shop_items}'], [$user_name, $shop_items], $message),
            ];

            static::inform($message_details, [$_incomplete_shop_order->phone], [$_incomplete_shop_order->email]);
        }

    }

    /////////////////   FOR VENDORS

    /**
     * Will look for users who have only one credit entry in credits_debit_credits table that isn't a purchase
     *  And do not have a meta entry: first_credits_expired
     *  And their 1st login is greater or equal 45 days ago
     * And debits those 1st 300 (or whichever amount of credits)
     *
     * -- Run only once a day. Preferably at midnight...
     *
     */
    private static function task_expire_first_user_credits()
    {
        $credits_to_expire = DB::select("SELECT
                                          cdc.user_id,
                                          cdc.credit,
                                          date(meta.login_date),
                                          meta_first_credits_expired.id
                                        FROM credits_debit_credits cdc
                                          JOIN (SELECT
                                                  count(id) AS count,
                                                  user_id
                                                FROM credits_debit_credits
                                                WHERE credit > 0
                                                GROUP BY user_id
                                                HAVING count(id) = 1) cdc_count ON cdc_count.user_id = cdc.user_id
                                          JOIN (
                                          select min_login.id, login_meta.object_id, login_meta.meta_value AS login_date
                                          	from meta_table login_meta
                                          	join (SELECT
                                                  max(id) as id,
                                                  object_id
                                                FROM meta_table
                                                WHERE meta_key = 'login'
                                                GROUP BY object_id) min_login on login_meta.id = min_login.id
                                          ) meta ON meta.object_id = cdc.user_id
                                          LEFT JOIN (SELECT
                                                       id,
                                                       meta_value,
                                                       object_id
                                                     FROM meta_table
                                                     WHERE meta_key = 'first_credits_expired' AND object_type = 'App\User'
                                                     GROUP BY object_id) meta_first_credits_expired ON meta.object_id = cdc.user_id
                                        WHERE
                                          cdc.credit > 0
                                          AND date(meta.login_date) <= date_sub(curdate(), INTERVAL 45 DAY)
                                          AND (cdc.object_name IS NULL OR cdc.object_name != 'credits')
                                          AND meta_first_credits_expired.id IS NULL");

        Log::info(count($credits_to_expire) . ' credits found to expire ');

        foreach($credits_to_expire as $obj) {

            Log::info("expiring {$obj->credit} credits for user {$obj->user_id}  to expire ");

            //debit the user these exact number of credits
            \App\CreditsDebitCredit::debitUser($obj->user_id, $obj->credit, 0, 'expiration');

            //enter in Meta Table
            // object = 'users', object_id = user_id, meta_key = 'first_credits_expired', meta_value = 1,
            \App\Meta::create([
                                  'object_type' => 'users',
                                  'object_id'   => $obj->user_id,
                                  'meta_key'    => 'first_credits_expired',
                                  'meta_value'  => 1,
                              ]);
        }

        Log::info(count($credits_to_expire) . ' credits expired (debited) ');
    }

    /**
     *
     */
    private static function task_inform_vendors_credits_below_100()
    {
        $vendors_below_100_credits = DB::Select("SELECT
                                                  users.first_name,
                                                  users.email,
                                                  profiles.phone,
                                                  (SUM(IFNULL(cdc.credit, 0)) - SUM(IFNULL(cdc.debit, 0))) AS credit_balance,
                                                  cdc.user_id,
                                                  ran_out
                                                FROM credits_debit_credits cdc
                                                  JOIN users ON cdc.user_id = users.id
                                                  JOIN profiles ON users.id = profiles.user_id
                                                  JOIN (SELECT
                                                          bids.user_id         AS bid_user_id,
                                                          max(bids.created_at) AS ran_out
                                                        FROM bids
                                                        GROUP BY bids.user_id) bids ON cdc.user_id = bids.bid_user_id
                                                GROUP BY users.id
                                                HAVING credit_balance < 100");

        if(!$vendors_below_100_credits) {
            return false;
        }
        Log::info(count($vendors_below_100_credits) . ' vendors below 100 credits to message');

        foreach($vendors_below_100_credits as $vendor) {

            $message_details = [
                'subject' => 'Top up to avoid missing out on jobs',
                'message' => 'You are almost out of credits. Top up to avoid missing out on jobs',
            ];

            static::inform($message_details, [$vendor->phone], [$vendor->email]);

        }

        Log::info(count($vendors_below_100_credits) . ' vendors below 100 credits messaged');
    }

    /**
     *
     */
    private static function task_inform_vendors_about_jobs()
    {
        Log::info(' task_inform_vendors_about_jobs ');

        return static::jobs_in_period("24 HOUR");

    }

    /**
     *
     */
    private static function task_inform_vendors_about_weekend_jobs()
    {
        Log::info(' task_inform_vendors_about_weekend_jobs ');

        return static::jobs_in_period("2 DAY");

    }

    /**
     * @param string $period
     */
    private static function jobs_in_period($period = "24 HOUR")
    {
        Log::info(' jobs_in_period period =  ' . $period);

        //get the relevant vendors & their contact details

        $previous_days_jobs = DB::select("SELECT
                                             classifications.id  as job_classification_id,
                                              users.id as user_id,
                                              users.first_name,
                                              users.email,
                                              profiles.phone,
                                              jobs.created_at
                                            FROM jobs
                                              JOIN classifications ON classifications.id = jobs.classification_id
                                              JOIN classification_user cu ON cu.classification_id = classifications.id
                                              JOIN users ON cu.user_id = users.id
                                              JOIN profiles ON profiles.user_id = users.id
                                              JOIN users creators ON creators.id = jobs.user_id
                                            WHERE
                                              jobs.created_at > date_sub(curdate(), INTERVAL {$period})
                                              AND users.deleted_at IS NULL
                                              AND jobs.deleted_at IS NULL
                                              AND jobs.completed = 0
                                            GROUP BY job_classification_id, user_id
                                            ORDER BY jobs.created_at DESC");

        $users_to_message = [];
        $jobs = [];

        Log::info(count($previous_days_jobs) . ' job categories created within past ' . $period . 's to email ');

        foreach($previous_days_jobs as $obj) {
            $users_to_message[$obj->user_id] = $obj;
            $users_in_category[$obj->user_id][$obj->job_classification_id] = $obj;
        }

        $userMailer = new UserMailer;

        foreach($users_to_message as $_obj) {

            if(!isset($jobs[$_obj->job_classification_id])) {
                //While the email is sent
                $jobs[$_obj->job_classification_id] = DB::select("SELECT
                                          jobs.id    job_id,
                                          jobs.title job_title,
                                          jobs.description,
                                          jobs.event_date
                                        FROM jobs
                                        WHERE
                                          jobs.created_at > date_sub(curdate(), INTERVAL {$period})
                                          AND jobs.classification_id = '{$_obj->job_classification_id}'
                                          AND jobs.deleted_at IS NULL
                                          AND jobs.completed = 0
                                        ORDER BY jobs.created_at DESC");

                Log::info(count($jobs) . ' jobs within category ' . $period . ' created within past ' . $period . 's to email ');
            }
        }

        foreach($users_to_message as $_obj) {

            $list_of_jobs = $jobs[$_obj->job_classification_id];
            $email = filter_var($_obj->email, FILTER_SANITIZE_EMAIL);
            $data = [
                'first_name'   => $_obj->first_name,
                'email'        => $email,
                'subject'      => 'Blissful Jobs: We have new jobs for you today',
                'list_of_jobs' => serialize($list_of_jobs),
            ];


            try {
                $userMailer->daily_jobs($data);
            }
            catch(\Exception $e) {
                Log::error('Could not email ' . $email . ' with error:' . $e->getMessage());
            }

        }

        Log::info('completed sending ' . count($previous_days_jobs) . ' jobs created within past ' . $period . 's');
    }

    /**
     * @return bool
     */
    private static function task_remind_users_to_respond()
    {
        $users_to_email = Bid::where('bids.status', 0)
                             ->join('jobs', function ($join) {
                                 $join
                                     ->on('jobs.id', '=', 'bids.job_id');
                             })
                             ->join('users', function ($join) {
                                 $join
                                     ->on('users.id', '=', 'jobs.user_id');
                             })
                             ->join('profiles', function ($join) {
                                 $join
                                     ->on('profiles.user_id', '=', 'users.id');
                             })
                             ->where('bids.created_at', '>', Carbon::now()->subDays(30))
                             ->groupBy('job_id')
                             ->get([
                                       'bids.id',
                                       'users.first_name',
                                       'jobs.id',
                                       'jobs.title',
                                       'jobs.user_id',
                                       'users.email',
                                       'profiles.phone',
                                   ]);
        if(!$users_to_email) {
            return false;
        }
        Log::info(count($users_to_email) . ' users to respond to bids to message');

        foreach($users_to_email as $user) {

            $message_details = [
                'subject' => 'Great vendors interested in your event',
                'message' => 'These great businesses have submitted quotes for your request <strong>' . $user->title . '</strong> on blissful.co.ke. Log in and review their quotes. Great vendors like these are usually booked. So act fast',
            ];

            static::inform($message_details, [$user->phone], [$user->email]);

        }

        Log::info(count($users_to_email) . ' users to respond to bids messaged');

    }
    private static function task_recommendation_email()
    {
        $users = DB::select("SELECT
                                  users.first_name,
                                  users.last_name,
                                  users.email,
                                  users.id user_id
                                FROM jobs
                                  JOIN users ON users.id = jobs.user_id
                                  JOIN role_user on users.id = role_user.user_id
                                  JOIN roles on role_user.role_id = roles.id
                                WHERE jobs.created_at BETWEEN date_sub(curdate(), INTERVAL 44 DAY) AND date_sub(curdate(), INTERVAL 14 DAY)
                                GROUP BY jobs.user_id");

        $users_to_message = [ ];

        foreach ($users as $obj) {
            $users_to_message[ $obj->user_id ] = $obj;
        }
        $userMailer = new UserMailer;
        foreach ($users_to_message as $_obj) {
            //While the email is sent
            $token = str_random(30);

            Meta::create([
                'object_type'=>'App\User',
                'object_id'=> 0,
                'meta_key'=>'token',
                'meta_value'=> $token,
            ]);

            $userMailer = new UserMailer;

            $data = [
                'first_name'   => $_obj->first_name,
                'email' => $_obj->email,
                'subject' => 'Blissful User Recommendation',
                'token' => $token,
            ];

            $userMailer->recommendEmail($data);
        }
    }

    private static function task_newsletter_email()
    {
        $users = DB::select("SELECT
                              users.email,
                              users.id user_id
                            FROM roles
                              JOIN role_user ON roles.id = role_user.role_id
                              JOIN users ON role_user.user_id = users.id
                            WHERE roles.name = 'vendor'");

        $users_to_message = [ ];

        foreach ($users as $obj) {
            $users_to_message[ $obj->user_id ] = $obj;
        }
        foreach ($users_to_message as $_obj) {

            $userMailer = new UserMailer;

            $data = [
                'email' => $_obj->email,
                'subject' => 'Supplier Deals - Jan 2017'
            ];

            $userMailer->newsletterEmail($data);
        }
    }
    /**
     *
     */
    private static function task_inform_vendors_of_about_to_expire_jobs()
    {
        //get the relevant vendors & their contact details

        $message = "Dear vendorName, X number of jobs that would interest you are just about to expire";

//        static::inform($message, $mobile_nos = [], $emails = []);
    }

    /////////////////   FOR USERS
    /**
     *
     */
    private static function task_inform_users_to_close_old_jobs()
    {
//        static::inform($message, $mobile_nos = [], $emails = []);
    }


    /////////////////   DATA TASKS

    /**
     *
     */
    private static function task_populate_analysis_data()
    {
        \App\Libraries\blissful\blissanalysisdata::populate_analysis_budget_bids();
    }

    /**
     *
     */
    private static function task_send_bliss_stats()
    {
        \App\Libraries\blissful\blissstats::run();
    }

    /////////////////   END OF TASKS
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////

    /**
     * @param array $attributes (check what static::$create_defaults has)
     *
     * @return static
     */
    public static function create(array $attributes = [])
    {
        $attributes = array_merge(static::$create_defaults, (array)$attributes, [
            'task_type' => 'task', //it needs to be reminder...
        ]);

        return CronTaskToUser::create($attributes);
    }

    /**
     *
     * @return bool
     */
    public static function run_tasks_in_db()
    {
        $schedule = static::todays_tasks_from_now();
        if(!$schedule || empty($schedule)) {
            return false;
        }

        $_due_task = CronSchedule::dueEvents($schedule);

        if(empty($_due_task)) {
            return false;
        }

        foreach($_due_task as $task) {
            $task_name = str_replace(' ', '_', trim($task->task_name));
            static::run_task($task_name);
        }
    }

    /**
     * This calls the local functions that run the tasks
     *
     * @param      $function_name
     * @param null $param
     *
     * @return mixed
     */
    private static function run_task($function_name, $param = null)
    {
        $_function = 'task_' . $function_name;

        if(is_callable([__CLASS__, $_function])) {
            return call_user_func([__CLASS__, $_function], $param);
        }
    }

    /**
     * Get today's tasks that are meant to still be valid
     *
     * @return mixed
     */
    public static function todays_tasks_from_now()
    {
//        return Cache::remember('todays_tasks_from_now', 60, function () {
        $now = Carbon::now();
        $tomorrow = Carbon::tomorrow();

        return CronSchedule::task()
                           ->where('cron_schedules.repeat_start', '<=', $tomorrow)
                           ->where(function ($query) use ($now) {
                               $query->where('cron_schedules.repeat_stop', '>=', $now)
                                     ->orWhere('cron_schedules.repeat_stop', '0000-00-00 00:00:00')
                                     ->orWhereNull('cron_schedules.repeat_stop');
                           })
                           ->get();
//        });
    }

    /**
     * Send messages to the targets
     *
     * @param array $message_details
     * @param array $mobile_nos
     * @param array $emails
     */
    private static function inform($message_details = [], $mobile_nos = [], $emails = [])
    {
        $message_details = array_merge([
                                           'subject' => 'Reminder',
                                           'message' => '',
                                       ],
                                       $message_details);

        if(!empty($mobile_nos)) {
            BlissNotify::pushSms($message_details['message'], $mobile_nos);
        }

        if(!empty($emails)) {
            BlissNotify::pushEmail($message_details, $emails);
        }
    }

    private static function task_rank_users()
    {

        $users = DB::table('rankings')
                   ->where(DB::raw('MONTH(created_at)'), '=', date('n'))
                   ->groupBy('user_id')
                   ->orderBy('sum_rank', 'DESC')
                   ->get([DB::raw('sum(ranking) as sum_rank'), DB::raw('user_id')]);
        foreach($users as $key => $user) {
            $site_rank = $key + 1;
            $current = Carbon::now();
            $current_rank = DB::table('user_period_rank')
                              ->where('user_id', '=', $user->user_id)
                              ->where(DB::raw('MONTH(created_at)'), '=', date('n'))
                              ->get();

            if(count($current_rank)) {
                DB::table('user_period_rank')
                  ->where('user_id', '=', $user->user_id)
                  ->where(DB::raw('MONTH(created_at)'), '=', date('n'))
                  ->update(['ranking' => $site_rank, 'points' => $user->sum_rank, 'updated_at' => $current]);
            } else {

                DB::table('user_period_rank')->insert(
                    [
                        'user_id'    => $user->user_id,
                        'ranking'    => $site_rank,
                        'points'     => $user->sum_rank,
                        'created_at' => $current,
                        'updated_at' => $current,
                    ]
                );
            }
            // no one touch this, too lazy to order vendor listings so I'm using this as a shortcut

            DB::table('users')->where('id', '=', $user->user_id)->update(['site_rank' => $site_rank]);
        }

    }

    private static function task_rank_users_by_category()
    {

        $classifications = DB::table('classifications')->whereNull('parent_id')->get();
        foreach($classifications as $key => $classification) {
            $cat_id = $classification->id;
            $users = DB::table('classification_user')
                       ->where('classification_id', '=', $cat_id)
                       ->groupBy('classification_user.user_id')
                       ->join('rankings', function ($join) use ($cat_id) {
                           $join->on('classification_user.user_id', '=', 'rankings.user_id');
                       })
                       ->orderBy('sum_rank', 'DESC')
                       ->get([DB::raw('sum(rankings.ranking) as sum_rank'), DB::raw('classification_user.user_id as user_id')]);
            foreach($users as $key => $user) {
                $site_rank = $key + 1;
                $current = Carbon::now();
                $current_rank = DB::table('user_period_rank')
                                  ->where('user_id', '=', $user->user_id)
                                  ->where('cat_id', '=', $cat_id)
                                  ->where(DB::raw('MONTH(created_at)'), '=', date('n'))
                                  ->get();

                if(count($current_rank)) {
                    DB::table('user_period_rank')
                      ->where('user_id', '=', $user->user_id)
                      ->where('cat_id', '=', $cat_id)
                      ->where(DB::raw('MONTH(created_at)'), '=', date('n'))
                      ->update(['ranking' => $site_rank, 'points' => $user->sum_rank, 'updated_at' => $current]);
                } else {

                    DB::table('user_period_rank')->insert(
                        [
                            'user_id'    => $user->user_id,
                            'ranking'    => $site_rank,
                            'cat_id'     => $cat_id,
                            'points'     => $user->sum_rank,
                            'created_at' => $current,
                            'updated_at' => $current,
                        ]
                    );
                }
            }

        }
    }

}