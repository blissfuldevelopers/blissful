<?php
/**
 * Created by PhpStorm.
 * User: davidhenia
 * Date: 17/04/2016
 * Time: 13:51
 */

namespace App\Libraries\blissful;

use Log;
use Cache;
use Carbon\Carbon;
use App\Libraries\blissful\blissnotify;
use App\Models\CronSchedule;
use App\Models\CronTaskToUser;

class BlissReminder
{
    public static $create_defaults = [
        'task_name'    => '',
        'task_type'    => 'reminder',
        'user_id'      => null,
        'cron_task_id' => null,
        'repeat_start' => null,
        'repeat_stop'  => null,
        'repeat'       => null,
    ];

    /**
     * @param array $attributes (check what static::$create_defaults has)
     *
     * @return static
     */
    public static function create(array $attributes = [ ])
    {
        $attributes = array_merge(static::$create_defaults, (array)$attributes, [
            'task_type' => 'reminder', //it needs to be reminder...
        ]);

        return CronTaskToUser::create($attributes);
    }

    /**
     * This task depends on two scheduled tasks (messages:send & reminders:send)
     *  reminders:send - to call this function
     *  messages:send - to send the messages at the correct time
     *
     * It Gets today's reminders
     *  Get those that are meant to be sent out within the next 15 minutes
     *  Add those to the cron_messages table with the run datetime as send datetime
     *
     *
     * @return bool
     */
    public static function send_reminders_in_db()
    {
        $schedule = static::todays_reminders_from_now();
        if (!$schedule || empty( $schedule )) {
            return false;
        }

        //get reminders that will be due for the next 15 minutes
        //so that you just set them into the cron_messages table
        //and let that run
        $_due_within_15_mins = CronSchedule::dueEvents($schedule, 15);

        if (empty( $_due_within_15_mins )) {
            return false;
        }

        $mobile_nos = [ ];
        $email = [ ];
        $message = [ ];
        $send_time = [ ];
        foreach ($_due_within_15_mins as $reminder) {
            $message[ $reminder->task_id ] = $reminder->reminder;
            $send_time[ $reminder->task_id ] = $reminder->next_cron_run_date;
            if ($reminder->phone) {
                $mobile_nos[ $reminder->task_id ][ $reminder->phone ] = clean_mobile_number($reminder->phone);
            }
            if ($reminder->email) {
                $email[ $reminder->task_id ][ $reminder->email ] = $reminder->email;
            }
        }

        foreach ($mobile_nos as $task_id => $phones) {
            BlissNotify::pushSms($message[ $task_id ], $phones, $send_time[ $task_id ]);
        }

        foreach ($email as $task_id => $addresses) {
            $message_details = [
                'subject' => 'Blissful Reminder',
                'message' => $message[ $task_id ],
            ];

            BlissNotify::pushEmail($message_details, $addresses, $send_time[ $task_id ]);
        }
    }

    /**
     * Get today's reminders that are meant to still be valid
     *
     * @return mixed
     */
    public static function todays_reminders_from_now()
    {
        return Cache::remember('todays_reminders_from_now', 30, function () {
            $now = Carbon::now();
            $tomorrow = Carbon::tomorrow();

            return CronSchedule::reminder()->withusersdetails()
                               ->where('cron_schedules.repeat_start', '<=', $tomorrow)
                               ->where(function ($query) use ($now) {
                                   $query->where('cron_schedules.repeat_stop', '>=', $now)
                                         ->orWhere('cron_schedules.repeat_stop', '0000-00-00 00:00:00')
                                         ->orWhereNull('cron_schedules.repeat_stop');
                               })
                               ->get();
        });
    }


}