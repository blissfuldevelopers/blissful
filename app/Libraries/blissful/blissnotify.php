<?php
/**
 * Created by PhpStorm.
 * User: davidhenia
 * Date: 17/04/2016
 * Time: 13:51
 */

namespace App\Libraries\blissful;

use Log;
use App\Libraries\blissful\blissmessage;

class BlissNotify
{


    /**
     * @param        $message
     * @param array  $mobile_numbers
     * @param int    $thread_id
     * @param string $send_date_time
     *
     * @return bool|void
     */
    public static function pushSms($message, $mobile_numbers = [ ], $thread_id = 0, $send_date_time = '<<NOW>>')
    {
        if (empty( $mobile_numbers )) {
            return false;
        }

        $mobile_numbers = ( is_array($mobile_numbers) ) ? $mobile_numbers : [ $mobile_numbers ];

        $message_details = [
            'message'   => $message,
            'send_date' => $send_date_time,
            'thread_id' => $thread_id,
        ];

        $recipient_details = [ ];
        foreach ($mobile_numbers as $mobile_number) {
            $recipient_details [] = [
                'mobile_number' => clean_mobile_number($mobile_number),
            ];
        }

        Log::info('Pushing SMS message ' . $message . ' to: ' . serialize($recipient_details));


        return BlissMessage::createCronMessage($message_details, $recipient_details, $thread_id);
    }

    /**
     * @param array  $message_details
     * @param array  $emails
     * @param int    $thread_id
     * @param string $send_date_time
     *
     * @return bool|void
     */
    public static function pushEmail($message_details = [ ], $emails = [ ], $thread_id = 0, $send_date_time = '<<NOW>>')
    {
        if (empty( $emails )) {
            return false;
        }

        $emails = ( is_array($emails) ) ? $emails : [ $emails ];

        $message_details = array_merge([
                                           'subject'   => ( $message_details[ 'subject' ] ) ? $message_details[ 'subject' ] : '',
                                           'message'   => ( $message_details[ 'message' ] ) ? $message_details[ 'message' ] : '',
                                           'send_date' => $send_date_time,
                                       ], (array)$message_details);

        $recipient_details = [ ];
        foreach ($emails as $email) {
            $recipient_details [] = [
                'email' => $email,
            ];
        }

        Log::info('Pushing EMAIL message ' . serialize($message_details) . ' to: ' . serialize($recipient_details) . ' thread: ' . serialize($thread_id));

        return BlissMessage::createCronMessage($message_details, $recipient_details, $thread_id);
    }

}