<?php
/**
 * Created by PhpStorm.
 * User: davidhenia
 * Date: 17/04/2016
 * Time: 13:50
 */

namespace App\Libraries\blissful;

use Log;
use App\Models\CronMessage;
use App\Models\CronRecipient;
use App\Logic\Mailers\UserMailer;
use App\Classes\AfricasTalkingGateway;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Thread;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use App\Activity;
use App\User;

class BlissMessage
{

    static $default_email_details = [
        'from'    => 'happy@blissful.co.ke',
        'subject' => 'Blissful Support - New Message',
        'message' => '',
    ];

    /**
     *
     */
    public static function sendCronMessages()
    {
        $_messages_query = CronMessage::where('sent', 0)
                                      ->where('send_date', '<=', date('Y-m-d H:i:s'));
        $messages = $_messages_query->get();

        $_messages = [ ];
        $_subjects = [ ];
        $mobile_numbers = [ ];
        $emails = [ ];
        $message_created_by = [ ];
        $sms_status = [ ];
        $email_status = [ ];
        $_sent_message_id = [ ];
        $thread = [ ];
        foreach ($messages as $_message) {
            $recipients = $_message->recipients()
                                   ->where('sent', 0)
                                   ->get();
            $message_created_by[ $_message->id ] = $_message->created_by;
            $thread[ $_message->id ] = $_message->thread_id;

            foreach ($recipients as $recipient) {
                if ($recipient->sent == 0) {
                    $_messages[ $_message->id ] = $_message->message;
                    $_subjects[ $_message->id ] = $_message->subject;
                    $email_status[ $_message->id ] = false;

                    if ($recipient->mobile_number) {
                        $mobile_numbers[ $_message->id ][ $recipient->id ] = $recipient->mobile_number;
                    }

                    if ($recipient->email) {
                        $emails[ $_message->id ][ $recipient->id ] = $recipient->email;
                    }
                }
            }
        } //that loop just makes sure the recipient details aren't repeated in one message...

        if (!empty( $mobile_numbers )) {
            foreach ($mobile_numbers as $message_id => $_mobile_numbers) {

                $statuses = static::sendSMS($_messages[ $message_id ], $_mobile_numbers);

                foreach ($statuses as $status) {
                    $sms_status[ $message_id ][ strtolower($status->status) ] [] = clean_mobile_number($status->number);
                }

                //update the successful ones as sent...
                if (isset( $sms_status[ $message_id ][ 'success' ] )) {

                    CronRecipient::where('sent', 0)
                                 ->where('message_id', $message_id)
                                 ->whereIn('mobile_number', $sms_status[ $message_id ][ 'success' ])
                                 ->update([ 'sent' => 1 ]);
                }

            }

        }

        if (!empty( $emails )) {
            foreach ($emails as $message_id => $_emails) {
                $email_status[ $message_id ] = static::sendEmail([
                                                                     'subject'    => $_subjects[ $message_id ],
                                                                     'message'    => $_messages[ $message_id ],
                                                                     'created_by' => $message_created_by[ $message_id ],
                                                                     'thread_id'  => $thread[ $message_id ],
                                                                 ], $_emails);
                if ($email_status[ $message_id ]) {

                    CronRecipient::where('sent', 0)
                                 ->where('message_id', $message_id)
                                 ->whereIn('email', $_emails)
                                 ->update([ 'sent' => 1 ]);
                }
            }
        }

        //update these messages...
        $_messages_query->update([ 'sent' => 1 ]);
    }

    /**
     * @param       $message_details
     * @param array $recipient_details
     * @param int   $thread_id
     *
     */
    public static function createCronMessage($message_details, $recipient_details = [ ], $thread_id = 0)
    {
        $message_details[ 'recipient_details' ] = $recipient_details;
        $message_details[ 'thread_id' ] = $thread_id;


        Log::info('BlissMessage createCronMessage ' . serialize($message_details) . ' to: ' . serialize($recipient_details));

        CronMessage::create($message_details);
    }

    /**
     * @param       $message
     * @param array $mobile_numbers
     */
    public static function sendSMS($message, $mobile_numbers = [ ])
    {
        if (empty( $mobile_numbers )) {
            return false;
        }

        $results = null;

        $username = env('AT_ID');
        $apikey = env('AT_SECRET');
        array_walk($mobile_numbers, "clean_mobile_number");
        $recipients = implode(',', $mobile_numbers);
        $message = static::formatMessage($message, 'sms');

        $gateway = new AfricasTalkingGateway($username, $apikey);

        try {
            $results = $gateway->sendMessage($recipients, $message);
//            foreach ($results as $result) {

            // status is either "Success" or "error message"
//                Flash::message('Number:' . $result->number);
//                Flash::message('Status:' . $result->status);
//                Flash::message('MessageId:' . $result->messageId);
//                Flash::message('Cost:' . $result->cost);

//            }
        }
        catch (AfricasTalkingGatewayException $e) {
            return false;
//            Flash::message('Encountered an error while sending: ' . $e->getMessage());
        }

        return $results;
    }

    /**
     * @param array $message_details = ['message' => ..., 'from' => 'happy@blissful.co.ke']
     * @param array $emails
     * @param       $thread_id
     *
     * @return array
     */
    public static function sendEmail($message_details = [ ], $emails = [ ])
    {
        $message_details = array_merge(static::$default_email_details, $message_details);

        if (!$message_details[ 'message' ] || $message_details[ 'message' ] == '') {
            return false;
        }

        $message = $message_details[ 'message' ];
        $users = User::whereIn('email', $emails)->get();

        if (!$users) {
            return false;
        }


        $thread_id = ( isset( $message_details[ 'thread_id' ] ) ) ? $message_details[ 'thread_id' ] : 0;

        $sender_id = 0;
        if (isset( $message_details[ 'created_by' ] )) {
            $sender_id = $message_details[ 'created_by' ];
        }

        if (!$sender_id || $sender_id == 0) {
            $sender = ( isset( $message_details[ 'from' ] ) )
                ? User::where('email', $message_details[ 'from' ])->first([ 'id' ])
                : User::where('email', static::$default_email_details[ 'from' ])->first([ 'id' ]);

            $sender_id = $sender->id;
        }

        $subject = ( isset( $message_details[ 'subject' ] ) && $message_details[ 'subject' ] <> '' )
            ? $message_details[ 'subject' ]
            : 'New Blissful Message';

        Log::info('BlissMessage::sendEmail() subject (' . $subject . ') & message (' . $message . ') & sender_id: ' . $sender_id);

        $userMailer = new UserMailer;
        foreach ($users as $user) {

            //The Thread's sender ID is the person who created the message
            static::createMessageThread($subject, $message, $sender_id, [ $user->id ], $thread_id);

            //While the email is sent
            $data = [
                'user_name' => $user->first_name . ' ' . $user->last_name,
                'to'        => $user->email,
                'subject'   => static::formatMessage($subject, 'email'),
                'text'      => $message,
            ];

            $userMailer->simpleEmail($data);
        }

        return true;
    }

    /**
     * @param string $subject
     * @param string $message
     * @param        $sender_id
     * @param array  $recipients_user_ids
     * @param        $thread_id
     *
     * @return static
     */
    public static function createMessageThread($subject = 'New Blissful Message', $message = '', $sender_id, $recipients_user_ids = [ ], $thread_id)
    {
//
        $thread = Thread::find($thread_id);
        if (!$thread) { //what happens here @Peter? do we return false or create a thread?
            $thread = Thread::create([
                                         'subject'    => $subject,
                                         'multi_send' => 3,
                                     ]);
        }
        // Message
        Message::create([
                            'thread_id' => $thread->id,
                            'user_id'   => $sender_id,
                            'body'      => $message,
                        ]);
        // Sender
        Participant::create([
                                'thread_id' => $thread->id,
                                'user_id'   => $sender_id,
                                'last_read' => new Carbon,
                            ]);

        $thread->addParticipants($recipients_user_ids);

        Activity::create([
                             'subject_id'   => $thread->id,
                             'subject_type' => config('messenger.message_model'),
                             'name'         => 'sent_message',
                             'user_id'      => $sender_id,
                         ]);

        return $thread;
    }


    /**
     * @param        $message
     * @param string $recipient_type
     */
    public static function formatMessage($message, $recipient_type = 'sms')
    {
        switch (strtolower($recipient_type)) {
            case 'email':
            case 'emails':
                return static::formatForEmail($message);
                break;

            case 'sms':
                return static::formatForSms($message);
                break;
        }

        return $message;
    }

    /**
     * @param $message
     *
     * @return mixed
     */
    public static function formatForSms($message)
    {
        //Clean for SMS...
        return clean_word(strip_tags(trim($message)));
    }

    /**
     * @param $message
     */
    public static function formatForEmail($message)
    {
        return str_replace([ "\n" ], [ "<br/>" ], strip_tags(clean_word($message), '<br/><a>'));
    }
}