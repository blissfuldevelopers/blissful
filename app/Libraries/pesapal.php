<?php

namespace App\Libraries;

use App\Boost;
use App\BoostToUser;
use Log;
use Cache;
use App\libraries\oauth\OAuthSignatureMethod_HMAC_SHA1;
use App\libraries\oauth\OAuthConsumer;
use App\libraries\oauth\OAuthRequest;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\UpdatePaymentTransaction;
use App\Models\ShopProduct;
use App\Models\ShopOrder;
use App\Models\ShopOrderDetail;
use Cart;
use App\Credit;
use App\Libraries\blissful\blissnotify;
use App\User;

/**
 * Created by PhpStorm.
 * User: davidhenia
 * Date: 20/03/2016
 * Time: 10:42
 */
class PesaPal
{
    use DispatchesJobs;

    public static $use_live_pesapal = true;
    public static $base_url;
    public static $consumer_key;
    public static $consumer_secret;


    public static $iframelink;

    public static $statusrequest;

    public static $merchant_type;
    public static $oauth_callback_url;
    public static $queryStatus;

    /**
     * Behaves like a __construct( ) functions
     *
     */
    public static function init()
    {
        $test_or_live = (static::$use_live_pesapal) ? 'live' : 'test';
        static::$consumer_key = config('paymentgateway.pesapal.consumer_key.' . $test_or_live);
        static::$consumer_secret = config('paymentgateway.pesapal.consumer_secret.' . $test_or_live);


        static::$base_url = config('paymentgateway.pesapal.base_url.' . $test_or_live);
        static::$iframelink = static::$base_url . config('paymentgateway.pesapal.api.iframe');

        static::$statusrequest = static::$base_url . config('paymentgateway.pesapal.api.status_request');

        static::$merchant_type = config('paymentgateway.pesapal.merchant_type'); //default value = MERCHANT

        if (isset($_SERVER['HTTP_HOST'])) {
            static::$oauth_callback_url = $_SERVER['HTTP_HOST'] . '/shop/finalstep';
        }
    }

    /**
     * @param $input
     *
     * @return array
     */
    public static function queryPaymentStatus($input)
    {
        static::init();

        Log::info('Querying Payment status for input: ' . serialize($input));

        $token = $params = null;
        $signature_method = new OAuthSignatureMethod_HMAC_SHA1();
        $consumer = new OAuthConsumer(static::$consumer_key, static::$consumer_secret);
        $status_src = OAuthRequest::from_consumer_and_token($consumer, $token, "GET", static::$statusrequest, $params);
        $merchant_ref = $input['pesapal_merchant_reference'];
        $tracking_id = $input['pesapal_transaction_tracking_id'];

        $url_response = [
            'pesapal_transaction_tracking_id' => $tracking_id,
            'payment_method' => null,
            'payment_status' => 'PENDING',
            'pesapal_merchant_reference' => $merchant_ref,
        ];


        //post transaction to pesapal
        $status_src->query_payment($merchant_ref, $tracking_id);
        $status_src->sign_request($signature_method, $consumer, $token);

        $response = Curl::to($status_src)->get(); //pesapal_response_data=pesapal_transaction_tracking_id,payment_method,payment_status,pesapal_merchant_reference

        if ($response) {
            //pesapal_response_data=pesapal_transaction_tracking_id,payment_method,payment_status,pesapal_merchant_reference
            //payment_status = <PENDING|COMPLETED|FAILED|INVALID>
            $response = str_replace('pesapal_response_data=', '', $response);
            $response_array = explode(',', $response);

            if (count($response_array) > 1) {
                $url_response = array_merge($url_response, [
                    'pesapal_transaction_tracking_id' => $response_array[0],
                    'payment_method' => $response_array[1],
                    'payment_status' => strtoupper($response_array[2]),
                    'pesapal_merchant_reference' => $response_array[3],
                ]);
            } else {
                $url_response = array_merge($url_response, [
                    'payment_status' => strtoupper($response_array[0]),
                ]);
            }
        }

        Log::info('Querying Payment URL response: ' . serialize($url_response));

        return $url_response;
    }

    /**
     * @param      $input
     * @param null $user_id
     * @param bool $QueryPaymentStatus
     *
     * @return array
     */
    public static function paymentFinalStep($input, $user_id = null, $QueryPaymentStatus = true)
    {
        $redirect = 'shop/checkout';
        $message = 'There was trouble processing your payment.';
        $message_status = 'warning';
        $pesapal_status = null;
        if ($QueryPaymentStatus) {
            $pesapal_status = static::queryPaymentStatus($input);
        }

        Log::info('Final Payment Step input: ' . serialize($input) . ' pesapal query status: ' . serialize($pesapal_status));

        //update the order with the payment status
        $update = static::updateOrderStatus($input['pesapal_merchant_reference'], [
            'status' => $pesapal_status['payment_status'],
            'pesapal_transaction_tracking_id' => ((isset($pesapal_status['pesapal_transaction_tracking_id']))
                ? $pesapal_status['pesapal_transaction_tracking_id']
                : ((isset($input['pesapal_transaction_tracking_id']))
                    ? $input['pesapal_transaction_tracking_id']
                    : null
                )
            ),
            'payment_method' => ((isset($pesapal_status['payment_method']))
                ? $pesapal_status['payment_method']
                : ((isset($input['payment_method']))
                    ? $input['payment_method']
                    : null
                )
            ),
            'pesapal_notification_type' => ((isset($pesapal_status['pesapal_notification_type']))
                ? $pesapal_status['pesapal_notification_type']
                : ((isset($input['pesapal_notification_type']))
                    ? $input['pesapal_notification_type']
                    : null
                )
            ),
        ]);

        Log::info('Final Payment update order: ' . serialize($update));

        switch ($pesapal_status['payment_status']) {//<PENDING|COMPLETED|FAILED|INVALID>
            case 'COMPLETED':
            case 'COMPLETE':
            case 'SUCCESS':
                $redirect = '/shop';
                $message_status = 'info';
                $message = 'Thank you for placing an order, allow for 5 minutes for your order to reflect';
                static::finalUpdateOfCompletedPayment($input, $user_id);

                break;

            case 'PENDING':
                $redirect = '/shop';
                $message = "\nPayment Status: '" . ucwords(strtolower($pesapal_status['payment_status'])) . "' ";
                $message .= "\nYou shall be notified when the payment status changes";
                //TODO: Before Adding to queue... find out if there's a cron task already running that updates ALL pending transactions
//                $query_again_queue = (new UpdatePaymentTransaction($input, $user_id))->delay(60);
//                app('Illuminate\Contracts\Bus\Dispatcher')->dispatch($query_again_queue);
                break;

            case 'FAILED':
            case 'INVALID':
            default:
                $message .= "\nPayment Status: '" . ucwords(strtolower($pesapal_status['payment_status'])) . "' ";
                break;
        }


        return [
            'redirect' => $redirect,
            'pesapal_status' => strtoupper(trim($pesapal_status['payment_status'])),
            'message_status' => $message_status,
            'messageZurb' => $message,
        ];

    }

    /**
     * @param       $pesapal_merchant_reference
     * @param array $details
     *
     * @return bool
     */
    public static function updateOrderStatus($pesapal_merchant_reference, $details = [])
    {
        $shop_order = static::getShopOrder(['pesapal_merchant_reference' => $pesapal_merchant_reference]);

        //then we really don't have that order...
        if (!$shop_order) {
            return false;
        }

        $status = $details['status'];
        $pesapalNotification = (isset($details['pesapal_notification_type'])) ? $details['pesapal_notification_type'] : null;
        $pesapal_transaction_tracking_id = (isset($details['pesapal_transaction_tracking_id'])) ? $details['pesapal_transaction_tracking_id'] : null;
        $payment_method = (isset($details['payment_method'])) ? $details['payment_method'] : null;

        $shop_order->pesapal_merchant_reference = $pesapal_merchant_reference;
        $shop_order->pesapal_notification_type = $pesapalNotification;
        $shop_order->pesapal_transaction_tracking_id = $pesapal_transaction_tracking_id;

        if (!is_null($payment_method)) {
            $shop_order->payment_method = $payment_method;
        }

        $shop_order->status = $status;

        return $shop_order->save();
    }

    /**
     *
     */
    public static function updateAllPendingOrders()
    {
        Log::info('Calling PesaPal::updateAllPendingOrders( ) ');

        $pending_shop_orders = ShopOrder::where('status', 'PENDING')
            ->get([
                'pesapal_merchant_reference',
                'pesapal_transaction_tracking_id',
                'user_id',
            ]);

        foreach ($pending_shop_orders as $pending_order) {
            $input = [
                'pesapal_merchant_reference' => $pending_order->pesapal_merchant_reference,
                'pesapal_transaction_tracking_id' => $pending_order->pesapal_transaction_tracking_id,
            ];

            Log::info('PesaPal::updateAllPendingOrders( ) ' . serialize($input));

            PesaPal::paymentFinalStep($input, $pending_order->user_id);
        }
    }

    /**
     * Includes Crediting User if the product is a credit_bundle
     * And emptying their shopping cart
     *
     * @param      $input
     * @param null $user_id
     *
     * @return bool
     */
    public static function finalUpdateOfCompletedPayment($input, $user_id = null)
    {
        Log::info('Final Update of Completed Order: ' . serialize($input));

        $cache_key = 'final_update_of_completed_payment_' . serialize($input);
        Log::info('Final Update cache key check: ' . $cache_key);

        if (Cache::has($cache_key)) {
            Log::info('Final Update cache key exists: ' . $cache_key);

            return true;
        }


        $shop_order = static::getShopOrder($input);
        if (!$shop_order) {
            Log::error('No Shop order found with input : ' . serialize($input));

            return false;
        }

        if (strtoupper($shop_order->status) <> 'COMPLETED') {
            Log::error('Shop order found BUT NOT COMPLETED input : ' . serialize($input));

            return false;
        }

        if (!$user_id) {
            $user_id = $shop_order->user_id;
        }

        $ShopOrderDetails = ShopOrderDetail::where('order_id', $shop_order->id)->get();
        //check if the product is a credit_bundle
        //if it is, then credit the person
        if ($ShopOrderDetails && $ShopOrderDetails->count()) {

            foreach ($ShopOrderDetails as $_shop_order_detail) {
                static::performSpecialProductActivities($_shop_order_detail, $shop_order, $user_id);
            }

            //empty their shopping cart...
            Cart::instance('Cart')->destroy($user_id);
        } else {
            Log::info('PesaPal::updateCreditsUsingCartItems( ) with input: ' . serialize($input));
            static::updateCreditsUsingCartItems($input, $user_id);
        }

        Log::info('Final Update of Completed Order: ' . serialize($input) . ' complete!');
        Log::info('Final Update adding cache key: ' . $cache_key);
        Cache::forever($cache_key, 1);
    }


    /**
     * @param      $ShopOrderDetail
     * @param null $shop_order
     * @param null $user_id
     */
    private static function performSpecialProductActivities($ShopOrderDetail, $shop_order = null, $user_id = null)
    {
        $status = \App\Libraries\blissful\blissshop::performSpecialProductActivities($ShopOrderDetail, $shop_order, $user_id);

        if ((isset($status['message'])) && $status['message'] <> '') {
            static::notifyUser($status['message'], [$user_id], 'Blissful Credits - Successful Transaction');
        }
    }

    /**
     * @param $input
     * @param $user_id
     */
    private static function updateCreditsUsingCartItems($input, $user_id)
    {
        $carts = Cart::instance('Cart')
            ->content($user_id)
            ->toArray();

        foreach ($carts as $ct) {
            $product = ShopProduct::where('id', $ct['id'])->first();

            if ($product) {


                $array_product = $product->toArray();

                $array_product['qty'] = $ct['qty'];
                $array_product['order_id'] = $input['pesapal_merchant_reference'];

                $ShopOrderDetail = ShopOrderDetail::create($array_product);

                static::performSpecialProductActivities($ShopOrderDetail, null, $user_id);

            } else {
                Cart::instance('Cart')->remove($ct['rowid']);
            }
        }

        Cart::instance('Cart')->destroy($user_id);
    }

    /**
     * @param array $details
     *
     * @return null
     */
    private static function getShopOrder($details = [])
    {
        if (empty($details)) {
            return null;
        }
        $shop_order = null;

        $pesapal_transaction_tracking_id = (isset($details['pesapal_transaction_tracking_id']))
            ? $details['pesapal_transaction_tracking_id']
            : null;

        $pesapal_merchant_reference = (isset($details['pesapal_merchant_reference']))
            ? $details['pesapal_merchant_reference']
            : null;

        if ($pesapal_merchant_reference) {
            $shop_order = ShopOrder::where('pesapal_merchant_reference', $pesapal_merchant_reference)->first();

            //if no shop order with that merchant_reference
            if (!$shop_order) { // check for any with that id...
                $shop_order = ShopOrder::where('id', $pesapal_merchant_reference)->first();
            }
        }

        if (!$shop_order) {
            $shop_order = ShopOrder::where('pesapal_transaction_tracking_id', $pesapal_transaction_tracking_id)->first();
        }

        return $shop_order;
    }

    /**
     * @param        $message
     * @param array $user_ids
     * @param string $subject
     */
    private static function notifyUser($message, $user_ids = [], $subject = '')
    {
        $users_to_be_notified = User::whereIn('id', $user_ids)->get();
        if (!$users_to_be_notified) {
            return false;
        }

        $_user_emails = [];
        $_user_mobile_numbers = [];
        $_admin_emails = [];

        foreach ($users_to_be_notified as $user) {
            $_user_mobile_numbers[$user->profile->phone] = $user->profile->phone;
            $_user_emails[$user->email] = $user->email;
        }

        if (!empty($_user_mobile_numbers)) {
            BlissNotify::pushSms($message, $_user_mobile_numbers);
        }

        if (!empty($_user_emails)) {
            BlissNotify::pushEmail(['subject' => $subject, 'message' => $message,], $_user_emails);
        }

        //Notify Site Admins
        $admins_to_be_notified = User::join('role_user', function ($join) {
            $join->on('users.id', '=', 'role_user.user_id');
        })->join('roles', function ($join) {
            $join->on('roles.id', '=', 'role_user.role_id');
            $join->on('roles.name', '=', \DB::raw('"administrator"'));
        })->get([
            'users.email',
        ]);

        foreach ($admins_to_be_notified as $user) {
            $_admin_emails[$user->email] = $user->email;
        }

        if (!empty($_admin_emails)) {
            $subject = 'Payments Activity - ' . $subject;
            $message = "These users ( " . implode('; ', $_user_emails) . " ) have been notified this message: \"" . $message . "\"";
            BlissNotify::pushEmail(['subject' => $subject, 'message' => $message,], $_user_emails);
        }
    }

    /**
     * @param int $amount
     * @param string $desc
     * @param string $reference
     * @param string $first_name
     * @param string $last_name
     * @param string $email
     * @param string $phonenumber
     *
     * @return OAuthRequest
     */
    public static function iframe($amount = 0, $desc = '', $reference = '', $first_name = '', $last_name = '', $email = '', $phonenumber = '')
    {
        if ($reference == '') {
            $ref = str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 5);
            $reference = substr(str_shuffle($ref), 0, 10);
        }

        $token = $params = null;
        $signature_method = new OAuthSignatureMethod_HMAC_SHA1();

        $post_xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><PesapalDirectOrderInfo xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" Amount=\"" . $amount . "\" Description=\"" . $desc . "\" Type=\"" . static::$merchant_type . "\" Reference=\"" . $reference . "\" FirstName=\"" . $first_name . "\" LastName=\"" . $last_name . "\" Email=\"" . $email . "\" PhoneNumber=\"" . $phonenumber . "\" xmlns=\"http://www.pesapal.com\" />";
        $post_xml = htmlentities($post_xml);

        $consumer = new OAuthConsumer(static::$consumer_key, static::$consumer_secret);

        //post transaction to pesapal
        $iframe_src = OAuthRequest::from_consumer_and_token($consumer, $token, "GET", static::$iframelink, $params);

        $iframe_src->set_parameter("oauth_callback", PesaPal::$oauth_callback_url);
        $iframe_src->set_parameter("pesapal_request_data", $post_xml);

        $iframe_src->sign_request($signature_method, $consumer, $token);

        return $iframe_src;
    }

    /**
     * @param $input
     */
    public static function ipn($input)
    {
        $pesapalNotification = $input['pesapal_notification_type'];
        $pesapalTrackingId = $input['pesapal_transaction_tracking_id'];
        $pesapal_merchant_reference = $input['pesapal_merchant_reference'];
        $signature_method = new OAuthSignatureMethod_HMAC_SHA1();


        if ($pesapalNotification == "CHANGE" && $pesapalTrackingId != '') {
            $token = $params = null;
            $consumer = new OAuthConsumer(static::$consumer_key, static::$consumer_secret);

            //get transaction status
            $request_status = OAuthRequest::from_consumer_and_token($consumer, $token, "GET", static::$statusrequestAPI, $params);
            $request_status->set_parameter("pesapal_merchant_reference", $pesapal_merchant_reference);
            $request_status->set_parameter("pesapal_transaction_tracking_id", $pesapalTrackingId);
            $request_status->sign_request($signature_method, $consumer, $token);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $request_status);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

            if (defined('CURL_PROXY_REQUIRED')) {

                if (CURL_PROXY_REQUIRED == 'True') {

                    $proxy_tunnel_flag = (defined('CURL_PROXY_TUNNEL_FLAG') && strtoupper(CURL_PROXY_TUNNEL_FLAG) == 'FALSE') ? false : true;
                    curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, $proxy_tunnel_flag);
                    curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
                    curl_setopt($ch, CURLOPT_PROXY, CURL_PROXY_SERVER_DETAILS);

                }
            }

            $response = curl_exec($ch);

            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $raw_header = substr($response, 0, $header_size - 4);
            $headerArray = explode("\r\n\r\n", $raw_header);
            $header = $headerArray[count($headerArray) - 1];

            //transaction status
            $elements = preg_split("/=/", substr($response, $header_size));
            $status = $elements[1];

            curl_close($ch);

            $update = static::paymentFinalStep($input, null, false);

            $resp = "pesapal_notification_type=$pesapalNotification&pesapal_transaction_tracking_id=$pesapalTrackingId&pesapal_merchant_reference=$pesapal_merchant_reference";
            if ($update && strtolower($status) != 'pending') {
                $resp = "pesapal_notification_type=$pesapalNotification&pesapal_transaction_tracking_id=$pesapalTrackingId&pesapal_merchant_reference=$pesapal_merchant_reference";
                ob_start();
                echo $resp;
                ob_flush();
                exit;
            }
        }
    }
}