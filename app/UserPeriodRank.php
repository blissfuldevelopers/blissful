<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPeriodRank extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_period_rank';
    protected static $db_table = 'user_period_rank';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'user_id','cat_id','ranking'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function classification()
    {
        return $this->belongsTo('App\classification','cat_id');
    }
}
