<?php

namespace App\Listeners;

use App\Events\JobStatusChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Libraries\blissful\blissnotify;

class JobStatusNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  JobStatusChanged  $event
     * @return void
     */
    public function handle(JobStatusChanged $event)
    {
        switch ($event->bid->status) {
            case 0:
                $status = 'New';
                break;
            case 1:
                $status = 'Shortlisted';
                break;
            case 2:

                $message_details = [
                    'subject' => 'Your Bid Status',
                    'message' => 'Your bid was accepted',
                ];

                static::inform($message_details, null, [$event->bid->user->email]);
                break;
            case 3:

                $message_details = [
                    'subject' => 'Your Bid Status',
                    'message' => 'Sorry, Your bid was declined',
                ];

                static::inform($message_details, null, [$event->bid->user->email]);
                break;
            case 4:
                $status = 'In Review';
                break;
        }
    }
    private static function inform($message_details = [], $mobile_nos = [], $emails = [])
    {
        $message_details = array_merge([
            'subject' => 'Reminder',
            'message' => '',
        ],
            $message_details);

        if (!empty($mobile_nos)) {
            BlissNotify::pushSms($message_details['message'], $mobile_nos);
        }

        if (!empty($emails)) {
            BlissNotify::pushEmail($message_details, $emails);
        }
    }
}
