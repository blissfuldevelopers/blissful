<?php

namespace App\Listeners;

use App\Events\JobWasCreated;
use App\Models\Boost;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Libraries\blissful\blissnotify;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class JobCreationNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  JobWasCreated $event
     * @return void
     */
    public function handle(JobWasCreated $event)
    {
        $users_to_notify = DB::select("SELECT
                                          classification_user.user_id,
                                          users.first_name,
                                          users.email,
                                          profiles.phone
                                        FROM classification_user
                                          JOIN users ON users.id = classification_user.user_id
                                          JOIN profiles ON users.id = profiles.user_id
                                          JOIN boost_user ON users.id = boost_user.user_id
                                          JOIN boosts ON boost_user.boost_id = boosts.id
                                        WHERE classification_id = {$event->job->classification_id}
                                              AND boost_user.stop_date > now() AND (boosts.id = 1 OR boosts.id = 2)
                                        GROUP BY classification_user.user_id");

        Log::info(count($users_to_notify) . ' users to notify');

        foreach ($users_to_notify as $user) {
            $message_details = [
                'subject' => $event->job->title,
                'message' => $event->job->description,
            ];

            static::inform($message_details, [$user->phone], [$user->email]);
        }
        Log::info(count($users_to_notify) . ' users notified');
    }

    private static function inform($message_details = [], $mobile_nos = [], $emails = [])
    {
        $message_details = array_merge([
            'subject' => 'Notification',
            'message' => '',
        ],
            $message_details);

        if (!empty($mobile_nos)) {
            BlissNotify::pushSms($message_details['message'], $mobile_nos);
        }

        if (!empty($emails)) {
            BlissNotify::pushEmail($message_details, $emails);
        }
    }
}
