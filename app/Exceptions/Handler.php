<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;
use Illuminate\Database\QueryException as QueryException;
use App\Jobs\SendErrorsEmail;
use Config;
use Illuminate\Support\Facades\Auth;


class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        //get the error
        $debugSetting = Config::get('app.debug');
        //only send emails on live
        if ($debugSetting == false) {
            if (!empty($e->getMessage())) {
                $error_message = "Error message: ".$e->getMessage()."<br> Error reported at: ".$e->getFile()."<br> On line: " .$e->getLine();
            
                //dispatch mail to the tech team
                // try {
                //     dispatch(new SendErrorsEmail($error_message));
                // } catch (Exception $e) {
                //     return parent::report($e);
                // }
            }
        }
        
        return parent::report($e);

    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {

        if (Auth::check() && Auth::user()->hasUserRole('administrator')) {
            return parent::render($request, $e);
        }
       // 404 page when a model is not found
        if ($e instanceof ModelNotFoundException) {
            return response()->view('errors.404', [], 404);
        }
        //500 page on Query exception
        if ( $e instanceof QueryException && app()->environment() == 'production') {
            return response()->view('errors.500', [], 500);
        }

        

        $debugSetting = Config::get('app.debug');
        if ($debugSetting == false) {
            return response()->view('errors.500', [], 500);
        }
        return parent::render($request, $e);

    }

}