<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = ['gallery_id', 'file_name', 'file_size','file_title', 'file_path', 'created_by', 'caption']; 

	public function gallery(){
		return $this->belongsTo('App\Gallery');
	}
	public function tags(){
		return $this->belongstoMany('App\Tag')->withTimestamps();
	}
}
