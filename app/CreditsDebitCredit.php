<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CreditsDebitCredit extends Model
{
    protected $table = 'credits_debit_credits';
    protected static $db_table = 'credits_debit_credits';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [ 'user_id', 'credit', 'debit', 'object_id', 'object_name' ];

    /**
     * This gives a better history
     * with an opening balance
     *
     * @return string
     */
    public static function credit_history_table_sql($user_id = null)
    {
        $where_user_id = ( $user_id ) ? " AND users.id=" . $user_id : null;

        return DB::Select("
            SELECT users.id as user_id, concat(users.first_name,' ',users.last_name) user_name,
                credits_debit_credits.created_at as date,
                credits_debit_credits.op_balance as Opening_Balance,
                credits_debit_credits.credit,
                credits_debit_credits.debit,
                credits_debit_credits.balance
            from (
                SELECT
                  t.*,
                  @tot_credit := if(@prev_client = t.user_id, @tot_credit + t.credit,t.credit) as tot_cred,
                  @tot_debit := if(@prev_client = t.user_id,@tot_debit + t.debit,t.debit) as tot_deb,
                  @cur_bal := if(@prev_client = t.user_id, @tot_credit - @tot_debit,t.credit-t.debit) as balance,
                  (@cur_bal + t.debit) - t.credit as op_balance,
                  @prev_client := t.user_id
                  from( SELECT * from credits_debit_credits order by user_id, created_at )t,
                  (SELECT @prev_client:=0,@cur_bal:=0,@tot_credit:=0,@tot_debit:= 0,@open_balance:=0) r
              ) credits_debit_credits
              join users on credits_debit_credits.user_id = users.id
              where
              1=1
              $where_user_id
              Order by credits_debit_credits.created_at ASC 
              ");
    }

    public static function credit_history($user_id = null)
    {
        $report = static::credit_summary($user_id);
    }

    /**
     * @param null $user_id
     *
     *
     * @return mixed
     */
    public static function credit_summary($user_id = null)
    {

        $result_ = DB::table('users')
                     ->join('credits_debit_credits', 'users.id', '=', 'credits_debit_credits.user_id')
                     ->leftJoin('credits', function ($join) {
                         $join
                             ->on('credits_debit_credits.object_id', '=', 'credits.id')
                             ->on('credits_debit_credits.object_name', '=', DB::raw("'credits'"));
                     })
                     ->leftJoin('credit_bundles', function ($join) {
                         $join
                             ->on('credits.credit_bundle_id', '=', 'credit_bundles.id');
                     })
                     ->leftJoin('shop_product', function ($join) {
                         $join
                             ->on('credit_bundles.product_id', '=', 'shop_product.id');
                     })
                     ->leftJoin('bids', function ($join) {
                         $join
                             ->on('bids.id', '=', 'credits_debit_credits.object_id')
                             ->on('credits_debit_credits.user_id', '=', 'bids.user_id')
                             ->on('users.id', '=', 'bids.user_id')
                             ->on('credits_debit_credits.object_name', '=', DB::raw("'bids'"));
                     })
                     ->leftJoin('jobs', function ($join) {
                         $join
                             ->on('jobs.id', '=', 'bids.job_id');
                     })
                     ->select(
                         'users.id as user_id',
                         DB::raw("concat(users.first_name,' ',users.last_name) name"),
                         'credit',
                         'debit',
                         DB::raw('(sum(IFNULL(credit,0)) - sum(IFNULL(debit, 0))) as credit_balance'),
                         'shop_product.name as credit_bundle_purchased',
                         'credits.quantity as no_of_credit_bundles_purchased',
                         'shop_product.sale_price as credit_bundle_sale_price',
                         'credit_bundles.credits as total_credits_purchased',
                         'credits.created_at as purchase_date',
                         'jobs.title as job_bid_for',
                         'bids.created_at as job_bid_date'
                     )
                     ->groupBy('users.id');

        if ($user_id) {
            $result_->where('users.id', $user_id)
                    ->groupBy('user_id');
        }

        $result_
            ->groupBy('credits_debit_credits.id')
            ->orderBy('credits_debit_credits.id', 'desc');

        return $result_;
    }
    public static function purchased_credit_table_sql()
    {
        $result_ = DB::table('credits')
            ->join('users', 'users.id', '=', 'credits.user_id')
            ->join('credit_bundles', 'credit_bundles.id', '=', 'credits.credit_bundle_id')
            ->select(
                'user_id',
                DB::raw("concat(users.first_name,' ',users.last_name) name"),
                'credit_bundles.credits',
                'quantity'
            )
            ->get();

        return $result_;
    }

    /**
     * @param null $user_id
     */
    public static function total_balance($user_id = null, $return_first_result = true)
    {
        $total_ = DB::table(static::$db_table)
                    ->select(
                        DB::raw('(sum(IFNULL(credit,0)) - sum(IFNULL(debit, 0))) as balance')
                    );

        if ($user_id) {
            $total_->where('user_id', $user_id)
                   ->groupBy('user_id');
        }

        if ($return_first_result) {
            $balance = $total_->first('balance');

            return ( isset( $balance->balance ) ) ? $balance->balance : 0;
        }

        return $total_->get('balance');
    }

    /**
     * @param null $user_id
     *
     * @return mixed
     */
    public static function total_credit($user_id = null)
    {
        $total_ = DB::table(static::$db_table);

        if ($user_id) {
            $total_->where('user_id', $user_id)
                   ->groupBy('user_id');
        }

        return $total_->sum('credit');
    }

    /**
     * @param null $user_id
     *
     * @return mixed
     */
    public static function total_debit($user_id = null)
    {
        $total_ = DB::table(static::$db_table);

        if ($user_id) {
            $total_->where('user_id', $user_id)
                   ->groupBy('user_id');
        }

        return $total_->sum('debit');
    }

    /**
     * @param     $user_id
     * @param int $amount
     *
     * @return static
     */
    public static function debitUser($user_id, $amount = 0, $object_id = null, $object_name = null)
    {
        return static::create([
                                  'user_id'     => $user_id,
                                  'debit'       => $amount,
                                  'object_id'   => $object_id,
                                  'object_name' => $object_name,
                              ]);
    }

    /**
     * @param     $user_id
     * @param int $amount
     *
     * @return static
     */
    public static function creditUser($user_id, $amount = 0, $object_id = null, $object_name = null)
    {
        return static::create([
                                  'user_id'     => $user_id,
                                  'credit'      => $amount,
                                  'object_id'   => $object_id,
                                  'object_name' => $object_name,
                              ]);
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function credit()
    {
        return $this->belongsTo('App\Credit', 'credit_id');
    }

}