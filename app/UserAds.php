<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAds extends Model
{
 	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type','link','full_img_src','mobile_img_src','name','user_id','start_date','stop_date'];

}
