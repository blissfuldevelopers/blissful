<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Activity;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Classification;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mixpanel;
use DateTime;
use App\Models\Event;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Job extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jobs';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'classification_id',
        'budget',
        'event_date',
        'event_location',
        'credits',
        'user_id',
        'event_id',
        'expiry_date',
        'completed',
        'in_review',
        'guest_number',
        'estimate_budget',
        'planning_requested',
    ];
    public static $db_attributes = [
        'title',
        'description',
        'classification_id',
        'budget',
        'event_date',
        'event_location',
        'credits',
        'user_id',
        'event_id',
        'expiry_date',
        'completed',
        'in_review',
        'guest_number',
        'estimate_budget',
        'planning_requested',
    ];
    public static $inputRules = [
        'classification' => 'required',
        'budget' => 'required_without_all:estimate_budget',
        'event_date' => 'required',
        'event_location' => 'required',
        'phone'          => 'required',
    ];
    public static $job_credits = 60;

    protected $appends = [
        'date_time_to_event',
        'expired',
        'user_can_bid',
        'user_has_bid',
        'user_can_view_bids',
    ];

    private static function pluralize($count, $text)
    {
        return $count . (($count == 1) ? (" $text") : (" ${text}s"));
    }

    /**
     * @return bool
     */
    public function getExpiredAttribute()
    {
        $datetime1 = new \DateTime();
        $datetime2 = new \DateTime(date('Y-m-d', strtotime($this->event_date)));

        return $this->attributes['datetime_to_event'] = ($datetime1 > $datetime2);
    }

    /**
     * @return null|string
     */
    public function getDateTimeToEventAttribute()
    {
        if($this->expired) {
            return $this->attributes['datetime_to_event'] = null;
        }

        $datetime1 = new \DateTime();
        $datetime2 = new \DateTime(date('Y-m-d', strtotime($this->event_date)));


        $interval = $datetime1->diff($datetime2);
        $interval_days = '';
        $suffix = ' ';
        if($v = $interval->y >= 1) {
            $interval_days .= static::pluralize($interval->y, 'year') . $suffix;
        }
        if($v = $interval->m >= 1) {
            $interval_days .= static::pluralize($interval->m, 'month') . $suffix;
        }
        if($v = $interval->d >= 1) {
            $interval_days .= static::pluralize($interval->d, 'day') . $suffix;
        }

        if($v = $interval->h >= 1) {
            $interval_days .= static::pluralize($interval->h, 'hour') . $suffix;
        } else {
            if($v = $interval->i >= 1) {
                $interval_days .= static::pluralize($interval->i, 'minute') . $suffix;
            } else {
                $interval_days .= static::pluralize($interval->s, 'second') . $suffix;
            }
        }

        return $this->attributes['datetime_to_event'] = $interval_days . ' left';
    }
    public function getCountdownAttribute(){

        $datetime1 = new \DateTime(date('Y-m-d h:i:s', strtotime(Carbon::now())));
        $datetime2 = new \DateTime(date('Y-m-d h:i:s', strtotime($this->expiry_date)));



        if ($datetime2 > $datetime1) {

            $interval = $datetime1->diff($datetime2);
            $interval_days = '';
            $suffix = ' ';

            if($v = $interval->d > 5) {
                return " ";
            }
            elseif($v = $interval->d > 1) {
                $interval_days .= static::pluralize($interval->d, 'day') . $suffix;
            }

            elseif($v = $interval->h > 1) {
                $interval_days .= static::pluralize($interval->h, 'hour') . $suffix;
            }
            elseif($v = $interval->i > 1) {
                $interval_days .= static::pluralize($interval->i, 'minute') . $suffix;
            } 
            else{
                $interval_days .= static::pluralize($interval->s, 'second') . $suffix;
            }
        
            return $this->attributes['countdown'] = 'Closing in '.$interval_days; 
        }
        elseif(Auth::check() && !Auth::user()->hasUserRole('vendor')){
            return "Job has been closed";
        }
        else{
            return false;
        }
           
    }

    /**
     * @return bool
     */
    public function getUserCanBidAttribute()
    {
        if($this->expired) {
            return $this->attributes['user_can_bid'] = false;
        }

        $user = Auth::user();
        if(!$user || !$user->can_bid_for_job || $this->user_id == $user->id) {
            return $this->attributes['user_can_bid'] = false;
        }

        if($this->user_has_bid) {
            return $this->attributes['user_can_bid'] = false;
        }

        return $this->attributes['user_can_bid'] = true;
    }

    /**
     * @return bool
     */
    public function getUserCanViewBidsAttribute()
    {

        $user = Auth::user();
        if($this->user_id == $user->id || $user->hasUserRole('administrator')) {
            return $this->attributes['user_can_view_bids'] = true;
        }

        return $this->attributes['user_can_view_bids'] = false;
    }

    /**
     * @return bool
     */
    public function getUserCanViewJobAttribute()
    {

        $user = Auth::user();

        $credit_balance = CreditsDebitCredit::total_balance($user->id);

        $date_diff = DB::Select("SELECT
                      datediff(curdate(), max(bids.created_at)) as date_difference
                    FROM bids
                    GROUP BY user_id
                    HAVING user_id = $user->id
                    ");
        if($this->user_id == $user->id || $user->hasUserRole('administrator')) {
            return $this->attributes['user_can_view_bids'] = true;
        }
        if(!$date_diff) {
            return $this->attributes['user_can_view_job'] = true;
        }
        foreach($date_diff as $date) {
            if($date->date_difference < 30 || $credit_balance > 100) {
                return $this->attributes['user_can_view_job'] = true;
            }
        }
    }

    /**
     * @return bool
     */
    public function getUserHasBidAttribute()
    {
        $user = Auth::user();
        if(!$user) {
            return $this->attributes['user_has_bid'] = false;
        }

        foreach($this->bids as $bid) {
            if($bid->user_id == $user->id)
                return $this->attributes['user_has_bid'] = $bid;
        }

        return $this->attributes['user_has_bid'] = false;
    }

    public static function fetch_job($attributes = [])
    {

    }

    /**
     * @param array $attributes
     *
     * @return mixed
     */
    public static function save_job($attributes = [])
    {
        $user_id = $attributes['user_id'];
        $event_date = $attributes['event_date'];

        $date = str_replace('/', '-', $event_date);
        $event_date = date("Y-m-d", strtotime($date));
        $attributes['event_date'] = $event_date;
        
        $event = Event::where(function ($q) use ($user_id, $event_date) {
            $q->where('user_id', '=', $user_id)
              ->where('start', '=', $event_date);
        })->first();

        if(is_null($event)) {

            $endOfDay = DateTime::createFromFormat('Y-m-d', $event_date);

            $endOfDay->modify('today');
            $endOfDay->modify('-1 second');
            if (!array_key_exists('event_name', $attributes)) {
                $attributes['event_name'] = "Wedding";
            }
            $event = Event::create([
                                       'title'   => $attributes['event_name'],
                                       'start'   => $event_date,
                                       'end'     => $endOfDay,
                                       'user_id' => $user_id,
                                       'fullday' => 1,
                                       'desc'    => 'Requests for a ' . $attributes['event_name'] . ' scheduled for ' . $attributes['event_date'],
                                   ]);
        }

        $job_classification = Classification::where('id', $attributes['classification_id'])->first();

//        $one_twenty_list = [
//            "Event Set Up",
//            "Event Planning",
//            "Catering"
//        ];
//
//        $one_fifty_list = [
//            "Photos and Videos"
//        ];
//
//        if (in_array($job_classification->name, $one_fifty_list)) {
//            $job_credits = 150;
//        } elseif(in_array($job_classification->name, $one_twenty_list)) {
//            $job_credits = 120;
//        } else {
//            $job_credits = 100;
//        }

        $jobAttributes = array_merge([
            'event_id' => $event->id,
            'credits' => $job_classification->bid_credits,
            'title' => 'Request for ' . $job_classification->name,
            'event_date' => (isset($event_date)) ? date('Y-m-d', strtotime($event_date)) : date('Y-m-d'),
            'budget'    =>(isset($attributes['budget'])?$attributes['budget'] : $attributes['estimate_budget']),
        ], (array)$attributes);

        // calculating expiry date
        $now = date('Y-m-d');
        // $event_date = strtotime($jobAttributes['event_date']);
        // $datediff = $event_date - $now;
        // $days_to_event = floor($datediff / (60 * 60 * 24));
        $date = DateTime::createFromFormat('Y-m-d', $now);

        //set expiry date to 2 weeks from now
        $date->modify('+2 weeks');
        
        $jobAttributes['expiry_date'] = date_format($date, 'Y-m-d');


        $jobAttributes = array_only($jobAttributes, static::$db_attributes);

        $job = static::firstOrCreate($jobAttributes);

        if($job->wasRecentlyCreated) {
            Activity::create([
                                 'subject_id'   => $job->id,
                                 'subject_type' => 'App\Job',
                                 'name'         => 'created_job',
                                 'user_id'      => $jobAttributes['user_id'],
                             ]);

        } else {
            Activity::create([
                                 'subject_id'   => $job->id,
                                 'subject_type' => 'App\Job',
                                 'name'         => 'updated_job',
                                 'user_id'      => $jobAttributes['user_id'],
                             ]);
        }

        return $job;
    }

    public static function filter_jobs_query_by_user_type($jobs_query, $user)
    {
        if($user->hasUserRole('administrator')) {
            return $jobs_query->withTrashed();
        }

        if($user->hasUserRole('vendor') && $user->classifications) {

            $classifications = [];

            foreach($user->classifications as $classification) {
                array_push($classifications, $classification->id);
                // $classifications[ 'classification' ] = $classification->name;
            }

            if(!empty($classifications)) {
                $jobs_query->whereIn('classification_id', $classifications)
                            ->whereHas('user', function ($query) {
                                $query->where('verified',1);
                            })
                            ->where('event_date', '>=', date('Y-m-d H:i:s', strtotime('NOW')))
                            ->where('completed', '=', 0)
                            ->where('in_review', '=', 0);


            }


            // $jobs_query->orWhere(function ($query) use ($classifications, $user) {
            //     $query
            //         ->where('event_date', '>=', date('Y-m-d H:i:s', strtotime('NOW')));
            // });

        } else {
            $jobs_query->where('user_id', $user->id);
        }


        return $jobs_query;
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function bids()
    {
        return $this->hasMany('App\Bid');
    }

    public function location()
    {
        return $this->hasOne('App\JobLocations', 'job_id');
    }

    public function event()
    {
        return $this->belongsTo('App\Model\Event', 'event_id');
    }

    public function classifications()
    {
        return $this->belongsToMany('App\Classification');
    }

    public function category()
    {
        return $this->belongsTo('App\Classification', 'classification_id');
    }
    public function invitees()
    {
        return $this->belongsToMany('App\User');
    }
    public function is_invited($user_id){
        
        if (in_array($user_id, $this->invitees()->lists('user_id')->toArray())) {
            return true;
        }
        
    }
}
