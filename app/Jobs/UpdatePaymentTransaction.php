<?php

namespace App\Jobs;

use Log;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Libraries\PesaPal;

class UpdatePaymentTransaction extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $input;
    protected $user_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input, $user_id)
    {
        $this->input = $input;
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $this->PesaPalPaymentFinalStep();
    }

    private function PesaPalPaymentFinalStep()
    {
        $status = PesaPal::paymentFinalStep($this->input, $this->user_id);
    }


}
