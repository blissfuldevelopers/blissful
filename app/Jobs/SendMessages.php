<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use App\Logic\Mailers\UserMailer;
use Cmgmyr\Messenger\Models\Thread;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use App\Activity;
use Illuminate\Support\Facades\Input;
use Laracasts\Flash\Flash;
use App\Classes\AfricasTalkingGateway;
use App\Jobs\SendErrorsEmail;
use OneSignal;
use App\Devices;
use App\Bid;

class SendMessages extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

     /**
     * Create a new job instance.
     *
     * @return void
     */
    
    protected $messageAttributes;

    public function __construct($messageAttributes)
    {
        $this->messageAttributes = $messageAttributes;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {       

            $attributes = $this->messageAttributes;
            $user = User::where('id',$attributes['user_id'])->first();
            $recipient = User::where('id',$attributes['recipients'][0])->first();

            //create thread
            $thread = Thread::create(
                [
                    'subject'    => 'Response from ' . $user->first_name,
                    'multi_send' => 3,
                ]
            );
            
            // Create Message

            Message::create(
                [
                    'thread_id'  => $thread->id,
                    'user_id'    => $user->id,
                    'body'       => $attributes[ 'message' ],
                    'multi_send' => 3,
                ]
            );

            // add Sender
            Participant::create(
                [
                    'thread_id' => $thread->id,
                    'user_id'   => $user->id,
                    'last_read' => new Carbon,
                ]
            );
            if ($recipient) {
                $thread->addParticipant($attributes[ 'recipients' ]);
            }

            //check if this is a bid
            if (array_key_exists('bid_id', $attributes)) {
                $message = "You have a response to your quotation request on blissful.co.ke from ".$user->first_name;

                //add thread id to bid but check if the bid id is valid
                if ($attributes['bid_id'] > 0) {
                    $bid = Bid::where('id',$attributes['bid_id'])->first();
                    $bid->thread_id = $thread->id;
                    $bid->save();
                }
                 
            }
            else{
                $message = "You have a message on blissful.co.ke from ".$user->first_name;
            }


            //Send Email

            $userMailer = new UserMailer;
            
            $data = [
                'user_name' => $recipient->first_name . ' ' . $recipient->last_name,
                'to'        => $recipient->email,
                'subject'   => 'Response to quotation request on blissful.co.ke',
                'text'      => $message,
            ];

            $userMailer->simpleEmail($data);


             //SMS notification
            $username = env('AT_ID');

            $apikey = env('AT_SECRET');

            $to = $recipient->profile->phone;

            $gateway = new AfricasTalkingGateway($username, $apikey);

            try {
                // Thats it, hit send and we'll take care of the rest.
                $results = $gateway->sendMessage($to, $message);
                foreach ($results as $result) {
                    // status is either "Success" or "error message"
                    Flash::message('Number:' . $result->number);
                    Flash::message('Status:' . $result->status);
                    Flash::message('MessageId:' . $result->messageId);
                    Flash::message('Cost:' . $result->cost);
                }
            }
            catch (AfricasTalkingGatewayException $e) {
                Flash::message('Encountered an error while sending: ' . $e->getMessage());
            }

            //send push notification
            $recipient_devices = Devices::where('user_id', $recipient->id)->get();
            $device_ids = '';

            foreach ($recipient_devices as $key => $recipient_device) {

                    if ($key == 0) {
                        $device_ids = $recipient_device->device_id;
                    }
                    else{
                        $device_ids .= $recipient_device->device_id.',';
                    }
                    

            }
            

           
            if (count($recipient_devices)) {
                
                try {
                    $response = OneSignal::postNotification([
                        "include_player_ids"   =>   [$device_ids],
                        "contents"              =>  ["en" => "You have a new request from ".$user->first_name],
                        "headings"              =>  ["en" => "Blissful.co.ke"],
                        "url"                   =>  "blissful.co.ke/messages/".$thread->id,
                    ]);
                    
                } 
                catch(\Exception $e) {
                    //dispatch mail to the tech team
                    $error_message = "Error message: ".$e->getMessage()."<br> Error reported at: ".$e->getFile()."<br> On line: " .$e->getLine()."for user ".$user->first_name;

                    dispatch(new SendErrorsEmail($error_message));
                }
                
                
            }
            
            
    }
}
