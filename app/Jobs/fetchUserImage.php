<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use App\User;
use Intervention\Image\Facades\Image;
use Illuminate\Contracts\Filesystem\Filesystem;

class fetchUserImage extends Job implements SelfHandling
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {   
        $id = $this->id;
        //find user
        $user = User::find($id);

        $email = $user->email;
        
        $url = "http://avatarapi.com/js.aspx?email=".$email;
        // first let's scrap avatar api cause its free
        $ch = curl_init($url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $res = curl_exec($ch);
        
        $dom = new \DOMDocument();
        if (isset($res) && $res !== '') {
            $dom->loadHTML($res);
        }
        

        $tag = $dom->getElementsByTagName('img')->item(0);
        // if the user is found
        if ($tag) {
            $src = $tag->getAttribute('src');
        }
        //not found? let's creep some more...
        else{
            //send curl to clearbit, its not free :(
            $url = "https://person.clearbit.com/v1/people/email/".$email;
            $ch = curl_init($url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Bearer '.env('CLEARBIT_API_KEY')]); // -H
            $res = curl_exec($ch);
            $res = json_decode($res, true);
            $key = "avatar";
            if(array_key_exists($key, $res) && isset($res[$key])) {
                    $src = $res[$key];
            }
        }

        // fetch & save the image 
        if (isset($src)) {

            $filename = uniqid().basename($src);
            $thumb = Image::make($src);

            $image_thumb = $thumb->stream();
            $s3 = \Storage::disk('s3');
            $filePath = 'gallery/images/featured-images/' . $filename;
            $s3->getDriver()->put($filePath, $image_thumb->__toString(),[ 
                
                                                                            'visibility' => 'public',
                                                                            'CacheControl' => 'max-age=2592000',
                                                                            'Expires' => gmdate("D, d M Y H:i:s T",strtotime("+3 years"))

                                                                        ]);

            //save details to user profile
            $profile = $user->profile;
            $profile->profilePic = $filePath;
            $user->profile()->save($profile);
            
        }
        
    }
}
