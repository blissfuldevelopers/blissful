<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use App\Logic\User\UserRepository;
use App\Logic\Mailers\UserMailer;
use Cmgmyr\Messenger\Models\Thread;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use App\Activity;
use Illuminate\Support\Facades\Input;
use Laracasts\Flash\Flash;
use App\Classes\AfricasTalkingGateway;
use OneSignal;
use App\Devices;
use DB;
use App\Jobs\SendErrorsEmail;

class SendMultipleLeads extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $id;
    protected $input;
    protected $userRepository;
    protected $userMailer;

    public function __construct($id, $input,UserRepository $userRepository,UserMailer $userMailer)
    {
        $this->id = $id;
        $this->input = $input;
        $this->userRepository = $userRepository;
        $this->userMailer = $userMailer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {


        if (empty( $_POST[ 'mult-vendors' ] ) && !isset( $_POST[ 'mult-vendors' ] )) {
            if (Input::has('recipients')) {
                $recipients = ( $this->input[ 'recipients' ] );
                $originalRecipients = ( $this->input[ 'recipients' ] );
            }
        }
        else {
            $originalRecipients = $this->input[ 'recipients' ];
            $recipients = $this->input[ 'mult-vendors' ];
            foreach ($originalRecipients as $originalRecipient) {
                array_push($recipients, $originalRecipient);
            }
        }

        //check if logged in
        if (!$this->id == false) {
            $user = User::findorfail($this->id);
            $user_id = $user->id;
            $contact_name = $user->first_name;
            $new = 0;
        }
        else {

            $user = User::where('email', $this->input[ 'sender_mail' ])->get();

            if (!$user->count()) {

                $random = substr(md5(rand()), 0, 8);

                $data = [
                    'first_name' => $this->input[ 'sender_name' ],
                    'last_name'  => '',
                    'email'      => $this->input[ 'sender_mail' ],
                    'verified'   => 1,
                    'password'   => $random,
                    'messenger'  => 1,

                ];
                $new = 1;
                $this->userRepository->register($data);
                $user = User::where('email', $this->input[ 'sender_mail' ])->get();
                foreach ($user as $sender) {
                    $user_id = $sender->id;
                    $contact_name = $sender->first_name;
                }
            }
            else {
                foreach ($user as $sender) {
                    $user_id = $sender->id;
                    $contact_name = $sender->first_name;
                    $new = 0;
                }
            }
        }
        //if not check if email exists 

        //get user details if exists

        //if not create user
        foreach ($recipients as $recipient) {

            //check if its a multiplied lead

            foreach ($originalRecipients as $originalRecipient) {
                if ($recipient == $originalRecipient) {
                    $multi_send = 0;
                }
                else {

                    $multi_send = 1;
                }
            }

            $user = User::findorfail($recipient);

            $thread = Thread::create(
                [

                    'subject'    => 'Contact regarding an event on ' . $this->input[ 'event_date' ],
                    'multi_send' => $multi_send,
                ]
            );
            // Message

            Message::create(
                [
                    'thread_id'  => $thread->id,
                    'user_id'    => $user_id,
                    'body'       => $this->input[ 'message' ],
                    'multi_send' => $multi_send,
                ]
            );

            // Sender
            Participant::create(
                [
                    'thread_id' => $thread->id,
                    'user_id'   => $user_id,
                    'last_read' => new Carbon,
                ]
            );

            Activity::create([
                                 'subject_id'   => $thread->id,
                                 'subject_type' => Config::get('messenger.message_model'),
                                 'name'         => 'sent_message',
                                 'user_id'      => $user_id,
                             ]);

            // Recipients
            if ($recipient) {
                $participant = [ ];
                array_push($participant, $recipient);
                $thread->addParticipant($participant);
            }
            //push notification
            //first get ids
            $recipient_devices = Devices::where('user_id', $user->id)->get();
            $device_ids = '';

            foreach ($recipient_devices as $key => $recipient) {

                    if ($key == 0) {
                        $device_ids = $recipient->device_id;
                    }
                    else{
                        $device_ids .= $recipient->device_id.',';
                    }
                    

            }
            

           
            if (count($recipient_devices)) {
                
                try {
                    $response = OneSignal::postNotification([
                        "include_player_ids"   =>   [$device_ids],
                        "contents"              =>  ["en" => "You have a new request from ".$user->first_name],
                        "headings"              =>  ["en" => "Blissful.co.ke"],
                        "url"                   =>  "blissful.co.ke/messages/".$thread->id,
                    ]);
                    
                } catch (Exception $e) {
                    //dispatch mail to the tech team
                    $error_message = "Error message: ".$e->getMessage()."<br> Error reported at: ".$e->getFile()."<br> On line: " .$e->getLine()."for user ".$user->first_name;

                    dispatch(new SendErrorsEmail($error_message));
                }
                
                
            }
            
            
            //email notification
            $link = 'login';
            $data = [
                'first_name'   => $user->first_name,
                'link'         => $link,
                'contact_name' => $contact_name,
                'subject'      => 'Contact regarding an event on ' . $this->input[ 'event_date' ],
                'email'        => $user->email,
            ];

            $this->userMailer->emailNotification($user->email, $data);
            // $mailUser->emailNotification($user->email, $data);
            //SMS notification
            $username = env('AT_ID');

            $apikey = env('AT_SECRET');

            $to = $user->profile->phone;

            $message = "You have a message on blissful.co.ke";

            $gateway = new AfricasTalkingGateway($username, $apikey);

            try {
                // Thats it, hit send and we'll take care of the rest.
                $results = $gateway->sendMessage($to, $message);
                foreach ($results as $result) {
                    // status is either "Success" or "error message"
                    Flash::message('Number:' . $result->number);
                    Flash::message('Status:' . $result->status);
                    Flash::message('MessageId:' . $result->messageId);
                    Flash::message('Cost:' . $result->cost);
                }
            }
            catch (AfricasTalkingGatewayException $e) {
                Flash::message('Encountered an error while sending: ' . $e->getMessage());
            }
            return $user;
        }
        // if ($new == 1) {
        //     $email      = Input::get('email');
        //     $password   = Input::get('password');
        //     $remember   = Input::get('remember');

        //     if($this->auth->attempt(

        //         [
        //             'email'=> $email,
        //             'verified' => 1,
        //             'password'  => $password
        //         ], $remember == 1 ? true : false))
        //     {
        //         Activity::create([
        //             'subject_id'=> $this->auth->user()->id,
        //             'subject_type'=>'App\User',
        //             'name'=>'login',
        //             'user_id'=> $this->auth->user()->id,
        //         ]);
        //         return redirect()->intended('user');
        //     }
        // }
    }
}
