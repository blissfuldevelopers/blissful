<?php

namespace App\Jobs;

use App\Post;
use App\Category;
use Carbon\Carbon;
use Illuminate\Contracts\Bus\SelfHandling;

class PostFormFields extends Job implements SelfHandling
{
  /**
   * The id (if any) of the Post row
   *
   * @var integer
   */
  protected $id;

  /**
   * List of fields and default value for each field
   *
   * @var array
   */
  protected $fieldList = [
    'title' => '',
    'subtitle' => '',
    'page_image' => '',
    'content' => '',
    'meta_description' => '',
    'is_draft' => "0",
    'publish_date' => '',
    'publish_time' => '',
    'layout' => 'blog.layouts.post',
    'categories' => [],
    'content_raw'=>'',
  ];

  /**
   * Create a new command instance.
   *
   * @param integer $id
   */
  public function __construct($id = null)
  {
    $this->id = $id;
  }

  /**
   * Execute the command.
   *
   * @return array of fieldnames => values
   */
  public function handle()
  {
    $fields = $this->fieldList;

    if ($this->id) {
      $fields = $this->fieldsFromModel($this->id, $fields);
    } else {
      $when = Carbon::now()->addHour();
      $fields['publish_date'] = $when->format('M-j-Y');
      $fields['publish_time'] = $when->format('g:i A');
    }

    foreach ($fields as $fieldName => $fieldValue) {
      $fields[$fieldName] = old($fieldName, $fieldValue);
    }

    return array_merge(
      $fields,
      ['allcategories' => Category::lists('category')->all()]
    );
  }

  /**
   * Return the field values from the model
   *
   * @param integer $id
   * @param array $fields
   * @return array
   */
  protected function fieldsFromModel($id, array $fields)
  {
    $post = Post::findOrFail($id);

    $fieldNames = array_keys(array_except($fields, ['categories']));

    $fields = ['id' => $id];
    foreach ($fieldNames as $field) {
      $fields[$field] = $post->{$field};
    }

    $fields['categories'] = $post->categories()->lists('category')->all();

    return $fields;
  }
}
