<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Logic\Mailers\UserMailer;

class SendErrorsEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $error_message;

    public function __construct($error_message)
    {
        $this->error_message = $error_message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {   
        $error_message = $this->error_message;
        
        $userMailer = new UserMailer;

        $data = [
            'to'        => "tech@blissful.co.ke",
            'subject'   => 'A new feature has been discovered!',
            'text'      => $error_message,
        ];

        $userMailer->errorsEmail($data);
    }
}
