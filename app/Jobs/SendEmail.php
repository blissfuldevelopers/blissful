<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use App\User;
use App\Logic\Mailers\UserMailer;
use DB;
use PDO;

class SendEmail extends Job implements SelfHandling
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $attributes;

    public function __construct($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {   
        $attributes = $this->attributes;

        if (array_key_exists('user_type', $attributes)) {

            DB::connection()->setFetchMode(PDO::FETCH_ASSOC);

            $user_result = DB::table('profiles')
                            ->join('role_user', 'profiles.user_id', '=', 'role_user.user_id')
                            ->join('users', 'users.id', '=', 'profiles.user_id')
                            ->where('role_user.role_id','=',$attributes['user_type'])
                            ->where('users.last_login','!=','0000-00-00 00:00:00')
                            ->select('users.email')
                            ->get();
            $users = array_filter(array_column($user_result, 'email'));
            
            foreach ($users as $user) {

                $userMailer = new UserMailer;
                $email = filter_var($user, FILTER_SANITIZE_EMAIL);

                if (isset($email)) {

                    $data = [
                    'to'        => $email,
                    'subject'   => $attributes['title'],
                    'text'      => $attributes['message'],
                    ];

                    try {
                      $userMailer->errorsEmail($data);
                    }
                    catch (\Exception $e) {
                        
                    }
                    
                }
                
            }
        }
        elseif (array_key_exists('email', $attributes)) {

            $userMailer = new UserMailer;
            
            $data = [
              'to'        => $attributes['email'],
              'subject'   => $attributes['title'],
              'text'      => $attributes['message'],
            ];

            try {
              $userMailer->errorsEmail($data);
            }
            catch (\Exception $e) {
                
            }
        }

        else{

          $user = User::where('id',$attributes['user_id'])->first();
          $email = $user->email;

          $userMailer = new UserMailer;
          $email = filter_var($email, FILTER_SANITIZE_EMAIL);

          if (isset($email)) {

              $data = [
              'to'        => $email,
              'subject'   => $attributes['title'],
              'text'      => $attributes['message'],
              ];

              try {
                $userMailer->errorsEmail($data);
              }
              catch (\Exception $e) {
                  
              }
              
          }

        }
        
        
    }
}
