<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Classes\AfricasTalkingGateway;
use DB;
use Laracasts\Flash\Flash;
use PDO;
use App\User;

class SendSMS extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $messageAttributes;

    public function __construct($messageAttributes)
    {
        $this->messageAttributes = $messageAttributes;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {       

            $attributes = $this->messageAttributes;

            if (array_key_exists('user_type', $attributes)) {
                DB::connection()->setFetchMode(PDO::FETCH_ASSOC);
                $user_result = DB::table('profiles')
                                ->join('role_user', 'profiles.user_id', '=', 'role_user.user_id')
                                ->join('users', 'users.id', '=', 'profiles.user_id')
                                ->where('role_user.role_id','=',$attributes['user_type'])
                                ->where('users.last_login','!=','0000-00-00 00:00:00')
                                ->select('profiles.phone')
                                ->get();
                $user_numbers = array_filter(array_column($user_result, 'phone'));
                
                //SMS notification
                $username = env('AT_ID');

                $apikey = env('AT_SECRET');
                
                $to = implode(",",$user_numbers);
                
                $message = $attributes['message'];

                if (isset($to)) {
                   $gateway = new AfricasTalkingGateway($username, $apikey);

                    try {
                        // Thats it, hit send and we'll take care of the rest.
                        $results = $gateway->sendMessage($to, $message);
                    }
                    catch (AfricasTalkingGatewayException $e) {
                        Flash::message('Encountered an error while sending: ' . $e->getMessage());
                    }
                }
                
            }
            else{

                $user = User::where('id',$attributes['user_id'])->first();

                //SMS notification
                $username = env('AT_ID');

                $apikey = env('AT_SECRET');

                $to = $user->profile->phone;

                $message = $attributes['message'];
                
                if (isset($to)) {
                   $gateway = new AfricasTalkingGateway($username, $apikey);

                    try {
                        // Thats it, hit send and we'll take care of the rest.
                        $results = $gateway->sendMessage($to, $message);
                    }
                    catch (AfricasTalkingGatewayException $e) {
                        Flash::message('Encountered an error while sending: ' . $e->getMessage());
                    }
                }
            }
            
            

    }
}
