<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Conner\Tagging\TaggableTrait;

class Gallery extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'gallery';

    public function images(){
        return $this->hasMany('App\Image');
    }
    public function videos(){
        return $this->hasMany('App\Video');
    }
     public function links(){
        return $this->hasMany('App\Link');
    }
    public function user(){
        return $this->hasOne('App\User');
    }
}
