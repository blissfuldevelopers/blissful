<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

	protected $fillable = ['name'];

    public function image() {

    	return $this->belongsToMany('App\Image');
    }
    public function video() {

    	return $this->belongsToMany('App\Video');
    }
    public function link() {

    	return $this->belongsToMany('App\Link');
    }
}
