<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\ShopProduct;
use App\Models\ShopCategory;
use App\Models\ShopSubcategory;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class CreditBundle extends Model
{
    protected $table = 'credit_bundles';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'credits',
        'unit_price',
        'expiration_time',
    ];
    public static $db_attributes = [
        'product_id',
        'credits',
        'unit_price',
        'expiration_time',
    ];
    public static $input_validation = [
        'name'            => 'required',
        'description'     => 'required',
        'credits'         => 'required',
        'cost'            => 'required',
        'unit_price'      => 'required',
        'expiration_time' => 'required',
    ];

    protected $appends = [
        'name',
        'description',
        'cost',
    ];

    public function getNameAttribute()
    {
        return $this->attributes[ 'name' ] = $this->product->name;
    }

    public function getDescriptionAttribute()
    {
        return $this->attributes[ 'description' ] = $this->product->description;
    }

    public function getCostAttribute()
    {
        return $this->attributes[ 'cost' ] = $this->product->sale_price;
    }


    /**
     * @param array $attributes
     * @param null  $credit_bundle_id
     *
     * @return bool|static
     */
    public static function saveCreditBundle($request, $credit_bundle_id = null)
    {
        $attributes = $request->all();
        if (empty( $attributes )) {
            return false;
        }
        if ($request->hasFile('cover_image_path')) {
            $credits_full_price = $attributes['unit_price'] * $attributes['credits'];
            $cost = (isset($attributes['cost'])) ? $attributes['cost'] : 0;
            $product_id = null;
            $credit_bundle = null;
            $discount = (($credits_full_price - $cost) < 0)
                ? ($credits_full_price - $cost) * -1
                : ($credits_full_price - $cost);
            $coverimg = $request->file('cover_image_path');
            $covername = uniqid() . $coverimg->getClientOriginalName();
            $thumb = Image::make($coverimg->getRealPath())->resize(600, 400);
            $image_thumb = $thumb->stream();
            $s3 = \Storage::disk('s3');
            $filePath = 'uploads/products/full_size' . $covername;
            $s3->put($filePath, $image_thumb->__toString(), 'public');

            $product_attributes = array_merge((array)$attributes, [
                'price' => $credits_full_price,
                'sale_price' => $cost,
                'discount' => $discount,
                'status' => 'active',
                'seller_id' => (Auth::user()) ? Auth::user()->id : 0,
                'qty' => 1,
                'description' => $request['description'],
                'picture' => ('uploads/products/full_size' . $covername),
            ]);
        }else{
            $credits_full_price = $attributes['unit_price'] * $attributes['credits'];
            $cost = (isset($attributes['cost'])) ? $attributes['cost'] : 0;
            $product_id = null;
            $credit_bundle = null;
            $discount = (($credits_full_price - $cost) < 0)
                ? ($credits_full_price - $cost) * -1
                : ($credits_full_price - $cost);

            $product_attributes = array_merge((array)$attributes, [
                'price' => $credits_full_price,
                'sale_price' => $cost,
                'discount' => $discount,
                'status' => 'active',
                'seller_id' => (Auth::user()) ? Auth::user()->id : 0,
                'qty' => 1,
                'description' => $request['description'],
            ]);
        }
        $product_attributes = array_only($product_attributes, ShopProduct::$db_attributes);

        if ($credit_bundle_id) {
            $credit_bundle = static::findOrFail($credit_bundle_id);
            $product = ShopProduct::findOrFail($credit_bundle->product_id);
            $product->update($product_attributes);
            $product_id = $credit_bundle->product_id;
        }
        else {
            //first create (or update) the product
            $product = ShopProduct::firstOrCreate($product_attributes);
            $product_id = $product->id;

        }

        $product_category = ShopCategory::firstOrCreate([
                                                            'name'        => 'Blissful Credits',
                                                        ]);

        if ($product_category->wasRecentlyCreated) {
            $product_category->assignUserRole('vendor');
        }

        $product->category_id = $product_category->id;

        $product_subcategory = ShopSubcategory::firstOrCreate([
                                                                  'name'        => 'Credit Bundle',
                                                              ]);

        $product->sub_category_id = $product_subcategory->id;
        $product->product_slug = str_slug($product->name, "-");

        $product->save();


        $_attributes = array_merge((array)$attributes, [
            'product_id'      => $product_id,
            'expiration_time' => ( isValidTimeStamp($attributes[ 'expiration_time' ]) )
                ? strtotime($attributes[ 'expiration_time' ])
                : $attributes[ 'expiration_time' ],
        ]);
        $_attributes = array_only($_attributes, static::$db_attributes);

        if ($credit_bundle) {
            return $credit_bundle->update($_attributes);
        }

        return parent::firstOrCreate($_attributes);
    }

    public function user_credits()
    {
        return $this->hasMany('App\Credit', 'credit_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\ShopProduct', 'product_id');
    }
}
