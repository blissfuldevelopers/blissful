<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Classification extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $table = 'classifications';

    protected $fillable = ['name', 'description', 'slug', 'parent_id','root_parent_id','bid_credits'];

    protected $dates = [ 'deleted_at' ];

    public function users()
    {

        return $this->belongsToMany('App\user')->withTimestamps();
    }

    public function parent()
    {
        return $this->belongsTo('App\Classification','parent_id','id');
    }

    public function children()
    {
        return $this->hasMany('App\Classification', 'parent_id','id');
    }
    public function root_parent()
    {
        return $this->belongsTo('App\Classification','root_parent_id','id');
    }
    public function featured_vendors()
    {
        return $this->belongsToMany('App\user','classification_featured_vendor','classification_id','user_id')->wherePivot('stop_date', '>',Carbon::now());
    }
    public function user_rankings()
    {
        return $this->belongsToMany('App\user','user_period_rank','cat_id','user_id')->withTimestamps();
    }
    public function latest_user_rankings()
    {
        return $this->belongsToMany('App\user','user_period_rank','cat_id','user_id')->withTimestamps();
    }
    public function jobs()
    {
        return $this->hasMany('App\Job')->with('classification');
    }
    public function descriptions()
    {
        return $this->hasMany('App\Description');
    }
}
