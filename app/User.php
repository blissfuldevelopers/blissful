<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Cmgmyr\Messenger\Traits\Messagable;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Bican\Roles\Traits\HasRoleAndPermission; //* For Further reading https://github.com/romanbican/roles
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Cache;
use App\UserTokens;
use App\Jobs\SendSMS;
use DateTime;

/**
 * @SWG\Definition(
 *      definition="User",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract, HasRoleAndPermissionContract
{

    use Authenticatable, CanResetPassword, Messagable, SoftDeletes, HasRoleAndPermission;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    protected static $db_table = 'users';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'email', 'password', 'slug', 'verified', 'added_by_admin'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    public static $rules = [
        'first_name' => 'required',
        'email' => 'required|email|unique:users,email',
        'password' => 'required|min:6|max:20',
        'password_confirmation' => 'required|same:password',
        'phone' => 'required',
        'g-recaptcha-response' => 'required',
    ];

    public static $messages = [
        'first_name.required' => 'First Name is required',
        'email.required' => 'Email is required',
        'email.email' => 'Email is invalid',
        'password.required' => 'Password is required',
        'password.min' => 'Password needs to have at least 6 characters',
        'password.max' => 'Password maximum length is 20 characters',
        'phone' => 'Your phone number is required',
        'g-recaptcha-response.required' => 'Captcha is required',
    ];
    public static $contact_rules = [
        'event_date.required' => 'The event date is required',
        'message.required' => 'You are required to write a message',
        'g-recaptcha-response.required' => 'Captcha is required',
    ];
    public static $contact_messages = [
        'sender_name.required' => 'First Name is required',
        'sender_mail.required' => 'Email is required',
        'sender_mail.email' => 'Email is invalid',
        'g-recaptcha-response.required' => 'Captcha is required',
    ];

    protected $appends = [
        'admin',
        'vendor',
        'user',
        'can_bid_for_job',
    ];

    public static function fetch_blissful_support()
    {
        return Cache::rememberForever('blissful_support_user', function () {
            return static::where('email', 'happy@blissful.co.ke')->first();
        });
    }


    public function UsersWithSimilarClassificationAs($user_id)
    {
        return DB::table(static::$db_table)
            ->join('classification_user', function ($join) use ($user_id) {
                $join->on('classification_user.user_id', '=', DB::raw($user_id));
            })
            ->join('classification_user as similar_cu', function ($join) {
                $join
                    ->on('similar_cu.user_id', '=', 'users.id')
                    ->on('classification_user.classification_id', '=', 'similar_cu.classification_id');
            })
            ->where('users.id', '!=', $user_id)
            ->select('users.*');

    }


    public function getVendorAttribute()
    {
        return $this->attributes['vendor'] = (bool)$this->hasUserRole('vendor');
    }

    public function getAdminAttribute()
    {
        return $this->attributes['admin'] = (bool)($this->hasUserRole('admin'));
    }

    public function getUserAttribute()
    {
        return $this->attributes['user'] = (bool)$this->hasUserRole('user');
    }

    public function getCanBidForJobAttribute()
    {
        $user = Auth::user();

        if ($user && $user->hasUserRole('administrator')) {
            return $this->attributes['can_bid_for_job'] = true;
        }

        return $this->attributes['can_bid_for_job'] = $this->can('create.bids');

    }

    ///////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////
    ////////// These are being handled by a different item now
    //////////  calling / defining these overrides the new fns
    /// * For Further reading on the roles functions being used
    /// * https://github.com/romanbican/roles

//    public function roles()
//    {
//        return $this->belongsToMany('App\Role')->withTimestamps();
//    }

//    public function type()
//    {
//        return $this->belongsToMany('App\Role');
//    }

    /**
     *  Use this when $this->hasRole() isn't working for some strange reason...
     *
     * @param $name
     *
     *
     * @return bool
     */
    public function hasUserRole($name)
    {
        if ($this->hasRole($name)) {
            return true;
        }

        foreach ($this->roles as $role) {
            if ($role->name == $name)
                return true;
        }

        return false;
    }
    ///////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////

    /**
     * @param $role // you can pass whole role object, or just an id
     *              For Further reading on the roles functions being used
     *              https://github.com/romanbican/roles
     *
     * @return bool|null
     */
    public function assignRole($role)
    {
        return $this->attachRole($role);  // you can pass whole role object, or just an id
//        return $this->roles()->attach($role);
    }

    /**
     * @param $role // you can pass whole role object, or just an id
     *              For Further reading on the roles functions being used
     *              https://github.com/romanbican/roles
     *
     * @return int
     */
    public function removeRole($role)
    {
        return $this->detachRole($role);   // you can pass whole role object, or just an id
//        return $this->roles()->detach($role);
    }

    public function social()
    {
        return $this->hasMany('App\Social');
    }

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function galleries()
    {
        return $this->hasMany('App\Gallery', 'created_by');
    }

    public function activity()
    {
        return $this->hasMany('App\Activity')->with(['user', 'subject'])->latest();
    }

    public function job()
    {
        return $this->hasMany('App\Job')->with('user');
    }

    public function bid()
    {
        return $this->hasMany('App\Bid')->with('user');
    }

    public function credits()
    {
        return $this->hasMany('App\Credit')->with('user');
    }

    public function credits_debits_credits()
    {
        return $this->hasMany('App\CreditsDebitCredit')->with('user');
    }

    public function recordActivity($name, $related)
    {
        return $related->recordActivity($name);
    }

    public function credit()
    {
        return $this->hasOne('App\Credit');
    }

    public function classifications()
    {

        return $this->belongsToMany('App\Classification')->withTimestamps();
    }

    public function featured_classifications()
    {

        return $this->belongsToMany('App\Classification', 'classification_featured_vendor', 'user_id', 'classification_id')->withTimestamps();
    }

    public function cron_tasks()
    {
        return $this->belongsToMany('App\Models\CronTaskToUser', 'cron_task_to_user', 'user_id', 'cron_task_id')->withTimestamps();
    }

    public function meta()
    {
        return $this->morphMany('App\Meta', 'object');
    }

    function hasMeta($key)
    {
        $meta = $this->meta;

        foreach ($meta as $item):
            if ($item->meta_key == $key) return true;
        endforeach;

        return false;
    }

    public function rankings()
    {
        return $this->hasMany('App\Ranking');
    }

    public function recalculateRanking($ranking)
    {
        $rankings = $this->rankings();
        $this->ranking = $rankings->sum('ranking');
        $this->save();
    }

    public function scopeRanked($query)
    {
        return $query->where('ranking', '>', 0);
    }

    public function periodic_rankings()
    {
        return $this->hasMany('App\UserPeriodRank');
    }

    public function top_vendor()
    {
        if (!$this->check_in_range()) {
            return false;
        }
        $current_rank = DB::table('user_period_rank')
            ->where('user_id', '=', $this->id)
            ->whereNull('cat_id')
            ->where(DB::raw('MONTH(created_at)'), '=', date('n'))
            ->first();

        if ($current_rank && $current_rank->ranking <= 20 && $current_rank->points >= 15) {
            return true;
        }
    }

    public function recommended_vendor()
    {   
        if (!$this->check_in_range()) {
            return false;
        }
        $current_rank = DB::table('user_period_rank')
            ->where('user_id', '=', $this->id)
            ->whereNotNull('cat_id')
            ->orderBy('points', 'DESC')
            ->where(DB::raw('MONTH(created_at)'), '=', date('n'))
            ->first();
        if ($current_rank && $current_rank->ranking <= 3 && $current_rank->points >= 15) {
            return true;
        }
    }

    public function devices()
    {
        return $this->hasMany('App\Devices', 'user_id');
    }

    public function boosts()
    {
        return $this->belongsToMany('App\Models\Boost')->withPivot('stop_date');
    }

    public function hasBoost($name)
    {
        foreach ($this->boosts as $boost) {
            $datetime1 = new \DateTime();
            $datetime2 = new \DateTime(date('Y-m-d', strtotime($boost->pivot->stop_date)));

            if ($boost->name == $name && $datetime1 < $datetime2) return true;
        }

        return false;
    }

    public function todos()
    {
        return $this->hasMany('App\Todo');
    }
    public function files()
    {
        return $this->hasMany('App\FileEntry');
    }

    public function ads(){
        return $this->hasMany('App\UserAds', 'user_id');
    }
    public function tokens(){
        return $this->hasMany('App\UserTokens', 'user_id');
    }
    public function descriptions(){
        return $this->hasMany('App\Description', 'user_id');
    }

    public function invited_jobs()
    {
        return $this->hasMany('App\Job');
    }

    public function send_sms_verification($user_id){


        //prepare SMS attributes
        $random = substr(md5(rand()), 0, 6);

        //delete all previous tokens
        UserTokens::where('user_id',$user_id)->delete();

        //create new token instance
        $token = new UserTokens;
        $token->user_id = $user_id;
        $token->token = $random;
        $token->save();

        $message = $random." is your blissful user activation code";

        $messageAttributes = [
            'user_id' => $user_id,
            'message' => $message
        ];

        //dispatch confirmation SMS to queue
        dispatch(new SendSMS($messageAttributes));

    }

    public function check_in_range(){

        $result_date = $this->last_login;
        
        //create some range values
        $start_date = date_format(DateTime::createFromFormat('Y-m-d', date('Y-m-01')), 'Y-m-d');
        $end_date = date_format(DateTime::createFromFormat('Y-m-d', date('Y-m-t')), 'Y-m-d');
        
        // Convert to timestamp
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $user_ts = strtotime($result_date);
        
      // Check that user date is between start & end
      return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }
    public function can_claim(){
        if($this->last_login == 0){
            return true;
        }
    }

    public function location()
    {
        return $this->hasOne('App\UserLocations','user_id');
    }
    public function bookings()
    {
        return $this->hasMany('App\Bookings','vendor_id');
    }
    public function bookmarks()
    {
        return $this->hasMany('App\Bookings','user_id');
    }

    public function can_book(){

        $cat = $this->classifications()->where('name','Venue')->get();
        if ($cat->count() > 0) {
            return $cat;
        }
        
    }

}
