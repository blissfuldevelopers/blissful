<?php

namespace App\Services\Mailers;

abstract class Mailer
{
    public function emailTo($user, $view, $data, $subject)
    {
        \Mail::send($view, $data, function($message) use($user, $subject)
        {
            $message->to($user->email)
                ->subject($subject);
        });
    }
}