<?php
namespace App\Services\Mailers;

class UserMailer extends Mailer
{
    protected $user;

    function __construct()
    {
        $this->user = \Auth::user();
    }

    public function subscribed()
    {
        $subject    = 'Welcome to the site!';
        $view       = 'emails.user.subscribed';
        $data       = ['enter view data here'];
        $this->emailTo($this->user, $view, $data, $subject);
    }

    public function notified()
    {
        $subject    = 'You Have a message!';
        $view       = 'emails.email-notification';
        $data       = ['first_name', 'email', 'phone'];
        $this->emailTo($this->user, $view, $data, $subject);
    }

}