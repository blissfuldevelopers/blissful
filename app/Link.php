<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{	
	protected $fillable = ['gallery_id', 'links', 'created_by', 'caption']; 

    public function gallery(){
		return $this->belongsTo('App\Gallery');
	}
	public function tags(){
		return $this->belongstoMany('App\Tag')->withTimestamps();
	}
}
