<?php

if (!function_exists('clean_mobile_number')) {
    function clean_mobile_number($no, $country_prefix = 254)
    {
        $no = str_replace(' ', null, $no);
        $no = str_replace('+', null, $no);
        $no = str_replace('-', null, $no);
        $no = str_replace('+' . $country_prefix, $country_prefix, $no);

        if (starts_with($no, '0'))
            $no = $country_prefix . substr($no, 1);

        if (strlen($no) == 9)
            $no = $country_prefix . $no;
        elseif (strlen($no) < 9)
            $no = $country_prefix . str_pad($no, 9, '0', STR_PAD_LEFT);

        return $no;
    }
}

if (!function_exists('get_ip_address')) {
    function get_ip_address()
    {
        foreach (array( 'HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR' ) as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[ $key ]) as $ip) {
                    $ip = trim($ip); // just to be safe

                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
                        return $ip;
                    }
                }
            }
        }
    }
}

if (!function_exists('isValidTimeStamp')) {
    function isValidTimeStamp($timestamp)
    {
        return ( (string)(int)$timestamp === $timestamp )
        && ( $timestamp <= PHP_INT_MAX )
        && ( $timestamp >= ~PHP_INT_MAX );
    }
}

if (!function_exists('human_filesize')) {
    /**
     * Return sizes readable by humans
     */
    function human_filesize($bytes, $decimals = 2)
    {
        $size = [ 'B', 'kB', 'MB', 'GB', 'TB', 'PB' ];
        $factor = floor(( strlen($bytes) - 1 ) / 3);

        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) .
        @$size[ $factor ];
    }
}


if (!function_exists('is_image')) {
    /**
     * Is the mime type an image
     */
    function is_image($mimeType)
    {
        return starts_with($mimeType, 'image/');
    }
}

if (!function_exists('checked')) {
    /**
     * Return "checked" if true
     */
    function checked($value)
    {
        return $value ? 'checked' : '';
    }
}

if (!function_exists('page_image')) {
    /**
     * Return img url for headers
     */
    function page_image($value = null)
    {
        if (empty( $value )) {
            $value = config('blog.page_image');
        }
        if (!starts_with($value, 'http') && $value[ 0 ] !== '/') {
            $value = config('blog.uploads.webpath') . '/' . $value;
        }

        return $value;
    }
}


if (!function_exists('clean_word')) {

    function clean_word($str)
    {
        $str = preg_replace('/[[:^print:]]/', '', $str);
        $str = iconv(
            mb_detect_encoding($str, mb_detect_order(), true),
            "UTF-8",
            $str);

        $specials = array(
            '‘'            => "'",
            '’'            => "'",
            '”'            => '"',
            '“'            => '"',
            "\t"           => ' ', // tab character
            "\xC2\xAB"     => '"', // « (U+00AB) in UTF-8
            "\xC2\xBB"     => '"', // » (U+00BB) in UTF-8
            "\xE2\x80\x98" => "'", // ‘ (U+2018) in UTF-8
            "\xE2\x80\x99" => "'", // ’ (U+2019) in UTF-8
            "\xE2\x80\x9A" => "'", // ‚ (U+201A) in UTF-8
            "\xE2\x80\x9B" => "'", // ‛ (U+201B) in UTF-8
            "\xE2\x80\x9C" => '"', // “ (U+201C) in UTF-8
            "\xE2\x80\x9D" => '"', // ” (U+201D) in UTF-8
            "\xE2\x80\x9E" => '"', // „ (U+201E) in UTF-8
            "\xE2\x80\x9F" => '"', // ‟ (U+201F) in UTF-8
            "\xE2\x80\xB9" => "'", // ‹ (U+2039) in UTF-8
            "\xE2\x80\xBA" => "'", // › (U+203A) in UTF-8
            chr(133)       => '...',
            chr(145)       => "'",
            chr(146)       => "'",
            chr(147)       => '"',
            chr(148)       => '"',
            chr(149)       => '*',
            chr(150)       => '-',
            chr(151)       => '-',
            chr(226)       => '-',

        );

        $reverse = array(
            "\x80" => "\xE2\x82\xAC", // EURO SIGN
            "\x82" => "\xE2\x80\x9A", // SINGLE LOW-9 QUOTATION MARK
            "\x83" => "\xC6\x92", // LATIN SMALL LETTER F WITH HOOK
            "\x84" => "\xE2\x80\x9E", // DOUBLE LOW-9 QUOTATION MARK
            "\x85" => "\xE2\x80\xA6", // HORIZONTAL ELLIPSIS
            "\x86" => "\xE2\x80\xA0", // DAGGER
            "\x87" => "\xE2\x80\xA1", // DOUBLE DAGGER
            "\x88" => "\xCB\x86", // MODIFIER LETTER CIRCUMFLEX ACCENT
            "\x89" => "\xE2\x80\xB0", // PER MILLE SIGN
            "\x8A" => "\xC5\xA0", // LATIN CAPITAL LETTER S WITH CARON
            "\x8B" => "\xE2\x80\xB9", // SINGLE LEFT-POINTING ANGLE QUOTATION MARK
            "\x8C" => "\xC5\x92", // LATIN CAPITAL LIGATURE OE
            "\x8E" => "\xC5\xBD", // LATIN CAPITAL LETTER Z WITH CARON
            "\x91" => "\xE2\x80\x98", // LEFT SINGLE QUOTATION MARK
            "\x92" => "\xE2\x80\x99", // RIGHT SINGLE QUOTATION MARK
            "\x93" => "\xE2\x80\x9C", // LEFT DOUBLE QUOTATION MARK
            "\x94" => "\xE2\x80\x9D", // RIGHT DOUBLE QUOTATION MARK
            "\x95" => "\xE2\x80\xA2", // BULLET
            "\x96" => "\xE2\x80\x93", // EN DASH
            "\x97" => "\xE2\x80\x94", // EM DASH
            "\x98" => "\xCB\x9C", // SMALL TILDE
            "\x99" => "\xE2\x84\xA2", // TRADE MARK SIGN
            "\x9A" => "\xC5\xA1", // LATIN SMALL LETTER S WITH CARON
            "\x9B" => "\xE2\x80\xBA", // SINGLE RIGHT-POINTING ANGLE QUOTATION MARK
            "\x9C" => "\xC5\x93", // LATIN SMALL LIGATURE OE
            "\x9E" => "\xC5\xBE", // LATIN SMALL LETTER Z WITH CARON
            "\x9F" => "\xC5\xB8" // LATIN CAPITAL LETTER Y WITH DIAERESIS

        );

        $specials = array_merge(array_flip($reverse), $specials);

        foreach ($specials as $old => $new) {
            $str = str_ireplace($old, $new, $str);
        }

        return $str;
    }

}
