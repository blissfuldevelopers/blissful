<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Mixpanel;
class Review extends Model
{
    use RecordsActivity;


    // Validation rules for the ratings
    public function getCreateRules()
    {
        return array(
            'comment'=>'required|min:10',
            'rating'=>'required|integer|between:1,5'
        );
    }
    // Relationships
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function profile()
    {
        return $this->belongsTo('App\Profile','user_id','profile_id');
    }
    // Scopes
    public function scopeApproved($query)
    {
        return $query->where('approved', true);
    }
    public function scopeSpam($query)
    {
        return $query->where('spam', true);
    }
    public function scopeNotSpam($query)
    {
        return $query->where('spam', false);
    }
    public function scopeNotPoint($query)
    {
        return $query->where('point', false);
    }
    // Attribute presenters
    public function getTimeagoAttribute()
    {
        $date = \Carbon\Carbon::createFromTimeStamp(strtotime($this->created_at))->diffForHumans();
        return $date;
    }
    // this function takes in profile ID, comment and the rating and attaches the review to the profile by its ID, then the average rating for the vendor is recalculated

    public function storeReviewForVendor($slug, $comment, $rating)
    {
        $user = User::whereSlug($slug)->first();

        $profile = $user->Profile ?: new Profile;
        $this->user_id = Auth::user()->id;
        $this->comment = $comment;
        $this->rating = $rating;
        $profile->reviews()->save($this);

        //recalculate ratings
        $profile->recalculateRating($rating);
    }
    
}
