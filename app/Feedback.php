<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedback';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    protected $fillable = ['comment', 'rating'];

    public static $rules = [
        'comment' => 'required',
        'g-recaptcha-response' => 'required',
    ];

    public static $messages = [
        'comment.required' => 'comment is required',
        'g-recaptcha-response.required' => 'Captcha is required',
    ];
}
