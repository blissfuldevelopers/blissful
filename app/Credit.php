<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Credit extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'credits';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['credit_bundle_id', 'expiry_date', 'user_id', 'shop_order_id', 'quantity'];

    public static $successfully_credited_message = 'Your blissful.co.ke account has been credited {total_credits} credits';
    public static $failed_credited_message = 'We had trouble assigning you your credits, please contact our customer care';

    /**
     * @param     $product
     * @param     $shop_order
     * @param int $quantity
     *
     * @return bool|static
     */
    public static function addProductCreditsToUser($product, $shop_order, $quantity = 1)
    {
        if(!$product->credit_bundle) {
            return false;
        }

        $attributes = [
            'credit_bundle_id' => $product->credit_bundle->id,
            'expiry_date'      => date_add(Carbon::now(), date_interval_create_from_date_string("365 days")),
            'user_id'          => $shop_order->user_id,
            'shop_order_id'    => $shop_order->id,
            'quantity'         => $quantity,
        ];

        $credit = static::updateOrCreate($attributes, $attributes);

        //rank user
        $ranking = new Ranking;
        $ranking->storeRankingForVendor($shop_order->user_id, 'credit purchase', 2);

        //credit the user
        $total_credits = ($product->credit_bundle->credits * $quantity);

        return CreditsDebitCredit::creditUser($shop_order->user_id, $total_credits, $credit->id, 'credits');

    }


    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function credit_bundle()
    {
        return $this->belongsTo('App\CreditBundle', 'credit');
    }

    public function credits_debit_credits()
    {
        return $this->hasOne('App\CreditsDebitCredit');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\ShopOrder');
    }
}