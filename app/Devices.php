<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Devices extends Model
{	
	 protected $table = 'devices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['device_id','user_id','device_type'];
    
    public function user(){

        return $this->belongsTo('App\User');

    }
}
