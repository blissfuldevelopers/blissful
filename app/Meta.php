<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    protected $table = 'meta_table';

    protected $fillable = [ 'object_type', 'object_id', 'meta_key', 'meta_value'];

    public function metable()
    {
        return $this->morphTo();
    }
}
