<?php

namespace App\DataTables;

use App\Models\CronSchedule;
use Form;
use Yajra\Datatables\Services\DataTable;

class CronScheduleDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('actions', function ($data) {
                            return '
                            ' . Form::open(['route' => ['cron_schedules.destroy', $data->id], 'method' => 'delete']) . '
                            <div class=\'btn-group\'>
                                <a href="' . route('cron_schedules.show', [$data->id]) . '" class=\'btn btn-default btn-xs\'><i class="glyphicon glyphicon-eye-open"></i></a>
                                <a href="' . route('cron_schedules.edit', [$data->id]) . '" class=\'btn btn-default btn-xs\'><i class="glyphicon glyphicon-edit"></i></a>
                                ' . Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-xs',
                                'onclick' => "return confirm('Are you sure?')"
                            ]) . '
                            </div>
                            ' . Form::close() . '
                            ';
                        })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $cronSchedules = CronSchedule::query();

        return $this->applyScopes($cronSchedules);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns(array_merge(
                $this->getColumns(),
                [
                    'actions' => [
                        'orderable' => false,
                        'searchable' => false,
                        'printable' => false,
                        'exportable' => false
                    ]
                ]
            ))
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => true,
                'buttons' => [
//                    'csv',
//                    'excel',
//                    'pdf',
//                    'print',
                    'reset',
                    'reload'
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'cron_task_id' => ['name' => 'cron_task_id', 'data' => 'cron_task_id'],
            'repeat_start' => ['name' => 'repeat_start', 'data' => 'repeat_start'],
            'repeat_stop' => ['name' => 'repeat_stop', 'data' => 'repeat_stop'],
            'cron_min' => ['name' => 'cron_min', 'data' => 'cron_min'],
            'cron_hour' => ['name' => 'cron_hour', 'data' => 'cron_hour'],
            'cron_day_of_month' => ['name' => 'cron_day_of_month', 'data' => 'cron_day_of_month'],
            'cron_month' => ['name' => 'cron_month', 'data' => 'cron_month'],
            'cron_day_of_week' => ['name' => 'cron_day_of_week', 'data' => 'cron_day_of_week'],
            'deleted_at' => ['name' => 'deleted_at', 'data' => 'deleted_at'],
            'created_at' => ['name' => 'created_at', 'data' => 'created_at'],
            'updated_at' => ['name' => 'updated_at', 'data' => 'updated_at'],
            'created_by' => ['name' => 'created_by', 'data' => 'created_by']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'cronSchedules';
    }
}
