<?php

namespace App\DataTables;

use App\Models\CronTaskToUser;
use Form;
use Yajra\Datatables\Services\DataTable;

class ReminderDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('actions', function ($data) {
                return '
                            ' . Form::open([ 'route' => [ 'reminders.destroy', $data->id ], 'method' => 'delete' ]) . '
                            <div class=\'btn-group\'>
                                <a href="' . route('reminders.show', [ $data->id ]) . '" class=\'btn btn-default btn-xs\'><i class="glyphicon glyphicon-eye-open"></i></a>
                                <a href="' . route('reminders.edit', [ $data->id ]) . '" class=\'btn btn-default btn-xs\'><i class="glyphicon glyphicon-edit"></i></a>
                                ' . Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                    'type'    => 'submit',
                    'class'   => 'btn btn-danger btn-xs',
                    'onclick' => "return confirm('Are you sure?')",
                ]) . '
                            </div>
                            ' . Form::close() . '
                            ';
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $reminders = CronTaskToUser::fetchReminders();

        return $this->applyScopes($reminders);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns(array_merge(
                                  $this->getColumns(),
                                  [
                                      'actions' => [
                                          'orderable'  => false,
                                          'searchable' => false,
                                          'printable'  => false,
                                          'exportable' => false,
                                      ],
                                  ]
                              ))
                    ->parameters([
                                     'dom'     => 'Bfrtip',
                                     'scrollX' => true,
                                     'buttons' => [
                                         'csv',
                                         'excel',
                                         'pdf',
                                         'print',
                                         'reset',
                                         'reload',
                                     ],
                                 ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'reminder'      => [ 'name' => 'reminder', 'data' => 'reminder' ],
            'start'         => [ 'name' => 'start', 'data' => 'repeat_start' ],
            'stop'          => [ 'name' => 'stop', 'data' => 'repeat_stop' ],
            'cron_schedule' => [ 'name' => 'schedule', 'data' => 'cron_schedule' ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'reminders';
    }
}
