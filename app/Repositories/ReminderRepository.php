<?php

namespace App\Repositories;

use App\Models\CronTaskToUser;
use App\libraries\blissful\blissreminder;
use InfyOm\Generator\Common\BaseRepository;

class ReminderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CronTaskToUser::class;
    }

    public function reminder()
    {
        return CronTaskToUser::fetchReminders();
    }

    public function create(array $attributes)
    {
        return BlissReminder::create($attributes);
    }
}
