<?php

namespace App\Repositories;

use App\Models\Event;
use App\Repositories\BlissBaseRepository;

class EventRepository extends BlissBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        "title",
        "start",
        "end",
    ];

    protected $default_columns = [
        'id',
        'title',
        'start',
        'end',
        'fullday',
        'desc',
        'created_at',
        'deleted_at',
    ];

    protected $default_with = [
        'jobs',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Event::class;
    }


}
