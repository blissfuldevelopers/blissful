<?php

namespace App\Repositories;

use App\User;
use App\Repositories\BlissBaseRepository;

class UserRepository extends BlissBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        "first_name",
        "last_name",
        "email",
        "slug",
    ];

    protected $default_columns = [
        'id',
        'first_name',
        'last_name',
        'email',
        'slug',
        'created_at',
        'deleted_at',
    ];
    protected $default_with = [ 'Profile' ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }
}
