<?php

namespace App\Repositories;

use App\Models\CronTask;
use InfyOm\Generator\Common\BaseRepository;

class CronTaskRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'type'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CronTask::class;
    }
}
