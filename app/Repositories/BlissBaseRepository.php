<?php
/**
 * Created by PhpStorm.
 * User: davidhenia
 * Date: 17/04/2016
 * Time: 22:51
 */

namespace App\Repositories;

use InfyOm\Generator\Common\BaseRepository;

abstract class BlissBaseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [ ];
    protected $default_columns = [ '*' ];
    protected $default_with = [ ];

    public function all($columns = [ '*' ])
    {
        $columns = $this->_columns($columns);

        return $this->model
            ->with($this->default_with)
            ->all($columns);
    }

    public function paginate($limit = 10, $columns = [ '*' ], $pageName = 'page', $page = null)
    {
        $columns = $this->_columns($columns);

        return $this->model
            ->with($this->default_with)
            ->paginate($limit, $columns, $pageName, $page);
    }

    public function find($id, $columns = [ '*' ])
    {
        $columns = $this->_columns($columns);

        return $this->model
            ->with($this->default_with)
            ->find($id, $columns);
    }

    private function _columns($columns = [ '*' ])
    {
        return ( is_array($columns) && ( empty( $columns ) || ( count($columns) == 1 && $columns[ 0 ] == '*' ) ) )
            ? $this->default_columns
            : $columns;
    }
}