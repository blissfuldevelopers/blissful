<?php

namespace App\Repositories;

use App\Models\CronMessage;
use App\Repositories\BlissBaseRepository;

class CronMessageRepository extends BlissBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'message',
        'send_date',
        'sent',
    ];

    protected $default_with = [ 'recipients' ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CronMessage::class;
    }

    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }
}
