<?php

namespace App\Repositories;

use App\Models\CronRecipient;
use App\Repositories\BlissBaseRepository;

class CronRecipientRepository extends BlissBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'mobile_number',
        'email',
        'sent',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CronRecipient::class;
    }

    public function create(array $attributes)
    {
        foreach ($attributes as $key => $value) {
            $attributes[ $key ] = ( $value === "<<NOW>>" )
                ? date('Y-m-d H:i:s')
                : ( ( $value === "<<NULL>>" ) ? "NULL" : $value );
        }

        return parent::create($attributes);
    }

    public static function createEntry(array $attributes)
    {
        return self::create($attributes);
    }
}
