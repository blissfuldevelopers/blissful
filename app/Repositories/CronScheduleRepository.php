<?php

namespace App\Repositories;

use App\Models\CronSchedule;
use InfyOm\Generator\Common\BaseRepository;

class CronScheduleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'repeat_start',
        'repeat_stop',
        'cron_min',
        'cron_hour',
        'cron_day_of_month',
        'cron_month',
        'cron_day_of_week'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CronSchedule::class;
    }
}
