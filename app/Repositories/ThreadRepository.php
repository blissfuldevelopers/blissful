<?php

namespace App\Repositories;

use App\Repositories\BlissBaseRepository;
use Cmgmyr\Messenger\Models\Thread;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;


class ThreadRepository extends BlissBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'subject',
    ];
    protected $default_with = [ 'messages', 'participants' ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Thread::class;
    }
}
