<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Activity extends Model
{
    protected static $db_table = 'activities';
    protected $fillable = [ 'subject_id', 'subject_type', 'name', 'user_id' ];


    /**
     *
     */
    public static function allActivitiesDetailed($input)
    {
        $query_ = static::allActivitiesQuery($input)
                        ->select(
                            "subject_id",
                            "subject_name",
                            "subject_type",
                            DB::raw("count(activity_id) as activity_count"),
                            "activity_name",
                            "activity_type",
                            DB::raw("count(distinct user_id) user_count"),
                            "created_at as date"
                        )
                        ->groupBy('subject_id')
                        ->groupBy('subject_type')
                        ->groupBy('activity_name')
                        ->groupBy('activity_type')
                        ->orderBy('activity_count', 'desc')
                        ->orderBy('user_count', 'desc');

        return $query_;
    }

    public static function allActivitiesSummary($input = [ ])
    {

    }

    /**
     * @param array $input
     *
     * @return mixed
     */
    public static function allActivitiesQuery($input = [ ])
    {

        $vendorActivities = static::vendorActivities();
        $categoryActivities = static::categoryActivities();
        $userActivities = static::userActivities();
        $pagesActivities = static::pageActivities();

        $union_query_table = DB::raw("({$vendorActivities->toSql()} union {$categoryActivities->toSql()} union {$userActivities->toSql()} union {$pagesActivities->toSql()}) as t");


        $query_ = DB::table($union_query_table);

        $user_join = false;
        $show_created_at = true;
        $query_
            ->select("activity_type as Type");


        if (isset( $input[ 'from' ] )) {
            $from = ( $input[ 'from' ] != '' ) ? date('Y-m-d', strtotime($input[ 'from' ])) : date('Y-m-d', strtotime('yesterday'));
            $from = $from . ' 00:00:00';
            $query_->where('t.created_at', '>=', $from);
        }

        if (isset( $input[ 'to' ] )) {
            $to = ( $input[ 'to' ] != '' ) ? date('Y-m-d', strtotime($input[ 'to' ])) : date('Y-m-d', strtotime('today'));
            $to = $to . ' 23:59:59';
            $query_->where('t.created_at', '<=', $to);
        }

        if (isset( $input[ 'activity_type' ] )) {
            $activity_type_ = ( isset( $input[ 'activity_type' ] ) ) ? $input[ 'activity_type' ] : null;
            $activity_type_array = ( !is_array($activity_type_) ) ? explode(',', $activity_type_) : $activity_type_;

            $query_->where(function ($query) use ($activity_type_array) {
                foreach ($activity_type_array as $activity_type) {
                    if (in_array(strtolower($activity_type), [ 'user', 'category', 'vendor', 'page' ])) {
                        $query->orWhere('activity_type', '=', Db::raw('"' . strtolower($activity_type) . '"'));
                    }
                }
            });


        }


        if (isset( $input[ 'activity_name' ] )) {
            $query_->where('activity_name', '=', $input[ 'activity_name' ]);
        }

        if (isset( $input[ 'subject_id' ] )) {
            $query_
                ->addSelect("subject_name as subject")
                ->where('subject_id', '=', $input[ 'subject_id' ]);
        }

        if (isset( $input[ 'user_id' ] )) {
            if (!$user_join) {
                $query_ = $user_join = static::user_join($query_, 't.user_id');
                $query_
                    ->addSelect("users.id as user_id")
                    ->addSelect(DB::raw("CONCAT (users.first_name, ' ', users.last_name) as user_name"));
            }
            $query_
                ->where('users.id', '=', $input[ 'user_id' ])
                ->orderBy('user_id');
        };


        $group_by_ = ( isset( $input[ 'group_by' ] ) ) ? $input[ 'group_by' ] : null;
        $group_by_array = ( !is_array($group_by_) ) ? explode(',', $group_by_) : $group_by_;
        foreach ($group_by_array as $group_by) {
            $query_ = static::addGroupBy($query_, $group_by, $user_join);
        }


        $sort_by = ( isset( $input[ 'sort_by' ] ) ) ? $input[ 'sort_by' ] : null;
        $sort_order = ( isset( $input[ 'sort_order' ] ) ) ? $input[ 'sort_order' ] : 'desc';
        switch (strtolower($sort_by)) {

            case 'user_count':
                $query_->orderBy('activity_count', $sort_order);
                break;

            case 'activity':
            case 'activities':
            case 'activity_count':
                $query_->orderBy('activity_count', $sort_order);
                break;

            default:
                $query_
                    ->orderBy('activity_count', $sort_order)
                    ->orderBy('user_count', $sort_order);
                break;
        }

        $query_
            ->addSelect(DB::raw("REPLACE(activity_name, '_',' ') as activity"))
            ->addSelect(DB::raw("count(distinct t.activity_id) as activity_count"))
            ->addSelect(DB::raw("count(distinct t.user_id) user_count"));


        return $query_;
    }

    private static function addGroupBy($query_, $group_by, $user_join = false)
    {
        switch ($group_by) {
            case 'subject':
            case 'subject_id':
            case 'subject_name':
                $query_
                    ->addSelect("subject_name as subject")
                    ->groupBy('subject_id');
                break;
            case 'subject_type':
                $query_
                    ->addSelect("subject_name as subject")
                    ->groupBy('subject_type');
                break;

            case 'activity_name':
            case 'activity':
                $query_->groupBy('activity_name');
                break;

            case 'activity_type':
            case 'type':
                $query_->groupBy('activity_type');
                break;

            case 'user':

                $query_ = $user_join = static::user_join($query_, 't.user_id');
                $query_
                    ->addSelect("users.id as user_id")
                    ->addSelect(DB::raw("CONCAT (users.first_name, ' ', users.last_name) as user_name"))
                    ->orderBy('user_id')
                    ->groupBy('user_id');
                break;


            case 'all':
            default:
                $query_->addSelect("subject_name as subject");
                $query_
                    ->groupBy('subject_id')
                    ->groupBy('subject_type')
                    ->groupBy('activity_name');
                break;
        }

        return $query_;
    }


    /**
     * -- Vendor activities
     *
     * select activities.subject_id, vendors.first_name as subject_name, subject_type, activities.name as
     * activity_name, activities.id as activity_id, activities.user_id as user_id, 'vendors' as activity_type from
     * activities join users as vendors on vendors.id = activities.subject_id join role_user on role_user.user_id =
     * vendors.id join roles on roles.id = role_user.role_id where roles.name = 'vendor' and activities.subject_type
     * like '%Profile%'
     *
     *
     * @return mixed
     */
    public static function vendorActivities()
    {
        $query_ = DB::table(static::$db_table)
                    ->where('activities.subject_type', 'like', DB::raw("'%Profile%'"));

        $query_ = static::vendor_join($query_);

        return $query_->select(
            "activities.subject_id",
            "vendors.first_name as subject_name",
            "activities.subject_type",
            "activities.name as activity_name",
            "activities.id as activity_id",
            "activities.user_id as user_id",
            DB::raw("'vendor' as activity_type"),
            "activities.created_at"
        );
    }

    /**
     * -- Category Activities
     *
     * select activities.subject_id, classifications.name as subject_name, subject_type, activities.name as
     * activity_name, activities.id as activity_id, activities.user_id as user_id, 'categories' as activity_type from
     * activities join classifications on classifications.id = activities.subject_id where activities.subject_type like
     * '%Classification%'
     *
     *
     * @return mixed
     */
    public static function categoryActivities()
    {
        return DB::table(static::$db_table)
                 ->join('classifications', function ($join) {
                     $join->on('classifications.id', '=', 'activities.subject_id');
                 })
                 ->where('activities.subject_type', 'like', DB::raw("'%Classification%'"))
                 ->select(
                     "activities.subject_id",
                     "classifications.name as subject_name",
                     "activities.subject_type",
                     "activities.name as activity_name",
                     "activities.id as activity_id",
                     "activities.user_id as user_id",
                     DB::raw("'category' as activity_type"),
                     "activities.created_at"
                 );
    }

    /**
     * -- Page Activities
     *
     * select activities.subject_id, pages.name as subject_name, subject_type, activities.name as activity_name,
     * activities.id as activity_id, activities.user_id as user_id, 'pages' as activity_type from activities join pages
     * on pages.id = activities.subject_id where activities.subject_type like '%Page%'
     *
     *
     * @return mixed
     */
    public static function pageActivities()
    {
        return DB::table(static::$db_table)
                 ->join('pages', function ($join) {
                     $join->on('pages.id', '=', 'activities.subject_id');
                 })
                 ->where('activities.subject_type', 'like', DB::raw("'%Page%'"))
                 ->select(
                     "activities.subject_id",
                     "pages.name as subject_name",
                     "activities.subject_type",
                     "activities.name as activity_name",
                     "activities.id as activity_id",
                     "activities.user_id as user_id",
                     DB::raw("'page' as activity_type"),
                     "activities.created_at"
                 );
    }


    /**
     * -- user activities
     * select activities.subject_id, CONCAT (users.first_name, ' ', users.last_name) as subject_name, subject_type,
     * activities.name as activity_name, activities.id as activity_id, activities.user_id as user_id, 'users' as
     * activity_type from activities join users on users.id = activities.subject_id join role_user on role_user.user_id
     * = users.id join roles on roles.id = role_user.role_id where
     * (roles.name = 'user' or roles.name = 'administrator')
     * and activities.subject_type like '%User%'
     *
     * @return mixed
     */
    public static function userActivities()
    {
        $guest_user = DB::table(static::$db_table)
                        ->where('activities.subject_type', 'like', DB::raw("'%User%'"))
                        ->where('activities.subject_id', DB::raw(0))
                        ->select(
                            "activities.subject_id",
                            DB::raw("'Guest' as subject_name"),
                            "activities.subject_type",
                            "activities.name as activity_name",
                            "activities.id as activity_id",
                            "activities.user_id as user_id",
                            DB::raw("'user' as activity_type"),
                            "activities.created_at"
                        );

        $query_ = DB::table(static::$db_table)
                    ->where('activities.subject_type', 'like', DB::raw("'%User%'"))
                    ->union($guest_user);

        $query_ = static::user_join($query_);

        return $query_->select(
            "activities.subject_id",
            DB::raw("CONCAT (users.first_name, ' ', users.last_name) as subject_name"),
            "activities.subject_type",
            "activities.name as activity_name",
            "activities.id as activity_id",
            "activities.user_id as user_id",
            DB::raw("'user' as activity_type"),
            "activities.created_at"
        );
    }

    /**
     * @param $query_
     *
     * @return mixed
     */
    private static function user_join($query_, $foreign_key = 'activities.subject_id')
    {
        return $query_
            ->join('users', function ($join) use ($foreign_key) {
                $join->on('users.id', '=', $foreign_key);
            })
            ->join('role_user', function ($join) {
                $join->on('role_user.user_id', '=', 'users.id');
            })
            ->join('roles', function ($join) {
                $join->on('roles.id', '=', 'role_user.role_id');
            })
            ->where(function ($query) {
                $query->where('roles.name', '=', DB::raw("'user'"))
                      ->orWhere('roles.name', '=', DB::raw("'administrator'"));
            });
    }

    /**
     * @param $query_
     *
     * @return mixed
     */
    private static function vendor_join($query_, $foreign_key = 'activities.subject_id')
    {
        return $query_
            ->join('users as vendors', function ($join) use ($foreign_key) {
                $join->on('vendors.id', '=', $foreign_key);
            })
            ->join('role_user', function ($join) {
                $join->on('role_user.user_id', '=', 'vendors.id');
            })
            ->join('roles', function ($join) {
                $join->on('roles.id', '=', 'role_user.role_id');
            })
            ->where('roles.name', DB::raw("'vendor'"));
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function subject()
    {
        return $this->morphTo();
    }
}