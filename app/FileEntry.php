<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileEntry extends Model
{
    protected $table = 'file_entries';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    protected $fillable = ['filename', 'mime', 'original_filename', 'user_id'];

    public function user()
    {
        return $this->belongsToMany('App\User');
    }
}
