<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriptions extends Model
{


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subscriptions';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_name',
        'email',
        'product_id',
        'number',
    ];
    public static $db_attributes = [
        'user_name',
        'email',
        'product_id',
        'number',
    ];
    public static $input_rules = [
        'user_name'    => 'required',
        'email' => 'required',
        'number' => 'required',
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\ShopProduct', 'product_id');
    }
}
