<?php

namespace App;

use Log;
use Illuminate\Database\Eloquent\Model;
use App\Activity;
use App\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Thread;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Illuminate\Support\Facades\Input;
use App\Classes\AfricasTalkingGateway;
use Illuminate\Support\Facades\Config;
use Laracasts\Flash\Flash;
use Mixpanel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Jobs\SendMessages;

class Bid extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bids';



    public static $inputRules = [
        'job_id' => 'required',
    ];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bid_details',
        'job_id',
        'user_id',
        'thread_id',
        'bid_accepted',
        'estimated_price',
    ];
    public static $db_attributes = [
        'bid_details',
        'job_id',
        'user_id',
        'thread_id',
        'bid_accepted',
        'estimated_price',
    ];

    /**
     * @param $attributes
     *
     * @return static
     */
    public static function bid_for_job($attributes)
    {

        
        $bidAttributes = array_merge([
                                         'bid_accepted' => ( isset( $attributes[ 'bid_accepted' ] ) ) ? $attributes[ 'bid_accepted' ] : 0,
                                         'thread_id'    => ( isset( $thread_id ) ) ? $thread_id : 0,
                                     ], (array)$attributes);
        
        $bidAttributes = array_only($bidAttributes, static::$db_attributes);
        
        $bid = static::firstOrCreate($bidAttributes);
        
        if ($bid->wasRecentlyCreated) {
            Activity::create([
                                 'subject_id'   => $bid->id,
                                 'subject_type' => 'App\Bid',
                                 'name'         => 'created_bid',
                                 'user_id'      => $bidAttributes['user_id'],
                             ]);

            
        }
        else {
            Activity::create([
                                 'subject_id'   => $bid->id,
                                 'subject_type' => 'App\Bid',
                                 'name'         => 'updated_bid',
                                 'user_id'      => $bidAttributes['user_id'],
                             ]);

           
        }
        
        if ($bid) {
            $total_credits = $bid->job->credits;

            //only debit if they arent invitees
            if (!$bid->job->is_invited($bidAttributes['user_id'])){
                CreditsDebitCredit::debitUser($bidAttributes[ 'user_id' ], $total_credits, $bid->id, 'bids');
                
            }

            
        }

        return $bid;
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function job()
    {
        return $this->belongsTo('App\Job', 'job_id');
    }

    public function thread()
    {
        return $this->hasOne(Config::get('messenger.thread_model'),'id', 'thread_id');
    }
    public function attachments()
    {
        return $this->belongsToMany('App\FileEntry');
    }
    public function details(){
        return $this->hasMany('App\BreakdownDetails');
    }
}
