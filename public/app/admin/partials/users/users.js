/**
 * Created by CIS1 on 05-02-2015.
 */

var Users = angular.module('Users', []);
modules.push('Users');


Users.config(function ($stateProvider) {

    $stateProvider
            .state('app.users', {
                url: '/users',
                templateUrl: '../app/admin/partials/users/users.html',
                controller: 'UsersCtrl',
                resolve: {
                    Users: function (UsersService) {
                        var obj = {
                            page: 1,
                            limit: 10
                        }
                        return UsersService.getUsers(obj);
                    }
                }
            })
            .state('app.users.add', {
                url: '/add',
                templateUrl: '../app/admin/partials/users/add.html',
                controller: 'addUsersCtrl',
            })
            .state('app.users.edit', {
                url: '/:id',
                templateUrl: '../app/admin/partials/users/edit.html',
                controller: 'editUsersCtrl',
                resolve: {
                    UsersById: function (UsersService, $stateParams) {
                        return UsersService.getUsersById($stateParams.id);
                    }
                }
            })
});


Users.controller('UsersCtrl', function ($scope, Users, UsersService) {
    $scope.users = [];
    $scope.page = 1;
    $scope.limit = 25;
    $scope.totalCount = 0;

    if (Users.data.flag) {
        $scope.users = Users.data.data.list;
        $scope.totalCount = Users.data.data.count
    }

    $scope.getPaginate = function () {
        var obj = {
            page: $scope.currentPage,
            limit: $scope.limit
        }
        UsersService.getUsers(obj).success(function (res) {
            if (res.flag) {
                $scope.users = res.data.list;
                if (res.data.count)
                    $scope.totalCount = res.data.count
            } else {
                $scope.users = [];
                $scope.totalCount = 0;
            }
        })
    }
});

Users.controller('addUsersCtrl', function ($scope, $timeout, UsersService) {
    $scope.user = {};

    $timeout(function () {
        $(".usersAdd").modal('show');
        $('.usersAdd').on('hidden.bs.modal', function () {
            $scope.goTo('app.users');
        })
    }, true);

    $scope.addUsersFn = function () {
        var obj = angular.copy($scope.user);
        obj.user_type = obj.user_type * 1;
        obj.password = '123';
        obj.active = 1;
        if (obj.photo) {
            obj.photo = obj.photo.dataURL;
        }
        console.log(obj);
        UsersService.addUsers(obj).success(function (res) {
            if (res.flag) {
                $scope.users.push(res.data);
                swal({title: "Success", text: res.message, timer: 3000});
                $(".usersAdd").modal('hide');
            } else {
                $scope.setFlash('e', res.message);
            }
        })
    }
});

Users.controller('editUsersCtrl', function ($scope, $timeout, $stateParams, UsersService, UsersById) {
    $scope.user = {};
    if (UsersById.data.flag) {
        $scope.user = UsersById.data.data;
        $scope.user.user_type = $scope.user.user_type.toString();
    }

    $timeout(function () {
        $(".usersEdit").modal('show');
        $('.usersEdit').on('hidden.bs.modal', function () {
            $scope.goTo('app.users');
        })
    }, true);

    $scope.updateUsersFn = function () {
        var obj = angular.copy($scope.user);
        obj.user_type = obj.user_type * 1;
        if (obj.photo) {
            obj.photo = obj.photo.dataURL;
        }
        console.log(obj);
        UsersService.updateUsers(obj, $stateParams.id).success(function (res) {
            if (res.flag) {
                _.each($scope.users, function (ct, index) {
                    if (ct.id == $stateParams.id) {
                        $scope.users[index] = res.data;
                    }
                })
                swal({title: "Success", text: res.message, timer: 3000});
                $(".usersEdit").modal('hide');
            } else {
                $scope.setFlash('e', res.message);
            }
        })
    }
});

Users.service('UsersService', function ($http) {
    return{
        getUsers: function (obj) {
            return $http.post('admin/user/paginate', obj);
        },
        addUsers: function (obj) {
            return $http.post('admin/users', obj);
        },
        getUsersById: function (id) {
            return $http.get('admin/users/' + id);
        },
        updateUsers: function (obj, id) {
            return $http.put('admin/users/' + id, obj);
        },
        deleteUsers: function (id) {
            return $http.delete('admin/users/' + id);
        },
    }
});