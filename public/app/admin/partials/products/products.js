/**
 * Created by CIS1 on 05-02-2015.
 */

var Products = angular.module('Products', []);
modules.push('Products');


Products.config(function ($stateProvider) {

    $stateProvider
            .state('app.products', {
                url: '/products',
                templateUrl: '../app/admin/partials/products/products.html',
                controller: 'ProductsCtrl',
                resolve: {
                    Products: function (ProductsService) {
                        var obj = {
                            page: 1,
                            limit: 10
                        }
                        return ProductsService.getProducts(obj);
                    }
                }
            })
            .state('app.products.add', {
                url: '/add',
                templateUrl: '../app/admin/partials/products/add.html',
                controller: 'addProductsCtrl',
                resolve: {
                    Categories: function (ProductsService) {
                        return ProductsService.getCategories();
                    },
                    Vendors: function (ProductsService) {
                        return ProductsService.getVendors();
                    }
                }
            })
            .state('app.products.edit', {
                url: '/:id',
                templateUrl: '../app/admin/partials/products/edit.html',
                controller: 'editProductsCtrl',
                resolve: {
                    Categories: function (ProductsService) {
                        return ProductsService.getCategories();
                    },
                    ProductsById: function (ProductsService, $stateParams) {
                        return ProductsService.getProductsById($stateParams.id);
                    },
                    Vendors: function (ProductsService) {
                        return ProductsService.getVendors();
                    }
                }
            })
});


Products.controller('ProductsCtrl', function ($scope, ProductsService, Products) {
    $scope.products = [];
    $scope.page = 1;
    $scope.limit = 25;
    $scope.totalCount = 0;
    if (Products.data.flag) {
        $scope.products = Products.data.data.list;
        $scope.totalCount = Products.data.data.count
    }

    $scope.deleteProductsFn = function (id) {
        ProductsService.deleteProducts(id).success(function (res) {
            if (res.flag) {
                _.each($scope.products, function (prd, index) {
                    if (prd && prd.id == id) {
                        $scope.products.splice(index, 1);
                    }
                });
                swal("Deleted!", res.message, "success");
            } else {
                $scope.setFlash('e', res.message);
            }
        })
    }

    $scope.getPaginate = function () {
        var obj = {
            page: $scope.currentPage,
            limit: $scope.limit
        }
        ProductsService.getProducts(obj).success(function (res) {
            if (res.flag) {
                $scope.products = res.data.list;
                if (res.data.count)
                    $scope.totalCount = res.data.count
            } else {
                $scope.products = [];
                $scope.totalCount = 0;
            }
        })
    }
});

Products.controller('addProductsCtrl', function ($scope, $timeout, ProductsService, Categories, Vendors) {
    $scope.product = {};
    $scope.vendors = [];
    $scope.categories = [];

    if (Categories.data.flag) {
        $scope.categories = Categories.data.data;
    }

    if (Vendors.data.flag) {
        $scope.vendors = Vendors.data.data;
    }

    $timeout(function () {
        $(".productAdd").modal('show');
        $('.productAdd').on('hidden.bs.modal', function () {
            $scope.goTo('app.products');
        })
    }, true);

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.getSubCategoryFn = function (category_id) {
        ProductsService.getSubCategories(category_id).success(function (res) {
            if (res.flag) {
                $scope.subcategories = res.data;
            } else {
                $scope.subcategories = [];
            }
        })
    }

    $scope.addProductsFn = function () {
        var obj = angular.copy($scope.product);

        obj.status = 'Inactive';
        if (obj.expiry_date) {
            obj.expiry_date = moment(obj.expiry_date).valueOf();
        }
        if (obj.picture) {
            obj.picture = obj.picture.dataURL;
        }
        if (obj.discount) {
            obj.sale_price = (obj.price * 1) - (((obj.price * 1) * (obj.discount * 1)) / 100);
        }
        ProductsService.addProducts(obj).success(function (res) {
            if (res.flag) {
                $scope.products.push(res.data);
                swal({title: "Success", text: res.message, timer: 3000});
                $(".productAdd").modal('hide');
            } else {
                $scope.setFlash('e', res.message);
            }
        })
    }
});

Products.controller('editProductsCtrl', function ($scope, $timeout, $stateParams, ProductsService, Categories, ProductsById, Vendors) {
    $scope.product = {};

    $scope.vendors = [];
    $scope.categories = [];

    if (Categories.data.flag) {
        $scope.categories = Categories.data.data;
    }

    if (Vendors.data.flag) {
        $scope.vendors = Vendors.data.data;
    }

    $scope.getSubCategoryFn = function (category_id) {
        if (category_id) {
            ProductsService.getSubCategories(category_id).success(function (res) {
                if (res.flag) {
                    $scope.subcategories = res.data;
                } else {
                    $scope.subcategories = []
                }
            })
        } else {
            $scope.subcategories = []
        }
    }

    if (ProductsById.data.flag) {
        $scope.product = ProductsById.data.data;
        if ($scope.product.discount) {
            $scope.product.discount = $scope.product.discount * 1;
        }
        $scope.product.price = $scope.product.price * 1;
        $scope.product.sale_price = $scope.product.sale_price * 1;
        $scope.getSubCategoryFn($scope.product.category_id);
    }

    $timeout(function () {
        $(".productEdit").modal('show');
        $('.productEdit').on('hidden.bs.modal', function () {
            $scope.goTo('app.products');
        })
    }, true);

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.updateProductsFn = function () {
        var obj = angular.copy($scope.product);
        if (obj.picture && obj.picture.dataURL) {
            obj.picture = obj.picture.dataURL;
        } else {
            delete obj.picture;
        }
        if (obj.discount == null) {
            obj.expiry_date = null;
            obj.sale_price = null;
        }
        ProductsService.updateProducts(obj, $stateParams.id).success(function (res) {
            if (res.flag) {
                _.each($scope.products, function (ct, index) {
                    if (ct.id == $stateParams.id) {
                        $scope.products[index] = res.data;
                    }
                })
                swal({title: "Success", text: res.message, timer: 3000});
                $(".productEdit").modal('hide');
            } else {
                $scope.setFlash('e', res.message);
            }
        })
    }
});

Products.service('ProductsService', function ($http) {
    return{
        getCategories: function () {
            return $http.get('admin/category');
        },
        getVendors: function () {
            return $http.get('admin/vendors');
        },
        getSubCategories: function (id) {
            return $http.get('admin/get-sub-category/' + id);
        },
        getProducts: function (obj) {
            return $http.post('admin/product/paginate', obj);
        },
        addProducts: function (obj) {
            return $http.post('admin/products', obj);
        },
        getProductsById: function (id) {
            return $http.get('admin/products/' + id);
        },
        updateProducts: function (obj, id) {
            return $http.put('admin/products/' + id, obj);
        },
        deleteProducts: function (id) {
            return $http.delete('admin/products/' + id);
        },
    }
});