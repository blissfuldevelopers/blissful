/**
 * Created by Ashok Hirpara on 03-12-2015.
 */
var modules = ['ui.router', 'ui.bootstrap', 'Directives', 'Constants', 'imageupload', 'textAngular'];

var App = angular.module('BlisFul', modules)
        .run(
                ['$rootScope', '$state', '$stateParams',
                    function ($rootScope, $state, $stateParams) {
                        $rootScope.$state = $state;
                        $rootScope.$stateParams = $stateParams;
                        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState) {

                            $rootScope.fromState = fromState;

                        });
                    },
                ])

        .factory('httpInterceptor', function ($q, $rootScope, $window, $log) {
            return {
                request: function (config) {
                    return config || $q.when(config);

                },
                response: function (response) {
                    if (response.data.code == 401) {
                        location.reload();
                    }

                    if (response || $q.when(response)) {
                        return response || $q.when(response);

                    }

                },
                responseError: function (response) {

                    return $q.reject(response);
                }
            };
        })

        .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
            $httpProvider.interceptors.push('httpInterceptor');
            $urlRouterProvider
                    .otherwise('/app/dashboard');

            $stateProvider
                    .state('app', {
                        abstract: true,
                        url: '/app',
                        templateUrl: '../app/common/partials/app.html',
                    })
        })


App.controller('AppCtrl', function ($scope, $timeout, $state, $stateParams, AppServices, $rootScope) {

    $scope.setFlash = function (mtype, msg, time) {

        var type;
        switch (mtype) {

            case 's' :
                type = 'success';
                break;
            case 'e' :
                type = 'error';
                break;
            case 'w' :
                type = 'warning';
                break;
            case 'n' :
                type = 'notice';
                break;
        }

        var notification = new NotificationFx({
            message: '<i class="glyphicon"></i><p>' + msg + '</p>',
            layout: 'attached',
            effect: 'bouncyflip',
            type: type,
            ttl: time || 10000
        });
        notification.show();
    }


    $scope.goTo = function (state, params) {

        $timeout(function () {
            if (params) {
                $state.transitionTo(state, angular.extend($stateParams, params));
            } else {
                $state.transitionTo(state, $stateParams);
            }
        }, 600);
    };


    $scope.loggedInUserFn = function () {
        AppServices.loggedInUser().success(function (res) {
            if (res.flag) {
                $rootScope.me = res.data;
                console.log($rootScope.me);
            }
        })
    }
    $scope.loggedInUserFn();

    $scope.doLogoutFn = function () {
        AppServices.doLogout().success(function (res) {
            if (res.flag) {
                location.reload();
            } else {
                $scope.setFlash('e', 'Error while logout, Try again');
            }
        })
    }

    $scope.format = 'dd-MMMM-yyyy';

});


App.factory('AppServices', function ($http) {
    return {
        loggedInUser: function () {
            return $http.get('loggedinuser');
        },
        doLogout: function () {
            return $http.get('logout');
        },
    };
});
