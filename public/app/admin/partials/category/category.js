/**
 * Created by CIS1 on 05-02-2015.
 */

var Category = angular.module('Category', []);
modules.push('Category');


Category.config(function ($stateProvider) {

    $stateProvider
            .state('app.category', {
                url: '/category',
                templateUrl: '../app/admin/partials/category/category.html',
                controller: 'CategoryCtrl',
                resolve: {
                    Categories: function (CategoryService) {
                        return CategoryService.getCategories();
                    }
                }
            })
            .state('app.category.add', {
                url: '/add',
                templateUrl: '../app/admin/partials/category/add.html',
                controller: 'addCategoryCtrl',
            })
            .state('app.category.edit', {
                url: '/:id',
                templateUrl: '../app/admin/partials/category/edit.html',
                controller: 'editCategoryCtrl',
                resolve: {
                    CategoryById: function (CategoryService, $stateParams) {
                        return CategoryService.getCategoryById($stateParams.id);
                    }
                }
            })
});


Category.controller('CategoryCtrl', function ($scope, Categories, CategoryService) {
    $scope.categories = [];
    if (Categories.data.flag) {
        $scope.categories = Categories.data.data;
    }

    $scope.deleteCategoryFn = function (id) {
        CategoryService.deleteCategory(id).success(function (res) {
            if (res.flag) {
                _.each($scope.categories, function (ct, index) {
                    if (ct && ct.id == id) {
                        $scope.categories.splice(index, 1);
                    }
                });
                swal("Deleted!", res.message, "success");
            } else {
                //error
                $scope.setFlash('e',res.message);
                console.log(res.message);
            }
        })
    }
});

Category.controller('addCategoryCtrl', function ($scope, $timeout, CategoryService) {
    $scope.category = {};

    $timeout(function () {
        $(".categoryAdd").modal('show');
        $('.categoryAdd').on('hidden.bs.modal', function () {
            $scope.goTo('app.category');
        })
    }, true);

    $scope.addCategoryFn = function () {
        CategoryService.addCategory($scope.category).success(function (res) {
            if (res.flag) {
                console.log(res.data);
                $scope.categories.push(res.data);
                swal({title: "Success", text: res.message, timer: 3000});
                $(".categoryAdd").modal('hide');
            } else {
                //error
                $scope.setFlash('e',res.message);
            }
        })
    }
});

Category.controller('editCategoryCtrl', function ($scope, $timeout, $stateParams, CategoryService, CategoryById) {
    $scope.category = {};
    if (CategoryById.data.flag) {
        $scope.category = CategoryById.data.data;
    }
    $timeout(function () {
        $(".categoryEdit").modal('show');
        $('.categoryEdit').on('hidden.bs.modal', function () {
            $scope.goTo('app.category');
        })
    }, true);

    $scope.updateCategoryFn = function () {
        CategoryService.updateCategory($scope.category, $stateParams.id).success(function (res) {
            if (res.flag) {
                _.each($scope.categories, function (ct, index) {
                    if (ct.id == $stateParams.id) {
                        $scope.categories[index] = res.data;
                    }
                })
                swal({title: "Success", text: res.message, timer: 3000});
                $(".categoryEdit").modal('hide');
            } else {
                //error
                $scope.setFlash('e',res.message);
            }
        })
    }
});

Category.service('CategoryService', function ($http) {
    return{
        getCategories: function () {
            return $http.get('admin/category');
        },
        addCategory: function (obj) {
            return $http.post('admin/category', obj);
        },
        getCategoryById: function (id) {
            return $http.get('admin/category/' + id);
        },
        updateCategory: function (obj, id) {
            return $http.put('admin/category/' + id, obj);
        },
        deleteCategory: function (id) {
            return $http.delete('admin/category/' + id);
        },
    }
});