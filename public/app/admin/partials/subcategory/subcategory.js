/**
 * Created by CIS1 on 05-02-2015.
 */

var subCategory = angular.module('subCategory', []);
modules.push('subCategory');


subCategory.config(function ($stateProvider) {

    $stateProvider
            .state('app.subcategory', {
                url: '/subcategory',
                templateUrl: '../app/admin/partials/subcategory/subcategory.html',
                controller: 'subCategoryCtrl',
                resolve: {
                    SubCategories: function (SubCategoryService) {
                        return SubCategoryService.getSubCategories();
                    }
                }
            })
            .state('app.subcategory.add', {
                url: '/add',
                templateUrl: '../app/admin/partials/subcategory/add.html',
                controller: 'addSubCategoryCtrl',
                resolve: {
                    Categories: function (SubCategoryService) {
                        return SubCategoryService.getCategories();
                    }
                }
            })
            .state('app.subcategory.edit', {
                url: '/:id',
                templateUrl: '../app/admin/partials/subcategory/edit.html',
                controller: 'editSubCategoryCtrl',
                resolve: {
                    Categories: function (SubCategoryService) {
                        return SubCategoryService.getCategories();
                    },
                    SubCategoryById: function (SubCategoryService, $stateParams) {
                        return SubCategoryService.getSubCategoryById($stateParams.id);
                    }
                }
            })
});


subCategory.controller('subCategoryCtrl', function ($scope, SubCategories, SubCategoryService) {
    $scope.subcategories = [];
    if (SubCategories.data.flag) {
        $scope.subcategories = SubCategories.data.data;
    }

    $scope.deleteSubCategoryFn = function (id) {
        SubCategoryService.deleteSubCategory(id).success(function (res) {
            if (res.flag) {
                _.each($scope.subcategories, function (ct, index) {
                    if (ct && ct.id == id) {
                        $scope.subcategories.splice(index, 1);
                    }
                });
                swal("Deleted!", res.message, "success");
            } else {
                //error
                $(".sweet-alert").modal('hide');
                $scope.setFlash('e', res.message);
            }
        })
    }
});

subCategory.controller('addSubCategoryCtrl', function ($scope, $timeout, SubCategoryService, Categories) {
    $scope.subcategory = {};

    if (Categories.data.flag) {
        $scope.categories = Categories.data.data;
    }

    $timeout(function () {
        $(".subCategoryAdd").modal('show');
        $('.subCategoryAdd').on('hidden.bs.modal', function () {
            $scope.goTo('app.subcategory');
        })
    }, true);

    $scope.addSubCategoryFn = function () {
        SubCategoryService.addSubCategory($scope.subcategory).success(function (res) {
            if (res.flag) {
                console.log(res.data);
                $scope.subcategories.push(res.data);
                swal({title: "Success", text: res.message, timer: 3000});
                $(".subCategoryAdd").modal('hide');
            } else {
                $scope.setFlash('e', res.message);
            }
        })
    }
});

subCategory.controller('editSubCategoryCtrl', function ($scope, $timeout, $stateParams, SubCategoryService, Categories, SubCategoryById) {
    $scope.subcategory = {};

    if (Categories.data.flag) {
        $scope.categories = Categories.data.data;
    }

    if (SubCategoryById.data.flag) {
        $scope.subcategory = SubCategoryById.data.data;
    }

    $timeout(function () {
        $(".subCategoryEdit").modal('show');
        $('.subCategoryEdit').on('hidden.bs.modal', function () {
            $scope.goTo('app.subcategory');
        })
    }, true);

    $scope.updateSubCategoryFn = function () {
        SubCategoryService.updateSubCategory($scope.subcategory, $stateParams.id).success(function (res) {
            if (res.flag) {
                _.each($scope.subcategories, function (ct, index) {
                    if (ct.id == $stateParams.id) {
                        $scope.subcategories[index] = res.data;
                    }
                })
                swal({title: "Success", text: res.message, timer: 3000});
                $(".subCategoryEdit").modal('hide');
            } else {
                $scope.setFlash('e', res.message);
            }
        })
    }
});

subCategory.service('SubCategoryService', function ($http) {
    return{
        getCategories: function () {
            return $http.get('admin/category');
        },
        getSubCategories: function () {
            return $http.get('admin/subcategory');
        },
        addSubCategory: function (obj) {
            return $http.post('admin/subcategory', obj);
        },
        getSubCategoryById: function (id) {
            return $http.get('admin/subcategory/' + id);
        },
        updateSubCategory: function (obj, id) {
            return $http.put('admin/subcategory/' + id, obj);
        },
        deleteSubCategory: function (id) {
            return $http.delete('admin/subcategory/' + id);
        },
    }
});