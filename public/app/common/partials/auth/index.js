var Auth = angular.module('Auth', ['ui.router'])

Auth.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider
            .otherwise('/signin');

    $stateProvider
            .state('signin', {
                url: '/signin',
                templateUrl: '../app/common/partials/auth/login.html',
                controller: 'LoginCtrl'
            })
});

Auth.controller('AppCtrl', function ($scope) {
    $scope.setFlash = function (mtype, msg, time) {

        var type;
        switch (mtype) {

            case 's' :
                type = 'success';
                break;
            case 'e' :
                type = 'error';
                break;
            case 'w' :
                type = 'warning';
                break;
            case 'n' :
                type = 'notice';
                break;
        }

        var notification = new NotificationFx({
            message: '<i class="glyphicon"></i><p>' + msg + '</p>',
            layout: 'attached',
            effect: 'bouncyflip',
            type: type,
            ttl: time || 10000
        });
        notification.show();
    }

})

Auth.controller('LoginCtrl', function ($scope, $timeout, AuthServices) {
    $scope.login = {};
    $scope.doLoginFn = function () {
        console.log($scope.login);
        AuthServices.doLogin($scope.login).success(function (res) {
            if (res.flag) {
                $timeout(function () {
                    window.location.reload();
                }, 200);
            }
            else {
                $scope.setFlash('e', res.message)
            }
        })
    }
})


Auth.factory('AuthServices', function ($http) {
    return{
        doLogin: function (obj) {
            return $http.post('auth/login', obj);
        },
    }
});

