angular.
        module('Constants', [])

        .constant('userType', {
            'USER_TYPE_ADMIN': 1,
            'USER_TYPE_VENDOR': 2,
            'USER_TYPE_MEMBER': 3,
        });
