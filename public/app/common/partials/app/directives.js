/***
 GLobal Directives
 ***/

// Route State Load Spinner(used on page or content load)
angular
        .module('Directives', [])
        .directive('ngConfirmClick', function () {
            return {
                link: function (scope, element, attr) {
                    var msg = attr.ngConfirmClick || "Are you sure want to delete?";
                    var clickAction = attr.confirmedClick;
                    element.bind('click', function (event) {
                        if (window.confirm(msg)) {
                            scope.$apply(clickAction);
                        }
                        else {
                        }
                    });
                }
            }
        })


        // Handle global LINK click
        .directive('a', function () {
            return {
                restrict: 'E',
                link: function (scope, elem, attrs) {
                    if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                        elem.on('click', function (e) {
                            e.preventDefault(); // prevent link click for above criteria
                        });
                    }
                }
            };
        })

        .directive('sidebarOpenButton', function () {
            return {
                link: function (scope, elem, attrs) {
                    elem.on('click', function () {
                        if ($('.sidebar').hasClass('hidden')) {
                            $('.sidebar').removeClass('hidden');
                            $('.content').css({
                                'marginLeft': 250
                            });
                        } else {
                            $('.sidebar').addClass('hidden');
                            $('.content').css({
                                'marginLeft': 0
                            });
                        }
                    });
                }
            };
        })

        .directive('sidebarOpenButtonMobile', function () {
            return {
                link: function (scope, elem, attrs) {
                    elem.on('click', function () {
                        $(".sidebar").toggle(150);
                    });
                }
            };
        });
