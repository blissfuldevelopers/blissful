/**
 * stepsForm.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2014, Codrops
 * http://www.codrops.com
 */
function toggleClassMenu() {

    myMenu.classList.add("start-slide--animatable");

    if (!myMenu.classList.contains("start-slide--visible")) {

        if (sliderElement.classList.contains("bounceInUp")) {
            sliderElement.classList.remove("bounceInUp");
        }

        myMenu.classList.add("start-slide--visible");
    } else {
        myMenu.classList.remove('start-slide--visible');
    }
}

function OnTransitionEnd() {
    myMenu.classList.remove("start-slide--animatable");
}

var myMenu = document.querySelector(".start-slide");
var sliderElement = document.querySelector(".budget-enquiry");
myMenu.addEventListener("transitionend", OnTransitionEnd, false);
var estimate_selection = [];
var execution_progress = 0;
// var budgetBtns = document.querySelector(".budget-buttons button");
// var oppMenu = document.querySelector("#first-click");
// budgetBtns.addEventListener("click", toggleClassMenu, false);
// myMenu.addEventListener("click", toggleClassMenu, false);


;
(function startStep(window) {

    'use strict';
    var executed = false;
    var slideNumber = 0;
    var clicks = 0;
    var transEndEventNames = {
            'WebkitTransition': 'webkitTransitionEnd',
            'MozTransition': 'transitionend',
            'OTransition': 'oTransitionEnd',
            'msTransition': 'MSTransitionEnd',
            'transition': 'transitionend'
        },
        transEndEventName = transEndEventNames[Modernizr.prefixed('transition')],
        support = { transitions: Modernizr.csstransitions };

    function extend(a, b) {
        for (var key in b) {
            if (b.hasOwnProperty(key)) {
                a[key] = b[key];
            }
        }
        return a;
    }

    function stepsForm(el, options) {
        this.el = el;
        this.options = extend({}, this.options);
        extend(this.options, options);
        this._init();
    }


    stepsForm.prototype.options = {
        onSubmit: function() {
            return false;
        }
    };

    stepsForm.prototype._init = function() {
        // current question
        this.current = 0;

        // questions
        this.questions = [].slice.call(this.el.querySelectorAll('ol.questions > li'));
        // total questions
        this.questionsCount = this.questions.length;
        // show first question
        classie.addClass(this.questions[0], 'current');

        // next question control
        this.ctrlNext = this.el.querySelector('button.next');

        // previous question control
        this.ctrlPrev = this.el.querySelector('button.prev');

        // progress bar
        this.progress = this.el.querySelector('div.progress');

        // question number status
        this.questionStatus = this.el.querySelector('span.number');
        // current question placeholder
        this.currentNum = this.questionStatus.querySelector('span.number-current');
        this.currentNum.innerHTML = Number(this.current + 1);
        // total questions placeholder
        this.totalQuestionNum = this.questionStatus.querySelector('span.number-total');
        this.totalQuestionNum.innerHTML = this.questionsCount;

        // error message
        this.error = this.el.querySelector('span.error-message');

        // init events
        this._initEvents();
    };

    stepsForm.prototype._initEvents = function() {
        var self = this,
            // first input
            firstElInput = this.questions[this.current].querySelector('input'),
            // focus
            onFocusStartFn = function() {
                firstElInput.removeEventListener('focus', onFocusStartFn);
                classie.addClass(self.ctrlNext, 'show');
            };

        // show the next question control first time the input gets focused
        // firstElInput.addEventListener( 'focus', onFocusStartFn );

        // show next question
        this.ctrlNext.addEventListener('click', function(ev) {
            ev.preventDefault();
            self._nextQuestion();
        });

        // show previous question
        this.ctrlPrev.addEventListener('click', function(ev) {
            ev.preventDefault();
            self._prevQuestion();
        });

        // pressing enter will jump to next question
        document.addEventListener('keydown', function(ev) {
            var keyCode = ev.keyCode || ev.which;
            // enter
            if (keyCode === 13) {
                ev.preventDefault();
                self._nextQuestion();
            }
        });

        // disable tab
        this.el.addEventListener('keydown', function(ev) {
            var keyCode = ev.keyCode || ev.which;
            // tab
            if (keyCode === 9) {
                ev.preventDefault();
            }
        });
    };

    stepsForm.prototype._nextQuestion = function() {


        // clear any previous error messages
        this._clearError();
        this.questions = [].slice.call(this.el.querySelectorAll('ol.questions > li'));
        this.questionsCount = this.questions.length;
        var totalQuestionsCount = this.questionsCount;

        // current question
        var currentQuestion = this.questions[this.current];

        //check if user details form exists and socials exist
        var infoExists = $('.info-form').length;
        var infoSocial = $(currentQuestion).has('.info-container .info-socials').length;
        var isToggled = $(currentQuestion).hasClass('is-toggled');
        // console.log(infoSocial+'slideNumber is '+slideNumber+'totalQuestionsCount is'+totalQuestionsCount+'infoExists  is'+infoExists)




        //Do not parse since its a social signup
        if (slideNumber > totalQuestionsCount && infoExists == 0 && infoSocial > 0) {
            var the_form = $('#theForm');
        } else if ($(currentQuestion).hasClass('planning-panel')) {
            var the_form = $('#theForm');
        } else {

            if (this.questions[this.current].querySelector('input')) {
                var input = this.questions[this.current].querySelector('input').getAttribute('data-parsley-group');

            } else {
                var input = this.questions[this.current].querySelector('textarea').getAttribute('data-parsley-group');

            }
            var the_form = $('#theForm');
            the_form.parsley({
                errorsWrapper: '<div style="text-align:center;"></div>',
                errorTemplate: '<span style="color:#F75B4A; font-size:0.85rem;"></span>'
            }).validate(input);
            $('textarea').removeClass('fullscreen-text', 2000, "easeInBack");
            $('.fullscreen-text-controlPanel').hide();
            if (!the_form.parsley().isValid(input)) {
                return false;

            }
        }

        //show budget suggestion if its the first slide
        // console.log('is-toggled '+isToggled+ 'slideNumber '+slideNumber)
        if (!isToggled && slideNumber < 1) {
            //show budget dialog

            if (clicks > 0) {

                var me = $('.budget-enquiry');

                // restart animation
                if (me.hasClass('bounceInUp')) {

                    me.removeClass('bounceInUp').animate({ 'nothing': null }, 1,
                        function() {
                            $(this).addClass('bounceInUp');
                        });

                } else {
                    $('.budget-enquiry').addClass('bounceInUp');
                }



            } else {

                document.body.scrollTop = document.documentElement.scrollTop = 0;

                clicks++
                toggleClassMenu();
            }

            return false;
        }
        //remove overlay if the user has selected 
        if (isToggled && slideNumber < 1) {

            toggleClassMenu();
        }

        //check if the current tab requires a budget calculation

        if ($('.questions > li.budget-result').length > 0 && $(currentQuestion).hasClass('child-class')) {

            var str = $("#pac-input").attr('data-city');
            var ret = str.split(" ");
            var str1 = ret[0];

            var classification_id = $(currentQuestion).find('.sub-classifs').data("classificationId"),
                classifications_parent = $(currentQuestion).find('.level-one-classifs select').val(),
                classifications_children = [],
                location = str1,
                event_date = $("input[name=event_date]").val(),
                guest_number = $("select[name=guest_number-" + classification_id + "]").val();

            //add each select from slide
            $(currentQuestion).find('.sub-sub-classifs select').each(function() {

                Array.prototype.push.apply(classifications_children, $(this).val());

            });

            //avoid executing classification twice
            var execution_status = $.inArray(classification_id, estimate_selection);


            if (execution_status == -1 && execution_progress == 0) {

                //add current id to array
                estimate_selection.push(classification_id);

                execution_progress = 1;
                var loader = ' <span class=\"loader loading-msg\"> Calculating budget.</span> <div class=\"loader loading loading--double\"></div>';

                $('.questions > li.budget-result').append(loader);


                //set variables
                var budget_params = {
                    classification_id: classification_id,
                    location: location,
                    event_date: event_date,
                    guest_number: guest_number,
                    classifications_parent: classifications_parent,
                    classifications_children: classifications_children
                };

                $.ajax({
                    type: 'get',
                    url: '/budget/create',
                    data: budget_params,
                    success: function(data) {
                        $('.loader').remove();

                        var val = $(data).find('input').val();
                        var inserted = $(data).insertBefore('.budget-total');

                        //if suggetsed budget is 0, make user set budget
                        if (val < 1) {
                            $(inserted).find('button').click();
                        }
                        findTotal();

                    }
                });

                execution_progress = 0;

            }




        }

        // check if form is filled
        if (this.current === this.questionsCount - 1) {
            this.isFilled = true;
        }

        // increment current question iterator
        ++this.current;

        // update progress bar
        this._progress();

        if (!this.isFilled) {
            // change the current question number/status
            this._updateQuestionNumber();

            // add class "show-next" to form element (start animations)
            classie.addClass(this.el, 'show-next');

            // remove class "current" from current question and add it to the next one
            // current question
            var nextQuestion = this.questions[this.current];

            classie.removeClass(currentQuestion, 'current');
            classie.addClass(nextQuestion, 'current');

            // hide close button for textarea if present
            $('.fullscreen-text-controlPanel').hide();
            ++slideNumber;
            ga('send', 'pageview', '/jobs/create/slide/' + slideNumber);
        }

        // after animation ends, remove class "show-next" from form element and change current question placeholder
        var self = this,
            onEndTransitionFn = function(ev) {
                if (support.transitions) {
                    this.removeEventListener(transEndEventName, onEndTransitionFn);
                }
                if (self.isFilled && !executed) {


                    executed = true;
                    //hide control bar

                    $('.controls').attr('style', 'display:none !important');
                    var theForm = document.getElementById('theForm');
                    var path = theForm.getAttribute('action');
                    // hide form
                    classie.addClass(theForm.querySelector('.simform-inner'), 'hide');
                    var messageEl = theForm.querySelector('.final-message');
                    messageEl.innerHTML = '<span class=\"loading-msg\"> Posting Job.</span> <div class=\"loading loading--double\"></div>';
                    classie.addClass(messageEl, 'show');
                    var values = $(theForm).serialize();
                    //add estimate budget if not there
                    var pac_input = $('#pac-input');
                    var params = $.param($('.estimate-details').find(':input'));
                    var city = { city_name: pac_input.attr('data-city'), lat: pac_input.attr('data-lat'), lng: pac_input.attr('data-lng') };
                    var address = $.param(city);
                    var data = values + '&' + params + '&' + address;
                    console.log(data)
                    $.ajax({
                        type: 'post',
                        url: path,
                        data: data,
                        dataType: 'html',
                        success: function(data) {
                            var src = window.location.pathname;
                            if (/dashboard/i.test(src)) {
                                $('.job-container').empty().load('/jobs/get/job_posted');
                                ga('send', 'pageview', path);
                            }
                            //redirect to dashboard on success
                            else {

                                $('body').html(data);
                            }

                        },
                        error: function(jqXHR, textStatus, errorThrown, data) {
                            console.log('error')
                                //catch unauthorized error
                            if (jqXHR.status == 401) {
                                $('.job-container').empty().load('/jobs/get/job_posted');
                            } else {
                                $('html').empty().load('/jobs/create');
                            }

                        }
                    });

                } else {
                    classie.removeClass(self.el, 'show-next');
                    self.currentNum.innerHTML = self.nextQuestionNum.innerHTML;
                    self.questionStatus.removeChild(self.nextQuestionNum);
                    // force the focus on the next input
                    // nextQuestion.querySelector( 'input' ).focus();
                }
            };

        if (support.transitions) {
            this.progress.addEventListener(transEndEventName, onEndTransitionFn);
        } else {
            onEndTransitionFn();
        }
    }

    stepsForm.prototype._prevQuestion = function() {
            // clear any previous error messages
            this._clearError();
            this.questions = [].slice.call(this.el.querySelectorAll('ol.questions > li'));
            this.questionsCount = this.questions.length;


            // current question
            var currentQuestion = this.questions[this.current];

            // decrement current question iterator
            --this.current;
            // current question
            var nextQuestion = this.questions[this.current];
            $('textarea').removeClass('fullscreen-text', 2000, "easeInBack");
            $('.fullscreen-text-controlPanel').hide();
            if (nextQuestion) {
                // update progress bar
                this._progress();

                // change the current question number/status
                this._updateQuestionNumber();

                // add class "show-next" to form element (start animations)
                classie.addClass(this.el, 'show-next');

                // remove class "current" from current question and add it to the next one


                classie.removeClass(currentQuestion, 'current');
                classie.addClass(nextQuestion, 'current');

                // after animation ends, remove class "show-next" from form element and change current question placeholder
                var self = this,
                    onEndTransitionFn = function(ev) {
                        if (support.transitions) {
                            this.removeEventListener(transEndEventName, onEndTransitionFn);
                        }

                        classie.removeClass(self.el, 'show-next');
                        self.currentNum.innerHTML = self.nextQuestionNum.innerHTML;
                        self.questionStatus.removeChild(self.nextQuestionNum);
                        // force the focus on the next input
                        // nextQuestion.querySelector( 'input' ).focus();

                        //ensure control bar is visible

                        $('.controls').show(1000, "swing");

                    };

                if (support.transitions) {
                    this.progress.addEventListener(transEndEventName, onEndTransitionFn);
                } else {
                    onEndTransitionFn();
                }

            }


        }
        // updates the progress bar by setting its width
    stepsForm.prototype._progress = function() {
        this.progress.style.width = this.current * (100 / this.questionsCount) + '%';
    }

    // changes the current question number
    stepsForm.prototype._updateQuestionNumber = function() {
        // first, create next question number placeholder
        this.nextQuestionNum = document.createElement('span');
        this.nextQuestionNum.className = 'number-next';
        this.nextQuestionNum.innerHTML = Number(this.current + 1);
        // insert it in the DOM
        this.questionStatus.appendChild(this.nextQuestionNum);
    }

    // submits the form
    stepsForm.prototype._submit = function() {
        this.options.onSubmit(this.el);
    }

    // TODO (next version..)
    // the validation function
    stepsForm.prototype._validade = function() {
        // current question´s input
        var input = this.questions[this.current].querySelector('input').value;
        if (input === '') {
            this._showError('EMPTYSTR');
            return false;
        }

        return true;
    }

    // TODO (next version..)
    stepsForm.prototype._showError = function(err) {
        var message = '';
        switch (err) {
            case 'EMPTYSTR':
                message = 'Please fill the field before continuing';
                break;
            case 'INVALIDEMAIL':
                message = 'Please fill a valid email address';
                break;
                // ...
        };
        this.error.innerHTML = message;
        classie.addClass(this.error, 'show');
    }

    // clears/hides the current error message
    stepsForm.prototype._clearError = function() {
        classie.removeClass(this.error, 'show');
    }

    // add to global namespace
    window.stepsForm = stepsForm;

})(window);
