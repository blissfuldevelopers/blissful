// Variable to store your files
var files;

// Add events
$('input[type=file]').on('change', prepareUpload);

// Grab the files and set them to our variable
function prepareUpload(event) {
    files = event.target.files;
}

var ajaxify_bliss = function(options) {
    var defaults = {
            container: $('#ajax-container'),
            main_container: $('main').first(),
            rx: /INPUT|SELECT|TEXTAREA/i,
            container_message: $('#ajax-container-message'),
            back_action_container: $('#ajax-container-back-action'),
            formMethod: 'POST',
            bindKeyboardBack: false,
            allowHistoryPushState: true,
            loadOnAction: 'window.load'
        },
        settings = $.extend({}, defaults, options),
        _container = settings.container,
        _main_container = settings.main_container,
        job_container_message = settings.container_message,
        back_action_container = settings.back_action_container,
        default_link = _container.data('url'),
        requested_link = _container.data('requested-url'),
        previous_links = [],
        rx = settings.rx,

        bindPaginationAction = function() {
            var page_form = $('form');
            _container.off('click', 'a');

            _container.on('click', 'a', function(e) {
                e.preventDefault();
                if (!$(this).hasClass('non-ajax')) {
                    var $this = $(this);

                    if ($this.hasClass("inner-page-link")) {


                        var $target_id = $this.attr('href'),
                            loadurl = $this.data('href'),
                            targ = $($target_id);

                        if (targ.hasClass("active")) {
                            targ.removeClass("active");
                        } else {
                            _container.find('.active').removeClass("active");
                            targ.addClass("active");
                            reload_page(loadurl, 'GET', targ);
                        }

                    } else {
                        reload_page($this.attr('href'), 'GET');
                    }

                    return false;
                } else {
                    if ($(this).attr('target') == '_blank') {
                        window.open($(this).attr('href'), '_blank');
                    } else {
                        window.location = $(this).attr('href');
                    }

                }
            });

            page_form.submit(function(e) {

                if (!$(this).hasClass('non-ajax')) {
                    e.preventDefault();
                    var form_url = $(this).attr('action'),
                        form_data = $(this).serialize(),
                        url = form_url + '?' + form_data;
                    var form_method = $(this).attr('method');
                    if (typeof form_method == typeof undefined || form_method == false) {
                        form_method = settings.formMethod;
                    }
                    if ($(this).hasClass('with-img')) {
                        form_with_img(page_form);
                    } else {
                        reload_page(url, form_method);
                    }

                }

            });

        },

        add_overlay = function() {

            _main_container.css("position", "relative");
            var overlay = $('<div id="overlay"></div>').css({
                    position: "fixed",
                    width: "100%",
                    height: "100%",
                    top: "50%",
                    left: "50%",
                    "-webkit-transform": "translate(-50%,-50%)",
                    "-moz-transform": "translate(-50%,-50%)",
                    "-ms-transform": "translate(-50%,-50%)",
                    "-o-transform": "translate(-50%,-50%)",
                    "transform": "translate(-50%,-50%)",
                    "z-index": "5999",
                    "background-color": "rgba(255,255,255,0.1)" /*dim the background*/
                }),
                modal = $('<div id="this-modal"><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>Loading...</div>').css({
                    position: "fixed",
                    width: "200px",
                    height: "200px",
                    top: "50%",
                    left: "50%",
                    "-webkit-transform": "translate(-50%,-50%)",
                    "-moz-transform": "translate(-50%,-50%)",
                    "-ms-transform": "translate(-50%,-50%)",
                    "-o-transform": "translate(-50%,-50%)",
                    "transform": "translate(-50%,-50%)",
                    "color": "#e9048b",
                    "text-align": "center",
                    "z-index": "5999"
                });

            modal.appendTo(overlay.appendTo(_main_container));

        },


        remove_overlay = function() {
            $('#overlay').hide().remove();


        },

        reload_page = function(link, get_or_post, target_container, message_container) {

            if (typeof link == 'undefined') {
                console.log('Cannot open ' + link + ' link');
                return false;
            }

            if (link.match("^#")) {
                return false;
            }


            add_overlay();
            var current_url = link;
            previous_links.push(link);

            var ajax_method = (typeof get_or_post !== 'undefined') ? get_or_post : 'GET',
                _target_container = (typeof target_container !== 'undefined') ? target_container : _container,
                _message_container = (typeof message_container !== 'undefined') ? message_container : job_container_message;

            if (ajax_method == 'GET') {
                // Detect if pushState is available
                if (history.pushState && settings.allowHistoryPushState) {
                    window.history.pushState(null, null, link);
                }
            }

            _message_container.html('');
            $.ajax({
                url: link,
                method: ajax_method
            }).done(function(data) {
                _message_container.html('');

                _target_container.html(data);

                bindPaginationAction();
                remove_overlay();
            }).error(function() {
                _message_container.html('<span class="red">Error loading request. Please try again</span>');
                remove_overlay();
            });
        },

        form_with_img = function(page_form, target_container, message_container) {

            console.log('with-img-ajax')
            var _target_container = (typeof target_container !== 'undefined') ? target_container : _container,
                _message_container = (typeof message_container !== 'undefined') ? message_container : job_container_message;


            add_overlay();

            var formData = new FormData(page_form[0]);
            _message_container.html('');
            $.ajax({
                type: page_form.attr('method'),
                url: page_form.attr('action'),
                data: formData,
                async: false,
                cache: false,
                // THIS MUST BE DONE FOR FILE UPLOADING
                contentType: false,
                processData: false,
                // ... Other options like success and etc
                success: function(data) {
                    _message_container.html('');
                    _target_container.html(data);
                    remove_overlay();

                },
                error: function(data) {
                    _message_container.html('<span class="red">Error loading request. Please try again</span>');
                    remove_overlay();
                }
            });

        },

        go_back = function() {

            if (history.pushState && settings.allowHistoryPushState) {
                window.history.back();
            }
            console.log(previous_links);
            var prev_links_length = previous_links.length;
            if (prev_links_length) {
                var previous_link = (prev_links_length > 1) ? previous_links[prev_links_length - 2] : previous_links[prev_links_length - 1];

                previous_links.splice(-1, 1);

                reload_page(previous_link, 'GET');
                previous_links.splice(-1, 1);

            } else {
                reload_page(default_link, 'GET');
            }

        },
        reload_current_page = function() {

            var prev_links_length = previous_links.length;
            if (prev_links_length) {
                var current_link = previous_links[prev_links_length - 1];

                reload_page(current_link, 'GET');
                previous_links.splice(-1, 1);
            } else {
                reload_page(default_link, 'GET');
            }
        },

        _ajax_init = function() {
            default_link = _container.data('url');

            var url_ = (typeof requested_link !== 'undefined' && requested_link !== '') ? requested_link : default_link;

            url_ = (url_ == '') ? 'all' : url_;

            reload_page(url_, 'GET');

            back_action_container.on('click', 'a', function(e) {
                e.preventDefault();
                var action = $(this).attr('href');

                switch (action) {
                    case '#reload':
                        return reload_current_page();
                        break;
                    case '#back':
                        return go_back();
                        break;

                    case '#home':
                    default:
                        return reload_page(default_link, 'GET');
                        break;
                }

            });
            // $(window).on("navigate", function (event, data) {
            //       var direction = data.state.direction;
            //       if (direction == 'back') {
            //          e.preventDefault();
            //                 go_back();
            //       }
            // });
            if (settings.bindKeyboardBack) {
                /*
                 * this swallows backspace keys on any non-input element.
                 * stops backspace -> back
                 */

                // $(document).bind("keydown keypress", function (e) {
                //     if (e.which == 8) { // 8 == backspace
                //         if (!rx.test(e.target.tagName) || e.target.disabled || e.target.readOnly) {
                //             e.preventDefault();
                //             go_back();
                //         }
                //     }
                // });

            }
        };

    return _ajax_init();
}
