  @extends('page.zurb-layout')
  @section('title')
  Welcome
  @endsection
  @section('styles')
  <link rel="stylesheet" href="css/slick.css" />
  <link rel="stylesheet" href="css/slick-theme.css" />

  @endsection
  @section('content')
  <!-- START OF MAIN SECTION -->
  <main role="main" id="homepg">
  <!-- Homepage Slideshow -->
  <section class="slideshow parallax-window" data-parallax="scroll" data-image-src="img/slideshow/ring.jpg">
      
        <!-- <img src="img/slideshow/ring.jpg"> -->
        <div class="slide">
          <h2>Kenya’s Largest Wedding Directory</h2>
          <p>Plan Your Wedding Simply. From wedding gowns to wedding venues & photographers.</p>
        </div>
        <div class="slide">
          <h2>Another explanation</h2>
          <p>For Users</p>
        </div>
  </section>
  <!-- First batch of categories slides -->
  <!-- <div id="ball" class="hide-for-small-only"></div> -->
  <section class="cat1">
    <div id="home_row" class="row">
      <div class="plac_cards small-12 columns">
        <h2>Explore Vendors</h2>
        <h3>See what services are available around you.</h3>
        @if ($count==1)
        @foreach($collection1 as $top)
         <div class="small-12 large-3 columns">
            <div class="ripple placard">
              <div class="image ">
                <img src="img/hcat/1.jpg">
              </div>
              <div class="content">
                <a href="vendor/list/{{$top->id}}">{{$top->name}} <a href="#" class="link hide-for-small-only">&#10095;</a></a> 
                <p>Get the best decor in Kenya here.</p>
           
              </div>
            </div>
          </div>
          @endforeach
        @else
        @foreach($collection1 as $top)
         <div class="small-12 large-3 columns">
            <div class="ripple placard">
              <div class="image ">
                <img src="img/hcat/1.jpg">
              </div>
              <div class="content">
                <a href="vendor/list/{{$top->id}}">{{$top->name}} <a href="#" class="link hide-for-small-only">&#10095;</a></a> 
                <p>Get the best decor in Kenya here.</p>
           
              </div>
            </div>
          </div>
          @endforeach
        <div class="small-12 large-3 columns">
            <div class="placard">
              <div class="image ">
                <img src="{{$src}}">
              </div>
              <div class="content">
                <a href="#">AD <a href="#" class="link hide-for-small-only">&#10095;</a></a> 
                <p>This is an AD.</p>
              </div>
            </div>
          </div>
        @endif
         
      </div>
    </div>
       
  </section>
  <!-- Categories breaker - Ad feature -->
  <div class="lazyload">
    <!-- <section class="cbreaker ripple" >
      <img src="img/cbreakers/1.jpg">
      <div class="row">
        <div class="small-12 columns">
          <h3>Exotic holiday getaways!</h3>
        </div>
      </div>
    </section> -->
  </div>
  <!-- Last batch of categories slides -->
  <section class="cat2">
    <div id="home_row" class="row">
      <div class="small-12 columns">
        @if ($count==1)
          @foreach($collection2 as $bottom)
          <div class="small-12 large-3 columns">
            <div class="ripple placard">
              <div class="image ">
                <img src="img/hcat/1.jpg">
              </div>
              <div class="content">
                <a href="vendor/list/{{$bottom->id}}">{{$bottom->name}} <a href="#" class="link hide-for-small-only">&#10095;</a></a> 
                <p>Get the best decor in Kenya here.</p>
              </div>
            </div>
          </div>
          @endforeach
        @else
          @foreach($collection2 as $bottom)
          <div class="small-12 large-3 columns">
            <div class="ripple placard">
              <div class="image ">
                <img src="img/hcat/1.jpg">
              </div>
              <div class="content">
                <a href="vendor/list/{{$bottom->id}}">{{$bottom->name}} <a href="#" class="link hide-for-small-only">&#10095;</a></a> 
                <p>Get the best decor in Kenya here.</p>
              </div>
            </div>
          </div>
          @endforeach
          <div class="small-12 large-3 columns">
            <div class="placard">
              <div class="image ">
                <img src="{{$src}}">
              </div>
              <div class="content">
                <a href="#">AD <a href="#" class="link hide-for-small-only">&#10095;</a></a> 
                <p>This is an AD.</p>
              </div>
            </div>
          </div>
        @endif
    </div>
  </section>
  </main>
  <!-- END OF MAIN SECTION -->
  @endsection
  
  @section('scripts')
   <script src="js/parallax.min.js"></script>
   <script src="js/jquery.lazyload-any.min.js"></script>
   <script>
      function load(section)
        {
          section.fadeOut(0, function() {
            section.fadeIn(1000);
          });
        }
        $('.lazyload').lazyload({load: load});
    </script>
  @endsection