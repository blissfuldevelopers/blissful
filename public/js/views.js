	$(window).load(function() {

	    if (window.location.pathname.split('/')[1] == 'vendor') {
	        var bStyle = $('.c-header > .row:first > div > .row:first').css('borderLeft');

	        $('.cat-li li .s-wrap').css('borderLeft', bStyle);
	    }

	    $('.loader').remove();

	    $('.ld').removeClass('ld');

	    //show popup on load
	    setTimeout(function() {
	        $('.container-popup').show(600);
	    }, 2500);


	    //close popup fn
	    $('.close-popup').click(function() {
	        $('.container-popup').hide(600);
	    });
	});

	$(document).ready(function() {

	    // initialize foundation logic
	    $(document).foundation();

	    //initiate notifications
	    function notification(type, msg, dely) {
	        // 'warning', 'info', 'success', 'error'
	        Lobibox.notify(type, {
	            size: 'normal', // normal, mini, large
	            icon: false,
	            closable: true,
	            sound: false,
	            position: "top right",
	            delay: dely || 5000,
	            msg: msg
	        });
	    }

	    // get cart
	    function cartCount() {
	        $.ajax({
	            url: '/shop/cart-count',
	            type: 'GET',
	            success: function(data) {
	                console.log(data);
	                if (data > 0) {
	                    $(".showCart").show();
	                }
	                $("#CartCount").text(data);
	            },
	            error: function(e) {}
	        });
	    }

	    cartCount();


	    $(".card-box").click(function() {
	        window.location = $(this).find("a").attr("href");
	        return false;
	    });
	    $(".placard").click(function() {
	        window.location = $(this).find("a").attr("href");
	        return false;
	    });

	    objectFit.polyfill({
	        selector: 'img', // this can be any CSS selector
	        fittype: 'cover', // either contain, cover, fill or none
	        disableCrossDomain: 'true' // either 'true' or 'false' to not parse external CSS files.
	    });

	    function loadlink() {
	        var bla = $('#right').val();

	        $('#notifs').load("/" + bla + " #notifs > *");

	    }

	    loadlink(); // This will run on page load

	    setInterval(function() {
	        loadlink() // this will run after every 5 seconds
	    }, 1000);

	    $(function() {
	        $('.ripple').materialripple();
	    });

	    if (window.location.pathname == '/') {
	        // auto size homepage slideshow
	        var hSlides = $('section.slideshow'),
	            docH = jQuery(document).height();

	        function resize() {
	            wW = $(window).width(), //width of window
	                wH = parseInt($(window).height()) - 70; //edited height of the window (minus header height)

	            hSlides.css({
	                width: wW,
	                height: wH
	            });
	        }
	        resize();

	        $(window).resize(function() {
	            resize();
	        });
	    }

	    // floating contact button on vendor pages
	    if (window.location.pathname.split('/')[1] == 'vendors') {
	        if (jQuery(window).width() > 600) {
	            $(window).scroll(function() {
	                if ($(".v-contact").offset().top < 547) {
	                    //get the left position of the parent container
	                    var rowL = parseInt($('.title-header > .row').offset().left) + 30;

	                    $(".v-contact").addClass("bfxd").css('right', rowL);
	                    // console.log('addfixed');
	                }
	            });
	        }

	    }

	    // show hide filters on category pages on small screens
	    if (window.location.pathname.split('/')[2] == 'list') {

	        var filterH = $('.cat-filters');

	        $('.filter-icon a').click(function() {
	            $(this).parent().parent().parent().parent().css('height', 'initial');
	            console.log('clicked');
	        });
	    }
	    //location filters
	    $(function() {
	        var cc = 0;
	        $('#locate').click(function() {
	            cc++;
	            if (cc == 2) {
	                $(this).change();
	                cc = 0;
	            }
	        }).change(function() {
	            window.location = $('option:selected', this).val();

	        });
	    });
	    //resize category filter
	    $(function() {
	        var filterH = $('.cat-filters');

	        $('.filter-icon a').click(function() {
	            $(this).parent().parent().parent().parent().css('height', 'initial');
	        });
	    });
	});
