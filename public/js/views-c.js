(function($){

	$(window).load(function(){

		if(window.location.pathname.split('/')[1] == 'vendor') {
			var bStyle = $('.c-header > .row:first > div > .row:first').css('borderLeft');

			$('.cat-li li .s-wrap').css('borderLeft',bStyle);
		}

		$('.loader').remove();

		$('.ld').removeClass('ld');

	});

	$(document).ready(function(){

		// initialize foundation logic
		$(document).foundation();


		function loadlink(){
			var bla = $('#right').val();

        $('#notifs').load("/"+bla+" #notifs > *");

		}

		loadlink(); // This will run on page load

		setInterval(function(){
		    loadlink() // this will run after every 5 seconds
		}, 1000);

	    $(function(){
	      $('.ripple').materialripple();
	    });

	    if(window.location.pathname == '/'){
			$('.slideshow').slick({
				autoplay: true,
		  		autoplaySpeed: 7000,
		      infinite: true,
		      arrows:false,
		      mobileFirst: true,
		    });

			// auto size homepage slideshow
			var hSlides = $('section.slideshow'),
				docH = jQuery(document).height();

			function resize() {
				wW = $(window).width(), //width of window
				wH = parseInt($(window).height()) - 70; //edited height of the window (minus header height)

			   hSlides.css({
				   width: wW,
				   height: wH
				});
			}
			resize();

			$(window).resize(function(){
				resize();
			});
		}

		// floating contact button on vendor pages
		if(window.location.pathname.split('/')[1] == 'vendor' && window.location.pathname.split('/')[2] != 'list') {
			if(jQuery(window).width() > 600) {
				$(window).scroll(function(){
				    if($(".v-contact").offset().top < 547) {
				    	//get the left position of the parent container
				    	var rowL = parseInt($('.v-header > .row').offset().left) + 30;

					    $(".v-contact").addClass("bfxd").css('right', rowL);
					    // console.log('addfixed');
				  	} else {
					    $(".v-contact").removeClass("bfxd");
					    // console.log('removefixed');
				  	}
				});
			}

		}

		// show hide filters on category pages on small screens
		if(window.location.pathname.split('/')[2] == 'list') {

			var filterH = $('.cat-filters');

			$('.filter-icon a').click(function(){
				$(this).parent().parent().parent().parent().css('height','initial');
				console.log('clicked');
			});
		}

	});

})(jQuery);
