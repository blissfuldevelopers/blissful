-- MySQL dump 10.13  Distrib 5.6.33, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: master_db
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `boost_category`
--

DROP TABLE IF EXISTS `boost_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `boost_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `boost_category`
--

LOCK TABLES `boost_category` WRITE;
/*!40000 ALTER TABLE `boost_category` DISABLE KEYS */;
INSERT INTO `boost_category` VALUES (1,'Visibility Plus',NULL,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL),(2,'Visibility Gold',NULL,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL),(3,'Intelligence Plus',NULL,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL),(4,'Intelligence Gold',NULL,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL),(5,'Quotation boost',1,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL),(6,'Category profile highlight',1,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL),(7,'Quotation boost',2,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL),(8,'Category profile highlight',2,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL),(9,'Home page featured vendor',2,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL),(10,'User dashboard highlight',2,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL),(11,'Email highlight',2,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL),(12,'View budget',3,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL),(13,'View client name',3,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL),(14,'Real-time job alert',3,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL),(15,'View budget',4,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL),(16,'View client name',4,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL),(17,'Real-time job alert',4,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL),(18,'Quote comparison',4,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL),(19,'Monthly insights and benchmarking',4,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL),(20,'See bid numbers and activity',4,'2016-09-08 11:33:29','2016-09-08 11:33:30',NULL);
/*!40000 ALTER TABLE `boost_category` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-27 16:16:41
