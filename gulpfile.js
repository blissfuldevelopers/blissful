var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss');
});
elixir(function(mix) {
    mix.styles([
        "foundation.css",
        "/fonts/foundation/foundation-icons.css",
        "parsley.css",
        "zurb-layout.css",
        "custom.css",
        "new-home.css",
        "shop.css",
        "lobibox.css",
        "polyfill.object-fit.min.css",
        "rating.css",
        "ionicons.min.css"
    ], 'public/css/zurb-layout.css');
    mix.copy('resources/assets/css/fonts', 'public/build/fonts');
    mix.copy('resources/assets/img/arrow.png', 'public/build/img/arrow.png');
});
elixir(function(mix) {
    mix.scripts([
        "jquery-2.2.1.min.js",
        "foundation.min.js",
        "modernizr.js",
        "jquery.materialripple.js",
        "notifications.js",
        "Lobibox.js",
        "polyfill.object-fit.min.js",
        "parsley.min.js",
        "views.js",
        "autosize.min.js",
        "starrr.js"
    ], 'public/js/zurb-layout.js');
});
elixir(function(mix) {
    mix.scripts([
        "image-picker.min.js"
    ], 'public/js/image-picker.js');
});
elixir(function(mix) {
    mix.scripts([
        "cart.js"
    ], 'public/js/cart.js');
});
elixir(function(mix) {
    mix.scripts([
        "foundation-datepicker.js",
        "recaptcha-api.js",
        "lightslider.min.js",
        "jquery.validate.min.js"
    ], 'public/js/vendor-page.js');
});
elixir(function(mix) {
    mix.scripts([
        "shop-product.js"
    ], 'public/js/shop-product.js');
});
elixir(function(mix) {
    mix.styles([
        "image-picker.css"
    ], 'public/css/image-picker.css');
});
elixir(function(mix) {
    mix.styles([
        "forum.css"
    ], 'public/css/forum.css');
});
elixir(function(mix) {
    mix.styles([
        "foundation-datepicker.css",
        "lightslider.min.css"
    ], 'public/css/vendor-page.css');
});
elixir(function(mix) {
    mix.scripts([
        "jquery.js",
        "what-input.js",
        "foundation.js",
        "date.js",
        "datePicker.js",
        "app.js",
        "slick.min.js",
        "responsive-tables.js",
        "notifications.js",
        "Lobibox.js"
    ], 'public/js/home-scripts.js');
});
elixir(function(mix) {
    mix.styles([
        "starrr.css"
    ], 'public/css/vendor-show.css');
});
elixir(function(mix) {
    mix.scripts([
        "starrr.js",
        "vendor-scripts.js"
    ], 'public/js/vendor-show.js');
});
elixir(function(mix) {
    mix.scripts([
        "venue-vendor.js"
    ], 'public/js/venue-vendor.js');
});
elixir(function(mix) {
    mix.styles([
        "font-awesome.css",
        "foundation.min.css",
        "/fonts/foundation/foundation-icons.css",
        "datePicker.css",
        "app.css",
        "slick.css",
        "responsive-tables.css",
        "lobibox.css",
    ], 'public/css/home-scripts.css');
    mix.copy('resources/assets/css/fonts', 'public/build/fonts');
    mix.copy('resources/assets/img/arrow.png', 'public/build/img/arrow.png');
});
elixir(function(mix) {
    mix.sass([
        "bliss-custom.scss"
    ], 'public/css/bliss-custom.css');
});
elixir(function(mix) {
    mix.scripts([
        "jquery.lazyload-any.min.js",
        "parallax.min.js",
        "typed.min.js"
    ], 'public/js/home.js').version([
        'public/css/forum.css',
        'public/css/zurb-layout.css',
        'public/css/image-picker.css',
        'public/css/vendor-page.css',
        'public/css/home-scripts.css',
        'public/css/bliss-custom.css',
        'public/css/vendor-show.css',
        'public/js/zurb-layout.js',
        'public/js/home.js',
        'public/js/image-picker.js',
        'public/js/vendor-page.js',
        'public/js/shop-product.js',
        'public/js/cart.js',
        'public/js/home-scripts.js',
        'public/js/vendor-show.js',
        'public/js/venue-vendor.js'
    ]);
});
