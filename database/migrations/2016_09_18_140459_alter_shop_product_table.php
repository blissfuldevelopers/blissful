<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterShopProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_product', function (Blueprint $table) {
            //
            $table->text('description')->nullable()->change();
            $table->text('picture')->nullable()->change();
            $table->text('highlights')->nullable()->change();
            $table->text('tnc')->nullable()->change();
            $table->integer('qty')->default(1)->change();
            $table->string('tag')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_product', function (Blueprint $table) {
            //
        });
    }
}
