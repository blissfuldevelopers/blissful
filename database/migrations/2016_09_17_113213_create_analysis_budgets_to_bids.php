<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalysisBudgetsToBids extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('analysis_budgets_to_bids')) {
            Schema::create('analysis_budgets_to_bids', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('job_id')->unsigned()->index()->nullable();
                $table->integer('category_id')->unsigned()->index()->nullable();
                $table->integer('location_id')->unsigned()->index()->nullable();
                $table->integer('budget_amount')->nullable();
                $table->integer('bid_amount')->nullable();
                $table->integer('jobs_closed')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('analysis_budgets_to_bids')) {
            Schema::drop('analysis_budgets_to_bids');
        }
    }
}
