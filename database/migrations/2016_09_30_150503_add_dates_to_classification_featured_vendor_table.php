<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDatesToClassificationFeaturedVendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('classification_featured_vendor', function (Blueprint $table) {
            $table->dateTime('start_date');
            $table->dateTime('stop_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('classification_featured_vendor', function (Blueprint $table) {
            //
        });
    }
}
