<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_locations', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('address');
            $table->float('lat',25, 20);
            $table->float('lng',25, 20);
            $table->integer('job_id');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('jobs_user');
    }
}
