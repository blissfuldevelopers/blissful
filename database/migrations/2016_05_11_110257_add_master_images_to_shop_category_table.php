<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMasterImagesToShopCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_category', function (Blueprint $table) {
            $table->string('header_image');
            $table->string('header_text');
            $table->string('sub_header_image');
            $table->string('sub_header_text');
            $table->string('sub_header_sub_text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_category', function (Blueprint $table) {
            //
        });
    }
}
