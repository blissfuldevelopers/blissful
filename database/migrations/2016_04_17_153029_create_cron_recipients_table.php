<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCronRecipientsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cron_recipients', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('message_id');
            $table->bigInteger('mobile_number');
            $table->string('email');
            $table->tinyInteger('sent')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cron_recipients');
    }
}
