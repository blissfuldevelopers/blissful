<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCronSchedulesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cron_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cron_task_id');
            $table->datetime('repeat_start');
            $table->datetime('repeat_stop');
            $table->string('cron_min');
            $table->string('cron_hour');
            $table->string('cron_day_of_month');
            $table->string('cron_month');
            $table->string('cron_day_of_week');
            $table->integer('created_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cron_schedules');
    }
}
