<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBreakdownDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('breakdown_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_name');
            $table->integer('unit_price');
            $table->integer('quantity');
            $table->string('description');
            $table->integer('cat_id');
            $table->integer('user_id');
            $table->integer('bid_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('breakdown_details');
    }
}
