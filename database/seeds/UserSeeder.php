<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UserSeeder extends Seeder{

    public function run(){
        DB::table('users')->delete();

        $adminRole = Role::whereName('administrator')->first();
        $userRole = Role::whereName('user')->first();
        $vendorRole = Role::whereName('vendor')->first();
        $editorRole = Role::whereName('editor')->first();

        $user = User::create(array(
            'first_name'    => 'Peter',
            'last_name'     => 'Mwai',
            'email'         => 'peter.mwai@hugeafrica.com',
            'password'      => Hash::make('password')
        ));
        $user->assignRole($adminRole);

        $user = User::create(array(
            'first_name'    => 'Ben',
            'last_name'     => 'Maina',
            'email'         => 'ben.maina@hugeafrica.com',
            'password'      => Hash::make('password')
        ));
        $user->assignRole($vendorRole);

        $user = User::create(array(
            'first_name'    => 'Higi',
            'last_name'     => 'Chege',
            'email'         => 'higi.chege@hugeafrica.com',
            'password'      => Hash::make('password')
        ));
        $user->assignRole($userRole);

        $user = User::create(array(
            'first_name'    => 'Mitchell',
            'last_name'     => 'Mitchell',
            'email'         => 'mitchell.mitchel@hugeafrica.com',
            'password'      => Hash::make('password')
        ));
        $user->assignRole($editorRole);
    }
}