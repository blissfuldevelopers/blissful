<?php

use Illuminate\Database\Seeder;
use App\Profile;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('profiles')->delete();

        Profile::create([
            'user_id'=> 1,
            'description' => 'I am an admin',
            'location' => 'Nakuru',
            'phone' => 345678,
        ]);

        Profile::create([
            'user_id'=> 2,
            'description' => 'I am a ',
            'location' => 'Kisii',
            'phone' => 45678,
        ]);

        Profile::create([
            'user_id'=> 3,
            'description' => 'I am an editor',
            'location' => 'Narok',
            'phone' => 99678,
        ]);
        Profile::create([
            'user_id'=> 3,
            'description' => 'I am User',
            'location' => 'Mombasa',
            'phone' => 5099678,
        ]);

    }
}
