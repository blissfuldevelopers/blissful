<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use App\User;
use Bican\Roles\Models\Role;
use Bican\Roles\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * php artisan db:seed --class=PermissionSeeder
     * Feel free to run this every time you update the permissions
     * Try to tie permissions to roles, rather than users.
     *  It's easier to manage them that way...
     *
     * For Further reading on the roles functions being used
     * https://github.com/romanbican/roles
     *
     */
    public function run()
    {

        DB::table('permissions')->delete();

        $permissions = Config::get('bliss_permissions.permissions');
        $permission_crud = Config::get('bliss_permissions.crud');
        $user_roles = Config::get('bliss_permissions.user_role_permissions');;


        $permissions = array_map('strtolower', $permissions);
        $permission_crud = array_map('strtolower', $permission_crud);
        $permissions_ = [ ];
        foreach ($permissions as $permission) {
            $permission = strtolower($permission);
            foreach ($permission_crud as $_crud) {
                $_crud = strtolower($_crud);

                $permissions_[ $permission ][ $_crud ] = Permission::create([
                                                                                'name' => ucwords($_crud) . ' ' . ucwords($permission),
                                                                                'slug' => $_crud . '.' . $permission,
                                                                            ]);
            }
        }

        $_user_role_names = array_keys($user_roles);
        $_user_role_names = array_map('strtolower', $_user_role_names);

        $roles = Role::whereIn(DB::raw('lower(name)'), $_user_role_names)->get();

        foreach ($roles as $_role) {

            switch (strtolower($_role->name)) {
                case 'administrator':
                case 'admin':
                    foreach ($permissions_ as $permission => $_crud) {
                        foreach ($_crud as $_inner_permission_) {
                            $_role->attachPermission($_inner_permission_);
                        }
                    }
                    break;
                default:
                    $_roles = $user_roles[ $_role->name ];

                    foreach ($_roles as $role_permission => $crud_array) {

                        if (!empty( $crud_array )) {
                            $crud_array = array_map('strtolower', $crud_array);

                            $_crud_array = ( in_array('all', $crud_array) )
                                ? $permission_crud //use the $permission_crud array
                                : $crud_array; // use the crud defined for this user role


                            foreach ($_crud_array as $crud) {

                                if (isset( $permissions_[ $role_permission ][ $crud ] )) {
                                    $_role->attachPermission($permissions_[ $role_permission ][ $crud ]);
                                }
                            }
                        }
                    }
                    break;
            }


        }

    }
}