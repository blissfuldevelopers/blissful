<?php

use Illuminate\Database\Seeder;
use Bican\Roles\Models\Role; //* For Further reading on the roles functions being used

class RoleSeeder extends Seeder
{

    public function run()
    {

        DB::table('roles')->delete();

        $main_roles = [
            'user',
            'vendor',
            'administrator',
            'editor',
        ];

        foreach ($main_roles as $role) {
            $role = strtolower($role);
            $slug = ( $role == 'administrator' ) ? 'admin' : $role;
            Role::create([
                             'name'        => ucwords($role),
                             'slug'        => $slug,
                             'description' => ucwords($role) . ' role', // optional
                             'level'       => 1, // optional, set to 1 by default
                         ]);
        }

//        Role::create([
//                         'name' => 'user',
//                     ]);
//
//        Role::create([
//                         'name' => 'vendor',
//                     ]);
//
//        Role::create([
//                         'name' => 'editor',
//                     ]);
//
//        Role::create([
//                         'name' => 'administrator',
//                     ]);
    }
}