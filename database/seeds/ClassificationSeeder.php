<?php

use Illuminate\Database\Seeder;
use App\Classification;
class ClassificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('classifications')->delete();

        Classification::create([
            'name'   => 'Venue',
            'description' => 'All wedding venues in Kenya in one place. Whether you are looking at having a garden wedding or a reception in a hotel, you will find all the contacts here.',
            'slug' =>'venue'
        ]);

        Classification::create([
            'name'   => 'Catering',
            'description' => 'Browse from the widest variety of wedding or party caterers from across Kenya. Choose different cuisines and dishes that match your budget from the best catering companies across the country.',
            'slug' =>'catering'
        ]);

        Classification::create([
            'name'   => 'Hair and Make Up',
            'description' =>'Being made up is not just about looking good but looking fantastic throughout the whole day. Find your prefered make up artists here',
            'slug' =>'hair-and-make-up'
        ]);

        Classification::create([
            'name'   => 'Flowers',
            'description' =>'Brighten up your wedding day with your selection of floral arrangements that match your colour scheme. Find the best florist in Kenya here.',
            'slug' =>'flowers'
        ]);

        Classification::create([
            'name'   => 'Cake',
            'description' =>'Browse the most creative wedding cakes and designs for a sweet and unique dessert table come your big day from bakers across Kenya.',
            'slug' =>'cake'
        ]);

        Classification::create([
            'name'   => 'Tents and Chairs',
            'description' =>'From dome tents to chiavari chairs. No matter the design find all tents and chairs suppliers in Kenya here.',
            'slug' =>'tents-and-chairs'
        ]);

        Classification::create([
            'name'   => 'Decor',
            'description' =>'Get the best decor in Kenya here',
            'slug' =>'decor'
        ]);

        Classification::create([
            'name'   => 'Photography and Videography',
            'description' =>'Get the best photographers in Kenya to capture this once in a lifetime moment. All of Kenyas wedding photographers listed in one place',
            'slug' =>'photography-and-videography'
        ]);
        Classification::create([
            'name'   => 'Stationery',
            'description' =>'Get the best designs for invitations, programs and any other keepsakes for your events.',
            'slug' =>'stationery'
        ]);
        Classification::create([
            'name'   => 'Bands and DJs',
            'description' =>'Create a fun and memorable atmosphere by hiring the best band and or DJ to entertain your guests at your reception. Find the contacts of your favorite bands in Kenya here',
            'slug' =>'bands-and-djs'
        ]);
        Classification::create([
            'name'   => 'Event Planner',
            'description' =>'Looking for a wedding planner for your big day. Get the best professional wedding and event planners in Kenya here.',
            'slug' =>'event-planner'
        ]);
        Classification::create([
            'name'   => 'Bridesmaids Dresses',
            'description' =>'Dress your bridesmaids in fantastic dresses from the vendors here.',
            'slug' =>'bridesmaids-dresses'
        ]);
        Classification::create([
            'name'   => 'Mens Wear',
            'description' =>'Best suits, tuxedos, blazers and even custom made African men wear',
            'slug' =>'mens-wear'
        ]);
        Classification::create([
            'name'   => 'Bridal Gowns',
            'description' =>'Discover the best wedding dress tailors and suppliers from across Kenya. With hundreds of unique bridal tailors to choose from, select the best dresses and themes thats perfect for your special day.',
            'slug' =>'bridal-gowns'
        ]);
    }
}
