<!doctype html>
<html class="no-js" lang="en">
  <head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Blissful | @yield('title')</title>
  <link rel="stylesheet" href="/css/foundation.css" />
  <link rel="stylesheet" href="/fonts/foundation/foundation-icons.css" />
  <link rel="stylesheet" href="/css/main.css" />
  <link rel="stylesheet" href="/css/custom.css" />
  <script src="/js/vendor/modernizr.js"></script>
  <link rel="stylesheet" href="/css/jquery.materialripple.css">

  @yield('styles')
</head>
<body>


  <!-- START OF HEADER SECTION -->
  <header id="header">
    <div class="row" >
      <div class="small-12 columns">
        <nav class="top-bar" data-topbar role="navigation">
          <ul class="title-area">
            <li class="name">
              <h1><a href="{{ route('public.home')  }}"><img src="/img/blissful-logo.svg"></a></h1>
               @if(!Auth::check())
               @else
              <li id="notif_sm" class="show-for-small-only">
              <?php
                  $count = Auth::user()->newMessagesCount();
                  $cssClass = $count == 0 ? 'hidden' : '';
                  ?>
                  <i class="fi-mail md header_mail"></i><span type="{{$cssClass}}" class="noti_bubble_sm {{$cssClass}}">{!! $count !!}</span>
              </li>
              @endif
            </li>
             <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a class="ripple" href="#"><span>MENU</span></a></li>
          </ul>
          <section class="top-bar-section">
            <!-- Right Nav Section -->
            <ul id="right" class="right">
              @if(!Auth::check())
              <li><a href="{{ route('auth.vendor-register') }}"><button class="ripple">List Your Business</button></a></li>
              <li><a class="ripple" href="#">Help</a></li>
              <li><a class="ripple" href="{{ route('auth.login') }}">Log In</a></li>
              @else
              <li id="notif" class="hide-for-small-only">
              <?php
                  $count = Auth::user()->newMessagesCount();
                  $cssClass = $count == 0 ? 'hidden' : '';
                  ?>
                  <i class="fi-mail md header_mail"></i><span type="{{$cssClass}}" class="noti_bubble {{$cssClass}}">{!! $count !!}</span>
              </li>
              <li class="pimg">
              <a class="ripple" data-dropdown="drop1" aria-controls="drop1" aria-expanded="false">
                <img src="{{Auth::user()->profile->profilePic}}" alt="profile pic">  {{Auth::user()->first_name}}
              </a>
              <ul id="drop1" class="f-dropdown hide-for-small-only" data-dropdown-content aria-hidden="true" tabindex="5">
                <li><a href="/login"> <i class="fi-home"></i> Dashboard</a></li>
                <li><a href="{{ url('profile/edit/'.Auth::user()->profile->user_id) }}"> <i class="fi-torso"></i> My Profile</a></li>
                <li><a href="#"> <i class="fi-alert"></i> Help</a></li>
                <li><a href="{{ route('authenticated.logout') }}"> <i class="fi-power" style="color:red;"></i> Logout</a></li>
              </ul>
              </li>
              <ul class="show-for-small-only">
              <li><a href="/login"> <i class="fi-home"></i> Dashboard</a></li>
              <li><a href="{{ url('profile/edit/'.Auth::user()->profile->user_id) }}"> <i class="fi-torso"></i> My Profile</a></li>
              <li><a href="#"> <i class="fi-alert"></i> Help</a></li>
              <li><a href="{{ route('authenticated.logout') }}"> <i class="fi-power" style="color:red;"></i> Logout</a></li>
              </ul>
              @endif
            </ul>
            <!-- Left Nav Section -->
            <ul class="left">
              <li>
                <form>
                  <div class="row">
                    <input id="search" type="text" placeholder="Find a Vendor" />
                  </div>
                </form>
              </li>
            </ul>
          </section>
        </nav>
      </div>
    </div>
  </header>
  <!-- END OF HEADER SECTION -->

  @yield('content')
  
    <!-- START OF FOOTER SECTION -->
  <footer>
    <div class="tft">
      <div class="row">
        <div class="small-6 columns">
          <div class="row">
            <div class="small-12 medium-6 large-6 columns">
              <h5>COMPANY</h5>
              <ul>
                <li><a href="">About Us</a></li>
                <li><a href="">Privacy Policy</a></li>
                <li><a href="">Contact Us</a></li>
                <li><a href="{{ route('blog') }}">Blog</a></li>
              </ul>
            </div>
            <div class="small-12 medium-6 large-6 columns">
              <h5>PARTNERS</h5>
              <ul>
                <li><a href="">List Your Business</a></li>
                <li><a href="">Advertise</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="small-6 columns">
          <div class="row">
            <div class="small-12 columns">
              <ul class="socials">
                <li class="f"><a href=""></a></li>
                <li class="t"><a href=""></a></li>
                <li class="g"><a href=""></a></li>
                <li class="p"><a href=""></a></li>
              </ul>
              <ul class="right">
                <li><a href="">Privacy Policy</a></li>
                <li><a href="">Terms and Conditions</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="bft">
      <div class="row">
        <div class="small-12 columns">
          <span>© 2015 Blissful</span>
        </div>
      </div>
    </div>
  </footer>
  <!-- END OF FOOTER SECTION -->
  <script src="/js/vendor/jquery.js"></script>
  <script src="/js/foundation.min.js"></script>
  <script src="/js/slick.min.js"></script>
  <script src="/js/jquery.materialripple.js"></script>
  <script src="/js/views.js"></script>
  <script type="text/javascript">// <![CDATA[
                var socket = io.connect('http://107.170.63.247:3000', {
                        query: 'user_id={{ Session::getId() }}'
                });
                socket.on('connect', function(data) {
                        socket.emit('subscribe', { channel: 'notifications' });
                });
                socket.on('notifications', function (data) {
                        console.log(data);
                });
        // ]]></script>
  @yield('scripts')
</body>
</html>

