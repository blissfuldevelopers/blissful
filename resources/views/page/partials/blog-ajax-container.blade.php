<!-- Start of main jobs section -->
<div class="row">
    <div id="blog-container-back-action">
        <ul class="sub-nav">
            <li class="small-3 columns">
                <a href="#back" class="small-12">
                    <i class="fi-arrow-left medium"></i>
                    <span class="text">Back</span>
                </a>
            </li>

            <li class="small-5 columns hm">
                <span class="small-12 grey">
                    <!-- <a href="#home" class="small-12"> -->
                    <!-- <span class="text">User Management</span> -->
                    <!-- </a> -->
                </span>
            </li>

            <li class="small-3 columns">
                <a href="#reload" class="">
                    <i class="fi-refresh medium right"></i>
                </a>
                <span id="curr-page-name"
                      data-url=""
                ></span>
            </li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="small-12 columns" id="blog-container-data" data-url="{{ @$default_blog_route }}"
         data-requested-url="{{ @$requested_blog_route }}">

    </div>
</div>