<style type="text/css">

    .stars {
        margin-bottom: 0;
        font-size: 24px;
        color: #92bfe6
    }

    .stars span {
        margin-right: 4px
    }
    .feedback-btn{
        display: block;
        position: fixed;
        top: 200px;
        right: -45px;
        z-index: 99;
        font-size: 1.2rem;
        padding: 5px 10px;
        color: #fff !important;
        background-color: #1bb2a6;
        -ms-transform: rotate(-90deg); /* IE 9 */
        -webkit-transform: rotate(-90deg); /* Safari */
        transform: rotate(-90deg);
    }
</style>
<div id="feedbackModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <div class="modal-header">
        <h3 id="myModalLabel">We'd Love to Hear From You</h3>
    </div>
    <div class="modal-body">
        {!! Form::open(['url' => '/feedback', 'id'=>'form','method' => 'POST'])!!}

        @include('includes.errors')

        {!!Form::hidden('rating', null, ['class'=>'ratings-hidden'])!!}

        {!!Form::textarea('comment', null, ['rows'=>'5','id'=>'new-review','class'=>'form-control animated','placeholder'=>'Enter your feedback here...','required'])!!}
        <div class="text-left">
            <div class="stars starrr" data-rating="{{Input::old('rating',0)}}"></div>
            <a href="#" class="tiny" id="close-review-box"
               style="display:none; margin-right:10px;">
                <span class="glyphicon glyphicon-remove"></span>Cancel</a>
            <div id="feedbackCaptcha" class="breaker" ></div>
            <button class="tiny breaker" type="submit">
                Save
            </button>
        </div>

        {!! Form::close()!!}
    </div>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<div id="feedback"><a href="#" class="feedback-btn" data-reveal-id="feedbackModal"> Feedback <i class="fi-pencil medium"></i></a></div>