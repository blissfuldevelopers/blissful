<a href="#" id="subscribe-link" style="display:none"></a>
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        
        var OneSignal = OneSignal || [];

        // different configs for ssl and non-ssl
        if (window.location.protocol != "https:"){
            OneSignal.push(["init", {
                appId: "{{env('ONESIGNAL_APP_ID')}}",
                subdomainName: "{{env('ONESIGNAL_SUBDOMAIN_NAME')}}",
                autoRegister: true,
                notifyButton:{
                    enable:true
                }
            }]);
        }
        else{
            OneSignal.push(["init", {
                appId: "{{env('ONESIGNAL_APP_ID')}}",
                autoRegister: true,
                notifyButton:{
                    enable:false
                }
            }]);
        }

        OneSignal.push(function() {

            // If we're on an unsupported browser, do nothing
            if (!OneSignal.isPushNotificationsSupported()) {
                return;
            }
            OneSignal.isPushNotificationsEnabled(function(isEnabled) {
                if (isEnabled) {
                    // The user is subscribed to notifications
                    //if the user device isn't registered on our system
                    @if(Auth::check())
                        saveDets();
                    @endif
                } else {
                    //request permission
                    // $( "#subscribe-link" ).on( "click", function(event) {
                    //   event.preventDefault();
                    //   subscribe();
                    // });
                    // $('#subscribe-link').trigger("click");
                }
            });
        });
        function subscribe() {
            OneSignal.push(function(){

                OneSignal.registerForPushNotifications();
                saveDets();
            });
            
        }

        function saveDets(){
            @if(Auth::check())
                OneSignal.sendTag("name", "{{Auth::user()->first_name}}");
                @if(Auth::user()->hasUserRole('administrator'))
                    OneSignal.sendTag("type", "administrator");
                @elseif(Auth::user()->hasUserRole('vendor'))
                    OneSignal.sendTag("type", "vendor");
                @else
                OneSignal.sendTag("type", "user");
                @endif
            @endif
            OneSignal.getUserId(function(userId) {
                    if (!userId) {
                        subscribe();
                    }

                    var dataObj = {};
                    dataObj['userId']=userId;
                    $.ajax({
                        type: 'POST',
                        url: '{{route('devices.store')}}',
                        data: dataObj
                    });
            });
        }

    </script>