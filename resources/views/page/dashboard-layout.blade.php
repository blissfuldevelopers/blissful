<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <script data-cfasync="false" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <link rel="manifest" href="/manifest.json">
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
    @yield('head')
    <title>Blissful | @yield('title')</title>
    <link rel="icon" type="image/png" href="/img/dashboard/favicon.ico"/>
    <link rel="stylesheet" href="/css/foundation.css"/>
    <link rel="stylesheet" href="/fonts/foundation/foundation-icons.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/zurb-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="/css/dashboard/redesign.css">
    <link href="/app/client/css/shop.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    @yield('styles')

    <script data-cfasync="false" type="text/javascript">
        if (!window.jQuery) {
            // Offline JS
            document.write('<script data-cfasync="false" src="https://code.jquery.com/jquery-2.1.1.min.js"><\/script>');
        }
    </script>
</head>
<body>
<!-- START OF HEADER SECTION -->
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-50669449-1', 'auto');
    ga('send', 'pageview');

</script>
<!-- START OF CREDIT BUY ALERT -->
@if(Auth::user()->hasUserRole('vendor') && $balance < 100)
    <div class="credit-banner">
        <div class="row">
            <div class="small-6 columns">
                <h5>You are running low on credits. Don't miss out on new jobs.</h5>
            </div>
            <div class="small-6 columns">
                <button class="buttons" onclick="window.location='/shop/category/blissful-credits';">TOP UP</button>
            </div>
        </div>
    </div>
    @endif
            <!-- END OF CREDIT BUY ALERT -->
    <header class="dash">
        <!-- <div class="row"> -->
        <div class="small-12">
            <nav class="top-bar" data-topbar role="navigation">
                <ul class="title-area">
                    <li class="name">
                        <h1 class="hide-for-small-only"><a onclick="window.location='{{ route('home')  }}';"><img
                                        src="https://s3.eu-central-1.amazonaws.com/blissful-ke/img/blissful-bw.png"></a></h1>
                        <h1 class="show-for-small-only"><a onclick="window.location='{{ route('home')  }}';"><img
                                        src="https://s3.eu-central-1.amazonaws.com/blissful-ke/img/blissful-bw-small.png"></a></h1>
                    </li>
                </ul>
                <section class="top-bar-section">
                    <!-- Right Nav Section -->
                    <!-- <ul id="right" class="right"> -->
                    <!-- <li>
              <form action="/search" method="GET" enctype="multipart/form-data">
                <div class="row warning">
                  <input name="find" id="find" type="text"
                   class="@if(Session::has('searchErrors')) red_border @endif"
                   placeholder="@if(Session::has('searchErrors'))Search not found @endif"/>
                </div>
              </form>
            </li> -->
                    <!-- <li>
                      <a href="/shop/cart" class="showCart" style="display:none; position: relative">
                        <h3><i class="fi-shopping-cart"></i></h3>
                        <span class="badge" id="CartCount">0</span>
                      </a>
                    </li> -->
                    @if(!Auth::check())
                            <!-- <li class="hide-for-small-only">
              <a href="{{ route('auth.user-type') }}">
                <button class="ripple">Join Us</button>
              </a>
            </li>
            <div class="breaker show-for-small-only"></div>
            <li class="show-for-small-only" style="background-color: #00A89B!important;font-size:15px;">
              <a class="error-center white" href="{{ route('auth.user-type') }}">Join Us</a>
            </li>
            <li>
              <a class=" error-center ripple" href="#">Help</a>
            </li>
            <li>
              <a class=" error-center ripple" href="{{ route('auth.login') }}">Log In</a>
            </li> -->
                    @else
                    <?php
                    $count = count(Auth::user()->threadsWithNewMessages());
                    $cssClass = $count == 0 ? 'hide' : '';
                    ?>
                            <!-- <li class="pimg">
              <div class="breaker show-for-small-only"></div>
              <a class="ripple" data-dropdown="drop1" aria-controls="drop1" aria-expanded="false" style="margin-right:15px;">
                <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{Auth::user()->profile->profilePic}}" alt="profile pic"> <span class="white">{{Auth::user()->first_name}}</span>
              </a>
              <ul id="drop1" class="f-dropdown hide-for-small-only" data-dropdown-content aria-hidden="true" tabindex="5">
                <li><a href="/dashboard">
                  <i class="fi-home"></i> Dashboard</a>
                </li>
                <li><a href="@if( Auth::user()->hasRole('vendor'))
                    {{route('users.edit',array(Auth::user()->profile->user_id))}}
                    @else
                    {{ url('profile/edit/'.Auth::user()->profile->user_id) }}
                    @endif">
                  <i class="fi-wrench"></i> Profile</a>
                </li>
                <li><a href="/dashboard"><i class="fi-mail"></i>
                   Inbox <span class="alert {{$cssClass}} label round"> {{$count}}</span></a>
                </li>
                <li><a href="/dashboard"><i class="fi-shopping-bag"></i> Jobs</a>
                <li><a href="{{ route('authenticated.logout') }}">
                  <i class="fi-power" style="color:red;"></i> Logout</a>
                </li>
              </ul>
            </li> -->
                    <!-- <ul class="show-for-small-only" style="margin-left:20px;">
                <li><a href="/dashboard"><i class="fi-home"></i> Dashboard</a>
                </li>
                <li><a href="/dashboard"> <i class="fi-mail"></i> Inbox
                      <span class="alert {{$cssClass}} label round"> {{$count}}</span></a>
                </li>
                <li><a href="{{ route('authenticated.logout') }}"> <i class="fi-power"
                      style="color:red;"></i> Logout</a>
                </li>
              </ul> -->
                    @endif
                            <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                    <!-- </ul> -->
                    <span class="udash-toggle show-for-small-only"><a href="#"><i class="fi-list"></i></a></span>
                </section>
            </nav>
        </div>
    <!--Start of Crisp Live Chat Script-->


    <!--End of Crisp Live Chat Script-->
    </header>
    <!-- END OF HEADER SECTION -->

    @yield('content')

<!-- START OF FOOTER SECTION -->
<footer>
    <div class="tft">
        <div class="row">
            <div class="small-12 medium-6 large-6 columns">
                <div class="row">
                    <div class="small-6 columns">
                        <h5>COMPANY</h5>
                        <ul>
                            <li><a href="/about">About Us</a></li>
                            <li><a href="/contact">Contact Us</a></li>
                            <li><a href="{{ route('blog') }}">Blog</a></li>
                        </ul>
                    </div>
                    <div class="small-6 columns">
                        <h5>PARTNERS</h5>
                        <ul>
                            <li><a href="{{ route('auth.user-type') }}">Join Us</a></li>
                            <li><a href="">Advertise</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="small-12 medium-6 large-6 columns">
                <div class="row">
                    <div class="small-12 columns">
                        <ul class="socials">
                            <li class="f"><a href="https://www.facebook.com/blissfulKE"></a></li>
                            <li class="t"><a href="https://twitter.com/blissfulKE"></a></li>
                            <li class="g"><a href="https://plus.google.com/112646574144413915809/posts"></a></li>
                            <li class="p"><a href="https://www.pinterest.com/blissfulKE"></a></li>
                        </ul>
                        <ul class="right">
                            <li><a href="/privacy-policy">Privacy Policy</a></li>
                            <li><a href="/terms-and-conditions">Terms and conditions</a></li>
                            <li><span class="fi-telephone tiny white"></span> <span
                                        class="white ns tiny"> 0775 987 555</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bft">
        <div class="row">
            <div class="small-12 columns">
                <span>© <?php echo date("Y"); ?> Blissful</span>
            </div>
        </div>
    </div>
</footer>
<!-- END OF FOOTER SECTION -->
<script src="/js/zurb-layout.js"></script>
<script src="/js/views.js"></script>
<script src="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
@yield('scripts')

    <script type="text/javascript">
        $(document).ready(function () {
            //correct header height
            if ($(window).width() < 739) {
                $(".parallax-mirror").css({top: "70px !important"});
            }

            // menu
            // <span class="udash-toggle"><a href="#"><i class="fi-list"></i></a></span>
            
            function toggleClassMenu() {
                myMenu.classList.add("menu--animatable");   
                if( myMenu.classList.contains("menu--hidden")) { 
                    myMenu.classList.remove('menu--hidden');    
                    myMenu.classList.add("menu--visible");
                    dashMain.classList.add("main--under");
                } else {
                    myMenu.classList.remove('menu--visible');
                    myMenu.classList.add('menu--hidden');
                    dashMain.classList.remove("main--under");    
                }   
            }

            function OnTransitionEnd() {
                myMenu.classList.remove("menu--animatable");
            }
            function check_if_sidebar_open(){
                if (myMenu.classList.contains("menu--visible")) {
                    toggleClassMenu();
                }
            }
            var dashMain = document.querySelector('.dashboard > .medium-10.columns');
            var myMenu = document.querySelector('.dashboard > .medium-2.columns');
            var oppMenu = document.querySelector('span.udash-toggle a');
            myMenu.addEventListener("transitionend", OnTransitionEnd, false);
            oppMenu.addEventListener("click", toggleClassMenu, false);
            myMenu.addEventListener("click", toggleClassMenu, false);
            dashMain.addEventListener("click", check_if_sidebar_open, false);

            $("body").on("swipeleft", function () {
               
                    if( myMenu.classList.contains("menu--visible")) {
                        toggleClassMenu();
                        myMenu.addEventListener("transitionend", OnTransitionEnd, false);
                    }
                    
            });
            $("body").on("swiperight", function (event) {
                    // take 15% of screen good for diffrent screen size
                    var window_width_15p = $( window ).width() * 0.08;
                    // check if the swipe right is from 15% of screen (coords[0] means X)
                    if ( event.swipestart.coords[0] < window_width_15p) {
                       // open your panel
                       toggleClassMenu();
                    }
                    
            });

            // $('span.udash-toggle a').click(function (event) {
            //     event.preventDefault();

            //     dashMenu.toggle(function () {
            //         // dashMenu.addClass('uopen');
            //         console.log('open');
            //     }, function () {
            //         // dashMenu.removeClass('uopen');
            //         console.log('closed');
            //     });


            // });
            // var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
            // if (isMobile) {
            //     $("body").on("swipeleft", function () {
            //         $('.dashboard > .medium-2.columns').hide(500);
            //     });
            //     $("body").on("swiperight", function () {
            //         $('.dashboard > .medium-2.columns').show(500);
            //     });
            // }

        });


    </script>

    <script src="/app/client/js/shop.js" type="text/javascript"></script>
    <script src="/app/client/js/notifications.js" type="text/javascript"></script>
    <script src="/app/client/js/Lobibox.js" type="text/javascript"></script>
    <script>

        function notification(type, msg, dely) {
            // 'warning', 'info', 'success', 'error'
            Lobibox.notify(type, {
                size: 'normal', // normal, mini, large
                icon: false,
                closable: true,
                sound: false,
                position: "top right",
                delay: dely || 5000,
                msg: msg
            });
        }

        function cartCount() {
            $.ajax({
                url: '/shop/cart-count',
                type: 'GET',
                success: function (data) {

                    if (data > 0) {
                        $(".showCart").show();
                    }
                    $("#CartCount").text(data);
                },
                error: function (e) {
                }
            });
        }
        cartCount();
    </script>
    <script type="text/javascript">
        $(window).load(function () {
            $("#spopup").show(700);
        });
        function closeSPopup() {
            $('#spopup').hide(700);
        }
    </script>
    <script type="text/javascript">
        $(".placard").click(function () {
            window.location = $(this).find("a").attr("href");
            return false;
        });
    </script>
    <script type="text/javascript">
        $(".card-box").click(function () {
            window.location = $(this).find("a").attr("href");
            return false;
        });
    </script>
</body>
</html>