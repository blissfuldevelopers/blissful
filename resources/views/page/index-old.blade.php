@extends('page.shop-layout')
@section('title')
    Event Planning in Kenya - Fast & Reliable
@endsection
@section('metas')
    <meta name="keyword" content=""/>
    <meta name="description" content=""/>
@endsection
@section('nav')
    @include('page.partials.home-nav')
@endsection
@section('content')
            <!-- START OF MAIN SECTION -->
    <main role="main" id="homepg">
    @include('includes.status-zurb')
    @include('includes.errors-zurb')
    <!-- Intro Feature -->
        <section class="intro">
            <img src="img/newhome/intro-bkgd.jpg">
            <div class="row">
                <div class="small-10 small-centered medium-6 columns">
                    <div class="intro-search-wrap">
                        <h2>Get the best quotes for </br> your <span id="typed"></span></h2>
                        <span class="typed-intro" style="display:none;">
                <p>Birthday...</p>
                <p>Graduation...</p>
                <p>Wedding...</p>
                <p>Event...</p>
              </span>
                        <form action="/jobs/create" method="GET">
                            <select name="option">
                                <option value="" disabled selected>Get me quotes for...</option>
                                @foreach($classifications as $classification)
                                    <option value="{{$classification->id}}">{{$classification->name}}</option>
                                @endforeach
                            </select>
                            <input type="submit" value="GO">
                        </form>
                        <ul>
                            <li>
                                <span class="ion-briefcase"></span>
                                <h5>+ 2,000</h5>
                                <h6>Suppliers Listed</h6>
                            </li>
                            <li>
                                <span class="ion-clipboard"></span>
                                <h5>+ 5,000</h5>
                                <h6>Quotes Delivered</h6>
                            </li>
                            <li>
                                <span class="ion-android-happy"></span>
                                <h5>+ 3,000</h5>
                                <h6>Happy Brides</h6>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- Intro Summary -->
        <section class="intro-summary">
            <div class="row">
                <div class="small-12 columns">
                    <ul>
                        <li class="small-12 columns">
                            <span class="blissful-hex"></span>
                            <h3>GIVE YOUR REQUIREMENTS</h3>
                            <p>Tell us what you are looking for. Give as much detail as possible</p>
                        </li>
                        <li class="small-12 columns">
                            <span class="blissful-hex"></span>
                            <h3>GET CUSTOMIZED QUOTES</h3>
                            <p>Suppliers will review your request & send you proposals</p>
                        </li>
                        <li class="small-12 columns">
                            <span class="blissful-hex"></span>
                            <h3>HIRE THE BEST</h3>
                            <p>Compare proposals, shortlist and hire the best person</p>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- <div class="row">
            <div class="small-12 medium-10 small-centered columns ad-section">
                <a href="http://www.superbtent.com/" target="_blank">
                    <img class="hide-for-small-only" src="/img/SUPERB1-728X90.jpg">
                    <img class="show-for-small-only" src="/img/SUPERB-320x100.jpg">
                </a>
            </div>
        </div> -->
        <!--         <div class="small-12 small-centered columns breaker breaker-bottom" style="max-width:720px;">
                    <script type="text/javascript">
                        sas.render('30032');  // Format : 728X90 Leader Board Banner (AF) 728x90
                    </script>
                </div> -->
        <!-- how it works -->
        <section class="hiw">
            <img src="img/newhome/hiw-bkgd.jpg">
            <div class="row">
                <div class="small-12 columns">
                    <h3>How Blissful Works</h3>
                    <ul>
                        <li>
                            <img src="img/newhome/blissful-works-1.svg">
                            <h3>1</h3>
                            <h6>Post a Job</h6>
                            <p>Create a job and give as much detail as possible</p>
                        </li>
                        <li>
                            <img src="img/newhome/blissful-works-2.svg">
                            <h3>2</h3>
                            <h6>Receive Multiple Proposals</h6>
                            <p>Get multiple proposals from different suppliers within 24Hrs</p>
                        </li>
                        <li>
                            <img src="img/newhome/blissful-works-3.svg">
                            <h3>3</h3>
                            <h6>Hire A Supplier</h6>
                            <p>Compare the different proposals and select the best supplier for the job.</p>
                        </li>
                    </ul>
                </div>
                <div class="small-12 columns">
                    <a href="/jobs/create" class="button hiw-button">Get Started</a>
                </div>
            </div>

        </section>

        <!-- packages -->
        <section class="packages">
            <!-- <div class="row">
                <div class="small-12 medium-10 small-centered columns ad-section">
                    <a href="http://www.superbtent.com/" target="_blank">
                        <img class="hide-for-small-only" src="/img/SUPERB2-728X90.jpg">
                        <img class="show-for-small-only" src="/img/SUPERB-320x100.jpg">
                    </a>
                </div>
            </div> -->
            <div class="row">
                <div class="small-12 columns">
                    <h2>The Best Wedding Packages</h2>
                    <h3>Save time & money choose one of these ready packages for your big day</h3>
                    <ul>
                        @foreach($packages as $package)
                            <li class="ripple">
                                <a href="/shop/product/{{$package->product_slug}}">
                                    <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$package->picture}}">
                                    <div class="wrap">
                                        <h3 class="title">{{$package->name}}</h3>
                                        <p>{{ str_limit(preg_replace('/(<.*?>)|(&.*?;)/', '', $package->description),65) }}</p>
                                    </div>
                                </a>
                            </li>
                        @endforeach

                        @if(isset($package_category->category_slug))
                            <li class="ripple">
                                <a href="/packages/{{ $package_category->category_slug }}">
                                    <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$package_category->header_image}}">
                                    <div class="wrap">
                                        <h3 class="title">{{$package_category->name}}</h3>
                                        <p>{{ str_limit(preg_replace('/(<.*?>)|(&.*?;)/', '', $package_category->description),65) }}</p>
                                    </div>
                                </a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </section>

        <!-- Featured Products -->
        <section class="prd-ft">
            <div class="row">
                <div class="small-12 columns">
                    <h2>Put Together Your Perfect Look</h2>
                    <h3>We have the little things that will make your wedding stand out</h3>
                    <a href="/shop" class="button">Discover More</a>
                    <ul>
                        @foreach($products as $product)
                            <li class="ripple">
                                <a href="/shop/product/{{$product->product_slug}}">
                                    <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$product->picture}}">
                                    <div class="wrap">
                                        <h4>{{$product->name}}</h4>
                                        <h6>Ksh {{number_format($product->sale_price)}}</h6>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                        <li class="ripple ad">
                            <a href="/shop">
                                <img class="hide-for-small-only" src="img/newhome/ad.jpg">
                                <img class="show-for-small-only" src="img/newhome/ad-small.jpg">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

        </section>


        <!-- testimonials -->
        <section class="testimonials">
            <!-- <div class="row">
                <div class="small-12 medium-10 small-centered columns ad-section">
                    <a href="/vendors/chic-events">
                        <img class="hide-for-small-only" src="/img/featured/Chic Events Banner.png">
                        <img class="show-for-small-only" src="/img/featured/Chic Events Banner Small.png">
                    </a>
                </div>
            </div> -->
            <div class="row">
                <div class="small-12 columns">
                    <h2>What people are saying about Blissful</h2>
                    <!--  <h3>Rather number can and set praise.</h3> -->
                    <ul>
                        <li>
                            <img src="img/newhome/blissful-reviews.svg">
                            <p>“Cake Art did a beautiful rose pink and gold four tier cake for my wedding. The layout
                                they did for the cake table was beautiful with rose petals and romantic candle lighting.
                                The cake flavours were a big hit with all the guests! My only downside was the
                                significant transport cost charged to take the cake to Naivasha.”</p>
                            <h6>Nyambura on <a href="/vendors/cake-art">Cake Art</a></h6>
                        </li>
                        <li>
                            <img src="img/newhome/blissful-reviews.svg">
                            <p>“Lilly was one of the easiest suppliers to work with. To start with, she gave me great ideas
                                of how to come up with a colour scheme and how to execute it. We managed to work within
                                our budget to achieve a beautiful effect.”</p>
                            <h6>Maggie on <a href="/vendors/floral-art-events-design-centre">Floral Art Events Design
                                    Centre</a></h6>
                        </li>
                        <li>
                            <img src="img/newhome/blissful-reviews.svg">
                            <p>“Mwende was probably my favourite of all my suppliers! She was professional, did her work
                                on time and even threw in some great advice to a distressed bride to be! I worked with
                                her for both my ruracio and wedding and she did a stellar job! The cards were beautiful
                                and even when my guest numbers went up unexpectedly, she was willing to make extra cards
                                for me with no fuss!”</p>
                            <h6>Elaine on <a href="/vendors/handmade-treasures"> Handmade Treasures</a></h6>
                        </li>
                    </ul>
                </div>
            </div>
        </section>

        <!-- featured blogs -->
        <section class="ft-blogs">
            <div class="row">
                <div class="small-12 columns">
                    <h2>How Others Have Done It Best</h2>
                    <h3>View the latest from our blog</h3>
                    <ul>
                        @foreach($posts as $post)
                            <li class="ripple">
                                <a href="/blog/{{$post->slug}}">
                                    <img src="{{page_image($post->page_image)}}">
                                    <div class="wrap">
                                        <h4 class="title">{{$post->title}}</h4>
                                        <p>{{  str_limit(preg_replace('/(<.*?>)|(&.*?;)/', '', $post->content_raw),85) }}</p>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </section>

        <!-- browse vendors -->
        <section class="vendor-cat">
            <!-- <div class="row">
                <div class="small-12 medium-10 small-centered columns ad-section">
                    <a href="/vendors/chic-events">
                        <img class="hide-for-small-only" src="/img/featured/Chic Events Banner.png">
                        <img class="show-for-small-only" src="/img/featured/Chic Events Banner Small.png">
                    </a>
                </div>
            </div> -->
            <div class="row">
                <div class="small-12 columns">
                    <h2>Browse The Largest Supplier Directory In Kenya</h2>
                    <h3>View our selection of suppliers</h3>
                    <ul>
                        @foreach($classifications as $classification)
                            <li class="ripple">
                                <a href="/{{$classification->slug}}">
                                    <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke{{$classification->thumbnail_path}}">
                                    <div class="wrap" style="border-left:4px solid {{$classification->border_color}}">
                                        <h4>{{$classification->name}}</h4>
                                    </div>
                                </a>
                            </li>
                        @endforeach

                    </ul>
                    <!-- continue browsing actions -->
                    <div class="cont-browse">
                        <a href="#" class="button title">Get Quotes Now</a>
                    </div>
                </div>
            </div>
            <!-- <div class="row">
              <div class="small-10 medium-8 small-centered columns breaker">
                <a href="http://www.superbtent.com/" target="_blank">
                  <img class="hide-for-small-only" src="/img/SUPERB-728X90.jpg">
                  <img class="show-for-small-only" src="/img/SUPERB-320x100.jpg">
                </a>
              </div>
            </div> -->
            <!-- /189532048/Bridal_Gowns_Skyscraper_Bottom_Blissful_KE -->
            <!-- <div class="row">
                <div class="small-12 medium-10 small-centered columns ad-section">
                    <a href="http://www.superbtent.com/" target="_blank">
                        <img class="hide-for-small-only" src="/img/SUPERB1-728X90.jpg">
                        <img class="show-for-small-only" src="/img/SUPERB-320x100.jpg">
                    </a>
                </div>
            </div> -->
        </section>


    </main>
    @include('pages.pop-up')
    <!-- END OF MAIN SECTION -->
@endsection

@section('scripts')
    <script defer onLoad="startTyped()" type="text/javascript" src="/js/typed.min.js"></script>
    <script type="text/javascript">
        function startTyped() {
            $("#typed").typed({
                stringsElement: $('.typed-intro'),
                typeSpeed: 100,
                backDelay: 1500,
                loop: false,
            });
        }

    </script>
@endsection