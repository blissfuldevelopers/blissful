<!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  @yield('head')
    <title>Blissful | @yield('title')</title>
  <link rel="stylesheet" href="/css/foundation.css" />
  <link rel="stylesheet" href="/fonts/foundation/foundation-icons.css" />
   <link rel="stylesheet" type="text/css" href="{{ elixir('css/zurb-layout.css') }}">

  @yield('styles')
</head>
<body>
  <!-- START OF HEADER SECTION -->
  <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-50669449-1', 'auto');
      ga('send', 'pageview');

  </script>
  <header>
    <div class="row">
      <div class="small-12 columns">
        <nav class="top-bar" data-topbar role="navigation">
          <ul class="title-area">
            <li class="name">
              <h1><a href="{{ route('home')  }}"><img src="/img/blissful-logo.svg"></a></h1>
            </li>
             <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a href="#"><span>MENU</span></a></li>
          </ul>
          <section class="top-bar-section">
            <!-- Right Nav Section -->
            <ul id="right" class="right">
              @if(!Auth::check())
              <li class="hide-for-small-only"><a href="{{ route('auth.vendor-register') }}"><button class="ripple">List Your Business</button></a></li>
              <li><a class="ripple" href="#">Help</a></li>
              <li><a class="ripple" href="{{ route('auth.login') }}">Log In</a></li>
              @else
              <?php
              $count = Auth::user()->newMessagesCount();
              $cssClass = $count == 0 ? 'hide' : '';
              ?>
              <li class="pimg">
              <div class="breaker show-for-small-only"></div>
              <a class="ripple" data-dropdown="drop1" aria-controls="drop1" aria-expanded="false">
                <img src="/{{Auth::user()->profile->profilePic}}" alt="profile pic">  {{Auth::user()->first_name}}
              </a>
              <ul id="drop1" class="f-dropdown hide-for-small-only" data-dropdown-content aria-hidden="true" tabindex="5">
                <li><a href="
                @if( Auth::user()->hasRole('administrator'))
                  {{ route('admin.home') }}
                @elseif( Auth::user()->hasRole('vendor'))
                  {{ route('vendor.home') }}
                @elseif( Auth::user()->hasRole('user'))
                  {{ route('user.home') }}
                @endif
                "> <i class="fi-home"></i> Dashboard</a></li>
                <li><a href="@if( Auth::user()->hasRole('vendor'))
                            {{route('users.edit',array(Auth::user()->profile->user_id))}}
                            @else 
                            {{ url('profile/edit/'.Auth::user()->profile->user_id) }}
                            @endif 
                            "> <i class="fi-wrench large"></i> Profile</a></li>
                <li><a href="@if( Auth::user()->hasRole('administrator'))
                        /admin#inbox
                        @elseif( Auth::user()->hasRole('vendor'))
                        /vndor#inbox
                        @elseif( Auth::user()->hasRole('user'))
                        /user#inbox
                        @endif"> <i class="fi-mail"></i> Inbox <span class="alert {{$cssClass}} label round">{{$count}}</span></a></li>
                <li><a href="{{ route('authenticated.logout') }}"> <i class="fi-power" style="color:red;"></i> Logout</a></li>
              </ul>
              </li>
              <ul class="show-for-small-only" style="margin-left:20px;">
              <li><a href="{{ route('auth.vendor-register') }}">List Your Business</a></li>
              <li><a href="/login"> <i class="fi-home"></i> Dashboard</a></li>
              <li><a href=" @if( Auth::user()->hasRole('vendor'))
                            {{route('users.edit',array(Auth::user()->profile->user_id))}}
                            @else 
                            {{ url('profile/edit/'.Auth::user()->profile->user_id) }}
                            @endif 
                            "> <i class="fi-wrench large"></i> Profile</a></li>
              <li><a href="@if( Auth::user()->hasRole('administrator'))
                        /admin#inbox
                        @elseif( Auth::user()->hasRole('vendor'))
                        /vndor#inbox
                        @elseif( Auth::user()->hasRole('user'))
                        /user#inbox
                        @endif"> <i class="fi-mail"></i> Inbox <span class="alert {{$cssClass}} label round">{{$count}}</span></a></li>
              <li><a href="{{ route('authenticated.logout') }}"> <i class="fi-power" style="color:red;"></i> Logout</a></li>
              </ul>
              @endif
            </ul>
            <!-- Left Nav Section -->
            <ul class="left">
              <li>
                <form action="/search" method="GET" enctype="multipart/form-data">
                  <div class="row warning">
                    <input name="find" id="find" type="text" class="@if(Session::has('searchErrors')) red_border @endif" placeholder="@if(Session::has('searchErrors'))Search not found @else Find a Vendor @endif" />
                  </div>
                </form>
              </li>
            </ul>
          </section>
        </nav>
      </div>
    </div>
  </header>
  <!-- END OF HEADER SECTION -->

  @yield('content')

    <!-- START OF FOOTER SECTION -->
  <footer>
    <div class="tft">
      <div class="row">
        <div class="small-12 medium-6 large-6 columns">
          <div class="row">
            <div class="small-6 columns">
              <h5>COMPANY</h5>
              <ul>
                <li><a href="/about">About Us</a></li>
                <li><a href="/contact">Contact Us</a></li>
                <li><a href="{{ route('blog') }}">Blog</a></li>
              </ul>
            </div>
            <div class="small-6 columns">
              <h5>PARTNERS</h5>
              <ul>
                <li><a href="{{ route('auth.vendor-register') }}">List Your Business</a></li>
                <li><a href="">Advertise</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="small-12 medium-6 large-6 columns">
          <div class="row">
            <div class="small-12 columns">
              <ul class="socials">
                <li class="f"><a href="https://www.facebook.com/blissfulKE"></a></li>
                <li class="t"><a href="https://twitter.com/blissfulKE"></a></li>
                <li class="g"><a href="https://plus.google.com/112646574144413915809/posts"></a></li>
                <li class="p"><a href="http://www.pinterest.com/blissfulKE"></a></li>
              </ul>
              <ul class="right">
                <li><a href="/privacy-policy">Privacy Policy</a></li>
                <li><a href="/terms-and-conditions">Terms and conditions</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="bft">
      <div class="row">
        <div class="small-12 columns">
          <span>© 2015 Blissful</span>
        </div>
      </div>
    </div>
  </footer>
  <!-- END OF FOOTER SECTION -->
  <script src="{{ elixir('js/zurb-layout.js') }}"></script>
  <script src="/js/views.js"></script>
  @yield('scripts')
</body>
</html>

