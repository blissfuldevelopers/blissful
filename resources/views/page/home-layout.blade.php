<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @yield('metas')
    @yield('head')
    <title>@yield('title') | Blissful</title>
    <link rel="icon" type="image/png" href="https://s3.eu-central-1.amazonaws.com/blissful-ke/img/dashboard/favicon.ico">
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/home-scripts.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/bliss-custom.css') }}">
        <!-- google ads -->
    <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
    <script>
        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];
    </script>

    <script>
        googletag.cmd.push(function () {
            googletag.defineSlot('/189532048/leaderboard_top_home', [728, 90], 'div-gpt-ad-1474450779427-0').addService(googletag.pubads());
            googletag.defineSlot('/189532048/Square_1', [300, 250], 'div-gpt-ad-1474450779427-1').addService(googletag.pubads());
            googletag.defineSlot('/189532048/Half_Page_1', [300, 600], 'div-gpt-ad-1474450779427-2').addService(googletag.pubads());
            googletag.defineSlot('/189532048/Mobile_Leaderboard_Top', [320, 50], 'div-gpt-ad-1474450779427-3').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
    </script>
    @yield('styles')

  </head>
  <body>
    <!-- masthead -->
    <div class="{{isset($header_class)? $header_class : 'masthead'}}">
        <!-- main navigation -->
        <nav>
          <div class="row">
            <div class="medium-12 columns">
              <div class="small-6 medium-4 columns">
                <!-- logo -->
                <h1 class="logo">
                  <a href="/">
                    <img src="/img/blissful-logo-w.svg">
                  </a>
                </h1>
              </div>
              <div class="hide-for-small-only medium-5 columns">
                <form action="/search" method="GET" enctype="multipart/form-data" class="search-form">
                    <input name="find" id="find" type="text" class="search-input @if(Session::has('searchErrors')) red_border @endif" placeholder="@if(Session::has('searchErrors'))Search not found @else Find a Vendor @endif"/>
                </form>
              </div>
              <div class="small-6 medium-3 columns">
                <a href="/shop/cart" class="show-cart">
                  <span class="shopping-cart"><i class="fi-shopping-cart"></i></span>
                  <span id="cart-count" class="cart-count badge">0</span>
                </a>
                <!-- nav links -->
                @if(Auth::check())
                <ul class="dropdown menu hide-for-small-only" data-dropdown-menu>
                  <li class="header-dropdown">
                    <a href="#">
                      <img class="round" src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{Auth::user()->profile->profilePic}}"> {{Auth::user()->first_name}}
                    </a>
                    <ul class="menu">
                      <li><a href="/dashboard">Dashboard</a></li>
                      <li><a href="{{route('users.show',[Auth::user()->id])}}">Profile</a></li>
                      <li><a href="/logout">Logout</a></li>
                      <!-- ... -->
                    </ul>
                  </li>
                </ul>
                <a id="mobile-menu-open" class="mobile-menu-open show-for-small-only">
                  <i class="fi-list large"></i>
                </a>
                @else
                <a id="mobile-menu-open" class="mobile-menu-open show-for-small-only">
                  <i class="fi-list large"></i>
                </a>
                <ul class="hide-for-small-only">
                  <li><a href="/register/user-type">Sign Up</a></li>
                  <li><a href="/login">Log In</a></li>
                </ul>
                @endif
                
              </div>
            </div>
          </div>
        </nav>
        @yield('header-content')
    </div>
    @yield('content')
    <div class="mobile-nav-back" id="mobile-bg"></div>
    <div class="mobile-nav" id="mobile-menu">
      <a href="#" id="mobile-menu-close" class="menu-close"><i class="fi-x"></i></a>
      <div class="small-12 columns">
        @if(Auth::check())
        <div class="small-12 columns text-center">
          <img class="circle" src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{Auth::user()->profile->profilePic}}"> 
          <p>{{Auth::user()->first_name}} {{Auth::user()->last_name}}</p>
        </div>
        <div class="show-for-small-only small-12 columns">
          <form action="/search" method="GET" enctype="multipart/form-data" class="search-form">
              <input name="find" id="find" type="text" class="search-input @if(Session::has('searchErrors')) red_border @endif" placeholder="@if(Session::has('searchErrors'))Search not found @else Find a Vendor @endif"/>
          </form>
        </div>
        <div class="small-12 columns">
          <ul>
            <li><a href="/dashboard">Dashboard</a></li>
            <li><a href="{{route('users.show',[Auth::user()->id])}}">Profile</a></li>
            <li><a href="/logout">Logout</a></li>
            <!-- ... -->
          </ul>
        </div>
        
        @else
        <div class="small-12 columns text-center">
          <img src="/img/badge-inverted.svg">
        </div>
        <div class="show-for-small-only small-12 columns">
          <form action="/search" method="GET" enctype="multipart/form-data" class="search-form">
              <input name="find" id="find" type="text" class="search-input @if(Session::has('searchErrors')) red_border @endif" placeholder="@if(Session::has('searchErrors'))Search not found @else Find a Vendor @endif"/>
          </form>
        </div>
        <ul>
          <li><a href="/register/user-type">Sign Up</a></li>
          <li><a href="/login">Log In</a></li>
        </ul>
        @endif
      </div>
    </div>
    <!-- footer -->
    <footer>
      <div class="f1">
        <div class="row">
          <div class="medium-3 columns hide-for-small-only">
            <img src="/img/blissful-logo-w.svg" class="f-logo">
          </div>
          <div class="medium-5 small-12 columns">
            <div class="row">
              <div class="small-6 columns">
                <ul>
                  <li><a href="/about">About</a></li>
                  <!-- <li><a href="/how-it-works">How It Works</a></li> -->
                  <li><a href="/contact">Contact Us</a></li>
                  <li><a href="/terms-and-conditions">Terms & Conditions</a></li>
                  <li><a href="/privacy-policy">Privacy Policy</a></li>
                </ul>
              </div>
              <div class="small-6 columns">
                <ul><!-- 
                  <li>Jobs</li>
                  <li>Messages</li>
                  <li>Bids</li>
                  <li>Credits</li> -->
                  <li><a href="/blog">Blog</a></li>
                  <li><a href="/shop">Shop</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="medium-4 small-12 columns">
            <ul class="socials">
              <li>
                <a target="_blank" href="https://www.facebook.com/blissfulKE">
                  <i class="fa fa-facebook" aria-hidden="true"></i>
                </a>
              </li>
              <li>
                <a target="_blank" href="https://twitter.com/blissfulKE">
                  <i class="fa fa-twitter" aria-hidden="true"></i>
                </a>
              </li>
              <li>
                <a target="_blank" href="https://www.instagram.com/blissful_ke/">
                  <i class="fa fa-instagram" aria-hidden="true"></i>
                </a>
              </li>
              <li>
                <a target="_blank" href="https://www.pinterest.com/blissfulKE">
                  <i class="fa fa-pinterest" aria-hidden="true"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="f2">
        <div class="row">
          <div class="small-12 columns">
            <p>©2017 Copyright</p>
          </div>
        </div>
      </div>
    </footer>
    <!--Start of Crisp Live Chat Script-->
<!--     <script type="text/javascript">
            
      CRISP_WEBSITE_ID = "aeb94ef1-11bf-4618-85b7-dce7f0ee2b92";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.im/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();
    </script> -->

    <!--End of Crisp Live Chat Script-->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-50669449-1', 'auto');
        ga('send', 'pageview');

    </script>
    <script type="text/javascript">
        adroll_adv_id = "TH4Q5IVG5FHXNNPGFG2JBA";
        adroll_pix_id = "342MRCCGYRABZFOVG5SIPM";
        /* OPTIONAL: provide email to improve user identification */
        @if(Auth::check())
                adroll_email = "{{Auth::user()->email}}";
        @endif
        (function () {
            var _onload = function(){
                if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
                if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
                var scr = document.createElement("script");
                var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
                scr.setAttribute('async', 'true');
                scr.type = "text/javascript";
                scr.src = host + "/j/roundtrip.js";
                ((document.getElementsByTagName('head') || [null])[0] ||
                    document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
            };
            if (window.addEventListener) {window.addEventListener('load', _onload, false);}
            else {window.attachEvent('onload', _onload)}
        }());
    </script>
    <script defer src="{{ elixir('js/home-scripts.js') }}" onLoad="load_page_scripts();" ></script>
    @yield('scripts')

  </body>
</html>
