@extends('page.home-layout')
@section('title')
Event Planning in Kenya - Fast & Reliable
@endsection
@section('header-content')
<!-- intro -->
<div class="homep-intro">
  <div class="row">
    <div class="medium-10 large-6 columns">
      <h2>Best Suppliers. Incredible Prices</h2>
      <h6>Find the best suppliers for all your events</h6>
      <form action="/jobs/create" method="GET" class="home-q">
          <select name="option">
              <option value="" disabled selected>Get me quotes for...</option>
              @foreach($classifications as $classification)
                  <option data-slug="{{$classification->slug}}" value="{{$classification->id}}">{{$classification->name}}</option>
              @endforeach
          </select>
          <input type="submit" class="button" value="Find Vendor">
      </form>
    </div>
  </div>
</div>

<!-- services list -->
<div class="homep-serv hide-for-small-only">
  <div class="row">
    <div class="medium-12 columns">
      <ul>

        <li>
          <i class="fa fa-university"></i>
          <h6>Venues</h6>
        </li>
        <li>
          <i class="fa fa-camera-retro " aria-hidden="true"></i>
          <h6>Photography</h6>
        </li>
        <li>
          <i class="fa fa-columns" aria-hidden="true"></i>
          <h6>Event Set Up</h6>
        </li>
        <li>
          <i class="fa fa-music" aria-hidden="true"></i>
          <h6>Entertainers</h6>
        </li>
      </ul>
    </div>
  </div>
</div>
@endsection
@section('content')
<!-- how it works -->
<div class="hiw">
  <div class="row">
    <div class="small-12 columns">
      <h3>How Blissful Works</h3>
      <ul>
        <li class="medium-4 small-12 columns">
          <h4 class="hide-for-small-only">1</h4>
          <img src="img/briefcase.svg">
          <div class="wrap">
            <h6>Post a Job</h6>
            <p>Create a job and give as much detail as possible</p>
          </div>
        </li>
        <li class="medium-4 small-12 columns">
          <h4 class="hide-for-small-only">2</h4>
          <img src="img/quotes.svg">
          <div class="wrap">
            <h6>Get Multiple Quotes</h6>
            <p>Get multiple quotes from different vendors within 24Hrs</p>
          </div>
        </li>
        <li class="medium-4 small-12 columns">
          <h4 class="hide-for-small-only">3</h4>
          <img src="img/hire.svg">
          <div class="wrap">
            <h6>Hire Vendor</h6>
            <p>Compare the different quotes and select the best vendor for the job.</p>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- Adsense Section 1-->
<div class="row">
    <div class="small-12 columns breaker breaker_bottom">
        <div class="hide-for-small-only">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- Blissful_responsive -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-2847929148248604"
                 data-ad-slot="1315972373"
                 data-ad-format="auto"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
        <div class="show-for-small-only">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- mobile_horizontal banner -->
            <ins class="adsbygoogle"
                 style="display:inline-block;width:320px;height:100px"
                 data-ad-client="ca-pub-2847929148248604"
                 data-ad-slot="2792705573"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
    </div>
</div>
<div class="row">
  <div class="small-12 medium-10 small-centered columns home-section">
    <h5>Top Garden Wedding Venues.</h5>
    <div class="home-slider">
      @foreach($vendors as $vendor)
      <div class="card">
        <a href="/vendor/{{$vendor->slug}}">
          <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$vendor->profile->profilePic}}">
          <div class="card-section">
            <h6>{{$vendor->first_name}}</h6>
          </div>
        </a>
      </div>
      @endforeach
    </div>
  </div>
</div>
<div class="row">
  <div class="small-12 medium-10 small-centered columns home-section">
    <h5>Planning a wedding?</h5>
    <div class="home-slider">
      <div class="card">
        <a href="/jobs/create?option=8">        
          <img src="/img/categories/blissful_lens_01_1.jpg">
          <div class="card-section">
            <h6>Find a Photographer</h6>
          </div>
        </a>
      </div>
      <div class="card">
        <a href="/venue"> 
          <img src="/img/categories/blissful_venue_02.jpg">
          <div class="card-section">
            <h6>Book a Venue</h6>
          </div>
        </a>
      </div>
      <div class="card">
        <a href="/jobs/create?option=6"> 
          <img src="/img/categories/blissful_food_02.jpg">
          <div class="card-section">
            <h6>Plan the Perfect Set Up</h6>
          </div>
        </a>
      </div>
      <div class="card">
        <a href="/jobs/create?option=15"> 
          <img src="/img/categories/5748117b05fb5transport.gif">
          <div class="card-section">
            <h6>Hire a Limo</h6>
          </div>
        </a>
      </div>
      <div class="card">
        <a href="/jobs/create?option=2"> 
          <img src="/img/categories/blissful_food_02.jpg">
          <div class="card-section">
            <h6>Find the Perfect Caterer</h6>
          </div>
        </a>
      </div>
    </div>
  </div>
</div>
<!-- stats -->
<div class="homep-stats">
  <div class="row">
    <div class="small-12 columns">
      <ul>
        <li class="medium-6 small-12 columns">
          <i class="fa fa-user-o" aria-hidden="true"></i>
          <p>More than 5,000 people registered on the website</p>
        </li>
        <li class="medium-6 small-12 columns">
          <i class="fa fa-credit-card" aria-hidden="true"></i>
          <p>Thousands of professionals have grown their businesses</p>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- Adsense Section 1-->
<div class="row">
    <div class="small-12 columns breaker breaker_bottom">
        <div class="hide-for-small-only">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- Blissful_responsive -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-2847929148248604"
                 data-ad-slot="1315972373"
                 data-ad-format="auto"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
        <div class="show-for-small-only">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- mobile_horizontal banner -->
            <ins class="adsbygoogle"
                 style="display:inline-block;width:320px;height:100px"
                 data-ad-client="ca-pub-2847929148248604"
                 data-ad-slot="2792705573"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
    </div>
</div>
<!-- video testimonials -->
<div class="homep-videos">
  <div class="row">
    <div class="small-12 columns">
      <h3>Word from Vendors</h3>
      <ul>
        @foreach($top_vendors as $vendor)
        <li class="medium-4 small-12 columns vendor-card">
        <a href="/vendor/{{$vendor->slug}}"> <span></span></a>
          <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$vendor->profile->profilePic}}">
          <div class="wrap">
            <h6>{{$vendor->first_name}}</h6>
            <h5>{{$vendor->classifications->first()->name}}</h5>
          </div>
        </li>
        @endforeach
        <a href="/vendor-register" class="button">Register Your Business</a>
      </ul>
    </div>
  </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
  function load_page_scripts(){
    $('.home-slider').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            centerMode: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            variableWidth: true
          }
        },
        {
          breakpoint: 480,
          settings: {
            arrows: false,
            slidesToShow: 2,
            slidesToScroll: 1,
            variableWidth: true
          }
        }
      ]
    });

    $('form.home-q .button').on('click', function(e){
      e.preventDefault();
      var val = $('form.home-q select').find(':selected').attr('data-slug');

      if (val == "venue") {
        window.location = "/venue";
      }
      else{
        $('form.home-q').submit();
      }
    });
  }
</script>
@endsection