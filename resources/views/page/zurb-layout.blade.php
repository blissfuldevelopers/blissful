<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="google-site-verification" content="CWchMzIhhA-tzl95fNy1tt7CQuXqMofK3jgYHZaW2KI" />
    @yield('metas')
    <script data-cfasync="false" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <link rel="manifest" href="/manifest.json">
    @yield('head')
    <title>@yield('title') | Blissful</title>
    
    <!-- google ads --> 
    <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
    <script>
      var googletag = googletag || {};
      googletag.cmd = googletag.cmd || [];
    </script>

    <script>
      googletag.cmd.push(function() {
        googletag.defineSlot('/189532048/leaderboard_top_home', [728, 90], 'div-gpt-ad-1474451309695-0').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
      });
    </script>

    
    <link rel="icon" type="image/png" href="/img/dashboard/favicon.ico"/>
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"/>
    <link rel="stylesheet" href="/css/foundation.css"/>
    <link rel="stylesheet" href="/fonts/foundation/foundation-icons.css"/>
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/zurb-layout.css') }}">
    <link href="/app/client/css/lobibox.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/css/newhome.css"/>
    @yield('styles')

    <script data-cfasync="false">
        if (!window.jQuery) {
            // Offline JS
            document.write('<script data-cfasync="false" src="https://code.jquery.com/jquery-2.1.1.min.js"><\/script>');
        }
    </script>
    <script src="/app/client/js/foundation.min.js" type="text/javascript"></script>


    <!--End of Crisp Live Chat Script-->

</head>
<body class="blissful-home">
<!-- START OF HEADER SECTION -->

<header>
    <div class="top-header">
        <div class="row">
            <div class="small-12">
                <nav class="top-bar" data-topbar role="navigation">
                    <ul class="title-area" style="padding-top:0;">
                        <li class="name">
                            <h1><a href="{{ route('home')  }}"><img style="width:166px;"
                                                                    src="https://s3.eu-central-1.amazonaws.com/blissful-ke/img/blissful-logo.png"></a>
                            </h1>
                        </li>
                        <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                        <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
                    </ul>
                    <section class="top-bar-section">
                        <!-- mobile visible search -->
                        <ul class="left show-for-small-only">
                            <li class="mobile-shop-search">
                                <form action="/search" method="GET" enctype="multipart/form-data">
                                    <div class="row warning">
                                        <input name="find" id="find" type="text"
                                               class="@if(Session::has('searchErrors')) red_border @endif"
                                               placeholder="@if(Session::has('searchErrors'))Search not found @else Find a Vendor @endif"/>
                                    </div>
                                </form>
                            </li>
                        </ul>
                        <!-- mobile visible shop main nav -->
                        @if(@$categories)
                        <ul class="shop-nav show-for-small-only">
                            <li><a href="/shop">All</a></li>
                            @foreach($categories as $data)
                                @if(@$cat->id == $data['id'])
                                    <li class="active"><a href="/shop/category/{{$data['category_slug']}}">{{$data['name']}}</a> </li>
                                @else
                                    <li><a href="/shop/category/{{$data['category_slug']}}">{{$data['name']}}</a></li>
                                @endif
                            @endforeach
                        </ul>
                        @endif
                        <!-- Right Nav Section -->
                        <ul id="right" class="right">
                            <li><a href="/shop/cart" class="showCart" style="display:none; position: relative">
                                    <h3><i class="fi-shopping-cart"></i></h3>
                                    <span class="badge" id="CartCount">0</span>
                                </a>
                            </li>
                            @if(!Auth::check())
                                <li class="hide-for-small-only"><a href="{{ route('auth.user-type') }}">
                                        <button class="ripple"
                                                onclick="ga('send', 'event', 'button', 'click', 'join us');">
                                            Join Us
                                        </button>
                                    </a></li>
                                <div class="breaker show-for-small-only"></div>
                                <li class="show-for-small-only"
                                    style="background-color: #00A89B!important;font-size:15px;">
                                    <a class="error-center white" href="{{ route('auth.user-type') }}"
                                       onclick="ga('send', 'event', 'button', 'click', 'join us');">Join Us</a></li>
                                <li><a class=" error-center ripple"
                                       onclick="ga('send', 'event', 'button', 'click', 'help');" href="#">Help</a></li>
                                <li><a class=" error-center ripple" href="{{ route('auth.login') }}">Log In</a></li>
                            @else
                                <?php
                                $count = count(Auth::user()->threadsWithNewMessages());
                                $cssClass = $count == 0 ? 'hide' : '';
                                ?>
                                <li class="pimg hide-for-small-only">
                                    <div class="breaker show-for-small-only"></div>
                                    <a class="ripple" data-dropdown = "profiledrop" aria-controls="profiledrop" aria-expanded="false">
                                        <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{Auth::user()->profile->profilePic}}"
                                             alt="profile pic"> {{Auth::user()->first_name}}
                                    </a>
                                    <ul id="profiledrop" class="f-dropdown hide-for-small-only" data-dropdown-content aria-hidden="true" tabindex="5">
                                        <li>
                                            <a href="/dashboard" onclick="ga('send', 'event', 'button', 'click', 'dropdown-dashboard');">

                                                <i class="fi-home"></i> Dashboard

                                            </a>
                                        </li>
                                        <li>
                                            <a href="/dashboard"> <i class="fi-mail"></i> Inbox 
                                                <span class="alert {{$cssClass}} label zurb-label round">{{$count}}</span>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="/dashboard">
                                                <i class="fi-shopping-bag"></i> Jobs
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('authenticated.logout') }}"> 
                                                <i class="fi-power" style="color:red;"></i> Logout
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <ul class="show-for-small-only" style="margin-left:20px;">
                                    <li><a href="/dashboard"> <i class="fi-home"></i> Dashboard</a></li>
                                <!-- <li><a href=" @if( Auth::user()->hasRole('vendor'))
                                    {{route('users.edit',array(Auth::user()->profile->user_id))}}
                                @else
                                    {{ url('profile/edit/'.Auth::user()->profile->user_id) }}
                                @endif
                                        " onclick="ga('send', 'event', 'button', 'click', 'dropdown-profile');"> <i class="fi-wrench large"></i> Profile</a></li> -->
                                    <li><a href="/dashboard"> <i class="fi-mail"></i> Inbox <span
                                                    class="alert {{$cssClass}} label zurb-label round">{{$count}}</span></a>
                                    </li>
                                    <li><a href="{{ route('authenticated.logout') }}"> <i class="fi-power"
                                                                                          style="color:red;"></i> Logout</a>
                                    </li>
                                </ul>
                            @endif
                        </ul>
                        <!-- Left Nav Section -->
                        <ul class="left hide-for-small-only">
                            <li>
                                <form action="/search" method="GET" enctype="multipart/form-data">
                                    <div class="row warning">
                                        <input name="find" id="find" type="text"
                                               class="@if(Session::has('searchErrors')) red_border @endif"
                                               placeholder="@if(Session::has('searchErrors'))Search not found @else Find a Vendor @endif"/>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </section>
                </nav>
            </div>
        </div>
    </div>
    <!-- start Mixpanel -->
    <script type="text/javascript">(function (e, b) {
            if (!b.__SV) {
                var a, f, i, g;
                window.mixpanel = b;
                b._i = [];
                b.init = function (a, e, d) {
                    function f(b, h) {
                        var a = h.split(".");
                        2 == a.length && (b = b[a[0]], h = a[1]);
                        b[h] = function () {
                            b.push([h].concat(Array.prototype.slice.call(arguments, 0)))
                        }
                    }

                    var c = b;
                    "undefined" !== typeof d ? c = b[d] = [] : d = "mixpanel";
                    c.people = c.people || [];
                    c.toString = function (b) {
                        var a = "mixpanel";
                        "mixpanel" !== d && (a += "." + d);
                        b || (a += " (stub)");
                        return a
                    };
                    c.people.toString = function () {
                        return c.toString(1) + ".people (stub)"
                    };
                    i = "disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
                    for (g = 0; g < i.length; g++)f(c, i[g]);
                    b._i.push([a, e, d])
                };
                b.__SV = 1.2;
                a = e.createElement("script");
                a.type = "text/javascript";
                a.async = !0;
                a.src = "undefined" !== typeof MIXPANEL_CUSTOM_LIB_URL ? MIXPANEL_CUSTOM_LIB_URL : "file:" === e.location.protocol && "//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//) ? "https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js" : "//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";
                f = e.getElementsByTagName("script")[0];
                f.parentNode.insertBefore(a, f)
            }
        })(document, window.mixpanel || []);
        mixpanel.init("5fcb45268f36faa1be9b3ce262e6e996 ");</script><!-- end Mixpanel -->
</header>
<!-- END OF HEADER SECTION -->
@include('page.partials.feedback')
@yield('content')

        <!-- START OF FOOTER SECTION -->
    
<footer>
    <div class="tft">
        <div class="row">
            <div class="small-12 medium-6 large-6 columns">
                <div class="row">
                    <div class="small-6 columns">
                        <h5>COMPANY</h5>
                        <ul>
                            <li><a href="/about">About Us</a></li>
                            <li><a href="/contact">Contact Us</a></li>
                            <li><a href="{{ route('blog') }}">Blog</a></li>
                        </ul>
                    </div>
                    <div class="small-6 columns">
                        <h5>PARTNERS</h5>
                        <ul>
                            <li><a href="{{ route('auth.user-type') }}">Join Us</a></li>
                            <li><a href="">Advertise</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="small-12 medium-6 large-6 columns">
                <div class="row">
                    <div class="small-12 columns">
                        <ul class="socials">
                            <li class="f"><a href="https://www.facebook.com/blissfulKE"></a></li>
                            <li class="t"><a href="https://twitter.com/blissfulKE"></a></li>
                            <li class="g"><a href="https://plus.google.com/112646574144413915809/posts"></a></li>
                            <li class="p"><a href="https://www.pinterest.com/blissfulKE"></a></li>
                        </ul>
                        <ul class="right">
                            <li><a href="/privacy-policy">Privacy Policy</a></li>
                            <li><a href="/terms-and-conditions">Terms and conditions</a></li>
                            <li><span class="fi-telephone tiny white"></span> <span
                                        class="white ns tiny"> 0775 987 555</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bft">
        <div class="row">
            <div class="small-12 columns">
                <span>© <?php echo date("Y"); ?> Blissful</span>
            </div>
        </div>
    </div>
</footer>
<!-- END OF FOOTER SECTION -->
<script src="/js/zurb-layout.js"></script>
<script src="/js/views.js"></script>
@include('page.partials.onesignal-script')
@yield('scripts')
<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer ></script>

<script type="text/javascript">
    var CaptchaCallback = function() {
        console.log('called')
        grecaptcha.render('formCaptcha', {'sitekey' : '{{ env('RE_CAP_SITE') }}'});
        grecaptcha.render('feedbackCaptcha', {'sitekey' : '{{ env('RE_CAP_SITE') }}'});
    }; 
</script>
<script type="text/javascript">
    $(document).ready(function () {
        //correct header height
        if ($(window).width() < 739) {
            $(".parallax-mirror").css({top: "70px !important"});
        }
    });


</script>

<script src="/app/client/js/shop.js" type="text/javascript"></script>
<script src="/app/client/js/notifications.js" type="text/javascript"></script>
<script src="/app/client/js/Lobibox.js" type="text/javascript"></script>
<script>

    function notification(type, msg, dely) {
        // 'warning', 'info', 'success', 'error'
        Lobibox.notify(type, {
            size: 'normal', // normal, mini, large
            icon: false,
            closable: true,
            sound: false,
            position: "top right",
            delay: dely || 5000,
            msg: msg
        });
    }

    function cartCount() {
        $.ajax({
            url: '/shop/cart-count',
            type: 'GET',
            success: function (data) {
                console.log(data);
                if (data > 0) {
                    $(".showCart").show();
                }
                $("#CartCount").text(data);
            },
            error: function (e) {
            }
        });
    }

    cartCount();

</script>
<script type="text/javascript">
    $(window).load(function () {
        $("#spopup").show(700);
    });
    function closeSPopup() {
        $('#spopup').hide(700);
    }
</script>
<script type="text/javascript">
    $(".placard").click(function () {
        window.location = $(this).find("a").attr("href");
        return false;
    });
</script>
<script type="text/javascript">
    $(".card-box").click(function () {
        window.location = $(this).find("a").attr("href");
        return false;
    });
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-50669449-1', 'auto');
    ga('send', 'pageview');

</script>
<script type="text/javascript">
    adroll_adv_id = "TH4Q5IVG5FHXNNPGFG2JBA";
    adroll_pix_id = "342MRCCGYRABZFOVG5SIPM";
    /* OPTIONAL: provide email to improve user identification */
    @if(Auth::check())
            adroll_email = "{{Auth::user()->email}}";
    @endif
    (function () {
        var _onload = function(){
            if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
            if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
                document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
        };
        if (window.addEventListener) {window.addEventListener('load', _onload, false);}
        else {window.attachEvent('onload', _onload)}
    }());
</script>
</body>
</html>