<h1 class="pull-left">CronTasks</h1>
<a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('cron_tasks.create') !!}">Add New</a>

<div class="clearfix"></div>

@include('flash::message')

<div class="clearfix"></div>
<div class="container">
    <div class="row">
        <div class="small-12">
            @include('cron_tasks.table')
        </div>
    </div>
</div>
