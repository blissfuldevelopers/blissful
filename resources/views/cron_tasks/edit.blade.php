<div class="row">
    <div class="col-sm-12">
        <h1 class="pull-left">Edit CronTask</h1>
    </div>
</div>

@include('core-templates::common.errors')

<div class="row">
    {!! Form::model($cronTask, ['route' => ['cron_tasks.update', $cronTask->id], 'method' => 'patch']) !!}

    @include('cron_tasks.fields')

    {!! Form::close() !!}
</div>
