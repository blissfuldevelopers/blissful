<div class="row">
    <div class="col-sm-12">
        <h1 class="pull-left">Create New CronTask</h1>
    </div>
</div>

@include('core-templates::common.errors')

<div class="row">
    {!! Form::open(['route' => 'cron_tasks.store']) !!}

    @include('cron_tasks.fields')

    {!! Form::close() !!}
</div>
