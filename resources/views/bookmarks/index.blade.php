@foreach($bookmarks as $bookmark)
<tr>
  <td class="ven">{{$bookmark->vendor->first_name}} <span class="loc">{{isset($bookmark->vendor->location->address)? $bookmark->vendor->location->address : $bookmark->vendor->location}}</span></td>
  <td class="quo">
    <a href="{{route('vendor.show',[$bookmark->vendor->slug])}}" class="button">View Vendor</a>
    <a data-token="{{ csrf_token() }}" data-href="{{route('bookmarks.destroy',$bookmark->id)}}" class="delete-bookmark float-right"><i class="fi-trash"></i></a>
  </td>
</tr>
@endforeach
@if($bookmarks->count() == 0)
<tr>
  <td><p class="text-center">No bookmarks saved</p></td>
  <td> </td>
</tr>
@endif