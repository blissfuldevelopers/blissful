@extends('page.shop-layout')

@section('title')
    {{ @$cat->name }}
@endsection
@section('styles')
    <link href="/css/hipster_cards.css" rel="stylesheet"/>
    <link href="/css/pe-icon-7-stroke.css" rel="stylesheet"/>
    
    @endsection

    @section('content')

    <section class="masthead">
        <div class="row">
          <div class="small-12 columns">
            <div class="mainft hm">    
             <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{@$cat->header_image}}">
              <div class="package-middle">
                <h2>{{ @$cat->header_text }}</h2>
                <h4>{{ @$cat->sub_header_text }}</h4>
                <a id="scrollr" href="#" class="button">VIEW PACKAGES</a>
              </div>
            </div>
          </div>
        </div>
    </section>

    <!-- END OF HOMEPG MASTHEAD -->

    <!-- START OF MAIN SECTION -->
    <main aria-role="main" id="shop-main">
        <div class="row">
            <div class="small-12 columns">
                <h3>Explore fantastic {{ @$cat->header_text }} </h3> 
                <div class="package-desc">
                  {!!@$cat->description !!}
                </div> 
                

                @if(count($products))
                  <ul class="shop-items">
                      @foreach($products as $product)
                          <li class="ripple">
                              <a href="/shop/product/{{$product->product_slug}}">
                                  <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{ $product->picture }}" style="height: 220px;"/>
                                  <div class="wrap">
                                      <h4 id="v-name">{{ $product->name }}</h4>
                                      @if( $product->sale_price && $product->sale_price !== $product->price )
                                          <h6>
                                            From: {{ $product->sale_price }}
                                          <h6>
                                      @else
                                          <h6>
                                            From: {{ $product->price }}
                                          </h6>
                                      @endif                            
                                  </div>
                              </a>
                          </li>
                      @endforeach
                  </ul>
              @else
                  <h4>No products found</h4>
              @endif
            </div>
        </div>
    </main>

    <!-- SHOP HOME META CONTENT SECTION -->
    <section class="shop-meta">
      <div class="row">
        <div class="small-12 columns">
          <p>From your engagement to the bachelorette, there's so much to celebrate before the big day. Make each party even more special with personal touches. Best party decor in Kenya.</p>
          <p>Give guests wedding favors that's beautiful, memorable and personalized to suit your style. The very best wedding favors in Kenya that are unique and can be personalized.</p>
          <p>Want to make your wedding stand out, grab our DIY decor kits. From glass jars (mason jars), ribbons, spray on paint, spray on adhesive, fabrics, stencils and more now available in Kenya.</p>
        </div>
      </div>
    </section>
@endsection

@section('scripts')
    <script src="/js/hipster-cards.js"></script>
    <script type="text/javascript">
        $('.filter-icon a').click(function () {
            $(this).parent().parent().parent().parent().css('height', 'initial');
            console.log('clicked');
        });
    </script>
   
    <script type="text/javascript">
        $("#scrollr").click(function () {
            var offset = 10; //Offset of 20px

            $('html, body').animate({
                scrollTop: $("#shop-main").offset().top + offset
            }, 1600);
        });
    </script>
@endsection