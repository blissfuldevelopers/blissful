<!DOCTYPE html>
<html><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<title>Wedding Photography in Kenya | Blissful </title>
    
    <!--pageMeta-->

    <!-- Loading Bootstrap -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
	
    <!-- Loading Elements Styles -->   
    <link href="/assets/css/landing-page.css" rel="stylesheet">
	
	<!-- Loading Magnific-Popup Styles --> 
    <link href="/assets/css/magnific-popup.css" rel="stylesheet"> 
	   
    <!-- Loading Font Styles -->
    <link href="/assets/css/iconfont-style.css" rel="stylesheet">

    <!-- WOW Animate-->
    <link href="/assets/js/animations/animate.css" rel="stylesheet">

	<!-- Favicons -->
	<link rel="icon" type="image/png" href="/img/dashboard/favicon.ico"/>
	<link rel="apple-touch-icon" href="/assets/img/favicons/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/assets/img/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/assets/img/favicons/apple-touch-icon-114x114.png">


    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="scripts/html5shiv.js"></script>
      <script src="scripts/respond.min.js"></script>
    <![endif]-->
    
    <!--headerIncludes-->
    
</head>
<body data-spy="scroll" data-target=".navMenuCollapse">

    <!--PRELOADER-->

    <div id="preloader">
        <div class="loading-data"></div>
    </div>
    
    <div id="wrap">
        
    <!-- NAVIGATION LOGIN FULL -->
		<nav class="navbar bg-color3 navbar-fixed-top">
			<div class="container-fluid"> <a class="navbar-brand goto" href="/"><img src="http://www.blissful.co.ke/img/blissful-logo.png" height="50" alt="Blissful"></a>
				<button class="round-toggle navbar-toggle menu-collapse-btn collapsed" data-toggle="collapse" data-target=".navMenuCollapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<div class="collapse navbar-collapse navMenuCollapse">
					<div class="login-panel"><a class="btn btn-success bg-blissful" href="{{route('jobs.create')}}">Get Quotes</a></div>
					<ul class="nav">
						<li data-selector="nav li"><a href="#content-left">Why Blissful?</a></li>
						<li data-selector="nav li"><a href="#testimonials-slider-star">Testimonials</a></li>
						
					</ul>
				</div>
			</div>
		</nav>
        
        <!-- INTRO CENTER SLIDER -->
        <header id="intro-center-slider" class="intro-block full-slider no-sep">
            <div id="carousel-full-header" class="carousel slide carousel-full" data-ride="carousel">
            
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#carousel-full-header" data-slide-to="0" class="active"></li>
                  
                </ol>
            
				<div class="carousel-inner" role="listbox">
                
                 
                    <div class="item active cover-bg header-img">
                        <div class="container double-padding">
                            <div class="row">
                                <div class="col-md-6">
                                    <h2 class="big-title"><strong>COMPARE</strong><br> QUOTES</h2>
                                    <p>Find the right photographer for your big day or next event.</p>
                                    <a href="{{route('jobs.create')}}" class="goto btn btn-lg btn-default-white bg-blissful">Get Quotes</a>
                                </div>
                                <div class="col-md-6"></div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </header>
    
    
        <!-- CONTENT LEFT BLOCK -->
		<section id="content-left" class="bg-color3">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-push-6">
						<img class="screen" src="/assets/img/wed1.jpg" alt="screen"> 
					</div>
					<div class="col-md-5 col-md-pull-6 editContent">
						<h2>All photographers in one place</h2>
				
						<p>With over 200 photographers & videographers to choose from, Blissful makes its easier and faster to reach out to multiple suppliers at once. Saving you time and the hustle of having to contact multiple suppliers individually.</p>
					</div>
				</div>
			</div>
		</section>
        
        
        <!-- CONTENT RIGHT 2 BLOCK -->
		<section id="content-right-2">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<img class="screen" src="/assets/img/wed2.jpg" alt="screen"> 
					</div>
					<div class="col-md-5 col-md-offset-1 editContent">
						<h2>Compare quotes & save money</h2>
					
						<p>Requests made on Blissful are matched to suppliers that can do the work. These suppliers will then send you their quotes with price estimates and a sample of their work. Call or message the photographers if you need more information. When you're ready, hire an experienced professional at a price that's right for you.</p>
					</div>
				</div>
			</div>
		</section>

        <!-- CONTENT LEFT BLOCK -->
		<section id="content-left" class="bg-color3">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-push-6">
						<img class="screen" src="/assets/img/wed3.jpg" alt="screen"> 
					</div>
					<div class="col-md-5 col-md-pull-6 editContent">
						<h2>More than just photography</h2>
				
						<p>Our technology is transforming the way people find any service provider, anywhere in Kenya. Just tell us what you need and we'll quickly deliver customized quotes so you can compare pricing, read reviews, and hire the right professional on the spot.</p>
					</div>
				</div>
			</div>
		</section>
       
        
        <!-- DOWNLOAD MARKER CIRCLE RIGHT BLOCK -->
		<section id="download-marker-circle-right">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<h2>Find your ideal photographer today!</h2>
					</div>
                    <div class="col-md-5 text-right">
						<div class="marker-circle"><a href="{{route('jobs.create')}}" class="btn btn-lg btn-success bg-blissful">Get Quotes</a></div>
					</div>
				</div>
			</div>
		</section>
		
		
    
    	<!-- COUNTER BLOCK -->
		
    
    	<!-- COUNTER BLOCK -->
		<section id="counter" class="facts-block bg-blissful dark-bg text-center" data-selector="section">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<i class="icon icon-camera2" data-selector=".icon" style="color: rgb(255, 255, 255); font-size: 64px; opacity: 0.4;" id="ui-id-3"></i>
						<h3 class="timer" data-selector="h3" style="">210<br></h3>
						<h4 data-selector="h4" style="">Photographers to choose from</h4>
					</div>
					<div class="col-sm-4">
						<i class="icon icon-briefcase2" data-selector=".icon" style="color: rgb(255, 255, 255); font-size: 64px; opacity: 0.4;" id="ui-id-2"></i>
						<h3 class="timer" data-selector="h3">350</h3>
						<h4 data-selector="h4" style="">Matches Made</h4>
					</div>
					<div class="col-sm-4">
						<i class="icon icon-emoticon-smile" data-selector=".icon" style=""></i>
						<h3 class="timer" data-selector="h3" style="">400</h3>
						<h4 data-selector="h4" style="">Happy Brides</h4>
					</div>
				</div>
			</div>
		</section>
    
    
    
        
        
        <!-- TESTIMONIALS SLIDER STAR BLOCK -->
		<section id="testimonials-slider-star" class="quote-block text-center dark-bg bg-color1 cover-bg footer-img">
		
			<div id="carousel-testimonials" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner" role="listbox">
				  
					<div class="item">
						<div class="container quote">
							<i class="icon icon-star"></i><i class="icon icon-star"></i><i class="icon icon-star"></i><i class="icon icon-star"></i><i class="icon icon-star"></i>
							<h4>I love this site! Definitely takes the work out of finding wedding suppliers from me!</h4>
							<small>Elaine</small>
						</div>
					</div>
					
					<div class="item active">
						<div class="container quote">
							<i class="icon icon-star"></i><i class="icon icon-star"></i><i class="icon icon-star"></i><i class="icon icon-star"></i>
							<h4>Can’t believe how quickly I started receiving Price Estimates</h4>
							<small>Sammy K</small>
						</div>
					</div>

					<div class="item">
						<div class="container quote">
							<i class="icon icon-star"></i><i class="icon icon-star"></i><i class="icon icon-star"></i><i class="icon icon-star"></i>
							<h4>We were skeptical at first but in the end we were wowed by the responsiveness of the suppliers. We ended up sourcing over 6 suppliers on it.</h4>
							<small>Juliet & Robert</small>
						</div>
					</div>
					
				
					
					
				</div>
				<!-- Controls -->
				<a class="left carousel-control" href="#carousel-testimonials" role="button" data-slide="prev">
					<span class="arrow"></span>
				</a>
				<a class="right carousel-control" href="#carousel-testimonials" role="button" data-slide="next">
					<span class="arrow"></span>
				</a>
			</div>
		</section>
        
        
      
        
        
        <!-- FOOTER LIST >
		<footer id="footer-list" class="dark-bg bg-color1">
			<div class="container"> 
				<div class="row">
					<div class="col-md-8 col-md-push-4 text-right">
						<div class="row">
							<div class="col-md-3">
								<h4>Networks</h4>
								<ul class="links-list">
									<li><a href="#" target="_blank">Promotion</a></li>
									<li><a href="#" target="_blank">Planning</a></li>
									<li><a href="#" target="_blank">Journal</a></li>
									<li><a href="#" target="_blank">Reader</a></li>
									<li><a href="#" target="_blank">Store</a></li>
								</ul>
							</div>
							<div class="col-md-3">
								<h4>Categories</h4>
								<ul class="links-list">
									<li><a href="#" target="_blank">Header</a></li>
									<li><a href="#" target="_blank">Description</a></li>
									<li><a href="#" target="_blank">Portfolio</a></li>
									<li><a href="#" target="_blank">Team</a></li>
									<li><a href="#" target="_blank">Footer</a></li>
								</ul>
							</div>
							<div class="col-md-3">
								<h4>Mainframe</h4>
								<ul class="links-list">
									<li><a href="#" target="_blank">Register</a></li>
									<li><a href="#" target="_blank">Login</a></li>
									<li><a href="#" target="_blank">Contact Us</a></li>
									<li><a href="#" target="_blank">Terms of Use</a></li>
									<li><a href="#" target="_blank">Privacy</a></li>
								</ul>
							</div>
							<div class="col-md-3">
								<h4>Follow Us</h4>
								<ul class="links-list">
									<li><a href="#" target="_blank">Facebook</a></li>
									<li><a href="#" target="_blank">Twitter</a></li>
									<li><a href="#" target="_blank">Google Plus</a></li>
									<li><a href="#" target="_blank">LinkedIn</a></li>
									<li><a href="#" target="_blank">Dribbble</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-md-pull-8">
						<img class="logo" src="images/footer-logo.png" alt="">
						
					</div>
				</div>
			</div>
		</footer></div><!-- /#wrap -->
	

	<!-- MODALS BEGIN-->
	<div class="modal fade" id="modalMessage" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 class="modal-title">&nbsp;</h3>
			</div>
		</div>
	</div>
	<!-- MODALS END-->
	 

    <!-- JavaScript --> 
	<script src="/assets/js/jquery-1.11.2.min.js"></script> 
	<script src="/assets/js/bootstrap.min.js"></script> 
	<script src="/assets/js/jquery.validate.min.js"></script>
	<script src="/assets/js/smoothscroll.js"></script> 
	<script src="/assets/js/jquery.smooth-scroll.min.js"></script> 
	<script src="/assets/js/placeholders.jquery.min.js"></script> 
	<script src="/assets/js/jquery.magnific-popup.min.js"></script> 
	<script src="/assets/js/jquery.counterup.min.js"></script>
	<script src="/assets/js/waypoints.min.js"></script>
	<script src="/assets/js/video.js"></script>
	<script src="/assets/js/bigvideo.js"></script>
    <script src="/assets/js/animations/wow.min.js"></script>
    <script src="/assets/js/jquery.jCounter-0.1.4.js"></script>
	<script src="/assets/js/custom.js"></script>
	<script>
	    (function (i, s, o, g, r, a, m) {
	        i['GoogleAnalyticsObject'] = r;
	        i[r] = i[r] || function () {
	                    (i[r].q = i[r].q || []).push(arguments)
	                }, i[r].l = 1 * new Date();
	        a = s.createElement(o),
	                m = s.getElementsByTagName(o)[0];
	        a.async = 1;
	        a.src = g;
	        m.parentNode.insertBefore(a, m)
	    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

	    ga('create', 'UA-50669449-1', 'auto');
	    ga('send', 'pageview');

	</script>
	<script type="text/javascript">
	    adroll_adv_id = "TH4Q5IVG5FHXNNPGFG2JBA";
	    adroll_pix_id = "342MRCCGYRABZFOVG5SIPM";
	    /* OPTIONAL: provide email to improve user identification */
	    @if(Auth::check())
	     adroll_email = "{{Auth::user()->email}}"; 
	    @endif
	    (function () {
	        var _onload = function(){
	            if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
	            if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
	            var scr = document.createElement("script");
	            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
	            scr.setAttribute('async', 'true');
	            scr.type = "text/javascript";
	            scr.src = host + "/j/roundtrip.js";
	            ((document.getElementsByTagName('head') || [null])[0] ||
	                document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
	        };
	        if (window.addEventListener) {window.addEventListener('load', _onload, false);}
	        else {window.attachEvent('onload', _onload)}
	    }());
	</script>
	


</body></html>
