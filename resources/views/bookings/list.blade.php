<link href="{{ url('_asset/css') }}/style.css" rel="stylesheet">
<link href="{{ url('_asset/css') }}/daterangepicker.css" rel="stylesheet">
<link href="{{ url('_asset/fullcalendar') }}/fullcalendar.min.css" rel="stylesheet">
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

<div class="row">
	<div clss="col-lg-12">
		<ul class="breadcrumbs">
			<li>You are here: <a href="{{ url('/booking/calendar') }}">Calendar</a></li>
			<li class="current"><a href="{{ url('/booking/events') }}">Events</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		@if($events->count() > 0)
		<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>Event's Title</th>
					<th>Start</th>
					<th>End</th>
                    <th>Status</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			<?php $i = 1;?>
			@foreach($events as $event)
				<tr>
					<th scope="row">{{ $i++ }}</th>
					<td><a href="{{ url('booking/events/' . $event->id) }}">{{ $event->title }}</a></td>
					<td>{{ date("g:ia\, jS M Y", strtotime($event->start_time)) }}</td>
					<td>{{date("g:ia\, jS M Y", strtotime($event->end_time)) }}</td>
                    <td style="text-align:center">
                        @if ($event->booking_status == 1)
                        <span class="round warning label"><i class="fi-alert"></i></span>
                        @elseif ($event->booking_status == 2)
                        <span class="round success label"><i class="fi-check"></i></span>
                        @elseif ($event->booking_status == 3)
                        <span class="round alert label"><i class="fi-x"></i></span>
                        @endif
                    </td>
                    <td>
						<a class="btn btn-primary btn-xs" href="{{ url('booking/events/' . $event->id . '/edit')}}">
							<span class="fi-pencil"></span> Edit</a>
						<form action="{{ url('booking/events/' . $event->id) }}" style="display:inline" method="POST">
							<input type="hidden" name="_method" value="DELETE" />
							{{ csrf_field() }}
							<button class="btn btn-danger btn-xs" type="submit"><span class="fi-trash"></span> Delete</button>
						</form>
						
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		@else
			<h2>No event yet!</h2>
		@endif
	</div>
</div>
