<link href="{{ url('_asset/css') }}/daterangepicker.css" rel="stylesheet">
<div class="row">
    <div clss="col-lg-12">
        <ul class="breadcrumbs">
            <li>You are here: <a href="{{ url('/booking/calendar') }}">Calendar</a></li>
            <li><a href="{{ url('/booking/events') }}">Bookings</a></li>
            <li class="current">Edit - {{ $event->title }}</li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">

        @if($errors)
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        @endif

        <form action="{{ url('booking/events/' . $event->id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT"/>
            <div class="form-group @if($errors->has('name')) has-error has-feedback @endif">
                <label for="name">Your Name</label>
                <input type="text" class="form-control" name="name" value="{{ $event->name }}"
                       placeholder="E.g. Pisyek">
                @if ($errors->has('name'))
                    <p class="help-block"><span class="fi-alert"></span>
                        {{ $errors->first('name') }}
                    </p>
                @endif
            </div>
            <div class="form-group @if($errors->has('title')) has-error has-feedback @endif">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" value="{{ $event->title }}"
                       placeholder="E.g. My event's title">
                @if ($errors->has('title'))
                    <p class="help-block"><span class="fi-alert"></span>
                        {{ $errors->first('title') }}
                    </p>
                @endif
            </div>
            <div class="form-group @if($errors->has('time')) has-error @endif">
                <label for="time">Time</label>
                <div class="input-group">
                    <input type="text" class="form-control" name="time"
                           value="{{ $event->start_time . ' - ' . $event->end_time }}"
                           placeholder="Select your time">
                    <span class="input-group-addon">
						<span class="fi-calendar"></span>
                    </span>
                </div>
                @if ($errors->has('time'))
                    <p class="help-block"><span class="fi-alert"></span>
                        {{ $errors->first('time') }}
                    </p>
                @endif
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>

<script src="{{ url('_asset/js') }}/daterangepicker.js"></script>
<script type="text/javascript">
    $(function () {
        $('input[name="time"]').daterangepicker({
            "timePicker": true,
            "timePicker24Hour": true,
            "timePickerIncrement": 15,
            "autoApply": true,
            "locale": {
                "format": "DD/MM/YYYY HH:mm:ss",
                "separator": " - ",
            }
        });
    });
</script>