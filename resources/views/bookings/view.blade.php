<div class="row">
    <div clss="col-lg-12">
        <ul class="breadcrumbs">
            <li>You are here: <a href="{{ url('/booking/calendar') }}">Calendar</a></li>
            <li><a href="{{ url('/booking/events') }}">Events</a></li>
            <li class="current">{{ $event->title }}</li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h2>{{ $event->title }}
            <small>booked by {{ $event->name }}</small>
        </h2>
        <hr>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">

        <p>Time: <br>
            {{ date("g:ia\, jS M Y", strtotime($event->start_time)) . ' until ' . date("g:ia\, jS M Y", strtotime($event->end_time)) }}
        </p>

        <p>Duration: <br>
            {{ $duration }}
        </p>
        <p>Status: <br>
            @if ($event->booking_status == 1)
                <span class="round warning label"><i class="fi-alert"></i></span>
            @elseif ($event->booking_status == 2)
                <span class="round success label"><i class="fi-check"></i></span>
            @elseif ($event->booking_status == 3)
                <span class="round alert label"><i class="fi-x"></i></span>
            @endif
            {{ $event->booking_status_description }}
        </p>

        <p>
        <form action="{{ url('booking/events/' . $event->id) }}" style="display:inline;" method="POST">
            <input type="hidden" name="_method" value="DELETE"/>
            {{ csrf_field() }}
            <button class="btn btn-danger" type="submit"><span class="fi-trash"></span> Delete
            </button>
        </form>
        <a class="btn btn-primary" href="{{ url('booking/events/' . $event->id . '/edit')}}">
            <span class="fi-pencil"></span> Edit</a>

        </p>
        <p>
        <form action="{{ url('booking/events/' . $event->id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT"/>
            <div class="form-group @if($errors->has('status')) has-error @endif">
                <label for="status">Status</label>
                <select name="status" id="status" placeholder="status" class="form-control">
                    <optgroup label="status">
                        <option value="2">Approve</option>
                        <option value="3">Decline</option>
                    </optgroup>
                </select>
            </div>
            <button type="submit" class="btn btn-primary"><span class="fi-save"></span> Update Status</button>
        </form>
        </p>

    </div>
</div>

<script src="{{ url('_asset/js') }}/daterangepicker.js"></script>
<script type="text/javascript">
    $(function () {
        $('input[name="time"]').daterangepicker({
            "timePicker": true,
            "timePicker24Hour": true,
            "timePickerIncrement": 15,
            "autoApply": true,
            "locale": {
                "format": "DD/MM/YYYY HH:mm:ss",
                "separator": " - ",
            }
        });
    });
</script>