<link href="{{ url('_asset/css') }}/style.css" rel="stylesheet">
<link href="{{ url('_asset/css') }}/daterangepicker.css" rel="stylesheet">
<link href="{{ url('_asset/fullcalendar') }}/fullcalendar.min.css" rel="stylesheet">
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>


<nav class="breadcrumbs">
    <a href="{{ url('/booking/events') }}">Events List</a>
    <a href="{{ url('/booking/events/create') }}">Add new event</a>
</nav>

<div class="row">
    <div class="col-sm-8">
        <div id='calendar'></div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
        crossorigin="anonymous"></script>
<script src="{{ url('_asset/fullcalendar/lib') }}/moment.min.js"></script>
<script src="{{ url('_asset/fullcalendar') }}/fullcalendar.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        var base_url = '{{ url('/booking/calendar') }}';

        $('#calendar').fullCalendar({
            weekends: true,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: {
                url: '/booking/api',
                error: function () {
                    alert("cannot load json");
                }
            }
        });
    });
</script>