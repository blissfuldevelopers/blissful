<link href="{{ url('_asset/css') }}/daterangepicker.css" rel="stylesheet">
<div class="row">
    <div clss="col-lg-12">
        <ol class="breadcrumbs">
            <li>You are here: <a href="{{ url('/booking/calendar') }}">Calendar</a></li>
            <li><a href="{{ url('/booking/events') }}">Bookings</a></li>
            <li class="current">Add new booking</li>
        </ol>
    </div>
</div>

@include('message')

<div class="row">
    <div class="col-lg-6">

        <form action="{{ url('booking/events') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group @if($errors->has('name')) has-error has-feedback @endif">
                <label for="name">Your Name</label>
                <input type="text" class="form-control" name="name" placeholder="E.g. Sanji-San"
                       value="{{ old('name') }}">
                @if ($errors->has('name'))
                    <p class="help-block"><span class="fi-alert"></span>
                        {{ $errors->first('name') }}
                    </p>
                @endif
            </div>
            <div class="form-group @if($errors->has('email')) has-error has-feedback @endif">
                <label for="email">Your Email</label>
                <input type="text" class="form-control" name="email" placeholder="E.g. sanji.san@gmail.com"
                       value="{{ old('email') }}">
                @if ($errors->has('email'))
                    <p class="help-block"><span class="fi-alert"></span>
                        {{ $errors->first('email') }}
                    </p>
                @endif
            </div>
            <div class="form-group @if($errors->has('phone')) has-error has-feedback @endif">
                <label for="phone">Your Phone</label>
                <input type="text" class="form-control" name="phone" placeholder="E.g. 0701234567"
                       value="{{ old('phone') }}">
                @if ($errors->has('phone'))
                    <p class="help-block"><span class="fi-alert"></span>
                        {{ $errors->first('phone') }}
                    </p>
                @endif
            </div>
            <div class="form-group @if($errors->has('title')) has-error has-feedback @endif">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title"
                       placeholder="E.g. Sarah's wedding" value="{{ old('title') }}">
                @if ($errors->has('title'))
                    <p class="help-block"><span class="fi-alert"></span>
                        {{ $errors->first('title') }}
                    </p>
                @endif
            </div>
            <div class="form-group @if($errors->has('time')) has-error @endif">
                <label for="time">Time</label>
                <div class="input-group">
                    <input type="text" class="form-control" name="time" placeholder="Select your time"
                           value="{{ old('time') }}">
                    <span class="input-group-addon">
						<span class="fi-calendar"></span>
                    </span>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <button id="check_availability" class="btn btn-small btn-primary" type="button"><i
                                    class="fi-search"></i> Check Availability
                        </button>
                    </div>
                </div>
                <div class="control-group msg-check-success">
                    <label class="control-label"></label>
                    <div class="controls padding-right20">
                        <div id="ajax-placeholder-success" class="alert-box success radius"></div>
                    </div>
                </div>
                <div class="control-group msg-check-error">
                    <label class="control-label"></label>
                    <div class="controls padding-right20">
                        <div id="ajax-placeholder-error" class="alert-box alert round"></div>
                    </div>
                </div>
                @if ($errors->has('time'))
                    <p class="help-block"><span class="glyphicon glyphicon-exclamation-sign"></span>
                        {{ $errors->first('time') }}
                    </p>
                @endif
            </div>
            <input type="submit" class="btn btn-primary btn-xs" value="Make Booking"/>
        </form>
    </div>
</div>
<script src="{{ url('_asset/js') }}/daterangepicker.js"></script>
<script type="text/javascript">
    $(function () {
        $('input[name="time"]').daterangepicker({
            "minDate": moment('<?php echo date('Y-m-d G')?>'),
            "timePicker": true,
            "timePicker24Hour": true,
            "timePickerIncrement": 15,
            "autoApply": true,
            "locale": {
                "format": "DD/MM/YYYY HH:mm:ss",
                "separator": " - ",
            }
        });
    });
</script>
<script type='text/javascript' language='javascript'>
    $(document).ready(function () {

        $('.msg-check-success').hide();
        $('.msg-check-error').hide();
        $('input[type=submit]').attr('disabled', true);

        $('#check_availability').click(function () {
            $('.msg-check-success').hide();
            $('.msg-check-error').hide();

            var time = $('input[name=time]').val().split("-");

            var start_date = time[0];
            var end_date = time[1];

            if ((start_date != '') && (end_date != '')) {
                if (start_date == end_date) {
                    $('#ajax-placeholder-error').html('<i class="icon-info-sign"></i> Please select the set deadline.');
                    $('.msg-check-error').fadeIn();
                }
                else {
                    $('#ajax-placeholder').html('<p><img src="/img/ajax-loader.gif" width="145" height="19" /></p>');

                    $.ajax({
                        type: 'POST',
                        url: '/ajaxify/check_availability/' + 1,
                        data: "start_date=" + start_date + "&end_date=" + end_date,
                        dataType: 'json',
                        success: function (msg) {

                            if (msg['result'] == true) {
                                $('input[type=submit]').attr('disabled', false);
                                $('#ajax-placeholder-success').html('<i class="icon-ok"></i> There is a vacancy. Please continue booking.');

                                $('.msg-check-success').fadeIn();

                            }
                            else {
                                $('input[type=submit]').attr('disabled', true);
                                $('#ajax-placeholder-error').html('<i class="icon-info-sign"></i> No vacancy. Please select another time or date.');
                                $('.msg-check-error').fadeIn();
                            }
                        }
                    });
                }
            }
            else {
                $('input[type=submit]').attr('disabled', true);
                $('#ajax-placeholder-error').html('<i class="icon-info-sign"></i> Please select a date.');
                $('.msg-check-error').fadeIn();
            }
            $('input[name=start_date], input[name=end_date]').focus(function () {
                $('.msg-check-success').fadeOut();
                $('.msg-check-error').fadeOut();
            });
        });

    });
</script>