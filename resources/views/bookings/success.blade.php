<div class="small-12 columns booking-success">
	<p> <strong> Booking successfully placed. </strong></p>
	<span class="success-tick"><i class="fi-check"></i></span>
	<p> Our team will get into contact with you within the next 24 hrs to confirm your booking.</p>
</div>