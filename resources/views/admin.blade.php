<!DOCTYPE html>
<html lang="en" ng-app="BlisFul">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<link rel="shortcut icon" href="/app/client/images/favicon.ico">-->

        <title>Blis Ful</title>

        <!-- ========== Css Files ========== -->
        <link href="/app/common/css/root.css" rel="stylesheet" type="text/css"/>

        <!--Css for notification-->
        <link href="/app/common/css/ns-default.css" rel="stylesheet"/>
        <link href="/app/common/css/ns-style-attached.css" rel="stylesheet"/>
        <link href="/app/common/bower_components/textAngular/dist/textAngular.css" rel="stylesheet"/>

    </head>
    <body ng-controller="AppCtrl">

        <!-- START CONTENT -->
        <div ui-view=""></div>
        <!-- End Content -->

        <!-- jQuery Library -->
        <script type="text/javascript" src="/app/common/js/jquery.min.js"></script>

        <!-- Bootstrap min js-->
        <script src="/app/common/js/bootstrap.min.js" type="text/javascript"></script>

        <!-- Angular Core JavaScript File -->
        <script src="/app/common/bower_components/angular/angular.min.js" type="text/javascript"></script>

        <!-- Angularjs ui router -->
        <script src="/app/common/bower_components/angular-ui-router/release/angular-ui-router.min.js" type="text/javascript"></script>

        <!-- UI-Bootstrap Angularjs-->
        <script src="/app/common/bower_components/angular-bootstrap/ui-bootstrap.min.js" type="text/javascript"></script>
        <script src="/app/common/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js" type="text/javascript"></script>

        <!-- Sweet Alert -->
        <script src="/app/common/js/sweet-alert/sweet-alert.min.js" type="text/javascript"></script>

        <!-- summernote -->
        <script src="/app/common/bower_components/textAngular/dist/textAngular-rangy.min.js" type="text/javascript"></script>
        <script src="/app/common/bower_components/textAngular/dist/textAngular-sanitize.min.js" type="text/javascript"></script>
        <script src="/app/common/bower_components/textAngular/dist/textAngular.min.js" type="text/javascript"></script>


        <!-- Moment.js -->
        <script src="/app/common/js/moment/moment.min.js" type="text/javascript"></script>

        <!--underscore.js-->
        <script src="/app/common/js/underscore-min.js" type="text/javascript"></script>

        <!-- Image Upload Js-->
        <script src="/app/common/js/imageupload.js" type="text/javascript"></script>

        <!-- Notification Js -->
        <script src="/app/common/js/modernizr.custom.js"></script>
        <script src="/app/common/js/classie.js"></script>
        <script src="/app/common/js/notificationFx.js"></script>

        <!-- Page level Js (Angular Js Controller, Directive, Constant etc) -->
        <script src="/app/common/partials/app/directives.js"></script>
        <script src="/app/common/partials/app/constants.js"></script>
        <script src="/app/admin/partials/app.js"></script>

        <!--Dashboard Js-->
        <script src="/app/admin/partials/dashboard/dashboard.js"></script>
        <script src="/app/admin/partials/category/category.js"></script>
        <script src="/app/admin/partials/subcategory/subcategory.js"></script>
        <script src="/app/admin/partials/users/users.js"></script>
        <script src="/app/admin/partials/products/products.js"></script>

    </body>
</html>