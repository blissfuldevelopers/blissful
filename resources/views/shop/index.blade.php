@extends('page.home-layout')

@section('title')
    @if(@$title)
      {{$title}} for Sale in Kenya
    @else
        Weddings & Event Shopping Online in Kenya
    @endif
@endsection
@section('header-content')
  <?php
    $header_class = 'contentpg-intro';
  ?>
<div class="row">
  <div class="small-12 columns">
      @if(@$categories)
          <ul class="hide-for-small-only dropdown menu shop-category-dropdown" data-dropdown-menu>
              <li><a href="/shop">All</a></li>
              @foreach($categories as $data)
                  @if(@$cat->id == $data->id)
                      <li><a href="/shop/category/{{$data->category_slug}}">{{$data->name}}</a>
                        @if(count($data->subcategories) > 0)
                          <ul id="{{$data->category_slug}}" class="menu sub-item">
                              @foreach($data->subcategories as $subcategory)
                                  <li>
                                      <a href="/shop/subcategory/{{$subcategory->subcategory_slug}}">{{$subcategory->name}}</a>
                                  </li>
                              @endforeach
                          </ul>
                        @endif
                      </li>
                  @else
                      <li><a href="/shop/category/{{$data->category_slug}}">{{$data->name}}</a>
                        @if(count($data->subcategories) > 0)
                          <ul id="{{$data->category_slug}}" class="menu sub-item">
                              @foreach($data->subcategories as $subcategory)
                                  <li>
                                      <a href="/shop/subcategory/{{$subcategory->subcategory_slug}}">{{$subcategory->name}}</a>
                                  </li>
                              @endforeach
                          </ul>
                        @endif
                      </li>
                  @endif
              @endforeach
          </ul>
      @endif
  </div>
</div>
@endsection

@section('content')

    <!-- START OF HOMEPG MASTHEAD -->
    @if(@$home_products)
    <section class="shop-home-products">
        <div class="row">
          <div class="small-12 columns">
            <div class="mainft">

              <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{@$home_products[0]['header_image']}}">
              <div class="wrap">
                <h2>Shop from trusted suppliers all over the country</h2>
                <a id="scrollr" href="#" class="button button-primary">START SHOPPING</a>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="small-12 columns sub-prd">
            <div class="small-6 columns imgwrap">
              <img class="float-right" src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{@$home_products[1]['sub_header_image']}}">
            </div>
            <div class="small-6 columns textwrap">
                <h3>{{ @$home_products[1]['sub_header_text'] }}</h3>
                <p>{{ @$home_products[1]['sub_header_sub_text'] }}</p>
              <a href="/shop/category/{{@$home_products[1]['category_slug']}}" class="button small">EXPLORE</a>
            </div>
          </div>
        </div>
    </section>
    @elseif( @$cat->header_text )
    <section class="shop-home-products">
        <div class="row">
          <div class="small-12 columns">
            <div class="mainft">    
             <img class="float-right" src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{@$cat->header_image}}">
              <div class="wrap">
                <h2>{{ @$cat->header_text }}</h2>
                @if(@$cat->package == 1)
                <a id="scrollr" href="#" class="button button-primary">VIEW PACKAGES</a>
                @else
                <a id="scrollr" href="#" class="button button-primary">START SHOPPING</a>
                @endif
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="small-12 columns sub-prd">
            <div class="small-6 columns imgwrap">
              <img class="float-right" src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{@$cat->sub_header_image}}">
            </div>
            <div class="small-6 columns textwrap">
                 <h3>{{ @$cat->sub_header_text }}</h3>
                <p>{{ @$cat->sub_header_sub_text }}</p>
                <a href="{{ @$cat->category_slug}}" class="button small">VIEW NOW</a>
            </div>
          </div>
        </div>
    </section>
    @endif
    <!-- END OF HOMEPG MASTHEAD -->

    <!-- START OF MAIN SECTION -->
    <main aria-role="main" id="shop-main">
        <div class="row">
            <div class="small-12 columns">
                <h3>Explore fantastic items from the best suppliers</h3>  
                <p class="shop-intro">We hope you enjoy our selection of wedding favors, gifts and accessories, carefully chosen to help you make your wedding and engagement a truly memorable experience.</p>             
                @yield('shop-content')
            </div>
        </div>
    </main>

    <!-- SHOP HOME META CONTENT SECTION -->
    <section class="shop-meta">
      <div class="row">
        <div class="small-12 columns">
          <p>From your engagement to the bachelorette, there's so much to celebrate before the big day. Make each party even more special with personal touches. Best party decor in Kenya.</p>
          <p>Give guests wedding favors that's beautiful, memorable and personalized to suit your style. The very best wedding favors in Kenya that are unique and can be personalized.</p>
          <p>Want to make your wedding stand out, grab our DIY decor kits. From glass jars (mason jars), ribbons, spray on paint, spray on adhesive, fabrics, stencils and more now available in Kenya.</p>
        </div>
      </div>
    </section>
@endsection
