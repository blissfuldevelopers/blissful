@extends('shop.index')

@section('shop-content')
        <!-- START OF MAIN SECTION -->
        <!--<h3>See what services are available around you.</h3>-->
        
@if(count($products))
    <ul class="shop-items small-12 medium-10 small-centered columns">
        @foreach($products as $product)
            <li class="ripple">
                <a href="/shop/product/{{$product->product_slug}}">
                    <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{ $product->picture }}" style="height: 220px;"/>
                    <div class="wrap">
                        <h4 id="v-name">{{ $product->name }}</h4>
                        @if( $product->sale_price && $product->sale_price !== $product->price )
                            <h6>
                                Ksh {{ number_format($product->sale_price) }}
                            <h6>
                        @else
                            <h6>
                                Ksh {{ number_format($product->price) }}
                            </h6>
                        @endif                            
                    </div>
                </a>
            </li>
        @endforeach
    </ul>
@else
    <h4>No products found</h4>
@endif


{{--@if(is_object($products))--}}
    {{--<div class="small-12">--}}
        {{--{!! $products->render(Foundation::paginate($products)) !!}--}}
    {{--</div>--}}
{{--@endif--}}


@if (session('status'))
    <script>
        notification('error', '<?php echo session('status') ?>');
    </script>
    @endif
            <!-- END OF MAIN SECTION -->
    @endsection