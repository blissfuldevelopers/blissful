{{--{!! HTML::style('/assets/css/dashboard-products.css') !!}--}}
<div id="myshop-container">
    <div class="small-12" id="myshop-container-message"></div>
    <div class="small-12" id="myshop-container-data"
         data-url="{{ @$default_myshop_route }}"
         data-requested-url="{{ @$requested_myshop_route }}">
    </div>
</div>