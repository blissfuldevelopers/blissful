<div class="row order-details">
	<div class="details-card small-12 medium-10 ">
		<div class="small-4 columns blurry">
            <h3> <span>1</span> Checkout</h3>
        </div>
        <div class="small-4 columns blurry">
            <h3> <span>2</span> Fill In Your Details</h3>
        </div>
        <div class="small-4 columns current">
            <h3> <span>3</span> Place Order</h3>
        </div>
		<h4>Order Summary</h4>
		
		<div class="small-12 medium-6 columns delivery-details user-delivery-details">
			<div class="small-12 columns">
				<h5><strong>Delivery Details</strong> </h5>
			</div>
			<div class="small-12 columns">
				<strong>Name:</strong> {{Auth::user()->first_name}} {{Auth::user()->last_name}}
			</div>

			<div class="small-12 columns">
				<strong>Phone:</strong> {{Auth::user()->profile->phone}}
			</div>

			<div class="small-12 columns">
				<strong>Email:</strong> {{Auth::user()->email}}
			</div>

			<div class="small-12 columns">
				<strong>Address:</strong> {{$input['address']}} , {{$input['town']}}  
			</div>
		</div>
		<div class="loading loading--double"></div> 
		<div class="small-12 medium-6 columns delivery-details">
			<h5><strong>Order Details</strong> </h5>
				<div class="small-12 columns">
					<div class="small-6 columns">
						<p>Item</p>
					</div>
					<div class="small-3 columns">
						<p>Quantity</p>
					</div>
					<div class="small-3 columns">
						<p>Price</p>
					</div>
				</div>
			@foreach($ShopOrder->details as $detail)
				<div class="small-12 columns">
					<div class="small-6 columns">
						{{$detail->product->name}}
					</div>
					<div class="small-3 columns">
						{{$detail->qty}}
					</div>
					<div class="small-3 columns">
						{{($detail->price)*($detail->qty)}}
					</div>
				</div>

			@endforeach
				<div class="small-12 columns pricing-area">
					<div class="small-12 columns">
						<div class="small-9 columns">
							<h5> <strong>Delivery Fee</strong></h5>
						</div>
						<div class="small-3 columns">
							<p> + {{$ShopOrder->shipping_fees}}</p>
						</div>
					</div>
					<div class="small-12 columns">
						<div class="small-7 columns">
							<h4><strong>Total:</strong></h4>
						</div>

						<div class="small-5 columns">
							<h4>{{$ShopOrder->total}} Kes</h4>
						</div>
						
					</div>
				</div>
				
		</div>
		
		<div class="small-12 columns">
			<div class="small-6 right">
				<button class="order-btn tiny"> Place Order</button>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
	$('button.order-btn').click(function(event){
		event.preventDefault();
		$('a.order-btn').attr('disabled', 'disabled');
        $('.loading').show();
        $.ajax({
        	url: '/shop/cod-order-confirmation/{{$ShopOrder->id}}',
            type: 'get',
            success: function (data) {
                    $('div.details-card  button').removeAttr('disabled', 'disabled');
                    $('.loading').hide(); 
                    $('.order-details').parent().html(data);
                    $("a.showCart").hide();
                    $('.cart-table').remove();
                    $('.shopping-cart-title').remove();
                },
            error: function (e) {
                notification('error', 'Error while placing order');
                $('div.details-card  button').removeAttr('disabled', 'disabled');
                $('.loading').hide();
            }
        });
	})
</script>