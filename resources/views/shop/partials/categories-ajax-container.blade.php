{{--{!! HTML::style('/assets/css/dashboard-products.css') !!}--}}
<div id="shopcategories-container">
    <div class="grey small-12 row">
        <div id="shopcategories-container-back-action">
            <ul class="sub-nav">
                <li class="small-3 columns">
                    <a href="#back" class="small-12">
                        <i class="fi-arrow-left medium"></i>
                        <span class="text">Back</span>
                    </a>
                </li>

                <li class="small-5 columns">
                    <span class="small-12 grey">
                        <a href="#home" class="small-12">
                            <span class="text">Shop Categories</span>
                        </a>
                    </span>
                </li>

                <li class="small-3 columns">
                    <a href="#reload" class="">
                        <i class="fi-refresh medium right"></i>
                    </a>
                    <span id="curr-page-name"
                          data-url=""
                    ></span>
                </li>
            </ul>
        </div>


    </div>
    <div class="small-12" id="shopcategories-container-message"></div>
    <div class="small-12" id="shopcategories-container-data"
         data-url="{{ @$default_shop_categories_route }}"
         data-requested-url="{{ @$requested_shop_categories_route }}">
    </div>
</div>