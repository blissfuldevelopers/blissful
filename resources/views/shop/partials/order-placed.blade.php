<div class="row">
	<div class="small-12 medium-10 columns small-centered successful-order-card">
		<div class="small-12 columns small-centered successful-order-header">
			<h3>Order Successfully Placed!</h3>
		</div>
		<div class="small-4 medium-3 small-centered columns img-container">
			<img src="/img/delivery-van.svg"> <!-- delivery icon -->
		</div>
		<div class="small-10 small-centered columns successful-order-content">
			<p>Thank You for shopping with blissful, your order will be delivered in 3-5 days.</p>
			<p>for enquires call: 0775 987 555</p>
			<span>To track your order go to Dashboard > Orders</span>
		</div>
	</div>
</div>