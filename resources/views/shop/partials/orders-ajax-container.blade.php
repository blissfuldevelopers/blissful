{{--{!! HTML::style('/assets/css/dashboard-products.css') !!}--}}
<div id="shoporders-container">
    <div class="grey small-12 row">
        <div id="shoporders-container-back-action">
            <ul class="sub-nav">
                <li class="small-3 columns">
                    <a href="#back" class="small-12">
                        <i class="fi-arrow-left medium"></i>
                        <span class="text">Back</span>
                    </a>
                </li>

                <li class="small-5 columns">
                    <span class="small-12 grey">
                        <a href="#home" class="small-12">
                            <span class="text">Shop Orders</span>
                        </a>
                    </span>
                </li>

                <li class="small-3 columns">
                    <a href="#reload" class="">
                        <i class="fi-refresh medium right"></i>
                    </a>
                    <span id="curr-page-name"
                          data-url=""
                    ></span>
                </li>
            </ul>
        </div>


    </div>
    <div class="small-12" id="shoporders-container-message"></div>
    <div class="small-12" id="shoporders-container-data"
         data-url="{{ @$default_shop_orders_route }}"
         data-requested-url="{{ @$requested_shop_orders_route }}">
    </div>
</div>