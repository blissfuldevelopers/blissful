<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Blissful | Blissful Shop</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"/>
    <link href="/app/client/css/foundation.css" rel="stylesheet" type="text/css"/>
    <link href="/app/client/css/shop.css" rel="stylesheet" type="text/css"/>
    <!--<link rel="stylesheet" type="text/css" href="{{ elixir('css/zurb-layout.css') }}">-->
    <link href="/app/client/css/lobibox.css" rel="stylesheet" type="text/css"/>
    <script src="/app/client/js/vendor/modernizr.js" type="text/javascript"></script>

    <script src="/app/client/js/vendor/jquery.js" type="text/javascript"></script>
    <script src="/app/client/js/foundation.min.js" type="text/javascript"></script>
    <script src="/app/client/js/shop.js" type="text/javascript"></script>
    <script src="/app/client/js/notifications.js" type="text/javascript"></script>
    <script src="/app/client/js/Lobibox.js" type="text/javascript"></script>
    <script>
        function notification(type, msg, dely) {
            // 'warning', 'info', 'success', 'error'
            Lobibox.notify(type, {
                size: 'normal', // normal, mini, large
                icon: false,
                closable: true,
                sound: false,
                position: "top right",
                delay: dely || 5000,
                msg: msg
            });
        }

        function cartCount() {
            $.ajax({
                url: '/shop/cart-count',
                type: 'GET',
                success: function (data) {
                    console.log(data);
                    $("#CartCount").text(data);
                },
                error: function (e) {
                }
            });
        }
        cartCount();
    </script>
</head>
<body>

<!-- START OF HEADER SECTION -->
<header>
    <div class="row">
        <div class="small-12 columns">
            <nav class="top-bar" data-topbar role="navigation">
                <ul class="title-area">
                    <li class="name">
                        <h1><a href="/shop"><img src="/app/client/img/blissful-logo.svg"></a></h1>
                    </li>
                    <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
                </ul>

                <section class="top-bar-section">
                    <!-- Right Nav Section -->
                    <ul class="right">
                        @if(!Auth::check())
                            <li><a href="#">
                                    <button>List Your Business</button>
                                </a></li>
                        @endif
                        <li><a href="/shop/cart" style="position: relative">
                                <i class="fa fa-2x fa-shopping-cart"></i>
                                <span class="badge" id="CartCount">0</span>
                            </a>
                        </li>
                        @if(!Auth::check())
                            <li><a href="#">Help</a></li>
                            <li><a href="#">Log In</a></li>
                        @else
                            <?php
                            $count = Auth::user()->newMessagesCount();
                            $cssClass = $count == 0 ? 'hide' : '';
                            ?>
                            <li class="pimg">
                                <div class="breaker show-for-small-only"></div>
                                <a class="ripple" data-dropdown="drop1" aria-controls="drop1" aria-expanded="false">
                                    <img src="/{{Auth::user()->profile->profilePic}}"
                                         alt="profile pic"> {{Auth::user()->first_name}}
                                </a>
                                <ul id="drop1" class="f-dropdown hide-for-small-only" data-dropdown-content
                                    aria-hidden="true" tabindex="5">
                                    <li>
                                        <a href="@if( Auth::user()->hasRole('administrator')){{ route('admin.home') }}@elseif( Auth::user()->hasRole('vendor')){{ route('vendor.home') }}@elseif( Auth::user()->hasRole('user')){{ route('user.home') }}@endif">
                                            <i class="fi-home"></i> Dashboard</a></li>
                                    <li><a href="@if( Auth::user()->hasRole('vendor'))
                                        {{route('users.edit',array(Auth::user()->profile->user_id))}}
                                        @else
                                        {{ url('profile/edit/'.Auth::user()->profile->user_id) }}
                                        @endif
                                                "> <i class="fi-wrench large"></i> Profile</a></li>
                                    <li><a href="@if( Auth::user()->hasRole('administrator'))
                                                /admin#inbox
                                                @elseif( Auth::user()->hasRole('vendor'))
                                                /vndor#inbox
                                                @elseif( Auth::user()->hasRole('user'))
                                                /user#inbox
                                                @endif"> <i class="fi-mail"></i> Inbox <span
                                                    class="alert {{$cssClass}} label round">{{$count}}</span></a></li>
                                    <li><a href="{{ route('authenticated.logout') }}"> <i class="fi-power"
                                                                                          style="color:red;"></i> Logout</a>
                                    </li>
                                </ul>
                            </li>
                            <ul class="show-for-small-only" style="margin-left:20px;">
                                <li><a href="{{ route('auth.vendor-register') }}">List Your Business</a></li>
                                <li><a href="/login"> <i class="fi-home"></i> Dashboard</a></li>
                                <li><a href=" @if( Auth::user()->hasRole('vendor'))
                                    {{route('users.edit',array(Auth::user()->profile->user_id))}}
                                    @else
                                    {{ url('profile/edit/'.Auth::user()->profile->user_id) }}
                                    @endif
                                            "> <i class="fi-wrench large"></i> Profile</a></li>
                                <li><a href="@if( Auth::user()->hasRole('administrator'))
                                            /admin#inbox
                                            @elseif( Auth::user()->hasRole('vendor'))
                                            /vndor#inbox
                                            @elseif( Auth::user()->hasRole('user'))
                                            /user#inbox
                                            @endif"> <i class="fi-mail"></i> Inbox <span
                                                class="alert {{$cssClass}} label round">{{$count}}</span></a></li>
                                <li><a href="{{ route('authenticated.logout') }}"> <i class="fi-power"
                                                                                      style="color:red;"></i> Logout</a>
                                </li>
                            </ul>
                        @endif
                    </ul>
                    <!-- Left Nav Section -->
                    <ul class="left">
                        <li>
                            <form>
                                <input type="text" placeholder="Find a Vendor"/>
                            </form>
                        </li>
                    </ul>
                </section>
            </nav>
        </div>
    </div>
</header>
<!-- END OF HEADER SECTION -->

<!-- START OF NAV SECTION -->
<section class="deals-cat">
    <div class="row">
        <div class="small-12 columns">
            <nav>
                <div class="show-for-small-only">show categories</div>
                <ul>
                    <li><a href="/shop">Home</a></li>
                    @foreach($category as $data)
                        <li><a href="/shop/category/{{$data['id']}}">{{$data['name']}}</a></li>
                    @endforeach
                </ul>
            </nav>
        </div>
    </div>
</section>
<!-- END OF NAV SECTION -->

<!-- START OF MAIN SECTION -->
@yield('content')
        <!-- END OF MAIN SECTION -->

<!-- START OF FOOTER SECTION -->
<footer>
    <div class="tft">
        <div class="row">
            <div class="small-6 columns">
                <div class="row">
                    <div class="small-12 medium-6 large-6 columns">
                        <h5>COMPANY</h5>
                        <ul>
                            <li><a href="">About Us</a></li>
                            <li><a href="">Privacy Policy</a></li>
                            <li><a href="">Contact Us</a></li>
                            <li><a href="">Blog</a></li>
                        </ul>
                    </div>
                    <div class="small-12 medium-6 large-6 columns">
                        <h5>PARTNERS</h5>
                        <ul>
                            <li><a href="">List Your Business</a></li>
                            <li><a href="">Advertise</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="small-6 columns">
                <div class="row">
                    <div class="small-12 columns">
                        <ul class="socials">
                            <li class="f"><a href=""></a></li>
                            <li class="t"><a href=""></a></li>
                            <li class="g"><a href=""></a></li>
                            <li class="p"><a href=""></a></li>
                        </ul>
                        <ul class="right">
                            <li><a href="">Privacy Policy</a></li>
                            <li><a href="">Terms and Conditions</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bft">
        <div class="row">
            <div class="small-12 columns">
                <span>© 2015 Blissful</span>
            </div>
        </div>
    </div>
</footer>
<!-- END OF FOOTER SECTION -->

</body>
</html>
