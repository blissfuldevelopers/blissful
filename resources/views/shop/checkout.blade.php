@extends('shop.index')
@section('content')

        <!-- START OF MAIN SECTION -->
<main role="main" id="shop">
    <div class="small-12 medium-10 large-10 columns">
        @include('includes.shop-status')
    </div>
    <!-- ad banner -->
    <div class="promo">
        <div class="row">
            <div class="small-12 columns text-center">
                <h4>Checkout</h4>
            </div>
        </div>
    </div>
    <!-- popular deals -->

    <section class="pop-deals">
        <div class="row">
            <div class="small-12 columns" style="background: white;padding:15px;">
                @if(count($products))
                    <h4>Confirm Order</h4>
                    <hr/>
                    <table class="table cart-table">
                        <thead>
                        <tr>
                            <th>Item</th>
                            <th>Name</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $data)
                            <tr>
                                <td>
                                    <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$data['picture']}}" style="width: 75px;height: 75px;">
                                </td>
                                <td>{{$data['name']}}</td>
                                <td>{{$data['qty']}}</td>
                                <td>{{$data['price']}}</td>
                                <td>{{$data['total']}}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4" style="text-align: right"><b>Total</b></td>
                            <td colspan="4"><b>{{$total}}</b></td>
                        </tr>
                        </tbody>
                    </table>
                @endif
                @if(isset($iframe_src))
                    <iframe src="{{$iframe_src}}" width="100%" height="970px" scrolling="no" frameBorder="0">
                        <p>Browser unable to load iFrame</p>
                    </iframe>
                @endif
            </div>
        </div>
    </section>

</main>
<!-- END OF MAIN SECTION -->
@section('scripts')
    <script defer src="{{elixir('js/cart.js')}}" type="text/javascript"></script>
    <script>

        if (<?php echo $flag; ?>) {
            notification('success', 'Please enter your mobile number');
        }

    </script>
@endsection

@endsection