@extends('shop.index')
@section('title')
    Your Shopping Cart
@endsection
@section('content')

<!-- START OF MAIN SECTION -->
<main role="main" id="shop">
    <!-- ad banner -->
    <!-- <section class="c-header">
        <img src="/img/catheaders/1.jpg">
        <div class="row">
            <div class="small-12 columns">
                <div class="row" style="">
                    <div class="text-center">
                        <h2>
                            <i class="fi-shopping-cart"></i>
                            {{ isset($title) ? $title : 'Your Shopping Cart' }}
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section> -->


    <!-- popular deals -->

    <section class="pop-deals">
        <div class="row">
            <div class="small-12 columns shopping-cart-title ">
                <h2>
                    <i class="fi-shopping-cart"></i>
                    {{ isset($title) ? $title : 'Your Shopping Cart' }}
                </h2>
                @include('includes.status')
            </div>

            <!-- cash on delvery checkout -->
             <div class="small-12 columns">
                @if( count(@$cod_products))
                    <table class="table cart-table hide-for-small-only">
                        <thead>
                        <tr>
                            <th>Item</th>
                            <th>Name</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Total</th>
                            <th>Remove</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cod_products as $cod_product)
                            <?php $cod_product = (object)$cod_product; ?>

                            <tr>
                                <td>
                                    <a href="/shop/dealview/{{$cod_product->id }}">
                                        <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{ $cod_product->picture }}" style="width: 75px;height: 75px;">
                                    </a>
                                </td>
                                <td>
                                    <a href="/shop/dealview/{{$cod_product->id }}">
                                        {{  $cod_product->name }}
                                    </a>
                                </td>
                                <td>
                                    <select onchange="updateCartFn('{{  $cod_product->rowid }}', this.value)">
                                        <?php
                                        for ($i = 1; $i <= 10; $i++) {
                                        ?>
                                        <option <?php if (  $cod_product->qty == $i) { ?> selected="<?php echo $cod_product->qty; ?>"
                                                <?php } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td>{{  $cod_product->price }}</td>
                                <td>{{  $cod_product->total }}</td>
                                <td class="cart-item-remove">
                                    <a style="cursor: pointer"
                                       onclick="removeCartFn('{{  $cod_product->rowid }}')">Remove &#x26CC;
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4" style="text-align: right"><h3>Total</h3></td>
                            <td colspan="4"><h3>{{$cod_total}}</h3></td>
                        </tr>
                        </tbody>
                    </table>

                    <ul class="cart-table show-for-small-only">
                      @foreach($cod_products as $cod_product)
                        <?php $cod_product = (object)$cod_product; ?>
                          <li>
                            <div class="cart-det">
                              <a href="/shop/dealview/{{$cod_product->id }}">
                                <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{ $cod_product->picture }}" style="width: 75px;height: 75px;">
                              </a>
                            </div>
                            <div class="cart-det">
                              <a href="/shop/dealview/{{$cod_product->id }}"><h3>{{  $cod_product->name }}</h3>
                              </a>
                            </div>
                          </li>
                          <li>
                            <div class="cart-det">
                              <select onchange="updateCartFn('{{  $cod_product->rowid }}', this.value)">
                                <?php
                                for ($i = 1; $i <= 10; $i++) {
                                ?>
                                <option <?php if (  $cod_product->qty == $i) { ?> selected="<?php echo $cod_product->qty; ?>"
                                        <?php } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php
                                }
                                ?>
                              </select>
                            </div>
                            <div class="cart-det">
                              {{  $cod_product->price }}
                            </div>
                          </li>
                          <li class="cart-item-remove">
                            <div class="cart-det">{{  $cod_product->total }}</div>
                            <div class="cart-det">
                              <a style="cursor: pointer" class="fa fa-times"
                                 onclick="removeCartFn('{{  $cod_product->rowid }}')"><span class="ion-ios-close-outline"></span></a>
                            </div>
                          </li>
                        @endforeach
                        <li class="cart-total">
                          <div class="cart-det"><h3>Total</h3></div>
                          <div class="cart-det"><h3>{{$cod_total}}</h3></div>
                        </li>
                    </ul>

                    <div class="row">
                        @if(isset($cod_iframe_src))

                            <form action="/shop/save-user-order-details" method="post" class="order-details">
                            <input type="hidden" name="shop_order_id" value="{{$cod_iframe_src->id}}">
                                <div class="details-card small-12 medium-10 ">
                                    <div class="small-4 columns blurry">
                                        <h3> <span>1</span> Checkout</h3>
                                    </div>
                                    <div class="small-4 columns current">
                                        <h3> <span>2</span> Fill In Your Details</h3>
                                    </div>
                                    <div class="small-4 columns blurry">
                                        <h3> <span>3</span> Place Order</h3>
                                    </div>

                                    <div class="small-12 medium-8 small-centered columns">
                                        <div class="loading loading--double"></div>
                                      <div class="row">
                                        <div class="small-4 columns">
                                          <label for="right-label" class="right inline">Name<span>&#42;</span> </label>
                                        </div>
                                        <div class="small-4 columns">
                                          <input type="text" name="first_name" required data-parsley-required-message = "first name is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z]*$/" data-parsley-minlength="2" data-parsley-maxlength="32" placeholder="First Name" value="{{Auth::user()->first_name}}" />
                                        </div>
                                        <div class="small-4 columns">
                                          <input type="text" name="last_name" required data-parsley-required-message = "last name is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z]*$/" data-parsley-minlength="2" data-parsley-maxlength="32"  placeholder="Last Name" value="{{Auth::user()->last_name}}" />
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="small-4 columns">
                                          <label for="right-label" class="right inline" >Phone Number<span>&#42;</span> </label>
                                        </div>
                                        <div class="small-8 columns">
                                          <input type="number" required data-parsley-required-message = "Please input your phone number" data-parsley-trigger="change focusout" data-parsley-minlength="7" data-parsley-maxlength="12" name ="phone" placeholder="0712345678" value="{{Auth::user()->profile->phone}}" />
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="small-4 columns">
                                          <label for="right-label" class="right inline" >Town<span>&#42;</span> </label>
                                        </div>
                                        <div class="small-8 columns">
                                          <select required name="town">
                                              <option value="Nairobi">Nairobi</option>
                                              <option value="Mombasa">Mombasa</option>
                                              <option value="Kisumu">Kisumu</option>
                                              <option value="Eldoret">Eldoret</option>
                                              <option value="Naivasha">Naivasha</option>
                                              <option value="Nakuru">Nakuru</option>
                                              <option value="Machakos">Machakos</option>
                                          </select>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="small-4 columns">
                                          <label for="right-label" class="right inline">Delivery Address<span>&#42;</span> </label>
                                        </div>
                                        <div class="small-8 columns">
                                            <textarea required data-parsley-required-message = "Please add a delivery address" data-parsley-trigger="change focusout" data-parsley-minlength="2" name="address"></textarea>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="small-12 medium-8 small-centered columns">
                                        <button class="tiny small-8 right save-details-btn" type="submit"> Save & Continue </button>
                                    </div>
                                </div>
                            </form>
                        @else
                            <div class="small-12 medium-4 large-4 column text-left">
                                <a role="button" href="/shop" class="button expand ripple has-ripple pull-left shopbtn">Continue
                                    Shopping</a>
                            </div>

                            <div class="small-12 medium-4 large-4 column text-right">
                                <a role="button" href="/shop/checkout"
                                   class="button button-primary expand checkoutbtn ripple has-ripple">Proceed To
                                    CheckOut</a>
                            </div>
                        @endif
                    </div>
            </div>

            <!-- Pesapal checkout -->
            <div class="small-12 columns">
                @elseif(count(@$pesapal_products))
                    <table class="table cart-table hide-for-small-only">
                        <thead>
                        <tr>
                            <th>Item</th>
                            <th>Name</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Total</th>
                            <th>Remove</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pesapal_products as $pesapal_product)
                            <?php $pesapal_product = (object)$pesapal_product; ?>

                            <tr>
                                <td>
                                    <a href="/shop/dealview/{{$pesapal_product->id }}">
                                        <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{ $pesapal_product->picture }}" style="width: 75px;height: 75px;">
                                    </a>
                                </td>
                                <td>
                                    <a href="/shop/dealview/{{$pesapal_product->id }}">
                                        {{  $pesapal_product->name }}
                                    </a>
                                </td>
                                <td>
                                    <select onchange="updateCartFn('{{  $pesapal_product->rowid }}', this.value)">
                                        <?php
                                        for ($i = 1; $i <= 10; $i++) {
                                        ?>
                                        <option <?php if (  $pesapal_product->qty == $i) { ?> selected="<?php echo $pesapal_product->qty; ?>"
                                                <?php } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td>{{  $pesapal_product->price }}</td>
                                <td>{{  $pesapal_product->total }}</td>
                                <td class="cart-item-remove">
                                    <a style="cursor: pointer"
                                       onclick="removeCartFn('{{  $pesapal_product->rowid }}')">Remove &#x26CC;
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4" style="text-align: right"><h3>Total</h3></td>
                            <td colspan="4"><h3>{{$pesapal_total}}</h3></td>
                        </tr>
                        </tbody>
                    </table>

                    <ul class="cart-table show-for-small-only">
                      @foreach($pesapal_products as $pesapal_product)
                        <?php $pesapal_product = (object)$pesapal_product; ?>
                          <li>
                            <div class="cart-det">
                              <a href="/shop/dealview/{{$pesapal_product->id }}">
                                <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{ $pesapal_product->picture }}" style="width: 75px;height: 75px;">
                              </a>
                            </div>
                            <div class="cart-det">
                              <a href="/shop/dealview/{{$pesapal_product->id }}"><h3>{{  $pesapal_product->name }}</h3>
                              </a>
                            </div>
                          </li>
                          <li>
                            <div class="cart-det">
                              <select onchange="updateCartFn('{{  $pesapal_product->rowid }}', this.value)">
                                <?php
                                for ($i = 1; $i <= 10; $i++) {
                                ?>
                                <option <?php if (  $pesapal_product->qty == $i) { ?> selected="<?php echo $pesapal_product->qty; ?>"
                                        <?php } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php
                                }
                                ?>
                              </select>
                            </div>
                            <div class="cart-det">
                              {{  $pesapal_product->price }}
                            </div>
                          </li>
                          <li class="cart-item-remove">
                            <div class="cart-det">{{  $pesapal_product->total }}</div>
                            <div class="cart-det">
                              <a style="cursor: pointer" class="fa fa-times"
                                 onclick="removeCartFn('{{  $pesapal_product->rowid }}')"><span class="ion-ios-close-outline"></span></a>
                            </div>
                          </li>
                        @endforeach
                        <li class="cart-total">
                          <div class="cart-det"><h3>Total</h3></div>
                          <div class="cart-det"><h3>{{$pesapal_total}}</h3></div>
                        </li>
                    </ul>

                    <div class="row">
                        @if(isset($pesapal_iframe_src))
                            <div class="small-12 columns">
                                <iframe src="{{$pesapal_iframe_src}}" width="100%" height="970px" scrolling="no"
                                        frameBorder="0">
                                    <h1>Uh oh...</h1>
                                    <h2>We had trouble loading the checkout area</h2>
                                    <h3>Please refresh this page</h3>
                                </iframe>
                            </div>
                        @else
                            <div class="small-12 medium-4 large-4 column text-left">
                                <a role="button" href="/shop" class="button expand ripple has-ripple pull-left shopbtn">Continue
                                    Shopping</a>
                            </div>

                            <div class="small-12 medium-4 large-4 column text-right">
                                <a role="button" href="/shop/checkout"
                                   class="button button-primary expand checkoutbtn ripple has-ripple">Proceed To
                                    CheckOut</a>
                            </div>
                        @endif
                    </div>
                @else
                    <div class="small-12 columns text-center form-group" style="margin-bottom: 10px;">
                        <h1 class="text-primary">There are no items in this cart. </h1>
                        <p>Your shopping cart is empty. Please click the link below to.</p>
                        <a href="/shop" class="btn btn-primary">Continue Shopping</a>
                    </div>

                    <ul>
                        @foreach($category as $product)
                            <?php $product = (object)$product; ?>
                            <li style="background: white">
                                <a href="/shop/category/{{$product->category_slug}}">
                                    <h4 style="text-align: center">{{  $product->name }}</h4>
                                </a>
                            </li>
                        @endforeach
                    </ul>

                @endif
            </div>


        </div>
    </section>
</main>
<!-- END OF MAIN SECTION -->
@section('scripts')
<script defer src="{{elixir('js/cart.js')}}" type="text/javascript"></script>
@endsection


@endsection