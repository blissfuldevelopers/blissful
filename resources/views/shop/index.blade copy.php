@extends('page.shop-layout')

@section('title')
    Shop
@endsection
@section('styles')
    <link href="/css/hipster_cards.css" rel="stylesheet"/>
    <link href="/css/pe-icon-7-stroke.css" rel="stylesheet"/>
    @endsection


    @section('content')

    <!-- START OF HOMEPG MASTHEAD -->
    <section class="masthead">
        <div class="row">
          <div class="small-12 columns">
            <div class="mainft">
              <img src="app/client/img/homepg/masthead/1.jpg">
              <div class="wrap">
                <h2>Shop from trusted vendors all over the country</h2>
                <a href="#" class="button">START SHOPPING</a>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="small-12 columns">
            <div class="ft">
              <div class="imgwrap">
                <img src="app/client/img/homepg/masthead/2.jpg">
              </div>
              <div class="textwrap">
                <h3>80% OFF</h3>
                <p>Colourful, eco-friendly plastic cocktail utensils.</p>
                <a href="#" class="button">BUY NOW</a>
              </div>
            </div>
          </div>
        </div>
    </section>
    <!-- END OF HOMEPG MASTHEAD -->

    <!-- START OF MAIN SECTION -->
    <main aria-role="main">
        <div class="row">
            <div class="small-12 columns">
                <h3>Explore fantastic items from the best suppliers</h3>               
                
                @yield('shop-content')

                @if(@$recent && count(@$recent))
                    <div class="row">
                        <div class="small-12 medium-offset-2 medium-10 columns">
                            <h3 class="shop_desc">Recently Viewed</h3>
                            @foreach($recent as $product)
                                <div class="card-box ripple small-12 medium-4 columns">
                                    <div class="card text-center" data-background="image"
                                         data-src="/{{ $product->picture }}">
                                        <h4 class="title title-modern">{{ $product->category->name }}</h4>

                                        <div class="content">
                                            <div class="price hm">
                                                @if( $product->sale_price )
                                                    <h5 class="white">{{ $product->sale_price }} <b class="currency">
                                                            Ksh</b></h5>
                                                    <h6 class="white">{{ round((($product->price -  $product->sale_price)/ $product->price)*100) }}
                                                        % Discount</h6>

                                                @else
                                                    <h5 class="white">{{ $product->price }} <b class="currency"> Ksh</b>
                                                    </h5>
                                                @endif

                                            </div>
                                            <p class="description">{{$product->name}}</p>
                                        </div>
                                        <div class="filter"></div>
                                        <div class="footer btn-center">
                                            <button class="btn btn-neutral btn-round btn-fill"><a
                                                        href="/shop/product/{{$product->product_slug}}"></a> Details
                                            </button>
                                        </div>
                                    </div> <!-- end card -->
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
    </main>
@endsection

@section('scripts')
    <script src="/js/hipster-cards.js"></script>
    <script type="text/javascript">
        $('.filter-icon a').click(function () {
            $(this).parent().parent().parent().parent().css('height', 'initial');
            console.log('clicked');
        });
    </script>
@endsection