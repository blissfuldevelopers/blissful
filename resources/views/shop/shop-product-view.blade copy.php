@extends('page.shop-layout')
@section('content')
<!-- START OF MAIN SECTION -->

<!-- START OF BODY -->
<main aria-role="main" class="shop-item">
    <!-- START OF SHOP ITEM HEADER SECTION -->
    <section class="title-header">
      <div class="row">
        <div class="small-12 columns">
          <h2>{{ $product->name }}</h2>
          <h4>Vendor Name</h4>
        </div>
      </div>
    </section>
    <!-- END OF SHOP ITEM HEADER SECTION -->
    <!-- START OF SHOP CART SECTION -->
    <section class="item-summary">
      <div class="row">
        <!-- Item Image -->
        <div class="small-12 medium-8 columns">
          <img src="/{{ $product->picture }}">
        </div>
        <!-- Cart Details -->
        <div class="small-12 medium-4 columns cart">
          <div class="row">
            <div class="wrap">
              <!-- quantity -->  
              <div class="small-12 columns">
                <h6>Quantity</h6>
                <form>
                  <select></select>
                </form>
              </div>
              <!-- size -->
              <div class="small-12 columns">
                <h6>Size</h6>
                <form>
                  <select></select>
                </form>
              </div>
              <!-- overview -->
              <div class="small-12 columns">
                <h6 class="bold">Overview</h6>
                <ul class="summary">
                  <li>Handmade item</li>
                  <li>Pure sterling silter</li>
                  <li>Made to order</li>
                </ul>
                <ul class="user">
                  <li>5 reviews</li>
                  <li>4 favourites</li>
                </ul>
              </div>
              <!-- Price -->
              <div class="small-12 columns">
                @if( $product->sale_price && $product->sale_price !== $product->price )
                <h5>{{ $product->sale_price }}</h5>
                @else
                <h5>{{ $product->price }}</h5>
                @endif
              </div>
              <!-- Cart Functions -->
              <div class="small-12 columns">
                <button onclick="addToCartFn({{ $product->id }})"
                    class="button buy ripple expand dark_orange_background ripple has-ripple">
                        Add to Cart
                </button>
              </div>
              <div class="small-12 columns">
                <a href="#" class="button query">Ask a Question</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- END OF SHOP CART SECTION -->

    <div class="row">
        
        <div class="small-12 large-4 collapse columns">
            <div class="ft-dets-wrap">
                <h3>{{ $product->name }}</h3>
                <h6>Category:<a href="/shop/category/{{$product->category->category_slug}}"> {{ $product->category->name }}</a></h6>
                <hr/>

                


                

            </div>
        </div>
    </div>
    <div class="row deal-dets">
        <div class="small-12 large-8 collapse columns">
            <div class="small-12 columns">
                <p class="shop_desc">Product Description</p>
                <h5>
                    <?php echo html_entity_decode($product->description) ?>
                </h5>
                <hr/>
                @if($product->highlights && $product->highlights <> '')
                    <h6>Highlights</h6>
                    <p>
                        <?php echo html_entity_decode($product->highlights) ?>
                    </p>
                @endif
            </div>
        </div>
        @if($product->tnc && $product->tnc <> '')
            <div class="small-12 large-4 collapse columns">
                <div class="small-12 columns">
                    <h6>Terms and Condition</h6>
                    <hr>
                    <p>
                        <?php echo html_entity_decode($product->tnc) ?>
                    </p>
                </div>
            </div>
        @endif
    </div>
    </div>
</section>

<!-- popular deals -->
<section class="browse-deals">
    <div class="row">
        <div class="small-12 columns">
            <h3 class="shop_desc">Similar Products</h3>
                @foreach($similar as $product)
                <div class="card-box ripple small-12 medium-4 columns">
                    <div class="card text-center" data-background="image" data-src="/{{ $product->picture }}">
                        <h4 class="title title-modern">{{ $product->category->name }}</h4>

                        <div class="content">
                            <div class="price hm">
                                @if( $product->sale_price )
                                    <h5 class="white">{{ $product->sale_price }} <b class="currency"> Ksh</b></h5>
                                    <h6 class="white">{{ round((($product->price -  $product->sale_price)/ $product->price)*100) }} % Discount</h6>

                                @else
                                    <h5 class="white">{{ $product->price }} <b class="currency"> Ksh</b></h5>
                                @endif

                            </div>
                            <p class="description">{{$product->name}}</p>
                        </div>
                        <div class="filter"></div>
                        <div class="footer btn-center">
                            <button class="btn btn-neutral btn-round btn-fill"><a href="/shop/product/{{$product->product_slug}}"></a> Details</button>
                        </div>
                    </div> <!-- end card -->
                </div>
                @endforeach
        </div>
    </div>
</section>

<!-- END OF MAIN SECTION -->

<script>
    function addToCartFn(id) {
        $.ajax({
            url: '/shop/cart/' + id,
            type: 'GET',
            success: function (data) {
                if (data.flag) {
                    notification('success', 'Item added to cart successfully');
                    window.location.href = '/shop/cart';
                    cartCount();
                } else {
                    notification('error', 'Error while add to cart');
                }
            },
            error: function (e) {
                notification('error', 'Error while add to cart');
            }
        });
    }
</script>
@stop