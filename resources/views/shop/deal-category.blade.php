@extends('shop.index')
@section('content')
<!-- START OF MAIN SECTION -->
<main role="main" id="dealcat">
    <!-- feature section -->
    <section class="main-ft">
        <div class="row">
            <div class="small-12 large-8 collapse columns">
                <div class="ft-img">
                    <img src="{{$data['picture']}}">
                </div>
            </div>
            <div class="small-12 large-4 collapse columns">
                <div class="ft-dets-wrap">
                    <h5>Deal of the Week</h5>
                    <h3>{{$data['name']}}</h3>
                    <h6>Travelsafe Tours</h6>
                    <ul class="stars">
                        <li class="active">&#9829;</li>
                        <li class="active">&#9829;</li>
                        <li class="active">&#9829;</li>
                        <li>&#9829;</li>
                        <li>&#9829;</li>
                    </ul>
                    <p>{{$data['description']}}</p>
                    <div class="deal-p">{{$data['price']}}</div>
                    <div class="d-buy">
                        <a href="#" class="button buy">BUY</a>
                        <a href="#" class="button query">ASK A QUESTION</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- popular deals -->
    <section class="browse-deals">
        <div class="row">
            <div class="small-12 columns">
                <h3>BROWSE</h3>
                <ul>
                    <li>
                        <img src="/app/client/img/landing/pop-deals/1.jpg">
                        <div class="d-dets">
                            <h4>Cake Toppings</h4>
                            <h5>Travelsafe Tours</h5>
                            <ul class="stars">
                                <li class="active">&#9829;</li>
                                <li class="active">&#9829;</li>
                                <li class="active">&#9829;</li>
                                <li>&#9829;</li>
                                <li>&#9829;</li>
                            </ul>
                            <div class="d-prices">
                                <ul>
                                    <li class="actual-p">4,500</li>
                                    <li class="deal-p">3,000</li>
                                </ul>
                            </div>
                            <div class="d-buy">
                                <a href="#" class="button buy">BUY</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <img src="/app/client/img/landing/pop-deals/2.jpg">
                        <div class="d-dets">
                            <h4>Decorated High Heels</h4>
                            <h5>Travelsafe Tours</h5>
                            <ul class="stars">
                                <li class="active">&#9829;</li>
                                <li class="active">&#9829;</li>
                                <li class="active">&#9829;</li>
                                <li>&#9829;</li>
                                <li>&#9829;</li>
                            </ul>
                            <div class="d-prices">
                                <ul>
                                    <li class="actual-p">4,500</li>
                                    <li class="deal-p">3,000</li>
                                </ul>
                            </div>
                            <div class="d-buy">
                                <a href="#" class="button buy">BUY</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <img src="/app/client/img/landing/pop-deals/3.jpg">
                        <div class="d-dets">
                            <h4>Event Security</h4>
                            <h5>Travelsafe Tours</h5>
                            <ul class="stars">
                                <li class="active">&#9829;</li>
                                <li class="active">&#9829;</li>
                                <li class="active">&#9829;</li>
                                <li>&#9829;</li>
                                <li>&#9829;</li>
                            </ul>
                            <div class="d-prices">
                                <ul>
                                    <li class="actual-p">4,500</li>
                                    <li class="deal-p">3,000</li>
                                </ul>
                            </div>
                            <div class="d-buy">
                                <a href="#" class="button buy">BUY</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <img src="/app/client/img/landing/pop-deals/4.jpg">
                        <div class="d-dets">
                            <h4>Make-Up</h4>
                            <h5>Travelsafe Tours</h5>
                            <ul class="stars">
                                <li class="active">&#9829;</li>
                                <li class="active">&#9829;</li>
                                <li class="active">&#9829;</li>
                                <li>&#9829;</li>
                                <li>&#9829;</li>
                            </ul>
                            <div class="d-prices">
                                <ul>
                                    <li class="actual-p">4,500</li>
                                    <li class="deal-p">3,000</li>
                                </ul>
                            </div>
                            <div class="d-buy">
                                <a href="#" class="button buy">BUY</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <img src="/app/client/img/landing/pop-deals/3.jpg">
                        <div class="d-dets">
                            <h4>Event Security</h4>
                            <h5>Travelsafe Tours</h5>
                            <ul class="stars">
                                <li class="active">&#9829;</li>
                                <li class="active">&#9829;</li>
                                <li class="active">&#9829;</li>
                                <li>&#9829;</li>
                                <li>&#9829;</li>
                            </ul>
                            <div class="d-prices">
                                <ul>
                                    <li class="actual-p">4,500</li>
                                    <li class="deal-p">3,000</li>
                                </ul>
                            </div>
                            <div class="d-buy">
                                <a href="#" class="button buy">BUY</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <img src="/app/client/img/landing/pop-deals/2.jpg">
                        <div class="d-dets">
                            <h4>Decorated High Heels</h4>
                            <h5>Travelsafe Tours</h5>
                            <ul class="stars">
                                <li class="active">&#9829;</li>
                                <li class="active">&#9829;</li>
                                <li class="active">&#9829;</li>
                                <li>&#9829;</li>
                                <li>&#9829;</li>
                            </ul>
                            <div class="d-prices">
                                <ul>
                                    <li class="actual-p">4,500</li>
                                    <li class="deal-p">3,000</li>
                                </ul>
                            </div>
                            <div class="d-buy">
                                <a href="#" class="button buy">BUY</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
</main>
<!-- END OF MAIN SECTION -->

@stop