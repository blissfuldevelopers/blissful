<div class="main_content container">
    <h1>Create New Product Category</h1>
    <hr/>

    {!! Form::open(['route' => 'shop.admin.category.store', 'class' => 'form-horizontal','method' => 'post','files' => true]) !!}

    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
        {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group row {{ $errors->has('package') ? 'has-error' : ''}}">
        {!! Form::label('package', 'Is this a package? ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="small-12 medium-5 columns">
            <div class="small-2 columns">
                {!! Form::radio('package', '1', ['id' => 'packageYes']); !!}
            </div>
            <div class="small-4 columns">
                {!! Form::label('packageYes', 'Yes') !!}
            </div>
            <div class="small-2 columns">
              {!! Form::radio('package', '0' , true, ['id' => 'packageNo']); !!}  
            </div>
            <div class="small-4 columns">
              {!! Form::label('packageNo', 'No') !!}  
            </div>
            {!! $errors->first('package', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
        {!! Form::label('description', 'Description: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('header_image') ? 'has-error' : ''}}">
        {!! Form::label('header_image', 'Header Image: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <input type="file" name="header_image" class="form-control">
            {!! $errors->first('header_image', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('header_text') ? 'has-error' : ''}}">
        {!! Form::label('header_text', 'Header Image Text: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('header_text', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('header_text', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('sub_header_image') ? 'has-error' : ''}}">
        {!! Form::label('sub_header_image', 'Sub-header Image: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <input type="file" name="sub_header_image" class="form-control">
            {!! $errors->first('sub_header_image', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('sub_header_text') ? 'has-error' : ''}}">
        {!! Form::label('sub_header_text', 'Sub-header Image Text: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('sub_header_text', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('sub_header_text', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('sub_header_sub_text') ? 'has-error' : ''}}">
        {!! Form::label('sub_header_sub_text', 'Sub-header Image Sub-text: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('sub_header_sub_text', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('sub_header_sub_text', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Create', ['class' => 'submit-btn btn btn-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
</div>
{!! HTML::script('/assets/plugins/parsley.min.js') !!}
{!! HTML::script('/js/dashboard/jquery.min.js') !!}
{!! HTML::script('/js/dashboard/jquery-ui/jquery-ui.min.js') !!}
{!! HTML::script('/js/foundation-datepicker.min.js') !!}
{!! HTML::script('/js/multistep-form.js') !!}
<script src="/assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    _container = $('main');
        add_overlay = function () {
            _container.css("position", "relative");
            var overlay = $('<div id="overlay"></div>').css({
                    position: "absolute",
                    width: "100%",
                    height: "100%",
                    top: 0,
                    left: 0,
                    "background-color": "rgba(255,255,255,0.1)" /*dim the background*/
                }),
                modal = $('<div id="this-modal"><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>Loading...</div>').css({
                    position: "fixed",
                    width: "200px",
                    height: "200px",
                    top: "50%",
                    left: "50%",
                    "color": "#e9048b",
                    "margin-left": "-150px",
                    "margin-top": "-100px",
                    "text-align": "center",
                    "z-index": "10",
                });

            modal.appendTo(overlay.appendTo(_container));

        }
        remove_overlay = function () {
            $('#overlay').hide().remove();

        }
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;

    jQuery(document).ready(function ($) {
        if (!isMobile) {
            tinymce.init({
                selector: "textarea",
                theme: "modern",

                menubar: true,
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern imagetools jbimages"
                ],
                toolbar: " fontsizeselect fontselect formatselect bold italic alignleft aligncenter alignright alignjustify bullist numlist outdent indent preview",
                relative_urls: false,
                image_advtab: true,
                templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
                ]
            });
        }
        else {
            tinymce.init({
                selector: ".tinymce-text",
                theme: "modern",
                menubar: false,
                plugins: [
                    "advlist autolink lists link charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime  nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern imagetools "
                ],
                toolbar: false,
                relative_urls: false,
                image_advtab: true,
                templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
                ]
            });
        }
    });
    $('.submit-btn').click(function (event) {
        event.preventDefault();
        tinyMCE.triggerSave();

        var editorContent = tinyMCE.activeEditor.getContent();
        if (editorContent == '' || editorContent == null) {
            $(tinyMCE.activeEditor.getContainer())
                    .css("background-color", '#ffeeee')
                    .parent()
                    .css({
                        "background-color": '#ffeeee',
                        "border": '1px solid #EC736C'
                    });
            $('<span class="red">Please Type in your message</span>').insertAfter($(tinyMCE.activeEditor.getContainer()));

        }

        else {
            add_overlay();
            var form = $(this).closest('form')[0];
            var formData = new FormData(form);
            $.ajax({    
                        type:'POST',
                        url: $(this).closest('form').attr('action'),
                        data: formData,
                        // THIS MUST BE DONE FOR FILE UPLOADING
                        contentType: false,
                        processData: false,
                        // ... Other options like success and etc
                        success : function(data) {
                         remove_overlay();
                         $('.container').html(data);
                          
                        },
                        error:  function(data){
                            remove_overlay();
                            alert('error');
                        }
                    }); 
        }
    });

</script>

