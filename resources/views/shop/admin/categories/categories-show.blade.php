{{--<div class="row">--}}
{{--<div class="small-12 columns">--}}
{{--@if($categories)--}}
{{--<ul>--}}
{{--@foreach($categories as $category)--}}

{{--<li style="background: white">--}}
{{--<a href="#category_{{ $category->id }}">--}}
{{--<h4 style="text-align: center">{{ $category->name }}</h4>--}}
{{--</a>--}}
{{--</li>--}}
{{--@endforeach--}}
{{--</ul>--}}
{{--@endif--}}
{{--</div>--}}
{{--</div>--}}
<h1>Category<a href="{{ url('shop/admin/category/create') }}" class="btn btn-primary pull-right btn-sm">Add New
        Category</a></h1>
 @include('flash::message')
<div class="table">
    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Slug</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        {{-- */$x=0;/* --}}
        @if($categories)
            @foreach($categories as $category)
                {{-- */$x++;/* --}}
                <tr>

                    <td><a href="{{ route('shop.admin.category.index', $category->id) }}">{{ $category->name }}</a></td>
                    <td>{{ $category->description }}</td>
                    <td>{{ $category->category_slug }}</td>
                    <td>
                        <a href="{{ route('shop.admin.category.edit', $category->id) }}">
                            <button type="submit" class="btn btn-primary btn-xs">Update</button>
                        </a> /
                        {!! Form::open([
                            'method'=>'DELETE',
                            'route' => ['shop.admin.category.destroy', $category->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
    {{--<div class="pagination"> {!! $category->render() !!} </div>--}}
</div>
