<script>
    $(document).foundation();
</script>
<link rel="stylesheet" type="text/css" href="/css/foundation-datepicker.css">
<script data-cfasync="false" type="text/javascript" src="/js/foundation-datepicker.min.js"></script>
<div id="dashboard-tabs">
   
    <ul id="myshop-tabs" class="tabs inner-content-tabs" data-tab>

        <li class="tab-title active">
            <a href="#shop-products"
               class="dashboard-tabs-btn"
               data-target-name="shopproducts"
            >
                Products
            </a>
        </li>

        <li class="tab-title">
            <a href="#shop-categories"
               class="dashboard-tabs-btn"
               data-target-name="shopcategories"
            >
                Categories
            </a>
        </li>

        <li class="tab-title">
            <a href="#shop-subcategories"
               class="dashboard-tabs-btn"
               data-target-name="shopsubcategories"
            >
                Sub Categories
            </a>
        </li>

        <li class="tab-title">
            <a href="#shop-orders"
               class="dashboard-tabs-btn"
               data-target-name="shoporders">
                Orders
            </a>
        </li>
    </ul>
    <div class="tabs-content inner-content-tabs">

        <div class="content active" id="shop-products">
            @include('shop.partials.products-ajax-container')
        </div>

        <div class="content" id="shop-categories">
            @include('shop.partials.categories-ajax-container')
        </div>

        <div class="content" id="shop-subcategories">
            @include('shop.partials.subcategories-ajax-container')
        </div>

        <div class="content" id="shop-orders">
            @include('shop.partials.orders-ajax-container')
        </div>
    </div>
</div>
<script>

    $(document).ready(function () {
        var tab_btn = $('#dashboard-tabs ul#myshop-tabs > li a'),
                active_tab_id = $('#dashboard-tabs  .tabs-content > .content.active').attr('id'),
                active_tab_parent_link = $("a[href='#" + active_tab_id + "']");

        tab_btn.click(function (e) {
            e.preventDefault();
            var $target_id = $(this).attr('href');

            //doing this incase foundation is still acting up
            $('.tab-title').removeClass('active');
            $('#dashboard-tabs  .tabs-content > .content').removeClass('active');
            $($target_id).addClass('active');
            $(this).parent(".tab-title").addClass('active');

            //defined in views/dashboard/partials/dashboard-scripts.blade
            //we're assuming you're loading this file in the dashboard view...
            open_tab($(this));

        });


        active_tab_parent_link.trigger("click");
    });


</script>
