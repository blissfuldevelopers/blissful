<div class="main_content">
    <h1>Create New Product</h1>
    <hr/>

    {!! Form::open(['route' => 'shop.admin.products.store', 'class' => 'form-horizontal','method' => 'post','files' => true]) !!}

    <div class="form-group">
        {!! Form::label('type', 'Type: ', ['class' => 'col-sm-3  control-label']) !!}
        <div class="col-sm-6">
            <select name="type" placeholder=type" class="form-control">
                <optgroup label="Type">
                    <option value="1">Product</option>
                    <option value="2">Service</option>
                </optgroup>
            </select>
        </div>
    </div>
    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
        {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
        {!! Form::label('description', 'Description: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('category', 'Category: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <select name="category_id" id="category_id" placeholder=category" class="form-control">
                <optgroup label="Category">
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('subcategory', 'Sub-Category: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <select name="sub_category_id" id="sub_category_id" placeholder=subcategory" class="form-control">
                <optgroup label="Sub-Category">
                    @foreach($subcategories as $subcategory)
                        <option value="{{$subcategory->id}}">{{$subcategory->name}}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('payment_method', 'payment_method:') !!}
        <select id="payment_method" name="payment_method" class="form-control">
            <optgroup label="payment method">
                    <option value="0">Pesapal</option>
                    <option value="1">Cash on delivery</option>
            </optgroup>
        </select>
    </div>
    <div class="form-group {{ $errors->has('highlights') ? 'has-error' : ''}}">
        {!! Form::label('highlights', 'Highlights: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::textarea('highlights', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('highlights', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('tnc') ? 'has-error' : ''}}">
        {!! Form::label('tnc', 'Terms & Conditions: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::textarea('tnc', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('tnc', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('location', 'Location: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <select name="location" placeholder=location" class="form-control">
                <optgroup label="location">
                    @foreach($locations as $location)
                        <option value="{{$location->name}}">{{$location->name}}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
    <div class="form-group {{ $errors->has('qty') ? 'has-error' : ''}}">
        {!! Form::label('qty', 'Quantity: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('qty', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('qty', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
        {!! Form::label('price', 'Price: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('price', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('discount') ? 'has-error' : ''}}">
        {!! Form::label('discount', 'Discount: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('discount', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('discount', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('expiry_date') ? 'has-error' : ''}}">
        {!! Form::label('expiry_date', 'Expiry Date: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('expiry_date', null,
                        [
                        'class' => 'getdate',
                        'id'=>'expiry_date',
                        'required' => 'required',
                        'placeholder'=>date('Y-m-d',strtotime('tomorrow')),
                        'data-parsley-required-message' => 'You need to include the epxiry date',
                        'data-parsley-trigger'          => 'change focusout'

                        ]) !!}
            {!! $errors->first('event_date', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('picture') ? 'has-error' : ''}}">
        {!! Form::label('picture', 'Picture: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <input type="file" name="picture" class="form-control">
            {!! $errors->first('picture', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Create', ['class' => 'submit-btn btn btn-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
</div>
<script src="/assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">

  var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
   _container = $('main');
        add_overlay = function () {
            _container.css("position", "relative");
            var overlay = $('<div id="overlay"></div>').css({
                    position: "absolute",
                    width: "100%",
                    height: "100%",
                    top: 0,
                    left: 0,
                    "background-color": "rgba(255,255,255,0.1)" /*dim the background*/
                }),
                modal = $('<div id="this-modal"><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>Loading...</div>').css({
                    position: "fixed",
                    width: "200px",
                    height: "200px",
                    top: "50%",
                    left: "50%",
                    "color": "#e9048b",
                    "margin-left": "-150px",
                    "margin-top": "-100px",
                    "text-align": "center",
                    "z-index": "10",
                });

            modal.appendTo(overlay.appendTo(_container));

        }
        remove_overlay = function () {
            $('#overlay').hide().remove();

        }
  jQuery(document).ready(function($){
      if(!isMobile) {
          tinymce.init({
          selector: "textarea",
          theme: "modern",

          menubar: false,
          plugins: [
              "advlist autolink lists link image charmap print preview hr anchor pagebreak",
              "searchreplace wordcount visualblocks visualchars code fullscreen",
              "insertdatetime nonbreaking save table contextmenu directionality",
              "emoticons template paste textcolor colorpicker textpattern imagetools jbimages"
          ],
        toolbar: " bold italic alignleft aligncenter alignright alignjustify bullist numlist outdent indent preview",
          relative_urls: false,
          image_advtab: true,
          templates: [
              {title: 'Test template 1', content: 'Test 1'},
              {title: 'Test template 2', content: 'Test 2'}
          ]
      });
      }
      else{
        tinymce.init({
        selector: ".tinymce-text",
        theme: "modern",
        menubar: false,
        plugins: [
            "advlist autolink lists link charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime  nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools "
        ],
        toolbar: false,
        relative_urls: false,
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ]
    });
    }
  });
  $('.submit-btn').click(function(event){
    event.preventDefault();
    tinyMCE.triggerSave();

  var editorContent = tinyMCE.activeEditor.getContent();
  if (editorContent == '' || editorContent == null)
  {
    $(tinyMCE.activeEditor.getContainer())
      .css("background-color", '#ffeeee')
      .parent()
      .css({
        "background-color": '#ffeeee',
        "border": '1px solid #EC736C'
      });
    $('<span class="red">Please fill in this field </span>').insertAfter($(tinyMCE.activeEditor.getContainer()));

  }

    else{

            add_overlay();
            var form = $(this).closest('form')[0];
            var formData = new FormData(form);
            $.ajax({    
                        type:'POST',
                        url: $(this).closest('form').attr('action'),
                        data: formData,
                        // THIS MUST BE DONE FOR FILE UPLOADING
                        contentType: false,
                        processData: false,
                        // ... Other options like success and etc
                        success : function(data) {
                         remove_overlay();
                         $('.main_content').html(data);
                          
                        },
                        error:  function(data){
                            remove_overlay();
                            alert('error');
                        }
                    }); 
    }
  });

</script>

<script>
    $('#category_id').on('change', function (e) {

        var cat_id = e.target.value;

        //ajax

        $.get('/api/category-dropdown?cat_id=' + cat_id, function (data) {

            //success data
            $('#sub_category_id').empty();

            $('#sub_category_id').append(' Please choose one');

            $.each(data, function (index, subcatObj) {

                $('#sub_category_id').append('<option value ="' + subcatObj.id + '">'
                        + subcatObj.name + '</option>');


            });


        });


    });
</script>
<script type="text/javascript">
    $('#expiry_date').fdatepicker({
        format: 'yyyy-mm-dd',
    });
</script>
