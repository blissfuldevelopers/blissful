{!! HTML::script('/js/jquery-2.2.1.min.js') !!}
{!! HTML::style('/assets/dropzone/dropzone.css') !!}
{!! HTML::script('/assets/dropzone/dropzone.js') !!}
{!! HTML::script('/assets/dropzone/dropzone-config.js') !!}
<link rel="stylesheet" href="/css/foundation.css"/>
<link rel="stylesheet" href="/fonts/foundation/foundation-icons.css"/>

<div class="row">
    @foreach ($product->images as $image)
        <div class="small-12 medium-3 columns">
            <img id="gallery" src="{{url($image->file_path)}}">
            <a id="delete_image" data-tooltip aria-haspopup="true" class="has-tip" title="Delete Image" href="{{url('shop/admin/product/images/delete/' .$image->id)}}"><i class="fi-trash sm"></i></a>
            {{--<button type="button" data-my-number="{{ $image->id}}" data-reveal-id="myModal" class="small round" id="open_modal" data-toggle="modal" data-whatever="{{ $image->id}}" data-target="#myModal" >✚</button>--}}
        </div>
    @endforeach
</div>
<div class="row">
    <div class="col-md-offset-1 col-md-10">
        <div class="jumbotron how-to-create" >

            <h3>Images <span id="photoCounter"></span></h3>
            <br />

            {!! Form::open(['url' => route('products.upload'), 'class' => 'dropzone', 'files'=>true, 'id'=>'real-dropzone']) !!}

            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <input type="hidden" name="product_id" value="{{ $product->id}}"/>
            <div class="dz-message">

            </div>
            <div class="dropzone-previews" id="dropzonePreview"></div>

            <h4 style="text-align: center;color:#428bca;">Drop images in this area  <span class="glyphicon glyphicon-hand-down"></span></h4>

            {!! Form::close() !!}

        </div>
        <div class="jumbotron how-to-create">
            <ul>
                <li>Images are uploaded as soon as you drop them</li>
                <li>Maximum allowed size of image is 8MB</li>
            </ul>

        </div>
    </div>
</div>