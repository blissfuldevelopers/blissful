<link rel="stylesheet" href="/css/dataTables.foundation.min.css">
<h1>Products<a href="{{ route('shop.admin.products.create') }}" class="btn btn-primary pull-right btn-sm">Add New
        Product</a></h1>
@include('flash::message')
<div class="table">
    <table id ="products_table" class="table table-striped table-bordered small-12">
        <thead>
        <tr>
            <th>Item</th>
            <th>Name</th>
            <th>Price</th>
            <th>Category</th>
            <th>Subcategory</th>
            <th>Discount</th>
            <th>Created on</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        {{-- */$x=0;/* --}}
        @if($products)
            @foreach($products as $product)
                {{-- */$x++;/* --}}
                <tr>
                    <td>
                        <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$product->picture}}" style="width: 75px;height: 75px;">
                    </td>
                    <td><a href="{{ route('shop.admin.products.index', $product->id) }}">{{ $product->name }}</a></td>
                    <td>{{ $product->price }}</td>
                    <td>{{ $product->category->name }}</td>
                    <td>{{ $product->subcategory ? $product->subcategory->name : 'No Subcategory'}}</td>
                    <td>{{ $product->discount }}</td>
                    <td>{{ $product->created_at }}</td>
                    <td>
                        <a href="{{ route('shop.admin.products.edit', $product->id) }}">
                            <button type="submit" class="btn btn-primary btn-xs">Update</button>
                        </a> -
                        <a href="{{ route('products.images', $product->id) }}">
                            <button type="submit" class="btn btn-primary btn-xs">Images</button>
                        </a> -
                        {!! Form::open([
                            'method'=>'DELETE',
                            'route' => ['shop.admin.products.destroy', $product->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
<script data-cfasync="false" type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
<script data-cfasync="false" type="text/javascript" src="/js/dataTables.foundation.min.js"></script>
<script data-cfasync="false" type="text/javascript">
    (function($) {
        $('#products_table').DataTable();
    })(jQuery);
</script>
