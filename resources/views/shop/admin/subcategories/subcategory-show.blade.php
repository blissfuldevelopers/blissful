<h1>subcategory<a href="{{ route('shop.admin.subcategory.create') }}" class="btn btn-primary pull-right btn-sm">Add New subcategory</a></h1>
<div class="table">
    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Category</th>
            <th>Slug</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        {{-- */$x=0;/* --}}
        @if($subcategories)
            @foreach($subcategories as $subcategory)
                {{-- */$x++;/* --}}
                <tr>

                    <td><a href="{{url('/shop/admin/subcategory', $subcategory->id)}}">{{ $subcategory->name }}</a></td>
                    <td>{{ $subcategory->description }}</td>
                    <td>{{ $subcategory->category->name}}</td>
                    <td>{{ $subcategory->subcategory_slug}}</td>
                    <td>
                        <a href="{{ route('shop.admin.subcategory.edit', $subcategory->id) }}">
                            <button type="submit" class="btn btn-primary btn-xs">Update</button>
                        </a> /
                        {!! Form::open([
                            'method'=>'DELETE',
                            'route' => ['shop.admin.subcategory.destroy', $subcategory->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
    {{--<div class="pagination"> {!! $category->render() !!} </div>--}}
</div>