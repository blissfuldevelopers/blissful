<div class="container-widget">

    <ul class="list-group">
        <div class="small-12 columns white_background admin-messages">
            <li class="list-group-item"><strong>Order ID | Pesapal Merchant Reference No.: </strong>{{ $order->id }}</li>
        </div>
        <div class="small-12 columns white_background admin-messages">
            <li class="list-group-item"> <strong>Client Name: </strong>{{$order->user->first_name}} {{@$order->user->last_name}}</li>
        </div>
        <div class="small-12 columns white_background admin-messages">
            <li class="list-group-item"> <strong>Phone: </strong>{{$order->phone}}</li>
        </div>
        <div class="small-12 columns white_background admin-messages">
            <li class="list-group-item"><strong>Amount Received: </strong>{{ $order->total}}</li>
        </div>
        <div class="small-12 columns white_background admin-messages">
            <li class="list-group-item"><strong>Payment Method: </strong>{{ $order ? $order->payment_method : 'unknown'}}</li>
        </div>
        <div class="small-12 columns white_background admin-messages">
            <li class="list-group-item"><strong>Status: </strong>{{ $order ? $order->status : 'unknown'}}</li>
        </div>
        <div class="small-12 columns white_background">
            <strong>Order Details</strong>
            <table class="table cart-table hide-for-small-only">
                        <thead>
                        <tr>
                            <th>Item</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Subcategory</th>
                            <th>Quantity</th>
                            <th>Price</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($order->details as $detail)
                            <tr>
                                <td>
                                    <img src="/{{$detail->product->picture}}" style="width: 75px;height: 75px;">
                                </td>
                                <td>
                                    {{ $detail->product->name }}
                                </td>
                                <td>
                                    {{ $detail->product->category->name }}
                                </td>
                                <td>
                                    {{ @$detail->product->subcategory->name }}
                                </td>
                                <td>
                                    {{ $detail->qty }}
                                </td>
                                <td>
                                    {{ $detail->price * $detail->qty }}
                                </td>
                            </tr>
                            @endforeach
                            <tr>
                                <td>Shipping fees</td>
                                <td>{{$order->shipping_fees }}</td>
                            </tr>
                            <tr>
                                <td> <strong>Total</strong> </td>
                                <td> <strong>{{ $order->total}}</strong> </td>
                            </tr>
                        </tbody>
            </table>
        </div>
        
</ul>
</div>
