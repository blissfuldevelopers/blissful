<div class="table">
    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th>Order Id</th>
            <th>User Name</th>
            <th></th>
            <th>Amount</th>
            <th>Status</th>
            <th>Received On</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        {{-- */$x=0;/* --}}
        @if($orders)
            @foreach($orders as $order)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $order->id }}</td>
                    <td><a style="color:#0000ff;" href="{{ route('users.show', $order->user->id) }}">{{ $order->user->first_name}}</a></td>
                    <td><a style="color:#0000ff;" href="{{route('shop.admin.orders.show', $order->id)}}">details</a></td>
                    <td>{{ $order->total }}</td>
                    <td>{{ $order ? $order->status : 'unknown' }}</td>
                    <td>{{ $order->created_at }}</td>
                    <td>
                        {{--<a href="{{ route('shop.admin.orders.edit', $order->id) }}">--}}
                            {{--<button type="submit" class="btn btn-primary btn-xs">Update</button>--}}
                        {{--</a> /--}}
                        {!! Form::open([
                            'method'=>'DELETE',
                            'route' => ['shop.admin.orders.destroy', $order->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
    <div class="pagination"> {!! $orders->render() !!} </div>
</div>