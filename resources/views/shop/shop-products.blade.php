@section('content')
        <!-- START OF MAIN SECTION -->
<main role="main" id="shop">
    <!-- ad banner -->
    <div class="promo">
        <div class="row">
            <div class="small-12 columns text-center">
                <h4>Shop {{ @$cat->name }}</h4>
            </div>
        </div>
    </div>

    <!-- popular deals -->

    <section class="pop-deals">
        <div class="row">
            <div class="small-12 columns">
                @if(count($products))
                    <h3>Products</h3>
                    <ul>
                        @foreach($products as $product)
                            <li>
                                <img src="/{{ $product->picture }}" style="height: 220px;">
                                <div class="d-dets">
                                    <h4>{{ $product->name }}</h4>
                                    <!--<h5>Travelsafe Tours</h5>-->
                                    <div class="d-prices">
                                        <ul>
                                            <li class="actual-p">{{ $product->price }}</li>
                                            @if($product->sale_price )
                                                <li class="deal-p">{{ $product->sale_price }}</li>
                                            @else
                                                <li class="deal-p">{{ $product->price }}</li>
                                            @endif
                                        </ul>
                                    </div>
                                    <div class="d-buy">
                                        <a href="/shop/dealview/{{ $product->id }}" class="button buy">DETAILS</a>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                @else
                    <div class="col-md-12 text-center form-group">
                        <h1 class="text-primary">There are no items in this category. </h1>
                        <p>Please see our other categories might have your products.</p>
                    </div>

                    @if($category)
                        <ul>
                            @foreach($category as $product)
                                <?php $product = (object)$product; ?>
                                <li style="background: white">
                                    <a href="/shop/category/{{ $product->id }}">
                                        <h4 style="text-align: center">{{ $product->name }}</h4>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                @endif
            </div>
        </div>
    </section>
</main>
<!-- END OF MAIN SECTION -->
@stop