@extends('page.home-layout')
@section('title')
    {{ $product->name }} for Sale in Kenya
@endsection
@section('header-content')
  <?php
    $header_class = 'contentpg-intro';
  ?>
<div class="row">
  <div class="small-12 columns">
      @if(@$categories)
          <ul class="hide-for-small-only dropdown menu shop-category-dropdown" data-dropdown-menu>
              <li><a href="/shop">All</a></li>
              @foreach($categories as $data)
                  @if(@$cat->id == $data->id)
                      <li><a href="/shop/category/{{$data->category_slug}}">{{$data->name}}</a>
                        @if(count($data->subcategories) > 0)
                          <ul id="{{$data->category_slug}}" class="menu sub-item">
                              @foreach($data->subcategories as $subcategory)
                                  <li>
                                      <a href="/shop/subcategory/{{$subcategory->subcategory_slug}}">{{$subcategory->name}}</a>
                                  </li>
                              @endforeach
                          </ul>
                        @endif
                      </li>
                  @else
                      <li><a href="/shop/category/{{$data->category_slug}}">{{$data->name}}</a>
                        @if(count($data->subcategories) > 0)
                          <ul id="{{$data->category_slug}}" class="menu sub-item">
                              @foreach($data->subcategories as $subcategory)
                                  <li>
                                      <a href="/shop/subcategory/{{$subcategory->subcategory_slug}}">{{$subcategory->name}}</a>
                                  </li>
                              @endforeach
                          </ul>
                        @endif
                      </li>
                  @endif
              @endforeach
          </ul>
      @endif
  </div>
</div>
@endsection
@section('content')
    <!-- START OF MAIN SECTION -->

    <!-- START OF BODY -->
    <main aria-role="main" class="shop-item" id="shop-main">
        <!-- START OF SHOP ITEM HEADER SECTION -->
        <section class="title-header">
            <div class="row">
                <div class="small-12 columns">
                    <h2>{{ $product->name }}</h2>
                    <h6><a href="/shop/category/{{ $product->category->category_slug }}">{{ $product->category->name }}
                            | {{ @$product->subCategory->name }}</a></h6>
                </div>
            </div>
        </section>
        <!-- END OF SHOP ITEM HEADER SECTION -->
        <!-- START OF SHOP CART SECTION -->
        <section class="item-summary">
            <div class="row">
                <!-- Item Image -->
                <div class="small-12 medium-8 columns">
                    <div class="lightslider">
                        <div>
                            <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{ $product->picture }}">
                        </div>
                        @foreach (@$product->images as $image)
                            <div>
                                <img src="{{url('https://s3.eu-central-1.amazonaws.com/blissful-ke/' .$image->file_path)}}">
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- Cart Details -->
                <div class="small-12 medium-4 columns cart">
                    <div class="row">
                        <div class="wrap">
                            <!-- quantity -->
                            @if ($product->category->package != 1)
                            <div class="small-12 columns">
                                <h4 class="discount">{{ round((($product->price -  $product->sale_price)/ $product->price)*100) }}% <span class="sup">Off</span></h4>
                            </div>
                            @endif
                            <div class="small-12 columns">
                                <h6><strong>Highlights</strong></h6>
                                <p>
                                    {!! $product->highlights !!}
                                </p>
                            </div>
                            <div hidden class="small-12 columns">
                                <h6><strong>Quantity</strong></h6>
                                <p>
                                    {{$product->qty}}
                                </p>
                            </div>
                            @if ($product->category->package != 1)
                            <div class="small-12 columns">
                                <h6><strong>Rating</strong></h6>
                                <ul class="stars">
                                    <li class="inactive"><span class="fi-heart "></span></li>
                                    <li class="inactive"><span class="fi-heart "></span></li>
                                    <li class="inactive"><span class="fi-heart "></span></li>
                                    <li class="inactive"><span class="fi-heart "></span></li>
                                    <li class="inactive"><span class="fi-heart "></span></li>
                                </ul>
                            </div>
                            @endif
                            <div class="small-12 columns">
                                <!-- Price -->
                                @if ($product->category->package != 1)
                                <div class="item-price">
                                    @if( $product->sale_price )
                                        @if($product->sale_price != $product->price)
                                        <h5 class="strike">Ksh {{ number_format($product->price) }}</h5>
                                        @endif
                                        <h5 class="sale-price">Ksh {{ number_format($product->sale_price) }}</h5>
                                     @else
                                        <h5 class="sale-price">Ksh {{ number_format($product->price) }}</h5>
                                     @endif
                                </div>
                                @else
                                <div class="item-price">
                                    <h5 class="sale-price">From: Ksh {{ number_format($product->price) }}</h5>
                                </div>
                                @endif
                                <!-- overview -->
                                <!-- Cart Functions -->
                                <div class="small-12 columns ">
                                @if ($product->category->package == 1)
                                    <button onclick="showForm()"
                                            class="button button-warning small-12">
                                        Enquire More
                                    </button>

                                    <form action="/honeymoon-subscription/create" method="post" id="subscribe_form_2" class="contact-form" >

                                        <h6>Leave your details below, and we'll get back to you :)</h6>
                                        <div class="form-group">
                                          <input required data-parsley-trigger="change focusout" class="form-control" type="text" name="user_name" id="subscribe_name_2" placeholder="Enter your name">
                                        </div>
                                        <div class="form-group">
                                          <input required data-parsley-trigger="change focusout" data-parsley-type = "email"  class="form-control" type="email" name="email" id="subscribe_email_2" placeholder="Enter your email">
                                        </div>
                                        <div class="loading loading--double"></div>
                                        <div class="form-group">
                                          <input required data-parsley-trigger="change focusout" class="form-control" type="number" name="number" id="subscribe_email_2" placeholder="Enter your Phone number">
                                        </div>
                                        <input type="hidden" name="product_id" value="{{$product->id}}">
                                        <button class="btn btn-primary save-subscription" type="submit" id="subscribe_submit_2" data-loading-text="•••">Subscribe</button>
                                  </form>
                                @else
                                    <button onclick="addToCartFn({{ $product->id }})"
                                            class="button button-warning small-12">
                                        Add to Cart
                                    </button>
                                @endif
                                </div>
                                <div hidden class="small-12 columns">
                                    <a href="#" class="button query">Ask a Question</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END OF SHOP CART SECTION -->
        <!-- START OF SHOP ITEM DESCRIPTORS -->
        <section class="item-desc">
            <div class="row">
                <div class="small-12 medium-8 columns">
                    <ul class="tabs" data-tab id="shop-item-tabs">
                        <li class="tab-title active"><a href="#panel1">DESCRIPTION</a></li>
                        <li class="tab-title"><a href="#panel2">TERMS & CONDITIONS</a></li>
                    </ul>
                    <div class="tabs-content">
                        <div class="content active" id="panel1">
                            <h5>{{ $product->name }}</h5>
                            <?php echo html_entity_decode($product->description) ?>
                        </div>
                        <div class="content" id="panel2">
                            <p>{!! $product->tnc !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END OF SHOP ITEM DESCRIPTORS -->

        <!-- popular deals -->
        <section class="browse-deals">
            <div class="row">
                <div class="small-12 columns">
                    <h3 class="shop_desc">Similar Products</h3>
                    <ul class="shop-items small-12 medium-10 small-centered columns">
                        @foreach($similar as $product)
                            <li class="ripple">
                                <a href="/shop/product/{{$product->product_slug}}">
                                    <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{ $product->picture }}" style="height: 220px;"/>
                                    <div class="wrap">
                                        <h4 id="v-name">{{ $product->name }}</h4>
                                        @if( $product->sale_price && $product->sale_price !== $product->price )
                                            <h6>
                                                Ksh {{ number_format($product->sale_price) }}
                                            <h6>
                                        @else
                                            <h6>
                                                Ksh {{ number_format($product->price) }}
                                            </h6>
                                        @endif                            
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </section>
        <!-- END OF MAIN SECTION -->
    </main>
@section('scripts')
<script defer src="{{ elixir('js/shop-product.js')}}" type="text/javascript"></script>
@endsection
@stop