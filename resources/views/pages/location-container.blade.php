<style type="text/css">
	.locations-container{
		background-color: #fff;
		width: 100%;
		height: 100%;
		position: fixed;
		-webkit-transform: none;
	    transform: none;
	    will-change: transform;
	    z-index: 160;
	    transition: all 1s ease-in !important;
	}
	.locations-container--hidden {
	    -webkit-transform: translateY(-120%) !important;
	    transform: translateY(-120%) !important;
	}
	.locations-container h1{
		text-align: center;
    	font-size: 1.8rem;
	}
</style>

<div class="locations-container locations-container--hidden">
</div>
<script type="text/javascript">
	$.ajax({
        type: "GET",
        url: "/user-location/{{Auth::user()->id}}",
        success:function(data){
        	console.log('success')
        	$('.locations-container').html(data);
        	$('.locations-container').removeClass('locations-container--hidden');
        },
        error:function (xhr, ajaxOptions, thrownError){
            alert(thrownError); //throw any errors
        }
        });
</script>