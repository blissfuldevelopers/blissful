@extends('page.home-layout')
@section('title')
    Search results for {{$mainSearch}}
@stop
@section('metas')
@endsection
@section('header-content')
  <?php
    $header_class = 'catpg-intro';
  ?>
    <!-- intro -->
    <div class="row">
      <div class="medium-10 small-12 medium-centered columns">
        <div class="row">
          <div class="small-12 medium-8 columns">
            <h2>Search results for<br><span class="ucase">{{$mainSearch}}</span></h2>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('content')
      <!-- how it works -->
      <div class="catpg-list">
        <div class="row">
          <div class="small-12 columns">
            <hr>
            <h3>View our list of professionals</h3>
            <ul class="vendors">
                
                @foreach ($arrays as $vendor)
                    <li class="small-12 columns">
                        <div class="prof-wrap">
                            <div class="w1">
                                <h5>{{$vendor->first_name}}</h5>
                                <h6>{{isset($vendor->location->address) ? $vendor->location->address : $vendor->profile->location}}</h6>
                                <ul class="stars">
                                  @for ($i=1; $i <= 5 ; $i++)
                                  <li>{!! ($i <= $vendor->profile->rating_cache) ? '&starf;' : '&#x02729;'!!} </li>
                                  @endfor
                                </ul>
                                <span class="reviews">({{$vendor->profile->reviews->count()}} {{($vendor->profile->reviews->count() == 1 ? 'Review' : 'Reviews')}} )</span>
                            </div>
                            <div class="w2">
                                <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$vendor->profile->profilePic}}" class="profile">
                            </div>
                        </div>
                        <div class="pdesc-wrap">
                          <p>{{str_limit($vendor->profile->description,40)}}</p>
                          <a href="/vendor/{{$vendor->slug}}" class="button">Request Quote</a>
                        </div>
                    </li>
                @endforeach
            </ul>
          </div>
        </div>
      </div>
    </div>

@endsection
@section('scripts')

@endsection