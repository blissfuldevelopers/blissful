@extends('page.home-layout')
@section('title')
    Contact Us
@endsection

@section('header-content')
  <?php
    $header_class = 'contentpg-intro';
  ?>
@endsection
@section('content')
    <!-- main content  -->
    <div class="contentpg-main">
        <div class="row">
          <div class="small-12 columns">
            <div class="wrapper">
            <div class="small-12 medium-7 large-6 columns small-centered">
                @include('admin.partials.errors')
                @include('admin.partials.success')
                <h2>We’d love to hear from you!</h2>
                <p> </p>
                <p>Just fill out the contact us form below with your question, concern or anything else that is on your mind!</p>
                <p>If you’d like to list your business on Blissful, please click the link 
                    <a href="{{ route('auth.vendor-register') }}" class="tiny"> Sign Up</a>
                </p>
                
                <h3>Contact Us</h3>
                <form action="/contact" method="post" data-abide novalidate>
                    <div class="row">

                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <div>
                            <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{{ old('name') }}" autofocus data-live-validate required>
                            <span class="form-error">Please include your name</span>
                        </div>
                        <div>
                            <input type="email" id="email" name="email" value="{{ old('email') }}" placeholder="Email"  required data-live-validate>
                            <span class="form-error">Please include a valid email</span>
                        </div>
                        <div>
                            <input type="tel" class="form-control" id="phone" name="phone" value="{{ old('phone') }}" pattern="integer" placeholder="Phone Number"  required data-live-validate>
                            <span class="form-error">Please include a valid phone number</span>
                        </div>
                        <div>
                            <textarea rows="3" id="message" name="message" placeholder="Message" pattern=".{6,}" data-live-validate required>{{ old('message') }}</textarea>
                            <span class="form-error">Please include your message. Should be a minimum of 6 characters</span>
                        </div>
                        <div class="form-group small-12 medium-12 columns">
                            <button type="submit" class="button button-primary small-12 columns">Contact Us</button>
                        </div>
                    </div>
                    </div>
                </form>
            </div>
                
            </div>
          </div>
        </div>
    </div>
@endsection