@extends('page.zurb-layout')
@section('title')
 {{($profile->user->first_name)}}
@endsection
@section('head')
<meta property="og:url"           content="http://dev.blissful.co.ke/vendors/{{($profile->user->slug)}}" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="Blissful" />
  <meta property="og:description"   content="Your description" />
  <meta property="og:image"         content="http://dev.blissful.co.ke/{{$profile->profilePic}}" />

@endsection
@section('styles')
    {!! HTML::style('/assets/css/parsley.css') !!}
    {!! HTML::style('/css/rating.css') !!}
    <link rel="stylesheet" type="text/css" href="/css/lightslider.min.css">
    <link rel="stylesheet" href="/css/default-skin.css"/>
    <link rel="stylesheet" href="/css/photoswipe.css"/>

@stop

@section('content')
  <main role="main" id="vendor" class="lightblue">
  @include('includes.status-zurb')
  @include('includes.errors-zurb')
  <!-- Vendor Header -->
  <section class="v-header">
    <img src="/img/catheaders/1.jpg">
    <div class="row">
      <div class="small-12 columns">
        <div class="row">
          <div class="small-6 columns">
            <h2>{{($profile->user->first_name)}}</h2>
            <ul class="h-rev">
              <li>
                <ul class="stars">
                  @for ($i=1; $i <= 5 ; $i++)
                    <li class="active"> <span class="fi-heart {{ ($i <= $profile->rating_cache) ? '' : 'inactive'}}"></span></li>
                  @endfor
                </ul>
              </li>
              <li>
                <span>{{$profile->rating_count}} {{ Str::plural('review', $profile->rating_count)}}</span><span>&#47;</span>
                <span>
                  @if(!Auth::check())
                  <a href="#" data-reveal-id="authModal"> Review Us</a>
                  @else
                  <a href="#reviews-anchor" id="open-review-box"> Review Us</a>
                  @endif
                </span>
              </li>
            </ul>
          </div>
          <div class="small-6 columns">
            @if(!Auth::check())
            <a href="#" data-reveal-id="authModal" class="hide-for-small-only"><button class="dark_orange_background ripple v-contact">Contact</button></a>
            <a href="#" data-reveal-id="authModal" class="show-for-small-only"><button class="dark_orange_background ripple v-contact">Contact</button></a>
            @else
            <a href="#" data-reveal-id="myModal" class="hide-for-small-only"><button class="dark_orange_background ripple v-contact">Contact</button></a>
            <a href="#" data-reveal-id="myModal" class="show-for-small-only"><button class="dark_orange_background ripple v-contact">Contact</button></a>
            @endif
            <div id="myModal" class="reveal-modal fifty" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">

              <div class="tabs-content messagecontent">
                <div class="content active" id="panel1">
                  <h5 id="modalTitle">Send Message</h5>
                  <div class="lead">
                    <i class="to">To:</i>
                    <div class="pimg placard taggy">
                      <img src="/{{$profile->profilePic}}" alt=" {{($profile->user->first_name)}} pic">
                      <i>{{($profile->user->first_name)}}</i>
                    </div>
                  </div>
                  <div class="row">
                    {!! Form::open(['route' => 'messages.store']) !!}
                    <div>
                        <div class="form-group">
                            <input type="hidden" name="recipients[]" id="recipients" value="{{($profile->user->id)}}">
                            <input type="hidden" name="email" id="email" value="{{($profile->user->email)}}">
                            <input type="hidden" name="first_name" id="first_name" value="{{($profile->user->first_name)}}">
                            <input type="hidden" name="phone" id="phone" value="{{($profile->phone)}}">
                        </div>
                        <!-- Subject Form Input -->
                        <div class="form-group">
                            {!! Form::label('subject', 'Subject', ['class' => 'control-label','style'=>'width:75%']) !!}
                            {!! Form::text('subject', null, ['class' => 'form-control','style'=>'width:90%']) !!}
                        </div>
                        <!-- Message Form Input -->
                        <div class="form-group">
                            {!! Form::label('message', 'Message', ['class' => 'control-label']) !!}
                            {!! Form::textarea('message', null, ['rows'=>'3', 'class' => 'form-control']) !!}
                        </div>
                        <!-- Submit Form Input -->
                        <div class="form-group">
                            {!! Form::submit('Submit', ['class' => 'button tiny right form-control']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
              <a class="close-reveal-modal md" aria-label="Close">&#215;</a>
            </div>
            <div id="authModal" class="reveal-modal fifty" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
              <div class="content" id="panel2">
                  <div class="modalBackground"></div>
                  <div class="tabs-content messagecontent">
                    <div class="row">
                      <div class="large-4 small-4 medium-4 columns">
                          <a class="show-login white" href="javascript:showonlyone('newboxes1');">Sign In</a>
                          <span class="white">or</span>
                          <a class="show-register white" href="javascript:showonlyone('newboxes2')">Register</a>
                          <div class="oauths">
                            <p class="or-social white">Or Sign In with</p>
                            <div class="button-group" data-grouptype="OR">
                            <a href="{{ route('social.redirect', ['provider' => 'facebook']) }}" class="button tiny circle facebookcircle" type="submit"><img src="/img/fob.png"></a>
                            <a href="{{ route('social.redirect', ['provider' => 'google']) }}" class="button tiny circle googlecircle" type="submit"><img src="/img/Gee.svg" style="width:20px; height:20px; fill:#fff;"></a>
                            </div>
                          </div>

                      </div>
                      <div class="large-8 small-8 medium-8 columns">
                            {!! Form::open(['url' => route('auth.login-review'), 'class' => 'newboxes', 'id'=>'newboxes1', 'data-parsley-validate' ] ) !!}

                            @include('includes.status')

                            <label for="inputEmail" class="sr-only">Email address</label>
                            {!! Form::email('email', null, [
                                'class'                         => 'form-control',
                                'placeholder'                   => 'Email address',
                                'required',
                                'id'                            => 'inputEmail',
                                'data-parsley-required-message' => 'Email is required',
                                'data-parsley-trigger'          => 'change focusout',
                                'data-parsley-type'             => 'email'
                            ]) !!}

                            <label for="inputPassword" class="sr-only">Password</label>
                            {!! Form::password('password', [
                                'class'                         => 'form-control',
                                'placeholder'                   => 'Password',
                                'required',
                                'id'                            => 'inputPassword',
                                'data-parsley-required-message' => 'Password is required',
                                'data-parsley-trigger'          => 'change focusout',
                                'data-parsley-minlength'        => '6',
                                'data-parsley-maxlength'        => '20'
                            ]) !!}
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('remember', 1) !!} Remember me

                                </label>
                            </div>
                            <p><a href="{{ route('auth.password') }}">Forgot password?</a></p>

                            <button type="submit" class="tiny right">Sign in</button>
                          {!! Form::close() !!}
                          {!! Form::open(['url' => route('auth.register-post'), 'id'=>'newboxes2', 'class' => 'newboxes form-signin', 'data-parsley-validate','style'=>'display:none;' ] ) !!}
                            <label for="inputEmail" class="sr-only">Email address</label>
                            {!! Form::email('email', null, [
                                'class'                         => 'form-control',
                                'placeholder'                   => 'Email address',
                                'required',
                                'id'                            => 'inputEmail',
                                'data-parsley-required-message' => 'Email is required',
                                'data-parsley-trigger'          => 'change focusout',
                                'data-parsley-type'             => 'email'
                            ]) !!}

                            <label for="inputFirstName" class="sr-only">First name</label>
                            {!! Form::text('first_name', null, [
                                'class'                         => 'form-control',
                                'placeholder'                   => 'First name',
                                'required',
                                'id'                            => 'inputFirstName',
                                'data-parsley-required-message' => 'First Name is required',
                                'data-parsley-trigger'          => 'change focusout',
                                'data-parsley-pattern'          => '/^[a-zA-Z]*$/',
                                'data-parsley-minlength'        => '2',
                                'data-parsley-maxlength'        => '32'
                            ]) !!}

                            <label for="inputLastName" class="sr-only">Last name</label>
                            {!! Form::text('last_name', null, [
                                'class'                         => 'form-control',
                                'placeholder'                   => 'Last name',
                                'required',
                                'id'                            => 'inputLastName',
                                'data-parsley-required-message' => 'Last Name is required',
                                'data-parsley-trigger'          => 'change focusout',
                                'data-parsley-pattern'          => '/^[a-zA-Z]*$/',
                                'data-parsley-minlength'        => '2',
                                'data-parsley-maxlength'        => '32'
                            ]) !!}


                            <label for="inputPassword" class="sr-only">Password</label>
                            {!! Form::password('password', [
                                'class'                         => 'form-control',
                                'placeholder'                   => 'Password',
                                'required',
                                'id'                            => 'regPassword',
                                'data-parsley-required-message' => 'Password is required',
                                'data-parsley-trigger'          => 'change focusout',
                                'data-parsley-minlength'        => '6',
                                'data-parsley-maxlength'        => '20'
                            ]) !!}


                            <label for="inputPasswordConfirm" class="sr-only has-warning">Confirm Password</label>
                            {!! Form::password('password_confirmation', [
                                'class'                         => 'form-control',
                                'placeholder'                   => 'Password confirmation',
                                'required',
                                'id'                            => 'inputPasswordConfirm',
                                'data-parsley-required-message' => 'Password confirmation is required',
                                'data-parsley-trigger'          => 'change focusout',
                                'data-parsley-equalto'          => '#regPassword',
                                'data-parsley-equalto-message'  => 'Not same as Password',
                            ]) !!}

                            <button type="submit" class="tiny right">Register</button>
                          {!! Form::close() !!}
                      </div>
                    </div>
                  </div>
                </div>
              <a class="close-reveal-modal md" aria-label="Close">&#215;</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- start of vendor section -->
  <section class="ven-main">
    <div class="row">
      <!-- category page filters -->
      <div class="medium-3 large-3 columns v-desc">
        <div class="row">
          <div class="small-12 columns">
          <h5>VENDOR DESCRIPTION</h5>
          <div class="desc-wrap white_background">
            <div class="small-12 columns">
              <div class="desc">  <!-- description section -->
                <p>{{$profile->description}}</p>
              </div>
              <div class="services">  <!-- services section -->
                <h6>SERVICES BY THIS VENDOR</h6>

                <ul>
                @foreach($profile->classifications as $classification )
                        <a href="/{{($classification->slug)}}"><li>{{$classification->name}}</li></a>
                @endforeach
                </ul>
              </div>
            </div>
          </div>
        </div>

        </div>
      </div>
      <!-- vendor listing details-->
      <div class="small-12 medium-9 large-9 columns">
        <!-- vendor page cookie crumbs -->
        <ul class="breadcrumbs">
          <li><a href="/">Home</a></li>
          <li class="current"><a href="#">{{($profile->user->first_name)}}</a></li>
        </ul>
        <!-- vendor gallery view -->
        <div class="gallery">
          <!-- <div class="row"> -->
            <!-- <div class="small-12 medium-9 large-9 columns"> -->
                @foreach ($portfolio as $port)
                <div class="cough" id="{{$port->name}}">
                  <ul class="lightSlider">
                    @foreach ($port->images as $image)
                    <li data-thumb="{{url('/gallery/images/thumbs/' .$image->file_name)}}">
                      <img src="{{url('/gallery/images/' .$image->file_name)}}">
                    </li>
                    @endforeach
                  </ul>
                </div>
                @endforeach
            <!-- </div> -->
            <!-- <div class="small-12 medium-3 large-3 columns slides"> -->
              <!-- .. -->
            <!-- </div> -->
          <!-- </div> -->

            </div>
            <!-- vendor listing additional info -->
            <div class="v-desc-wrap">
              <div class="row">
                <div class="small-12 medium-9 large-9 columns">
                  <h3>More About Listing</h3>
                  <p>{{$profile->description}}</p>
                </div>
                <div class="small-12 medium-3 large-3 columns">
                  <ul>
                    <!-- <li>Response rate: <span>100%</span></li>
                    <li>Response time: <span>Within an hour</span></li> -->
                  </ul>
                </div>
              </div>
            </div>
                    <div class="small-12 columns">
          <div class="misc-wrap">
            <div class="o-gal">  <!-- special features section -->
              <h6>Other Galleries by this vendor</h6>
              @if($galleries->count() >0)
              <div class="row"></div>
              @foreach ($galleries as $gallery)
              <div class="small-12 medium-3 my-gallery placard" itemscope itemtype="http://schema.org/ImageGallery">
                      <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="{{url($gallery->gallery_image)}}" itemprop="contentUrl" data-size="964x1024">
                              <img class="sideimg" src="{{url($gallery->gallery_image)}}" itemprop="thumbnail" alt="Image description" />
                              <p><span class="left">{{$gallery->name}}</span> <span class="right">{{$gallery->images()->count()}} Photos</span></p>
                          </a>
                           <figcaption itemprop="caption description">{{($gallery->description)}}</figcaption>
                      </figure>
                      @foreach ($gallery->images as $image)
                      <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                      <a href="{{url($image->file_path)}}" itemprop="contentUrl" data-size="964x1024"></a>
                      <img src="{{url($image->file_path)}}" itemprop="thumbnail" alt="Image description" style="display:none;" />
                      <figcaption itemprop="caption description">{{ $image->caption}}</figcaption>
                      </figure>
                      @endforeach
              </div>
              @endforeach
              </div>
              @else
              <span class="sad-gallery"><img src="/img/sad.svg"> No other Gallery</span>
              @endif
      <!-- Root element of PhotoSwipe. Must have class pswp. -->
                <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

                    <!-- Background of PhotoSwipe.
                         It's a separate element, as animating opacity is faster than rgba(). -->
                    <div class="pswp__bg"></div>

                    <!-- Slides wrapper with overflow:hidden. -->
                    <div class="pswp__scroll-wrap">

                        <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
                        <!-- don't modify these 3 pswp__item elements, data is added later on. -->
                        <div class="pswp__container">
                            <div class="pswp__item"></div>
                            <div class="pswp__item"></div>
                            <div class="pswp__item"></div>
                        </div>

                        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                        <div class="pswp__ui pswp__ui--hidden">

                            <div class="pswp__top-bar">

                                <!--  Controls are self-explanatory. Order can be changed. -->

                                <div class="pswp__counter"></div>

                                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                                <button class="pswp__button pswp__button--share" title="Share"></button>

                                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                                <!-- element will get class pswp__preloader--active when preloader is running -->
                                <div class="pswp__preloader">
                                    <div class="pswp__preloader__icn">
                                        <div class="pswp__preloader__cut">
                                            <div class="pswp__preloader__donut"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                <div class="pswp__share-tooltip"></div>
                            </div>

                            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                            </button>

                            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                            </button>

                            <div class="pswp__caption">
                                <div class="pswp__caption__center"></div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
            <!-- <div class="ame">   --> <!-- amenities section -->
            <!--   <h6>AMENITIES</h6>
              <ul>
                <li>Floodlights</li>
                <li>Back Up Electricity</li>
                <li>Toilets</li>
                <li>Wi-fi</li>
                <li>Children’s Play Area</li>
              </ul>
            </div> -->
          </div>
        </div>
          </div>
        </div>

          <div class="well" id="reviews-anchor">
              <div class="row">
                  <div class="small-12 medium-9 large-9 right">
                      @if(Session::get('errors'))
                          <div data-alert class="alert-box alert">
                              <a href="#" class="close">&times;</a>
                              There were errors while submitting this review:
                              @foreach($errors->all(":message") as $message)
                                <li>
                                  {{$message}}
                                </li>
                              @endforeach
                          </div>
                      @endif
                      @if(Session::has('review_posted'))
                          <div data-alert class="alert-box info">
                              <a href="#" class="close">&times;</a>
                              Your review has been posted!
                          </div>
                      @endif
                      @if(Session::has('review_removed'))
                          <div data-alert class="alert-box alert">
                              <a href="#" class="close">&times;</a>
                              Your review has been removed!
                          </div>
                      @endif
                  </div>
              </div>
              <div class="row text-right">

              </div>
              <div class="row" id="post-review-box" style="display:none;">
                  <div class="small-12 medium-9 large-9 right">
                      {!! Form::open(['url' => '/vendor/'.$profile->user->slug, 'id'=>'form','method' => 'POST'])!!}
                      {!!Form::hidden('rating', null, array('id'=>'ratings-hidden'))!!}
                      {!!Form::textarea('comment', null, array('rows'=>'5','id'=>'new-review','class'=>'form-control animated','placeholder'=>'Enter your review here...'))!!}
                      <div class="text-right">
                          <div class="stars starrr" data-rating="{{Input::old('rating',0)}}"></div>
                          <a href="#" class="tiny" id="close-review-box" style="display:none; margin-right:10px;"> <span class="glyphicon glyphicon-remove"></span>Cancel</a>
                          <button class="tiny" type="submit">Save</button>
                      </div>

                      {!! Form::close()!!}
                  </div>
              </div>

              @foreach($reviews as $review)
                  <hr>
                  <div class="row">
                    <div class="small-12 medium-9 right">
                      <div class="rimg small-2 columns">
                        <img src="/{{ $review->user->profile->profilePic}}">
                      </div>
                      <div class="small-10 columns">
                        <ul class="stars">
                        {{ $review->user ? $review->user->first_name : 'Anonymous'}}
                        <span class="right">
                        @for ($i=1; $i <= 5 ; $i++)
                          <li class="active"> <span class="fi-heart {{ ($i <= $review->rating) ? '' : 'inactive'}}"></span></li>
                        @endfor
                        </span>
                        </ul>        
                          <p>{{$review->comment}}</p>
                          <span>{{$review->timeago}}</span>
                        </div>
                    </div>
                  </div>
              @endforeach
              {{--{{ $reviews->links() }}--}}
          </div>
      </div>
    </div>
  </section>
  </main>
@stop
@section('scripts')
    <script type="text/javascript" src="/js/lightslider.min.js"></script>
    <script type="text/javascript" src="/js/expanding.js"></script>
    <script type="text/javascript" src="/js/starrr.js"></script>
    <script type="text/javascript" src="/js/photoswipe.min.js"></script>
    <script type="text/javascript" src="/js/photoswipe-ui-default.min.js"></script>
    <script type="text/javascript" src="/js/pswip.js"></script>
    {!! HTML::script('/assets/plugins/parsley.min.js') !!}

    <script type="text/javascript">
        $(function(){

            // initialize the autosize plugin on the review text area
            $('#new-review').autosize({append: "\n"});

            var reviewBox = $('#post-review-box');
            var newReview = $('#new-review');
            var openReviewBtn = $('#open-review-box');
            var closeReviewBtn = $('#close-review-box');
            var ratingsField = $('#ratings-hidden');

            openReviewBtn.click(function(e)
            {
                reviewBox.slideDown(400, function()
                {
                    $('#new-review').trigger('autosize.resize');
                    newReview.focus();
                });
                openReviewBtn.fadeOut(100);
                closeReviewBtn.show();
            });

            closeReviewBtn.click(function(e)
            {
                e.preventDefault();
                reviewBox.slideUp(300, function()
                {
                    newReview.focus();
                    openReviewBtn.fadeIn(200);
                });
                closeReviewBtn.hide();

            });

            // If there were validation errors we need to open the comment form programmatically
            @if($errors->first('comment') || $errors->first('rating'))
              openReviewBtn.click();
            @endif

            // Bind the change event for the star rating - store the rating value in a hidden field
            $('.starrr').on('starrr:change', function(e, value){
                ratingsField.val(value);
            });
        });
    </script>
    <script type="text/javascript">
      $('.lightSlider').lightSlider({
          gallery: true,
          item: 1,
          loop:true,
          slideMargin: 0,
          thumbItem: 9
      });
      $('#newboxes1').parsley({
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="alert-box alert parsley" role="alert"> <a href="#" class="close">&times;</a></div>'
      });
       $('#newboxes2').parsley({
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="alert-box alert parsley" role="alert"> <a href="#" class="close">&times;</a></div>'
       });
    </script>
    <script type="text/javascript">
      function showonlyone(thechosenone) {
     $('.newboxes').each(function(index) {
          if ($(this).attr("id") == thechosenone) {
               $(this).show(200);
          }
          else {
               $(this).hide(600);
          }
           });
      }
    </script>

@stop

