
 
<div class="container-popup">
      <!--close button-->
      <a class="close-popup" href="javascript:void(0);">
            <span class="fi-x-circle "></span>
      </a>
  <main class="main">
    <div class="main-container">
      <div class="container-content">
          @if(Auth::check() && Auth::user()->hasUserRole('vendor'))
            Apply for more jobs, grow your business  &nbsp; &nbsp; <a href="/shop" class="button tiny alert">Buy Credits</a>
          @else
            Compare quotes and save &nbsp; &nbsp; <a href="/jobs/create" class="button tiny alert">Get Started</a>
          @endif
      </div> 
    </div>
   </main>

</div>

<style type="text/css">
.container-popup a.button{
    color: #FFF;
    transition: all 200ms ease-in-out;
    font-family: 'clear_sanslight';
    padding: 12px 2rem !important;
    background-color: #FF334D;
}
.container-popup {
    color: #fff;
    font-weight: bold;
    position: fixed;
    bottom: 0;
    z-index: 99;
    width: 100%;
    height: 65px;
    display: none;
}

.main-container {
  background: #607D8C;
  position: relative;
}



.container-content {
    margin: 0 auto;
    padding: 12px 20px;
    /* padding-bottom: 10px; */
    position: relative;
    max-width: 700px;
    text-align: center;
    font-size: 0.8rem;
}
.close-popup{
  position:absolute;
  top:3px;
  right:3px;
  color:rgba(195, 183, 183, 0.78);
  font-size:16px;
  font-weight:bold;
  z-index:99;

}
@media only screen and (min-width:768px){
  .close-popup{
    top: 5px;
    right: 10px;
  }
  .main-container:before {
    -webkit-transform: skewY(1.3deg);
  -moz-transform: skewY(1.3deg);
  -ms-transform: skewY(1.3deg);
  -o-transform: skewY(1.3deg);
  transform: skewY(1.3deg);
  -webkit-backface-visibility: hidden;
    top: -14px;
    width: 100%;
    height: 30px;
  }
  .container-content {
    font-size: 1.1rem;
  }
}
</style>
