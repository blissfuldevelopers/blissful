@extends('layouts.main')

@section('content')

    <div class="jumbotron">
        <h1>Hello...!</h1>
        <p>...word</p>
        <p><a class="btn btn-primary btn-lg" href="http://blissful.dev" target="_blank" role="button">Learn more about blissful</a></p>
    </div>

@stop