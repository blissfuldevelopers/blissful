@extends('page.zurb-layout')
@section('title')
{{$classification->name}}
@stop
@section('content')
<!-- START OF MAIN SECTION -->
  <main role="main" id="categories" class="lightblue">
  <!-- Category Header -->
  <section class="c-header">
    <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/img/catheaders/1.jpg">
    <div class="row">
      <div class="small-12 columns">
        <div class="row" style="border-left:5px solid {{$classification->border_color}};">
          <div class="small-12 medium-4 large-4 columns">
            <h2>{{$classification->name}}</h2>
          </div>
          <div class="medium-8 large-8 hide-for-small-only columns">
            <div class="c-desc">{{$classification->description}}</div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- First batch of categories slides -->
  <section class="cat-main">
    <div class="row">
      <!-- category page filters -->
      <div class="medium-3 large-3 columns cat-filters">
        <div class="row">
        <h5 style="background:{{$classification->border_color}};">Refine by
        <!-- small screens toggle -->
        <span class="filter-icon show-for-small-only"><a href="#"><span>show filters</span></a></span>
        </h5>
        <div class="show-for-medium-up">
            @if(Session::has('errorsZurb'))
            <div data-alert class="alert-box warning {{ Session::get('status') }}">
                <strong>Sorry <i class="fi-info sm"></i></strong> - {{ Session::get('errorsZurb') }}
                <a href="#" class="close">&times;</a>
            </div>
            @endif
        </div>
        <div class="filter-wrap">
          <ul class="cat-loc">
              <div class="row">
                <div class="large-12 columns">
                  <h6>Filter by Location</h6>
                  <select id="locate" name="locate">
                      <option value="/{{$classification->slug}}">All</option>
                      @foreach($cities as $city)
                       <option value="/find/{{$classification->slug}}/{{$city}}/">{{$city}}</option>
                      @endforeach
                  </select>
                </div>
              </div>
          </ul>
          </div>
        </div>
      </div>
      <div class="show-for-small-only">
            @if(Session::has('errorsZurb'))
            <div data-alert class="alert-box warning {{ Session::get('status') }}">
                <strong>Sorry <i class="fi-info sm"></i></strong> - {{ Session::get('errorsZurb') }}
                <a href="#" class="close">&times;</a>
            </div>
            @endif
        </div>
      <!-- category list view section-->
      <div class="small-12 medium-9 large-9 columns v-c-listing ld">
        <div class="loader">
          <div class="loader-inner ball-clip-rotate">
            <div></div>
          </div>
        </div>
        <!-- category page cookie crumbs -->
        <ul class="breadcrumbs">
          <li><a href="/">Home</a></li>
          <li class="current"><a href="#" style="color:{{$classification->border_color}};">{{$classification->name}}</a></li>
        </ul>
        <!-- category page categories list view -->
       @if($featured)
          <ul class="row breaker-bottom feat-ven">
              @foreach($featured as $array)
                      <li class=" small-12 medium-4 columns ripple">
                          <a class="lazyload ninety" href="{{url('vendors/'.($array->user->slug))}}">
                              <div class="v-img">
                                  <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$array->profilePic}}">
                              </div>
                              <div class="s-wrap" style="border-left:4px solid {{$classification->border_color}}">
                                  <h5 id="v-name">{{($array->user->first_name)}}</h5>
                                  <div class="v-loc">{{$array->location}}</div>
                                  <a href="{{url('vendors/'.$array->user->slug)}}" class="ten link hide-for-small-only">&#10095;</a>
                                  <ul class="stars">
                                      @if($array->rating_cache == 0)
                                          <li><span class="fi-heart"></span></li>
                                          <li><span class="fi-heart"></span></li>
                                          <li><span class="fi-heart"></span></li>
                                          <li><span class="fi-heart"></span></li>
                                          <li><span class="fi-heart"></span></li>
                                      @else
                                          @for ($i=1; $i <= 5 ; $i++)
                                              <li class="active"><span class="fi-heart {{ ($i <= $array->rating_cache) ? '' : 'inactive'}}"></span></li>
                                          @endfor
                                      @endif
                                  </ul>
                              </div>
                          </a>
                      </li>
              @endforeach
          </ul>
          @endif
        <ul class="cat-li ">
          @foreach($arrays as $array)
          <li class="ripple">
            <a class="lazyload ninety" href="{{url('vendors/'.($array->user->slug))}}">
              <div class="v-img">
                <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$array->profilePic}}">
              </div>
              <div class="s-wrap" style="border-left:4px solid {{$classification->border_color}}">
                <h5 id="v-name">{{($array->user->first_name)}}</h5>
                <div class="v-loc">{{$array->location}}</div>
                <a href="{{url('vendors/'.$array->user->slug)}}" class="ten link hide-for-small-only">&#10095;</a>
                <ul class="stars">
                  @if($array->rating_cache == 0)
                  <li><span class="fi-heart"></span></li>
                  <li><span class="fi-heart"></span></li>
                  <li><span class="fi-heart"></span></li>
                  <li><span class="fi-heart"></span></li>
                  <li><span class="fi-heart"></span></li>
                  @else
                  @for ($i=1; $i <= 5 ; $i++)
                    <li class="active"><span class="fi-heart {{ ($i <= $array->rating_cache) ? '' : 'inactive'}}"></span></li>
                  @endfor
                  @endif
                </ul>
              </div>
            </a>
          </li>
          @endforeach
        </ul>
      </div>
    </div>
    <div class="row">
          <div class="pagination-centered small-12 medium-9 right">
              {!! $arrays->render(Foundation::paginate($arrays)) !!}
          </div>
      </div>
  </section>
  </main>
@include('pages.pop-up')

  <!-- END OF MAIN SECTION -->
@endsection
@section('scripts')


<script type="text/javascript">
 $(function () {
  var filterH = $('.cat-filters');

      $('.filter-icon a').click(function(){
        $(this).parent().parent().parent().parent().css('height','initial');
      });
    });
</script>
<script type="text/javascript">
  $(function () {
    var cc = 0;
    $('#locate').click(function () {
        cc++;
        if (cc == 2) {
            $(this).change();
            cc = 0;
        }
    }).change (function () {
         window.location = $('option:selected', this).val();

    });
});
</script>

@endsection



