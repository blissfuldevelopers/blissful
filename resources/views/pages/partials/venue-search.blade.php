<div class="row">
    <div class="small-12 columns">
        <ul class="venues">
            @if($vendors->count() == 0)
                <p>No venues found</p>
            @endif
            @foreach ($vendors as $vendor)
            <li class="medium-4 small-12 columns">
                <!-- instant booking badge -->
                <span class="ist-bk">
                    <img src="/img/badge.svg">
                </span>
                <div class="vimg"><img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{@$vendor->profilePic}}"></div>
                <div class="vwrap">
                    <h5>{{@$vendor->first_name}}</h5>
                    <h6> {{@$vendor->address}} </h6>
                    <ul class="stars">
                        @for ($i=1; $i <= 5 ; $i++)
                        <li>{!! ($i <= @$vendor->rating_cache) ? '&starf;' : '&#x02729;'!!} </li>
                        @endfor
                    </ul>
                    <span class="distance">@if($vendor->distance) {{number_format($vendor->distance, 2, '.', '')}} KM away @endif</span>
                    <form action="/vendors/{{@$vendor->slug}}" method="GET">
                        <input type="hidden" name="date" value="{{@$input['date']}}">
                        <button type="submit">Reserve</button>
                    </form>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
</div>
<div class="row">
    <div class="small-12 medium-9 small-centered venue-search-results">
        {!! $vendors->render(Foundation::paginate($vendors)) !!}
    </div>
</div>