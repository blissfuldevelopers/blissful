@extends('page.home-layout')
@section('title')
    @if($classification->meta_title)
        {{$classification->meta_title}}
    @else
        {{$classification->name}}
    @endif
@stop
@section('metas')
    {{--@if($classification->primary_keyword)--}}
        {{--<meta name="keyword" content="{{$classification->primary_keyword}}"/>--}}
    {{--@endif--}}
    @if($classification->meta_description)
        <meta name="description" content="{{$classification->meta_description}}"/>
    @endif
    <link rel="canonical" href="https://www.blissful.co.ke/{{$classification->name}}"/>
    @if($arrays->previousPageUrl())
    <link rel="prev" href="{!!$arrays->previousPageUrl()!!}" />
    @endif
    @if($arrays->nextPageUrl())
    <link rel="next" href="{!!$arrays->nextPageUrl()!!}" />
    @endif
@endsection
@section('header-content')
  <?php
    $header_class = 'catpg-intro';
  ?>
    <!-- intro -->
    <div class="row">
      <div class="medium-10 small-12 medium-centered columns">
        <div class="row">
          <div class="small-12 medium-8 columns">
            <h2>The best<br><span class="ucase">{{$classification->name}}</span><br>in Kenya</h2>
          </div>
          <div class="small-12 medium-4 columns">
            <form action="/jobs/create" method="GET">
              <select placeholder="Get me quotes for..." name="child">
                <option disabled selected>Select the service you want</option>
                @foreach($classification->children as $child)
                <option value="{{$child->id}}">{{$child->name}}</option>
                @endforeach
              </select>
              <button type="submit" class="button">Get Started</button>
            </form>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('content')
      <!-- how it works -->
      <div class="catpg-list">
        <div class="row">
          <div class="small-12 columns">
            <hr>
            <h3>View our list of professionals</h3>
            <ul class="vendors">
                @foreach ($arrays as $vendor)
                    <li class="small-12 columns">
                        <div class="prof-wrap">
                            <div class="w1">
                                <h5>{{$vendor->first_name}}</h5>
                                <h6>{{isset($vendor->location->address) ? $vendor->location->address : $vendor->profile->location}}</h6>
                                <ul class="stars">
                                  @for ($i=1; $i <= 5 ; $i++)
                                  <li>{!! ($i <= $vendor->profile->rating_cache) ? '&starf;' : '&#x02729;'!!} </li>
                                  @endfor
                                </ul>
                                <span class="reviews">({{$vendor->profile->reviews->count()}} {{($vendor->profile->reviews->count() == 1 ? 'Review' : 'Reviews')}} )</span>
                            </div>
                            <div class="w2">
                                <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$vendor->profile->profilePic}}" class="profile">
                            </div>
                        </div>
                        <div class="pdesc-wrap">
                          <p>{{str_limit($vendor->profile->description,40)}}</p>
                          <a href="/vendor/{{$vendor->slug}}" class="button">Request Quote</a>
                        </div>
                    </li>
                @endforeach
            </ul>
          </div>
        </div>
      </div>
      
      <!-- calculator -->
      <div class="calculator">
        <div class="row">
          <div class="medium-10 small-12 medium-centered columns">
            <h3>Cost Calculator</h3>
            <div class="row">
              <form action="/jobs/create" method="GET">
                <div class="medium-4 small-12 columns">
                  <select>
                    <option disabled selected>What are you looking for?</option>
                    @foreach($classification->children as $child)
                    <option value="{{$child->id}}">{{$child->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="medium-4 small-12 columns">
                  <input type="date" name="event_date" placeholder="When is your event?">
                </div>
                <div class="medium-4 small-12 columns">
                  <input type="text" name="event_location" placeholder="Where will it be held?">
                </div>
                <div class="small-12 columns">
                  <button type="submit" class="button">Get Quotes</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>

@endsection
@section('scripts')

@endsection