<!-- vendor listings -->
<div class="vencatpg-list">
    <div class="row">
        <div class="small-12 columns">
            <ul class="venues">
                @foreach ($arrays as $vendor)
                <li class="medium-4 small-12 columns">
                    <!-- instant booking badge -->
                    <span class="ist-bk">
                        <img src="/img/badge.svg">
                    </span>
                    <div class="vimg"><img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$vendor->profile->profilePic}}"></div>
                    <div class="vwrap">
                        <h5>{{$vendor->first_name}}</h5>
                        <h6>{{isset($vendor->location->address) ? $vendor->location->address : $vendor->profile->location}}</h6>
                        <ul class="stars">
                            @for ($i=1; $i <= 5 ; $i++)
                            <li>{!! ($i <= $vendor->profile->rating_cache) ? '&starf;' : '&#x02729;'!!} </li>
                            @endfor
                        </ul>
                        <a href="/vendor/{{$vendor->slug}}" class="button">Request Quote</a>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="small-12 medium-9 small-centered">
            {!! $arrays->render(Foundation::paginate($arrays)) !!}
        </div>
    </div>
</div>