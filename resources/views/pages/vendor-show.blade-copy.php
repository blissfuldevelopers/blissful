@extends('page.zurb-layout')
@section('title')
 {{($profile->user->first_name)}}
@endsection
@section('head') 
@endsection


@section('content')
  <main role="main" id="vendor" class="lightblue">
  @include('includes.status')
  <!-- Vendor Header -->
  <section class="v-header">
    <img src="/img/catheaders/1.jpg">
    <div class="row">
      <div class="small-12 columns">
        <div class="row">
          <div class="small-4 columns">
            <h2>{{($profile->user->first_name)}}</h2>
            <ul class="h-rev">
              <li>
                <!-- <ul class="stars">
                  <li class="active">&#9733;</li>
                  <li class="active">&#9733;</li>
                  <li class="active">&#9733;</li>
                  <li>&#9733;</li>
                  <li>&#9733;</li>
                </ul> -->
              </li>
              <li>
                <!-- <span>7 Reviews</span><span>&#47;</span><span>Review us</span> -->
              </li>
            </ul>
          </div>
          <div class="small-8 columns">
            @if(!Auth::check())
            <a href="#" data-reveal-id="authModal"><button class="ripple">Contact</button></a>
            @else
            <a href="#" data-reveal-id="myModal"><button class="ripple">Contact</button></a>
            @endif
            <div id="myModal" class="reveal-modal fifty" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
              
              <div class="tabs-content messagecontent">
                <div class="content active" id="panel1">
                  <h5 id="modalTitle">Send Message</h5>
                  <div class="lead">
                    <i class="to">To:</i>
                    <div class="pimg placard taggy">
                      <img src="/{{$profile->profilePic}}" alt=" {{($profile->user->first_name)}} pic">
                      <i>{{($profile->user->first_name)}}</i>
                    </div>
                  </div>
                  <div class="row">
                    {!! Form::open(['route' => 'messages.store']) !!}
                    <div>
                        <div class="form-group">
                            <input type="hidden" name="recipients[]" id="recipients" value="{{($profile->user->id)}}">
                            <input type="hidden" name="email" id="email" value="{{($profile->user->email)}}">
                            <input type="hidden" name="first_name" id="email" value="{{($profile->user->first_name)}}">
                            <input type="hidden" name="phone" id="phone" value="{{($profile->phone)}}">
                        </div>
                        <!-- Subject Form Input -->
                        <div class="form-group">
                            {!! Form::label('subject', 'Subject', ['class' => 'control-label','style'=>'width:75%']) !!}
                            {!! Form::text('subject', null, ['class' => 'form-control','style'=>'width:90%']) !!}
                        </div>
                        <!-- Message Form Input -->
                        <div class="form-group">
                            {!! Form::label('message', 'Message', ['class' => 'control-label']) !!}
                            {!! Form::textarea('message', null, ['rows'=>'6', 'class' => 'form-control']) !!}
                        </div>
                        <!-- Submit Form Input -->
                        <div class="form-group">
                            {!! Form::submit('Submit', ['class' => 'button tiny right form-control']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
              <a class="close-reveal-modal md" aria-label="Close">&#215;</a>
            </div>
            <div id="authModal" class="reveal-modal fifty" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
              <div class="content" id="panel2">
                  <div class="modalBackground"></div>
                  <div class="tabs-content messagecontent">
                    <div class="row">
                      <div class="large-4 small-4 medium-4 columns">
                          <a class="show-login white" href="javascript:showonlyone('newboxes1');">Sign In</a>
                          <span class="white">or</span>
                          <a class="show-register white" href="javascript:showonlyone('newboxes2')">Register</a>
                          <div class="oauths">
                            <p class="or-social white">Or Sign In with</p>
                            <div class="button-group" data-grouptype="OR">
                            <a href="{{ route('social.redirect', ['provider' => 'facebook']) }}" class="button tiny circle facebookcircle" type="submit"><img src="/img/fob.png"></a>
                            <a href="{{ route('social.redirect', ['provider' => 'google']) }}" class="button tiny circle googlecircle" type="submit"><img src="/img/Gee.svg" style="width:20px; height:20px; fill:#fff;"></a>
                            </div>
                          </div>
                          
                      </div>
                      <div class="large-8 small-8 medium-8 columns">
                            {!! Form::open(['url' => route('auth.login-post'), 'class' => 'newboxes', 'id'=>'newboxes1', 'data-parsley-validate' ] ) !!}
                            
                            @include('includes.status')

                            <label for="inputEmail" class="sr-only">Email address</label>
                            {!! Form::email('email', null, [
                                'class'                         => 'form-control',
                                'placeholder'                   => 'Email address',
                                'required',
                                'id'                            => 'inputEmail',
                                'data-parsley-required-message' => 'Email is required',
                                'data-parsley-trigger'          => 'change focusout',
                                'data-parsley-type'             => 'email'
                            ]) !!}

                            <label for="inputPassword" class="sr-only">Password</label>
                            {!! Form::password('password', [
                                'class'                         => 'form-control',
                                'placeholder'                   => 'Password',
                                'required',
                                'id'                            => 'inputPassword',
                                'data-parsley-required-message' => 'Password is required',
                                'data-parsley-trigger'          => 'change focusout',
                                'data-parsley-minlength'        => '6',
                                'data-parsley-maxlength'        => '20'
                            ]) !!}

                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('remember', 1) !!} Remember me

                                </label>
                            </div>
                            <p><a href="{{ route('auth.password') }}">Forgot password?</a></p>
                           
                            <button type="submit" class="tiny right">Sign in</button>
                          {!! Form::close() !!}
                          {!! Form::open(['url' => route('auth.register-post'), 'id'=>'newboxes2', 'class' => 'newboxes form-signin', 'data-parsley-validate','style'=>'display:none;' ] ) !!}
                            <label for="inputEmail" class="sr-only">Email address</label>
                            {!! Form::email('email', null, [
                                'class'                         => 'form-control',
                                'placeholder'                   => 'Email address',
                                'required',
                                'id'                            => 'inputEmail',
                                'data-parsley-required-message' => 'Email is required',
                                'data-parsley-trigger'          => 'change focusout',
                                'data-parsley-type'             => 'email'
                            ]) !!}

                            <label for="inputFirstName" class="sr-only">First name</label>
                            {!! Form::text('first_name', null, [
                                'class'                         => 'form-control',
                                'placeholder'                   => 'First name',
                                'required',
                                'id'                            => 'inputFirstName',
                                'data-parsley-required-message' => 'First Name is required',
                                'data-parsley-trigger'          => 'change focusout',
                                'data-parsley-pattern'          => '/^[a-zA-Z]*$/',
                                'data-parsley-minlength'        => '2',
                                'data-parsley-maxlength'        => '32'
                            ]) !!}

                            <label for="inputLastName" class="sr-only">Last name</label>
                            {!! Form::text('last_name', null, [
                                'class'                         => 'form-control',
                                'placeholder'                   => 'Last name',
                                'required',
                                'id'                            => 'inputLastName',
                                'data-parsley-required-message' => 'Last Name is required',
                                'data-parsley-trigger'          => 'change focusout',
                                'data-parsley-pattern'          => '/^[a-zA-Z]*$/',
                                'data-parsley-minlength'        => '2',
                                'data-parsley-maxlength'        => '32'
                            ]) !!}


                            <label for="inputPassword" class="sr-only">Password</label>
                            {!! Form::password('password', [
                                'class'                         => 'form-control',
                                'placeholder'                   => 'Password',
                                'required',
                                'id'                            => 'regPassword',
                                'data-parsley-required-message' => 'Password is required',
                                'data-parsley-trigger'          => 'change focusout',
                                'data-parsley-minlength'        => '6',
                                'data-parsley-maxlength'        => '20'
                            ]) !!}


                            <label for="inputPasswordConfirm" class="sr-only has-warning">Confirm Password</label>
                            {!! Form::password('password_confirmation', [
                                'class'                         => 'form-control',
                                'placeholder'                   => 'Password confirmation',
                                'required',
                                'id'                            => 'inputPasswordConfirm',
                                'data-parsley-required-message' => 'Password confirmation is required',
                                'data-parsley-trigger'          => 'change focusout',
                                'data-parsley-equalto'          => '#regPassword',
                                'data-parsley-equalto-message'  => 'Not same as Password',
                            ]) !!}

                            <button type="submit" class="tiny right">Register</button>
                          {!! Form::close() !!}
                      </div>
                    </div>
                  </div>
                </div>
              <a class="close-reveal-modal md" aria-label="Close">&#215;</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- start of vendor section -->
  <section class="ven-main">
    <div class="row">
      <!-- category page filters -->
      <div class="medium-3 large-3 columns v-desc">
        <div class="row">
        <h5>VENDOR DESCRIPTION</h5>
        <div class="desc-wrap">
          <div class="small-12 columns">
            <div class="desc">  <!-- description section -->
              <p>{{$profile->description}}</p>
            </div>
            <div class="services">  <!-- services section -->
              <h6>SERVICES BY THIS VENDOR</h6>

              <ul>
              @foreach($profile->classifications as $classification )
                      <a href="list/{{$classification->id}}"><li>{{$classification->name}}</li></a>
              @endforeach
              </ul>
            </div>
          </div>
        </div>
        <div class="misc-wrap">
          <div class="small-12 columns">
            <div class="feat">  <!-- special features section -->
              <!-- <h6>SPECIAL FEATURES</h6>
              <ul>
                <li>2 Acres</li>
                <li>200 Guest Capacity</li>
                <li>Mowed Lawn</li>
                <li>Fish Pond</li>
                <li>Wild Birds</li>
              </ul> -->
            </div>
            <div class="ame">   <!-- amenities section -->
              <!-- <h6>AMENITIES</h6>
              <ul>
                <li>Floodlights</li>
                <li>Back Up Electricity</li>
                <li>Toilets</li>
                <li>Wi-fi</li>
                <li>Children’s Play Area</li>
              </ul> -->
            </div>
          </div>
        </div>
        </div>
      </div>
      <!-- vendor listing details-->
      <div class="small-12 medium-9 large-9 columns">
        <!-- vendor page cookie crumbs -->
        <ul class="breadcrumbs">
          <li><a href="/">Home</a></li>
          <li><a href="#">Vendors</a></li>
          <li class="current"><a href="#">{{($profile->user->first_name)}}</a></li>
        </ul>
        <!-- vendor gallery view -->
        <div class="gallery">
          <div class="row">
            <div class="small-12 medium-9 large-9 columns">
              <ul id="lightSlider">
                @foreach ($gall as $gallery)
                @foreach ($gallery->images as $image)
                <li data-thumb="{{url('/gallery/images/thumbs/' .$image->file_name)}}">
                  <img src="{{url('/gallery/images/' .$image->file_name)}}">
                </li>
                @endforeach
                @endforeach
              </ul>
            </div>
            <div class="small-12 medium-3 large-3 columns slides">
              ..
            </div>
          </div>
        </div>
        <!-- vendor listing additional info -->
       <!--  <div class="v-desc-wrap">
          <div class="row">
            <div class="small-12 medium-9 large-9 columns">
              <h3>More About Listing</h3>
              <p>A large 2 acre fresh green grassed natural forest with huge fish pond with goldfish and footbridge with a scenic view of Karura forest and easily accessible from the eastern bypass.</p>
              <br>
              <p>Open : 06:00hrs</p>
              <p>Closed : 23:00hrs</p>
              <br>
              <p>If you need earlier or later times please ask before booking, we always try to flexible.</p>
            </div>
            <div class="small-12 medium-3 large-3 columns">
              <ul>
                <li>Response rate: <span>100%</span></li>
                <li>Response time: <span>Within an hour</span></li>
              </ul>
            </div>
          </div>
        </div> -->
      </div>
    </div>
  </section>
  </main>
@stop
@section('scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            $('.alert-box').fadeOut(1000);
            // initialize the autosize plugin on the review text area
            $('#new-review').autosize({append: "\n"});
            var reviewBox = $('#post-review-box');
            var newReview = $('#new-review');
            var openReviewBtn = $('#open-review-box');
            var closeReviewBtn = $('#close-review-box');
            var ratingsField = $('#ratings-hidden');
            openReviewBtn.click(function(e)
            {
                reviewBox.slideDown(400, function()
                {
                    $('#new-review').trigger('autosize.resize');
                    newReview.focus();
                });
                openReviewBtn.fadeOut(100);
                closeReviewBtn.show();
            });
            closeReviewBtn.click(function(e)
            {
                e.preventDefault();
                reviewBox.slideUp(300, function()
                {
                    newReview.focus();
                    openReviewBtn.fadeIn(200);
                });
                closeReviewBtn.hide();

            });
            // If there were validation errors we need to open the comment form programmatically
            @if($errors->first('comment') || $errors->first('rating'))
              openReviewBtn.click();
            @endif
            // Bind the change event for the star rating - store the rating value in a hidden field
            $('.starrr').on('starrr:change', function(e, value){
                ratingsField.val(value);
            });
        });
    </script>
    <script type="text/javascript" src="/js/lightslider.min.js"></script>
    {!! HTML::script('/assets/plugins/parsley.min.js') !!}
    <script type="text/javascript">
      $('#lightSlider').lightSlider({
          gallery: true,
          item: 1,
          loop:true,
          slideMargin: 0,
          thumbItem: 9
      });
      $('#newboxes1').parsley({
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="alert-box alert parsley" role="alert"> <a href="#" class="close">&times;</a></div>'
      });
       $('#newboxes2').parsley({
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="alert-box alert parsley" role="alert"> <a href="#" class="close">&times;</a></div>'
       });
    </script>
    <script type="text/javascript">
      function showonlyone(thechosenone) {
     $('.newboxes').each(function(index) {
          if ($(this).attr("id") == thechosenone) {
               $(this).show(200);
          }
          else {
               $(this).hide(600);
          }
     });
}
    </script>
     
@stop

@section('styles')
    {!! HTML::style('/assets/css/parsley.css') !!} 
    <style type="text/css">
        /* Enhance the look of the textarea expanding animation */
        .animated {
            -webkit-transition: height 0.2s;
            -moz-transition: height 0.2s;
            transition: height 0.2s;
        }
        .stars {
            margin: 20px 0;
            font-size: 24px;
            color: #d17581;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="/css/lightslider.min.css">

@stop