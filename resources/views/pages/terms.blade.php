@extends('page.home-layout')
@section('title')
    Terms and Conditions
@endsection

@section('header-content')
  <?php
    $header_class = 'contentpg-intro';
  ?>
@endsection
@section('content')
    <!-- main content  -->
    <div class="contentpg-main">
        <div class="row">
          <div class="small-12 columns">
            <div class="wrapper">
            <div class="small-12 medium-7 columns small-centered">
                <h2> Blissful Terms of Use </h2>
                    <div style="height:300px;line-height:3em;overflow:auto;padding:5px;">
                        <p> Blissful is a wedding website that provides valuable wedding-related information for the modern

                        bride. The services offered by Huge Africa Media Ltd (Blissful or &quot we &quot) include the Blissful websites

                        located at www.blissful.co.ke or any sub domain of blissful.co.ke (the &quotBlissful Website&quot), and any

                        other feature, content or applications offered from time to time by Blissful in connection with the

                        Blissful Website whether accessed directly or through our application for mobile devices

                        (collectively, the &quotBlissful Services&quot). The Blissful Services are hosted in Kenya.</p>

                        <p>These Terms of Use (this &quotAgreement&quot) set forth the legally binding terms for your use of the Blissful

                        Services. By using the Blissful Services, whether as a &quotVisitor&quot (meaning you simply browse the

                        Blissful website, application or services) or as a &quotUser&quot (meaning you have registered with and/or

                        submitted content to the Blissful website, application or services as an individual) or as a &quotSupplier&quot

                        (meaning you have registered with and/or submitted content to the Blissful website, application or

                        services as a company), you agree to be bound by this Agreement and the Blissful Privacy Policy

                        located at <a href="http://www.blissful.co.ke/privacy-policy">www.blissful.co.ke/privacy-policy.</a> This Agreement includes your rights, obligations and

                        restrictions regarding your use of the Blissful Services, please read it carefully. If you do not agree

                        with the Terms of this Agreement, you should leave the Blissful Website, application or service, and

                        discontinue use of the Blissful Services immediately. If you wish to become a User, submit content,

                        video or images, communicate with other Users and generally make use of the Blissful Services, you

                        must read this Agreement and indicate your acceptance during the content submission process.</p>

                        <p>Blissful may modify this Agreement from time to time and each modification will be effective when

                        it is posted on the Blissful Website, application or services. You agree to be bound to any changes to

                        this Agreement through your continued use of the Blissful Services. You will not be notified of any

                        modifications to this Agreement so it is important that you review this Agreement regularly to

                        ensure you are updated as to any changes.</p>

                        <p><strong>WE URGE YOU TO THINK BEFORE YOU UPLOAD, SUBMIT OR EMBED CONTENT. THIS

                        AGREEMENT PERMITS YOU TO UPLOAD TO, SUBMIT TO OR EMBED ON THE BLISSFUL

                        SERVICES ONLY PHOTOS OR OTHER CONTENT THAT YOU OWN THE COPYRIGHT TO OR

                        OTHERWISE HAVE THE RIGHT TO PUBLISH. BY UPLOADING, SUBMITTING OR

                        EMBEDDING PHOTOS OR OTHER CONTENT THAT YOU DO NOT OWN THE COPYRIGHT TO OR

                        DO NOT OTHERWISE HAVE THE RIGHT TO PUBLISH, YOU MAY SUBJECT YOURSELF TO LEGAL

                        LIABILITY (SEE E.G., SECTIONS 4, 5 AND 6 BELOW). IT IS YOUR RESPONSIBILITY TO ENSURE

                        YOU HAVE ADEQUATE RIGHTS TO PUBLISH TO THE BLISSFUL SERVICES ALL PHOTOS AND

                        OTHER CONTENT YOU POST.</strong></p>

                        <p><strong>&quotUser&quot</strong> means a person who completes Blissful&#8217s account registration process, including but not

                        limited to Individuals and Businesses, as described under "Account Registration" below.

                        <br><strong>"User Content"</strong> means all Content that a User posts, uploads, publishes, submits, transmits, or

                        includes in their Listing or Member profile to be made available through the Site, Application or

                        Services.<br> 

                        <strong>&quotSupplier&quot</strong> means a person who lists a business or themselves as a supplier of any services on the

                        website.</p>

                        <h4>Eligibility.</h4>

                        <p>Use of the Blissful Services is void where prohibited. You must be eighteen (18) years of age or over

                        to be a Visitor or User of the Blissful Services. By registering, you (i) represent and warrant that you

                        have the right, authority, and capacity to enter into and to fully abide by all of the terms and

                        conditions of this Agreement, and (ii) agree to comply with all applicable domestic and international

                        laws, statutes, ordinances and regulations regarding your use of the Blissful Services and all

                        applicable laws regarding the transmission of technical data exported from the Kenya or the

                        country in which you reside.</p>

                        <h4>Term.</h4>

                        <p>This Agreement shall remain in full force and effect while you use the Blissful Services or are a Supplier or User. Blissful may terminate your use of the Blissful Website, application or the Blissful

                        Services, in its sole discretion, for any reason or no reason whatsoever, at any time, without warning

                        or notice to you.</p>

                        <h4>Account Registration.</h4>

                        <p>In order to access certain features of the Site and Application, and to contact a Supplier or create a

                        Listing, you must register to create an account ("Blissful Account") and become a User. You may

                        register to join the Services directly via the Site or Application or as described in this section.<br>

                        You can also register to join by logging into your account with certain third-party social networking

                        sites ("SNS") (including, but not limited to, Facebook; each such account, a "Third-Party Account"),

                        via our Site or Application, as described below. As part of the functionality of the Site, Application

                        and Services, you may link your Blissful Account with Third-Party Accounts, by either: (i) providing

                        your Third-Party Account login information to Blissful through the Site, Services or Application; or

                        (ii) allowing Blissful to access your Third-Party Account, as permitted under the applicable terms

                        and conditions that govern your use of each Third-Party Account. You represent that you are

                        entitled to disclose your Third-Party Account login information to Blissful and/or grant Blissful

                        access to your Third-Party Account (including, but not limited to, for use for the purposes described

                        herein), without breach by you of any of the terms and conditions that govern your use of the

                        applicable Third-Party Account and without obligating Blissful to pay any fees or making Blissful

                        subject to any usage limitations imposed by such third-party service providers. By granting Blissful

                        access to any Third-Party Accounts, you understand that Blissful will access, make available and

                        store (if applicable) any Content that you have provided to and stored in your Third-Party Account

                        ("SNS Content") so that it is available on and through the Site, Services and Application via your

                        Blissful Account and Blissful Account profile page. Unless otherwise specified in these Terms, all SNS

                        Content, if any, will be considered to be User Content for all purposes of these Terms. Depending on

                        the Third-Party Accounts you choose and subject to the privacy settings that you have set in such

                        Third-Party Accounts, personally identifiable information that you post to your Third-Party

                        Accounts will be available on and through your Blissful Account on the Site, Services and

                        Application. Please note that if a Third-Party Account or associated service becomes unavailable or

                        Blissful's access to such Third-Party Account is terminated by the third-party service provider, then

                        SNS Content will no longer be available on and through the Site, Services and Application. You have

                        the ability to disable the connection between your Blissful Account and your Third-Party Accounts,

                        at any time, by accessing the "Settings" section of the Site and Application. <strong>PLEASE NOTE THAT

                        YOUR RELATIONSHIP WITH THE THIRD-PARTY SERVICE PROVIDERS ASSOCIATED WITH YOUR

                        THIRD-PARTY ACCOUNTS IS GOVERNED SOLELY BY YOUR AGREEMENT(S) WITH SUCH THIRD-
                        PARTY SERVICE PROVIDERS.</strong> Blissful makes no effort to review any SNS Content for any purpose,

                        including but not limited to for accuracy, legality or non-infringement and Blissful is not responsible

                        for any SNS Content.</p>

                        <p>Your Blissful Account and your Blissful Account profile page will be created for your use of the Site

                        and Application based upon the personal information you provide to us or that we obtain via an SNS

                        as described above. You may not have more than one (1) active Blissful Account. You agree to

                        provide accurate, current and complete information during the registration process and to update

                        such information to keep it accurate, current and complete. Blissful reserves the right to suspend or

                        terminate your Blissful Account and your access to the Site, Application and Services if you create

                        more than one (1) Blissful Account, or if any information provided during the registration process or

                        thereafter proves to be inaccurate, fraudulent, not current or incomplete. You are responsible for

                        safeguarding your password. You agree that you will not disclose your password to any third party

                        and that you will take sole responsibility for any activities or actions under your Blissful Account,

                        whether or not you have authorized such activities or actions. You will immediately notify Blissful of

                        any unauthorized use of your Blissful Account.</p>

                        <h4>Non-commercial Use by Members.</h4>

                        <p>The Blissful Services are for the personal use of Visitors and Users only and may not be used by you

                        in connection with any commercial endeavors except those that are specifically endorsed or

                        approved by Blissful. Illegal and/or unauthorized use of the Blissful Services, including collecting

                        usernames and/or email addresses of Users by electronic or other means for the purpose of sending

                        unsolicited email or unauthorized framing of or linking to the Blissful Website is prohibited.

                        Appropriate legal action will be taken for any illegal or unauthorized use of the Blissful Services</p> 

                        <h4>User Content.</h4>

                        <ol>
                            <li>Blissful does not claim any ownership rights in the text, files, images, photos, video, sounds,

                        musical works, works of authorship, or any other materials (collectively, &quotUser Content&quot)

                        that you upload to, submit to, or embed on the Blissful Services. You represent and warrant

                        that you own the User Content posted by you on or through the Blissful Services or that you

                        otherwise have sufficient right, title and interest in and to such User Content to grant

                        Blissful the licenses and rights set forth below without violating, infringing or

                        misappropriating the privacy rights, publicity rights, copyrights, contract rights, intellectual

                        property rights or any other rights of any person. You agree to pay for all royalties, fees, and

                        any other monies owing any person by reason of any User Content posted by you to or

                        through the Blissful Services.</li>

                        <li>After posting, uploading or embedding User Content to the Blissful Services, you continue to

                        retain such rights in such User Content as you held prior to posting such User Content on the

                        Blissful Services and you continue to have the right to use your User Content in any way you

                        choose. However, by displaying or publishing (&quotposting&quot) any User Content on or through

                        the Blissful Services, you hereby grant to Blissful a non-exclusive, royalty-free, transferable,

                        sublicensable, worldwide license to use, display, reproduce, adapt, modify (e.g., re-format),

                        re-arrange, and distribute your User Content through any media now known or developed

                        in the future. Photographs used on the Blissful Services on in any Blissful publication will

                        include attribution to the photographer and/or copyright holder. As part of the non-
                        exclusive license you grant to Blissful, you agree to allow other Users of the Blissful Website

                        and Blissful Services to add your User Content to their Blissful Inspiration Board(s) and post

                        these Inspiration Board(s) on other websites provided that such use is for non-commercial

                        purposes only.</li> 
                        <li> Without this license, Blissful would be unable to provide the Blissful Services or its

                        publications. For example, the license you grant to Blissful is non-exclusive (meaning you

                        are free to license your Content to anyone else in addition to Blissful), fully-paid and royalty-
                        free (meaning that Blissful is not required to pay you for the use of the User Content that

                        you post), sublicensable (so that Blissful is able to use its affiliates and subcontractors such

                        as Internet content delivery networks to provide the Blissful Services), and worldwide

                        (because the Internet and the Blissful Services are global in reach).</li> 

                        <li> This license will terminate at the time you remove your User Content from the Blissful

                        Services except as to any User Content that Blissful has sublicensed prior to your removal of

                        your User Content from the Blissful Services, which license shall continue in perpetuity. To

                        remove User Content, please send a request to contentabuse@blissful.co.ke and include a

                        brief description of the item(s) to be removed along with a URL of the item(s) current

                        location on the Blissful Website. We will remove the item(s) as quickly as possible.</li> 

                        <li> The Blissful Services contain Content owned by Blissful (&quotBlissful Content&quot). Blissful Content

                        is protected by copyright, trademark, patent, trade secret and other laws, and Blissful owns

                        and retains all rights in the Blissful Content and the Blissful Services. Blissful hereby grants

                        you a limited, revocable, non-sublicensable license to view the Blissful Content (excluding

                        any software code) solely for your personal use in connection with viewing the Blissful

                        Website and using the Blissful Services. Without limiting the generality of the foregoing, you

                        agree that you shall not copy, modify, translate, publish, broadcast, transmit, license,
                s
                        sublicense, assign, distribute, perform, display, or sell any Blissful Content appearing on or

                        through the Blissful Services.</li> 

                        <li>The Blissful Services contain Content of other Users and other Blissful licensors (&quotThird

                        Party Content&quot) and you are permitted to access the Third Party Content solely for your

                        personal use in connection with viewing the Blissful Website and using the Blissful Services.

                        Without limiting the generality of the foregoing, you agree that you shall not copy, modify,

                        translate, publish, broadcast, transmit, license, sublicense, assign, distribute, perform,

                        display, or sell any Third Party Content appearing on or through the Blissful Services.

                        </li>
                        </ol>
                            <h4>Content Uploaded, Submitted, Embedded.</h4>
                <ol>
                        <li> Please choose carefully the User Content that you upload to, submit to, or embed on the

                        Blissful Services and that you provide to other Users as you are solely responsible for your

                        User Content and for any liability that may result from the User Content you post. You

                        expressly agree that you will not upload to, submit to, or embed on the Blissful Services or

                        include in your Blissful dashboard(s) any User Content that violates this Agreement or that

                        is contrary to the purposes of the Blissful Services, that may be offensive, illegal or violate

                        the rights of, harm, or threaten the safety of any person, or that presents any business or

                        individual in a negative light. You further agree that you will not upload to, submit to or

                        embed on the Blissful Services or include in your Blissful dashboard(s) any photographs or

                        video containing nudity, or obscene, lewd, violent, harassing, libelous, sexually explicit or

                        otherwise objectionable subject matter or any content owned by third parties for which you

                        do not have the right to post, display or publish.</li>

                        <li> Blissful does not endorse or guarantee the accuracy, efficacy, or veracity of the User Content

                        posted by you or other Users. You acknowledge that we have no way to monitor all User

                        Content that is uploaded to or embedded on the Blissful Website and that despite our

                        reasonable efforts to ensure that all User Content is consistent with the purposes of the

                        Blissful Services, Users may post User Content that contains illegal, inaccurate, libelous,

                        inappropriate, offensive or sexually explicit material, products or services, and Blissful

                        assumes no responsibility or liability for this material. Blissful makes no warranties,

                        express or implied, as to any aspect of the User Content uploaded or embedded by you or

                        other Users or to the reliability of the User Content or any material or information that you

                        or other Users transmit to other Users or Visitors via the Blissful Services. If you become

                        aware of any inappropriate User Content on the Blissful Services or any misuse of the

                        Blissful Services by any person, please contact Blissful at <a href="http://contentabuse@blissful.co.ke">contentabuse@blissful.co.ke</a></li> 

                        <li>Blissful reserves the right, in its sole discretion, to reject, refuse to post or remove any User

                        Content you provide, or to restrict, suspend, or terminate your access to all or any part of

                        the Blissful Services at any time, for any reason or for no reason, with or without prior

                        notice, and without liability to you or any third party.</li>
                </ol>

                        <h4> No Endorsement </h4>

                        <p> Blissful does not endorse any User or any Supplier. You understand that Verified badges are intended

                        only to indicate a photographic representation of the supplier&#8217s work at the time the photograph was

                        taken. Verified badges on Supplier profiles are therefore not an endorsement by Blissful of any User

                        or any Supplier. Suppliers are required by these Terms to provide accurate information, and although

                        Blissful may undertake additional checks and processes designed to help verify or check the

                        identities or work or other details, we do not make any representations about, confirm, or endorse

                        any Supplier or the Supplier's trustworthiness or quality.</p>

                        <p> Any references in the Site, Application or Services to a Supplier being "verified" or "connected" (or

                        similar language) only indicate that the Supplier has completed a relevant verification process, and

                        does not represent anything else. Any such description is not an endorsement, certification or

                        guarantee by Blissful about any Supplier, including of the Supplier&#8217s identity and whether the Supplier

                        is trustworthy, safe or suitable. Instead, any such description is intended to be useful information

                        for you to evaluate when you make your own decisions about the identity and suitability of others

                        whom you contact or interact with via the Site, Application and Services. We therefore recommend

                        that you always exercise due diligence and care when deciding whether to book any Supplier for

                        your function.</p>

                        <h4>Prohibited Content.</h4>

                        <p>Blissful reserves the right, in its sole and absolute discretion, to determine whether User Content is

                        appropriate for the Blissful Services and to remove any User Content, without notice or liability to

                        you, which it determines to be inappropriate. Without limiting the generality of the foregoing, the

                        following is a partial list of the types of User Content that Blissful deems to be inappropriate:</p>

                    <ol>
                        <li> Content that criticizes a business or individual beyond that of merely offering an opinion;</li>
                        <li> Content that harasses or advocates harassment of another person;</li>
                        <li> Content that exploits people in a sexual or violent manner;</li> 
                        <li> Content that contains nudity, violence, or offensive subject matter or contains a link to an

                        adult website;</li>
                        <li> Content that includes racially, ethically, or otherwise objectionable language;</li>
                        <li> Content that is libelous, defamatory, or otherwise tortious language;</li>
                        <li> Content that solicits personal information from anyone under 18; </ul>
                        <li> Content that promotes information that you know is false or misleading or promotes illegal

                        activities or conduct that is abusive, threatening, obscene, defamatory or libelous;</li>
                        <li> Content that promotes an illegal or unauthorized copy of another person&#8217s copyrighted

                        work, such as providing pirated computer programs or links to them, providing information

                        to circumvent manufacture-installed copy-protect devices, or providing pirated music or

                        links to pirated music files; </li>
                        <li> Content that involves the transmission of &quotjunk mail,&quot &quotchain letters,&quot or unsolicited mass

                        mailing, instant messaging, &quotspimming,&quot or &quotspamming&quot; </li>
                        <li> Content that contains restricted or password only access pages or hidden pages or images

                        (those not linked to or from another accessible page); </li>
                        <li> Content that furthers or promotes any criminal activity or enterprise or provides

                        instructional information about illegal activities including, but not limited to making or

                        buying illegal weapons, violating someone&#8217s privacy, or providing or creating computer

                        viruses; </li>
                        <li> Content that solicits passwords or personal identifying information for commercial or

                        unlawful purposes from other Users; </li>
                        <li> Content that involves commercial activities and/or sales without our prior written consent

                        such as contests, sweepstakes, barter, advertising, or pyramid schemes; and </li>
                        <li> Content that includes a photograph of another person that you have posted without that

                        person&#8217s consent.</li>
                    </ol>

                        <p> Blissful&#8217s right to remove inappropriate User Content shall not be its sole right with respect to

                        inappropriate User Content and Blissful expressly reserves the right to investigate and take

                        appropriate legal action against anyone who, in Blissful&#8217s sole discretion, violates this provision,

                        including without limitation, reporting you to law enforcement authorities.</p>

                        <h4>Prohibited Activity.</h4>

                        <p>You expressly agree that you are prohibited from engaging in, and will not engage in, the following

                        prohibited activities in connection with your use of the Blissful Services:</p> 
                    <ol>
                        <li> copying, modifying, translating, publishing, broadcasting, transmitting, licensing,

                        sublicensing, assigning, distributing, performing, publicly displaying, or selling any Third

                        Party Content or Blissful Content appearing on or through the Blissful Services;</li>
                        <li> criminal or tortious activity, including child pornography, fraud, trafficking in obscene

                        material, drug dealing, gambling, harassment, stalking, spamming, spimming, sending of

                        viruses or other harmful files, copyright infringement, patent infringement, or theft of trade

                        secrets;</li>
                        <li> covering or obscuring the banner advertisements on your personal profile page, or any

                        Blissful page via HTML/CSS or any other means;</li>
                        <li> any automated use of the system, such as using scripts to add friends or send comments or

                        messages;</li>
                        <li> interfering with, disrupting, or creating an undue burden on the Blissful Services or the

                        networks or services connected to the Blissful Services; </li>
                        <li> attempting to impersonate another User, person, or representative of Blissful; </li>
                        <li> using the account, username, or password of another User at any time or disclosing your

                        password to any third party or permitting any third party to access your account;</li>
                        <li> selling or otherwise transferring your profile, without our permission;</li>

                        <li> using any information obtained from the Blissful Services in order to harass, abuse, or harm

                        another person;</li>
                        <li> displaying an advertisement on your profile, or accepting payment or anything of value

                        from a third person in exchange for your performing any commercial activity on or through

                        the Blissful Services on behalf of that person, such as placing commercial content on your

                        profile, posting blogs or bulletins with a commercial purpose, or sending private messages

                        with a commercial purpose; or </li>
                        <li> using the Blissful Services in a manner inconsistent with any and all applicable laws and

                        regulations.</li>
                    </ol>    

                        <h4>Copyright Policy.</h4>

                        <p>You may not post, modify, distribute, or reproduce in any way any copyrighted material,

                        trademarks, or other proprietary information belonging to Blissful or others (including without

                        limitation Third Party Content or Blissful Content) without obtaining the prior written consent of the

                        owner of such copyrighted material, trademarks, or other proprietary information. If we become

                        aware that one of our users is a repeat copyright infringer, it is our policy to take reasonable steps

                        within our power to terminate that user. Without limiting the foregoing, if you believe that your

                        work has been copied and posted on the Blissful Services in a way that constitutes copyright

                        infringement, please provide our Copyright Agent with the following information: (i) an electronic

                        or physical signature of the person authorized to act on behalf of the owner of the copyright interest;

                        (ii) a description of the copyrighted work that you claim has been infringed; (iii) a description of

                        where the material that you claim is infringing is located on the Blissful Services; (iv) your address,

                        telephone number, and email address; (v) a written statement by you that you have a good faith

                        belief that the disputed use is not authorized by the copyright owner, its agent, or the law; (vi) a

                        statement by you, made under penalty of perjury, that the above information in your notice is

                        accurate and that you are the copyright owner or authorized to act on the copyright owner&#8217s behalf.

                        Blissful&#8217s Copyright Agent for notice of claims of copyright infringement can be reached as follows:

                        Copyright Agent, Huge Africa Ltd, 72235, 00200 Nairobi, Kenya; and email: dmca@blissful.co.ke or

                        dmca@hugeafrica.com.</p>

                        <h4> User Disputes.</h4>

                        <p>You are solely responsible for your interactions with other Blissful Users. Blissful reserves the right,

                        but has no obligation, to monitor disputes between you and other Users and to immediately

                        terminate the privileges of any User for any reason or for no reason.</p>

                        <h4>Privacy.</h4>

                        <p>Use of the Blissful Services is also governed by our Privacy Policy, located at

                        www.blissful.co.ke/privacy-policy and incorporated into this Agreement by this reference.</p>

                        <h4>Promotions and Giveaways.</h4> 
                        <p>From time to time, Blissful will offer sweepstakes, promotions or giveaways on behalf of third

                        parties. Each promotion or giveaway will have its own rules that will disclose what information is

                        gathered, how that information is used, and who that information shared with. Blissful encourages

                        you to review such information prior to engaging with each sweepstakes, promotion or giveaway.</p>

                        
                        <h4>Disclaimer of Warranties.</h4>

                        <p> <strong> THE BLISSFUL SERVICE IS PROVIDED TO YOU ON AN &quotAS IS&quot AND &quotAS AVAILABLE&quot BASIS WITHOUT

                        REPRESENTATIONS OR WARRANTIES OF ANY KIND AND BLISSFUL EXPRESSLY DISCLAIMS ANY

                        AND ALL IMPLIED OR STATUTORY WARRANTIES TO THE MAXIMUM EXTENT PERMITTED BY

                        APPLICABLE LAW, INCLUDING WITHOUT LIMITATION IMPLIED WARRANTIES OF TITLE, NON-
                        INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. NO ADVICE OR

                        INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM THE WEBSITE OR

                        BLISSFUL SERVICES SHALL CREATE ANY WARRANTY NOT EXPRESSLY STATED IN THE TERMS OF

                        USE. AS WITH THE PURCHASE OF A PRODUCT OR SERVICE THROUGH ANY MEDIUM OR IN ANY

                        ENVIRONMENT, YOU SHOULD USE YOUR BEST JUDGMENT AND EXERCISE CAUTION WHERE

                        APPROPRIATE.</strong></p>

                        <p>Without limiting the generality of the foregoing, Blissful is not responsible for any incorrect or

                        inaccurate Content posted on the Blissful Website or in connection with the Blissful Services. User

                        Content created and posted on the Blissful Website may contain links to other websites. Blissful is

                        not responsible for the accuracy or opinions contained in User Content or on third party websites

                        linked from User Content. Such websites are in no way investigated, monitored or checked for

                        accuracy or completeness by Blissful. Inclusion of any linked website on the Blissful Services does

                        not imply approval or endorsement of the linked website by Blissful. When you access these third-
                        party sites, you do so at your own risk. Blissful takes no responsibility for third party advertisements

                        which are posted on this Blissful Website or through the Blissful Services, nor does it take any

                        responsibility for the goods or services provided by its advertisers. Blissful is not responsible for the

                        conduct, whether online or offline, of any User of the Blissful Services. Blissful assumes no

                        responsibility for any error, omission, interruption, deletion, defect, delay in operation or

                        transmission, communications line failure, theft or destruction or unauthorized access to, or

                        alteration of, any User communication or any Content. Blissful is not responsible for any problems

                        or technical malfunction of any telephone network or lines, computer online systems, servers or

                        providers, computer equipment, software, failure of any email or players due to technical problems

                        or traffic congestion on the Internet or on any of the Blissful Services or combination thereof,

                        including any injury or damage to Users or to any person&#8217s computer related to or resulting from

                        participation or downloading materials in connection with the Blissful Services. Under no

                        circumstances shall Blissful be responsible for any loss or damage, including personal injury or

                        death, resulting from use of the Blissful Services, attendance at a Blissful event, from any Content

                        posted on or through the Blissful Services, or from the conduct of any Users of the Blissful Services,

                        whether online or offline. Blissful cannot guarantee and does not promise any specific results from

                        use of the Blissful Services.</p>

                        <h4>Limitation of Liability.</h4>

                        <p><strong>IN NO EVENT SHALL BLISSFUL OR ANY PARENT, SUBSIDIARY, AFFILIATE, DIRECTOR, OFFICER,

                        EMPLOYEE, LICENSOR, DISTRIBUTOR, SUPPLIER, AGENT, RESELLER, OWNER, OR OPERATOR OF

                        BLISSFUL BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY DIRECT, INDIRECT, CONSEQUENTIAL,

                        EXEMPLARY, INCIDENTAL, SPECIAL OR PUNITIVE DAMAGES, INCLUDING LOST PROFIT DAMAGES

                        ARISING FROM YOUR USE OF THE SERVICES, EVEN IF BLISSFUL HAS BEEN ADVISED OF THE

                        POSSIBILITY OF SUCH DAMAGES. NOTWITHSTANDING ANYTHING TO THE CONTRARY CONTAINED

                        HEREIN, BLISSFUL&#8217S LIABILITY TO YOU FOR ANY CAUSE WHATSOEVER AND REGARDLESS OF THE

                        FORM OF THE ACTION, WILL AT ALL TIMES BE LIMITED TO THE AMOUNT PAID, IF ANY, PAID BY

                        YOU TO BLISSFUL FOR THE BLISSFUL SERVICES DURING THE TERM OF YOUR USE. THE

                        FOREGOING LIMITATION OF LIABILITY SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY

                        LAW IN THE APPLICABLE JURISDICTION. YOU SPECIFICALLY ACKNOWLEDGE THAT BLISSFUL

                        SHALL NOT BE LIABLE FOR USER CONTENT OR FOR ANY DEFAMATORY, OFFENSIVE, OR ILLEGAL

                        CONDUCT OF ANY THIRD PARTY AND THAT THE RISK OF HARM OR DAMAGE FROM THE

                        FOREGOING RESTS ENTIRELY WITH YOU.

                        SOME JURISDICTIONS DO NOT ALLOW LIMITATIONS ON IMPLIED WARRANTIES OR THE

                        EXCLUSION OR LIMITATION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES.

                        ACCORDINGLY, SOME OF THE LIMITATIONS MAY NOT APPLY TO YOU</strong></p>

                        
                        <h4>Special Admonitions for International Use.</h4>

                        <p>Recognizing the global nature of the Internet, you agree to comply with all local rules regarding

                        online conduct and acceptable Content. Specifically, you agree to comply with all applicable laws

                        regarding the transmission of technical data exported from the Kenya or the country in which you

                        reside.</p>

                        <h4>Disputes; Choice of Law; Venue.</h4>

                        <p>If there is any dispute about or involving the Blissful Services, you agree that the laws of Kenya shall

                        govern the dispute, without regard to conflict of law provisions and you agree to exclusive personal

                        jurisdiction and venue in the state courts of Kenya located in the County of Nairobi. You hereby

                        waive to the maximum extent permitted by applicable law, any claim that such courts constitute an

                        inconvenient venue. The prevailing party in any action brought in connection with this Agreement

                        shall be entitled to an award of attorneys&#8217 fees and costs incurred by the prevailing party in

                        connection with such action.</p>

                        <h4>Indemnity.</h4>

                        <p>You agree to indemnify and hold harmless Blissful, and any parent, subsidiary, and affiliate,

                        director, officer, employee, licensor, distributor, supplier, agent, reseller, owner and operator, from

                        and against any and all claims, damages, obligations, losses, liabilities, costs or debt, including but

                        not limited to reasonable attorneys&#8217 fees, made by any third party due to or arising out of your use of

                        the Blissful Services in violation of this Agreement and/or arising from: (i) your use of and access to

                        the Blissful Website; (ii) your violation of any term of these Terms of Use; (iii) your violation of any

                        third party right, including without limitation any copyright, property, or privacy right; or (iv) any

                        claim that your User Content caused damage to a third party. This defense and indemnification

                        obligation will survive these Terms of Use and your use of the Blissful Website and/or the Blissful

                        Services</p>

                        <h4>Other.</h4>

                        <p>This Agreement is accepted upon your use of the Blissful Website or any of the Blissful Services and

                        is further affirmed by you becoming a User. This Agreement constitutes the entire agreement

                        between you and Blissful regarding the use of the Blissful Services. The failure of Blissful to exercise

                        or enforce any right or provision of this Agreement shall not operate as a waiver of such right or

                        provision. The section titles in this Agreement are for convenience only and have no legal or

                        contractual effect. Blissful is a trademark of Huge Africa Ltd. This Agreement operates to the fullest

                        extent permissible by law. If any provision of this Agreement is unlawful, void or unenforceable,

                        that provision is deemed severable from this Agreement and does not affect the validity and

                        enforceability of any remaining provisions.</p>

                        <h4>Waiver and Severability of Terms.</h4>

                        <p>The failure of Blissful to exercise or enforce any right or provision of this Agreement shall not

                        constitute a waiver of such right or provision. If any provision of this Agreement is found by a court

                        of competent jurisdiction to be invalid, the parties nevertheless agree that the court should

                        endeavor to give effect to the parties&#8217 intentions as reflected in the provision, and the other

                        provisions of this Agreement remain in full force and effect.</p>

                        <h4>Statute of Limitations.</h4>

                        <p>You agree that regardless of any statute or law to the contrary, any claim or cause of action arising

                        out of or related to use of the Service or this Agreement must be filed within one (1) year after such

                        claim or cause of action arose or be forever barred.</p>

                        <h4>Violations.</h4>

                        <p>Please report any violations of these Terms of Use to us by emailing us

                        at contentabuse@blissful.co.ke Please contact us at: support@blissful.co.ke with any questions regarding this Agreement.</p>

                        <p><strong>Last Updated: October 27, 2015</strong></p>
                    </div> 
            </div>    
            </div>
          </div>
        </div>
    </div>
@endsection
