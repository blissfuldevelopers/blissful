@extends('page.zurb-layout')
@section('head')
<meta name="keywords" content="{{$classification->primary_keyword}}">
<meta name="description" content="{{$classification->meta_description}}">
@endsection
@section('title')
{{$classification->meta_title}}
@stop
@section('content')
<!-- START OF MAIN SECTION -->
  <main role="main" id="categories" class="lightblue">
  <!-- Category Header -->
  <section class="c-header">
    <img src="/img/catheaders/1.jpg">
    <div class="row">
      <div class="small-12 columns">
        <div class="row" style="border-left:5px solid {{$classification->border_color}};">
          <div class="small-12 medium-4 large-4 columns">
            <h1>{{$classification->h1}}</h1>
          </div>
          <div class="medium-8 large-8 hide-for-small-only columns">
            <div class="c-desc">{{$classification->page_content}}</div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- First batch of categories slides -->
  <section class="cat-main">
    <div class="row">
      <!-- category page filters -->
      <div class="medium-3 large-3 columns cat-filters">
        <div class="row">
        <h5 style="background:{{$classification->border_color}};">Refine by
        <!-- small screens toggle -->
        <span class="filter-icon show-for-small-only"><a href="#"><span>show filters</span></a></span>
        </h5>
        @if(Session::has('errorsZurb'))
        <div data-alert class="alert-box warning {{ Session::get('status') }}">
            <strong>Sorry <i class="fi-info sm"></i></strong> - {{ Session::get('errorsZurb') }}
            <a href="#" class="close">&times;</a>
        </div>
        @endif
        <div class="filter-wrap">
          <ul class="cat-loc">
            <form id="locs" method="get" action="{{url('/fetchSubCategory')}}">
              <div class="row">
              <input type="hidden" name="class_id" value="{{$classification->id}}">
                <div class="large-12 columns">
                  <h6>Filter by Location</h6>
                  <select id="locate" name="locate">
                      <option value="all">All</option>
                      @foreach($cities as $city)
                       <option value="{{$city}}">{{$city}}</option>
                      @endforeach
                  </select>
                </div>
              </div>
            </form>
          </ul>
          <!-- <ul class="cat-feat">
            <form>
              <div class="row">
                <div class="large-12 columns">
                  <h6>Sort by Rating</h6>
                  <select>
                    <option value="asc">Ascending</option>
                    <option value="desc">Descending</option>
                  </select>
                </div>
              </div>
            </form>
          </ul> -->
  <!--    <ul class="cat-cap">
            <form>
              <div class="row">
                <div class="large-12 columns">
                  <h6>Capacity</h6>
                  <input type="checkbox"><label>0 - 100</label>
                  <input type="checkbox"><label>101 - 200</label>
                  <input type="checkbox"><label>201 - 300</label>
                  <input type="checkbox"><label>301 - 400</label>
                </div>
              </div>
            </form>
          </ul> -->
          </div>
        </div>
      </div>
      <!-- category list view section-->
      <div class="small-12 medium-9 large-9 columns v-c-listing ld">
        <div class="loader">
          <div class="loader-inner ball-clip-rotate">
            <div></div>
          </div>
        </div>
        <!-- category page cookie crumbs -->
        <ul class="breadcrumbs">
          <li><a href="/">Home</a></li>
          <li class="current"><a href="#" style="color:{{$classification->border_color}};">{{$classification->name}}</a></li>
        </ul>
        <!-- category page categories list view -->
        <ul class="cat-li ">
          @foreach($arrays as $array)
          <li class="ripple">
            <a class="lazyload ninety" href="{{url('vendors/'.($array->user->slug))}}">
              <div class="v-img">
                <img src="/{{$array->profilePic}}">
              </div>
              <div class="s-wrap" style="border-left:4px solid {{$classification->border_color}}">
                <h5 id="v-name">{{($array->user->first_name)}}</h5>
                <div class="v-loc">{{$array->location}}</div>
                <a href="{{url('vendor/'.$array->user->slug)}}" class="ten link hide-for-small-only">&#10095;</a>
                <ul class="stars">
                  @if($array->rating_cache == 0)
                  <li><span class="fi-heart"></span></li>
                  <li><span class="fi-heart"></span></li>
                  <li><span class="fi-heart"></span></li>
                  <li><span class="fi-heart"></span></li>
                  <li><span class="fi-heart"></span></li>
                  @else
                  @for ($i=1; $i <= 5 ; $i++)
                    <li class="active"><span class="fi-heart {{ ($i <= $array->rating_cache) ? '' : 'inactive'}}"></span></li>
                  @endfor
                  @endif
                </ul>
              </div>
            </a>
          </li>
          @endforeach
        </ul>
      </div>
    </div>
  </section>
  </main>
  <!-- END OF MAIN SECTION -->
@endsection
@section('scripts')
<script src="js/jquery.lazyload-any.min.js"></script>
<script>
      function load(section)
        {
          section.fadeOut(0, function() {
            section.fadeIn(1000);
          });
        }
        $('.lazyload').lazyload({
          load: load,
        });
    </script>

<script type="text/javascript">
 $(function () {
  var filterH = $('.cat-filters');

      $('.filter-icon a').click(function(){
        $(this).parent().parent().parent().parent().css('height','initial');
      });
    });
</script>
<script type="text/javascript">
  $(function () {
    var cc = 0;
    $('#locate').click(function () {
        cc++;
        if (cc == 2) {
            $(this).change();
            cc = 0;
        }
    }).change (function () {
        $('#locs').submit();

    });
});
</script>
@endsection



