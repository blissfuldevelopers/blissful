<div class="row">
	<div class="small-12 columns">
		@include('flash::message')
	</div>
	<div class="small-12 columns">
		<form method="GET" action="/sms/post">
			<p>Send notification to:</p>
			<div class="small-12 columns">
				<div class="small-4 columns">
					<label for="type">Type</label>
				</div>
				<div class="small-8 columns">
					<select id="type" name="notification_type[]">
						<option value="sms">SMS</option>
						<option value="email">Email</option>
					</select>
				</div>
			</div>
			<div class="small-12 columns">
				<div class="small-4 columns">
					<label for="user_type">User type</label>
				</div>
				<div class="small-8 columns">
					<select id="user_type" name="user_type">
						@foreach($roles as $role)
						<option value="{{$role->id}}">{{$role->name}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="small-12 columns">
				<div class="small-4 columns">
					<label for="title">Title:</label>
				</div>
				<div class="small-8 columns">
					<input id="title" type="text" name="title">
				</div>
			</div>
			<div class="small-12 columns">
				<textarea name="message" placeholder="Type in message here"></textarea>
			</div>
			<div class="small-12 columns hm">
				<button type="submit" class="tiny small-5"> Send</button>
			</div>
			
		</form>
	</div>
</div>