@extends('page.shop-layout')
@section('title')
    @if($classification->meta_title)
        {{$classification->meta_title}}
    @else
        {{$classification->name}}
    @endif
@stop
@section('metas')
    {{--@if($classification->primary_keyword)--}}
        {{--<meta name="keyword" content="{{$classification->primary_keyword}}"/>--}}
    {{--@endif--}}
    @if($classification->meta_description)
        <meta name="description" content="{{$classification->meta_description}}"/>
    @endif
@endsection
<link rel="canonical" href="https://www.blissful.co.ke/{{$classification->name}}"/>
@if($arrays->previousPageUrl())
<link rel="prev" href="{!!$arrays->previousPageUrl()!!}" />
@endif
@if($arrays->nextPageUrl())
<link rel="next" href="{!!$arrays->nextPageUrl()!!}" />
@endif
@section('styles')
    <style type="text/css">
        .browse-vendors li:hover .wrap {
            border-bottom-color: {{$classification->border_color}};
        }
    </style>
@endsection
@section('content')
    <!-- START OF MAIN SECTION -->
    <main role="main" id="categories" class="lightblue">
        <!-- Category Header -->
        <section class="c-header">
            <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/img/catheaders/1.jpg">
            <div class="row">
                <div class="small-12 columns">
                    <div class="row" style="border-left:5px solid {{$classification->border_color}};">
                        <div class="small-12 medium-4 large-4 columns">
                            <h2>{{$classification->name}}</h2>
                        </div>
                        <div class="medium-8 large-8 hide-for-small-only columns">
                            <h1 class="c-desc">{{$classification->description}}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- First batch of categories slides -->
        <section class="cat-main ">
            <div class="row small-12 columns">
                <!-- category page filters -->
                <div class="medium-3 large-3 columns cat-filters">
                    <div class="row">
                        <h5 style="background:{{$classification->border_color}};">Refine by
                            <!-- small screens toggle -->
                            <span class="filter-icon show-for-small-only"><a
                                        href="#"><span>show filters</span></a></span>
                        </h5>
                        <div class="show-for-medium-up">
                            @if(Session::has('errorsZurb'))
                                <div data-alert class="alert-box warning {{ Session::get('status') }}">
                                    <strong>Sorry <i class="fi-info sm"></i></strong> - {{ Session::get('errorsZurb') }}
                                    <a href="#" class="close">&times;</a>
                                </div>
                            @endif
                        </div>
                        <div class="filter-wrap">
                            <ul class="cat-loc">
                                <div class="row">
                                    <div class="large-12 columns">
                                        <h6>Filter by Location</h6>
                                        <select id="locate" name="locate">
                                            <option value="/{{$classification->slug}}">All</option>
                                            @foreach($cities as $city)
                                                @if(@$mainSearch && $city->name == @$mainSearch)
                                                    <option selected
                                                            value="/find/{{$classification->slug}}/{{$city->name}}/">{{$city->name}}</option>
                                                @else
                                                    <option value="/find/{{$classification->slug}}/{{$city->name}}/">{{$city->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </ul>
                        </div>
                    </div>
                    <!-- <div class="small-12 small-centered columns">
                        <a href="http://www.superbtent.com/">
                            <img src="/img/SUPERB-300x250.jpg">
                        </a>
                    </div> -->

                </div>
                <div class="show-for-small-only">
                    @if(Session::has('errorsZurb'))
                        <div data-alert class="alert-box warning {{ Session::get('status') }}">
                            <strong>Sorry <i class="fi-info sm"></i></strong> - {{ Session::get('errorsZurb') }}
                            <a href="#" class="close">&times;</a>
                        </div>
                    @endif
                </div>
                <!-- category list view section-->
                <div class="small-12 medium-9 large-9 columns v-c-listing ld">
                    <div class="loader">
                        <div class="loader-inner ball-clip-rotate">
                            <div></div>
                        </div>
                    </div>

                    <!-- category page cookie crumbs -->
                    <ul class="breadcrumbs">
                        <li><a href="/">Home</a></li>
                        <li class="current"><a href="#"
                                               style="color:{{$classification->border_color}};">{{$classification->name}}</a>
                        </li>
                    </ul>

                    @if($featured)
                        <section class="browse-vendors">
                            <div class="row">
                                <!-- <div class="small-12 medium-10 small-centered columns ad-section">
                                    <a href="http://www.superbtent.com/" target="_blank">
                                        <img class="hide-for-small-only" src="/img/SUPERB1-728X90.jpg">
                                        <img class="show-for-small-only" src="/img/SUPERB-320x100.jpg">
                                    </a>
                                </div> -->

                                <div class="small-12 columns">
                                    <ul class="vendor-container">
                                        @foreach ($classification->featured_vendors as $vendor)
                                            <li class="card-box ripple">
                                                <a href="/vendors/{{$vendor->slug}}">
                                                    <div class="section-attributes">
                                                        @if($vendor->top_vendor())
                                                            <div class="top-vendor-badge infotip">
                                                                <img src="/img/super-vendor.svg">
                                                                <span class="infotiptext">Super Vendor</span>
                                                            </div>
                                                        @endif
                                                        @if($vendor->recommended_vendor())
                                                            <div class="recommended-badge infotip">
                                                                <i class="ion-thumbsup"> </i>
                                                                <span class="infotiptext">Recommended</span>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    
                                                    <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$vendor->profile->profilePic}}"
                                                         style="height: 180px;"/>
                                                    <div class="wrap">
                                                        <h4 id="v-name">{{ $vendor->first_name }}</h4>
                                                        <h4 id="v-name">{{ $vendor->profile->location}}</h4>
                                                        <h6>
                                                            <ul class="stars">
                                                                @for ($i=1; $i <= 5 ; $i++)
                                                                    <li class="active"><span
                                                                                class="fi-heart {{ ($i <= $vendor->profile->rating_cache) ? '' : 'inactive'}}"></span>
                                                                    </li>
                                                                @endfor
                                                            </ul>
                                                        </h6>
                                                    </div> <!-- end card -->
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </section>
                    @endif
                    <section class="browse-vendors">
                        <div class="row">
                            <div class="small-12 columns">
                                <ul class="vendor-container">
                                    @foreach ($arrays as $vendor)
                                        <li class="card-box ripple">
                                            <a href="/vendors/{{$vendor->slug}}">
                                                <div class="section-attributes">
                                                    @if($vendor->top_vendor())
                                                        <div class="top-vendor-badge infotip">
                                                            <img src="/img/super-vendor.svg">
                                                            <span class="infotiptext">Super Vendor</span>
                                                        </div>
                                                    @endif
                                                    @if($vendor->recommended_vendor())
                                                        <div class="recommended-badge infotip">
                                                            <i class="ion-thumbsup"> </i>
                                                            <span class="infotiptext">Recommended</span>
                                                        </div>
                                                    @endif
                                                </div>
                                                
                                                <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$vendor->profile->profilePic}}"
                                                     style="height: 180px;"/>
                                                <div class="wrap">
                                                    <h4 id="v-name">{{ $vendor->first_name }}</h4>
                                                    <h4 id="v-name">{{ $vendor->profile->location}}</h4>
                                                    <h6>
                                                        <ul class="stars">
                                                            @for ($i=1; $i <= 5 ; $i++)
                                                                <li class="active"><span
                                                                            class="fi-heart {{ ($i <= $vendor->profile->rating_cache) ? '' : 'inactive'}}"></span>
                                                                </li>
                                                            @endfor
                                                        </ul>
                                                    </h6>
                                                </div> <!-- end card -->
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </section>
                    <!-- <div class="row">
                        <div class="small-12 medium-10 small-centered columns ad-section">
                            <a href="http://www.superbtent.com/" target="_blank">
                                <img class="hide-for-small-only" src="/img/SUPERB1-728X90.jpg">
                                <img class="show-for-small-only" src="/img/SUPERB-320x100.jpg">
                            </a>
                        </div>
                    </div> -->
                </div>
            </div>

            <div class="row">
                <div class="small-12 medium-9 small-centered">
                    {!! $arrays->render(Foundation::paginate($arrays)) !!}
                </div>
            </div>
        </section>
    </main>
    @include('pages.pop-up')
    <!-- END OF MAIN SECTION -->
@endsection




