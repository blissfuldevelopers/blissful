@extends('page.shop-layout')
@section('title')
    Search results for {{$mainSearch}}
@stop
@section('styles')
<style type="text/css">
  .browse-vendors li:hover .wrap {
    border-bottom-color: {{$border_color}};
}
</style>
@endsection
    @section('content')
            <!-- START OF MAIN SECTION -->
    <main role="main" id="categories" class="lightblue">

        <!-- Category Header -->
        <section class="c-header">
            <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/img/catheaders/1.jpg">
            <div class="row">
                <div class="small-12 columns">
                    <div class="row" style="border-left:4px solid {{$border_color}};">
                        <div class="small-4 columns">
                            <h2>Search</h2>
                        </div>
                        <div class="small-8 columns">
                            <h1 class="c-desc">Search results for "{{$mainSearch}}"</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- First batch of categories slides -->
        <section class="cat-main">
            <div class="row myRow">
                <!-- category page filters -->
                <div class="medium-3 large-3 columns cat-filters">
                    <div class="row">
                        <h5>Refine by

                            <span class="filter-icon show-for-small-only"><a
                                        href="#"><span>show filters</span></a></span>
                        </h5>
                        @if(Session::has('errorsZurb'))
                            <div data-alert class="alert-box warning {{ Session::get('status') }}">
                                <strong>Sorry <i class="fi-info sm"></i></strong> - {{ Session::get('errorsZurb') }}
                                <a href="#" class="close">&times;</a>
                            </div>
                        @endif
                        <div class="filter-wrap">
                            <ul class="cat-loc">
                                <form id="locs" method="GET" action="{{url('/find')}}">
                                    <div class="row">
                                        <input type="hidden" name="search" value="{{$mainSearch}}">
                                        <div class="large-12 columns">
                                            <h6>Location</h6>
                                            <select id="locate" name="locate">
                                                <option value="all">All</option>
                                                @foreach($cities as $city)
                                                    <option value="{{$city->name}}">{{$city->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </ul>
                            <ul class="cat-feat">
                                <form>
                                    <div class="row">
                                        <div class="large-12 columns">
                                            <h6>Categories</h6>


                                            <ul>
                                                @if($counts >0)
                                                    @foreach($counts as $key => $value)
                                                        <li><a href="/search/{{$mainSearch}}/{{$key}}"
                                                               class="tiny sm">{{"$key"}} ({{"$value"}})</a></li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                            <!-- <input id="Lawn" type="checkbox"><label for="Lawn">Lawn</label>
                                            <input id="Flowers" type="checkbox"><label for="Flowers">Flowers</label>
                                            <input id="Trees" type="checkbox"><label for="Trees">Trees</label>
                                            <input id="Playground" type="checkbox"><label for="Playground">Playground</label>
                                            <input id="Ballroom" type="checkbox"><label for="Ballroom">Ballroom</label> -->
                                        </div>
                                    </div>
                                </form>
                            </ul>
                            <!--  <ul class="cat-cap">
                               <form>
                                 <div class="row">
                                   <div class="large-12 columns">
                                     <h6>Capacity</h6>
                                     <input type="checkbox"><label>0 - 100</label>
                                     <input type="checkbox"><label>101 - 200</label>
                                     <input type="checkbox"><label>201 - 300</label>
                                     <input type="checkbox"><label>301 - 400</label>
                                   </div>
                                 </div>
                               </form>
                             </ul> -->
                        </div>
                    </div>
                </div>
                <!-- category list view section-->
                <div class="small-12 medium-9 large-9 columns">
                    <!-- category page cookie crumbs -->
                    <ul class="breadcrumbs">
                        <li><a href="#">Home</a></li>
                        <li class="current"><a href="#">{{$mainSearch}}</a></li>
                    </ul>
                    <!-- category page categories list view -->
                    <section class="browse-vendors">
                    <div class="row">
                      <div class="small-12 columns">
                        <ul class="vendor-container">
                          @foreach ($arrays as $vendor)
                          <li class="card-box ripple">
                            <a href="/vendors/{{$vendor->slug}}">
                              <div class="section-attributes">
                                @if($vendor->top_vendor())
                                  <div  class="top-vendor-badge infotip">
                                    <img src="/img/super-vendor.svg">
                                    <span class="infotiptext">Super Vendor</span>
                                  </div>
                                @endif
                                @if($vendor->recommended_vendor())
                                  <div  class="recommended-badge infotip">
                                    <i class="ion-thumbsup"> </i> 
                                    <span class="infotiptext">Recommended</span>
                                  </div>
                                @endif
                              </div>
                                <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$vendor->profile->profilePic}}" style="height: 180px;"/>
                                <div class="wrap">
                                    <h4 id="v-name">{{ $vendor->first_name }}</h4>
                                    <h4 id="v-name">{{ $vendor->profile->location}}</h4>
                                    <h6>
                                      <ul class="stars">
                                    @for ($i=1; $i <= 5 ; $i++)
                                        <li class="active"><span
                                                    class="fi-heart {{ ($i <= $vendor->profile->rating_cache) ? '' : 'inactive'}}"></span>
                                        </li>
                                    @endfor
                                </ul>
                                    </h6>
                                </div> <!-- end card -->
                              </a>
                          </li>
                          @endforeach
                        </ul>
                      </div>
                    </div>
                  </section>
                </div>
            </div>
        </section>
    </main>
    @include('pages.pop-up')
    <!-- END OF MAIN SECTION -->
@endsection




