@extends('page.home-layout')
@section('title')
    About Blissful, Kenya's Premier Events Website
@endsection

@section('header-content')
  <?php
    $header_class = 'contentpg-intro';
  ?>
  <!-- intro -->
    <div class="row">
      <div class="medium-11 small-12 medium-centered columns">
        <h2>Get Connected to<br><span class="ucase">Service Jobs Requests</span><br>on Blissful</h2>
      </div>
    </div>
@endsection
@section('content')
    <!-- main content  -->
    <div class="contentpg-main">
        <div class="row">
          <div class="small-12 columns">
            <div class="wrapper">
              <section class="intro">
                <h3>About Blissful</h3>
                <p>Unless you are a wedding or event planner, then planning a wedding or any other function is tough. You have to pull together all these suppliers / vendors and you probably have no idea where to start or how to tell if someone will deliver what they promise. There has to be a better way to do this.</p>
                <p>Blissful takes care of this problem, we collect supplier information and make it easier for you to search and select suppliers. Then once you make the booking and have your event, you come back and leave a review for each suppliers.</p>
              </section>
              <section class="stats">
                <h4></h4>
                <ul>
                  <li>
                    <span class="blissful-stat">5,000</span>
                    <h6>Registered Vendors</h6>
                  </li>
                  <li>
                    <span class="blissful-stat">10,000</span>
                    <h6>Vendor Services</h6>
                  </li>
                  <li>
                    <span class="blissful-stat">3,000</span>
                    <h6>Registered Users</h6>
                  </li>
                  <li>
                    <span class="blissful-stat">2,000</span>
                    <h6>Monthly Quote Requests</h6>
                  </li>
                </ul>
              </section>
              <section class="join-blissful">
                <h4>Join Blissful</h4>
                <p>The reviews you and others like you leave result in a better selection process for the next person or for you later in the future. Rewarding good suppliers and making them known to others.</p>
                <p>For businesses or individuals in the events industry, showcase your work and testimonials on Blissful and grow your business strategically.</p>
                <a href="#" class="button">Get Started</a>
              </section>
            </div>
          </div>
        </div>
    </div>
@endsection