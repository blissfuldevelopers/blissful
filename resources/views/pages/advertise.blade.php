<div class="small-12">
                    <div class="small-12">
                        @if(@$success)
                            <div data-alert class="alert-box alert-box-messages success">
                               Thank you for your message. We will get back to you soon
                                <a href="#" class="close">&times;</a>
                            </div>
                        @endif
                    </div>
                    <p>Thousands of consumers use Blissful every day to make purchase & booking decisions. Blissful Ads
                        showcase your business to users looking for a business like yours.

                        <br>If you would like to find out more give us your details below.</p>

                    <form id="form_center" action="/send-advert" method="post" name="send-advert">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <div class="row control-group">
                            <div class="small-12 medium-12 columns">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name"
                                       value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="small-12 medium-12 columns">
                                <label for="email">Email Address</label>
                                <input type="email" class="form-control" id="email" name="email"
                                       value="{{ old('email') }}">
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="small-12 medium-12 columns">
                                <label for="phone">Phone Number</label>
                                <input type="tel" class="form-control" id="phone" name="phone"
                                       value="{{ old('phone') }}">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="small-12 medium-12 columns">
                                <button id="send-advert" type="submit" class="button tiny right">Send</button>
                            </div>
                        </div>
                    </form>
                </div>