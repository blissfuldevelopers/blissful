<div class="social-buttons main-socials">
    <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode($url) }}"
       target="_blank">
        <i class="fi-social-facebook"></i>
    </a>
    <a href="https://twitter.com/share?url={{urlencode($url)}}&via=blissfulKE" target="_blank">
        <i class="fi-social-twitter"></i>
    </a>
    <a href="https://plus.google.com/share?url={{ urlencode($url) }}"
       target="_blank">
        <i class="fi-social-google-plus"></i>
    </a>
    <a href="https://pinterest.com/pin/create/button/?{{
        http_build_query([
            'url' => $url,
            'media' => $image,
            'description' => $description
        ])
        }}" target="_blank">
        <i class="fi-social-pinterest"></i>
    </a>
</div>