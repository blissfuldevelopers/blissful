@extends('page.shop-layout')
@section('title')
    {{($user->first_name)}}
@endsection
@section('metas')
    <meta property="og:url" content="{{request()->fullUrl()}}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{$user->first_name}} | Blissful" />
    <meta property="og:description" content="{{$user->profile->description}}" />
    <meta property="og:image" content="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$user->profile->profilePic}}" />
    <meta property="fb:app_id" content="1615380565398173" />
@endsection
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/vendor-page.css') }}">
@endsection
@section('content')

    <!-- START OF MAIN SECTION -->

    <!-- START OF BODY -->
    <main aria-role="main" class="shop-item" id="shop-main">


        <!-- START OF SHOP ITEM HEADER SECTION -->
        <section class="title-header vendor-header">
            <div class="row">
                <div class="small-10 columns small-centered">
                    @include('includes.status-zurb')
                    @include('includes.errors-zurb')
                    @if(Session::get('errors'))
                        <div data-alert class="alert-box alert">
                            <a href="#" class="close">&times;</a>
                            There were errors while submitting this review:
                            @foreach($errors->all(":message") as $message)
                                <li>
                                    {{$message}}
                                </li>
                            @endforeach
                        </div>
                    @endif
                    @if(Session::has('review_posted'))
                        <div data-alert class="alert-box info">
                            <a href="#" class="close">&times;</a>
                            Your review has been posted!
                        </div>
                    @endif
                    @if(Session::has('review_removed'))
                        <div data-alert class="alert-box alert">
                            <a href="#" class="close">&times;</a>
                            Your review has been removed!
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="small-10 medium-7 columns">
                    <div class="small-9 medium-6 columns">
                        <h2>{{($user->first_name)}}</h2>
                        <h6>
                            @foreach($user->classifications->slice(0, 3) as $classification)
                                <a href="/{{$classification->slug}}">{{$classification->name}} |</a>
                            @endforeach
                            <a href="#reviews-anchor" onclick="ga('send', 'event', 'button', 'click', 'review-us');"
                               class="green open-review-box"> Review Us </a>
                        </h6>
                        <p>{{@$user->location->address}}</p>
                        @include('pages.share', [
                                'url' => request()->fullUrl(),
                                'description' => $user->profile->description,
                                'image' => 'https://s3.eu-central-1.amazonaws.com/blissful-ke/' .$user->profile->profilePic
                            ])
                    </div>
                    <div class="small-3 medium-6 columns">
                        <div class="section-attributes">
                            @if($user->top_vendor())
                                <div class="top-vendor-badge infotip">
                                    <img src="/img/super-vendor.svg">
                                    <span class="infotiptext">Super Vendor</span>
                                </div>
                            @endif
                            @if($user->recommended_vendor())
                                <div class="recommended-badge infotip">
                                    <i class="ion-thumbsup"> </i>
                                    <span class="infotiptext">Recommended</span>
                                </div>
                            @endif

                        </div>
                    </div>
                    <div class="small-12 medium-6 columns">
                        @if($user->can_claim())
                            <span>Do you own this business?</span>
                            <a class="red" href="/claim-business?slug={{$user->slug}}"> Claim business</a>
                        @endif
                    </div>
                </div>
                <div class="small-2 medium-5 columns">
                @if(!Auth::check() || Auth::user()->id != $user->id)
                    <a href="#" data-reveal-id="myModal" class="right">
                        <button class="dark_orange_background ripple v-contact small"
                                onclick="ga('send', 'event', 'button', 'click', 'contact-vendor');">Contact
                        </button>
                    </a>
                @endif
                
                </div>
            </div>
        </section>
        <!-- END OF SHOP ITEM HEADER SECTION -->
        <!-- START OF SHOP CART SECTION -->
        <section class="item-summary">
            <div class="row">
                <!-- Item Image -->
                <div class="small-12 medium-8 columns">
                    <ul class="lightSlider">

                        @foreach ($portfolio as $port)
                            @foreach ($port->images as $image)
                                <li>
                                    <img src="{{url('https://s3.eu-central-1.amazonaws.com/blissful-ke/' .$image->file_path)}}">
                                </li>
                            @endforeach
                        @endforeach
                    </ul>
                </div>
                <!-- Cart Details -->
                <div class="small-12 medium-4 columns cart">
                    <div class="row">
                        <div class="wrap">
                            <div class="small-12 columns">
                                <h6><strong>Description</strong></h6>
                                <p>
                                    {{str_limit($user->profile->description, 150)}}
                                </p>
                            </div>
                            <div class="small-12 columns">
                                <h6><strong>Rating</strong></h6>
                                <ul class="stars">
                                    @for ($i=1; $i <= 5 ; $i++)
                                        <li class="active"><span
                                                    class="fi-heart {{ ($i <= $user->profile->rating_cache) ? '' : 'inactive'}}"></span>
                                        </li>
                                    @endfor
                                </ul>

                            </div>
                            <div class="small-12 columns">
                                <!-- Cart Functions -->
                                <div class="small-12 columns">
                                    @if(!Auth::check() || Auth::user()->id != $user->id)
                                        <a href="#" data-reveal-id="myModal" class="hide-for-small-only">
                                            <button class="button buy ripple expand dark_orange_background ripple has-ripple "
                                                    onclick="ga('send', 'event', 'button', 'click', 'contact-vendor');">
                                                Contact
                                            </button>
                                        </a>
                                        <a href="#" data-reveal-id="myModal" class="show-for-small-only">
                                            <button class="button buy ripple expand dark_orange_background ripple has-ripple "
                                                    onclick="ga('send', 'event', 'button', 'click', 'contact-vendor');">
                                                Contact
                                            </button>
                                        </a>
                                        @include('pages.contact-modal')
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END OF SHOP CART SECTION -->
        <!-- START OF SHOP ITEM DESCRIPTORS -->
        <section class="item-desc">
            <div class="row">
                <div class="small-12 medium-8 columns">
                    <ul class="tabs" data-tab id="shop-item-tabs">
                        <li class="tab-title active"><a href="#panel1">DESCRIPTION</a></li>
                        <li class="tab-title"><a id="review-panel" href="#panel2">REVIEWS</a></li>
                    </ul>
                    <div class="tabs-content">
                        <div class="content active" id="panel1">
                            <h5>{{ $user->first_name }}</h5>
                            {{$user->profile->description}}
                        </div>
                        <div class="content" id="panel2">
                            @if(!Auth::check() )
                                <a href="/login"> Login to Review</a>
                            @elseif(Auth::user()->id != $user->id)
                                <a href="#reviews-anchor" onclick="ga('send', 'event', 'button', 'click', 'review-us');"
                                   class="review-text open-review-box"> Review Us</a>
                            @endif
                            <div class="well" id="reviews-anchor">
                                <div class="row">
                                    <div class="small-12 columns">
                                        @if(Session::get('errors'))
                                            <div data-alert class="alert-box alert">
                                                <a href="#" class="close">&times;</a>
                                                There were errors while submitting this review:
                                                @foreach($errors->all(":message") as $message)
                                                    <li>
                                                        {{$message}}
                                                    </li>
                                                @endforeach
                                            </div>
                                        @endif
                                        @if(Session::has('review_posted'))
                                            <div data-alert class="alert-box info">
                                                <a href="#" class="close">&times;</a>
                                                Your review has been posted!
                                            </div>
                                        @endif
                                        @if(Session::has('review_removed'))
                                            <div data-alert class="alert-box alert">
                                                <a href="#" class="close">&times;</a>
                                                Your review has been removed!
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="small-12 columns" id="post-review-box" style="display:none;">
                                    <div class="small-10 small-centered columns">
                                        {!! Form::open(['url' => '/vendor/'.$user->slug, 'id'=>'form','method' => 'POST'])!!}
                                        {!!Form::hidden('rating', null, array('class'=>'ratings-hidden'))!!}
                                        {!!Form::textarea('comment', null, array('rows'=>'5','id'=>'new-review','class'=>'form-control animated','placeholder'=>'Enter your review here...'))!!}
                                        <div class="text-right">
                                            <div class="stars starrr" data-rating="{{Input::old('rating',0)}}"></div>
                                            <a href="#" class="tiny" id="close-review-box"
                                               style="display:none; margin-right:10px;">
                                                <span class="glyphicon glyphicon-remove"></span>Cancel</a>
                                            <button class="tiny breaker" type="submit"
                                                    onclick="ga('send', 'event', 'button', 'click', 'submit-review');">
                                                Save
                                            </button>
                                        </div>

                                        {!! Form::close()!!}
                                    </div>
                                </div>

                                @foreach($reviews as $review)
                                    <hr>
                                    <div class="row">
                                        <div class="small-12 columns">
                                            <div class="rimg small-3 medium-2 columns">
                                                <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{ $review->user->profile->profilePic}}">
                                            </div>
                                            <div class=" arrow_box small-9 medium-10 columns">
                                                <ul class="stars">
                                                    {{ $review->user ? $review->user->first_name : 'Anonymous'}}
                                                    <span class="right">
                                                      @for ($i=1; $i <= 5 ; $i++)
                                                            <li class="active"><span
                                                                        class="fi-heart {{ ($i <= $review->rating) ? '' : 'inactive'}}"></span>
                                                          </li>
                                                        @endfor
                                                  </span>
                                                </ul>
                                                <p>{{$review->comment}}</p>
                                                <span>{{$review->timeago}}</span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END OF SHOP ITEM DESCRIPTORS -->

        <!-- popular deals -->
        <section class="browse-deals">
            <div class="row">
                <!-- <div class="small-12 medium-10 small-centered columns ad-section">
                    <a href="http://www.superbtent.com/" target="_blank">
                        <img class="hide-for-small-only" src="/img/SUPERB1-728X90.jpg">
                        <img class="show-for-small-only" src="/img/SUPERB-320x100.jpg">
                    </a>
                </div> -->
                <div class="small-12 columns">
                    <h3 class="shop_desc">Similar Vendors</h3>
                    <ul>
                        @foreach($similars as $similar)
                            @foreach($similar as $vendor)
                                <li class="card-box ripple small-12 medium-4 columns">
                                    <a href="/vendors/{{$vendor->slug}}">
                                        <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$vendor->profile->profilePic}}"
                                             style="height: 220px;"/>
                                        <div class="wrap">
                                            <h4 id="v-name">{{ $vendor->first_name }}</h4>
                                            <h6>
                                                <ul class="stars">
                                                    @for ($i=1; $i <= 5 ; $i++)
                                                        <li class="active"><span
                                                                    class="fi-heart {{ ($i <= $vendor->profile->rating_cache) ? '' : 'inactive'}}"></span>
                                                        </li>
                                                    @endfor
                                                </ul>
                                            </h6>
                                        </div> <!-- end card -->
                                    </a>
                                </li>
                            @endforeach
                        @endforeach
                    </ul>
                </div>
            </div>
            <div id="fb-root"></div>
        </section>
        <!-- END OF MAIN SECTION -->
    </main>
@endsection

@section('scripts')

    <script defer type="text/javascript" src="{{ elixir('js/vendor-page.js') }}" onLoad="startVendorScripts()"></script>
    <script>
        function startVendorScripts() {
            (function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

            $(document).ready(function () {
                var header_height = $('header').height();
                $('.title-header').css('margin-top', ' calc(' + header_height + 'px + 1rem)');
                var items = $('ul.lightSlider li').length + 1;
                $('.lightSlider').lightSlider({
                    gallery: false,
                    item: 1,
                    auto: true,
                    loop: false,
                    pauseOnHover: true,
                    pager: false,
                    speed: 1800,
                    thumbItem: items,
                    pause: 4000,
                    enableTouch: true,
                    enableDrag: true,
                });

                // initialize the autosize plugin on the review text area

                autosize($('#new-review'));
                var reviewBox = $('#post-review-box');
                var newReview = $('#new-review');
                var closeReviewBtn = $('#close-review-box');


                $('.open-review-box').click(function (e) {
                    $('#review-panel').click();
                    reviewBox.slideDown(400, function () {
                        $('#new-review').trigger('autosize.resize');
                        newReview.focus();
                    });
                    $('.review-text').fadeOut(100);
                    closeReviewBtn.show();
                });

                closeReviewBtn.click(function (e) {
                    e.preventDefault();
                    reviewBox.slideUp(300, function () {
                        newReview.focus();
                        $('.review-text').fadeIn(200);
                    });
                    closeReviewBtn.hide();

                });
                // If there were validation errors we need to open the comment form programmatically
                @if(Session::get('errors'))
                  $('#review-panel').click();
                $('.open-review-box').click();
                @endif



            });
            $('#event_date').fdatepicker({
                format: 'yyyy-mm-dd',
                disableDblClickSelection: true
            });

            
            $('#modal-back').click(function(){

                var current_q = $('.current-q');
                if (!current_q.is(':first-child')) {

                    current_q.prev().addClass('current-q');
                    current_q.removeClass('current-q');  
                }
                

            });
            $( "body" ).on( "click", "#budget-yes",function(e){
                e.preventDefault();

                var current_q = $('.current-q');
                var spinner = '<div class="cp-spinner cp-skeleton"></div>';
                $(this).html('');
                $(this).html(spinner);
                current_q.find('label').html('');
                current_q.find('label').html('Input budget');
                var input = '<input name=\'budget\' type=\'number\' min-value=\'5000\' />';
                $('.budget-btns').remove();
                current_q.append(input);


            });
            $( "body" ).on( "click", "#budget-no",function(e){
                e.preventDefault();

                var spinner = '<div class="cp-spinner cp-skeleton"></div>';
                $(this).html('');
                $(this).html(spinner);

                var current_q = $('.current-q');
                //set variables
                var budget_params = {
                    classification_id: $("#service").find(':selected').data('id'),
                    location: $('#location').val(),
                    event_date: $('#event_date').val()
                };

                $.ajax({
                    type: 'get',
                    url: '/budget/create',
                    data: budget_params,
                    success: function(data) {
                        current_q.empty().append(data);

                    }
                });

                

            });
            $( document ).on( 'click', '.change-budget', function(e) {
                e.preventDefault();
                //get name of the input
                var name = $(this).data('inputName');

                var input = $("input[name="+name+"]");

                var input_val = input.val();
                var id = input.data('estimateId');

                //change name to budget
                input.attr('name','budget-'+id+'').removeAttr('disabled');
                //insert hidden input with estimate value
                $('<input name=\"'+name+'\" type=\"number\" disabled required data-parsley-type=\"number\" style=\"display:none;\" value=\"'+input_val+'\" data-parsley-group = \"budget-estimates\" data-parsley-trigger = \"change focusout\" min=\"5000\"/>').insertAfter(input);

                //show suggestion
                input.siblings('div.estimate-suggestion').addClass('estimate-visible');
                //focus on the input
                input.focus();
                //hide change button
                $(this).hide();

            });
            $( "body" ).on( "click", "#modal-next",function(e){

                e.preventDefault();
                var the_form = $('#contact-form');
                var current_q = $('.current-q');

                //check if they have set a budget

                if(current_q.hasClass('budget-qn') && current_q.find('input').length < 1){
                    return false;
                }

                //validate form elements

                form_validation = validateInputs(the_form);
                if (form_validation) {
                    if (!current_q.is(':last-child')) {
                        current_q.next().addClass('current-q');
                        current_q.removeClass('current-q');
                        $('.error_message').remove();
                    }
                    else{
                        $('.estimate-budget-input').removeAttr('disabled');
                        var the_form = $('#contact-form');
                        the_form.submit();
                    }
                }
                else{
                    return false;
                }
                
            
            });
        }
        function validateInputs(the_form){
                var current_q = $('.current-q');

                var group = current_q.find(':input');
                // validate inputs
                var inputs_valid = true;

                $.each( group, function( key, input ) {
                    
                    if (input.getAttribute('type') == 'email') {
                        var valid = validateEmail(input.value);
                        if (!valid) {
                            var message = 'Please insert a valid email';
                            showError(message);
                            return inputs_valid = false;
                        }
                    }
                    else if (input.getAttribute('name') == 'event_date') {
                        var value = input.value;
                        var valid = validatedate(input);
                        if (!valid) {
                            var message = 'Please input a valid date';
                            showError(message);
                            return inputs_valid = false;
                        }
                    }
                    else if (input.getAttribute('type') == 'text' || input.getAttribute('id') == 'description') {
                        var length = input.getAttribute('min-length');
                        var value = input.value;
                        var valid = validateText(value,length);
                        if (!valid) {
                            var message = 'The input text is too short, should be at least '+length+' characters';

                            showError(message);
                            return inputs_valid = false;
                            
                        }
                    }
                    else if (input.getAttribute('type') == 'number') {
                        var length = input.getAttribute('min-length');
                        var min_value = input.getAttribute('min-value');
                        var value = input.value;
                        var valid = validateNumber(value,min_value,length);
                        if (!valid) {
                            if (min_value) {
                                var message = 'The minimum budget allowed is '+min_value;
                            }
                            else{
                                var message = 'Please enter a valid phone number';
                            }

                           showError(message);
                           return inputs_valid = false;
                        }
                    }


                });

                return inputs_valid;
        }
        function validateEmail(email) {
          var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(email);
        }
        function validateText(str,length){
            if (str.replace(/\s/g, '').length >= length) {
                return true;
            }
        }
        function validateNumber(value,min_value,length){
            if (value.length >= length && value >= min_value) {
                return true;
            }

        }
        function validatedate(dateVal)  
        {  
            var dateVal = dateVal.value;
 
            if (dateVal == null) 
              return false;
     
            var validatePattern = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
     
              dateValues = dateVal.match(validatePattern);
     
              if (dateValues == null) 
                  return false;
     
            var dtYear = dateValues[1];        
              dtMonth = dateValues[3];
              dtDay=  dateValues[5];
     
            if (dtMonth < 1 || dtMonth > 12) 
              return false;
            else if (dtDay < 1 || dtDay> 31) 
             return false;
            else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
             return false;
            else if (dtMonth == 2){ 
             var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
             if (dtDay> 29 || (dtDay ==29 && !isleap)) 
                    return false;
          }
     
         return true;      
        } 
          function showError(message){
            
            var error_container = $('.error_container');
            var error_element = $('<p class=\"red error_message\"></p>');
            if ($('.error_container > p:contains('+message+')').length < 1) {
                error_container.append(error_element.text(message));
            }
            
            return false;
          }
    </script>
@stop