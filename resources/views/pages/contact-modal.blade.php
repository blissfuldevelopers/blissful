<style type="text/css">
  .contact-qs{
    display: block;
    margin-top: 40px;
    overflow: hidden;
    max-height: 230px;
  }
  .contact-qs select, .contact-qs input {
    width: 70%;
    font-size: 1.2rem;
    height: auto;
    margin: 0 auto;
    display: block;
    border-radius: 5px;
  }
  .contact-qs .user-info input{
    width: 100%;
  }
  .contact-qs textarea{
    width: 80%;
    font-size: 1.2rem;
    min-height: 120px;
    margin: 0 auto;
  }
  .contact-qs label {
    text-align: center;
    font-size: 1.5rem;
    width: 100%;
    margin-bottom: 30px;
  }
  .contact-qs > div{
    -ms-transform: translateY(300px); /* IE 9 */
    -webkit-transform: translateY(300px); /* Safari */
    transform: translateY(300px);
    visibility:hidden;
    opacity:0;
    transition:visibility 0.2s linear 0.3s,opacity 0.2s linear 0.3s,transform linear .7s;
  }
  .contact-qs > div.current-q{
    position: absolute;
    top: 70px;
    left: 0;
    right: 0;
    visibility: visible;
    opacity: 1;
    -ms-transform: translateY(0); /* IE 9 */
    -webkit-transform: translateY(0); /* Safari */
    transform: translateY(0);
  }
  #modal-back{
    font-size: 1rem;
    background: transparent;
    color: #008cba;
  }
  #modal-back:active,#modal-back:focus{
    border: none;
    outline: none;
  }
  #budget-yes{
    font-size: 1rem;
    background: transparent;
    color: #008cba;
    border: 1px solid #008cba;
    border-radius: 100px;
  }
  #budget-no{
    font-size: 1rem;
    background: transparent;
    color: #00a89b;
    border: 1px solid #00a89b;
    border-radius: 100px;
  }
  #budget-yes:active,#budget-yes:focus, #budget-no:active, #budget-no:focus{
    outline: none;
  }
</style>
<div id="myModal" class="reveal-modal fifty" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  <div class="tabs-content messagecontent">
      <div class="content active" id="panel1">
          <div class="row">
              {!! Form::open(['route' => 'jobs.store', 'id' => 'contact-form']) !!}
              <div class="row ">
                  <div class="form-group">
                      <input type="hidden" name="invitee" id="recipients"
                             value="{{$user->id}}">
                  </div>
                  <!-- Subject Form Input -->
                  <div class="contact-qs">
                      <div class="small-12 columns current-q">
                        <label for="service">What service would you like {{$user->first_name}} to provide?</label>
                        <select id="service" class="breaker-bottom" name="classification[]" required data-parsley-required-message="Please select a service" data-parsley-trigger="change focusin focusout" data-parsley-group="block1">
                                @foreach($user->classifications as $classification)
                                <option data-id="{{$classification->id}}" value="{{$classification->slug}}">{{$classification->name}}</option>
                                @endforeach
                        </select>
                      </div>
                      <div class="small-12 columns">
                        <label for="location"> Where will the event be held?</label>
                        <select id="location" name="event_location" required data-parsley-group="block2">
                          @foreach($locations as $location)
                          <option value="{{$location->name}}">{{$location->name}}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="small-12 columns">
                        <label for="event_date">When will the event be held?</label>
                        {!! Form::text('event_date', null, [
                          'required',
                          'id'                            => 'event_date',
                          'class'                         =>'span2',
                          'data-parsley-required-message' => 'Please input when your event will be held',
                          'data-parsley-group'            =>'second',
                          'data-parsley-trigger'          => 'change focusin focusout',
                        ]) !!}
                      </div>
                      <div class="small-12 columns budget-qn">
                        <label id="budget">Do you have a set budget?</label>
                        <div class="small-12 columns hm budget-btns">
                          <button id="budget-yes">Yes</button>
                          <button id="budget-no">No</button>
                        </div>
                      </div>
                      <div class="small-12 columns">
                          {!! Form::label('description', 'Please describe the services needed in detail.') !!}
                          {!! Form::textarea('description', null, ['rows'=>'3',
                            'id'                            =>  'description',
                            'required',
                            'min-length'                    =>  '12',]) !!}
                      </div>
                      @if(!Auth::check())
                      <div class="small-12 user-info">
                          <div class="small-12 medium-4 columns">
                              {!! Form::label('email', 'Email') !!}
                              {!! Form::email('email',null, [
                                'id'                            =>  'email',
                                'required'
                                ]) !!}
                          </div>
                          <div class="small-12 medium-4 columns">
                              {!! Form::label('first_name', 'Name') !!}
                              {!! Form::text('first_name', null, [
                                'id'                            =>  'first_name',
                                'required',
                                'min-length'                    =>  '5'
                              ]) !!}
                          </div>
                          <div class="small-12 medium-4 columns">
                              {!! Form::label('phone', 'Phone number') !!}
                              {!! Form::input('number','phone','07', [
                                'id'                            =>  'phone_number',
                                'required',
                                'min-length'                    =>  '9',
                              ]) !!}
                          </div>
                      </div>
                         
                      @endif
                  </div>
                  <div class="row error_container hm"></div>
              </div>

            {!! Form::close() !!}
            
            <div class="row">
              <div class="small-12 columns">
                <button id="modal-back" class="small"> <span>&#x0003C; Back</span> </button>
                <button id="modal-next" class="small right">Next</button>
              </div>
            </div>
          </div>
      </div>
  </div>
  <a class="close-reveal-modal md" aria-label="Close">&#215;</a>
</div>
