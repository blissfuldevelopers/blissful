 
<style type="text/css">
.social_container{
    bottom: 30px;
    position: fixed;
    right:10px;
    z-index: 99;

}
.social_box {
    background: #FF9700;
    color:#fff;
    text-align: center;
    border-radius: 5px;
    padding: 8px;
    display: none;
    margin-bottom: 8px;
    font-weight: 100;
    font-family: 'clear_sanslight' !important;
}
/*.first{
    bottom: 100px;

}
.second{
    
}*/
@media(max-width: 768px){
  .social_container {
    right: 0;
   }
    
}
</style>
<div class="social_container small-12 medium-3">
    <div  class="first social_box small-12">
        <span class="dets">
            <strong class="social_number"></strong>
            <span class="social_message"></span> 
            <span class="social_day_number"></span>
        </span>
    </div>
    <div  class="second social_box small-12">
        <span class="dets">
            <strong class="social_number"></strong> 
            <span class="social_message"></span>
            <span class="social_day_number"></span>
        </span>
    </div>   
</div>
