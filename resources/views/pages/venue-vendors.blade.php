@extends('page.home-layout')
@section('title')
    @if($classification->meta_title)
        {{$classification->meta_title}}
    @else
        {{$classification->name}}
    @endif
@stop
@section('metas')
    {{--@if($classification->primary_keyword)--}}
        {{--<meta name="keyword" content="{{$classification->primary_keyword}}"/>--}}
    {{--@endif--}}
    @if($classification->meta_description)
        <meta name="description" content="{{$classification->meta_description}}"/>
    @endif
    <link rel="canonical" href="https://www.blissful.co.ke/{{$classification->name}}"/>
    @if($arrays->previousPageUrl())
    <link rel="prev" href="{!!$arrays->previousPageUrl()!!}" />
    @endif
    @if($arrays->nextPageUrl())
    <link rel="next" href="{!!$arrays->nextPageUrl()!!}" />
    @endif
@endsection
@section('header-content')
  <?php
    $header_class = 'catpg-intro';
  ?>
    <!-- intro -->
    <div class="row">
        <div class="medium-10 small-12 medium-centered columns">
            <div class="row">
                <div class="small-12 columns">
                    <h2>Find The<br><span class="ucase">Perfect Venue</span><br>For Your Event</h2>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <!-- venues filters -->
    <div class="vencatpg-filters">
        <div class="row">
            <div class="medium-11 small-12 medium-centered columns">
                <div class="row">
                    <div class="small-12 medium-5 columns">
                        <form class="filters" method="GET" action="/venues/search">
                            <input type="hidden" name="venue_search" value="{{$classification->id}}">
                            <input type="text" name="location" id="pac-input" placeholder="Enter a location">                            
                            <select class="typ" name="type">
                                <option selected disabled>Type of venue</option>
                                <option value="any">Any</option>
                                @foreach($classification->children as $child)
                                <option value="{{$child->id}}">{{$child->name}}</option>
                                @endforeach
                            </select>
                            <div class="small-12">
                                <label>Number of guests</label>
                            </div>
                            <div class="slider" data-slider data-initial-start="50" data-initial-end="2500" data-options="start: 25;end:3000;">
                                <span class="slider-handle" data-slider-handle role="slider" tabindex="1"></span>
                                <span class="slider-fill" data-slider-fill></span>
                                <span class="slider-handle" data-slider-handle role="slider" tabindex="1"></span>
                                <input name="min_capacity" class="capacity-input">
                                <input name="max_capacity" class="capacity-input">
                            </div>
                            <input type="text" name="date" class="dat" placeholder="Event Date">
                            <div class="small-12">
                                <label>Budget Range</label>
                            </div>
                            <div class="slider" data-slider data-initial-start="6000" data-initial-end="800000" data-options="start: 5000;end:1000000;">
                                <span class="slider-handle" data-slider-handle role="slider" tabindex="1"></span>
                                <span class="slider-fill" data-slider-fill></span>
                                <span class="slider-handle" data-slider-handle role="slider" tabindex="1"></span>
                                <input name="min_budget" class="budget-input">
                                <input name="max_budget" class="budget-input">
                            </div>
                            <!-- instant booking radio button -->
                            <div class="switch">
                                <label class="ibk">Instant Booking</label>
                                <input class="switch-input" id="exampleSwitch" type="checkbox" name="exampleSwitch">
                                <label class="switch-paddle" for="exampleSwitch">
                                    <span class="show-for-sr">Instant Booking</span>
                                    <span class="switch-active" aria-hidden="true">Off</span>
                                    <span class="switch-inactive" aria-hidden="true">On</span>
                                </label>
                            </div>
                            <a class="button venue-search">Find a Venue</a>
                        </form>
                    </div>
                    <div class="small-12 medium-7 columns hide-for-small-only">
                        <div id="search-map" class="vendor-list-map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3012.5216678912807!2d36.81448862499067!3d-1.2646047746338347!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2ske!4v1484645803780" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- vendor listings -->
    <div class="vencatpg-list">
                    <!-- Adsense Section 1-->
        <!-- Adsense Section 1-->
        <div class="row">
            <div class="small-12 columns breaker breaker_bottom">
                <div class="hide-for-small-only">
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Blissful_responsive -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-2847929148248604"
                         data-ad-slot="1315972373"
                         data-ad-format="auto"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
                <div class="show-for-small-only">
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- mobile_horizontal banner -->
                    <ins class="adsbygoogle"
                         style="display:inline-block;width:320px;height:100px"
                         data-ad-client="ca-pub-2847929148248604"
                         data-ad-slot="2792705573"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <ul class="venues">
                    @foreach ($arrays as $vendor)
                    <li class="medium-4 small-12 columns">
                        <!-- instant booking badge -->
                        <span class="ist-bk">
                            <img src="/img/badge.svg">
                        </span>
                        <div class="vimg"><img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$vendor->profile->profilePic}}"></div>
                        <div class="vwrap">
                            <h5>{{$vendor->first_name}}</h5>
                            <h6>{{isset($vendor->location->address) ? $vendor->location->address : $vendor->profile->location}}</h6>
                            <ul class="stars">
                                @for ($i=1; $i <= 5 ; $i++)
                                <li>{!! ($i <= $vendor->profile->rating_cache) ? '&starf;' : '&#x02729;'!!} </li>
                                @endfor
                            </ul>
                            <a href="/vendor/{{$vendor->slug}}" class="button">Reserve</a>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="small-12 medium-9 small-centered">
                {!! $arrays->render(Foundation::paginate($arrays)) !!}
            </div>
        </div>
        <!-- Adsense Section 1-->
        <div class="row">
            <div class="small-12 columns breaker breaker_bottom">
                <div class="hide-for-small-only">
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Blissful_responsive -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-2847929148248604"
                         data-ad-slot="1315972373"
                         data-ad-format="auto"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
                <div class="show-for-small-only">
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- mobile_horizontal banner -->
                    <ins class="adsbygoogle"
                         style="display:inline-block;width:320px;height:100px"
                         data-ad-client="ca-pub-2847929148248604"
                         data-ad-slot="2792705573"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script defer src="{{ elixir('js/venue-vendor.js') }}"></script>
<script  defer src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&libraries=places&callback=initMap"></script>

@endsection