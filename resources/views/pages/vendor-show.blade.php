@extends('page.home-layout')
@section('title')
    {{($user->first_name)}}
@endsection
@section('metas')
    <meta property="og:url" content="{{request()->fullUrl()}}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{$user->first_name}} | Blissful"/>
    <meta property="og:description" content="{{$user->profile->description}}"/>
    <meta property="og:image"
          content="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$user->profile->profilePic}}"/>
    <meta property="fb:app_id" content="1615380565398173"/>
@endsection
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/vendor-show.css') }}">
    <link href="{{ url('_asset/css') }}/daterangepicker.css" rel="stylesheet">
@endsection
@section('header-content')
    <?php
    $header_class = 'vendorpg-intro';
    ?>
    <div class="vendorpg-intro">
        <!-- intro -->
        <div class="row">
            <div class="small-12 medium-6 columns">
                <h2>{{($user->first_name)}}</h2>
                <h4 class="cat">
                    @foreach($classifications as $key => $classification)
                        <a href="/{{$classification->slug}}">{{$classification->name}} {{((count($classifications) - 1) == $key) ? '' : ' |'}}</a>
                    @endforeach
                </h4>
                <h5 class="loc">{{isset($user->location->address) ? $user->location->address : $user->profile->location}}</h5>
            </div>
            <div class="small-12 medium-6 columns">
                <div class="wrapper">
                    <!-- <h5>Hired <b>20</b> Times</h5> -->
                    <ul class="stars">
                        @for ($i=1; $i <= 5 ; $i++)
                            <li>{!! ($i <= $user->profile->rating_cache) ? '&starf;' : '&#x02729;'!!} </li>
                        @endfor
                    </ul>
                    <span class="reviews">({{$user->profile->reviews->count()}} {{($user->profile->reviews->count() == 1 ? 'Review' : 'Reviews')}}
                        )</span>
                </div>

            </div>
            <div class="small-12 medium-6 columns">
                @include('pages.share', [
                                          'url' => request()->fullUrl(),
                                          'description' => $user->profile->description,
                                          'image' => 'https://s3.eu-central-1.amazonaws.com/blissful-ke/' .$user->profile->profilePic
                                      ])
            </div>
            <div class="small-12 medium-6 columns vendr-claim">
                @if($user->can_claim())
                    <span>Do you own this business?</span>
                    <a class="red" href="/claim-business?slug={{$user->slug}}"> Claim business</a>
                @endif

            </div>
        </div>
    </div>
@endsection
@section('content')
    <!-- how it works -->
    <div class="vendorpg-main">
        <div class="row">
            <div class="small-12 medium-7 large-6 breaker columns small-centered">
                @include('includes.status')
            </div>
            <div class="small-12 columns">
                <div class="wrapper">
                    <section class="vendorpg-act">
                      <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$user->profile->profilePic}}"
                           class="profile">
                      @if($user->top_vendor() || $user->recommended_vendor())
                          <span class="verify">
                            <img src="/img/verified.png">
                          </span>
                      @endif

                      @if($user->can_book())
                      <a data-toggle="bookingModal" class="button">Book Now</a>

                      <div class="reveal" id="bookingModal" data-reveal data-close-on-click="false"
                           data-animation-in="slide-in-right" data-animation-out="slide-out-right">

                        <form class="booking-form" data-abide novalidate>
                            <input type="hidden" name="vendor_id" value="{{$user->id}}">
                            <p class='lead'>Fill in your details below to place a booking
                                for {{$user->first_name}}</p>
                          <div class="row">
                              <div class="small-12 columns">
                                  <div class="small-3 columns">
                                      <label for="date" class="text-right middle">Booking Date</label>
                                  </div>
                                  <div class="small-9 columns">
                                      <input type="text" class="date-pick" name="time" id="time"
                                             placeholder="Select your time" value="{{ @$date }}" onchange="check_date();">
                                      <span class="form-error">
                                        Please input the event date
                                      </span>
                                  </div>
                              </div>
                              <div class="small-12 columns">
                                  <div class="small-3 columns">
                                      <label for="name" class="text-right middle">Name</label>
                                  </div>
                                  <div class="small-9 columns">
                                      <input name="name" type="text" id="name" placeholder="Type in your name"
                                             data-live-validate required>
                                      <span class="form-error">
                                        Please input your name
                                      </span>
                                  </div>
                              </div>
                              <div class="small-12 columns">
                                  <div class="small-3 columns">
                                      <label for="email" class="text-right middle">Email</label>
                                  </div>
                                  <div class="small-9 columns">
                                      <input name="email" type="email" id="email" placeholder="Email"
                                             data-live-validate required>
                                      <span class="form-error">
                                        Please input your email
                                      </span>
                                  </div>
                              </div>
                              <div class="small-12 columns">
                                  <div class="small-3 columns">
                                      <label for="phone" class="text-right middle">Phone Number</label>
                                  </div>
                                  <div class="small-9 columns">
                                      <input name="phone" type="number" id="phone" pattern=".{8,}"
                                             placeholder="07XX XXX XXX" data-live-validate required>
                                      <span class="form-error">
                                        Please input a valid phone number
                                      </span>
                                </div>
                              </div>   
                          <div class="small-12 columns">
                            <button type="submit" class="button"> <span>Place Booking</span> </button>
                          </div>
                        </div>
                      </form>
                      <button class="close-button" data-close aria-label="Close reveal" type="button">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    @else
                    <a href="#" class="button">Get Quotes</a>
                    @endif
                    @if(Auth::check())
                    <form method="POST" action="{{route('bookmarks.store')}}" class="bookmark-form">
                      <input type="hidden" name="vendor_id" value="{{$user->id}}">
                      <button type="submit"> <i class="fi-plus"></i> Bookmark Vendor</button>
                    </form>
                    @endif
                  </section>
        <section class="intro">
          <p>{{$user->profile->description}}</p>
        </section>
         <!-- if vendor has venue services -->
        <section class="venue-detz">
          <dl>
            @foreach($user->descriptions as $description)
            <dd>
              <h6>{{$description->option->name}}</h6>
              @if($description->option->name == 'Price')
              <p>{{number_format($description->value)}} Kes.</p>
              @elseif($description->option->name == 'Capacity')
              <p>{{number_format($description->value)}} People</p>
              @else
              <p>{{$description->description_detail}}</p>
              @endif
            </dd>
            @endforeach
          </dl>
        </section>
        <section class="gallery">
          <ul>
            @foreach ($portfolio as $port)
                @foreach ($port->images as $image)
                  <li>
                      <img data-lazy="{{url('https://s3.eu-central-1.amazonaws.com/blissful-ke/' .$image->file_path)}}">
                  </li>
                @endforeach
            @endforeach
          </ul>
        </section>
        <div class="flex-wrap">
          <section class="reviews">
            <h4>Reviews</h4>
            <span class="vendorpg-reviews">
              <ul class="stars">
                @for ($i=1; $i <= 5 ; $i++)
                      <li>{!! ($i <= $user->profile->rating_cache) ? '&starf;' : '&#x02729;'!!} </li>
                  @endfor
              </ul>
              <span class="reviews">({{$user->profile->reviews->count()}} {{($user->profile->reviews->count() == 1 ? 'Review' : 'Reviews')}}
                  )</span>
            </span>
                            <div class="row">
                                <div class="small-10 columns small-centered">
                                    @include('includes.status-zurb')
                                    @include('includes.errors-zurb')
                                    @if(Session::get('errors'))
                                        <div data-alert data-closable class="callout alert">
                                            <button class="close-button" aria-label="Dismiss alert" type="button"
                                                    data-close>
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            There were errors while submitting this review:
                                            @foreach($errors->all(":message") as $message)
                                                <li>
                                                    {{$message}}
                                                </li>
                                            @endforeach
                                        </div>
                                    @endif
                                    @if(Session::has('review_posted'))
                                        <div data-alert data-closable class="callout success">
                                            <button class="close-button" aria-label="Dismiss alert" type="button"
                                                    data-close>
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            Your review has been posted!
                                        </div>
                                    @endif
                                    @if(Session::has('review_removed'))
                                        <div data-alert data-closable class="callout alert">
                                            <button class="close-button" aria-label="Dismiss alert" type="button"
                                                    data-close>
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            Your review has been removed!
                                        </div>
                                    @endif
                                </div>
                            </div>
                            @foreach($reviews as $review)
                                <div class="review">
                                    <h5>{{ $review->user ? $review->user->first_name : 'Anonymous'}}</h5>
                                    <h6>{{$review->timeago}}</h6>
                                    <div class="reviewer-container">
                                        @if($review->user)
                                            <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$review->user->profile->profilePic}}">
                                        @else
                                            <img src="/img/anaony.png">
                                        @endif
                                        <ul class="stars">
                                            @for ($i=1; $i <= 5 ; $i++)
                                                <li>{!! ($i <= $review->rating) ? '&starf;' : '&#x02729;'!!} </li>
                                            @endfor
                                        </ul>
                                    </div>
                                    <!-- <h4>Amazing Food!</h4> -->
                                    <p>{{$review->comment}}</p>
                                </div>
                            @endforeach
                            <div class="small-12 columns" id="post-review-box">
                                @if(!Auth::check())
                                    <p><a href="/login">Login/Register to review {{$user->first_name}}</a></p>
                                @elseif(Auth::user()->id != $user->id)
                                    <div class="small-12 small-centered columns review-container">
                                        {!! Form::open(['url' => '/vendor/'.$user->slug, 'id'=>'review-form','method' => 'POST'])!!}
                                        {!!Form::hidden('rating', null, array('class'=>'ratings-hidden'))!!}
                                        {!!Form::textarea('comment', null, array('rows'=>'5','id'=>'new-review','class'=>'form-control animated','placeholder'=>'Enter your review here...'))!!}
                                        <div class="text-right">
                                            <div class="stars starrr" data-rating="{{Input::old('rating',0)}}"></div>
                                            <button class="submit-review button" type="submit"
                                                    onclick="ga('send', 'event', 'button', 'click', 'submit-review');">
                                                Post Review
                                            </button>
                                        </div>
                                        {!! Form::close()!!}
                                    </div>
                                @endif
                            </div>
                        </section>
                        <!-- map section -->
                        <div class="inner-flex">
                            @if(isset($user->location->lat))
                                <section class="vendor-map">
                                    <iframe width="100%" height="100%" frameborder="0" style="border:0"
                                            src="https://www.google.com/maps/embed/v1/place?q={{$user->location->lat}},{{$user->location->lng}}&amp;key={{env('GOOGLE_MAPS_KEY')}}"></iframe>
                                </section>
                        @endif
                        <!-- ads section -->
                            <!-- <section class="ads">
                              <span>ADS</span>
                            </section> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script defer src="{{ elixir('js/vendor-show.js') }}" onLoad="check_date();"></script>
    <script type="text/javascript">
    function check_date(){
      var bookings = [];
      var val = $('.date-pick').val();

      @foreach($user->bookings as $booking)
      bookings.push("{{date("d/m/Y",strtotime($booking->date))}}");
      @endforeach
      if ($.inArray( val, bookings ) != -1) {
        $('.date-pick').addClass('red_border').val('').attr('placeholder','Sorry, the vendor is booked on this date');
        $('.booking-form button.button').hide();
      }
      else{
        $('.date-pick').removeClass('red_border');
        $('.booking-form button.button').show();
      }
    }
    </script>
@endsection