@extends('page.zurb-layout')
@section('title')
 Vendor Categories
@endsection
@section('head')
    
@endsection

@section('content')
<div id="home_row" class="row">
    <div class="plac_cards small-12 columns">
        <h2>Explore Suppliers</h2>
        <h3>See what services are available around you.</h3>
        <ul class="breadcrumbs">
          <li><a href="/">Home</a></li>
          <li><a href="/vendor/list">Suppliers</a></li>
          <li class="current"><a href="#">Venues</a></li>
        </ul>
        @foreach($classifications as $classification)
         <div class="small-12 large-3 columns">
            <div class="ripple placard">
              <div class="image ">
                <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke//img/hcat/1.jpg">
              </div>
              <div class="content">
                <a href="/vendor/list/{{$classification->id}}">{{$classification->name}} <a href="#" class="link hide-for-small-only">&#10095;</a></a> 
                <p>{{$classification->description}}</p>
           
              </div>
            </div>
          </div>
          @endforeach
    </div>
</div>
@endsection