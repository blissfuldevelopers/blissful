<div class="row">
    <div class="small-12">
             @if(@$success)
                <div data-alert class="alert-box alert-box-messages success">
                   Your request has been successfully sent.
                    <a href="#" class="close">&times;</a>
                </div>
            @endif
    </div>
    <div class="small-12 medium-6 columns"> 
        {!! Form::open(['name'=>'review-request','route' => 'review-request']) !!}
        <div class="row">
        <p>
            Businesses with reviews on their pages are trusted more by users. If you have someone you have worked with before of whom you'd like to be reviewed by on blissful, you can ask them to do so below.
        </p>
            {!! Form::label('Email', 'Please Enter Their Email') !!}
            {!! Form::email('email', null) !!}
        </div>

        <div class="row">
            {!! Form::submit('Submit', ['class' => 'button tiny right form-control', 'id'=>'review-request']) !!}
        </div>
        {!! Form::close() !!}
    </div>
    <div class="small-12 medium-6 columns disabled">
        <h3>Hi,</h3>
        <p>
            My business relies on recommendations from my clients. I would appreciate it if you would write
            a brief review for me on www.blissful.co.ke, the largest and most influential directory for
            wedding & events professional in Kenya.
        </p>
        <p>
            Kindly click on the link below and share your experience when you worked with me.
        </p>
        <p>
            <p>Thanks.</p>
            <p>{{Auth::user()->first_name}}</p>
        </p>
    </div>

</div>