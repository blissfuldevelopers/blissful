@extends('page.home-layout')
@section('title')
    Privacy Policy
@endsection

@section('header-content')
  <?php
    $header_class = 'contentpg-intro';
  ?>
@endsection
@section('content')
    <!-- main content  -->
    <div class="contentpg-main">
        <div class="row">
          <div class="small-12 columns">
            <div class="wrapper">
            <div class="small-12 medium-7 columns small-centered">
                <h2> Blissful Privacy Policy </h2>
                  <div style="height:500px;line-height:3em;overflow:auto;padding:5px;">
                       <p>Blissful is committed to protecting your privacy and using technology that gives you the safest and

                        most powerful online experience. We promise to work hard to earn and keep your trust, and

                        therefore we will never rent or sell your personally identifiable information to third parties for

                        marketing purposes or share your contact information with another user without your consent.

                        By using the Blissful Website, you consent to the data practices described in this Privacy Policy as

                        well as in our Terms of Use. We will post notices of all changes that materially affect the way in

                        which your personally identifiable information may be used or shared in updates to our Privacy

                        Policy. If you continue to use Blissful�s Website and Services after these notices of changes have been

                        posted to the site, you hereby provide your consent to the changed practices.</p>

                        <h4>How We Collect Your Information</h4>

                        <p>The first thing to remember about privacy is that there is only so much information that any website

                        can collect without your answering of specific questions. For example, in order to post any content

                        on the Blissful Website, you must first register. During the registration process you are required to

                        provide certain personally identifiable information, such as your e-mail address.</p>

                        <p>Blissful logs IP addresses (in other words, the location of your computer on the Internet), a unique

                        device ID, system and browser type, domain names, the dates and times that you visit the site,

                        referring website addresses, and other such information for system administration, troubleshooting,

                        and to examine overall traffic trends. This information, in aggregate form, is recorded for

                        administrative purposes only. IP addresses are not linked to any personal information. We also use

                        cookies and similar technology (as described below) to collect information for the purpose of

                        customizing the content you receive.</p>

                        <p>We also may collect information from third party websites with whom we connect. As an example, if

                        you connect to our site via Facebook, Facebook provides us with certain demographic information

                        from your Facebook account to the extent that you elect to make publicly available on Facebook. If

                        you have any questions about Facebook�s Privacy Policy, you can review it here,

                        http://www.facebook.com/about/privacy/.

                        <h3>User Content</h3>

                        <p>Any content you post on the Blissful Website becomes public. By submitting content or posting

                        information on public message boards [profile pages, public forums, blogs, comments, image

                        galleries, inspiration boards and favorites], this information may be collected and used by others.

                        User Content may be also used by Blissful as described in our Terms of Use. We are not responsible

                        for any personally identifiable information that you choose to include in such User Content. You

                        understand and acknowledge that, even after removal, copies of User Content may remain viewable

                        in cached and archived pages or if other users have copied or stored your User Content.</p>

                        <h4>Use of your Personal Information</h4>

                        <p>Blissful collects and uses your personal information to operate the Blissful Website and deliver the

                        services you have requested. Blissful also uses your personally identifiable information to inform

                        you of other products or services available from Blissful and its affiliates.</p>

                        <p>Blissful does not sell, rent or lease its user lists to third parties. However, should someone choose to

                        contact via the contact form on the website, application and or service, we may forward your

                        contacts to them. We may, from time to time, also contact you on behalf of external business

                        partners about a particular offering that may be of interest to you. In those cases, your unique

                        personally identifiable information (e-mail, name) is not transferred to the third party. We may also

                        share aggregated information that cannot be used to identify you with our clients and business

                        partners.</p>

                        <p>Blissful shares data with trusted partners to help us customize the content and advertising you see,

                        improve our services, conduct research, perform statistical analysis, send you email or postal mail,

                        or provide user support. All such third parties are prohibited from using your personal information

                        except to provide these services to Blissful and they are required to maintain the confidentiality of

                        your information.</p>

                        <p>Blissful will use any data collected to enforce our Terms of Use, to comply with legal obligations, act

                        under exigent circumstances to protect the personal safety of users of Blissful or the public, and

                        otherwise manage our business.</p>

                        <p>Blissful may share your information with any company who may purchase Blissful or substantially

                        all of our assets in the future.</p>

                        Blissful will only use and share your information as described in this policy.

                        <h4>Text Message Service and Other Communication</h4>

                        <p>We may seek to send messages to you on your wireless device via short message service ("Text

                        Messaging"). By submitting your mobile phone number, you consent to receive text messages and

                        offerings from us to the mobile device associated with that number using an automatic system for

                        such purposes unless and until you elect not to receive such messages by following the opt-out

                        instructions provided in connection the particular campaign.</p>

                        <p>You represent that you are the owner or authorized user of the device you use to sign up for Text

                        Messaging, and that you are authorized to approve the applicable charges. You may not consent on

                        behalf of someone else. You consent to receive phone calls from us even and other Blissful Users</p>

                        <h4>Use of Cookies</h4>

                        <p>Like most websites, Blissful uses �cookies� to help you personalize your online experience. A cookie

                        is a tiny data file which resides on your computer and allows Blissful to recognize you as a user

                        when you return to the Blissful website from the same computer and browser. Cookies cannot be

                        used to run programs or deliver viruses to your computer. Cookies are uniquely assigned to you,

                        and can only be read by a web server in the domain that issued the cookie to you.</p>


                        <p>You have the ability to accept or decline cookies. Most web browsers automatically accept cookies,

                        but you can usually modify your browser setting to decline cookies if you prefer. If you choose to

                        decline cookies, you may not be able to fully experience the interactive features of the Blissful

                        Website or Services you visit.</p>

                        <h4>Third Party Advertisers</h4>

                        <p></p>Advertisements, promotions and offers from third-party advertisers may be provided to you while

                        you are visiting the Blissful website. We may also use third-party advertising agencies to serve ads

                        on our behalf or the behalf of third-party advertisers (e.g., retailers of goods or services). These

                        companies may employ cookies and action tags (also known as single pixel gifs or web beacons) to

                        measure advertising effectiveness. Any information that these third parties collect via cookies and

                        action tags is completely anonymous. Blissful does not have access to or control of the cookies or

                        action tags that may be placed by the third-party advertisers. Third-party advertisers have no access

                        to your contact information stored on Blissful unless you choose to share it with them.</p>

                        <p>This Privacy Policy covers the use of cookies by Blissful and does not cover the use of cookies or

                        other tracking technologies by any of its advertisers.</p>

                        <h4>Third Party Content</h4>

                        <p>Blissful may contain links to other websites. We are not responsible for the content, operation or

                        privacy practices of these other websites. We encourage you to be aware that when you leave our

                        site, you should read the privacy statements of each and every website that collects personally

                        identifiable information. This Privacy Policy applies solely to information collected by Blissful.

                        Blissful is not responsible for the privacy practices of websites outside of the Blissful family of

                        websites.</p>

                        <h4>Data Security</h4>

                        <p>Blissful follows generally accepted industry standards to protect the personally identifiable

                        information submitted to us, both during transmission and once we receive it. We use industry

                        standard efforts to safeguard the confidentiality of your personal identifiable information, such as

                        firewalls and Secure Socket Layers (SSL) where appropriate. We also protect the personally

                        identifiable information you provide on computer servers in a controlled, secure environment,

                        protected from unauthorized access, use or disclosure.</p>

                        <p>But no method of transmission over the Internet, or method of electronic storage, is 100% secure,

                        however. Therefore, while we strive to use commercially acceptable means to protect your personal

                        information, we cannot guarantee its absolute security. If you have any questions about security on

                        our Website, you can send us an email at privacy-policy@blissful.co.ke</p>

                        <h4>Account</h4>

                        <p>Once you become a registered user of Blissful, you will receive an account, accessible by a password

                        of your choosing. You are responsible for maintaining the confidentiality of your password and

                        account. Furthermore, you are entirely responsible for any and all activities that occur under your

                        account. You agree to immediately notify Blissful of any unauthorized use of your account or any

                        other breach of security known to you.</p>

                        <h4>Legal Disclaimer</h4>

                        <p>Though we make every effort to preserve your privacy we may need to disclose personal

                        information when required by law wherein we have a good-faith belief that such action is necessary

                        to comply with a current judicial proceeding, a court order or legal process served on our Web site.

                        Changes to this Policy</p>

                        <p>Blissful will occasionally update this Privacy Policy to reflect company and customer feedback.

                        Blissful encourages you to periodically review this Policy to be informed of how Blissful is protecting

                        your information.</p>

                        <h4>Contact Information</h4>

                        <p>Blissful welcomes your comments regarding this Privacy Policy. If you believe that Blissful has not

                        adhered to this Policy, please contact Blissful at privacy-policy@blissful.co.ke. We will use

                        commercially reasonable efforts to promptly determine and remedy the problem.</p>

                        <p><strong>Last Updated: October 27, 2015</strong></p>
                  </div>
            </div>    
            </div>
          </div>
        </div>
    </div>
@endsection
