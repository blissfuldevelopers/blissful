<!-- Bootstrap -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

<!-- DataTable Bootstrap -->
<link href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css" rel="stylesheet">
<div class="container">
    <div class="container">

        <h1>Boost - {{ $boost->name }}
            <a href="{{ route('admin.boosts.show', [$boost->id]) }}/edit" class="btn btn-primary btn-xs"
               title="Edit Boost"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
            {!! Form::open([
                'method'=>'DELETE',
                'route' => ['admin.boosts.destroy', $boost->id],
                'style' => 'display:inline'
            ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Boost',
                    'onclick'=>'return confirm("Confirm delete?")'
            ))!!}
            {!! Form::close() !!}
        </h1>
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <tbody>

                <tr>
                    <th> Name</th>
                    <td> {{ $boost->name }} </td>
                </tr>
                <tr>
                    <th> Days</th>
                    <td> {{ $boost->days }} </td>
                </tr>
                <tr>
                    <th> Cost</th>
                    <td> {{ $boost->cost }} </td>
                </tr>
                <tr>
                    <th> Sale Price</th>
                    <td> {{ $boost->sale_price }} </td>
                </tr>

                <tr>
                    <th> Description</th>
                    <td> {{ $boost->product->description }} </td>
                </tr>

                <tr>
                    <th> Highlights</th>
                    <td> {{ $boost->product->highlights }} </td>
                </tr>

                <tr>
                    <th> Terms & Conditions</th>
                    <td> {{ $boost->product->tnc }} </td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
