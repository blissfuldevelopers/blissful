<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('days') ? 'has-error' : ''}}">
    {!! Form::label('days', 'Days', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::number('days', null, ['class' => 'form-control']) !!}
        {!! $errors->first('days', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('sale_price') ? 'has-error' : ''}}">
    {!! Form::label('sale_price', 'Sale Price', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::number('sale_price', null, ['class' => 'form-control']) !!}
        {!! $errors->first('sale_price', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('cost') ? 'has-error' : ''}}">
    {!! Form::label('cost', 'Cost', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::number('cost', null, ['class' => 'form-control']) !!}
        {!! $errors->first('cost', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        {!! $errors->first('cost', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('highlights') ? 'has-error' : ''}}">
    {!! Form::label('highlights', 'Highlights', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::textarea('highlights', null, ['class' => 'form-control']) !!}
        {!! $errors->first('cost', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('tnc') ? 'has-error' : ''}}">
    {!! Form::label('tnc', 'Terms & Conditions', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::textarea('tnc', null, ['class' => 'form-control']) !!}
        {!! $errors->first('cost', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@if(isset($categories))
    <div class="form-group">
        {!! Form::label('category', 'Category: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <select name="category_id" placeholder=category" class="form-control">
                <optgroup label="Category">
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
@endif

<div class="form-group">
    {!! Form::label('published', 'Published: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-3">
        Yes {!!  Form::radio('published', 1)  !!}
    </div>
    <div class="col-sm-3">
        No {!! Form::radio('published', 0)  !!}
    </div>
</div>

<div class="form-group {{ $errors->has('cover_image_path') ? 'has-error' : ''}}">
    {!! Form::label('cover_image_path', 'Cover Image Path: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        <input type="file" name="cover_image_path" class="form-control">
        {!! $errors->first('cover_image_path', '<p class="help-block">:message</p>') !!}
    </div>
</div>