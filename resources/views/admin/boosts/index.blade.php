<!-- Bootstrap -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

<!-- DataTable Bootstrap -->
<link href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css" rel="stylesheet">
<div class="container">

    <h1>Boosts
        <a href="{{ route('admin.boosts.create') }}" class="btn btn-primary btn-xs" title="Add New Boost">
            <span class="glyphicon glyphicon-plus" aria-hidden="true">
            </span>
        </a>
    </h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
                <th> Name</th>
                <th> Days</th>
                <th> Sale Price</th>
                <th> Cost</th>
                <th> Published</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach($boosts as $item)
                <tr>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->days }}</td>
                    <td>{{ $item->sale_price }}</td>
                    <td>{{ $item->cost }}</td>
                    <td>{{ ($item->published) ? 'Published' : 'Unpublished' }}</td>
                    <td>
                        <a href="{{ route('admin.boosts.show', [$item->id]) }}" class="btn btn-success btn-xs"
                           title="View Boost"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                        <a href="{{ route('admin.boosts.show', [$item->id]) }}/edit" class="btn btn-primary btn-xs"
                           title="Edit Boost"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'route' => ['admin.boosts.destroy', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Boost" />', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-xs',
                                'title' => 'Delete Boost',
                                'onclick'=>'return confirm("Confirm delete?")'
                        )) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination-wrapper"> {!! $boosts->render() !!} </div>
    </div>

</div>
<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<!-- Datatables -->
<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>

