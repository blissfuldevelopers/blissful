@if(!empty($event->user))
<a href ="/reviews/{{$event->subject_id}}">{{$event->user->first_name}}</a> registered as a <strong>Vendor</strong> for the following services @foreach($event->user->classifications as $classification )
   <strong>{{$classification->name}}</strong>,
@endforeach {{$event->created_at->diffForHumans()}}
@endif