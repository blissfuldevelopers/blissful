<div class="row">
    <div class="small-12 columns white_background admin-messages">
        <h2>Leader Board</h2>
        <form action="/admin/reports/rankings" method="GET">
            <div class="small-12 columns">
                <div class="small-6 columns">
                    <select name="type">
                        <option value="all">All</option>
                        <option value="top vendors">Top Vendors</option>
                        <option value="recommended vendors">Recommended vendors</option>
                    </select>
                </div>
                <div class="small-6 columns">
                    <button type="submit" class="tiny">Filter</button>
                </div>
            </div>
        </form>
        <div class="row">
            @if(@$data)
            <div class="table">
                <table id="users-table" class="table table-striped table-bordered small-12">
                    <thead>
                    <tr>
                        @foreach($data as $row_data)
                            @foreach($row_data as $column_title => $column_value)
                                <th>{{ ucwords(strtolower(str_replace('_', ' ', $column_title))) }}</th>
                            @endforeach
                            <?php break; ?>
                        @endforeach
                            <th>View transactions</th>
                            <th>View Profile</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $row_data)
                        <tr>
                            @foreach($row_data as $column_title => $column_value)
                                @if($column_title == 'period')
                                    <td>{{date("F Y", strtotime($column_value))}}</td>
                                @else
                                    <td>{{$column_value}}</td>
                                @endif
                            @endforeach
                                <td>
                                    <a class="button tiny alert" href="/admin/reports/credits/user/{{$row_data->user_id}}">View</a>
                                </td>
                                <td>
                                    <a class="button tiny alert" href="/users/{{$row_data->user_id}}">View Profile</a>
                                </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                @if(is_object($data))
                <div class="pagination-centered small-12 columns">
                      {!! $data->appends(Input::except('page'))->render(Foundation::paginate($data)) !!}
                </div>
                @endif
            </div>
            @else
                <div>
                    No Report data found
                </div>
            @endif
        </div>
    </div>
</div>
