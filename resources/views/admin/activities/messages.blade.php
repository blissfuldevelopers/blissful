
<link rel="stylesheet" type="text/css" href="/css/foundation-datepicker.css">

  <div class="row">
    <div class="small-12">
      <form action="/admin/messages" method="GET">
          <div class="row breaker-bottom breaker">
            <div class="large-3 hm columns">
              <h5>Total Messages</h5>
              <p class="green">{{$messages->total()}}</p>
            </div>
            <div class="large-2 small-12 columns breaker-bottom">
              <div class="small-4 medium-12 columns">
                <label>From</label>
              </div>
              <div class="small-8 medium-12 columns">
                <input name="from" type="text" class="datepicker" value="{{$from}}" />
              </div>  
            </div>
            <div class="large-2 small-12 columns breaker-bottom">
              <div class="small-4 medium-12 columns">
                <label>To</label>
              </div>
              <div class="small-8 medium-12 columns">
                <input name="to" type="text" class="datepicker" value="{{$to}}" />
              </div>
            </div>
            <div class="large-3 columns">
              <div class="row">
                <button type="submit" class="tiny">Filter</button>
              </div>
            </div>
          </div>
      </form>
    </div>
    <div class="row">
    @foreach(@$messages as $message)
      <div class="small-12 columns white_background admin-messages">
        <div class="small-12 medium-4 columns">
          <p><strong>From: </strong> <a href ="users/{{@$message->user->id}}">{{@$message->user->first_name}}</a></p>
          <p class="ns"><strong>To: </strong> <a href ="users/{{@$message->thread->participantsString($message->user_id, ['id'])}}">{!! @$message->thread->participantsString(@$message->user_id, ['first_name']) !!}</a></p>
        </div>
        <div class="small-12 medium-8 columns">
          <p> <strong>Message: </strong></p>
          <p>{!!@$message->body!!}</p>
          <p>{{@$message->created_at->format('l jS F Y h:i:s A')}}</p>
        </div>
      </div>
    @endforeach
    </div>
      <div class="pagination-centered small-12 columns">
                  {!! $messages->appends(Input::except('page'))->render(Foundation::paginate($messages)) !!}
      </div>
  </div>

<script type="text/javascript">
  function loadBackupScript(callback) {
      var script;
      if (typeof callback !== 'function') {
         throw new Error('Not a valid callback');  
      }
      script = document.createElement('script');
      script.onload = callback;  
      script.src = '/js/foundation-datepicker.min.js';
      document.head.appendChild(script);
  }

  loadBackupScript(function() { 
    console.log('call')
    $('.datepicker').fdatepicker({
        format: 'yyyy-mm-dd',
    }); 
  });
</script>