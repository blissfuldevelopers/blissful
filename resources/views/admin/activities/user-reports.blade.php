@extends('page.home-layout')
@section('title')
    Site Stats
@endsection

@section('header-content')
  <?php
    $header_class = 'vendorpg-intro';
  ?>
@endsection
@section('content')
    <!-- main content  -->
    	<div class="row">
	    	<div class="small-12 medium-10 columns small-centered show-jobs-section stats-section breaker">
	    		<h4>Site Stats</h4>
				@if($jobs->count() > 0)
				<h5 class="breaker">Quotation Requests</h5>
				<table class="responsive">
					<thead>
						<tr>
							<th>User</th>
							<th>Category</th>
							<th>Credits</th>
							<th>Date Posted</th>
						</tr>
					</thead>
					<tbody>
						@foreach($jobs as $job)
						<tr>
							<td>{{$job->user->first_name}} {{$job->user->last_name}}</td>
							<td>{{$job->category->name}}</td>
							<td>{{$job->credits}}</td>
							<td>{{$job->created_at}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<div class="row">
		            <div class="small-12 medium-9 small-centered">
		                {!! $jobs->render(Foundation::paginate($jobs)) !!}
		            </div>
		        </div>
				@endif
				@if($bids->count() > 0)
				<h5 class="breaker">Quotation Responses</h5>
				<table class="responsive">
					<thead>
						<tr>
							<th>User</th>
							<th>Category</th>
							<th>Credits</th>
							<th></th>
							<th>Date Posted</th>
						</tr>
					</thead>
					<tbody>
						@foreach($bids as $bid)
						@if($bid->job)
						<tr>
							<td>{{@$bid->job->user->first_name}} {{@$bid->job->user->last_name}}</td>
							<td>{{@$bid->job->category->name}}</td>
							<td>{{@$bid->job->credits}}</td>
							<td> <a href="/messages/{{@$bid->thread->id}}" class="button button-primary">View Bid</a></td>
							<td>{{@$bid->created_at}}</td>
						</tr>
						@endif
						@endforeach
					</tbody>
				</table>
				<div class="row">
		            <div class="small-12 medium-9 small-centered">
		                {!! $bids->render(Foundation::paginate($bids)) !!}
		            </div>
		        </div>
				@endif
				@if($credits_report->count() > 0)
				<h5 class="breaker">Credit Purchases</h5>
				<table class="responsive">
					<thead>
						<tr>
							<th>Credit Amount</th>
							<th>Quantity</th>
							<th>Date Purchased</th>
						</tr>
					</thead>
					<tbody>
						@foreach($credits_report as $credit)
						<tr>
							<td>{{@$credit->credits}} Credit Bundle</td>
							<td>{{@$credit->quantity}}</td>
							<td>{{@$credit->created_at}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<div class="row">
		            <div class="small-12 medium-9 small-centered">
		                {!! $credits_report->render(Foundation::paginate($credits_report)) !!}
		            </div>
		        </div>
				@endif
			</div>
		</div>
@endsection