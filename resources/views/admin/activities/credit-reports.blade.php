<link rel="stylesheet" href="/css/dataTables.foundation.min.css">
<div class="row">
    @if(@$data)
        
        <div class="table">
            <div class="row">
                <div class="small-6 small-centered columns">
                    <a href="/admin/reports/credits/under_hundred?csv=true" class="button tiny">Export to csv</a>
                </div>          
            </div>
            <table id="users-table" class="table table-striped table-bordered small-12">
                <thead>
                <tr>
                    @foreach($data as $row_data)
                        @foreach($row_data as $column_title => $column_value)
                            <th>{{ ucwords(strtolower(str_replace('_', ' ', $column_title))) }}</th>
                        @endforeach
                        <?php break; ?>
                    @endforeach
                        <th>View transactions</th>
                        <th>View Profile</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $row_data)
                    <tr>
                        @foreach($row_data as $column_value)
                            <td>{{$column_value}}</td>
                        @endforeach
                            <td>
                                <a class="btn btn-success btn-xs" href="/admin/reports/credits/user/{{$row_data->user_id}}">View</a>
                            </td>
                            <td>
                                <a class="btn btn-info btn-xs" href="/users/{{$row_data->user_id}}">View Profile</a>
                            </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            @if(is_object($data))
            <div class="pagination-centered small-12 columns">
                  {!! $data->appends(Input::except('page'))->render(Foundation::paginate($data)) !!}
            </div>
            @endif
        </div>
    @else
        <div>
            No Report data found
        </div>
    @endif
</div>
<script data-cfasync="false" type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
<script data-cfasync="false" type="text/javascript" src="/js/dataTables.foundation.min.js"></script>
<script data-cfasync="false" type="text/javascript">
    (function($) {
        $('#users-table').DataTable();
    })(jQuery);
</script>