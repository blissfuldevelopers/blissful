
<link rel="stylesheet" type="text/css" href="/css/foundation-datepicker.css">

  <div class="row">
    <div class="small-12">
      <form action="/honeymoon-subscription" method="GET">
          <div class="row breaker-bottom breaker">
            <div class="large-3 hm columns">
              <h5>Total Subscriptions</h5>
              <p class="green">{{$subscriptions->total()}}</p>
            </div>
            <div class="large-3 columns">
              <label>From
                <input name="from" type="text" class="datepicker" value="{{@$from}}" />
              </label>
            </div>
            <div class="large-3 columns">
              <label>To
                <input name="to" type="text" class="datepicker" value="{{@$to}}" />
              </label>
            </div>
            <div class="large-3 columns">
              <div class="row">
                <button type="submit" class="tiny">Filter</button>
              </div>
            </div>
          </div>
      </form>
    </div>
    <div class="row">
    @foreach(@$subscriptions as $subscription)
      <div class="small-12 columns white_background admin-messages">
        <div class="small-12 medium-4 columns">
          <p><strong>From: </strong> <a href ="#">{{@$subscription->user_name}}</a></p>
        </div>
        <div class="small-12 medium-8 columns">
          <p> <strong>Email: </strong>{{@$subscription->email}}</p>
          <p>{{@$subscription->created_at->diffForHumans()}}</p>
        </div>
      </div>
    @endforeach
    </div>
      <div class="pagination-centered small-12 columns">
                  {!! $subscriptions->appends(Input::except('page'))->render(Foundation::paginate($subscriptions)) !!}
      </div>
  </div>

<script data-cfasync="false" type="text/javascript" src="/js/foundation-datepicker.min.js"></script>
<script data-cfasync="false" type="text/javascript">
    $('.datepicker').fdatepicker({
     format: 'yyyy-mm-dd',
            });
</script>
<!--Start of Crisp Live Chat Script-->
    <script type="text/javascript">
            CRISP_WEBSITE_ID = "aeb94ef1-11bf-4618-85b7-dce7f0ee2b92";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.im/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();

    </script>

    <!--End of Crisp Live Chat Script-->