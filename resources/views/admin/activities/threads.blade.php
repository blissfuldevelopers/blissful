
<link rel="stylesheet" type="text/css" href="/css/foundation-datepicker.css">

  <div class="row">
    <div class="small-12">
      <form action="/admin/leads" method="GET">
          <div class="row breaker-bottom breaker">
            <div class="large-2 hm columns">
              <h5>Total Leads</h5>
              <p class="green">{{$threads->total()}}</p>
            </div>
            <div class="large-2 hm columns">
              <h5>Direct Contact</h5>
              <p class="green">{{$threads->total() - $bids->count()}}</p>
            </div>
            <div class="large-2 hm columns">
              <h5>Bids</h5>
              <p class="green">{{$bids->count()}}</p>
            </div>
            <div class="large-2 small-12 columns breaker-bottom">
              <div class="small-4 medium-12 columns">
                <label>From</label>
              </div>
              <div class="small-8 medium-12 columns">
                <input name="from" type="text" class="datepicker" value="{{$from}}" />
              </div>  
            </div>
            <div class="large-2 small-12 columns breaker-bottom">
              <div class="small-4 medium-12 columns">
                <label>To</label>
              </div>
              <div class="small-8 medium-12 columns">
                <input name="to" type="text" class="datepicker" value="{{$to}}" />
              </div>
            </div>
            <div class="large-2 columns">
              <div class="row">
                <button type="submit" class="tiny">Filter</button>
              </div>
            </div>
          </div>
      </form>
    </div>
    <div class="row">
    @foreach(@$threads as $thread)
      <div class="small-12 columns white_background admin-messages">
        <div class="small-12 medium-4 columns">
          <p><strong>From: </strong> <a href ="users/{{@$thread->creator()->id}}">{{@$thread->creator()->first_name}}</a></p>
          <p class="ns"><strong>To: </strong> 
            @foreach($thread->participants as $participant)
              @if($participant->user->id != $thread->creator()->id)
              <a href ="users/{{@$participant->user->id}}">{!!$participant->user->first_name !!}</a>
              @endif
            @endforeach
          </p>
        </div>
        <div class="small-12 medium-6 columns">
        	<p><strong>Subject </strong> {{@$thread->subject}}</p>
        	<p><strong>Number of messages </strong> {{@$thread->messages->count()}}</p>
          	<p> <strong>Latest Message: </strong> {{@$thread->updated_at->format('l jS F Y h:i:s A')}}</p>
          	<p><strong>Created: </strong> {{@$thread->created_at->format('l jS F Y h:i:s A')}}</p>
        </div>
        <div class="small-12 medium-2 columns hm">
        	<a href="{{ route('messages') }}/{{ $thread->id }}" class="button tiny"> View Messages</a>
        </div>
      </div>
    @endforeach
    </div>
      <div class="pagination-centered small-12 columns">
                  {!! $threads->appends(Input::except('page'))->render(Foundation::paginate($threads)) !!}
      </div>
  </div>
<script type="text/javascript">
  function loadBackupScript(callback) {
      var script;
      if (typeof callback !== 'function') {
         throw new Error('Not a valid callback');  
      }
      script = document.createElement('script');
      script.onload = callback;  
      script.src = '/js/foundation-datepicker.min.js';
      document.head.appendChild(script);
  }

  loadBackupScript(function() { 
    console.log('call')
    $('.datepicker').fdatepicker({
        format: 'yyyy-mm-dd',
    }); 
  });
</script>
