@foreach($activity as $event)
     <div class="small-12 columns white_background admin-messages">
        @include("admin.activities.types.{$event->name}")
    </div>
@endforeach
{!! $activity->render(Foundation::paginate($activity))!!}