<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Option {{ $option->id }}</div>
                <div class="panel-body">

                    <a href="{{ url('admin/options/' . $option->id . '/edit') }}" class="btn btn-primary btn-xs"
                       title="Edit Option"><span class="fi-pencil" aria-hidden="true"/></a>
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['admin/options', $option->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<span class="fi-trash" aria-hidden="true"/>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete Option',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $option->id }}</td>
                            </tr>
                            <tr>
                                <th> Name</th>
                                <td> {{ $option->name }} </td>
                            </tr>
                            <tr>
                                <th> Description</th>
                                <td> {{ $option->description }} </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>