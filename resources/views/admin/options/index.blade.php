<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Options</div>
                <div class="panel-body">

                    <a href="{{ url('/admin/options/create') }}" class="btn btn-primary btn-xs"
                       title="Add New Option"><span class="fi-plus" aria-hidden="true"/></a>
                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th> Name</th>
                                <th> Description</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($options as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->description }}</td>
                                    <td>
                                        <a href="{{ url('/admin/options/' . $item->id) }}"
                                           class="btn btn-success btn-xs" title="View Option"><span class="fi-eye"
                                                                                                    aria-hidden="true"/></a>
                                        <a href="{{ url('/admin/options/' . $item->id . '/edit') }}"
                                           class="btn btn-primary btn-xs" title="Edit Option"><span class="fi-pencil"
                                                                                                    aria-hidden="true"/></a>
                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/options', $item->id],
                                            'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<span class="fi-trash" aria-hidden="true" title="Delete Option" />', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs',
                                                'title' => 'Delete Option',
                                                'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $options->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>