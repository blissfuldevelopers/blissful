<link rel="stylesheet" href="/css/select2.min.css"/>
{!! HTML::style('/assets/css/parsley.css') !!}
<link rel="stylesheet" href="/css/foundation-datepicker.min.css">
<div class="row">
	<div class="small-12 medium-8 columns">
		<form action="{{route('user-ads.store')}}" method="POST" enctype="multipart/form-data" class="with-img">
			<div class="row">
				<div class="small-4 columns">
					<label for="type" class="left inline">Type</label>
				</div>
				<div class="small-8 columns">
					<select id="type" name="type" required >
						<option value="email">Email Ad</option>
						<option value="fcfg">Two</option>
						<option value="fcfg">Three</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="small-4 columns">
					<label for="user_name" class="left inline">User Name</label>
				</div>
				<div class="small-8 columns">
					<select id="user_name" name="user_name" class="select-user" required >
					</select>
				</div>
			</div>
			<div class="row">
				<div class="small-4 columns">
					<label for="link" class="left inline">Link</label>
				</div>
				<div class="small-8 columns">
					<input id="link" type="text" name="link" placeholder="http://abcd.ef" value="http://" required >
				</div>
			</div>
			<div class="row">
				<div class="small-4 columns">
					<label for="img_src" class="left inline">728 x 90 Banner</label>
				</div>
				<div class="small-8 columns">
					<input id="img_src" type="file" name="full_img_src" required >
				</div>
			</div>
			<div class="row">
				<div class="small-4 columns">
					<label for="img_src" class="left inline">320 x 100 Banner</label>
				</div>
				<div class="small-8 columns">
					<input id="img_src" type="file" name="mobile_img_src" required >
				</div>
			</div>
			<div class="row">
				<div class="small-4 columns">
					<label for="start_date" class="left inline">Start Date</label>
				</div>
				<div class="small-8 columns">
					<input type="text" id="start_date" name="start_date" class="datepicker" placeholder="0000-00-00" required >
				</div>
			</div>
			<div class="row">
				<div class="small-4 columns">
					<label for="stop_date" class="left inline">Stop Date</label>
				</div>
				<div class="small-8 columns">
					<input type="text" id="stop_date" name="stop_date" class="datepicker" placeholder="0000-00-00" required >
				</div>
			</div>
			<div class="row">
				<div class="small-8 columns right">
					<button class="small-12 button tiny" type="submit">Save</button>
				</div>
			</div>
		</form>
	</div>
</div>
<script src="/js/select2.full.min.js"></script>
{!! HTML::script('/assets/plugins/parsley.min.js') !!}
<script type="text/javascript" src="/js/file-validator.js"></script>
<script  type="text/javascript" src="/js/foundation-datepicker.min.js"></script>
<script type="text/javascript">

	$('.select-user').select2({
	   	placeholder:"Type in user name",
	   	ajax: {
	                dataType: 'json',
	                url: '{{ url("users/search") }}',
	                delay: 400,
	                data: function (params) {
	      return {
	        term: params.term, // search term
	        page: params.page
	      };
	    },
	    processResults: function (data, page) {
	      // parse the results into the format expected by Select2
	      // since we are using custom formatting functions we do not need to
	      // alter the remote JSON data, except to indicate that infinite
	      // scrolling can be used
	      page = page || 1;

		      return {
		        results: data
		      };
		    },
		    cache: true
		},
		escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
		minimumInputLength: 1,
		tags: true,
	  	tokenSeparators: [',']
	});
	$('.datepicker').fdatepicker({
        format: 'yyyy-mm-dd',
        disableDblClickSelection: true
    });

</script>