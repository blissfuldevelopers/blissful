
<div class="row">
@if($type == "line")
<?php echo Lava::render('LineChart', 'Votes', 'chartContainer'); ?>
@else
<?php echo Lava::render('BarChart', 'Votes', 'chartContainer'); ?>
@endif
<div id="chartContainer" style="height: 600px; width: 100%;"></div>
</div>
