<link href="/assets/pickadate/themes/default.css" rel="stylesheet">
<link href="/assets/pickadate/themes/default.date.css" rel="stylesheet">
<link href="/assets/pickadate/themes/default.time.css" rel="stylesheet">
<link rel="stylesheet" href="/css/select2.min.css"/>


<div class="container-fluid">
    <div class="row page-title-row">
        <div class="col-md-12">
            <h3>Posts
                <small>» Edit Post</small>
            </h3>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Post Edit Form</h3>
                </div>
                <div class="panel-body">

                    @include('admin.partials.errors')
                    @include('admin.partials.success')

                    <form class="form-horizontal" role="form" method="POST"
                          action="{{ route('admin.post.update', $id) }}" enctype="multipart/form-data">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="PUT">

                        @include('admin.post._form')

                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary btn-lg"
                                            name="action" value="continue">
                                        <i class="fa fa-floppy-o"></i>
                                        Save - Continue
                                    </button>
                                    <button type="submit" class="withimg btn btn-success btn-lg"
                                            name="action" value="finished">
                                        <i class="fa fa-floppy-o"></i>
                                        Save - Finished
                                    </button>
                                    <button type="button" class="btn btn-danger btn-lg"
                                            data-toggle="modal" data-target="#modal-delete">
                                        <i class="fa fa-times-circle"></i>
                                        Delete
                                    </button>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

    {{-- Confirm Delete --}}
    <div class="modal fade" id="modal-delete" tabIndex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×
                    </button>
                    <h4 class="modal-title">Please Confirm</h4>
                </div>
                <div class="modal-body">
                    <p class="lead">
                        <i class="fa fa-question-circle fa-lg"></i>
                        Are you sure you want to delete this post?
                    </p>
                </div>
                <div class="modal-footer">
                    <form method="POST" action="{{ route('admin.post.destroy', $id) }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">Close
                        </button>
                        <button type="submit" class=" btn btn-danger">
                            <i class="fa fa-times-circle"></i> Yes
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="/assets/tinymce/tinymce.min.js"></script>
<script src="/assets/pickadate/picker.js"></script>
<script src="/assets/pickadate/picker.date.js"></script>
<script src="/assets/pickadate/picker.time.js"></script>
<script src="/js/select2.full.min.js"></script>
<script>
    $(function () {
        $("#publish_date").pickadate({
            format: "mmm-d-yyyy"
        });
        $("#publish_time").pickatime({
            format: "h:i A"
        });
        $('#categories').select2({
            tags: true,
            placeholder: "Add Categories"
        });
    });
</script>
<script type="text/javascript">
    tinymce.init({
        selector: "#content",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools jbimages"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
        toolbar2: "print preview media | forecolor backcolor emoticons",
        relative_urls: false,
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ]
    });
</script>
<script type="text/javascript">

    _container = $('main');
        add_overlay = function () {
            _container.css("position", "relative");
            var overlay = $('<div id="overlay"></div>').css({
                    position: "absolute",
                    width: "100%",
                    height: "100%",
                    top: 0,
                    left: 0,
                    "background-color": "rgba(255,255,255,0.1)" /*dim the background*/
                }),
                modal = $('<div id="this-modal"><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>Loading...</div>').css({
                    position: "fixed",
                    width: "200px",
                    height: "200px",
                    top: "50%",
                    left: "50%",
                    "color": "#e9048b",
                    "margin-left": "-150px",
                    "margin-top": "-100px",
                    "text-align": "center",
                    "z-index": "10",
                });

            modal.appendTo(overlay.appendTo(_container));

        }
        remove_overlay = function () {
            $('#overlay').hide().remove();

        }
        $('.withimg').click(function(event){
            event.preventDefault();
            tinyMCE.triggerSave();
            add_overlay();
            var form = $(this).closest('form')[0];
            var formData = new FormData(form);
            $.ajax({    
                        type:'POST',
                        url: $(this).closest('form').attr('action'),
                        data: formData,
                        // THIS MUST BE DONE FOR FILE UPLOADING
                        contentType: false,
                        processData: false,
                        // ... Other options like success and etc
                        success : function(data) {
                         remove_overlay();
                         $('.success').show(); 
                         $('#overlay').css({ display: "none" });
                         $( "#blog-container-data" ).html( data );
                          
                        },
                        error:  function(data){
                            $('.alert').show(); 
                            remove_overlay();
                        }
                    });    

        });
    </script>