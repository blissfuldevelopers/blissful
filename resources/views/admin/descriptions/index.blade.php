<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Descriptions</div>
                <div class="panel-body">

                    <a href="{{ url('user/options/' . $user->id . '/create') }}" class="btn btn-primary btn-xs"
                       title="Add New Description"><span class="fi-plus" aria-hidden="true"/></a>
                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>Classification</th>
                                <th>Description Type<span class="warning label round radius"><i class="fi-wrench"></i> <a href="/admin/options">Add Options</a></span></th>
                                <th>Description Detail</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($descriptions as $item)
                                <tr>
                                    <td>{{ $item->classification->name }}</td>
                                    <td>{{ $item->option->name }}</td>
                                    <td>{{ $item->description_detail }}</td>
                                    <td>
                                        <a href="{{ url('user/options/' . $item->id . '/edit') }}"
                                           class="btn btn-primary btn-xs" title="Edit Description"><span
                                                    class="fi-pencil" aria-hidden="true"/></a>
                                        <a href="{{ url('user/options/' . $item->id . '/delete') }}"
                                           class="btn btn-danger btn-xs" title="Edit Description"><span
                                                    class="fi-trash" aria-hidden="true"/></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $descriptions->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>