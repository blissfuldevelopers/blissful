<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create New Description</div>
                <div class="panel-body">

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::open(['url' => '/user/options/' . $user->id . '/create', 'class' => 'form-horizontal', 'files' => true]) !!}

                        <div class="form-group">
                            {!! Form::label('classification_id', 'Category:') !!}
                            <select id="classification_id" name="classification_id" placeholder="classification" class="form-control">
                                <optgroup label="Category">
                                    @foreach($user->classifications as $classification)
                                        <option value="{{$classification->id}}">{{$classification->name}}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>

                        <div class="form-group">
                            {!! Form::label('option_id', 'Option:') !!}
                            <span class="warning label round radius"><i class="fi-wrench"></i> <a href="/admin/options">Add options</a></span>
                            <select id="option_id" name="option_id" placeholder="option" class="form-control">
                                <optgroup label="Category">
                                    @foreach($options as $option)
                                        <option value="{{$option->id}}">{{$option->name}}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                        <div class="form-group {{ $errors->has('description_detail') ? 'has-error' : ''}}">
                            {!! Form::label('description_detail', 'Description Detail', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('description_detail', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('description_detail', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
                            </div>
                        </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>