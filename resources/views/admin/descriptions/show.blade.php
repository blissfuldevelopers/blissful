    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Description {{ $description->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('admin/descriptions/' . $description->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Description"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/descriptions', $description->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Description',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $description->id }}</td>
                                    </tr>
                                    <tr><th> User Id </th><td> {{ $description->user_id }} </td></tr><tr><th> Classification Id </th><td> {{ $description->classification_id }} </td></tr><tr><th> Description Type </th><td> {{ $description->description_type }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>