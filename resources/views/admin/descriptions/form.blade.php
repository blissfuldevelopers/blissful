<div class="form-group">
    {!! Form::label('classification_id', 'Category:') !!}
    <select id="classification_id" name="classification_id" placeholder="classification" class="form-control">
        <optgroup label="Category">
            @foreach($user->classifications as $classification)
                    <option value="{{$classification->id}}">{{$classification->name}}</option>
            @endforeach
        </optgroup>
    </select>
</div>

<div class="form-group">
    {!! Form::label('option_id', 'Option:') !!}
    <select id="option_id" name="option_id" placeholder="option" class="form-control">
        <optgroup label="Category">
            @foreach($options as $option)
                <option value="{{$option->id}}">{{$option->name}}</option>
            @endforeach
        </optgroup>
    </select>
</div>
<div class="form-group {{ $errors->has('description_detail') ? 'has-error' : ''}}">
    {!! Form::label('description_detail', 'Description Detail', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('description_detail', null, ['class' => 'form-control']) !!}
        {!! $errors->first('description_detail', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>