<div class="container">
     <h1>Create New Creditbundle</h1>
    <hr/>

    {!! Form::open(['route' => 'admin.bundles.store', 'class' => 'form-horizontal','files' => true, 'method' => 'post',]) !!}

    @include('admin.creditbundles.create-edit-form-items')
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
</div>

