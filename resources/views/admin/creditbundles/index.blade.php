<h1>Credit Bundles <a href="{{ route('admin.bundles.create') }}" class="button tiny warning right">Add New
        Creditbundles</a></h1>
<div class="table">

    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th>S.No</th>
            <th>Name</th>
            <th>Description</th>
            <th>Credits</th>
            <th>Unit Price</th>
            <th>Cost</th>
            <th>Expiration Time</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        {{-- */$x=0;/* --}}
        @foreach($creditbundles as $item)
            {{-- */$x++;/* --}}

            <tr>
                <td>{{ $x }}</td>
                <td><a href="{{ url('admin/bundles', $item->id) }}">{{ @$item->name }}</a></td>
                <td>{{ @$item->description }}</td>
                <td>{{ $item->credits }}</td>
                <td>{{ $item->unit_price }}</td>
                <td>{{ @$item->cost }}</td>
                <td>{{ $item->expiration_time }}</td>
                <td>
                    <a href="{{ route('admin.bundles.edit', $item->id) }}">
                        <i class="fa fa-pencil-square-o"></i>
                    </a>
                    {!! Form::open([
                        'method'=>'DELETE',
                        'route' => ['admin.bundles.destroy', $item->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="pagination"> {!! $creditbundles->render() !!} </div>
</div>

