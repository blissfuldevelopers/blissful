<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Credit Bundle Name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required','rows'=>'5']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('credits') ? 'has-error' : ''}}">
    {!! Form::label('credits', 'Credits: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('credits', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('credits', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('unit_price') ? 'has-error' : ''}}">
    {!! Form::label('unit_price', 'Unit Price: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('unit_price', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('unit_price', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('cost') ? 'has-error' : ''}}">
    {!! Form::label('cost', 'Bundle Cost: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('cost', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('cost', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('expiration_time') ? 'has-error' : ''}}">
    {!! Form::label('expiration_time', 'Expiration Time: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('expiration_time', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('expiration_time', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('cover_image_path') ? 'has-error' : ''}}">
    {!! Form::label('cover_image_path', 'Cover Image Path: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        <input type="file" name="cover_image_path" class="form-control">
        {!! $errors->first('cover_image_path', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-sm-offset-3 col-sm-3">
        {!! Form::submit('Save', ['class' => 'btn btn-primary form-control submit-btn']) !!}
    </div>
</div>
<script type="text/javascript">

     _container = $('main');
        add_overlay = function () {
            _container.css("position", "relative");
            var overlay = $('<div id="overlay"></div>').css({
                    position: "absolute",
                    width: "100%",
                    height: "100%",
                    top: 0,
                    left: 0,
                    "background-color": "rgba(255,255,255,0.1)" /*dim the background*/
                }),
                modal = $('<div id="this-modal"><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>Loading...</div>').css({
                    position: "fixed",
                    width: "200px",
                    height: "200px",
                    top: "50%",
                    left: "50%",
                    "color": "#e9048b",
                    "margin-left": "-150px",
                    "margin-top": "-100px",
                    "text-align": "center",
                    "z-index": "10",
                });

            modal.appendTo(overlay.appendTo(_container));

        }
        remove_overlay = function () {
            $('#overlay').hide().remove();

        }

    $('.submit-btn').click(function (event) {
            event.preventDefault();
            console.log('hit');
            add_overlay();
            var form = $(this).closest('form')[0];
            var formData = new FormData(form);
            $.ajax({    
                        type:'POST',
                        url: $(this).closest('form').attr('action'),
                        data: formData,
                        // THIS MUST BE DONE FOR FILE UPLOADING
                        contentType: false,
                        processData: false,
                        // ... Other options like success and etc
                        success : function(data) {
                         remove_overlay();
                         $('.container').html(data);
                          
                        },
                        error:  function(data){
                            remove_overlay();
                            alert('error');
                        }
                    }); 
    });

</script>
