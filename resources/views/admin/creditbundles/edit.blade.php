<div class="container">
<h1>Edit Creditbundle</h1>
<hr/>

{!! Form::model($creditbundle, [
    'method' => 'PATCH',
    'route' => ['admin.bundles.update', $creditbundle->id],
    'class' => 'form-horizontal',
    'enctype'=>'multipart/form-data',
    'files' => true,
]) !!}

@include('admin.creditbundles.create-edit-form-items')
{!! Form::close() !!}

@if ($errors->any())
    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif

</div>

