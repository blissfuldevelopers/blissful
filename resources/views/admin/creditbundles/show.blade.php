<h1>Creditbundle</h1>
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th>ID.</th>
            <th>Name</th>
            <th>Description</th>
            <th>Credits</th>
            <th>Cost</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{ $creditbundle->id }}</td>
            <td> {{ $creditbundle->name }} </td>
            <td> {{ $creditbundle->description }} </td>
            <td> {{ $creditbundle->credits }} </td>
            <td> {{ $creditbundle->cost }} </td>
        </tr>
        </tbody>
    </table>
</div>

