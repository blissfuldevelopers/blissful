<!-- START OF CREDITS HEADER -->
<section class="credits-header">
  <div class="row">
    <div class="small-12 columns">
      <span class="credits-icon"></span>
      <h2>About Blissful Credits</h2>
      <h6>Use credits to send quotes and get hired on Blissful</h6>
      <p>Blissful is a pay-per-quote system where you buy credits to send quotes to customers that are a good fit for your business. You only pay to quote, that’s it: we do not charge commission on jobs you complete or future jobs with the same customer and their referrals.</p>
    </div>
  </div>
</section>
<!-- END OF CREDITS HEADER -->
<!-- START OF CREDITS SELECT -->
<section class="credits-list">
  <div class="row">
    <div class="small-12 columns">
      <ul class="credits">
        <li>
            <div class="text-wrap">
              <h3>STARTER<br>PACK</h3>
              <hr>
              <h5>450 Credits</h5>
              <span>6-8 Jobs</span>
              <hr>
              <h6>Kshs 799.00</h6>
              <span>Kshs 1.78 per credit</span>
              <a class="button" onclick="window.location='/shop/product/460-credits';">BUY NOW</a>
            </div>
            <span class="backgrounds wrap">
              <span class="lcard"></span>
              <span class="rcard"></span>
              <span class="mcard"></span>
            </span>
        </li>
        <li>
            <div class="text-wrap">
              <h3>MOST<br>POPULAR</h3>
              <hr>
              <h5>900 Credits</h5>
              <span>11-15 Jobs</span>
              <hr>
              <h6>Kshs 1499.00</h6>
              <span>Kshs 1.67 per credit</span>
              <a class="button" onclick="window.location='/shop/product/900-credits';">BUY NOW</a>
            </div>
            <span class="backgrounds wrap">
              <span class="lcard"></span>
              <span class="rcard"></span>
              <span class="mcard"></span>
            </span>
        </li>
        <li>
            <div class="text-wrap">
              <h3>BEST<br>PRICE</h3>
              <hr>
              <h5>1500 Credits</h5>
              <span>18-25 Jobs</span>
              <hr>
              <h6>Kshs 2,400.00</h6>
              <span>Kshs 1.60 per credit</span>
              <a class="button" onclick="window.location='/shop/product/1500-credits';">BUY NOW</a>
            </div>
            <span class="backgrounds wrap">
              <span class="lcard"></span>
              <span class="rcard"></span>
              <span class="mcard"></span>
            </span>
        </li>
      </ul>
    </div>
  </div>
</section>
<!-- END OF CREDITS SELECT -->
<!-- START OF CREDITS FAQS -->
<section class="faqs">
  <div class="row">
    <div class="small-12 columns">
      <h3>FAQs</h3>
      <div class="credit-columns">
        <ul>
          <li>
            <h5>Do I have to use my credits on every request?</h5>
            <p>You must use credits to send a quote. However, you always have the option to read and pass on a customer request if it is not right for you.</p>
          </li>
          <li>
            <h5>How many credits do I need to apply for one job?</h5>
            <p>Each job is weighted and assigned a number of credits needed before a quote is sent. The number of credits needed is displayed along side each job.</p>
          </li>
          <li>
            <h5>Do I get charged for every message I send?</h5>
            <p>No we only deduct credits once, when you respond to a Job. The other messages that result from this one job are not billed.</p>
          </li>
          <li>
            <h5>What happens when I run out of credits?</h5>
            <p>You can buy more credits before you write your quote. You will alerted as soon as your credit balance drops below 100.</p>
          </li>
          <li>
            <h5>Can my Blissful credits expire?</h5>
            <p>The Blissful Credits that you buy do not expire. You can use them whenever you want, on the job you're interested in. However, the free credits given you new business on Blissful expire in 45 days.</p>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<!-- END OF CREDITS FAQS -->