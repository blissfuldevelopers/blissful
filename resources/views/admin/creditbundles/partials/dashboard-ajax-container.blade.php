{{--{!! HTML::style('/assets/css/dashboard-creditbundles.css') !!}--}}
<div id="creditbundles-container">
    <div class="grey small-12 row">
    
        <div id="creditbundles-container-back-action">
            <ul class="sub-nav">
                <li class="small-3 columns">
                    <a href="#back" class="small-12">
                        <i class="fi-arrow-left medium"></i>
                        <span class="text">Back</span>
                    </a>
                </li>

                <li class="small-6 columns">
                    <span class="small-12 grey">
                        <!-- <a href="#home" class="small-12">
                            <span class="text">CreditBundles Home</span>
                        </a> -->
                    </span>
                </li>

                <li class="small-3 columns">
                    <a href="#reload" class="">
                        <i class="fi-refresh medium right"></i>
                    </a>
                    <span id="curr-page-name"
                          data-url=""
                    ></span>
                </li>
            </ul>
        </div>


    </div>
    <div class="small-12" id="creditbundles-container-message"></div>
    <div class="small-12" id="creditbundles-container-data"
         data-url="{{ @$default_creditbundles_route }}"
         data-requested-url="{{ @$requested_creditbundles_route }}">

    </div>
</div>