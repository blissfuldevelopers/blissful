<div class="container">
    <h1>Credit Vendor</h1>
    <hr/>

    {!! Form::open(['route' => 'vendor.credit-vendor', 'class' => 'form-horizontal','files' => true, 'method' => 'post',]) !!}

    <div class="form-group {{ $errors->has('credit') ? 'has-error' : ''}}">
        {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('name', $user->first_name, ['class' => 'form-control', 'required' => 'required','disabled' => 'disabled']) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('credit') ? 'has-error' : ''}}">
        {!! Form::label('type', 'Type: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('object_name', 'credits', ['class' => 'form-control', 'required' => 'required','disabled' => 'disabled']) !!}
            {!! $errors->first('object_name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('credit') ? 'has-error' : ''}}">
        {!! Form::label('credit', 'Credits: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('credit', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('credit', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    {!! Form::hidden('user_id', $user->id) !!}
    <button type="submit" class="btn btn-primary"><span class="fi-dollar"></span> Credit User</button>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
</div>

