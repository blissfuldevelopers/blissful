<ul class="credit-packages">
    {{-- */$x=0;/* --}}
    @foreach($creditbundles as $item)
        {{-- */$x++;/* --}}
        <li>
            <a href="#">
                <span class="package"></span>
                <span class="credit">{{ @$item->name }}</span>
                <span class="text">{{ @$item->description }}</span>
                <span class="cost">{{ @$item->cost }}</span>
            <span class="buy"><a
                                 @if($item->id == 1)
                                 onclick="window.location='/shop/dealview/1';"
                                 @elseif($item->id == 2)
                                 onclick="window.location='/shop/dealview/2';"
                                 @elseif($item->id == 3)
                                 onclick="window.location='/shop/dealview/3';"
                                 @endif
                                 class="button">BUY NOW</a></span>
            </a>
        </li>
    @endforeach
</ul>