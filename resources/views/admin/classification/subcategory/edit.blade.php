<h1>Edit Category</h1>
<hr/>
<div class="small-12">
    @include('flash::message')
</div>
{!! Form::model($classification, [
    'method' => 'POST',
    'route' => ['parent.update', $classification->id],
    'class' => 'form-horizontal'
]) !!}
<div class="form-group">
    {!! Form::label('classification', 'Classification: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6" placeholder=classification" class="form-control">
        <select name="root_parent_id" placeholder=classification" class="form-control">
            <optgroup label="classification">
                @foreach($categories as $category)
                    @if($classification->parent->name == $category->name)
                        <option selected="selected"
                                value="{{$classification->parent->id}}">{{$classification->parent->name}}</option>
                    @else
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endif
                @endforeach
            </optgroup>
        </select>
    </div>
</div>
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-sm-offset-3 col-sm-3">
        {!! Form::submit('Update', ['class' => 'btn btn-primary btn-xs']) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-sm-offset-3 col-sm-3">
        {!! Form::close() !!}
        {!! Form::open([
                                    'method'=>'DELETE',
                                    'route' => ['classification.destroy', $classification->id],
                                    'style' => 'display:inline'
                                ]) !!}

        {!! Form::submit('DELETE', ['class' => 'btn btn-primary btn-xs']) !!}
    </div>
</div>
{!! Form::close() !!}
@if ($errors->any())
    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif