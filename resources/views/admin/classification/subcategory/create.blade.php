<h1>Create New Subcategory</h1>
<hr/>
<div class="small-12">
    @include('flash::message')
</div>
{!! Form::open(['route' => 'subcategory.store', 'class' => 'form-horizontal','method' => 'post','files' => true]) !!}
<div class="form-group">
    {!! Form::label('classification', 'Classification: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        <select name="parent_id" placeholder=classification" class="form-control">
            <optgroup label="Category">
                @foreach($classifications as $classification)
                    <option value="{{$classification->id}}">{{$classification->name}}</option>
                @endforeach
            </optgroup>
        </select>
    </div>
</div>
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-3 col-sm-3">
        {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
{!! Form::close() !!}

@if ($errors->any())
    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif