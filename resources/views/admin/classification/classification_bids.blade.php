<div class="row"> 
	<form action="{{route('classification.bids')}}" method="GET">
		<div class="row">
			<div class="small-8 columns">
				<select name="classification">
					<option value="all">All</option>
					@foreach($classifications as $classification)
						@if(@$selected && $selected->slug == $classification->slug)
						<option selected value="{{$classification->slug}}"> {{$classification->name}}</option>
						@else
						<option value="{{$classification->slug}}"> {{$classification->name}}</option>
						@endif
					@endforeach
				</select>
			</div>
			<div class="small-4 columns">
				<input type="submit" value="Filter" class="button tiny">	
			</div>
		</div>
	</form>
	@if(@$total_jobs)
	<div class="small-6 medium-5 columns small-centered">
		<div class="small-6 columns">
			<p>Total Jobs: {{$total_jobs->jobs}}</p>
		</div>
		<div class="small-6 columns">
			<p>Total bids: {{$total_jobs->bids}}</p>
		</div>
		
	</div>
	@endif
	@if(@$users)
	<div class="small-12 medium-10 columns small-centered">
		<table>
		  <thead>
		    <tr>
		      <th width="200">Name</th>
		      <th width="150">Bids</th>
		      <th width="150">Percentage</th>
		    </tr>
		  </thead>
		  <tbody>
		  	@foreach($users as $user)
		    <tr>
		      <td>{{$user->first_name}}</td>
		      <td>{{$user->bids}}</td>
		      @if($total_jobs->bids > 0)
		      <td>{{round($user->bids/$total_jobs->bids*100,2)}} %</td>
		      @endif
		    </tr>
		    @endforeach
		  </tbody>
		</table>
	</div>
	<div class="row">
      	<div class="pagination-centered small-12 medium-8">
          	{!! $users->render(Foundation::paginate($users)) !!}
      	</div>
	 </div>
	@else
	<div class="small-12 medium-10 columns small-centered">
		<p>Nothing to display, Please select a value</p>
	</div>
	@endif
</div>