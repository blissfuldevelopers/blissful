<div class="main_content">
    <h1>Edit Classification</h1>
    <hr/>

    {!! Form::model($classification, [
        'method' => 'PATCH',
        'route' => ['classification.update', $classification->id],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
        {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
        {!! Form::label('description', 'Description: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('border_color') ? 'has-error' : ''}}">
        {!! Form::label('border_color', 'Border Color: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::input('color', 'border_color',null) !!}
            {!! $errors->first('border_color', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('thumbnail_path') ? 'has-error' : ''}}">
        {!! Form::label('thumbnail_path', 'Thumbnail Path: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <input type="file" name="thumbnail_path" class="form-control">
            {!! $errors->first('thumbnail_path', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('cover_image_path') ? 'has-error' : ''}}">
        {!! Form::label('cover_image_path', 'Cover Image Path: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <input type="file" name="cover_image_path" class="form-control">
            {!! $errors->first('cover_image_path', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('meta_title') ? 'has-error' : ''}}">
        {!! Form::label('meta_title', 'Meta Title: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('meta_title', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('meta_title', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : ''}}">
        {!! Form::label('meta_description', 'Meta Description: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::textarea('meta_description', null, ['class' => 'form-control']) !!}
            {!! $errors->first('meta_description', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('primary_keyword') ? 'has-error' : ''}}">
        {!! Form::label('primary_keyword', 'Primary Keyword: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('primary_keyword', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('primary_keyword', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('page_content') ? 'has-error' : ''}}">
        {!! Form::label('page_content', 'Page Content: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::textarea('page_content', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('page_content', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('priority') ? 'has-error' : ''}}">
        {!! Form::label('priority', 'Priority: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('priority', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('priority', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('bid_credits') ? 'has-error' : ''}}">
        {!! Form::label('bid_credits', 'Bid Credits: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('bid_credits', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('bid_credits', '<p class="help-block">:message</p>') !!}
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary ']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
</div>