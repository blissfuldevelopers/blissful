<div class="main_content">
    <h1>Create New Classification</h1>
    <hr/>
    <div style="display:none;" data-alert class="alert-box alert-box-messages success">
        Classification Successfully created
        <a href="#" class="close">&times;</a>
    </div>
    <div style="display:none;" data-alert class="alert-box alert-box-messages alert">
        There was an error during creation.
        <a href="#" class="close">&times;</a>
    </div>
    {!! Form::open(['route' => 'classification.store', 'class' => 'class-create','enctype'=>'multipart/form-data']) !!}

    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
        {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
        {!! Form::label('description', 'Description: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('border_color') ? 'has-error' : ''}}">
        {!! Form::label('border_color', 'Border Color: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <input type="color" name="border_color" class="form-control">
            {!! $errors->first('border_color', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('thumbnail_path') ? 'has-error' : ''}}">
        {!! Form::label('thumbnail_path', 'Thumbnail: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <input type="file" name="thumbnail_path" class="form-control">
            {!! $errors->first('thumbnail_path', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('cover_image_path') ? 'has-error' : ''}}">
        {!! Form::label('cover_image_path', 'Cover Image Path: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <input type="file" name="cover_image_path" class="form-control">
            {!! $errors->first('cover_image_path', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('meta_title') ? 'has-error' : ''}}">
        {!! Form::label('meta_title', 'Meta Title: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('meta_title', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('meta_title', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : ''}}">
        {!! Form::label('meta_description', 'Meta Description: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::textarea('meta_description', null, ['class' => 'form-control']) !!}
            {!! $errors->first('meta_description', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('primary_keyword') ? 'has-error' : ''}}">
        {!! Form::label('primary_keyword', 'Primary Keyword: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('primary_keyword', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('primary_keyword', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('page_content') ? 'has-error' : ''}}">
        {!! Form::label('page_content', 'Page Content: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::textarea('page_content', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('page_content', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('priority') ? 'has-error' : ''}}">
        {!! Form::label('priority', 'Priority: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('priority', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('priority', '<p class="help-block">:message</p>') !!}
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
</div>
<script type="text/javascript">
    _container = $('main');
    add_overlay = function () {
        _container.css("position", "relative");
        var overlay = $('<div id="overlay"></div>').css({
                    position: "absolute",
                    width: "100%",
                    height: "100%",
                    top: 0,
                    left: 0,
                    "background-color": "rgba(255,255,255,0.1)" /*dim the background*/
                }),
                modal = $('<div id="this-modal"><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>Loading...</div>').css({
                    position: "fixed",
                    width: "200px",
                    height: "200px",
                    top: "50%",
                    left: "50%",
                    "color": "#e9048b",
                    "margin-left": "-150px",
                    "margin-top": "-100px",
                    "text-align": "center",
                    "z-index": "10",
                });

        modal.appendTo(overlay.appendTo(_container));

    }
    remove_overlay = function () {
        $('#overlay').hide().remove();

    }
    $('form.class-create').submit(function (event) {
        event.preventDefault();
        console.log('here')
            add_overlay();
            var form = $(this).closest('form')[0];
            var formData = new FormData(form);
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: formData,
                // THIS MUST BE DONE FOR FILE UPLOADING
                contentType: false,
                processData: false,
                // ... Other options like success and etc
                success: function (data) {
                    remove_overlay();
                    $('.success').show();
                    $('#overlay').css({display: "none"});

                },
                error: function (data) {
                    $('.alert').show();
                    $('#overlay').css({display: "none"});
                    remove_overlay();
                }
            });

    });
</script>