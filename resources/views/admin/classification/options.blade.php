<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {
        $( "#sortable" ).sortable();
        $( "#sortable" ).disableSelection();
    } );
</script>
@foreach($classifications as $classification)
    <ul id="sortable">
        <li class="ui-state-default">
            {{$classification->name}}
            @foreach( $classification->descriptions as $description)
                <p>{{@$description->description_type}}: {{@$description->description_detail}}</p>
            @endforeach
            @foreach( $classification->children as $children)
                <ul id="sortable">
                    <li class="ui-state-default">
                        <strong>{{$children->name}}</strong>
                    </li>
                    @foreach( $children->children as $child)
                        <ul id="sortable">
                            <li class="ui-state-default">
                                {{$child->name}}
                                @foreach( $child->descriptions as $description)
                                    <p>{{@$description->description_type}}: {{@$description->description_detail}}</p>
                                @endforeach
                            </li>
                        </ul>
                    @endforeach
                </ul>
            @endforeach
        </li>
    </ul>
@endforeach