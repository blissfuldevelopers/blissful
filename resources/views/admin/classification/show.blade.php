<div class="main_content">
    <h1>Classifications <a href="{{ route('subcategory.create') }}" class="btn btn-primary pull-right btn-sm">Add New </a></h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
                <th>ID.</th>
                <th>Name</th>
                <th>Catgeories</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{ $classification->id }}</td>
                <td> {{ $classification->name }} </td>
                <td>
                    @foreach( $classification->children as $children)
                        <ul>
                            <li>
                                <a href="{{ url('/classification/category', $children->id) }}"><strong>{{$children->name}}</strong></a>
                            </li>
                            @foreach( $children->children as $child)
                                <ul>
                                    <li>
                                        <a href="{{ url('/classification/subcategory', $child->id) }}">{{$child->name}}</a>
                                    </li>
                                </ul>
                            @endforeach
                        </ul>
                    @endforeach
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>