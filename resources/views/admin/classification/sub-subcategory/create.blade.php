<h1>Create New Sub Sub-category</h1>
<hr/>
<div class="small-12">
    @include('flash::message')
</div>
{!! Form::open(['route' => 'sub-subcategory.store', 'class' => 'form-horizontal','method' => 'post','files' => true]) !!}

<div class="form-group">
    {!! Form::label('classification', 'Category: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        <select name="root_parent_id" id="root_parent_id" placeholder=classification" class="form-control">
            <optgroup label="Classification">
                @foreach($classifications as $classification)
                    <option value="{{$classification->id}}">{{$classification->name}}</option>
                @endforeach
            </optgroup>
        </select>
    </div>
</div>
<div class="form-group">
    {!! Form::label('child', 'Sub-Category: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        <select name="parent_id" id="parent_id" placeholder="child" class="form-control">
            <optgroup label="child">
                @foreach($classifications as $classification)
                    @foreach($classification->children as $child)
                        <option value="{{$child->id}}">{{$child->name}}</option>
                    @endforeach
                @endforeach
            </optgroup>
        </select>
    </div>
</div>
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-3 col-sm-3">
        {!! Form::submit('Create', ['class' => 'submit-btn btn btn-primary']) !!}
    </div>
</div>
{!! Form::close() !!}

@if ($errors->any())
    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif
<script src="/js/jquery-2.2.1.min.js"></script>
<script type="text/javascript">
    $('#root_parent_id').on('change', function (e) {

        var cat_id = e.target.value;

        //ajax

        $.get('/api/classification-dropdown?cat_id=' + cat_id, function (data) {

            //success data
            $('#parent_id').empty();

            $('#parent_id').append(' Please choose one');

            $.each(data, function (index, subcatObj) {

                $('#parent_id').append('<option value ="' + subcatObj.id + '">' + subcatObj.name + '</option>');

            });

        });

    });
</script>