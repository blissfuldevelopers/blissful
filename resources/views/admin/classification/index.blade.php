<div class="main_content">
    <h1>Classifications <a href="{{ route('classification.create') }}" class="btn btn-primary pull-right btn-sm">Add New
            Classification</a></h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
                <th>S.No</th>
                <th>Name</th>
                <th>Description</th>
                <th>Border Color</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($classifications as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $x }}</td>
                    <td><a href="{{ url('/classification', $item->id) }}">{{ $item->name }}</a></td>
                    <td>{{ $item->description }}</td>
                    <td>{{ $item->border_color }}</td>
                    <td>
                        <a href="{{ route('classification.edit', $item->id) }}">
                            <button type="submit" class="btn btn-primary btn-xs">Update</button>
                        </a> /
                        {!! Form::open([
                            'method'=>'DELETE',
                            'route' => ['classification.destroy', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination"> {!! $classifications->render() !!} </div>
    </div>
</div>
