<div class="row">
    <div class="small-12 columns white_background admin-messages">
        <p><strong>Rankings:</strong><a href="{{ url('/admin/reports/rankings')}}" > Vendor rankings</a></p>
    </div>
    <div class="small-12 columns white_background admin-messages">
        <p><strong>Honeymoon:</strong><a href="{{ url('/honeymoon-subscription')}}" > Subscription</a></p>
    </div>
    <div class="small-12 columns white_background admin-messages">
        <p><strong>Add:</strong><a href="{{ url('/admin/classifications/subcategory')}}" > Vendor Subcategory</a></p>
        <p><strong>Add:</strong><a href="{{ url('/admin/classifications/sub-subcategory')}}" > Vendor sub-subcategory</a></p>
    </div>
    <div class="small-12 columns white_background admin-messages">
        <p><strong>Locations:</strong><a href="{{ url('/locations')}}"> User locations</a></p>
    </div>
    <div class="small-12 columns white_background admin-messages">
        <p><strong>Transaction:</strong><a href="{{ url('/admin/reports/credits/')}}"> Full Vendor Transactions</a></p>
    </div>
    <div class="small-12 columns white_background admin-messages">
        <p><strong>Credit purchases:</strong><a href="{{ url('/admin/reports/credits/purchased')}}"> Vendor purchased credits</a></p>
    </div>
    <div class="small-12 columns white_background admin-messages">
        <p><strong>Below 100 credits:</strong><a href="{{ url('/admin/reports/credits/under_hundred')}}"> Vendors below 100 credits</a></p>
    </div>
    <div class="small-12 columns white_background admin-messages">
        <p><strong>Free Credits Expiry:</strong><a href="{{ url('/admin/reports/free_credit_expiry')}}"> 1st free 100 credits</a></p>
    </div>
    <div class="small-12 columns white_background admin-messages">
        <p><strong>Cron:</strong><a href="{{ url('/admin/cron_tasks')}}" > Tasks</a></p>
        <p><strong>Cron:</strong><a href="{{ url('/admin/cron_schedules')}}"> Schedules</a></p>
    </div>
     <div class="small-12 columns white_background admin-messages">
        <p><strong>Bids:</strong><a href="{{route('classification.bids')}}"> Bids per Category</a></p>
    </div>
    <div class="small-12 columns white_background admin-messages">
        <p><strong>Email Ads</strong><a href="{{route('user-ads.create')}}"> Manage Email Ads</a></p>
    </div>
    <div class="small-12 columns white_background admin-messages">
        <p><strong>Send Notifications:</strong><a href="/sms/get"> Send notifications</a></p>
    </div>
</div>