<link rel="stylesheet" href="/css/dataTables.foundation.min.css">


<p>{!!link_to_route('users.create', 'Add new Vendor') !!}</p>


@if ($users->count())
    <table id="users-table" class="table table-striped table-bordered small-12">
        <thead>
        <tr>
            <th>Username</th>
            <th>Email</th>
            <th>Verified</th>
            <th>Role</th>
            <th>Created</th>
            <th>Last Login</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>
                    {!! link_to_route('users.show', ($user->first_name), array($user->id)) !!}
                    @if($user->trashed())
                        <span class="fi-skull"></span>
                    @endif
                </td>
                <td>{{$user->email}}</td>
                <td>{{$user->verified}}</td>
                <td> @foreach($user->roles as $role)
                        {{$role->name}}
                    @endforeach
                </td>
                <td>{{$user->created_at}}</td>
                <td>{{$user->last_login}}</td>
                <td>
                    <a class="btn btn-success btn-xs" href="/reset/first/{{$user->id}}">
                        <i class="fi-plus"></i>
                    </a>
                    <a class="btn btn-success btn-xs" href="/users/{{$user->id}}">
                        <i class="fi-pencil"></i>
                    </a>
                    @if($user->hasUserRole('vendor') )
                    <a class="btn btn-xs" href="/vendor/gallery/{{$user->id}}">
                        <i class="fi-photo"></i>
                    </a>
                    <a class="btn btn-xs" href="/vendor/credit-vendor/{{$user->id}}">
                        <i class="fi-bitcoin"></i>
                    </a>
                    @endif
                    <a href="/admin/users/delete/{{$user->id}}" class="btn btn-info btn-xs"><span
                                class="fi-trash"></span></a>
                    @if($user->trashed())
                        <a href="/admin/users/restore/{{$user->id}}" class="btn btn-info btn-xs"><span
                                    class="fi-refresh"></span></a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>


    </table>
@else
    There are no users
@endif
<script data-cfasync="false" type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
<script data-cfasync="false" type="text/javascript" src="/js/dataTables.foundation.min.js"></script>
<script data-cfasync="false" type="text/javascript">
    (function ($) {
        $('#users-table').DataTable();
    })(jQuery);
</script>
