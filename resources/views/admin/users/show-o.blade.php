<link rel="stylesheet" href="/css/select2.min.css"/>
{!! HTML::style('/assets/css/parsley.css') !!}
<link rel="stylesheet" type="text/css" href="/css/file-validator.css">
<div class="small-12 columns">
    @include('flash::message')
</div>
<div>
    @if(Auth::user()->hasUserRole('administrator'))
        <div class="small-12 columns white_background" style="border-radius: 25px;padding: 20px; ">
            <div class="small-12 columns">
                {!! Form::model($user, [
                    'method' => 'POST',
                    'route' => ['status.update', $user->id],
                    'class' => 'form-horizontal',
                    'enctype'=>'multipart/form-data'
                ]) !!}
                <div class="form-group small-12 columns {{ $errors->has('verified') ? 'has-error' : ''}}">
                    {!! Form::label('Verified', 'Verified: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!!  Form::checkbox('verified',1,$user->verified) !!}
                    </div>
                </div>
                <div class="form-group small-12 columns {{ $errors->has('added_by_admin') ? 'has-error' : ''}}">
                    {!! Form::label('added_by_admin', 'Added by admin: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!!  Form::checkbox('added_by_admin',1,$user->added_by_admin) !!}
                    </div>
                </div>
                <div class="form-group small-12 columns {{ $errors->has('test_account') ? 'has-error' : ''}}">
                    {!! Form::label('test_account', 'Test Account: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!!  Form::checkbox('test_account',1,$user->test_account) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="show-for-medium-up medium-4 columns">
                        {!! Form::label('classification_list','Featured in:',['class' => 'right inline']) !!}
                    </div>
                    <div class="small-6 medium-8 columns tip-right"
                         data-options="disable_for_touch:true" data-tooltip="classification"
                         aria-haspopup="true"
                         title="Select the services you offer. Resist the temptation to choose many. Specialists tend to succeed more than generalists.">


                        <select id="classification_list" name="classifications_list[]"
                                class="step2 has-tip tip-right" multiple style="width:100%"
                                data-parsley-trigger="change focusout">
                            @if(@$selected)
                                @foreach($classifications as $key =>$classification)

                                    @foreach($selected as $select)
                                        @if($classification == $select)
                                            <option selected="selected"
                                                    value="{{$key}}">{{$classification}}</option>
                                        @else
                                            <option value="{{$key}}">{{$classification}}</option>
                                        @endif
                                    @endforeach
                                @endforeach
                            @else
                                @foreach($classifications as $key =>$classification)
                                    <option value="{{$key}}">{{$classification}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                {!! Form::submit('Update', ['class' => 'open-close  button small']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    @endif
    <div class="small-12 columns white_background " style="border-radius: 25px; padding: 20px; ">
        <h4>{{$user->first_name}} {{$user->last_name}} | Personal Information <span
                    class="warning label round radius"><i class="fi-wrench"></i> <a
                        href="/users/{{$user->id}}/edit">Edit Profile</a></span></h4>
        <div class="row">
            <img id="featuredImage" class="circle show-for-medium-up medium-4 columns"
                 src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$user->profile->profilePic}}"
                 alt="Featured image"/>
        </div>
        <p><i class="fi-mail"></i> {{$user->email}}</p>
        <p><i class="fi-telephone"></i> {{$user->profile->phone}}</p>
        <p><i class="fi-marker"></i> {{@$user->location->address}} <a class="warning label"
                                                                      href="/user-location/{{$user->id}}"><i
                        class="fi-marker"></i> Change </a></p>
    </div>
    @if(Auth::user()->hasUserRole('administrator') || Auth::user()->hasUserRole('vendor'))
        @if($user->hasUserRole('vendor'))
            <div class="small-12 columns white_background " style="border-radius: 25px; padding: 20px; ">
                <h4>Overview:</h4>
                @if($user->top_vendor())
                    <h5>***Top Vendor***</h5>
                @endif
                @if($user->recommended_vendor())
                    <h6>***Recommended Vendor***</h6>
                @endif

                <p><strong>Description:</strong> {{$user->profile->description}}</p>

                @if(Auth::user()->hasUserRole('administrator'))
                    <p><strong>Ranking:</strong> {{$user->ranking}} points</p>
                @endif
                <p><strong>Rating :</strong></p>
                <ul class="stars">
                    @for ($i=1; $i <= 5 ; $i++)
                        <li class="active"><span
                                    class="fi-heart {{ ($i <= $user->profile->rating_cache) ? '' : 'inactive'}}"></span>
                        </li>
                    @endfor
                </ul>

                <p><strong>Galleries:</strong></p>
                <ul>
                    @foreach($user->galleries as $gallery)
                        <li>{{$gallery->name}} ({{$gallery->images->count()}} Photos)</li>
                    @endforeach
                </ul>
            </div>
            <div class="small-12 columns green_background" style="border-radius: 25px; padding: 20px; ">
                @unless($user->classifications->isEmpty())
                    <p><strong>My Services:</strong>
                        @if(Auth::user()->hasUserRole('administrator'))
                            <span
                                    class="warning label round radius"><i class="fi-wrench"></i> <a
                                        href="/user/categories/{{$user->id}}">Edit Services</a></span>
                        @endif
                    </p>
                    @foreach($user->classifications as $classification )
                        <span class="info label round radius">
                    {{$classification->name}}
                </span>
                    @endforeach
                @endunless
            </div>
            @if(Auth::user()->hasUserRole('administrator'))
                <div class="small-12 columns green_background" style="border-radius: 25px; padding: 20px; ">
                    <p><strong>Services Details:</strong>

                        <span
                                class="warning label round radius"><i class="fi-wrench"></i> <a
                                    href="/user/options/{{$user->id}}">Edit Details</a></span>
                    </p>
                    @unless($user->descriptions->isEmpty())
                        @foreach($user->descriptions as $description )
                            <span class="info label round radius">
                    {{$description->classification->name}} | {{$description->option->name}}
                                | {{$description->description_detail}}
                </span>
                        @endforeach
                    @endunless
                </div>
            @endif
            <div class="small-12 columns white_background" style="border-radius: 25px; padding: 20px; ">
                <h4>Credit History:</h4>
                <p><strong>Purchases:</strong> {{count($credit_history)}} </p>
                <table id="example" class="table table-striped table-bordered col-md-12">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Sale Price</th>
                        <th>Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($credit_history as $obj)
                        <tr>
                            <td>{{@$obj->name}}</td>
                            <td>{{@$obj->quantity}}</td>
                            <td>{{@$obj->sale_price}}</td>
                            <td>{{@$obj->created_at->diffForHumans()}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="small-12 columns white_background" style="border-radius: 25px; padding: 20px; ">
                <h4>Reports:</h4>
                <p><strong>Number of bids made:</strong> {{$user->bid->count()}} </p>
                <p><strong>Credit Balance:</strong> {{$credits}} </p>
                <table id="example" class="table table-striped table-bordered col-md-12">
                    <thead>
                    <tr>
                        <th>Job Title</th>
                        <th>Classification</th>
                        <th>Credits</th>
                        <th>Posted by</th>
                        <th>Date</th>
                        @if(Auth::user()->hasUserRole('administrator'))
                            <th>Job Budget</th>
                            <th>Your Quote</th>
                            <th>Bid Status</th>
                            <th>Job Status</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($user->bid as $bid)
                        <tr>
                            <td>{{str_limit(@$bid->job->title,28,'...')}}</td>
                            <td>{{@$bid->job->classification}}</td>
                            <td>{{@$bid->job->credits}}</td>
                            <td>{{@$bid->job->user->first_name}}</td>
                            <td>{{@$bid->created_at->diffForHumans()}}</td>
                            @if(Auth::user()->hasUserRole('administrator'))
                                <td> {{@$bid->job->budget}}</td>
                                <td>
                                    @if(@$bid->estimated_price)
                                        {{@$bid->estimated_price}}
                                    @else
                                        Not Set
                                    @endif
                                </td>
                                <td>
                                    @if(@$bid->status == 2)
                                        Accepted
                                    @elseif(@$bid->status == 1)
                                        Shortlisted
                                    @elseif(@$bid->status == 3)
                                        Declined
                                    @elseif(@$bid->status == 0)
                                        Unread
                                    @elseif(@$bid->status == 4)
                                        In review
                                    @endif
                                </td>
                                <td>{{@$bid->job->completed? 'Closed' : 'Open' }}</td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
        @if(Auth::user()->hasUserRole('administrator'))
            @if($user->hasUserRole('user') || $user->hasUserRole('administrator') )
                <div class="small-12 columns white_background" style="border-radius: 25px; padding: 20px; ">
                    <h4>Jobs:</h4>
                    <p><strong>Price Estimates Requested:</strong> {{$user->job->count()}} </p>
                    <table id="example" class="table table-striped table-bordered col-md-12">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Budget</th>
                            <th>location</th>
                            <th>Created</th>
                            <th>No. of bids</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($user->job as $obj)
                            <tr>
                                <td>{{@$obj->title}}</td>
                                <td>{{@$obj->budget}} vs
                                    [@foreach(@$obj->bids as $bid)
                                        {{@$bid->estimated_price}},
                                    @endforeach]
                                </td>
                                <td>{{@$obj->event_location}}</td>
                                <td>{{@$obj->created_at->diffForHumans()}}</td>
                                <td>{{$obj->bids->count()}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        @endif
</div>
@endif
<script data-cfasync="false" src="/js/select2.full.min.js"></script>
<script type="text/javascript">
    $('#classification_list').select2({
        maximumSelectionLength: 3,
        placeholder: "To be featured in: "
    });
</script>

