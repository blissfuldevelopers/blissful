@extends('layouts.main')
@section('title')
    Log
@endsection
@section('styles')

@endsection
@section('content')
    <div class="container">
        @foreach($latestActivities as $latestActivity)
        <p>
            {{$latestActivity->user_id}} |
            {{$latestActivity->text}} |
            {{$latestActivity->ip_address}}  |
            {{$latestActivity->created_at}}
            {{$latestActivity->first_name}}
        </p>
            @endforeach
    </div>
@endsection