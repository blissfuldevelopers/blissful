@section('title')
Profile
@overwrite

<div class="small-12 columns user-prof">
    <div class="small-12 columns">
        <div class="small-5 columns">
            <img id="featuredImage"  src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$user->profile->profilePic}}" alt="Featured image"/>
        </div>
        <div class="small-7 columns">
            <h4>{{$user->first_name}} {{$user->last_name}}</h4>
            <p><i class="fi-mail"></i> {{$user->email}}</p>
            <p><i class="fi-telephone"></i> {{$user->profile->phone}}</p>
            @if($user->hasUserRole('vendor'))
            <p><i class="fi-marker"></i> {{@$user->location->address}} 
                <a class="warning label" href="/user-location/{{$user->id}}"><i class="fi-marker"></i> Change </a>
            </p>
            <p><strong>Credit Balance:</strong> {{$credits}} </p>
            @endif
        </div>
        <div class="small-12 columns">
            
            @if($user->top_vendor())
                <h6>***Top Vendor***</h6>
            @endif
            @if($user->recommended_vendor())
                <h6>***Recommended Vendor***</h6>
            @endif
            @if(Auth::user()->hasUserRole('administrator'))
                <p><strong>Ranking:</strong> {{$user->ranking}} points</p>
            @endif
            @if($user->hasUserRole('vendor'))
            <div class="small-12 columns"><strong class="float-left">Rating :</strong> 
                <ul class="stars">
                    @for ($i=1; $i <= 5 ; $i++)
                        <li class="active"><span
                                    class="fi-heart {{ ($i <= $user->profile->rating_cache) ? '' : 'inactive'}}"></span>
                        </li>
                    @endfor
                </ul>
            </div>
            @endif
            @if($user->classifications->count() > 0)
            <div class="small-12 columns"><strong>Services:</strong>
                @foreach($user->classifications as $classification )
                    <span class="info label round radius">
                        {{$classification->name}}
                    </span>
                @endforeach
            </div>
            @endif
        </div>  
    </div>
    <!-- navigation area -->
    <div class="small-12 columns user-navs">
        <div class="small-6 medium-4 columns text-center user-nav-block">
            <a href="{{route('users.edit',[$user->id])}}">
                <i class="fa fa-user-circle-o"></i>
                <p>Edit Profile</p>
            </a>
        </div>
        <div class="small-6 medium-4 columns text-center user-nav-block">
            <a href="/user/reports/{{$user->id}}">
                <i class="fa fa-tasks"></i>
                <p>Site Activity</p>
            </a>
        </div>
        @if($user->hasUserRole('vendor'))
        <div class="small-6 medium-4 columns text-center user-nav-block">
            <a href="/gallery/list">
                <i class="fa fa-photo"></i>
                <p>Photo Galleries</p>
            </a>
        </div>
        @endif
    </div>
</div>


