<div id="form_sixty" style="width:80%;">
    {!!  Form::model($user, array('id'=>'form','class'=>'with-img','method' => 'POST', 'enctype'=>'multipart/form-data' ,'route' => array('categories.update', $user->id))) !!}
    <p>Categories</p>
    <ul>
        @foreach($user->classifications as $classification)
            <li>{!! Form::label($classification->id, $classification->name) !!}{!!   Form::checkbox('classifications[]', $classification->id, in_array($classification->id, $selected)) !!}</li>
            <ul>
                @foreach($classification->children as $children)
                    <li>{!! Form::label($children->id, $children->name) !!}{!!   Form::checkbox('classifications[]', $children->id, in_array($children->id, $selected)) !!}</li>
                    <ul class="inline-list">
                        @foreach($children->children as $grands)
                            <li>{!! Form::label($grands->id, $grands->name) !!}{!!   Form::checkbox('classifications[]', $grands->id, in_array($grands->id, $selected)) !!}</li>
                        @endforeach
                    </ul>
                @endforeach
            </ul>
        @endforeach
    </ul>
    <div class="row">
        <div class="right breaker">
            {!! Form::submit('Update',[
            'class' => 'button withimg tiny btn-primary'
            ]) !!}
        </div>
    </div>
    {!!  Form::close() !!}
</div>