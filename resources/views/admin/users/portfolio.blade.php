@section('title')
Portfolio
@overwrite
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" />

<main role="main" class="row" id="contactpg">
    <div class="small-12 columns">
        @if ($errors->any())
        <ul>
        {!!  implode('', $errors->all('<li data-alert class="alert-box alert"><a href="#" class="close">&times;</a> :message</li>')) !!}
        </ul>
        @endif
        <div class="small-12 columns">
            <h3>Let’s add some pictures to your profile </h3>
            <h6> Make a great first impression, upload some great pictures of your work. Each picture should not be more than <strong> 2MB. </strong></h6>
        </div>
        <div class="small-12 columns">
            <div class="row">
                <ul class="tabs vertical medium-3 columns show-for-medium-up" data-tab disabled>
                  <li class="tab-title active"><a href="#">Step 3 of 3 Final Step</a></li>
                </ul>
                <div class="small-12 medium-9 columns drop_zone">
                    <div class="small-12 columns breaker-left">
                        <form method="post" action="{{url('portfolio/'.$id)}}"
                            class="dropzone"
                            id="addPortfolio"
                            enctype="multipart/form-data">
                            {{ csrf_field() }}
                        </form>
                        <div class="right breaker">
                            <a class="button small button-primary" id="continue" href="/dashboard" >Done</a>
                        </div>
                    </div>       
                </div>
            </div>
        </div>
    </div>
</main>
@section('scripts')
<script defer src="/js/dropzone.js"></script>
<script defer src="/js/app.js"></script>
@overwrite