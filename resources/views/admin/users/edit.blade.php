@section('title')
Edit Profile
@overwrite
<link rel="stylesheet" href="/css/select2.min.css"/>
{!! HTML::style('/assets/css/parsley.css') !!}
<link rel="stylesheet" type="text/css" href="/css/file-validator.css">
<main role="main" id="contactpg">
    @if ($errors->any())
        <ul>
            {!!  implode('', $errors->all('<li data-alert class="alert-box alert"><a href="#" class="close">&times;</a> :message</li>')) !!}
        </ul>
    @endif
    @include('includes.status')

        <div id="form_sixty" class="small-12 columns">
            {!!  Form::model($user, array('id'=>'form','class'=>'with-img','method' => 'PATCH', 'enctype'=>'multipart/form-data' ,'route' => array('users.update', $user->id))) !!}
            <div class="edit-tabs" id="myWizard">
                <div class="content active" id="step1">
                    <div class="row">

                        <div class="small-12 columns">
                            <div class="row">
                                <div class="hide-for-small-only medium-4 columns">
                                    {!! Form::label('first_name', 'Business Name:',['class' => 'right inline']) !!}
                                </div>
                                <div class="small-12 medium-8 columns">
                                    {!! Form::text('first_name',null,[
                                  'class' => ' first has-tip tip-right ',
                                  'required',
                                  'placeholder' =>'Business Name',
                                  'data-options'=>'disable_for_touch:true',
                                  'data-tooltip'=>'name',
                                  'aria-haspopup'=>'true',
                                  'title'=>'This is what users will see on your profile. Your name will be searchable. Don’t add words like - Limited, Enterprises, Partners etc. We just need the brand name.',
                                  'id'=>'first_name',
                                  'data-parsley-required-message' => 'Business name is required',
                                  'data-parsley-minlength'        => '6',
                                  'data-parsley-trigger'          => 'change focusout']) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="hide-for-small-only medium-4 columns">
                                    {!! Form::label('inputEmail', 'Email:',['class' => 'right inline']) !!}
                                </div>
                                <div class="small-12 medium-8 columns">
                                    {!! Form::email('email',null,[
                                      'class' => ' first has-tip tip-right ',
                                      'required',
                                      'placeholder' =>'Email',
                                      'data-options'=>'disable_for_touch:true',
                                      'data-tooltip'=>'email',
                                      'aria-haspopup'=>'true',
                                      'title'=>'Should be valid. We will validate this and this is what you will use to login. All future communication will be sent here',
                                      'id'                            => 'inputEmail',
                                      'data-parsley-required-message' => 'Email is required',
                                      'data-parsley-trigger'          => 'change focusout',
                                      'data-parsley-type'             => 'email'
                                      ]) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="hide-for-small-only medium-4 columns">
                                    {!! Form::label('password', 'Password:',['class' => 'right inline']) !!}
                                </div>
                                <div class="small-12 medium-8 columns">
                                    {!! Form::password('password',[
                                      'class'                         => ' first has-tip tip-right',
                                      'id'                            =>'password',
                                      'placeholder' =>'Password',
                                      'data-options'=>'disable_for_touch:true',
                                      'data-tooltip'=>'password',
                                      'aria-haspopup'=>'true',
                                      'title'=>'Should be a minimum of 6 characters, make it hard to guess :)',
                                      'data-parsley-required-message' => 'Password is required',
                                      'data-parsley-trigger'          => 'change focusout',
                                      'data-parsley-minlength'        => '6',
                                      'data-parsley-maxlength'        => '20'
                                      ]) !!}
                                </div>
                                <div id="messages"></div>
                            </div>
                            <div class="row">
                                <div class="hide-for-small-only medium-4 columns">
                                    {!! Form::label('inputPassword', 'Confirm Password:',['class' => 'right inline']) !!}
                                </div>
                                <div class="small-12 medium-8 columns">
                                    {!! Form::password('password_confirmation',[
                                    'class'                         => 'first',
                                    'placeholder' =>'Confirm Password',
                                    'id'                            => 'inputPassword',
                                    'data-parsley-required-message' => 'Please confirm your password',
                                    'data-parsley-trigger'          => 'change focusout',
                                    'data-parsley-equalto'          => '#password'
                                    ]) !!}
                                </div>
                            </div>
                            <div class="row">
                                    <button id="continue" class="tiny right next button-primary non-ajax">Continue</button>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="content" id="step2">
                    <div class="row">
                        <div class="small-12 columns">
                            <div class="row">
                                <div class="hide-for-small-only medium-4 columns">
                                    {!! Form::label('Description', 'Business Description:',['class' => 'right inline']) !!}
                                </div>
                                <div class="small-12 medium-8 columns">
                                    {!! Form::textarea('description', $profile->description,[
                                        'class'                         => 'step2 has-tip tip-right',
                                        'rows'                          =>'4',
                                        'required',
                                        'placeholder' =>'Business Description',
                                        'data-options'=>'disable_for_touch:true',
                                        'data-tooltip'=>'description',
                                        'aria-haspopup'=>'true',
                                        'title'=>'Your business description should be simple, specific and should answer the question - “ Why should someone choose you?"',
                                        'data-parsley-required-message' => 'Business description is required',
                                        'data-parsley-minlength'        => '10',
                                        'data-parsley-maxlength'        => '350',
                                        'data-parsley-trigger'          => 'change focusout'
                                    ]) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="hide-for-small-only medium-4 columns">
                                    {!! Form::label('Business Location', 'Business Location:',['class' => 'right inline']) !!}
                                </div>
                                <div class="show-for-small-only small-4 columns">
                                    {!! Form::label('Business Location', 'Location:',['class' => 'inline']) !!}
                                </div>
                                <div class="small-8 medium-8 columns">
                                    <select name="location" placeholder="Business Location"
                                            class="step2 has-tip tip-right" data-options="disable_for_touch:true" ,
                                            data-tooltip="location" aria-haspopup="true"
                                            title="Select the city or town your business is located." required
                                            data-parsley-required-message="Please select where you are located"
                                            data-parsley-trigger="change focusout" data-placeholder="Services Offered">
                                        <optgroup>
                                            @foreach($locations as $location)
                                                @if($location->name == $profile->location)
                                                    <option selected="selected"
                                                            value="{{$location->name}}">{{$location->name}}</option>
                                                @else
                                                    <option value="{{$location->name}}">{{$location->name}}</option>
                                                @endif
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="hide-for-small-only medium-4 columns">
                                    {!! Form::label('phone', 'Business Phone no:',['class' => 'right inline']) !!}
                                </div>
                                <div class="small-12 medium-8 columns">
                                    {!! Form::text('phone', $profile->phone,[
                                    'class'                         => 'step2 has-tip tip-right',
                                    'required',
                                    'placeholder' =>'Business Phone no',
                                    'data-options'=>'disable_for_touch:true',
                                    'data-tooltip'=>'phone',
                                    'aria-haspopup'=>'true',
                                    'title'=>'This should be a mobile number ONLY. One that clients can reach you on.',
                                    'data-parsley-type'=>'number',
                                    'data-parsley-pattern'=>'(0|\+?254)(7|\2)([0-9|7])(\d){7}',
                                    'data-parsley-required-message' => 'Phone number is required',
                                    'data-parsley-pattern-message' => 'Phone number should be valid',
                                    'data-parsley-trigger'          => 'change focusout'
                                    ]) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="hide-for-small-only medium-4 columns">
                                    {!! Form::label('classification_list','Services Offered:',['class' => 'right inline']) !!}
                                </div>
                                <div class="small-12 medium-8 columns tip-right"
                                     data-options="disable_for_touch:true" data-tooltip="classification"
                                     aria-haspopup="true"
                                     title="Select the services you offer. Resist the temptation to choose many. Specialists tend to succeed more than generalists.">


                                    <select id="classification_list" name="classifications_list[]"
                                            class="step2 has-tip tip-right select_multiple" multiple style="width:100%" required
                                            data-parsley-required-message="Please Select a Category"
                                            data-parsley-trigger="change focusout">

                                        @foreach($classifications as $classification)
                                                <option {{(in_array($classification->id,$selected)) ? "selected" : ""}} value="{{$classification->id}}">
                                                    {{$classification->name}}
                                                </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @if(Auth::user()->hasUserRole('administrator'))
                            <div class="row breaker">
                                <div class="hide-for-small-only medium-4 columns">
                                    {!! Form::label('boosts_list','Boosts Subscribed:',['class' => 'right inline']) !!}
                                </div>
                                <div class="small-12 medium-8 columns tip-right"
                                     data-options="disable_for_touch:true" data-tooltip="boosts"
                                     aria-haspopup="true"
                                     title="These are the boosts that the vendor has subscribed to">


                                    <select id="boosts_list" name="boosts_list[]"
                                            class="step2 has-tip tip-right select_multiple" multiple style="width:100%" data-placeholder="boosts">
                                        @foreach($boosts as $boost)
                                        <option {{(in_array($boost->id,$selected_boosts)) ? "selected" : ""}} value="{{$boost->id}}">
                                            {{$boost->name}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endif
                            <div class="row breaker-bottom">
                                <div class="small-4 columns">
                                    {!! Form::label('file','Profile Picture:',['class' => 'right input_img']) !!}
                                </div>
                                <div class="small-8 columns">
                                    <div class="row breaker">
                                      <div class="medium-4 columns hide-for-small-only">
                                        <img id="featuredImage" class="circle"
                                             src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$profile->profilePic}}" alt="Featured image"/>
                                      </div>
                                      <div class="small-12 medium-8 columns">
                                        <input type="file" placeholder="Profile Picture"
                                               data-parsley-required-message="Please upload a featured image"
                                               class="small-12 medium-8 columns feat_input has-tip tip-right jfilestyle"
                                               data-placeholder="Profile Picture" data-options="disable_for_touch:true"
                                               data-tooltip="profile" aria-haspopup="true"
                                               title="This is what users see when they search for your business. It should not be more than 2MB in size."
                                               data-max-size='2mb' data-type='image' id="file" name="file"/>
                                      </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="right breaker">
                                    <button class="button tiny return" href="#">Back</button>
                                    {!! Form::submit('Update',[
                                    'class' => 'button withimg tiny button-primary'
                                    ]) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!!  Form::close() !!}
            </div>
</main>
@section('scripts')
<script defer src="/js/select2.full.min.js"></script>
<script defer src="/assets/plugins/parsley.min.js"></script>
<script defer type="text/javascript" src="/js/file-validator.js" onLoad="start_scripts()"></script>
<script defer src='https://www.google.com/recaptcha/api.js'></script>

<script type="text/javascript">
function start_scripts(){
    $('.select_multiple').select2({
        maximumSelectionLength: 3,
        placeholder: $(this).attr('placeholder')
    });
    $('input.feat_input').fileValidator({
        onValidation: function (files) {
            $(this).attr('class', '');
        },
        onInvalid: function (type, file) {
            $(this).addClass('invalid ' + type);
            $(this).val(null);
        }
    });
    $('#form').parsley({
        errorsWrapper: '<div></div>',
        errorTemplate: ' <small class="error"></small>'
    });

    $('#myWizard').on('click', '.next', function (event) {
        event.preventDefault();
        $('#myWizard > .active').removeClass('active').next().addClass('active');
    });
    $('#myWizard').on('click', '.return', function (event) {
        event.preventDefault();
        $('#myWizard > .active').removeClass('active').prev().addClass('active');
    });

    function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#featuredImage').attr('src', e.target.result);
          }
          reader.readAsDataURL(input.files[0]);
      }
    }

    $("#file").change(function () {
        readURL(this);
    });
}
</script>
@overwrite
