@section('title')
    Vendor Registration Services in Kenya
@overwrite
<link rel="stylesheet" href="/css/select2.min.css"/>
{!! HTML::style('/assets/css/parsley.css') !!}
<link rel="stylesheet" type="text/css" href="/css/file-validator.css">
<main role="main" class="small-12 columns" id="contactpg">
<div class="small-12 columns">
    <h3>@if(!Auth::check()) Grow your wedding & events business</h3>
    <h6> If you have the skills, equipment and or location to put together a beautiful wedding, we have thousands of users looking for you. </h6>
    <h5><strong>Join the community and grow your business!</strong></h5>
    <h3>@elseif( Auth::user()->hasRole('administrator'))Add Vendor @endif</h3>
</div>
<div class="small-12 medium-7 large-6 columns small-centered">
    @include('includes.status')
</div>
<div class="small-12 columns">
    {!!  Form::open(['url' => route('users.store'), 'id'=>'form', 'data-parsley-validate', 'enctype'=>'multipart/form-data']) !!}
        <div class="small-12 registration-container">
          <div class="registration-tab visible" id="step1">
                <div class="row">
                    <ul class="tabs vertical medium-3 columns show-for-medium-up" data-tab disabled>
                      <li class="tab-title active"><a href="#">Step 1 of 3 Introduction</a></li>
                    </ul>
                    <div class="small-12 medium-9 columns">
                      <div class="row">
                        <div class="show-for-medium-up medium-4 columns">
                          {!! Form::label('first_name', 'Business Name:',['class' => 'right inline']) !!} 
                        </div>
                        <div class="small-12 medium-8 columns">
                          {!! Form::text('first_name',null,[
                        'class' => ' first has-tip tip-right ',
                        'required',
                        'placeholder' =>'Business Name',
                        'data-options'=>'disable_for_touch:true',
                        'data-tooltip'=>'name',
                        'aria-haspopup'=>'true',
                        'title'=>'This is what users will see on your profile. Your name will be searchable. Don’t add words like - Limited, Enterprises, Partners etc. We just need the brand name.',
                        'id'=>'first_name',
                        'data-parsley-required-message' => 'Business name is required',
                        'data-parsley-minlength'        => '6',
                        'data-parsley-trigger'          => 'change focusout']) !!}
                        </div>
                      </div>
                        <div class="row">
                            <div class="show-for-medium-up medium-4 columns">
                                {!! Form::label('inputEmail', 'Email:',['class' => 'right inline']) !!}
                            </div>
                            <div class="small-12 medium-8 columns">
                              {!! Form::email('email',null,[
                                'class' => ' first has-tip tip-right ',
                                'required',
                                'placeholder' =>'Email',
                                'data-options'=>'disable_for_touch:true',
                                'data-tooltip'=>'email',
                                'aria-haspopup'=>'true',
                                'title'=>'Should be valid. We will validate this and this is what you will use to login. All future communication will be sent here',
                                'id'                            => 'inputEmail',
                                'data-parsley-required-message' => 'Email is required',
                                'data-parsley-trigger'          => 'change focusout',
                                'data-parsley-type'             => 'email'
                                ]) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="show-for-medium-up medium-4 columns">
                                {!! Form::label('password', 'Password:',['class' => 'right inline']) !!}
                            </div>
                            <div class="small-12 medium-8 columns">
                              {!! Form::password('password',[
                                'class'                         => ' first has-tip tip-right', 
                                'id'                            =>'password',
                                'required',
                                'placeholder' =>'Password',
                                'data-options'=>'disable_for_touch:true',
                                'data-tooltip'=>'password',
                                'aria-haspopup'=>'true',
                                'title'=>'Should be a minimum of 6 characters, make it hard to guess :)',
                                'data-parsley-required-message' => 'Password is required',
                                'data-parsley-trigger'          => 'change focusout',
                                'data-parsley-minlength'        => '6',
                                'data-parsley-maxlength'        => '20'
                                ]) !!}
                            </div>
                            <div id="messages"></div>
                        </div>
                        <div class="row">
                            <div class="show-for-medium-up medium-4 columns">
                                {!! Form::label('inputPassword', 'Confirm Password:',['class' => 'right inline']) !!}
                            </div>
                            <div class="small-12 medium-8 columns">
                            {!! Form::password('password_confirmation',[
                            'class'                         => 'first',
                            'required',
                            'placeholder' =>'Confirm Password',
                            'id'                            => 'inputPassword',
                            'data-parsley-required-message' => 'Please confirm your password',
                            'data-parsley-trigger'          => 'change focusout',
                            'data-parsley-equalto'          => '#password'
                            ]) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 columns">
                                <button id="continue" class="tiny button-primary button right next" disabled>Continue</button>
                            </div> 
                        </div>     
                    </div>
                    
                </div>
          </div>
          <div class="registration-tab" id="step2">
                <div class="row">
                    <ul class="tabs vertical medium-3 columns show-for-medium-up" data-tab disabled>
                        <li class="tab-title"><a href="#">Step 2 of 3 Almost There</a></li>
                    </ul>
                    <div class="small-12 medium-9 columns">
                      <div class="row">
                        <div class="show-for-medium-up medium-4 columns">
                            {!! Form::label('Description', 'Business Description:',['class' => 'right inline']) !!}
                        </div>
                        <div class="small-12 medium-8 columns">
                            {!! Form::textarea('description', null,[
                                'class'                         => 'step2 has-tip tip-right', 
                                'rows'                          =>'4',
                                'required',
                                'placeholder' =>'Business Description',
                                'data-options'=>'disable_for_touch:true',
                                'data-tooltip'=>'description',
                                'aria-haspopup'=>'true',
                                'title'=>'Your business description should be simple, specific and should answer the question - “ Why should someone choose you?"',
                                'data-parsley-required-message' => 'Business description is required',
                                'data-parsley-minlength'        => '10',
                                'data-parsley-maxlength'        => '350',
                                'data-parsley-trigger'          => 'change focusout'
                            ]) !!}
                        </div>
                      </div>
                      <div class="row">
                        <div class="show-for-medium-up medium-4 columns">
                            {!! Form::label('Business Location', 'Business Location:',['class' => 'right inline']) !!}
                        </div>
                        <div class="show-for-small-only small-4 columns">
                            {!! Form::label('Business Location', 'Location:',['class' => 'inline']) !!}
                        </div>
                        <div class="small-8 medium-8 columns">
                            <select name="location" placeholder="Business Location" class="step2 has-tip tip-right" data-options= "disable_for_touch:true", data-tooltip="location" aria-haspopup="true" title="Select the city or town your business is located." required data-parsley-required-message="Please select where you are located" data-parsley-trigger="change focusout" >
                                <optgroup label="Business Location">
                                    @foreach($locations as $location)
                                    <option value="{{$location->name}}">{{$location->name}}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                      </div>
                        <div class="row">
                            <div class="show-for-medium-up medium-4 columns">
                                {!! Form::label('phone', 'Business Phone no:',['class' => 'right inline']) !!}
                            </div>
                            <div class="small-12 medium-8 columns">
                                {!! Form::text('phone', null,[
                                'class'                         => 'step2 has-tip tip-right',
                                'required',
                                'placeholder' =>'Business Phone no',
                                'data-options'=>'disable_for_touch:true',
                                'data-tooltip'=>'phone',
                                'aria-haspopup'=>'true',
                                'title'=>'This should be a mobile number ONLY. One that clients can reach you on.',
                                'data-parsley-type'=>'number',
                                'data-parsley-pattern'=>'(0|\+?254)(7|\2)([0-9|7])(\d){7}',
                                'data-parsley-required-message' => 'Phone number is required',
                                'data-parsley-pattern-message' => 'Phone number should be valid',
                                'data-parsley-trigger'          => 'change focusout'
                                ]) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="show-for-medium-up medium-4 columns">
                                {!! Form::label('classification_list','Services Offered:',['class' => 'right inline']) !!}
                            </div>
                            <div class="small-12 medium-8 columns tip-right" data-options = "disable_for_touch:true" data-tooltip="classification" aria-haspopup="true" title="Select the services you offer. Resist the temptation to choose many. Specialists tend to succeed more than generalists.">
                                {!! Form::select('classifications_list[]', $classifications, null, [
                                'id'=>'classification_list',
                                'class'                         => 'step2 has-tip tip-right',
                                'multiple', 
                                'style'                         =>'width:100%;',
                                'required',
                                'data-parsley-required-message' => 'Category is required',
                                'data-parsley-trigger'          => 'change focusout'
                                ]) !!}
                            </div>
                        </div>
                        <div class="row breaker">
                            <div class="small-5 columns">
                                {!! Form::label('file','Profile Picture:',['class' => 'input_img']) !!}
                            </div>
                            <div class="small-7 columns">
                                <div class="row">
                                    <div class="show-for-medium-up medium-4 columns">
                                        <img id="featuredImage" class="circle" src="/img/grey.jpg" alt="Featured image" />
                                    </div>
                                    <div class="small-12 medium-8 columns">
                                        <input type="file" required="required" placeholder="Profile Picture" data-parsley-required-message="Please upload a featured image" class=" feat_input has-tip tip-right jfilestyle" data-placeholder="Profile Picture" data-options = "disable_for_touch:true" data-tooltip="profile" aria-haspopup="true" title="This is what users see when they search for your business. It should not be more than 2MB in size." data-max-size='2mb' data-type='image' id="file" name="file"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row breaker">
                            <div class="show-for-medium-up medium-4 columns"></div>
                            <div class="small-12 medium-8 columns">
                                <div id="formCaptcha" class="right"></div>
                            </div>
                        </div>
                        <div class="row">
                           <div class="small-12 columns">
                                <div class="right breaker">
                                    <button class="button small back" >Back</button>
                                    {!! Form::submit('Save',[
                                    'class' => 'button button-primary small btn-save'
                                    ]) !!}
                                </div>
                            </div> 
                        </div>
                    </div>
              </div>
            </div>
        </div>
    {!!  Form::close() !!}
</div>
</main>
@section('scripts')
<script defer src="/js/select2.full.min.js"></script>
<script defer type="text/javascript" src="/assets/plugins/parsley.min.js"></script>
<script defer type="text/javascript" src="/js/file-validator.js" onLoad="start_scripts();"></script>

<script type="text/javascript">

    function start_scripts(){

        calculate_height();
        $('#classification_list').select2({
           maximumSelectionLength: 3,
           placeholder:"Services Offered"
        });

        $('input.feat_input').fileValidator({
        onValidation: function(files){      $(this).attr('class','');          },
        onInvalid:    function(type, file){ 
            $(this).addClass('invalid '+type); 
            $(this).val(null); 
        }
        });

        $('#form').parsley( {
            errorsWrapper: '<div></div>',
            errorTemplate: ' <small class="error"></small>'
            });
        $(document).on('click', '.next', function(event) {
               event.preventDefault();
               $('.visible').removeClass('visible').next().addClass('visible');
               calculate_height();
        }); 
        $(document).on('click', '.back', function(event) {
               event.preventDefault();
               $('.visible').removeClass('visible').prev().addClass('visible');
               calculate_height();
        });
                
        var $input = $('.first'),
        $register = $('#continue');    
        $register.attr('disabled', true);

        $input.keyup(function() {
            var trigger = false;
            $input.each(function() {
                if (!$(this).val()) {
                    trigger = true;
                }
            });
            trigger ? $register.attr('disabled', true) : $register.removeAttr('disabled');
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#featuredImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        
        $("#file").change(function(){
            readURL(this);
        }); 
    }
    
    function calculate_height() {
        var re_height = $('.registration-tab.visible').height()+8;
        var re_width = $('.registration-tab.visible').width()+5;
        $('.registration-container').height(re_height);
        $('.registration-container').width(re_width);
    }
</script>
@endsection
