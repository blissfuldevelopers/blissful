<h1>Location</h1>
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th>ID.</th>
            <th>Name</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{ $location->id }}</td>
            <td> {{ $location->name }} </td>
        </tr>
        </tbody>
    </table>
</div>