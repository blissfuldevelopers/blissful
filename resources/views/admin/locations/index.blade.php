<h1>Locations <a href="{{ route('locations.create') }}" class="btn btn-primary pull-right btn-sm">Add New Locations</a>
</h1>
<div class="table">
    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th>S.No</th>
            <th>Name</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        {{-- */$x=0;/* --}}
        @foreach($locations as $item)
            {{-- */$x++;/* --}}
            <tr>
                <td>{{ $x }}</td>
                <td><a href="{{ url('/locations', $item->id) }}">{{ $item->name }}</a></td>
                <td>
                    <a href="{{ route('locations.edit', $item->id) }}">
                        <button type="submit" class="btn btn-primary btn-xs">Update</button>
                    </a> /
                    {!! Form::open([
                        'method'=>'DELETE',
                        'route' => ['locations.destroy', $item->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="pagination"> {!! $locations->render() !!} </div>
</div>
