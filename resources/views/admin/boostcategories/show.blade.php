<!-- Bootstrap -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

<!-- DataTable Bootstrap -->
<link href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css" rel="stylesheet">
<div class="container">
    <div class="container">

        <h1>Boost Category - {{ $boostcategory->name }}
            <a href="{{ route('admin.boostcategories.show', [$boostcategory->id]) }}/edit"
               class="btn btn-primary btn-xs"
               title="Edit Boost"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
            {!! Form::open([
                'method'=>'DELETE',
                'route' => ['admin.boostcategories.destroy', $boostcategory->id],
                'style' => 'display:inline'
            ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Boost',
                    'onclick'=>'return confirm("Confirm delete?")'
            ))!!}
            {!! Form::close() !!}
        </h1>
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <tbody>

                <tr>
                    <th> Name</th>
                    <td> {{ $boostcategory->name }} </td>
                </tr>
                <tr>
                    <th> Parent Category</th>
                    <td> {{ $boostcategory->parent->name }} </td>
                </tr>
                <tr>
                    <th> Children</th>
                    <td> {{ $boostcategory->children->count() }} </td>
                </tr>

                </tbody>
            </table>
        </div>

    </div>
