@if(isset($categories))
    <div class="form-group">
        {!! Form::label('parent_id', 'Parent Category: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <select name="parent_id" placeholder=category" class="form-control">
                <option value="">No Parent Category</option>
                <optgroup label="Category">
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
@endif

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
