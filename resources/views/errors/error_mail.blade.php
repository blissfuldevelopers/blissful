<html style="padding: 0; margin: 0">
<body style="padding: 20px; margin: 0; font-family: 'Helvetica Neue', Arial, sans-serif; font-size: 13px;">
<p>
    User, {{ $user }} ran into the error below:
</p>
<hr size="1"/>

<h3>URL: /{{ $current_url }}</h3>
<h4>Date: {{ date('d-M-y H:i:s') }}</h4>
<hr size="1"/>
<p>