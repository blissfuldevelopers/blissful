@extends('page.zurb-layout')
@section('title')
404
@endsection
@section('content')
<!-- START OF MAIN SECTION -->
  <main role="main" id="categories" class="lightblue">
      <section class="c-header hundrd">
        <img src="/img/catheaders/1.jpg">
      </section>
      <section class="error-box small-12 medium-6" style="background:transparent;">
        <div class="row">
          <div class="small-12 medium-7 columns">
            <img src="/img/236.png">
          </div>
          <div class="small-12 medium-5 columns" style="margin-top:15%;">
            <h3 class="grey"><strong>Whoa There!</strong></h3>
            <p class="grey"><strong></strong> Looks like there's nothing to see here...</p>
            <a href="{{ URL::previous() }}" class="button tiny left">Go Back</a>
          </div>
        </div>
      </section>
  </main>
@stop