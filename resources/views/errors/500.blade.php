@extends('page.zurb-layout')
@section('title')
    500
    @endsection
    @section('content')
            <!-- START OF MAIN SECTION -->
    <main role="main" id="categories" class="lightblue">
        <section class="c-header hundrd">
            <img src="/img/catheaders/1.jpg">
        </section>
        <section class="error-box small-12 medium-6" style="background:transparent;">
            <div class="row">
                <div class="small-12 medium-8 columns">
                    <h3 class="grey"><strong>ERROR 500</strong></h3>
                    <p class="grey"><strong>OOPS!</strong> Something went terribly wrong..</p>
                    <img src="/img/500.jpg">
                </div>

            </div>
        </section>
    </main>
@stop