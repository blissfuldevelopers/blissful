@extends('layouts.main')

@section('content')
    @foreach($credit_bundles as $bundle)
        <div class="col-sm-4 col-lg-4 col-md-4">
            <div class="thumbnail">
                <img src="http://placehold.it/320x120" alt="">

                <div class="caption">
                    <h4 class="pull-right">${{ $bundle->cost}}</h4>
                    <h4>{{$bundle->title}}</h4>

                    <p>{{$bundle->description}} <br> <strong>Expiration time:</strong> {{$bundle->expiration_time}}</p>

                </div>
                <a href="{{ url('bundles/home', $bundle->id) }}">
                    <button type="submit" class="btn btn-primary btn-xs">Purchase</button>
                </a>
            </div>
        </div>
    @endforeach
@endsection