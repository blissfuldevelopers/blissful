@section('scripts')
<link rel="stylesheet" type="text/css" href="/css/jquery-te-1.4.0.css">
<link rel="stylesheet" type="text/css" href="/assets/css/cleanslate.css">
<link rel="stylesheet" type="text/css" href="/assets/css/message-tab.css">
@endsection
@if($thread)
    <div class="small-12 columns">
      <div id="quotation-area">
            @foreach($thread->participants as $participant)
                @if(@$participant->user && @$participant->user->id != Auth::user()->id)
                <div class="row">
                  <div class="small-12 medium-8 columns">
                      <span class="pimg noMessage left">
                        <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$participant->user->profile->profilePic}}" alt="{!! $participant->user->first_name !!}" class="img-circle">
                      </span>
                      <div class="left status-user">
                        <span> {{$participant->user->first_name}}</span><br>
                        <span> {{$participant->user->email}}</span><br>
                        <span> {{@$participant->user->profile->phone}}</span>
                      </div>
                  </div>
                  <div class="small-12 medium-4 columns">
                      <div class="status-tag">
                          {{$status}}
                      </div>
                  </div>                  
                </div>
                @endif
            @endforeach
        <div id="message_center" class="row collapse">
            <div class="small-12 columns">
              <h4>{!! $thread->subject !!} :</h4>
              <div class="row sub-classifs bid-classifs">
                  <div> 
                      <div class="small-12 columns classif-desc">
                          <p>Service: {{@$bid->job->category->name}}</p>
                      </div>
                  </div>
              </div>
              @foreach(@$breakdown_categories as $breakdown_category)
                <div class="row sub-classifs bid-classifs">
                    <div>
                        <div class="bid-section-container">
                            <p class="bid-section-title"> {{$breakdown_category->name}} </p>
                        </div>
                    </div>
                </div>
                @foreach(@$bid->details as $detail)
                  @if($detail->cat_id == $breakdown_category->id)
                    <div class="row bid-section-input">
                      <div class="small-12 medium-3 columns">
                        <label>Item
                          <p>{{$detail->item_name}}</p>
                        </label>
                      </div>
                      <div class="small-12 medium-4 columns">
                        <label>Description
                          <div class="detail-description">
                            {!!$detail->description!!}
                          </div>
                          
                        </label>
                      </div>
                      <div class="small-4 medium-1 columns">
                        <label>Quantity
                          <p>{{$detail->quantity}}</p>
                        </label>
                      </div>
                      <div class="small-4 medium-2 columns">
                        <label>Unit Price
                          <p>{{number_format($detail->unit_price)}} ksh</p>
                        </label>
                      </div>
                      <div class="small-4 medium-2 columns">
                        <label>Sub Total
                          <p class="bid-total"><strong> {{number_format($detail->unit_price * $detail->quantity)}} Ksh</strong></p>
                        </label>
                      </div>
                    </div>
                  @endif
                @endforeach
              @endforeach
              
              <div class="row bid-section-total">
                  <p>Total: <strong>{{number_format($bid->estimated_price)}} Ksh</strong></p>
              </div>
              <p class="bid-tnc">Terms and conditions or Additional info</p>
              <div id="thread_{{$thread->id}}">
                @foreach($thread->messages as $key => $message)
                  @if($key == 0)
                  <div class="small-12 medium-10 small-centered columns">
                      <div class="media-body">
                          <div class="cleanslate">
                              {!! $message->body !!}
                          </div>
                      </div>
                  </div>
                  
                  <div class="small-12 columns" id= "file-area">
                      <div class="small-12 text-center">
                          <button class="download-pdf button tiny button-primary">Download as PDF</button>
                      </div>
                      @if($bid)
                      @foreach(@$bid->attachments as $attachment)
                      <a href="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$attachment->file_path}}" target="_blank" class="non-ajax download-link">
                        <div class="small-12 medium-7 small-centered columns attachment-row ongoing breaker-bottom">
                          <div class="doc-container">
                            <div class="small-5 columns">
                              <div class="doc-icon">
                                <div class="doc-icon-container">
                                  <i class="fi-page large"></i>
                                </div>
                                <div class="doc-loader">
                                  <div></div>
                                </div>
                              </div>
                            </div>
                            <div class="small-7  columns">
                              <p class="doc-name">{{$attachment->original_filename}}</p>
                              <span class="doc-size">{{$attachment->readable_size}}</span>
                            </div>
                            <p class="green doc-download text-center">Download</p>
                          </div>
                        </div>
                      </a>
                      @endforeach
                      @endif
                  </div>
                  @else
                  <div class="row message-card">
                    <div class="small-12 medium-2 large-1 columns">
                        <span class="pimg message-avi">
                          <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$message->user->profile->profilePic}}" alt="{!! $message->user->first_name !!}"
                               class="img-circle">
                        </span>
                        <div class="message-user-name left show-for-small-only">
                          <span>{{$message->user->first_name }}</span>
                        </div>
                    </div>
                    <div class="small-12 medium-10 large-11 columns">
                      <div class="media-body">
                          <div class="cleanslate">
                              {!! $message->body !!}
                          </div>
                          <div class="text-muted">
                              Posted {!! $message->created_at->diffForHumans() !!}
                          </div>
                      </div>
                    </div>
                    
                  </div>
                  @endif
                @endforeach
              </div>
            </div>
          </div>
        </div>
        @if( in_array($bid->status, [0,4])  && Auth::user()->id == @$bid->job->user_id)
          <div class="small-12 medium-8 columns actions-wrapper small-centered">
              <div class="small-12 medium-6 columns ">
                  {!! Form::open(['route' => 'bids.change_status']) !!}
                  {!! Form::hidden('id', $bid->id) !!}
                  {!! Form::hidden('status', '1') !!}
                  {!! Form::button('Shortlist Vendor and Reply', ['type'=>'submit','class' => 'accept-button']) !!}
                  {!! Form::close() !!}
              </div>
              <div class="small-12 medium-6 columns ">
                  {!! Form::open(['route' => 'bids.change_status']) !!}
                  {!! Form::hidden('id', $bid->id) !!}
                  {!! Form::hidden('status', '3') !!}
                  {!! Form::button('Decline Quotation', ['type'=>'submit','class' => 'decline-button']) !!}
                  {!! Form::close() !!}
              </div>
          </div>
        @elseif($bid->status == 3)
        <h5 class="hm red">Price Estimate declined by user</h5>
        @elseif(!$bid->job)
        <h5 class="hm red">This Job has been closed</h5>
        @elseif($bid->job->completed == 1 && $bid->status != 2)
        <h5 class="hm red">This Job has been closed</h5>
        @else
        <h5>Reply</h5>
        <div class="small-12">
          {!! Form::open(['route' => ['messages.update', $thread->id], 'method' => 'PUT', 'onLoad'=>'functionLoadMCE();']) !!}
                    <!-- Message Form Input -->
            <div class="small-12">
                {!! Form::textarea('message', null, ['rows'=>'6','class' => 'tinymce-text msg-area']) !!}
            </div>
            <input type="hidden" id="recipients" value="{{$message->user->profile->id}}">
            <!-- Submit Form Input -->
            <div class="small-10 medium-5 small-centered columns">
                {!! Form::submit('Send', ['class' => 'submit-btn button send-message-btn']) !!}
            </div>
            {!! Form::close() !!}
        </div>
          @if(Auth::user()->id == @$bid->job->user_id)
          <div class="row">
              <div class="small-12 medium-4 small-centered columns">
                  {!! Form::open(['route' => 'bids.change_status', 'class'=>'' ]) !!}
                  {!! Form::hidden('id', $bid->id) !!}
                  {!! Form::hidden('status', '2') !!}
                  {!! Form::submit('Accept quotation & close job', ['class' => 'decline-button']) !!}
                  {!! Form::close() !!}
              </div>
          </div>
          @endif
        @endif
    </div>
@else
  <h5>No Message Thread found</h5>  
@endif

<script defer src="/assets/tinymce/tinymce.min.js" onLoad="functionLoadMCE();"></script>
<script defer type="text/javascript" src="/assets/js/jspdf.min.js"></script>
<script defer type="text/javascript" src="/assets/js/html2canvas.js"></script>
<script type="text/javascript">
  function load_page_scripts(){
    $.getScript( "/js/jquery-te-1.4.0.min.js" );
    $(".download-pdf").click(function() { 
        $('#file-area').hide();
        html2canvas($("#quotation-area"), {
            onrendered: function(canvas) {
                theCanvas = canvas;
                var imgData = canvas.toDataURL('image/png');              
                var doc = new jsPDF('l', 'mm');
                doc.addImage(imgData, 'PNG', 10, 10);
                doc.save('{{$bid->user->first_name}}-quotation.pdf');
                $('#file-area').show();
            }
        });
    });   
    $('.submit-btn').click(function(event){
      event.preventDefault();
      tinyMCE.triggerSave();

        var editorContent = tinyMCE.activeEditor.getContent();
        if ( editorContent == '' || editorContent == null)
        {
          $(tinyMCE.activeEditor.getContainer())
            .css("background-color", '#ffeeee')
            .parent()
            .css({
              "background-color": '#ffeeee',
              "border": '1px solid #EC736C'
            });
          $('<span class="red">Please Type in your message</span>').insertAfter($(tinyMCE.activeEditor.getContainer()));

        }

      else{
          $(this).closest('form').submit();
      }

    });
  }
  function functionLoadMCE() {
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;

    if(!isMobile) {
      tinymce.init({
        selector: ".tinymce-text",
        theme: "modern",
        menubar: false,
        plugins: [
            "advlist autolink lists link charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime  nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools "
        ],
        toolbar: " bold italic alignleft aligncenter alignright alignjustify bullist numlist outdent indent preview",
        relative_urls: false,
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ]
      });
    }

    else{
        tinymce.init({
          selector: ".tinymce-text",
          theme: "modern",
          menubar: false,
          toolbar: false,
          relative_urls: false,
          image_advtab: true,
          templates: [
              {title: 'Test template 1', content: 'Test 1'},
              {title: 'Test template 2', content: 'Test 2'}
          ]
        });
    } 
  }
</script>
