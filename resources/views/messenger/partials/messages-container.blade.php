{!! HTML::style('/assets/css/dashboard-messages.css') !!}
<div id="messages-container">
<!--     <div class="dash-stats small-12 columns collapse hide-for-small-only">
        <ul>
            <li>
                <i class="fa fa-envelope"></i>
                <span class="stats-title">New Messages</span>
                <h5 class="new-jobs ns">0</h5>
            </li>
            <li>
                <i class="fa fa-envelope-o"></i>
                <span class="stats-title">Active Messages</a></span>
                <h5 class="credits-balance ns">0</h5>
            </li>
        </ul>
    </div> -->
    <div class="grey small-12 row">
       <div id="messages-container-back-action">
            <ul class="sub-nav">
                <li class="small-3 columns">
                    <a href="#back" class="small-12">
                        <i class="fi-arrow-left medium"></i>
                        <span class="text">Back</span>
                    </a>
                </li>

                <li class="small-5 columns hm">
                    <span class="small-12 grey">
                        <a href="#home" class="small-12">
                            <!-- <span class="text">Inbox</span> -->
                        </a>
                    </span>
                </li>

                <li class="small-3 columns">
                    <a href="#reload" class="">
                        <i class="fi-refresh medium right"></i>
                    </a>
                    <span id="curr-page-name"
                          data-url=""
                    ></span>
                </li>
            </ul>
        </div>
    </div>
    <div class="small-12" id="messages-container-message"></div>
    <div class="small-12" id="messages-container-data"
         data-url="{{ @$default_messages_route }}"
         data-requested-url="{{ @$requested_messages_route }}">

    </div>
</div>
