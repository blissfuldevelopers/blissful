<link rel="stylesheet" type="text/css" href="/css/jquery-te-1.4.0.css">
<link rel="stylesheet" type="text/css" href="/assets/css/reset-context.css">
@section('title')
{{$thread->subject}}
@endsection
<div class="container">
    @if($thread)
        <div class="small-12 columns">
                @foreach($thread->participants as $participant)
                    @if(@$participant->user && @$participant->user->id != Auth::user()->id)
                    <div class="row collapse">
                        <div class="small-3 medium-1 large-1 columns">
                            <span class="pimg noMessage">
                                <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{@$participant->user->profile->profilePic}}" alt="{!! @$participant->user->first_name !!}" class="img-circle">
                            </span>
                        </div>
                        <div class="small-9 medium-3 large-3 columns left">
                            <span> {{@$participant->user->first_name}}</span></br>
                            <span> {{@$participant->user->email}}</span>
                            <span> {{@$participant->user->profile->phone}}</span>
                        </div>
                    </div>
                    @endif
                @endforeach
            <div id="message_center" class="row collapse">
              <div class="small-12 medium-11 small-centered columns">
                <h4>{!! $thread->subject !!}</h4>
                <div id="thread_{{$thread->id}}">
                  @foreach($thread->messages as $key => $message)
                    <div class="row collapse">
                      <div class="small-3 medium-1 large-1 columns">
                        <a class="left avatarm" href="#">
                          <span class="pimg">
                              <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{@$message->user->profile->profilePic}}" alt="{!! @$message->user->first_name !!}"
                                   class="img-circle">
                          </span>
                        </a>
                      </div>
                      <div class="small-9 medium-11 large-11 columns">
                        <div class="media-body breaker">
                            <div class="yui-cssreset">
                                {!! $message->body !!}
                            </div>
                            <div class="text-muted">
                                <small>Posted {!! $message->created_at->diffForHumans() !!}</small>
                            </div>
                        </div>
                      </div>
                     
                    </div>
                  @endforeach
                </div>
                <h5>Reply</h5>
              </div>     
            </div>
            <div class="row">
              {!! Form::open(['route' => ['messages.update', $thread->id], 'method' => 'PUT', 'onLoad'=>'functionLoadMCE();']) !!}
                        <!-- Message Form Input -->
                <div class="small-12 medium-11 small-centered columns">
                    {!! Form::textarea('message', null, ['rows'=>'6','class' => 'tinymce-text msg-area']) !!}
                </div>
                <input type="hidden" id="recipients" value="{{@$message->user->id}}">
                <!-- Submit Form Input -->
                <div class="small-8 small-centered columns breaker text-center">
                  <button type="submit" class="submit-btn button small-12 send-message-btn">Reply</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    @else
        No Message Thread found
    @endif

</div>
<script type="text/javascript" src="/js/jquery-te-1.4.0.min.js"></script>
<script type="text/javascript">
    // $(".msg-area").jqte();
</script>
<script src="/assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">

  var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
    functionLoadMCE();

      function functionLoadMCE() {
        if(!isMobile) {
            tinymce.init({
            selector: ".tinymce-text",
            theme: "modern",
            menubar: false,
            plugins: [
                "advlist autolink lists link charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime  nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern imagetools "
            ],
            toolbar: " bold italic alignleft aligncenter alignright alignjustify bullist numlist outdent indent preview",
            relative_urls: false,
            image_advtab: true,
            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ]
        });
        }

    else{
            tinymce.init({
            selector: ".tinymce-text",
            theme: "modern",
            menubar: false,
            toolbar: false,
            relative_urls: false,
            image_advtab: true,
            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ]
        });
        } 
      }
    
    
$('.submit-btn').click(function(event){
    event.preventDefault();
    tinyMCE.triggerSave();

      var editorContent = tinyMCE.activeEditor.getContent();
      if ( editorContent == '' || editorContent == null)
      {
        $(tinyMCE.activeEditor.getContainer())
          .css("background-color", '#ffeeee')
          .parent()
          .css({
            "background-color": '#ffeeee',
            "border": '1px solid #EC736C'
          });
        $('<span class="red">Please Type in your message</span>').insertAfter($(tinyMCE.activeEditor.getContainer()));

      }

    else{
        $(this).closest('form').submit();
    }

  });
</script>
