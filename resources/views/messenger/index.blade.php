<div class="container">
    @if (Session::has('error_message'))
        <div class="alert alert-danger" role="alert">
            {!! Session::get('error_message') !!}
        </div>
    @endif
    <!-- Adsense Section 1-->
    <div class="row">
        <div class="small-12 columns breaker breaker_bottom">
            <div class="hide-for-small-only">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Blissful_responsive -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-2847929148248604"
                     data-ad-slot="1315972373"
                     data-ad-format="auto"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
            <div class="show-for-small-only">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- mobile_horizontal banner -->
                <ins class="adsbygoogle"
                     style="display:inline-block;width:320px;height:100px"
                     data-ad-client="ca-pub-2847929148248604"
                     data-ad-slot="2792705573"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </div>
    </div>
    @if($threads && $threads->count() > 0)
        <div>
            <ul class="messages accordion" data-accordion role="messagelist">
                @foreach($threads as $thread)
                    <?php $class = $thread->isUnread($currentUserId) ? 'unread' : 'read'; ?>

                    <li class="accordion-navigation">
                        <div class="small-12 columns msg-block {{ $class }} ">
                             <a
                                id="thread_list_{{@$thread->id}}" class="inner-page-link"
                                role="tab"
                                id="panel1d-heading"
                                aria-controls="panel1d"
                                href="{{ route('messages') }}/{{ $thread->id }}">
                            
                            <div class="small-3 medium-1 columns">
                                <div class="msg-avi">
                                    <span>{{ str_limit(@$thread->participantsString(Auth::id(), ['first_name']), 1,"" ) }}</span> 
                                </div>
                            </div>
                            <div class="small-9 breaker medium-3 columns media-heading">
                                <strong class="left breaker-right">
                                    {!! @$thread->participantsString(Auth::id(), ['first_name']) !!}
                                    {!! @$thread->participantsString(Auth::id(), ['last_name']) !!}
                                </strong>
                                <div class="dash-section-attributes left">
                                @foreach(@$thread->participants as $participant)
                                    @if(@$participant->user && @$participant->user->id != Auth::user()->id)
                                        @if(@$participant->user->top_vendor())
                                            <div  class="top-vendor-badge infotip">
                                                <img src="/img/super-vendor.svg">
                                                <span class="infotiptext">Super Vendor</span>
                                            </div>
                                        @endif
                                        @if(@$participant->user->recommended_vendor())
                                            <div  class="recommended-badge infotip">
                                              <i class="ion-thumbsup"> </i> 
                                              <span class="infotiptext">Recommended</span>
                                            </div>
                                        @endif
                                    @endif
                                @endforeach
                                </div>
                                
                            </div>
                            <div class="small-12 breaker medium-5 columns" id="thread_list_{{@$thread->id}}_text">
                                {{ str_limit(@$thread->subject, 70, "...") }}
                            </div>
                            <div class="small-12 breaker medium-3 columns">
                                @if(@$thread->created_at->isToday())
                                    {{ @$thread->created_at->format('g:ia') }}
                                @else
                                    {{ @$thread->created_at->format('j M Y') }}
                                @endif
                            </div>
                        </a>
                        </div>
                       
                        <div id="message_{{@$thread->id}}"
                             class="content inner-page-target"
                             role="tabpanel"
                             aria-labelledby="panel1d-heading"
                        >
                           
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
        @if(is_object($threads))
            <div class="small-12 pagination-centered breaker">
                {!! $threads->render(Foundation::paginate($threads)) !!}
            </div>
        @endif
        <!-- Adsense Section 1-->
        <div class="row">
            <div class="small-12 columns breaker breaker_bottom">
                <div class="hide-for-small-only">
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Blissful_responsive -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-2847929148248604"
                         data-ad-slot="1315972373"
                         data-ad-format="auto"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
                <div class="show-for-small-only">
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- mobile_horizontal banner -->
                    <ins class="adsbygoogle"
                         style="display:inline-block;width:320px;height:100px"
                         data-ad-client="ca-pub-2847929148248604"
                         data-ad-slot="2792705573"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
        </div>
    @else
        <p>Sorry, no threads.</p>
    @endif
</div>

<script>
    function load_page_scripts(){
        $('.msg-avi').each(function () {
            var hexArray = ['#5eb8c1','#0d2c3a','#ffbb3f', '#4CAF50']; 
            var randomColor = hexArray[Math.floor(Math.random() * hexArray.length)];
            console.log(randomColor);
            $(this).css("background",randomColor);
        });
    }
</script>

