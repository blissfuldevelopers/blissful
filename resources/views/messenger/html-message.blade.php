<div class="media">
    <a class="pull-left" href="#">
    <span class="pimg">
        <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$message->user->profile->profilePic}}" alt="{!! $message->user->first_name !!}" class="img-circle">
        <h5 class="media-heading">{!! $message->user->first_name !!} {!! $message->user->last_name !!}</h5>
    </span>
    </a>
    <div class="media-body placard">
        <p>{!! $message->body !!}</p>
        <div class="text-muted"><small>Posted {!! $message->created_at->diffForHumans() !!}</small></div>
    </div>
</div>
