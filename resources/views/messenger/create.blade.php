<link rel="stylesheet" href="/css/select2.min.css"/>

<div class="row">
    {!! Form::open(['route' => 'messages.store']) !!}
    <div class="small-12 medium-6 large-6 columns">
        <h4>Create a new message</h4>
        <div class="form-group">
            @if($users->count() > 0)
                {!! Form::label('recipients', 'To:', ['class' => 'control-label']) !!}
                {!!Form::select('recipients[]', $users, null, ['id'=>'recipients', 'class'=>'form-control','multiple', 'style'=>'width: 85%; margin: 0 !important; padding: 2px !important;']) !!}

            @endif
        </div>
        <!-- Subject Form Input -->
        <div class="form-group">
            {!! Form::label('subject', 'Subject', ['class' => 'control-label']) !!}
            {!! Form::text('subject', null, ['class' => 'form-control']) !!}
        </div>
        <!-- Message Form Input -->
        <div class="form-group">
            {!! Form::label('message', 'Message', ['class' => 'control-label']) !!}
            {!! Form::textarea('message', null, ['rows'=>'6','class' => 'form-control']) !!}
        </div>
        <!-- Submit Form Input -->
        <div class="form-group">
            {!! Form::submit('Send', ['class' => 'button tiny right form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}
</div>
</div>


<script src="/js/select2.full.min.js"></script>
<script>
    $('#recipients').select2({
        tags: true,
        placeholder: "Message recipients"
    });
</script>

