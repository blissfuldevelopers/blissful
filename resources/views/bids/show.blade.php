<h1>Bid</h1>
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th>ID.</th>
            <th>Bid Details</th>
            <th>Bid Accepted</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{ $bid->id }}</td>
            <td> {{ $bid->bid_details }} </td>
            <td> {{ $bid->bid_accepted }} </td>
        </tr>
        </tbody>
    </table>
</div>

