
@foreach($bids as $bid)
    @if($bid)
    <?php
        // this needs cleanup
        foreach ($bid->user->classifications as $classification) {
            $user_classifs[]= $classification->name;
        }
        foreach ($job->classifications as $classification) {
            $job_classifs[]= $classification->name;
        }
        if (isset($job_classifs)) {
           $results = array_intersect($user_classifs, $job_classifs);
        }
        else{
            $results = [];
        }
        switch ($bid->status) {
                    case 0:
                        $status = 'New';
                        break;
                    case 1:
                        $status = 'Shortlisted';
                        break;
                    case 2:
                        $status = 'Accepted';
                        break;
                    case 3:
                        $status = 'Declined';
                        break;
                    case 4:
                        $status = 'In Review';
                        break;
                }
    ?>

    <table class="small-12 columns">
        <tbody>
                <tr class="status-card">
                    <td class="status-avatar">
                        <div class="small-12 status-avi">
                            <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$bid->user->profile->profilePic}}"
                             alt="{{$bid->user->first_name}}">
                        </div>       
                    </td>
                    <td class="status-details">
                        <div class="small-12 columns status-name">
                                <div class="left">
                                    {{$bid->user->first_name}}
                                </div>
                            
                                @if($bid->user->top_vendor())
                                <div class="top-vendor-tag">
                                    Super Vendor
                                </div>
                                @endif
                                @if($bid->user->recommended_vendor())
                                <div class="recommended-vendor-tag">
                                    Recommended
                                </div>
                                @endif
                        </div>
                        <div class="small-12 columns status-location">
                            <img src="/assets/img/location-pointer.png">
                            {{$bid->user->profile->location}}
                        </div>
                        <div class="small-12 columns status-tags-wrapper">
                            @foreach($results as $result)
                            <div class="status-tags">
                                {{$result}}
                            </div>  
                            @endforeach
                        </div>
                       <div class="small-12 columns">
                           <div class="status-type">
                               {{@$status}}
                           </div>
                       </div>
                    </td>
                    <td class="status-actions">
                        <div class="small-12 ratings-wrapper">
                            <div class="ratings-text">
                                Rating: 
                            </div>
                            <div class="ratings-icons">
                                <ul class="h-rev">
                                    <li>
                                        <ul class="stars">
                                            @for ($i=1; $i <= 5 ; $i++)
                                            <li class="active"><span
                                                        class="fi-heart {{ ($i <= $bid->user->profile->rating_cache) ? '' : 'inactive'}}"></span>
                                            </li>
                                            @endfor
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <a href="{{ route('messages') }}/{{ $bid->thread->id }}"
                           class="tiny round button">View
                            Proposal</a>
                    </td>
                   <!--  <td>
                        @if($bid->status != 2)
                            <?php $status = 2; ?>
                            @include('bids.status-change-form')
                        @endif
                    </td>
                    <td>
                        @if($bid->status != 1 && $bid->status != 2)
                            <?php $status = 1; ?>
                            @include('bids.status-change-form')
                        @endif
                    </td>
                    <td>
                        @if($bid->status != 3 && $bid->status != 2)
                            <?php $status = 3; ?>
                            @include('bids.status-change-form')
                        @endif
                    </td> -->
                </tr>
        </tbody>
    </table>
  @endif
@endforeach