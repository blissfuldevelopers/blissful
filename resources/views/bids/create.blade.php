@extends('page.home-layout')
@section('title')
    Reply to quotation
@endsection
@section('styles')
<link rel="stylesheet" type="text/css" href="/assets/css/bid-form.css">
@endsection
@section('header-content')
  <?php
    $header_class = 'vendorpg-intro';
  ?>
@endsection
@section('content')
<div class="wrapper">
    <div class="row">
        @if(Session::has('success')|| $job->user_has_bid)
            <div class="reveal" id="user_success" data-reveal data-close-on-click="false"  data-animation-in="slide-in-right" data-animation-out="slide-out-right">
                <h2 id="modalTitle">ThankYou.</h2>
                <p class="lead">Your proposal was successfully sent.</p>
                <p>If you would like to follow up with a call or email, below are their contacts.</p>
                <div class="small-12 columns">
                    <div class="small-6 medium-4 columns pimg">
                        <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{@$job->user->profile->profilePic}}" class="circle" alt="{{@$bid->user->first_name}}">
                        <span>{{@$job->user->first_name.' '.@$job->user->last_name }}</span>
                    </div>
                    <div class="small-6 medium-8 columns dets">
                        <span><i class="fi-telephone large"></i> {{@$job->user->profile->phone}}</span>
                        <span><i class="fi-mail large"></i> {{@$job->user->email}}</span>
                    </div>
                </div>
                <button class="close-button" data-close aria-label="Close reveal" type="button">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if($job)
            @if(Auth::user()->hasUserRole('administrator') )
                <div class="row job-stats" data-equalizer>
                    <div class="small-4 medium-1 columns" data-equalizer-watch>
                        Total </br>
                        {{$bids->total()}}
                    </div>
                    <div class="small-4 medium-2 columns" data-equalizer-watch>
                        New Proposals </br>
                        {{(isset($bid_stats['unread']) ? $bid_stats['unread'] : '0')}}
                    </div>
                    <div class="small-4 medium-2 columns" data-equalizer-watch>
                        Shortlisted Proposals </br>
                        {{(isset($bid_stats['shortlisted']) ? $bid_stats['shortlisted'] : '0')}}
                    </div>
                    <div class="small-4 medium-2 columns" data-equalizer-watch>
                        Declined Proposals </br>
                        {{(isset($bid_stats['declined']) ? $bid_stats['declined'] : '0')}}
                    </div>
                    <div class="small-4 medium-2 columns" data-equalizer-watch>
                        Accepted Proposal </br>
                        {{$accepted_bid->count()}}
                    </div>
                    <div class="small-4 medium-3 columns">

                        {!! Form::model($job, [
                            'method' => 'PATCH',
                            'route' => ['jobs.update', $job->id],
                            'class' => 'form-horizontal'
                        ]) !!}
                        @if($job->completed == 0)
                            <input type="hidden" name="completed" value="1">
                            {!! Form::submit('Close Job', ['class' => 'open-close  button small']) !!}
                        @else
                            <input type="hidden" name="completed" value="0">
                            {!! Form::submit('Reopen Job', ['class' => 'open-close  button small']) !!}
                        @endif
                        {!! Form::close() !!}
                    </div>
                </div>
            @endif
            <div class="white_background job-details-left small-12 medium-10 small-centered columns 
            @if($job && $job->user_can_view_bids)hide-for-small-only  @endif">
                <div class="small-12 medium-7 large-6 columns small-centered">
                    @include('includes.status')
                </div>
                <div class="small-12">
                    <span class="avi">{{str_limit(@$job->user->first_name, 1,"" )}}{{str_limit(@$job->user->last_name, 1,"")}}</span>
                    <small class="right">
                        <span class="fi-calendar"></span> ( Posted {{$job->created_at->diffForHumans()}} )
                    </small>
                </div>
                <div class="small-12">
                    <h3>{{$job->title}}</h3>
                    <span class="crds">Credits needed to apply: {{$job->credits}} credits
                        <a href="#" onclick="window.open('/shop');" target="_blank">
                            Shop for Credits
                        </a>
                    </span>
                    <p>Category: {{@$job->category->name}}</p>
                    <p>Location: {{$job->event_location}}</p>
                    @if($job->guest_number)
                        <p>Estimated guest attendance: {{$job->guest_number}} guests</p>
                    @endif
                </div>
                <div class="small-12">
                    @if(!$job->classifications->isEmpty() && Auth::user()->hasUserRole('administrator'))
                        <p>I would like Price Estimates from suppliers that can provide the following.</p>
                        <ul class="job-subcategories">
                            @foreach($job->classifications as $subcategory)
                                <li>{{$subcategory->name}}</li>
                            @endforeach
                        </ul>
                    @endif
                    <h4 class="grey">Description</h4>
                    <p>{!! $job->description !!}</p>
                </div>
                <div class="small-12">
                    <h4 class="grey">Date</h4>
                    <p>{{ date('d, M Y',strtotime($job->event_date)) }} (
                        <small>{{ $job->datetime_to_event }}</small>
                        )
                    </p>

                </div>
                @if(Auth::user()->hasuserRole('administrator') || Auth::user()->hasBoost('Intelligence Boost - Basic') || Auth::user()->hasBoost('Intelligence Boost - Gold'))
                    <div class="small-12 ">
                        <p class="green"><strong>Posted by: </strong> {{@$job->user->first_name}}</p>
                        @if(Auth::user()->hasuserRole('administrator') )
                            <p class="green"><strong>Email: </strong> {{@$job->user->email}}</p>
                            <p class="green"><strong>Number: </strong> {{@$job->user->profile->phone}}</p>
                        @endif
                        <p class="green"><strong> Job Budget: </strong> {{ number_format($job->budget) }}</p>

                        @if(Auth::user()->hasuserRole('administrator') || Auth::user()->hasBoost('Intelligence Boost - Gold'))
                            <p class="green"><strong>Number of proposals so far: </strong>{{$job->bids()->count()}}</p>
                            <p> Shortlisted
                                Proposals: {{(isset($bid_stats['shortlisted']) ? $bid_stats['shortlisted'] : '0')}}</p>
                            <p>Declined Proposals: {{(isset($bid_stats['declined']) ? $bid_stats['declined'] : '0')}}</p>
                            <p>Accepted Proposal: {{$accepted_bid->count()}} </p>
                            @foreach(@$job->bids as $bid)
                                <p class="green">Proposal estimated price by <strong>{{@$bid->user->first_name}}:</strong>
                                    {{@$bid->estimated_price}}
                                </p>
                            @endforeach
                        @endif

                    </div>
                @else
                    {{--<div class="small-12">--}}
                    {{--<h4 class="grey">Budget</h4>--}}
                    {{--<p><i class="fi-alert"></i> <strong>This is a Premium Exclusive Feature</strong><span--}}
                    {{--class="alert label round radius"><i class="fi-unlock"></i> <a--}}
                    {{--onclick="window.location='/shop/subcategory/visibility-intelligence-boosts';">Get Premium</a></span>--}}
                    {{--</p>--}}
                    {{--</div>--}}
                    <div class="small-12">
                        <p class="green"><strong> Job Budget: </strong> {{ number_format($job->budget) }}</p>
                    </div>
                    
                @endif
                @if($job->user_has_bid)
                <p class="warning text-center">You have sent a proposal for this job</p>
                <a href="/messages/{{@$job->bid->thread->id}}"><p class="text-center green">Click here to view your bid</p></a>
                @else
                <div class="small-12 text-center">
                    <button class="button small button-primary show-bid-container">Send Proposal</button>
                </div>
                @endif
            </div>
            <div class="white_background job-details-right columns">
                <div class="job-details-container">
                    <div class="job-details-inner-container">
                        <div class="small-12 columns">
                            <div class="small-3 columns">
                                <img class="circle"
                                     src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{Auth::user()->profile->profilePic}}">
                            </div>
                            <div class="small-9 columns">
                                <div class="small-12 medium-6 columns">
                                    <h4>{{Auth::user()->first_name}}</h4>
                                    <p>{{isset(Auth::user()->location->address)? Auth::user()->location->address : Auth::user()->profile->location}}</p>
                                </div>
                                <div class="small-12 medium-6 columns">
                                    <p>{{Auth::user()->profile->phone}}</p>
                                    <p>{{Auth::user()->email}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="small-12 columns">
                            @include('bids.form')
                        </div>
                    </div>
                </div>
            </div>
            <span class="btn tiny show-bid-btn">go back to quotation request</span>
        @endif
    </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
    function load_page_scripts(){
        $('.show-bid-container').click(function(e){
            e.preventDefault();
            $('.job-details-right').addClass('job-details-right-visible');
            $('.job-details-right').on(
                "transitionend MSTransitionEnd webkitTransitionEnd oTransitionEnd",
                function() {
                    $('.show-bid-container').text('continue writing proposal');
                }
            );
            $('.show-bid-btn').attr('style','display:block !important');

        });
        $('.show-bid-btn').click(function(e){
            e.preventDefault();
            $('.job-details-right').removeClass('job-details-right-visible');
            $(this).attr('style','display:none !important');;
        });
        load_bids_scripts();

        @if(Session::has('success')|| $job->user_has_bid)
            $('#user_success').foundation('open');
        @endif
    }
    
</script>
@endsection
