<link rel="stylesheet" type="text/css" href="/assets/css/job-status.css">
<div class="row">
    <div class="small-12 columns">
        @include('flash::message')
    </div>
    @if($job && $job->user_can_view_bids)
        <div class="row">
            <div class="small-12 status-wrapper">
                <ul class="status-nav">
                    <li><a class="active" id="applications" href="/jobs/status/{{$job->id}}/0">Applications
                            <span>|</span>
                        </a>
                    </li>
                    <li><a id="shortlisted" href="/jobs/status/{{$job->id}}/1">Shortlisted
                            <span>|</span>
                        </a>
                    </li>
                    <li><a id="accepted" href="/jobs/status/{{$job->id}}/2">Accepted
                            <span>|</span>
                        </a>
                    </li>
                    <li><a id="declined" href="/jobs/status/{{$job->id}}/3">Declined
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        @if($bids->total() > 0 )
            <div class="row">
                <div class="small-12 ">
                    @include('bids.status-view-bid-details')
                </div>
            </div>
        @else
            <div class="row">
                <div class="small-12 columns no-bid">
                    <p>Hold on, there will be more proposals soon</p>
                </div>
            </div>
        @endif
    @endif
</div>

<script type="text/javascript">
    $('ul.status-nav > li > a').click(function(){
        $('ul.status-nav  li  a').removeClass("active");
        $(this).addClass("active");
    })
</script>
@if(@$state)
<script type="text/javascript">
    $('ul.status-nav  li  a').removeClass("active");
    $('#{{$state}}').addClass("active");
</script>
@endif