<div class="row">
    <div class="small-12">
        @include('flash::message')
    </div>
    <div class="small-12 medium-4 columns">
        <p>
        <ul>
            <li><strong>Job Title: </strong>{{$bid->job->title}}</li>
            <li><strong>Proposal by: </strong><a>{{$bid->user->first_name}}</a></li>
        </ul>
        </p>
        {!! Form::open(['method' => 'PATCH','route' => ['bids.update', $bid->id]]) !!}
        {!! Form::hidden('bid_accepted', 1) !!}
        {!! Form::submit('Accept Bid', ['class' => 'button small']) !!}
        {!! Form::close() !!}
    </div> 
    <div class="small-12 medium-8 columns">
        @foreach($bid->thread->messages()->get() as $message)
        <div class="media">
            <a 
            @if(($message->user->id) == (Auth::user()->id))
            class="right avatarm" 
            @else
            class="left avatarm"
            @endif 
            href="#">
            <span class="pimg">
                <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$message->user->profile->profilePic}}" alt="{!! $message->user->first_name !!}" class="img-circle">
            </span>
            </a>
            <div class="media-body placard white green_background">
                <p class="white">{!! $message->body !!}</p>
                <div class="text-muted"><small>Posted {!! $message->created_at->diffForHumans() !!}</small></div>
            </div>
        </div>
        @endforeach

        {!! Form::open(['route' => ['messages.update', $bid->thread->id], 'method' => 'PUT','id'=>'form', 'class'=>'breaker']) !!}
        {!! Form::textarea('message', null, [
                'required',
                'placeholder'=>'Write reply here',
                'rows'  => '4',
                'data-parsley-required-message' => 'Please write a message :)',
                'data-parsley-minlength'        => '2',
                'data-parsley-trigger'          => 'change focusout'
                ]) 
        !!}
        {!! Form::hidden('recipients', $message->user->profile->id) !!}
        {!! Form::submit('Reply', ['class' => 'button tiny round right']) !!}
        {!! Form::close() !!}
    </div>
</div>


@if ($errors->any())
    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif