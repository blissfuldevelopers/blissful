
<h1 class="km">Responses</h1>
<div class="row">
    <div class="small-12">
        <a href="{{ route('jobs.create') }}" class="button right alert tiny">Create new job</a>
        <table class="table small-12 table-bordered table-striped table-hover">
            <thead>
            <tr>
                <th>S.No</th>
                <th>Job Title</th>
                <th>Category</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}

            @foreach($bids as $bid)
                @foreach($bid as $item)
                    {{-- */$x++;/* --}}
                    <tr>
                        <td>{{ $x }}</td>
                        <td><a href="{{ url('/jobs', $item->job->id) }}">{{ $item->job->title }}</a></td>
                        <td>{{ $item->job->classification}}</td>
                        <td>{{$item->status}}</td>
                        <td>
                            <a href="#">
                                <button type="submit" class="tiny info" id="btn-shortlist" name="btn-shortlist"
                                        value="{{$item->id}}">Shortlist
                                </button>
                            </a>
                        </td>
                    </tr>
                @endforeach
            @endforeach
            </tbody>
        </table>
    </div>

</div>
