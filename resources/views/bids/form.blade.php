
<div class="row">
    <div class="bid-details">
        @if($job && $job->user_can_view_bids)
            @if($bids->total() > 0 )
            <div class="row bid-card">         
                <table class="small-12 columns">
                    <tbody>
                        @foreach($bids as $bid)
                        <tr>
                            <td>
                                <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{@$bid->user->profile->profilePic}}" class="circle" alt="{{@$bid->user->first_name}}">
                            </td>
                            <td>
                                {{@$bid->user->first_name}}

                            </td>
                            <td class="hm">
                                @if(@$bid->thread->id)
                                <a href="{{ route('messages') }}/{{ @$bid->thread->id }}" class="tiny round button">View Proposal
                                </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table> 
            </div>
            @else
            <p class="warning hm">No proposals for this job yet</p>
            @endif
        @elseif($job && $job->user_can_bid)
            {!! Form::open(['route' => 'bids.store','style'=>'width:100%', 'class'=>'with-editor', 'onLoad'=>'functionLoadMCE();','id'=>'job-bid-form' ]) !!}
            {!! Form::hidden('job_id', $job->id) !!}
            {!! Form::hidden('recipients[]', $job->user->id) !!}
            {!! Form::hidden('estimated_price',null,['id'=>'estimate_price']) !!}
            

            @if(!$job->classifications->isEmpty())
            <div class="row sub-classifs bid-classifs">
                <div> 
                    <div class="small-12 columns classif-desc">
                        <p>Service: {{@$job->category->name}}</p>
                        <p>The client would like a quotation break down for the following services</p>
                    </div>
                    @foreach($job->classifications as $subcategory)
                        @if($subcategory->parent_id == $subcategory->root_parent_id)
                        <div class="bid-section-container">
                            <p class="bid-section-title"><strong>{{$subcategory->name}}</strong> {{isset($subcategory->suggested) ? 'Client preference:'.$subcategory->suggested : ''}} </p>
                            <span data-cat="{{$subcategory->id}}" class="button tiny add-item">Add Item</span>
                        </div>
                        @endif
                    @endforeach
                </div>
            </div>
            @else
            <div class="row sub-classifs bid-classifs">
                <div> 
                    <div class="small-12 columns classif-desc">
                        <p>Service: <strong>{{@$job->category->name}}</strong></p>
                        <p>The client would like a quotation break down for the above service</p>
                    </div>
                    <div class="bid-section-container">
                        <p class="bid-section-title"> {{@$job->category->name}} </p>
                        <span data-cat="{{@$job->category->id}}" class="button tiny add-item">Add Item</span>
                    </div>
                </div>
                
            </div>
            @endif
            <div class="row bid-section-total">
                <p>Total: <strong>0 Ksh</strong></p>
            </div>
            <p class="bid-tnc">Terms and conditions or Additional info</p>
            {!! Form::textarea('message', null, ['rows'=>'6','id'=>'main-text','class'=>'tinymce-text', 'placeholder'=>'Type in Your Terms and conditions or Comments']) !!}
            @if($use_bootstrap_version)
                {!! Form::hidden('use_bootstrap_version', 'true') !!}
            @endif
            <div class="small-8 medium-8 columns small-centered breaker">
                <div class="small-4 columns">
                    <label for="file" class="right inline">Attach File</label>
                </div>
                <div class="small-8 columns">
                    <form id="upload_form" enctype="multipart/form-data" method="post">
                        <div class="small-12">
                            <input type="file" class="tiny" name="file" id="attachment-file">
                        </div>

                    </form>
                </div>
                <div class="attachment-container">
                    
                </div>
            </div>
            <div class="row">
                <div class="small-8 text-center small-centered columns breaker">
                    {!! Form::submit('Send Quote', ['class' => ' submit-btn button button-primary']) !!}
                </div>
            </div>
            {!! Form::close() !!}

        @elseif($job && $job->user_has_bid)
            <p class="warning text-center">You have made a proposal for this job</p>
            <a href="/messages/{{@$job->bid->thread->id}}"><p class="hm green">Click here to view your bid</p></a>
        
        @else
            <p class="warning text-center">You cannot make a proposal for this job</p>
        @endif
    </div>
</div>

<script defer src="/assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
function load_bids_scripts(){
        $.getScript( "/assets/plugins/parsley.min.js" );
        var click = 1;
        var children = [];
        @foreach($job->classifications as $subcategory)
            @if($subcategory->parent_id == $subcategory->root_parent_id)
                children[{{$subcategory->id}}] = '';
                @foreach($subcategory->children as $child)
                children[{{$subcategory->id}}] += '<option value="{{$child->id}}">{{$child->name}}</option>';
                @endforeach
            @endif
        @endforeach
        $(document).on('click', '.add-item', function(e) {
            e.preventDefault();
            var cat_id = $(this).data("cat");
            var input;

            if (children[cat_id] !== undefined && children[cat_id].length > 0) {
                
                input = '<select name="item_name-'+cat_id+'-'+click+'" required>'+children[cat_id]+'</select>';
            }
            else{
                input = '<input type="text" name="item_name-'+cat_id+'-'+click+'" required>';
            }
            
            var input_tab = '<div class="row bid-section-input"><span class="close-mark">&times;</span><span class="btn tiny add-mark">Add</span><div class="small-12 medium-3 columns"><label>Item '+input+'</label></div><div class="small-12 medium-4 columns"><label>Description<textarea id="description-'+cat_id+'-'+click+'" name="description-'+cat_id+'-'+click+'" class="bid-description-input" required></textarea></label></div><div class="small-4 medium-1 columns"><label>Quantity<input type="number" name="quantity-'+cat_id+'-'+click+'" class="total-input quantity-input" required></label></div><div class="small-4 medium-2 columns"><label>Unit Price<input type="number" name="unit_price-'+cat_id+'-'+click+'" class="total-input unit-price-input" required></label></div><div class="small-4 medium-2 columns"><label>Sub Total<p class="bid-total"><strong> 0.00 Ksh</strong></p></label></div></div>';
            
            click = click + 1;
            var sections = $('.bid-section-input');
            var if_valid = [];
            var needle = 0;

            //check if all input fields have been filled
            sections.each(function(){
                var valid = check_validity($(this));
                if_valid.push(valid);
            });
            
            //only add new item if previous items have been filled
            if (!if_valid.includes(needle)) {
                $('.section-error').remove();
                $(input_tab).insertBefore(this);
                //description 
                tinymce.init({
                    selector: ".bid-description-input",
                    theme: "modern",
                    menubar: false,
                    toolbar: false,
                    statusbar:false,
                    relative_urls: false,
                    image_advtab: false,
                    templates: [
                        {title: 'Test template 1', content: 'Test 1'},
                        {title: 'Test template 2', content: 'Test 2'}
                    ],
                    setup: function (ed) {
                        ed.on('init', function(args) {
                            var id = ed.id;
                            var height = 60;

                            document.getElementById(id + '_ifr').style.height = height + 'px';
                            // document.getElementById(id + '_tbl').style.height = (height + 30) + 'px';
                        });
                    },
                });
                var el = $(this).parent().find('.bid-section-input:last-of-type');
                $(el).animate({
                    opacity: 1
                }, 400 );  
            }
            
            

        });
        $(document).on('change', '.total-input', function(e) {
            e.preventDefault();
            calculate_totals();

        });
        $(document).on('click', '.add-mark', function(e) {
            e.preventDefault();
            var parent = $(this).closest('.bid-section-input');
            check_validity(parent);
        });
        $(document).on('click', '.edit-mark', function(e) {
            e.preventDefault();
            var parent = $(this).closest('.bid-section-input');
            parent.find(":input").removeAttr('readonly').css('border','');
            
            var id = parent.find("textarea").attr('id');
            //make tinymce editable
            tinyMCE.get(id).getBody().setAttribute('contenteditable', true);
            $(this).removeClass('edit-mark').addClass('add-mark').text('Add');

        });
        $(document).on('click', '.close-mark', function(e) {
            e.preventDefault();
            var parent = $(this).closest('.bid-section-input');
            parent.attr('style','opacity:0;');

            $(parent).on(
                "transitionend MSTransitionEnd webkitTransitionEnd oTransitionEnd",
                function() {
                    $(this).remove();
                    calculate_totals();
                }
            );

        });
        $(function() {
            $(document).on('click', '.remove-doc', function(e) {
                e.preventDefault();
                var remove = $(this).parents('.attachment-row').first();
                var id = remove.data("id");
                $("#"+id).remove();
                remove.remove();
            });
        });

        function _(el){
            return document.getElementById(el);
        }

        $('#attachment-file').change(function(){
            uploadFile();
        });

        function calculate_totals(){
            var sections = $('.bid-section-input');
            var tot = 0;
            var formated_tot = 0;
            var estimate_input = $('input[name=estimated_price]');
            sections.each(function(){
                var q = $(this).find(".quantity-input").val();
                var u = $(this).find(".unit-price-input").val();
                var t = q * u;
                tot = tot + t;

                t = t.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
                $(this).find(".bid-total").text(t+" Ksh");
                
            });
            if (tot == 0) {
                estimate_input.val('');
            }
            else{
                estimate_input.val(tot);
            }
            
            formated_tot = tot.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
            $('.bid-section-total > p > strong').text(formated_tot+" Ksh");
        }

        function check_validity(parent){
            
            var inputs = parent.find(':input');
            var error = '<p class="error" style="color:#F75B4A; font-size:0.7rem;"> input cannot be empty. </p>';
            var valid = [];
            var needle = 0;
            var is_valid;

            inputs.each(function(){
                if ($(this).is('textarea')) {
                    var id = $(this).attr('id');
                    tinyMCE.triggerSave();
                }
                var val = $(this).val();
                if (!val.replace(/\s/g, '').length) {
                    valid.push(0);
                    if ($(this).siblings('.error').length < 1) {
                        $(error).insertAfter(this);
                    }
                    
                }
                else{
                    if (id) {
                        tinyMCE.get(id).getBody().setAttribute('contenteditable', false);
                    }
                    valid.push(1);
                    $(this).siblings('.error').remove();
                }
            });

            //only add new item if all inputs have been filled
            if (!valid.includes(needle)) {
                parent.find("input").attr('readonly','readonly').attr('style','border:none !important;background-color:#fff');
                parent.find("select").attr('disabled','disabled').attr('style','border:none !important;background-color:#fff');
                parent.find('.add-mark').addClass('edit-mark').removeClass('add-mark').text('Edit');
                is_valid = 1;  
            }
            else{
                is_valid = 0;
            }
            //return response with value showing validity
            return is_valid;
        }
        function uploadFile(){
            $('.submit-btn').unbind('click');
            var file = _("attachment-file").files[0];
            var file_size = bytesToSize(file.size);

            var attachment_container = '<div class=\"small-12 small-centered columns attachment-row ongoing\"><div class=\"doc-container\"><div class=\"small-5 columns\"><div class=\"doc-icon\"><div class=\"doc-icon-container\"><i class=\"fi-page large\"></i></div><span class=\"progress_text\">0%</span><div class=\"doc-loader\"><div></div></div></div></div><div class=\"small-7  columns\"><p class=\"doc-name\">'+file.name+'</p><span class=\"doc-size\">'+file_size+'</span></div><span class=\"remove-doc non-ajax\">&times;</span></div></div>';

            // alert(file.name+" | "+file.size+" | "+file.type);
            $('.attachment-container').append(attachment_container);
            var ongoing = $('.ongoing');

            if (file.size > 5242880) {
                ongoing.find('.doc-name').addClass('red').html('Failed! Max file size is 5MB');
                ongoing.removeClass('ongoing');
                return false;
            }
            ongoing.find('.doc-loader').show();
            var formdata = new FormData();
            formdata.append("file", file);
            $.ajax({
                  xhr: function()
                  {
                    var xhr = new window.XMLHttpRequest();
                    //Upload progress
                    xhr.upload.addEventListener("progress", function(evt){
                      if (evt.lengthComputable) {
                        var percentComplete = Math.round((evt.loaded / evt.total * 100) - 1) +' %';
                        //Do something with upload progress
                        ongoing.find('.progress_text').html(percentComplete);
                        console.log(percentComplete);
                      }
                    }, false);
                    //Download progress
                    xhr.addEventListener("progress", function(evt){
                      if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        //Do something with download progress
                        console.log(percentComplete);
                      }
                    }, false);
                    return xhr;
                  },
                    url: '/fileentry/saveS3',
                    data: formdata,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(data){
                       console.log(data)
                       ongoing.find('.doc-loader').hide();
                       ongoing.find('.progress_text').html('&#10004;');
                       ongoing.data('id', 'file-'+data.id+'');
                       ongoing.removeClass('ongoing');
                       $('#job-bid-form').append('<input type="hidden" id="file-'+data.id+'" name="file-'+data.id+'" value="'+data.id+'"/>');
                       $('.submit-btn').bind('click');
                    },
                    error: function(data){
                        ongoing.find('.doc-loader').hide();
                        ongoing.find('.progress_text').addClass('red').html('&#10004;');
                        ongoing.removeClass('ongoing');
                        $('.submit-btn').bind('click');
                    }
            });
        }
        function bytesToSize(bytes) {
           var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
           if (bytes == 0) return '0 Byte';
           var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
           return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        }


        var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
        functionLoadMCE();

          function functionLoadMCE() {
            if(!isMobile) {
                tinymce.init({
                selector: ".tinymce-text",
                theme: "modern",
                menubar: false,
                plugins: [
                    "advlist autolink lists link charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime  nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern imagetools placeholder"
                ],
                toolbar: " bold italic alignleft aligncenter alignright alignjustify bullist numlist outdent indent preview",
                relative_urls: false,
                image_advtab: true,
                templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
                ]
            });
            }

            else{
                    tinymce.init({
                    selector: ".tinymce-text",
                    theme: "modern",
                    menubar: false,
                    toolbar: false,
                    statusbar:false,
                    relative_urls: false,
                    image_advtab: true,
                    templates: [
                        {title: 'Test template 1', content: 'Test 1'},
                        {title: 'Test template 2', content: 'Test 2'}
                    ]
                });
            } 

            
          }
        
        
     $(document).on('click', '.submit-btn', function(event) {

        event.preventDefault();
        tinyMCE.triggerSave();

        var editorContent = $('#main-text').val();
        console.log('process')
        var sections = $('.bid-section-input');
        var if_valid = [];
        var needle = 0;
        if (sections.length < 1) {
            if ($('.section-error').length < 1) {
                var error = '<p class="section-error" style="color:#F75B4A; font-size:1.3rem;"> Please include your budget breakdown below. </p>';
                $(error).insertAfter('.classif-desc');
            }
            
            return false; 
        }
        //there's an ongoing file upload
        if ($('.ongoing').length > 0) {
            return false;
        }
        //check if all input fields for the breakdown have been filled
        sections.each(function(){
            var valid = check_validity($(this));
            if_valid.push(valid);
        });
        
        //Stop if something's not filled
        if (if_valid.includes(needle)) {
            return false; 
        }

        $('#job-bid-form').parsley({
                errorsWrapper: '<div style="text-align:center;"></div>',
                errorTemplate: '<span style="color:#F75B4A; font-size:0.85rem;"></span>'
        }).validate();

        if (!$('#job-bid-form').parsley().isValid()){
                console.log('invalid')
                    return false;
                    
        }
        if ( editorContent == '' || editorContent == null)
          {
            if ($('.tinyMCE-err').length < 1) {
                $('<span class="tinyMCE-err red error">Please Type in your message</span>').insertAfter($(tinyMCE.activeEditor.getContainer()));  
            }
            return false;
        }

        else{
            console.log('submit')
            $('#job-bid-form').submit();
        }

      });
    $('.open-close').click(function(event){
        event.preventDefault();
        $(this).closest('form').submit();
    });
}

</script>