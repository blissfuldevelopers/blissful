<?php
$status_text = '';
switch ($status) {
    case 1:
        $status_text = 'Shortlist';
        break;
    case 2:
        $status_text = 'Accept';
        break;
    case 3:
        $status_text = 'Decline';
        break;
}
?>
{!! Form::open(['route' => 'bids.store','style'=>'width:100%', 'class'=>'' ]) !!}
{!! Form::hidden('job_id', $job->id) !!}
{!! Form::hidden('id', $bid->id) !!}
{!! Form::hidden('status', $status) !!}
{!! Form::hidden('action', 'change_status') !!}
{!! Form::submit($status_text, ['class' => 'submit-btn tiny round button']) !!}
{!! Form::close() !!}