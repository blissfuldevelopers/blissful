<div class="row">
    <div class="col-sm-12">
        <h1 class="pull-left">Create New Reminder</h1>
    </div>
</div>

@include('core-templates::common.errors')

<div class="row">
    {!! Form::open(['route' => 'reminders.store']) !!}

    @include('reminders.fields')

    {!! Form::close() !!}
</div>
