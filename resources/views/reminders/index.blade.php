@extends('page.zurb-layout')

@section('content')
        <h1 class="pull-left">Reminders</h1>
        <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('reminders.create') !!}">Add New</a>

        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        @include('reminders.table')
        
@endsection