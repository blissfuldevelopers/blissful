<!-- Submit Field -->
<div class="form-group col-sm-12">

    <div class="form-group col-sm-6">
        {!! Form::label('task_name', 'Remind me to:') !!}
        {!! Form::text('task_name', null, ['class' => 'form-control']) !!}
        {!! Form::hidden('task_type', 'Reminder', ['class' => 'form-control']) !!}
    </div>

    <!-- Repeat Stop Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('repeat_start', 'From:') !!}
        {!! Form::date('repeat_start', date('d-m-Y'), ['class' => 'form-control']) !!}
    </div>

    <!-- Repeat Stop Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('repeat', 'Repeat:') !!}
        <?php
        $repeat_array = [
                ''         => 'Never',
                '@daily'   => 'Every Day',
                '@weekly'  => 'Every Week',
                '@monthly' => 'Every Month',
                '@yearly'  => 'Every Year',
        ];
        ?>
        {!! Form::select('repeat', $repeat_array, null, ['placeholder' => 'Is this a recurring reminder?']) !!}
    </div>

    <!-- Repeat Stop Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('repeat_stop', 'End Repeat:') !!}
        {!! Form::date('repeat_stop', null, ['class' => 'form-control']) !!}
    </div>

    {!! Form::submit('Set Reminder', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('reminders.index') !!}" class="btn btn-default">Cancel</a>
</div>
