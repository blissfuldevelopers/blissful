<div class="row">
    <div class="col-sm-12">
        <h1 class="pull-left">Edit Reminder</h1>
    </div>
</div>

@include('core-templates::common.errors')

<div class="row">
    {!! Form::model($reminder, ['route' => ['reminders.update', $reminder->id], 'method' => 'patch']) !!}

    @include('reminders.fields')

    {!! Form::close() !!}
</div>
