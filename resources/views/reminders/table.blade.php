<div class="small-12 columns">
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
            @foreach($reminders as $reminder)
                <tr>
                    @foreach($reminder['attributes'] as $key => $value)
                        <th>{{ $key }}</th>
                    @endforeach
                </tr>
                <?php break; ?>
            @endforeach
            </thead>
            <tbody>
            @foreach($reminders as $reminder)
                <tr>
                    @foreach($reminder['attributes'] as $key => $value)
                        <td>{{ $value }}</td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="small-12 columns">
    @if(is_object($reminders))

        {!! $reminders->render(Foundation::paginate($reminders)) !!}

    @endif
</div>