<!-- Load Bootstrap CSS -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

<div class="container-narrow">
    @if($task)
        <h2>{{$task->title}}</h2>


        <div>
            <h3>{{$task->status? 'Completed' : 'Pending' }} | {{$task->created_at->diffForHumans()}}</h3>
            @else
                <div class="text-center">
                    <h3>No tasks available yet</h3>
                </div>
        @endif
        <!-- Nav tabs -->
            <ul class="nav nav-tabs tabs-4 indigo" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#panel1" role="tab"><i class="fa fa-user"></i>
                        Add Note</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel2" role="tab"><i class="fa fa-envelope"></i>
                        Add Sub-task</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel3" role="tab"><i class="fa fa-heart"></i> Attach
                        File</a>
                </li>
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" data-toggle="tab" href="#panel4" role="tab"><i class="fa fa-envelope"></i>--}}
                        {{--Tag</a>--}}
                {{--</li>--}}
            </ul>

            <!-- Tab panels -->
            <div class="tab-content">

                <!--Panel 1-->
                <div class="tab-pane fade in active" id="panel1" role="tabpanel">
                    <br>
                    {!! Form::model($task, ['method' => 'POST','route' => ['todo.note', $task->id],'class' => 'form-horizontal','files' => true]) !!}
                    <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                        {!! Form::label('description', 'Add Notes: ', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-6">
                            {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
                            {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-3">
                            {!! Form::submit('Add', ['class' => 'submit-btn btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!--/.Panel 1-->
                <!--Panel 2-->
                <div class="tab-pane fade" id="panel2" role="tabpanel">
                    <br>
                    {!! Form::open(['route' => ['todo.subtask'],'class' => 'form-horizontal', 'method' => 'POST','files' => true]) !!}
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                        {!! Form::label('title', 'Add Sub-task: ', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-6">
                            {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
                            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    {!! Form::hidden('parent_id', $task->id, array('id' => 'parent_id')) !!}
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-3">
                            {!! Form::submit('Add', ['class' => 'submit-btn btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="row">
                            <ul>
                            @foreach($subtasks as $subtask)


                                        <li>{{$subtask->title}}</li>

                            @endforeach
                            </ul>
                    </div>
                </div>
                <!--/.Panel 2-->

                <!--Panel 3-->
                <div class="tab-pane fade" id="panel3" role="tabpanel">
                    <br>
                    {!! Form::open(['route' => 'addentry','class' => 'form-horizontal', 'method' => 'post','files' => true,'enctype'=>'multipart/form-data']) !!}
                    <div class="form-group {{ $errors->has('file') ? 'has-error' : ''}}">
                        {!! Form::label('file', 'Attach File: ', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-6">
                            <input type="file" name="filefield" class="form-control">
                            {!! $errors->first('file', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-3">
                            {!! Form::submit('Upload', ['class' => 'submit-btn btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}

                    <div class="row">
                        <ul class="thumbnails">
                            @foreach($entries as $entry)
                                <div class="col-md-2">
                                    <div class="thumbnail">
                                        <img src="{{route('getentry', $entry->filename)}}" alt="ALT NAME"
                                             class="img-responsive"/>
                                        <div class="caption">
                                            <p>
                                                <a href="/fileentry/get/{{$entry->filename}}">{{$entry->original_filename}}</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <!--/.Panel 4-->

                <!--Panel 4-->
            <!--ToDO: share note via email && create account for new user tagged -->
                {{--<div class="tab-pane fade" id="panel4" role="tabpanel">--}}
                    {{--<br>--}}

                    {{--<p></p>--}}

                {{--</div>--}}
                <!--/.Panel 4-->

            </div>
            <!-- Modal-->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Task Editor</h4>
                        </div>
                        <div class="modal-body">
                            <form id="frmTasks" name="frmTasks" class="form-horizontal" novalidate="">

                                <div class="form-group error">
                                    <label for="inputTask" class="col-sm-3 control-label">Title</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control has-error" id="title" name="title"
                                               placeholder="Title" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Description</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="description" name="description"
                                               placeholder="Description" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputReminder" class="col-sm-3 control-label">Reminder</label>
                                    <div class="col-sm-9">
                                        <input type="datetime-local" class="form-control" id="reminder" name="reminder"
                                               placeholder="Reminder" value="">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes
                            </button>
                            <input type="hidden" id="task_id" name="task_id" value="0">
                        </div>
                    </div>
                </div>
            </div>
        </div>

</div>
<meta name="_token" content="{!! csrf_token() !!}"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        var url = "/todo";

        //display modal form for task editing
        $('.open-modal').click(function () {
            var task_id = $(this).val();

            $.get(url + '/' + task_id, function (data) {
                //success data
                console.log(data);
                $('#task_id').val(data.id);
                $('#title').val(data.title);
                $('#description').val(data.description);
                $('#reminder').val(data.reminder);
                $('#btn-save').val("update");

                $('#myModal').modal('show');
            })
        });

        //display modal form for creating new task
        $('#btn-add').click(function () {
            $('#btn-save').val("add");
            $('#frmTasks').trigger("reset");
            $('#myModal').modal('show');
        });

        //delete task and remove it from list
        $('.delete-task').click(function () {
            var task_id = $(this).val();

            $.ajax({

                type: "DELETE",
                url: url + '/' + task_id,
                success: function (data) {
                    console.log(data);

                    $("#task" + task_id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        //create new task / update existing task
        $("#btn-save").click(function (e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            e.preventDefault();

            var formData = {
                title: $('#title').val(),
                description: $('#description').val(),
                reminder: $('#reminder').val(),
            }

            //to determine [add=POST] or [update=PUT]
            var state = $('#btn-save').val();

            var type = "POST"; //for creating new record
            var task_id = $('#task_id').val();
            ;
            var my_url = url;

            if (state == "update") {
                type = "PATCH"; //for updating existing record
                my_url += '/' + task_id;
            }

            console.log(formData);

            $.ajax({

                type: type,
                url: my_url,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);

                    var task = '<tr id="task' + data.id + '"><td>' + data.id + '</td><td>' + data.title + '</td><td>' + data.description + '</td><td>' + data.status + '</td><td>' + data.created_at + '</td>';
                    task += '<td><button class="btn btn-warning btn-xs btn-detail open-modal" value="' + data.id + '">Edit</button>';
                    task += '<button class="btn btn-danger btn-xs btn-delete delete-task" value="' + data.id + '">Delete</button></td></tr>';

                    if (state == "add") { //if user added a new record
                        $('#tasks-list').append(task);
                    } else { //if user updated an existing record

                        $("#task" + task_id).replaceWith(task);
                    }

                    $('#frmTasks').trigger("reset");

                    $('#myModal').modal('hide')
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
    });
</script>