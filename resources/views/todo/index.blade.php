<!-- Load Bootstrap CSS -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

<div class="container-narrow">
    <h2>My ToDo List</h2>
    <button id="btn-add" name="btn-add" class="btn btn-primary btn-xs">Add New Task</button>

    @if(count($todos))
        <div>

            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    {{--<th>Completed</th>--}}
                    <th>Created</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody id="tasks-list" name="tasks-list">

                @foreach ($todos as $task)
                    <tr id="task{{$task->id}}">
                        <td>{{$task->id}}</td>
                        <td>{!! link_to_route('todo.view', ($task->title), array($task->id))!!}</td>
                        {{--<td>{{$task->status? 'Completed' : 'Pending' }}</td>--}}
                        <td>{{$task->created_at->diffForHumans()}}</td>
                        <td>
                            {!! Form::open(['route' => ['todo.status', $task->id], 'method' => 'POST']) !!}
                            @if($task->status)
                                {!!  Form::hidden('title', $task->title) !!}
                                {!! Form::hidden('description', $task->description) !!}
                                {!! Form::submit('Incomplete', ['class' => 'btn btn-info btn-xs']) !!}
                            @else
                                {!!  Form::hidden('title', $task->title) !!}
                                {!! Form::hidden('description', $task->description) !!}
                                {!! Form::submit('Complete', ['class' => 'btn btn-success btn-xs']) !!}
                            @endif
                            {!! Form::close() !!}

                            <button class="btn btn-warning btn-xs btn-detail open-modal" value="{{$task->id}}">Edit
                            </button>
                            <button class="btn btn-danger btn-xs btn-delete delete-task" value="{{$task->id}}">Delete
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-center">
                {!! $todos->render() !!}
            </div>
            @else
                <div class="text-center">
                    <h3>No tasks available yet</h3>
                </div>
        @endif
        <!-- Modal-->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Task Editor</h4>
                        </div>
                        <div class="modal-body">
                            <form id="frmTasks" name="frmTasks" class="form-horizontal" novalidate="">

                                <div class="form-group error">
                                    <label for="inputTask" class="col-sm-3 control-label">Title</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control has-error" id="title" name="title"
                                               placeholder="Title" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputReminder" class="col-sm-3 control-label">Reminder</label>
                                    <div class="col-sm-9">
                                        <input type="datetime-local" class="form-control" id="reminder" name="reminder"
                                               placeholder="Reminder" value="">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes
                            </button>
                            <input type="hidden" id="task_id" name="task_id" value="0">
                        </div>
                    </div>
                </div>
            </div>
        </div>

</div>
<meta name="_token" content="{!! csrf_token() !!}"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        var url = "/user/todo";

        //display modal form for task editing
        $('.open-modal').click(function () {
            var task_id = $(this).val();

            $.get(url + '/' + task_id, function (data) {
                //success data
                console.log(data);
                $('#task_id').val(data.id);
                $('#title').val(data.title);
                $('#reminder').val(data.reminder);
                $('#btn-save').val("update");

                $('#myModal').modal('show');
            })
        });

        //display modal form for creating new task
        $('#btn-add').click(function () {
            $('#btn-save').val("add");
            $('#frmTasks').trigger("reset");
            $('#myModal').modal('show');
        });

        //delete task and remove it from list
        $('.delete-task').click(function () {
            var task_id = $(this).val();

            $.ajax({

                type: "DELETE",
                url: url + '/' + task_id,
                success: function (data) {
                    console.log(data);

                    $("#task" + task_id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        //create new task / update existing task
        $("#btn-save").click(function (e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            e.preventDefault();

            var formData = {
                title: $('#title').val(),
                description: $('#description').val(),
                reminder: $('#reminder').val(),
            }

            //to determine [add=POST] or [update=PUT]
            var state = $('#btn-save').val();

            var type = "POST"; //for creating new record
            var task_id = $('#task_id').val();
            ;
            var my_url = url;

            if (state == "update") {
                type = "PATCH"; //for updating existing record
                my_url += '/' + task_id;
            }

            console.log(formData);

            $.ajax({

                type: type,
                url: my_url,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);

                    var task = '<tr id="task' + data.id + '"><td>' + data.id + '</td><td>' + data.title + '</td><td>' + data.created_at + '</td>';
                    task += '<td><button class="btn btn-warning btn-xs btn-detail open-modal" value="' + data.id + '">Edit</button>';
                    task += '<button class="btn btn-danger btn-xs btn-delete delete-task" value="' + data.id + '">Delete</button></td></tr>';

                    if (state == "add") { //if user added a new record
                        $('#tasks-list').append(task);
                    } else { //if user updated an existing record

                        $("#task" + task_id).replaceWith(task);
                    }

                    $('#frmTasks').trigger("reset");

                    $('#myModal').modal('hide')
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
    });
</script>