<!DOCTYPE html>
<html lang="en" data-ng-app="Auth" ng-controller="AppCtrl">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<link rel="shortcut icon" href="/app/client/images/favicon.ico">-->

        <title>BlisFul</title>

        <!-- ========== Css Files ========== -->
        <link href="/app/common/css/root.css" rel="stylesheet" type="text/css"/>

        <!--Css for notification-->
        <link href="/app/common/css/ns-default.css" rel="stylesheet"/>
        <link href="/app/common/css/ns-style-attached.css" rel="stylesheet"/>

        <style type="text/css">
            body{background: #F5F5F5;}
        </style>

    </head>
    <body>

        <!-- START CONTENT -->
        <div ui-view=""></div>
        <!-- End Content -->


        <script src="/app/common/js/jquery.min.js"></script>
        <!-- Notification Js -->
        <script src="/app/common/js/modernizr.custom.js"></script>
        <script src="/app/common/js/classie.js"></script>
        <script src="/app/common/js/notificationFx.js"></script>

        <!-- Angular File-->
        <script src="/app/common/bower_components/angular/angular.min.js" type="text/javascript"></script>
        <script src="/app/common/bower_components/angular-ui-router/release/angular-ui-router.min.js" type="text/javascript"></script>

        <script src="/app/common/partials/auth/index.js" type="text/javascript"></script>

    </body>
</html>
