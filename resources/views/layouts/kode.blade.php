<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="/img/dashboard/favicon.ico" />

    <title>Blissful | @yield('title')</title>
    <link rel="stylesheet" href="/css/dashboard/root.css"/>
    <link rel="stylesheet" href="/css/dashboard/dashes.css"/>
    @yield('styles')
            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

@yield('head')
<body>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-50669449-1', 'auto');
    ga('send', 'pageview');

</script>
<?php
$count = Auth::user()->newMessagesCount();
$cssClass = $count == 0 ? 'hidden' : '';
?>
        <!-- Start Page Loading -->
<div class="loading"><img src="/img/dashboard/loading.gif" alt="loading-img"></div>
<!-- End Page Loading -->
<!-- //////////////////////////////////////////////////////////////////////////// -->
<!-- START TOP -->
<div id="top" class="clearfix white_background materialize">

    <!-- Start App Logo -->
    <div class="applogo white_background">
        <a href="{{ route('home')  }}" class="logo"><img src="/img/dashboard/zurb/blissful-logo-blue.png"></a>
    </div>
    <!-- End App Logo -->

    <!-- Start Sidebar Show Hide Button -->
    <a href="#" class="sidebar-open-button"><i class="fa fa-bars"></i></a>
    <a href="#" class="sidebar-open-button-mobile"><i class="fa fa-bars"></i></a>
    <!-- End Sidebar Show Hide Button -->

    <!-- Start Searchbox -->
    <form class="searchform" action="/search" method="GET" enctype="multipart/form-data">
        <input type="text" name="find" class="searchbox" id="searchbox" placeholder="Find a Vendor">
        <span class="searchbutton"><i class="fa fa-search"></i></span>
    </form>
    <!-- End Searchbox -->

    <!-- Start Top Right -->
    <ul class="top-right">
        @if( Auth::user()->hasRole('vendor'))
            @if(isset($balance))
                <li class="link">
                    <a class="color5-bg notifications ">Credit Balance: {{$balance}}</a>
                </li>
            @endif
            <li class="dropdown link">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle hdbutton">Create New <span
                            class="caret"></span></a>
                <ul class="dropdown-menu dropdown-menu-list">
                    <!-- <li><a href="#"><i class="fa falist fa-paper-plane-o"></i>Product or Item</a></li> -->
                    <li><a href="gallery/list"><i class="fa falist fa-file-image-o"></i>Image Gallery</a></li>
                </ul>
            </li>
            @endif
                    <!--
            <li class="link">
              <a href="#" class="notifications">6</a>
            </li> -->

            <li class="dropdown link">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle profilebox">
                    <img src="/{{Auth::user()->profile->profilePic}}" alt="img">
                    <b>{{Auth::user()->first_name}}</b>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-list dropdown-menu-right">
                    <li role="presentation" class="dropdown-header"><a href="
                        @if( Auth::user()->hasRole('administrator'))
                                /admin
                                @elseif( Auth::user()->hasRole('vendor'))
                                /vndor
                                @elseif( Auth::user()->hasRole('user'))
                                /user
                                @endif"><i class="fa falist fa-home"></i>Dashboard</a>
                    </li>
                    <li><a href="@if( Auth::user()->hasRole('vendor'))
                        {{route('users.edit',array(Auth::user()->profile->user_id))}}
                        @else
                        {{ url('profile/edit/'.Auth::user()->profile->user_id) }}
                        @endif
                                "><i class="fa falist fa-wrench"></i>Profile</a>
                    </li>
                    <li><a href="@if( Auth::user()->hasRole('administrator'))
                                /admin#inbox
                                @elseif( Auth::user()->hasRole('vendor'))
                                /vndor#inbox
                                @elseif( Auth::user()->hasRole('user'))
                                /user#inbox
                                @endif"><i class="fa falist fa-inbox"></i>Inbox<span
                                    class="badge label-danger {{$cssClass}}">{{$count}}</span></a>
                    </li>
                    @if( Auth::user()->hasRole('vendor'))
                        <li><a href="/vndor#jobs-board"><i class="fa falist fa-briefcase"></i>Jobs</a>
                    @elseif( Auth::user()->hasRole('user'))
                        <li><a href="/user#jobs-board"><i class="fa falist fa-briefcase"></i>Jobs</a>
                    @endif

                    <li class="divider"></li>
                    <li><a href="{{ route('authenticated.logout') }}"><i class="fa falist fa-power-off"
                                                                         style="color:red;"></i> Logout</a></li>
                </ul>
            </li>

    </ul>
    <!-- End Top Right -->

</div>
<!-- END TOP -->
@yield('content')
@yield('footer')
        <!-- ================================================
        jQuery Library
        ================================================ -->
<script type="text/javascript" src="/js/dashboard/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="/js/dashboard/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="/js/dashboard/plugins.js"></script>

<!-- ================================================
Bootstrap Select
================================================ -->
<script type="text/javascript" src="/js/dashboard/bootstrap-select/bootstrap-select.js"></script>

<!-- ================================================
Bootstrap Toggle
================================================ -->
<script type="text/javascript" src="/js/dashboard/bootstrap-toggle/bootstrap-toggle.min.js"></script>

<!-- ================================================
Kode Alert
================================================ -->
<script src="/js/dashboard/kode-alert/main.js"></script>

<!-- ================================================
jQuery UI
================================================ -->
<script type="text/javascript" src="/js/dashboard/jquery-ui/jquery-ui.min.js"></script>

<!-- ================================================
Below codes are only for index widgets
================================================ -->
<!-- Today Sales -->
<script type="text/javascript">
    $(document).ready(function () {
        var hash = window.location.hash;
        hash && $('ul.nav a[href="' + hash + '"]').tab('show');
    });
</script>
@yield('scripts')
</body>
</head>