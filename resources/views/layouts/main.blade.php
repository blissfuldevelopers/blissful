<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Blissful | @yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/material.css" rel="stylesheet">
    <link href="/css/ripples.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"/>
    <link type="text/css" rel="stylesheet" href="/css/custom.css"/>
    @yield('styles')
            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    @yield('head')

</head>

<body>


<div class="navbar navbar-default white_background grey">
    <div class="header_row">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target=".navbar-responsive-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('home')  }}"><img class="img-responsive" src="/img/logo-pink.png"
                                                                     alt="Blissful"
                                                                     style="max-width:100px; margin-top: -8px;"/></a>
        </div>
        <div class="navbar-collapse collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav navbar-right">
                @if(!Auth::check())
                    <li><a class="btn btn-sm white green_background " href="{{ route('auth.user-type') }}">Join Us</a>
                    </li>
                @else
                    <?php
                    $count = Auth::user()->newMessagesCount();
                    $cssClass = $count == 0 ? 'hidden' : '';
                    ?>
                    <li class="dropdown">
                        <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle"
                           data-toggle="dropdown"><img src="/{{Auth::user()->profile->profilePic}}"
                                                       class="img-circle special-img"> <span
                                    class="grey">{{Auth::user()->first_name}}</span> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li role="presentation" class="dropdown-header"><a href="
                        @if( Auth::user()->hasRole('administrator'))
                                        /admin
                                        @elseif( Auth::user()->hasRole('vendor'))
                                        /vndor
                                        @elseif( Auth::user()->hasRole('user'))
                                        /user
                                        @endif"><i class="fa falist fa-home"></i> Dashboard</a>
                            </li>
                            <li><a href="@if( Auth::user()->hasRole('vendor'))
                                {{route('users.edit',array(Auth::user()->profile->user_id))}}
                                @else
                                {{ url('profile/edit/'.Auth::user()->profile->user_id) }}
                                @endif
                                        "><i class="fa falist fa-wrench"></i> Profile</a>
                            </li>
                            <li><a href="@if( Auth::user()->hasRole('administrator'))
                                        /admin#inbox
                                        @elseif( Auth::user()->hasRole('vendor'))
                                        /vndor#inbox
                                        @elseif( Auth::user()->hasRole('user'))
                                        /user#inbox
                                        @endif"><i class="fa falist fa-inbox"></i> Inbox <span
                                            class="badge label-danger {{$cssClass}}">{{$count}}</span></a>
                            </li>
                            @if( Auth::user()->hasRole('vendor'))
                                <li><a href="/vndor#jobs-board"><i class="fa falist fa-briefcase"></i>Jobs</a>
                            @elseif( Auth::user()->hasRole('user'))
                                <li><a href="/user#jobs-board"><i class="fa falist fa-briefcase"></i>Jobs</a>
                            @endif
                            <li><a href="{{ route('authenticated.logout') }}"><i class="fa falist fa-power-off"
                                                                                 style="color:red;"></i> Logout</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>

<div class="container">

    @yield('content')

</div> <!-- /container -->


{!! HTML::script('/assets/js/ie10-viewport-bug-workaround.js') !!}
        <!-- Scripts -->

<script src="/js/vendor/jquery.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="/js/ripples.min.js"></script>
<script src="/js/material.min.js"></script>
<script>
    $(document).ready(function () {
        // This command is used to initialize some elements and make them work properly
        $.material.init();
    });
</script>
{!! HTML::script ('css/rating.css')!!}
{!! HTML::script ('js/rating.js')!!}
        <!-- Start Messenger Demo Changes -->
@yield('footer')
{{--<script src="{{ asset('/js/all.js') }}" type="text/javascript"></script>--}}
{{--@if(Auth::check())--}}
{{--<script src="//js.pusher.com/2.2/pusher.min.js" type="text/javascript"></script>--}}
{{--<script type="text/javascript">--}}
{{--var pusher = new Pusher('{{Config::get('pusher.appKey')}}');--}}
{{--var channel = pusher.subscribe('for_user_{{Auth::id()}}');--}}
{{--channel.bind('new_message', function(data) {--}}
{{--var thread = $('#' + data.div_id);--}}
{{--var thread_id = data.thread_id;--}}
{{--var thread_plain_text = data.text;--}}

{{--if (thread.length) {--}}
{{--// add new message to thread--}}
{{--thread.append(data.html);--}}

{{--// make sure the thread is set to read--}}
{{--$.ajax({--}}
{{--url: "/messages/" + thread_id + "/read"--}}
{{--});--}}
{{--} else {--}}
{{--var message = '<p>' + data.sender_name + ' said: ' + data.text + '</p><p><a href="' + data.thread_url + '">View Message</a></p>';--}}

{{--// notify the user--}}
{{--$.growl.notice({ title: data.thread_subject, message: message });--}}

{{--// set unread count--}}
{{--$.ajax({--}}
{{--url: "{{route('messages.unread')}}"--}}
{{--}).success(function( data ) {--}}
{{--var div = $('#unread_messages');--}}

{{--var count = data.msg_count;--}}
{{--if (count == 0) {--}}
{{--$(div).addClass('hidden');--}}
{{--} else {--}}
{{--$(div).text(count).removeClass('hidden');--}}

{{--// if on messages.index - add alert class and update latest message--}}
{{--$('#thread_list_' + thread_id).addClass('alert-info');--}}
{{--$('#thread_list_' + thread_id + '_text').html(thread_plain_text);--}}
{{--}--}}
{{--});--}}
{{--}--}}
{{--});--}}
{{--</script>--}}
{{--@endif--}}
@yield('scripts')
</body>
</html>
