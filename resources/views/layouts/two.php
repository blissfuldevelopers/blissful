@if(!Auth::check())
<li><a class="btn btn-sm " href="{{ route('auth.vendor-register') }}">List Your Business</a></li>
<li><a href="{{ route('auth.login') }}">Login</a></li>
<li><a href="{{ route('auth.register') }}">Register</a></li>
@else
<!-- <li><a href="{{ route('profile') }}">Your Profile</a></li>
<li><a href="{{ route('messages') }}">Messages @include('messenger.unread-count')</a></li>
<li><a href="{{ route('messages.create') }}">+New Message</a></li>
<li><a href="{{ route('authenticated.logout') }}">Logout</a></li> -->

<li class="dropdown">
    <a href="#" class="dropdown-toggle profile-image" data-toggle="dropdown-menu">
        <img src="http://placehold.it/30x30" class="img-circle special-img"> Test <b class="caret"></b>
    </a>
    <ul class="dropdown-menu">
        <li><a href="#"><i class="fa fa-cog"></i> Account</a></li>
        <li class="divider"></li>
        <li><a href="#"><i class="fa fa-sign-out"></i> Sign-out</a></li>
    </ul>
</li>
@endif