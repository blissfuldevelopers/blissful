@extends('page.home-layout')
@section('title')
    Quotations
@endsection
@section('styles')

@endsection
@section('header-content')
  <?php
    $header_class = 'vendorpg-intro';
  ?>
@endsection
@section('content')
<div class="row">
	<div class="small-12 medium-10 columns small-centered show-jobs-section breaker">
        @include(@$content)
    </div>
</div>
<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer ></script>
<script type="text/javascript">
    var CaptchaCallback = function() {
        console.log('called')
        grecaptcha.render('formCaptcha', {'sitekey' : '{{ env('RE_CAP_SITE') }}'});
        grecaptcha.render('feedbackCaptcha', {'sitekey' : '{{ env('RE_CAP_SITE') }}'});
    }; 
</script>
@endsection