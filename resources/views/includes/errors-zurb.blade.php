@if(Session::has('errorsZurb'))
<div data-alert class="alert-box alert-box-messages {{ Session::get('status') }}">
    <strong>Sorry <i class="fi-info sm"></i></strong> - {{ Session::get('errorsZurb') }}
    @if(Session::get('errors'))
             there were errors while submitting your message:
            @foreach($errors->all(":message") as $message)
                <li>
                    {{$message}}
                </li>
            @endforeach
    @endif
    <a href="#" class="close">&times;</a>
</div>
@endif