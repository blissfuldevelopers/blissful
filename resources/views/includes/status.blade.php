@if(Session::has('message'))
<div data-alert data-closable class="callout {{ Session::get('status') }}">
  	<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
    	<span aria-hidden="true">&times;</span>
 	</button>
  	{{ Session::get('message') }}
  	@if ($errors->any())
		<ul>
			{!!  implode('', $errors->all('<li class="error small"> :message</li>')) !!}
		</ul>
	@endif
</div>
@elseif(Session::has('flash_message'))
<div data-alert data-closable class="callout {{ Session::get('status') }}">
  	<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
    	<span aria-hidden="true">&times;</span>
  	</button>
  	{{ Session::get('flash_message') }}
  	@if ($errors->any())
		<ul>
			{!!  implode('', $errors->all('<li class="error small"> :message</li>')) !!}
		</ul>
	@endif
</div>
@endif