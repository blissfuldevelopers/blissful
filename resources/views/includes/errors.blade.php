@if($errors->has())
<div data-alert class="alert-box alert-box-messages alert">
		<span class="ns">Following errors occurred</span>
        <ul class="ns alert-box-messages">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
		<a href="#" class="close">&times;</a>
</div>
@endif