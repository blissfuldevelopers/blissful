<div style="margin-bottom:10px">
    @if(Session::has('messageZurb'))
        <div data-alert data-closable class="callout {{ Session::get('status') }}">
          <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
            <span aria-hidden="true">&times;</span>
          </button>
          {{ Session::get('messageZurb') }}
        </div>
    @endif

    @if(Session::has('errorsZurb'))
        <div data-alert data-closable class="callout {{ Session::get('status') }}">
          <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
            <span aria-hidden="true">&times;</span>
          </button>
          {{ Session::get('errorsZurb') }}
        </div>
    @endif
</div>