<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard | Blissful</title>

    <!-- google ads -->
    <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
    <script>
        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];
    </script>

    <script>
        googletag.cmd.push(function () {
            googletag.defineSlot('/189532048/leaderboard_top_home', [728, 90], 'div-gpt-ad-1474450779427-0').addService(googletag.pubads());
            googletag.defineSlot('/189532048/Square_1', [300, 250], 'div-gpt-ad-1474450779427-1').addService(googletag.pubads());
            googletag.defineSlot('/189532048/Half_Page_1', [300, 600], 'div-gpt-ad-1474450779427-2').addService(googletag.pubads());
            googletag.defineSlot('/189532048/Mobile_Leaderboard_Top', [320, 50], 'div-gpt-ad-1474450779427-3').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
    </script>

    <link rel="icon" type="image/png"
          href="https://s3.eu-central-1.amazonaws.com/blissful-ke/img/dashboard/favicon.ico">
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/home-scripts.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/bliss-custom.css') }}">
</head>
<body>
<div class="wrapper vendor" id="dashboard">

    <!-- masthead -->
    <div class="dheader">

        <!-- main navigation -->
        <nav>
            <div class="row">
                <div class="small-12 columns">
                    <div class="small-6 columns">
                        <!-- logo -->
                        <h1 class="logo">
                            <a href="/">
                                <img src="/img/blissful-logo-w.svg">
                            </a>
                        </h1>
                    </div>
                    <div class="small-6 columns">
                        <a href="/shop/cart" class="show-cart">
                            <span class="shopping-cart"><i class="fi-shopping-cart"></i></span>
                            <span id="cart-count" class="badge cart-count">0</span>
                        </a>
                        <!-- nav links -->
                        <ul class="dropdown menu hide-for-small-only" data-dropdown-menu>
                            <li class="header-dropdown">
                                <a href="#">
                                    <img class="round"
                                         src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{Auth::user()->profile->profilePic}}"> {{Auth::user()->first_name}}
                                </a>
                                <ul class="menu">
                                    <li><a href="/dashboard">Dashboard</a></li>
                                    <li><a href="{{route('users.show',[Auth::user()->id])}}">Profile</a></li>
                                    <li><a href="/logout">Logout</a></li>
                                    <!-- ... -->
                                </ul>
                            </li>
                        </ul>
                        <a id="mobile-menu-open" class="mobile-menu-open show-for-small-only">
                            <i class="fi-list large"></i>
                        </a>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <!-- start side nav -->
    <div class="mobile-nav-back" id="mobile-bg"></div>
    <div class="mobile-nav" id="mobile-menu">
        <a href="#" id="mobile-menu-close" class="menu-close"><i class="fi-x"></i></a>
        <div class="small-12 columns">
            @if(Auth::check())
                <div class="small-12 columns text-center">
                    <img class="circle"
                         src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{Auth::user()->profile->profilePic}}">
                    <p>{{Auth::user()->first_name}} {{Auth::user()->last_name}}</p>
                </div>
                <div class="show-for-small-only small-12 columns">
                    <form action="/search" method="GET" enctype="multipart/form-data" class="search-form">
                        <input name="find" id="find" type="text"
                               class="search-input @if(Session::has('searchErrors')) red_border @endif"
                               placeholder="@if(Session::has('searchErrors'))Search not found @else Find a Vendor @endif"/>
                    </form>
                </div>
                <div class="small-12 columns">
                    <ul>
                        <li><a href="/dashboard">Dashboard</a></li>
                        <li><a href="{{route('users.show',[Auth::user()->id])}}">Profile</a></li>
                        <li><a href="/logout">Logout</a></li>
                        <!-- ... -->
                    </ul>
                </div>

            @else
                <div class="small-12 columns text-center">
                    <img src="/img/badge-inverted.svg">
                </div>
                <div class="show-for-small-only small-12 columns">
                    <form action="/search" method="GET" enctype="multipart/form-data" class="search-form">
                        <input name="find" id="find" type="text"
                               class="search-input @if(Session::has('searchErrors')) red_border @endif"
                               placeholder="@if(Session::has('searchErrors'))Search not found @else Find a Vendor @endif"/>
                    </form>
                </div>
                <ul>
                    <li><a href="/register/user-type">Sign Up</a></li>
                    <li><a href="/login">Log In</a></li>
                </ul>
            @endif
        </div>
    </div>
    <!-- end side nav -->
    <!-- dashboard main -->
    <div class="dashboard-main">
        <header>
            <div class="row">
                <div class="small-12 medium-6 columns">
                    <h3>Dashboard</h3><!--
              <span><a href="#" class="button">Switch to User</a></span> -->
                </div>
                <div class="medium-6 columns hide-for-small-only">
                    <ul>
                        @if(Auth::user()->hasUserRole('vendor'))
                            <li><a href="/jobs/all">Jobs</a></li>
                        @else
                            <li><a href="/jobs/mine">Quotation Requests</a></li>
                    @endif
                    <!--
                <li><a href="/quotations">Quotes</a></li>
                <li><a href="/bookings">Bookings</a></li> -->
                    </ul>
                </div>
            </div>
        </header>
        <section class="wrapper">
            <!-- Adsense Section 1-->
            <div class="row">
                <div class="small-12 columns breaker">
                    <div class="hide-for-small-only">
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Blissful_responsive -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-2847929148248604"
                             data-ad-slot="1315972373"
                             data-ad-format="auto"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                    <div class="show-for-small-only">
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- mobile_horizontal banner -->
                        <ins class="adsbygoogle"
                             style="display:inline-block;width:320px;height:100px"
                             data-ad-client="ca-pub-2847929148248604"
                             data-ad-slot="2792705573"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                </div>
            </div>
            <!-- stats summary -->
            <div class="row">
                <div class="small-12 columns">
                    <ul class="sum-dstats">
                        @if(Auth::user()->hasUserRole('vendor'))
                            <li>
                                <div class="small-7 medium-12 columns">
                                    <h5>NEW JOBS</h5>
                                </div>
                                <div class="small-5 medium-12 columns">
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                    <h2 class="new-jobs"></h2>
                                </div>
                            </li>
                            <li>
                                <div class="small-7 medium-12 columns">
                                    <h5>CREDIT BALANCE</h5>
                                </div>
                                <div class="small-5 medium-12 columns">
                                    <i class="fa fa-credit-card-alt" aria-hidden="true"></i>
                                    <h2 class="credits-balance"></h2>
                                    <a href="/shop/category/blissful-credits">Top up</a>
                                </div>
                            </li>
                            <li>
                                <div class="small-7 medium-12 columns">
                                    <h5>NEW RESPONSES</h5>
                                </div>
                                <div class="small-5 medium-12 columns">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    <h2 class="new-messages"></h2>
                                    <a href="/messages"> View Responses</a>
                                </div>
                            </li>
                        @else
                            <li>
                                <div class="small-7 medium-12 columns">
                                    <h5>DAYS TO EVENT</h5>
                                </div>
                                <div class="small-5 medium-12 columns">
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                    <h2 class="event-countdown"></h2>
                                </div>
                            </li>
                            <li>
                                <div class="small-7 medium-12 columns">
                                    <h5>ACTIVE PROPOSALS</h5>
                                </div>
                                <div class="small-5 medium-12 columns">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                    <h2 class="active-proposals"></h2>
                                </div>
                            </li>
                            <li>
                                <div class="small-7 medium-12 columns">
                                    <h5>QUOTATIONS RECEIVED</h5>
                                </div>
                                <div class="small-5 medium-12 columns">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    <h2 class="quotations-received"></h2>
                                </div>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>

          <!-- quotes -->
          <div class="row">
            <div class="small-12 medium-4 columns">
            @if(!Auth::user()->hasUserRole('vendor'))
              <div class="sum-dquotes">
                <table>
                  <thead>
                    <tr>
                      <th>Bookmarks</th>
                      <th class="hide-for-small-only"> </th>
                    </tr>
                  </thead>
                  <tbody class="bookmarks-body">
                      <tr>
                        <td></td>
                        <td><div class="section-loader"></div></td>
                      </tr>
                  </tbody>
                </table>
              </div>
            @else
              <div class="cal-wrapper">
                <iframe src="https://calendar.google.com/calendar/embed?src=bf73mccun4vcaqpe4jkn7h86mo%40group.calendar.google.com&ctz=Africa/Nairobi" style="border: 0" width="100%" height="100%" frameborder="0" scrolling="no"></iframe>
              </div>
            @endif
            </div>
            <div class="small-12 medium-8 columns">
              <div class="sum-dquotes">
                <table>
                  <thead>
                    @if(Auth::user()->hasUserRole('vendor'))
                    <th class="title">New Requests</th>
                    @else
                    <th class="title">Top Quotes</th>
                    @endif
                    <th class="hide-for-small-only"> <a href="/jobs/all"> View All</a></th>
                  </thead>
                  <tbody class="jobs-body">
                      <tr>
                        <td></td>
                        <td><div class="section-loader"></div></td>
                      </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          @if(Auth::user()->hasUserRole('user'))
          <!-- brand msg -->
          <div class="row">
            <div class="small-12 columns">
              <div class="brd-dmsg">
                <img src="/img/flower.jpg">
                <h5>Book a venue today</h5>
                <a href="/venue" class="button">Get Started</a>
              </div>
            </div>
          </div>
          @elseif(Auth::user()->hasUserRole('vendor'))
          <!-- brand msg -->
          <div class="row">
            <div class="small-12 columns">
              <div class="brd-dmsg">
                <img src="/img/bliss-credits.jpg">
                <h5>Need to top up?</h5>
                <a href="/shop/category/blissful-credits" class="button">Purchase Credits</a>
              </div>
            </div>
          </div>
          @endif
        <!-- Adsense Section 2-->
            <div class="row">
                <div class="small-12 columns">
                    <div class="hide-for-small-only">
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Blissful_responsive -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-2847929148248604"
                             data-ad-slot="1315972373"
                             data-ad-format="auto"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                    <div class="show-for-small-only">
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- mobile_horizontal banner -->
                        <ins class="adsbygoogle"
                             style="display:inline-block;width:320px;height:100px"
                             data-ad-client="ca-pub-2847929148248604"
                             data-ad-slot="2792705573"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<!-- footer -->
<footer>
    <div class="f1">
        <div class="row">
            <div class="medium-3 columns hide-for-small-only">
                <img src="/img/blissful-logo-w.svg" class="f-logo">
            </div>
            <div class="medium-5 small-12 columns">
                <div class="row">
                    <div class="small-6 columns">
                        <ul>
                            <li><a href="/about">About</a></li>
                            <li><a href="/how-it-works">How It Works</a></li>
                            <li><a href="/contact">Contact Us</a></li>
                            <li><a href="/terms-and-conditions">Terms & Conditions</a></li>
                            <li><a href="/privacy-policy">Privacy Policy</a></li>
                        </ul>
                    </div>
                    <div class="small-6 columns">
                        <ul>
                            <!-- <li>Jobs</li>
                            <li>Messages</li>
                            <li>Bids</li>
                            <li>Credits</li> -->
                            <li><a href="/blog">Blog</a></li>
                            <li><a href="/shop">Shop</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="medium-4 small-12 columns">
                <ul class="socials">
                    <li>
                        <a target="_blank" href="https://www.facebook.com/blissfulKE">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="https://twitter.com/blissfulKE">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="https://www.instagram.com/blissful_ke/">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="https://www.pinterest.com/blissfulKE">
                            <i class="fa fa-pinterest" aria-hidden="true"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="f2">
        <div class="row">
            <div class="small-12 columns">
                <p>©2017 Copyright</p>
            </div>
        </div>
    </div>
</footer>
<!--Start of Crisp Live Chat Script-->
<script type="text/javascript">

    CRISP_WEBSITE_ID = "aeb94ef1-11bf-4618-85b7-dce7f0ee2b92";
    (function () {
        d = document;
        s = d.createElement("script");
        s.src = "https://client.crisp.im/l.js";
        s.async = 1;
        d.getElementsByTagName("head")[0].appendChild(s);
    })();
</script>

<!--End of Crisp Live Chat Script-->
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-50669449-1', 'auto');
    ga('send', 'pageview');

</script>
<script type="text/javascript">
    adroll_adv_id = "TH4Q5IVG5FHXNNPGFG2JBA";
    adroll_pix_id = "342MRCCGYRABZFOVG5SIPM";
    /* OPTIONAL: provide email to improve user identification */
    @if(Auth::check())
        adroll_email = "{{Auth::user()->email}}";
    @endif
    (function () {
        var _onload = function(){
            if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
            if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
                document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
        };
        if (window.addEventListener) {window.addEventListener('load', _onload, false);}
        else {window.attachEvent('onload', _onload)}
    }());
</script>
<script defer src="{{ elixir('js/home-scripts.js') }}" onLoad="start_scripts();"></script>
<script data-cfasync="false" type="text/javascript">
    function start_scripts() {
        fetch_jobs();
        fetch_bookmarks();
        //Checking Web worker Support
        if (typeof (Worker) !== "undefined") {
            //Creating Worker Object
            var worker = new Worker("/js/dashstats_worker.js");
            //Posting Data to worker: Here We are passing Email ID
            worker.postMessage({'email': "sreejithv@orioninc.com"});
            function workerResultReceiver(e) {
                //Updating UI With Result
                var myResponse = JSON.parse(e.data);
                $(".credits-balance").html(myResponse.credits.balance);
                $(".new-jobs").html(myResponse.jobs.new);
                $(".review-number").html(myResponse.reviews.total);
                $(".new-messages").html(myResponse.messages.unread);
                $(".quotations-received").html(myResponse.messages.total);
                $(".active-proposals").html(myResponse.jobs.active);
                $(".event-countdown").html(myResponse.jobs.days_to_event);
            }

            //Call Back Function for Success
            worker.onmessage = workerResultReceiver;
            //Call Back function if some error occurred
            worker.onerror = workerErrorReceiver;

                function workerErrorReceiver(e) {
                    console.log("there was a problem with the WebWorker within " + e);
                }
            }
            else {
                alert("Sorry!!! Worker Not Supported in Your Browser");
            }

            $(document).on('click','.delete-bookmark',function(e){
              e.preventDefault();
              var btn = $(this), btn_parent = btn.closest('tr'), url = btn.data('href'), token = btn.data('token');
              
              btn_parent.remove();
              $.ajax({
                  type: "post",
                  url: url,
                  data: {_method: 'delete',_token :token},
                  success: function(data) {
                    console.log('deleted')
                    
                  },
                  error: function(data) {
                      console.log('error')
                  }
              });
            });
        }
        function fetch_bookmarks(){
            //Checking Web worker Support
            if (typeof (Worker) !== "undefined") {
                //Creating Worker Object
                var worker = new Worker("/js/dashboard_bookmarks.js");
                //Posting Data to worker: Here We are passing Email ID

                var route = "{{route('bookmarks.show',Auth::user()->id)}}"

                worker.postMessage(route);
                function workerResultReceiver(e) {
                    //Updating UI With Result
                    console.log(e.data)
                    $('.bookmarks-body').html(e.data);
                }

                //Call Back Function for Success
                worker.onmessage = workerResultReceiver;
                //Call Back function if some error occurred
                worker.onerror = workerErrorReceiver;

                function workerErrorReceiver(e) {
                    console.log("there was a problem with the WebWorker within " + e);
                }
            }
            else {
                alert("Sorry!!! Worker Not Supported in Your Browser");
            }
        }
        function fetch_jobs() {
            //Checking Web worker Support
            if (typeof (Worker) !== "undefined") {
                //Creating Worker Object
                var worker = new Worker("/js/dashboard_request.js");
                //Posting Data to worker: Here We are passing Email ID
                worker.postMessage({'email': "sreejithv@orioninc.com"});
                function workerResultReceiver(e) {
                    //Updating UI With Result

                    $('.jobs-body').html(e.data);
                }

                //Call Back Function for Success
                worker.onmessage = workerResultReceiver;
                //Call Back function if some error occurred
                worker.onerror = workerErrorReceiver;

                function workerErrorReceiver(e) {
                    console.log("there was a problem with the WebWorker within " + e);
                }
            }
            else {
                alert("Sorry!!! Worker Not Supported in Your Browser");
            }
        }

</script>
</body>
</html>