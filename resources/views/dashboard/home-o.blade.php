@extends('page.dashboard-layout')
@section('title')
    Dashboard
@endsection

@section('styles')
    {!! HTML::style('/assets/css/jobsboard-custom.css?v=sjkn3') !!}
    {!! HTML::style('/assets/css/dashboard-foundation.css') !!}
    {!! HTML::style('/assets/css/dashboard-creditbundles.css') !!}
@endsection

@section('content')
    <main role="main" title="dashboard" class="dashboard">
        <div class="medium-2 menu--hidden columns">
            <ul class="left-navigation dash-tabs tabs vertical" data-tab>
                <li class="nav-vcard-container">
                    <ul class="vcard">
                        <li class="fn dash-prof">
                            <ul>
                                <li class="profile-img-container">
                                    <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{@Auth::user()->profile->profilePic}}">
                                </li>
                                <li>
                                    {{ @Auth::user()->first_name }}
                                    {{ @Auth::user()->last_name }}
                                </li>
                                <!-- <li class="edit-prof">My Profile</li> -->

                            </ul>
                        </li>
                        <div class="dash-section-attributes">
                            @if(Auth::user()->top_vendor())
                                <div class="top-vendor-badge infotip">
                                    <img src="/img/super-vendor.svg">
                                    <span class="infotiptext">Super Vendor</span>
                                </div>
                            @endif
                            @if(Auth::user()->recommended_vendor())
                                <div class="recommended-badge infotip">
                                    <i class="ion-thumbsup"> </i>
                                    <span class="infotiptext">Recommended</span>
                                </div>
                            @endif
                        </div>
                    </ul>

                </li>
                <li class="tab-title active">
                    <i class="dash-icon"></i>
                    <a href="#tab-dashboard">Dashboard</a>
                </li>

                @permission('view.tracker')
                <li class="tab-title">
                    <i class="chart-icon"></i>
                    <a href="#tab-tracker" id='tracker-btn' class="dashboard-tabs-btn" data-target-name="tracker">
                        Site Stats
                    </a>
                </li>
                @endpermission

                @permission('view.jobs')
                <li class="tab-title">
                    <i class="jobs-icon"></i>
                    <a href="#tab-jobs" id='jobs-board-btn' class="dashboard-tabs-btn" >Jobs</a>
                </li>
                @endpermission

                @permission('view.messages')
                <li class="tab-title">
                    <i class="msg-icon"></i>
                    <a href="#tab-messages" id='messages-btn' class="dashboard-tabs-btn" data-target-name="messages">
                        Messages
                        <span class="new-messages sidebar-notifs"></span>
                    </a>
                </li>
                @endpermission

                @permission('view.leads')
                <li class="tab-title">
                    <a href="#tab-leads"
                       class="dashboard-tabs-btn"
                       data-target-name="leads">
                        Leads Stats
                    </a>
                </li>
                @endpermission
                @permission('view.leads')
                <li class="tab-title">
                    <a href="#tab-mstats"
                       class="dashboard-tabs-btn"
                       data-target-name="mstats">
                        Messaging Stats
                    </a>
                </li>
                @endpermission

                @permission('edit.users')
                <li class="tab-title">
                    <i class="users-icon"></i>
                    <a href="#tab-users"
                       class="dashboard-tabs-btn"
                       data-target-name="users">
                        Manage Users
                    </a>
                </li>
                @endpermission

                @permission('view.activities')
                <li class="tab-title">
                    <i class="actlrep-icon"></i>
                    <a href="#tab-calendar"
                       class="dashboard-tabs-btn"
                       data-target-name="calendar">
                        Booking Calendar
                    </a>
                </li>
                @endpermission

                @permission('view.activities')
                <li class="tab-title">
                    <i class="actlog-icon"></i>
                    <a href="#tab-activitylog"
                       class="dashboard-tabs-btn"
                       data-target-name="activitylog">
                        Activity Log
                    </a>
                </li>
                @endpermission
                @permission('view.activities')
                <li class="tab-title">
                    <i class="actlrep-icon"></i>
                    <a href="#tab-creditreports"
                       class="dashboard-tabs-btn"
                       data-target-name="creditreports">
                        Credit Reports
                    </a>
                </li>
                @endpermission
                @permission('view.activities')
                <li class="tab-title">
                    <i class="actlrep-icon"></i>
                    <a href="#tab-vendorcategories"
                       class="dashboard-tabs-btn"
                       data-target-name="vendorcategories">
                        Vendor Categories
                    </a>
                </li>
                @endpermission

                @permission('view.creditbundles')
                <li class="tab-title">
                    <i class="credit-icon"></i>
                    <a href="#tab-creditbundles" id='creditbundles-btn' class="dashboard-tabs-btn"
                       data-target-name="creditbundles">
                        Credit Bundles
                    </a>
                </li>
                @endpermission

                @permission('edit.shop') <!-- hide till complete -->
                <li class="tab-title">
                    <i class="shop-icon"></i>
                    <a href="#tab-shop"
                       class="dashboard-tabs-btn"
                       data-target-name="myshop">
                        My Shop
                    </a>
                </li>
                @endpermission

                @permission('edit.blog')
                <li class="tab-title">
                    <i class="blog-icon"></i>
                    <a href="#tab-blog"
                       class="dashboard-tabs-btn"
                       data-target-name="blog"
                    >
                        Blog
                    </a>
                </li>
                @endpermission
                @permission('view.activities')
                <li class="tab-title">
                    <i class="actlrep-icon"></i>
                    <a href="#tab-links"
                       class="dashboard-tabs-btn"
                       data-target-name="links">
                        Admin Links
                    </a>
                </li>
                @endpermission

                @if(Auth::user()->hasuserRole('vendor'))
                    <li class="tab-title">
                        <i class="credit-icon"></i>
                        <a href="#tab-vendorcredits" id='vendorcredits-btn' class="dashboard-tabs-btn"
                           data-target-name="vendorcredits">
                            Buy Credits
                        </a>
                    </li>
                    <li class="tab-title">
                        <i class="boost-icon"></i>
                        <a href="#tab-advertise"
                           class="dashboard-tabs-btn"
                           data-target-name="advertise">
                            Boost Business
                        </a>
                    </li>

                    <li class="tab-title">
                        <i class="review-icon"></i>
                        <a href="#tab-reviews"
                           class="dashboard-tabs-btn"
                           data-target-name="reviews">
                            Reviews
                        </a>
                    </li>

                    <li class="tab-title">
                        <i class="gallery-icon"></i>
                        <a href="/gallery/list" onclick="window.location='/gallery/list';">
                            Galleries
                        </a>
                    </li>
                @endif
                <li class="tab-title">
                    <i class="profile-icon"></i>
                    <a href="#tab-profile"
                       class="dashboard-tabs-btn"
                       data-target-name="profile">
                        Profile
                    </a>
                </li>
            </ul>
        @if(!Auth::user()->hasUserRole('admin'))
            <!-- Need Help -->
                <ul class="help">
                    <h5>NEED HELP?</h5>
                    <li>
                        <i class="dash-tel-icon"></i>
                        <ul>
                            <li>020 239 5673</li>
                            <li>0775 987 555</li>
                        </ul>
                    </li>
                </ul>
            @endif
        </div>
        <div class="medium-10 columns">
            <!-- <div class="small-12 medium-9 columns main-content"> -->
            <div class="dash-tabs tabs-content">
                <div class="content active" id="tab-dashboard">
                    <div class="row tab-title-row">
                        <i class="dash-icon-m"></i>
                        <h3>Dashboard</h3>
                    </div>
                    @if(Auth::user()->hasUserRole('vendor'))
                        <!-- <div class="small-12 medium-10 small-centered columns ad-section">
                            <a href="shop/subcategory/credit-bundle">
                                <img class="hide-for-small-only" src="/img/BlackFridaySale.gif">
                                <img class="show-for-small-only" src="/img/BlackFridaySaleSmall.png">
                            </a>
                        </div> -->
                    @else
                       <!--  <div class="small-12 medium-10 small-centered columns ad-section">
                            <a href="vendors/chic-events">
                                <img class="hide-for-small-only" src="/img/featured/Chic Events Banner.png">
                                <img class="show-for-small-only" src="/img/featured/Chic Events Banner Small.png">
                            </a>
                        </div> -->
                    @endif
                    @include('dashboard.partials.dashboard-tab')
                </div>

                @permission('view.tracker')
                <div class="content" id="tab-tracker">
                    <div class="row tab-title-row">
                        <i class="chart-icon"></i>
                        <h3>Site Stats</h3>
                    </div>
                    @include('page.partials.tracker-ajax-container')
                </div>
                @endpermission

                @permission('view.jobs')
                <div class="content" id="tab-jobs">
                    <div class="row tab-title-row">
                        <i class="jobs-icon-m"></i>
                        <h3>Jobs</h3>
                    </div>
                    @include('jobs.partials.jobs-container')
                </div>
                @endpermission

                @permission('view.messages')
                <div class="content" id="tab-messages">
                    <div class="row tab-title-row">
                        <i class="msgs-icon-m"></i>
                        <h3>Messages</h3>
                    </div>
                    @include('messenger.partials.messages-container')
                </div>
                @endpermission

                @permission('view.creditbundles')
                <div class="content" id="tab-creditbundles">
                    <div class="row tab-title-row">
                        <i class="credit-icon-m"></i>
                        <h3>Credit Bundles</h3>
                    </div>
                    @include('admin.creditbundles.partials.dashboard-ajax-container')
                </div>
                @endpermission
                @permission('view.credits')
                <div class="content" id="tab-vendorcredits">
                    <div class="row tab-title-row">
                        <i class="credit-icon-m"></i>
                        <h3>Buy Credits</h3>
                    </div>
                    @include('admin.creditbundles.partials.vendor_credits-ajax-container')
                </div>
                @endpermission
                @permission('edit.shop') <!-- hide for vendors till complete -->
                <div class="content" id="tab-shop">
                    <div class="row tab-title-row">
                        <i class="shop-icon-m"></i>
                        <h3>My Shop</h3>
                    </div>
                    @include('shop.partials.myshop-ajax-container')
                </div>
                @endpermission

                @permission('edit.blog')
                <div class="content" id="tab-blog">
                    <div class="row tab-title-row">
                        <h3>Blog</h3>
                    </div>
                    @include('page.partials.blog-ajax-container')
                </div>
                @endpermission
                @permission('view.activities')
                <div class="content" id="tab-links">
                    <div class="row tab-title-row">
                        <h3>Admin links</h3>
                    </div>
                    @include('page.partials.links-ajax-container')
                </div>
                @endpermission

                @permission('view.leads')
                <div class="content" id="tab-leads">
                    <div class="row tab-title-row">
                        <h3>Leads Stats</h3>
                    </div>
                    @include('page.partials.leads-ajax-container')
                </div>
                @endpermission

                @permission('view.leads')
                <div class="content" id="tab-mstats">
                    <div class="row tab-title-row">
                        <h3>Messaging Stats</h3>
                    </div>
                    @include('page.partials.mstats-ajax-container')
                </div>
                @endpermission

                @permission('view.users')
                <div class="content" id="tab-users">
                    <div class="row tab-title-row">
                        <h3>User Management</h3>
                    </div>
                    @include('page.partials.users-ajax-container')
                </div>
                @endpermission

                @permission('view.activities')
                <div class="content" id="tab-calendar">
                    <div class="row tab-title-row">
                        <h3>Booking Calendar</h3>
                    </div>
                    @include('page.partials.calendar-ajax-container')
                </div>
                @endpermission

                @permission('view.activities')
                <div class="content" id="tab-activitylog">
                    <div class="row tab-title-row">
                        <h3>Activity Log</h3>
                    </div>
                    @include('page.partials.activitylog-ajax-container')
                </div>
                @endpermission

                @permission('view.activities')
                <div class="content" id="tab-creditreports">
                    <div class="row tab-title-row">
                        <h3>Credit Reports</h3>
                    </div>
                    @include('page.partials.creditreports-ajax-container')
                </div>
                @endpermission

                @permission('view.activities')
                <div class="content" id="tab-vendorcategories">
                    <div class="row tab-title-row">
                        <h3>Vendor Categories</h3>
                    </div>
                    @include('admin.classification.partials.classifications-ajax-container')
                </div>
                @endpermission
                @permission('create.advertise')
                <div class="content" id="tab-advertise">
                    <div class="row tab-title-row">
                        <i class="boost-icon-m"></i>
                        <h3>Boost Your Business</h3>
                    </div>
                    @include('page.partials.advertise-ajax-container')
                </div>
                @endpermission

                @permission('create.advertise')
                <div class="content" id="tab-reviews">
                    <div class="row tab-title-row">
                        <i class="reviews-icon-m"></i>
                        <h3>Request a review</h3>
                    </div>
                    @include('page.partials.reviews-ajax-container')
                </div>
                @endpermission

                <div class="content" id="tab-profile">
                    <div class="row tab-title-row">
                        <i class="profile-icon-m"></i>
                        <h3>Profile</h3>
                    </div>
                    @include('admin.users.partials.profile-ajax-container')
                </div>
                @if(Auth::user()->hasUserRole('vendor'))
                    <!-- <div class="small-12 medium-10 small-centered columns ad-section">
                        <a href="shop/subcategory/credit-bundle">
                            <img class="hide-for-small-only" src="/img/BlackFridaySale.gif">
                            <img class="show-for-small-only" src="/img/BlackFridaySaleSmall.png">
                        </a>
                    </div> -->
                @else
                   <!--  <div class="small-12 medium-10 small-centered columns ad-section">
                        <a href="vendors/chic-events">
                            <img class="hide-for-small-only" src="/img/featured/Chic Events Banner.png">
                            <img class="show-for-small-only" src="/img/featured/Chic Events Banner Small.png">
                        </a>
                    </div> -->
                @endif
            </div>
            <!-- </div> -->
        </div>
        @if(Auth::user()->hasUserRole('vendor') && !isset(Auth::user()->location))
            @include('pages.location-container')
        @endif
    </main>
@endsection
@section('scripts')
    <link rel="stylesheet" type="text/css" href="/css/foundation-datepicker.css">
    @include('dashboard.partials.dashboard-scripts')
    <script type="text/javascript">
        $('#jobs-board-btn').click(function(e){
            e.preventDefault();
            window.location = '/jobs/all';
        });
    </script>

@stop