<div class="row">
    <div class="text-center">
        @if(@$first_time)
            <h1>Welcome to Blissful</h1>
            <h2>How can we help you?</h2>

            <h3>
                <a href="#" class="button alert">
                    TAKE A TOUR
                </a>
            </h3>
        @else
            <div class="icon-bar five-up">
                <a class="item">
                    <img src="/public/assets/fonts/foundation/svgs/fi-home.svg">
                    <label>Jobs Available</label>
                </a>
                <a class="item">
                    <img src="/public/assets/fonts/foundation/svgs/fi-bookmark.svg">
                    <label>Reviews</label>
                </a>
                <a class="item">
                    <img src="/public/assets/fonts/foundation/svgs/fi-info.svg">
                    <label>Category Position</label>
                </a>
                <a class="item">
                    <img src="/public/assets/fonts/foundation/svgs/fi-mail.svg">
                    <label>Mail</label>
                </a>
                <a class="item">
                    <img src="/public/assets/fonts/foundation/svgs/fi-like.svg">
                    <label>Like</label>
                </a>
            </div>
        @endif
    </div>
    <div class="dashboard-main-icons-container small-12 columns">
        <ul class="small-block-grid-3">
            <li>
                <a href="#tab-jobs"
                   class="dashboard-tabs-btn"
                   data-target-name="jobs"
                >
                    <span class="counter">0</span>
                    <i class="fi-clipboard-pencil"></i>
                </a>
            </li>
            <li>
                <a href="#tab-messages"
                   class="dashboard-tabs-btn"
                   data-target-name="messages"
                >
                    <span class="counter">0</span>
                    <i class="fi-mail"></i>
                </a>
            </li>
            <li>
                <a href="#">
                    <span class="counter">0</span>
                    <i class="fi-mail"></i>
                </a>
            </li>
            <li>
                <a href="#">
                    <span class="counter">0</span>
                    <i class="fi-mail"></i>
                </a>
            </li>
            <li>
                <a href="#">
                    <span class="counter">0</span>
                    <i class="fi-mail"></i>
                </a>
            </li>
            <li>
                <a href="#">
                    <span class="counter">0</span>
                    <i class="fi-mail"></i>
                </a>
            </li>
        </ul>
    </div>
</div>

<script>
    $('#myTabs').on('toggled', function (event, tab) {
        //reset the dashboard counter


    });
</script>