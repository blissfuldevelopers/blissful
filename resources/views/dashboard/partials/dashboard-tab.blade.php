<div class="row">
    <!-- @if(@$first_time)
    <div class="text-center">
        <h1>Welcome to Blissful</h1>
        <h2>How can we help you?</h2>
        <h3>
            <a href="#" class="button alert">
                TAKE A TOUR
            </a>
        </h3>
    </div>    
    @endif
     -->
    <!-- start of stats section -->
    @if(Auth::user()->hasuserRole('vendor')) <!-- regular users shouldn't see this -->
    <div class="dash-stats small-12 columns collapse hide-for-small-only">
        <ul>
            <li>
                <i class="fa fa-briefcase"></i>
                <span class="stats-title">New Jobs</span>
                <h5 class="new-jobs ns">0</h5>
            </li>
            <li>
                <i class="fa fa-bitcoin"></i>
                <span class="stats-title">Credits Balance </span>
                <a style="position:absolute; bottom:0.5rem;" class="ns"  onclick="window.location='/shop/category/blissful-credits';">Buy Credits</a>
                <h5 class="credits-balance ns">0</h5>
            </li>
            <li>
                <i class="fa fa-star-o"></i>
                <span class="stats-title">Current Rating</span>
                <h5 class="review-number ns">0</h5>
            </li>
            <li>
                <i class="fa fa-bookmark-o"></i>
                <span class="stats-title">Total Jobs</span>
                <h5 class="jobs-value">0</h5>
            </li>
        </ul>
    </div>
    @endif
    @if(Auth::user()->hasuserRole('administrator')) <!-- regular users shouldn't see this -->
    <!-- Start of main jobs section -->
    <div class="dash-stats small-12 columns collapse ">
        <ul>

            <li>
                <i class="fa fa-star-o"></i>
                <span class="stats-title">Total Vendors</span>
                <h5 class="vendor_number ns">0</h5>
                <a style="position:absolute; bottom:0.5rem;" class="ns black"  href="/admin/analytics/new_vendors">View Stats</a>

            </li>

            <li>
                <i class="fa fa-bookmark-o"></i>
                <span class="stats-title">Total Users</span>
                <h5 class="user_number">0</h5>
                <a style="position:absolute; bottom:0.5rem;" class="ns black"  href="/admin/analytics/new_users">View Stats</a>

            </li>

            <li>
                <i class="fa fa-bookmark-o"></i>
                <span class="stats-title">Number of categories</span>
                <h5 class="category_number">0</h5>
                <a style="position:absolute; bottom:0.5rem;" class="ns black"  href="/admin/analytics/vendors_per_category">View Stats</a>
            </li>

            <li>
                <i class="fa fa-bookmark-o"></i>
                <span class="stats-title">Number of Messages Sent</span>
                <h5 class="messages_number">0</h5>
                <a style="position:absolute; bottom:0.5rem;" class="ns black"  href="/admin/analytics/sent_messages">View Stats</a>
            </li>

            <li>
                <i class="fa fa-briefcase"></i>
                <span class="stats-title">Total Jobs</span>
                <h5 class="jobs_number ns">0</h5>
                <a style="position:absolute; bottom:0.5rem;" class="ns black"  href="/admin/analytics/jobs_per_category">View Stats</a>
            </li>
            <li>
                <i class="fa fa-briefcase"></i>
                <span class="stats-title">Active Jobs</span>
                <h5 class="active_jobs_number ns">0</h5>
                <a style="position:absolute; bottom:0.5rem;" class="ns black"  href="/admin/analytics/jobs_created">View Stats</a>
            </li>
            <li>
                <i class="fa fa-briefcase"></i>
                <span class="stats-title">Total Job Responses</span>
                <h5 class="bids_number ns">0</h5>
                <a style="position:absolute; bottom:0.5rem;" class="ns black"  href="/admin/analytics/bids_made">View Stats</a>
            </li>
            <li>
                <i class="fa fa-briefcase"></i>
                <span class="stats-title">Total Leads</span>
                <h5 class="leads_number ns">0</h5>
                <a style="position:absolute; bottom:0.5rem;" class="ns black"  href="/admin/analytics/sent_leads">View Stats</a>
            </li>
            <li>
                <i class="fa fa-briefcase"></i>
                <span class="stats-title">Total credits on network</span>
                <h5 class="credits_on_network ns">0</h5>
<!--                  <a style="position:absolute; bottom:0.5rem;" class="ns"  href="/plot/credits_within_period">View Stats</a>
 -->            </li>
            <li>
                <i class="fa fa-bitcoin"></i>
                <span class="stats-title">Credits Purchased</span>
<!--                 <a style="position:absolute; bottom:0.5rem;" class="ns"  href="/users">Buy Credits</a>
 -->                <h5 class="credits_purchased ns">0</h5>
            </li>
            <li>
                <i class="fa fa-bitcoin"></i>
                <span class="stats-title">Vendor credit purchases</span>
<!--                 <a style="position:absolute; bottom:0.5rem;" class="ns"  href="/plot/users_bought_credits">Buy Credits</a>
 -->                <h5 class="vendors_purchased ns">0</h5>
            </li>
            <li>
                <i class="fa fa-bitcoin"></i>
                <span class="stats-title">Credits Spent</span>
<!--                 <a style="position:absolute; bottom:0.5rem;" class="ns"  onclick="window.location='/shop/category/1';">Buy Credits</a>
 -->                <h5 class="credits_spent ns">0</h5>
            </li>
        </ul>
    </div>
    @endif
    <!-- end of stats section -->
    @if(!Auth::user()->hasUserRole('administrator'))
    <div class="dashboard-main-icons-container small-12 columns">
        <ul class="small-block-grid-3">
            <li>
                <a href="#tab-jobs"
                   class="dashboard-tabs-btn"
                   data-target-name="jobs"
                >
                   
                    <i class="fa fa-briefcase">
                    @if(!Auth::user()->hasuserRole('user'))
                    @endif
                    </i>
                    
                    <span class="dash-titles">Jobs</span>
                </a>
            </li>
            <li>
                <a href="#tab-messages"
                   class="dashboard-tabs-btn"
                   data-target-name="messages">
                    <i class="fa fa-envelope-o"> <span class="new-messages icon-notifs"></span></i>
                    <span class="dash-titles">Messages</span>

                </a>
            </li>
            <li>
                <a href="#tab-profile"
                   class="dashboard-tabs-btn"
                   data-target-name="profile">
                    <i class="fa fa-user"></i>
                    <span class="dash-titles">Profile</span>
                </a>
            </li>
            @if(!Auth::user()->hasuserRole('user'))
            <li>
                <a href="#tab-reviews"
                   class="dashboard-tabs-btn"
                   data-target-name="reviews">
                    <i class="fa fa-star-o"></i>
                    <span class="dash-titles">Reviews</span>
                </a>
            </li>
            @endif
            <!-- <li>
                <a href="#">
                    <i class="fa fa-bitcoin"></i>
                    <span class="dash-titles">Credits</span>
                </a>
            </li> -->
        </ul>
    </div>
    @endif
</div>

<script>
    $('#myTabs').on('toggled', function (event, tab) {
        //reset the dashboard counter


    });
</script>