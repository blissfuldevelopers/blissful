{!! HTML::script('/js/foundation-datepicker.min.js') !!}
{!! HTML::script('/assets/js/ajaxify_bliss.script.js?v=rfskotw6r7b') !!}

@if(Auth::user()->hasUserRole('administrator'))
    <script data-cfasync="false" type="text/javascript">
        $(document).ready(function () {
            //Checking Web worker Support
            if (typeof (Worker) !== "undefined") {
                //Creating Worker Object
                var worker = new Worker("/js/admin_dashstats.js");

                //Posting Data to worker: Here We are passing Email ID
                worker.postMessage({'email': "sreejithv@orioninc.com"});
                function workerResultReceiver(e) {
                    //Updating UI With Result
                    var myResponse = JSON.parse(e.data);
                    $(".jobs_number").html(myResponse.jobs.total);
                    $(".active_jobs_number").html(myResponse.jobs.active);
                    $(".bids_number").html(myResponse.jobs.bids);
                    $(".credits_on_network").html(myResponse.credits.credits_on_network);
                    $(".credits_purchased").html(myResponse.credits.credits_purchased);
                    $(".vendor_number").html(myResponse.users.vendor_number);
                    $(".user_number").html(myResponse.users.user_number);
                    $(".category_number").html(myResponse.categories.total);
                    $(".messages_number").html(myResponse.messages.total_messages);
                    $(".leads_number").html(myResponse.messages.total_leads);
                    $(".vendors_purchased").html(myResponse.credits.vendors_purchased);
                    $(".credits_spent").html(myResponse.credits.credits_spent);

                }

                //Call Back Function for Success
                worker.onmessage = workerResultReceiver;
                //Call Back function if some error occurred
                worker.onerror = workerErrorReceiver;
                function workerErrorReceiver(e) {
                    console.log("there was a problem with the WebWorker within " + e);
                }
            }
            else {
                alert("Sorry!!! Worker Not Supported in Your Browser");
            }
        });
    </script>
@else
    <script data-cfasync="false" type="text/javascript">
        $(document).ready(function () {
            //Checking Web worker Support
            if (typeof (Worker) !== "undefined") {
                //Creating Worker Object
                var worker = new Worker("/js/dashstats_worker.js");
                //Posting Data to worker: Here We are passing Email ID
                worker.postMessage({'email': "sreejithv@orioninc.com"});
                function workerResultReceiver(e) {
                    //Updating UI With Result
                    var myResponse = JSON.parse(e.data);
                    $(".credits-balance").html(myResponse.credits.balance);
                    $(".new-jobs").html(myResponse.jobs.new);
                    $(".review-number").html(myResponse.reviews.total);
                    if (myResponse.messages.unread > 0) {
                        $(".new-messages").html("");
                        $(".new-messages").html(myResponse.messages.unread);
                        $(".new-messages").show();

                    }
                    else{
                        $(".new-messages").hide();
                    }
                    $(".jobs-value").html(myResponse.jobs.value);
                }

                //Call Back Function for Success
                worker.onmessage = workerResultReceiver;
                //Call Back function if some error occurred
                worker.onerror = workerErrorReceiver;

                function workerErrorReceiver(e) {
                    console.log("there was a problem with the WebWorker within " + e);
                }
            }
            else {
                alert("Sorry!!! Worker Not Supported in Your Browser");
            }
        });
    </script>
@endif
<script>
    $(document).foundation();
    var opened = [];

    function open_tab(_this) {
        var target_id = _this.attr('href'),
                item_name = _this.data('target-name');
        if (typeof opened[item_name] == 'undefined') {
            opened[item_name] = true;
            //just for the jobs-container
            ajaxify_bliss({
                container: $(target_id + ' #' + item_name + '-container-data'),
                container_message: $(target_id + ' #' + item_name + '-container-message'),
                back_action_container: $(target_id + ' #' + item_name + '-container-back-action'),
                bindKeyboardBack: false,
                allowHistoryPushState: false
            });
            $("a[href='"+target_id+"']").click();

        }
        return false;
    }

    $('.dashboard-tabs-btn').dblclick(function (e) {
        e.preventDefault();
        var _this = $(this),
                target_id = _this.attr('href'),
                item_name = _this.data('target-name');

        if (typeof opened[item_name] !== 'undefined') {
            delete opened[item_name];
        }

        $("a[href=" + target_id + "]").trigger("click");
        return false;
    });
    // function withimg(e){     
    //     e.preventDefault();
    //     console.log('hit');


    //     var id = $(e.target)[0].id;
    //     var fd = new FormData($('form.'+id)[0]);
    //     console.log(fd);
    //     $.ajax({
    //         url: $('.'+id).attr('action'),
    //         type:"POST",
    //         data:  fd,
    //         cache: false,
    //         processData:false,
    //         enctype: 'multipart/form-data',
    //         success: function(data){

    //             console.log(data);
    //         },
    //         error: function(){

    //         }             
    //         });
    // }


    $('.dashboard-tabs-btn').click(function (e) {
        e.preventDefault();
        open_tab($(this));

        function shuffle(array) {
            var i = array.length;

            if (i == 0) return false;

            while (--i) {
                var j = Math.floor(Math.random() * (i + 1)),
                        tempi = array[i],
                        tempj = array[j];

                array[i] = tempj;
                array[j] = tempi;
            }
        }

        if ($(this).attr('id') == 'messages-btn') {
           
            $('#messages-container').load(function () {

                

                var colors = ["#00A79C", "#0C2C3A", "#F79420", "#2AB573", "#BCBDBF", "#9A8579", "#D2E28D"];
                shuffle(colors);


                $('span.avi').each(function () {
                    $(this).css('background-color', colors[0]);
                });


            });

        }

    });
</script>