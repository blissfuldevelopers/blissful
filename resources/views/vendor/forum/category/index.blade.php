{{-- $category is passed as NULL to the master layout view to prevent it from showing in the breadcrumbs --}}
@extends ('forum::master', ['category' => null])

@section ('container')
    @can('createCategories')
            @include ('forum::category.partials.form-create')
    @endcan
    <div class="row">
        <h4>All Categories</h4>

        <table class="table table-index small-12">
            <thead>
                <tr>
                    <th class="medium-1"></th>
                    <th>{{ trans_choice('forum::categories.category', 1) }}</th>
                    <th class="medium-2">Discussions</th>
                    <th class="medium-2">Comments</th>
                    <th class="medium-2">Latest Discussion</th>
                    <th class="medium-2">Last Comment</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                <tr>
                    @include ('forum::category.partials.list', ['titleClass' => 'lead'])
                </tr>
                @if (!$category->children->isEmpty())
                    <tr>
                        <th colspan="5">{{ trans('forum::categories.subcategories') }}</th>
                    </tr>
                    @foreach ($category->children as $subcategory)
                        @include ('forum::category.partials.list', ['category' => $subcategory])
                    @endforeach
                @endif
                @endforeach
            </tbody>
        </table>
    </div>
@stop
