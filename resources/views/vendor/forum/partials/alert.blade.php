<div data-alert class="alert-box {{ $type }} radius small-12 medium-4 medium-centered columns">
  {!! $message !!}
  <a href="#" class="close">&times;</a>
</div>
