<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Blissful.co.ke</title>

    <style>
        * {
            margin: 0;
            padding: 0;
            font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif
        }

        img {
            max-width: 100%
        }

        .collapse {
            margin: 0;
            padding: 0
        }

        body {
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100% !important;
            height: 100%
        }

        a {
            color: #2BA6CB
        }

        p.callout {
            padding: 15px;
            background-color: #ECF8FF;
            margin-bottom: 15px
        }

        .callout a {
            font-weight: 700;
            color: #2BA6CB
        }

        table.body-wrap {
            width: 100%
        }

        table.footer-wrap {
            width: 100%;
            clear: both !important;
            background: #1f9ac3
        }

        .footer-wrap .container td.content p {
            border-top: 1px solid #d7d7d7;
            padding-top: 15px;
            font-size: 10px;
            font-weight: 700
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            line-height: 1.1;
            margin-bottom: 15px;
            color: #000
        }

        h1 small, h2 small, h3 small, h4 small, h5 small, h6 small {
            font-size: 60%;
            color: #6f6f6f;
            line-height: 0;
            text-transform: none
        }

        h1 {
            font-weight: 200;
            font-size: 44px
        }

        h2 {
            font-weight: 200;
            font-size: 37px
        }

        h3 {
            font-weight: 500;
            font-size: 27px
        }

        h4 {
            font-weight: 500;
            font-size: 23px
        }

        h5 {
            font-weight: 900;
            font-size: 17px
        }

        h6 {
            font-weight: 900;
            font-size: 14px;
            text-transform: uppercase;
            color: #444
        }

        p, ul {
            margin-bottom: 10px;
            font-weight: 400;
            font-size: 14px;
            line-height: 1.6
        }

        p.lead {
            font-size: 17px
        }

        p.last {
            margin-bottom: 0
        }

        ul li {
            margin-left: 5px;
            list-style-position: inside
        }

        ul.sidebar {
            background: #ebebeb;
            display: block;
            list-style-type: none
        }

        ul.sidebar li {
            display: block;
            margin: 0
        }

        ul.sidebar li a {
            text-decoration: none;
            color: #666;
            padding: 10px 16px;
            cursor: pointer;
            border-bottom: 1px solid #777;
            border-top: 1px solid #FFF;
            display: block;
            margin: 0
        }

        ul.sidebar li a.last {
            border-bottom-width: 0
        }

        ul.sidebar li a h1, ul.sidebar li a h2, ul.sidebar li a h3, ul.sidebar li a h4, ul.sidebar li a h5, ul.sidebar li a h6, ul.sidebar li a p {
            margin-bottom: 0 !important
        }

        .container {
            display: block !important;
            max-width: 600px !important;
            margin: 0 auto !important;
            clear: both !important
        }

        .content {
            padding: 15px;
            max-width: 600px;
            margin: 0 auto;
            display: block
        }

        .content table {
            width: 100%
        }

        .column {
            width: 300px;
            float: left
        }

        .column tr td {
            padding: 15px
        }

        .column-wrap {
            padding: 0 !important;
            margin: 0 auto;
            max-width: 600px !important
        }

        .column table {
            width: 100%
        }

        .social .column {
            width: 280px;
            min-width: 279px;
            float: left
        }

        .clear {
            display: block;
            clear: both
        }

        @media only screen and (max-width: 600px) {
            a[class="btn"] {
                display: block !important;
                margin-bottom: 10px !important;
                background-image: none !important;
                margin-right: 0 !important
            }

            div[class="column"] {
                width: auto !important;
                float: none !important
            }

            table.social div[class="column"] {
                width: auto !important
            }
        }
    </style>

</head>

<body bgcolor="#FFFFFF">
<div>
    <h6>Statistics - {{ date('dS, M-Y') }}</h6>
</div>
<div class="email-body">
    @if(isset($stats_data) && !empty($stats_data))
        <ul style="list-style:none;">
            @foreach($stats_data as $title => $data)
                <li>
                    <h2> {{ ucwords(str_ireplace(['_','-'], ' ',strtolower(trim($title)))) }}
                        @if(isset($data['number']))
                            : {{ number_format($data['number']) }}
                        @endif
                    </h2>
                    <div>
                        <ul>
                            @if(!is_array($data))
                                <li>
                                    {{ ucwords(str_ireplace(['_','-'], ' ',strtolower(trim($title)))) }}
                                    : {{ ucwords(str_ireplace(['_','-'], ' ',strtolower(trim($data)))) }}
                                </li>
                            @else
                                @foreach($data as $inner_title => $inner_data)
                                    @if(!is_array($inner_data) && $inner_title <> 'number')
                                        <li>
                                            <h3>
                                                {{ ucwords(str_ireplace(['_','-'], ' ',strtolower(trim($inner_title)))) }}
                                                : {{ number_format($inner_data) }}
                                            </h3>
                                        </li>
                                    @elseif(is_array($inner_data))
                                        @if($inner_title <> '')
                                            <li>
                                                <h4>{{ ucwords(str_ireplace(['_','-'], ' ',strtolower(trim($inner_title)))) }}</h4>
                                                @endif
                                                <ul>
                                                    @foreach($inner_data as $inner_inner_title => $inner_inner_data)
                                                        <li>
                                                            {{ ucwords(str_ireplace(['_','-'], ' ',strtolower(trim($inner_inner_title)))) }}
                                                            : {{ number_format($inner_inner_data) }}
                                                        </li>
                                                    @endforeach
                                                </ul>
                                                @if($inner_title <> '')
                                            </li>
                                        @endif
                                    @endif
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </li>
            @endforeach
        </ul>
    @endif
    <br/>
    <br/>
    <hr size="1">

    <p>Thanks.</p>

    <p>Blissful Team</p>

</div>
</body>