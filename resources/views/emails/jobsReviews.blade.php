<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" name="viewport">
    <title>{{$subject}}</title>

    <style type="text/css">
        @font-face {font-family: 'source_sans_probold'; src: url('http://www.stampready.net/Fonts/source_sans_pro/sourcesanspro-bold-webfont.eot'); src: url('http://www.stampready.net/Fonts/source_sans_pro/sourcesanspro-bold-webfont.eot?#iefix') format('embedded-opentype'), url('http://www.stampready.net/Fonts/source_sans_pro/sourcesanspro-bold-webfont.woff2') format('woff2'), url('http://www.stampready.net/Fonts/source_sans_pro/sourcesanspro-bold-webfont.woff') format('woff'), url('http://www.stampready.net/Fonts/source_sans_pro/sourcesanspro-bold-webfont.ttf') format('truetype'), url('http://www.stampready.net/Fonts/source_sans_pro/sourcesanspro-bold-webfont.svg#source_sans_probold') format('svg'); font-weight: normal; font-style: normal;}
        @font-face {font-family: 'source_sans_prosemibold'; src: url('http://www.stampready.net/Fonts/source_sans_pro/sourcesanspro-semibold-webfont.eot'); src: url('http://www.stampready.net/Fonts/source_sans_pro/sourcesanspro-semibold-webfont.eot?#iefix') format('embedded-opentype'), url('http://www.stampready.net/Fonts/source_sans_pro/sourcesanspro-semibold-webfont.woff2') format('woff2'), url('http://www.stampready.net/Fonts/source_sans_pro/sourcesanspro-semibold-webfont.woff') format('woff'), url('http://www.stampready.net/Fonts/source_sans_pro/sourcesanspro-semibold-webfont.ttf') format('truetype'), url('http://www.stampready.net/Fonts/source_sans_pro/sourcesanspro-semibold-webfont.svg#source_sans_prosemibold') format('svg'); font-weight: normal; font-style: normal;}
        @font-face {font-family: 'source_sans_proregular'; src: url('http://www.stampready.net/Fonts/source_sans_pro/sourcesanspro-regular-webfont.eot'); src: url('http://www.stampready.net/Fonts/source_sans_pro/sourcesanspro-regular-webfont.eot?#iefix') format('embedded-opentype'), url('http://www.stampready.net/Fonts/source_sans_pro/sourcesanspro-regular-webfont.woff2') format('woff2'), url('http://www.stampready.net/Fonts/source_sans_pro/sourcesanspro-regular-webfont.woff') format('woff'), url('http://www.stampready.net/Fonts/source_sans_pro/sourcesanspro-regular-webfont.ttf') format('truetype'), url('http://www.stampready.net/Fonts/source_sans_pro/sourcesanspro-regular-webfont.svg#source_sans_proregular') format('svg'); font-weight: normal; font-style: normal;}
        body{
            margin:0px;
            padding:0px;
        }
        ::selection{
            background:#ff2f2f;
        }
        ::-moz-selection{
            background:#ff2f2f;
        }
        @media only screen and (max-width:640px){
            table[class=scale]{
                width:100%!important;
            }

        }   @media only screen and (max-width:640px){
            table[class=scale-center-both]{
                width:100%!important;
                text-align:center!important;
                padding-left:20px!important;
                padding-right:20px!important;
            }

        }   @media only screen and (max-width:640px){
            table[class=scale-center-bottom]{
                width:100%!important;
                text-align:center!important;
                padding-bottom:12px!important;
            }

        }   @media only screen and (max-width:640px){
            table[class=margin-center]{
                margin:auto!important;
            }

        }   @media only screen and (max-width:640px){
            table[class=hide]{
                display:none!important;
            }

        }   @media only screen and (max-width:640px){
            td[class=scale-center-both]{
                width:100%!important;
                text-align:center!important;
                padding-left:20px!important;
                padding-right:20px!important;
            }

        }   @media only screen and (max-width:640px){
            td[class=scale-center-left]{
                width:100%!important;
                text-align:right!important;
            }

        }   @media only screen and (max-width:640px){
            td[class=scale-center-bottom]{
                width:100%!important;
                text-align:center!important;
                padding-bottom:24px!important;
            }

        }   @media only screen and (max-width:640px){
            td[class=scale-center-bottom-both]{
                width:100%!important;
                text-align:center!important;
                padding-bottom:12px!important;
                padding-left:20px!important;
                padding-right:20px!important;
            }

        }   @media only screen and (max-width:640px){
            td[class=bottom12]{
                padding-bottom:12px!important;
            }

        }   @media only screen and (max-width:640px){
            td[class=bottom19]{
                padding-bottom:19px!important;
            }

        }   @media only screen and (max-width:640px){
            td[class=bottom21]{
                padding-bottom:21px!important;
            }

        }   @media only screen and (max-width:640px){
            td[class=height4]{
                height:4px!important;
                font-size:1px!important;
            }

        }   @media only screen and (max-width:640px){
            img[class=scale]{
                width:90%!important;
            }

        }   @media only screen and (max-width:640px){
            img[class=reset]{
                width:100%!important;
            }

        }</style></head>
<body style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">


<!-- NAVIGATION -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td bgcolor="#e5eaeb">
            <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                <tr>
                    <td bgcolor="#FFFFFF">
                        <table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                            <tr><td height="15" style="font-size: 1px;">&nbsp;</td></tr>
                            <tr>
                                <td>
                                    <table width="165" border="0" cellspacing="0" cellpadding="0" align="left" class="scale">
                                        <tr>
                                            <img src="http://www.blissful.co.ke/img/blissful-logo.png" border="0" height="35%" alt="blissful" />    </tr>
                                        </tr>
                                    </table>
                                    <table width="365" border="0" cellspacing="0" cellpadding="0" align="right" class="scale">
                                        <tr>
                                            <td align="right" style="font-family:'source_sans_proregular', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 30px; color: #557777;" class="scale-center-both">

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!-- Divider -->
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                            </table><!-- End Divider -->                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<!-- TITLE -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td bgcolor="#e5eaeb">
            <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                <tr>
                    <td bgcolor="#FFFFFF">
                        <table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                            <tr><td height="48">&nbsp;</td></tr>
                            <tr>
                            </tr>
                            <tr><td height="1" style="font-size: 1px;">&nbsp;</td></tr>
                            <tr>
                                <td height="18">
                                    <table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr><td height="4" bgcolor="#557777" style="border-radius: 1px;"></td></tr>
                                    </table>
                                </td>

                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


<!-- TEXT +BUTTON -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td bgcolor="#e5eaeb">
            <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                <tr>
                    <td bgcolor="#FFFFFF">
                        <table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="scale-center-both">
                            <tr><td height="24">&nbsp;</td></tr>
                            <tr>
                                <td align="center" style="font-family:'source_sans_proregular', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 27px; color: #112D3B;">
                                    Hi, {{$first_name}}<br><br>
                                    <br><br>
                                        {!!$mail_content!!}
                                    <br><br>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table align="center" style="border-radius: 5px;" border="0" cellpadding="0" cellspacing="0" bgcolor="#EE3750">
                                        <tr>
                                            
                                            <!-- Divider -->
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                                <tbody><tr>
                                                    <td width="100%" height="25"></td>
                                                </tr>

                                                <!-- Divider -->
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                                    <tbody><tr>
                                                        <td width="100%" height="25"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="100%" height="1" bgcolor="#f1f1f1" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="100%" height="25"></td>
                                                    </tr>
                                                    </tbody></table><!-- End Divider -->


                                                </tbody></table></tr>
                                    </table>
                                </td>
                            </tr>
                        </table>




                        <!-- ICONS -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                                <td bgcolor="#e5eaeb">
                                    <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                <table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="scale-center-both">
                                                    <tr><td height="20">&nbsp;</td></tr>
                                                    <tr>
                                                        <td align="center">
                                                            <a href="https://www.facebook.com/blissfulKE"><img src="https://gallery.mailchimp.com/b50a12294fa94835bd7f24d86/images/543e8b68-8d50-43cd-8dc8-7e05cdc7763d.png" border="0" style="display: inline-block;" alt=""></a>&nbsp;&nbsp;&nbsp;<a href="https://twitter.com/blissfulKE"><img src="https://gallery.mailchimp.com/b50a12294fa94835bd7f24d86/images/426b7418-d38f-49d8-bebb-528f728c9dd4.png" border="0" style="display: inline-block;" alt=""></a>&nbsp;&nbsp;&nbsp;<a href="https://www.instagram.com/blissful_ke"><img src="https://gallery.mailchimp.com/b50a12294fa94835bd7f24d86/images/e6f04c36-9fe6-4f03-9e63-65b114ab0d02.png" border="0" style="display: inline-block;" alt=""></a>&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr><td height="35">&nbsp;</td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                        <!-- FOOTER -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                                <td bgcolor="#e5eaeb">
                                    <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                                        <tr>
                                            <td bgcolor="#112D3B">
                                                <table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="scale-center-both">
                                                    <tr><td height="18">&nbsp;</td></tr>
                                                    <tr>
                                                        <td align="center" style="font-family:'source_sans_proregular', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 27px; color: #FFFFFF;">
                                                            Copyright © 2016 Blissful. All rights are reserved.
                                                        </td>
                                                    </tr>
                                                    <tr><td height="15" style="font-size: 1px;">&nbsp;</td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                        <!-- SPACE -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="hide">
                            <tr>
                                <td bgcolor="#e5eaeb">
                                    <table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
                                        <tr>
                                            <td height="42" bgcolor="#e5eaeb">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                    </td></tr></table></td></tr></table></body>
</html>
