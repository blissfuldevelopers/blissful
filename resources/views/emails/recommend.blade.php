<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <style type="text/css">
        body {
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            -webkit-line-break: after-white-space;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 15px;
            font-weight: normal;
            word-wrap: break-word;
            color: #75848D;
            line-height: 1.5;
            margin: 0;
            padding: 0;
            min-width: 100% !important;
            height: 100%;
        }

        hr {
            background-color: #E9EFF2;
            color: #E9EFF2;
            border: 0 none;
            height: 1px;
        }

        table h1, h1.intercom-align-center {
            font-weight: 200 !important;
            font-size: 20px !important;
            color: #000000 !important;
            padding: 0 30px 20px 30px;
            width: 440px !important;
            margin: 0px !important;
        }

        table h2 {
            font-weight: 200 !important;
            font-size: 17px !important;
            color: #000000 !important;
            padding: 0 30px 20px 30px;
            width: 440px !important;
            margin: 0px !important;
        }

        table h3 {
            font-weight: 500 !important;
            font-size: 14px !important;
            color: #000000 !important;
            padding: 0 30px 20px 30px;
            width: 440px !important;
            margin: 0px !important;
        }

        table a {
            color: #42AFE3;
            border-bottom: 1px solid #42AFE3;
            font-weight: bold;
            font-family: Arial;
            cursor: pointer;
            text-decoration: none !important;
        }

        .intercom-h2b-button {
            padding: 20px 30px;
            margin: 30px;
            display: inline-block;
            background-color: #42AFE3;
            border-radius: 5px;
            color: #FFF;
            font-size: 16px;
            font-weight: bold;
            text-decoration: none !important;
            text-align: center;
            cursor: pointer;
        }

        table.content {
            width: 500px !important;
            margin: 0px !important;
            padding: 0 !important;
        }

        table .content .container p, p, p.intercom-align-left {
            width: 440px !important;
            padding: 0 30px 20px 30px !important;
            margin: 0px;
            color: #75848D !important
        }

        table .content .container ul,
        table .content .container li,
        {
            width: 440px !important;
            padding: 30px;
            margin: 0px;
        }

        table .content p font img {
            width: 100% !important;
            padding: 0px !important;
            margin: -120px 0 0 0 !important;
        }

        .block-html {
            margin-left: -31px !important;
            margin-right: -31px !important;
        }

        .block-html img {
            width: 100%;
            margin-left: -31px !important;
            margin-right: -31px !important;
        }

        img.block-image {
            width: 100% !important;
            margin: 0px -31px 0px -31px
        }

        img.second-announcement {
            margin: 0 auto !important;
            width: 150px !important;
        }

        .gs li, li .gs {
            display: inline-block;
            margin: 0px 0px 0px 0px !important;
        }

        @media only screen and (max-width: 500px) {

            a[class="intercom-h2b-button"] {
                display: block !important;
                margin-bottom: 10px !important;
                background-image: none !important;
                margin-right: 0 !important;
                text-decoration: none !important;
            }
        }
    </style>
    <style>
        .intercom-align-right {
            text-align: right !important;
        }

        .intercom-align-center {
            text-align: center !important;
        }

        .intercom-align-left {
            text-align: left !important;
        }

        /* Over-ride for RTL */
        .right-to-left .intercom-align-right {
            text-align: left !important;
        }

        .right-to-left .intercom-align-left {
            text-align: right !important;
        }

        .right-to-left .intercom-align-left {
            text-align: right !important;
        }

        .right-to-left li {
            text-align: right !important;
            direction: rtl;
        }

        .right-to-left .intercom-align-left img,
        .right-to-left .intercom-align-left .intercom-h2b-button {
            margin-left: 0 !important;
        }

        .intercom-attachment,
        .intercom-attachments,
        .intercom-attachments td,
        .intercom-attachments th,
        .intercom-attachments tr,
        .intercom-attachments tbody,
        .intercom-attachments .icon,
        .intercom-attachments .icon img {
            border: none !important;
            box-shadow: none !important;
            padding: 0 !important;
            margin: 0 !important;
        }

        .intercom-attachments {
            margin: 10px 0 !important;
        }

        .intercom-attachments .icon,
        .intercom-attachments .icon img {
            width: 16px !important;
            height: 16px !important;
        }

        .intercom-attachments .icon {
            padding-right: 5px !important;
        }

        .intercom-attachment {
            display: inline-block !important;
            margin-bottom: 5px !important;
        }

        .intercom-interblocks-content-card {
            width: 334px;
            max-height: 136px;
            max-width: 100%;
            overflow: hidden;
            border-radius: 20px;
            font-size: 16px;
            border: 1px solid #e0e0e0;
        }

        a.intercom-interblocks-article-card {
            text-decoration: none;
        }

        .intercom-interblocks-article-icon {
            width: 22.5%;
            height: 136px;
            float: left;
            background-color: #fafafa;
            background-image: url('https://static.intercom-mail.com/assets/article_book-1a595be287f73c0d02f548f513bfc831.png');
            background-repeat: no-repeat;
            background-size: 32px;
            background-position: center;
        }

        .intercom-interblocks-article-text {
            width: 77.5%;
            float: right;
            background-color: #fff;
        }

        .intercom-interblocks-article-title {
            color: #519dd4;
            margin: 16px 18px 12px;
            line-height: 1.3;
            overflow: hidden;
        }

        .intercom-interblocks-article-body {
            margin: 0 18px 12px;
            font-size: 14px;
            color: #65757c;
            line-height: 1.3;
        }

        .intercom-interblocks-article-author {
            margin: 10px 15px;
            height: 24px;
            line-height: normal;
        }

        .intercom-interblocks-article-author-avatar {
            width: 16px;
            height: 16px;
            display: inline-block;
            vertical-align: middle;
            float: left;
            margin-right: 5px;
        }

        img.intercom-interblocks-article-author-avatar-image {
            width: 16px;
            height: 16px;
            border-radius: 50%;
            margin: 0;
            vertical-align: top;
        }

        .intercom-interblocks-article-author-name {
            color: #74848b;
            margin: 0 0 0 5px;
            font-size: 12px;
            font-weight: 500;
            overflow: hidden;
        }

        .intercom-interblocks-article-written-by {
            color: #8897a4;
            margin: 1px 0 0 5px;
            font-size: 12px;
            overflow: hidden;
            vertical-align: middle;
            float: left;
        }
    </style>
</head>

<body style="-webkit-font-smoothing: antialiased; -webkit-line-break: after-white-space; -webkit-text-size-adjust: none; color: #75848D; font-family: Arial, Helvetica, sans-serif; font-size: 15px; font-weight: normal; height: 100%; line-height: 1.5; margin: 0; min-width: 100% !important; padding: 0; word-wrap: break-word"
      bgcolor="#E9EFF2">
<style type="text/css">
    body {
        -webkit-font-smoothing: antialiased;
        -webkit-text-size-adjust: none;
        -webkit-line-break: after-white-space;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 15px;
        font-weight: normal;
        word-wrap: break-word;
        color: #75848D;
        line-height: 1.5;
        margin: 0;
        padding: 0;
        min-width: 100% !important;
        height: 100%;
    }
</style>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#E9EFF2">
    <tbody>
    <tr bgcolor="#E9EFF2">
        <td style="font-size: 0; line-height: 0"></td>
        <td align="center">
            <table width="500px" border="0" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td style="font-size: 0; height: 28px; line-height: 0"></td>
                </tr>
                <tr>
                    <td style="font-size: 0; line-height: 0"></td>
                    <td align="center"
                        style="clear: both !important; display: block !important; margin: 0 auto; width: 500px"
                        bgcolor="#E9EFF2" width="100%" height="51px">
                        <div class="logotype">
                            <a href="https://www.blissful.co.ke" style="text-decoration: none;" target="_blank">
                                <img src="https://blissful.co.ke/img/blissful-bw.png" alt="Azendoo" width="250px"
                                     height="64px" style="max-width: 100%; position: relative; top: -15px">
                            </a>
                        </div>
                    </td>
                    <td style="font-size: 0; line-height: 0"></td>
                </tr>
                <tr style="height: 25px">
                    <td style="font-size: 0; line-height: 0"></td>
                </tr>
            </table>
        </td>
        <td style="font-size: 0; line-height: 0"></td>
    </tr>
    <tr bgcolor="#E9EFF2">
        <td style="font-size: 0; line-height: 0" bgcolor="#E9EFF2"></td>
        <td align="center">
            <table class="content" width="500px" bgcolor="#ffffff" align="center" border="0" cellpadding="0"
                   cellspacing="0"
                   style="border: 1px solid #c7d6dd; border-radius: 5px; border-spacing: 0 !important; margin: 0px; padding: 0; width: 500px !important">
                <tr>
                    <td align="center" style="clear: both !important; margin: 0 auto; padding: 0; width: 440px"
                        colspan="3">
                        <div align="center"
                             style="color: #75848D !important; font-family: Arial; font-size: 15px; font-weight: normal; line-height: 1.3; margin: 0; padding: 52px 0px">
                            <h1 style="color: #000000 !important; font-family: Arial; font-size: 20px !important; font-weight: 200 !important; line-height: 26px; margin: 0px; padding: 0px; width: 440px !important">
                                Hi {{$first_name}},<br>
                                How likely are you to recommend Blissful to a friend or colleague?</h1>
                        </div>
                        <div class="bar-score" align="center">
                            <ul style="color: #75848D; font-family: Arial-BoldMT; font-size: 17px; padding-left: 0px">
                                <li style="display: inline-block; margin: 0px">
                                    <a href="{{ $link = url('https://www.blissful.co.ke/email/1/'.$token)}}"
                                       style="border: 1px solid #75848d; border-radius: 20px; color: #75848D; cursor: pointer; display: inline-block; font-family: Arial; font-weight: bold; height: 35px; line-height: 36px; text-align: center; text-decoration: none !important; width: 35px">1</a>
                                </li>
                                <li style="display: inline-block; margin: 0px">
                                    <a href="{{ $link = url('https://www.blissful.co.ke/email/2/'.$token)}}"
                                       style="border: 1px solid #75848d; border-radius: 20px; color: #75848D; cursor: pointer; display: inline-block; font-family: Arial; font-weight: bold; height: 35px; line-height: 36px; text-align: center; text-decoration: none !important; width: 35px">2</a>
                                </li>
                                <li style="display: inline-block; margin: 0px">
                                    <a href="{{ $link = url('https://www.blissful.co.ke/email/3/'.$token)}}"
                                       style="border: 1px solid #75848d; border-radius: 20px; color: #75848D; cursor: pointer; display: inline-block; font-family: Arial; font-weight: bold; height: 35px; line-height: 36px; text-align: center; text-decoration: none !important; width: 35px">3</a>
                                </li>
                                <li style="display: inline-block; margin: 0px">
                                    <a href="{{ $link = url('https://www.blissful.co.ke/email/4/'.$token)}}"
                                       style="border: 1px solid #75848d; border-radius: 20px; color: #75848D; cursor: pointer; display: inline-block; font-family: Arial; font-weight: bold; height: 35px; line-height: 36px; text-align: center; text-decoration: none !important; width: 35px">4</a>
                                </li>
                                <li style="display: inline-block; margin: 0px">
                                    <a href="{{ $link = url('https://www.blissful.co.ke/email/5/'.$token)}}"
                                       style="border: 1px solid #75848d; border-radius: 20px; color: #75848D; cursor: pointer; display: inline-block; font-family: Arial; font-weight: bold; height: 35px; line-height: 36px; text-align: center; text-decoration: none !important; width: 35px">5</a>
                                </li>
                                <li style="display: inline-block; margin: 0px">
                                    <a href="{{ $link = url('https://www.blissful.co.ke/email/6/'.$token)}}"
                                       style="border: 1px solid #75848d; border-radius: 20px; color: #75848D; cursor: pointer; display: inline-block; font-family: Arial; font-weight: bold; height: 35px; line-height: 36px; text-align: center; text-decoration: none !important; width: 35px">6</a>
                                </li>
                                <li style="display: inline-block; margin: 0px">
                                    <a href="{{ $link = url('https://www.blissful.co.ke/email/7/'.$token)}}"
                                       style="border: 1px solid #75848d; border-radius: 20px; color: #75848D; cursor: pointer; display: inline-block; font-family: Arial; font-weight: bold; height: 35px; line-height: 36px; text-align: center; text-decoration: none !important; width: 35px">7</a>
                                </li>
                                <li style="display: inline-block; margin: 0px">
                                    <a href="{{ $link = url('https://www.blissful.co.ke/email/8/'.$token)}}"
                                       style="border: 1px solid #75848d; border-radius: 20px; color: #75848D; cursor: pointer; display: inline-block; font-family: Arial; font-weight: bold; height: 35px; line-height: 36px; text-align: center; text-decoration: none !important; width: 35px">8</a>
                                </li>
                                <li style="display: inline-block; margin: 0px">
                                    <a href="{{ $link = url('https://www.blissful.co.ke/email/9/'.$token)}}"
                                       style="border: 1px solid #75848d; border-radius: 20px; color: #75848D; cursor: pointer; display: inline-block; font-family: Arial; font-weight: bold; height: 35px; line-height: 36px; text-align: center; text-decoration: none !important; width: 35px">9</a>
                                </li>
                                <li style="display: inline-block; margin: 0px">
                                    <a href="{{ $link = url('https://www.blissful.co.ke/email/10/'.$token)}}"
                                       style="border: 1px solid #75848d; border-radius: 20px; color: #75848D; cursor: pointer; display: inline-block; font-family: Arial; font-weight: bold; height: 35px; line-height: 36px; text-align: center; text-decoration: none !important; width: 35px">10</a>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="500px"
                        style="color: #313131; font-family: Arial; font-size: 14px; line-height: 20px; padding-bottom: 10px">
                        <div style="display: inline-block; float: left; padding-left: 32px">Not at all likely</div>
                        <div style="display: inline-block; float: right; padding-right: 32px">Extremely likely</div>
                        <div class="">
                            <p class="intercom-align-left"
                               style="color: #75848D !important; margin: 0px; padding: 0 30px 20px; text-align: left !important; width: 440px !important"
                               align="left"></p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 0px">
                        <img src="https://blissful.co.ke/img/newhome/intro-bkgd.jpg" alt="footer image of desk"
                             width="500px" height="139px">
                    </td>
                </tr>
            </table>
        </td>
        <td bgcolor="#E9EFF2"></td>
    </tr>
    <tr bgcolor="#E9EFF2">
        <td style="font-size: 0; line-height: 0"></td>
        <td align="center">
            <table width="500px" style="clear: both !important" align="center" border="0" cellpadding="0"
                   cellspacing="0" bgcolor="#E9EFF2">
                <tbody>
                <tr>
                    <td style="height: 50px"></td>
                </tr>
                <tr>
                    <td style="font-size: 0; line-height: 0"></td>
                    <td align="center"
                        style="clear: both !important; display: block !important; margin: 0 auto; padding: 0 30px; width: 500px">
                        <div>
                            <img src="https://blissful.co.ke/img/blissful-bw-small.png" alt="Azendoo" width="34px"
                                 height="34px" style="max-width: 100%">
                            <p style="color: #75848D !important; font-weight: normal; margin: 0 auto; padding: 14px 30px 20px; width: 440px !important">
                                Get the best quotes for your Event....</p>
                        </div>
                    </td>
                    <td style="font-size: 0; line-height: 0"></td>
                </tr>
                <tr>
                    <td style="font-size: 0; line-height: 0"></td>
                    <td>
                        <div align="center" style="padding: 10px 0px">
                            <p style="color: #75848D !important; font-weight: normal; margin: 0px; padding: 0 30px 20px; width: 440px !important">
                                © 2016
                                Blissful.
                                All Rights
                                Reserved.</p>
                        </div>
                    </td>
                    <td style="font-size: 0; line-height: 0"></td>
                </tr>
                </tbody>
            </table>
        </td>
        <td style="font-size: 0; line-height: 0"></td>
    </tr>
    </tbody>
</table>

</body>

</html>