<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>{{$subject}}</title>
    <div id="frame" class="empty">


        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">


        <table data-module="Hero" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-spacing:
        0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
            <tbody>
            <tr>
                <td data-bgcolor="Body Background" align="center" bgcolor="ffffff" width="100%"
                    style="border-collapse: collapse;">
                    <table style="max-width: 960px;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"
                           align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                        <tr>
                            <td style="background-position: center center;background-repeat: no-repeat;background-size: cover;border-collapse: collapse;"
                                data-bgcolor="Hero Background" data-bg="Hero Background Image" data-link-color="Hero Links"
                                data-link-style="color: #0e2d3b; text-decoration: underline;" bgcolor="ffffff" valign="top">
                                <table class="container"
                                       style="width: 650px;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"
                                       align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                    <tr>
                                        <td style="padding: 0 10px 0 10px;border-collapse: collapse;" align="left"
                                            valign="top">
                                            <!-- Top Bar -->
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                                   style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                <tbody>
                                                <tr>
                                                    <td style="padding-top: 10px;padding-bottom: 10px;border-collapse: collapse;"
                                                        align="left" valign="top" width="100%">
                                                        <!-- Preheader -->
                                                        <table class="columnContainer" align="left" border="0"
                                                               cellpadding="0" cellspacing="0" width="400"
                                                               style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                            <tbody>
                                                            <tr>
                                                                <td style="padding-top: 8px;padding-bottom: 8px;font-size: 16px;line-height: 24px;font-family: Arial,Helvetica,sans-serif;color: #ffffff;text-align: left;border-collapse: collapse;"
                                                                    data-color="Hero Preheader" align="left" valign="top">
                                                                    <a href="#"
                                                                       style="font-weight: bold;color: #ffffff;text-decoration: none;font-size: 45px;line-height: 0.72em;margin: 0;padding: 0;"><img
                                                                                src="https://gallery.mailchimp.com/2dda4c199c168004c3ee71cee/images/3e19b9c6-e91c-40fb-85a1-03d0c8104d03.png"
                                                                                alt="Blissful"
                                                                                style="display: block;border: none;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;"
                                                                                align="left" border="0"></a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!-- // END Preheader -->
                                                        <!-- Icons -->
                                                        <table class="columnContainer" align="right" border="0"
                                                               cellpadding="0" cellspacing="0"
                                                               style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                            <tbody>
                                                            <tr>
                                                                <td style="padding-top: 5px;padding-bottom: 5px;border-collapse: collapse;"
                                                                    align="left" valign="top">
                                                                    <table class="smIcons" border="0" cellpadding="0"
                                                                           cellspacing="0"
                                                                           style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td style="padding-left: 10px;border-collapse: collapse;">
                                                                                <a href="www.facebook.com/blissfulke"
                                                                                   style="margin: 0;padding: 0;"><img
                                                                                            src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2016/02/13/Sc3QiaVMPIoeXk72yx6JOKFm/index-template/images/icon-sm2-facebook.png"
                                                                                            alt="Facebook"
                                                                                            style="display: block;border: none;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;"
                                                                                            height="30" border="0"
                                                                                            width="30"></a>
                                                                            </td>
                                                                            <td style="padding-left: 10px;border-collapse: collapse;">
                                                                                <a href="www.twitter.com/blissfulke"
                                                                                   style="margin: 0;padding: 0;"><img
                                                                                            src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2016/02/13/Sc3QiaVMPIoeXk72yx6JOKFm/index-template/images/icon-sm2-twitter.png"
                                                                                            alt="Twitter"
                                                                                            style="display: block;border: none;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;"
                                                                                            height="30" border="0"
                                                                                            width="30"></a>
                                                                            </td>

                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!-- // END Icons -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 1px;line-height: 1px;border-collapse: collapse;"
                                                        data-bgcolor="Hero Topbar Divider" height="2" bgcolor="#ffffff"
                                                        width="100%">&nbsp;</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <!-- // END Top Bar -->

                                            <!-- Content -->
                                            <table class="columnContainer" align="left" border="0" cellpadding="0"
                                                   cellspacing="0" width="100%"
                                                   style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                <tbody>
                                                <tr>
                                                    <td style="font-size: 1px;line-height: 1px;border-collapse: collapse;"
                                                        class="heroSpacing" height="20" width="100%">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 30px;line-height: 36px;font-weight: bold;letter-spacing: -0.02em;font-family: Arial,Helvetica,sans-serif;color: #0e2d3b;text-align: left;border-collapse: collapse;"
                                                        data-color="Hero Headline">
                                                        {{$first_name}}, here are today's top jobs
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 18px;line-height: 20px;font-family: Arial,Helvetica,sans-serif;color: #0e2d3b;text-align: left;padding-top: 20px;border-collapse: collapse;"
                                                        data-color="Hero Text">
                                                        www.blissful.co.ke - the smarter way to find customers and grow your
                                                        business
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-top: 0px;border-collapse: collapse;" valign="top">

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 1px;line-height: 1px;border-collapse: collapse;"
                                                        class="heroSpacing" height="40" width="100%">&nbsp;</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <!-- // END Content -->
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>



        <!-- End Content -->
        <table data-module="Events" align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
               style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
            <tbody>
            <tr>
                <td data-bgcolor="Body Background" align="center" bgcolor="#ffffff" width="100%"
                    style="border-collapse: collapse;">
                    <table style="max-width: 960px;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"
                           align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                        <tr>
                            <td data-bgcolor="Events Background" data-link-color="Events Links"
                                data-link-style="color: #0e2d3b; text-decoration: underline;" bgcolor="#ffffff"
                                valign="top" style="border-collapse: collapse;">
                                <table class="container"
                                       style="width: 650px;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"
                                       align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                    <tr>
                                        <td style="padding: 40px 10px 40px 10px;border-collapse: collapse;"
                                            valign="top">
                                            <!-- Title -->
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                                   style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                <tbody>
                                                <tr>
                                                    <td style="font-size: 1px;line-height: 1px;border-collapse: collapse;"
                                                        height="20" width="100%">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="100%" style="border-collapse: collapse;">
                                                        <table class="columnContainer" align="left" border="0"
                                                               cellpadding="0" cellspacing="0" width="400"
                                                               style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                            <tbody>
                                                            <tr>
                                                                <td style="font-family: Arial,Helvetica,sans-serif;font-size: 22px;line-height: 32px;color: #0e2d3b;text-transform: uppercase;border-collapse: collapse;"
                                                                    data-color="Events Headline" align="left"
                                                                    valign="top">
                                                                    Top Jobs Today
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <table class="mobileHide" align="right" border="0"
                                                               cellpadding="0" cellspacing="0"
                                                               style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                            <tbody>
                                                            <tr>
                                                                <td align="left" valign="top"
                                                                    style="border-collapse: collapse;">
                                                                    <!-- Button -->
                                                                    <table style="border: 2px solid 0e2d3b;color: #0e2d3b;border-radius: 25px;font-family: Arial,Helvetica,sans-serif;font-weight: bold;text-transform: uppercase;display: inline-block;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"
                                                                           data-border-top-color="Events Button Border"
                                                                           data-border-bottom-color="Events Button Border"
                                                                           data-border-right-color="Events Button Border"
                                                                           data-border-left-color="Events Button Border"
                                                                           data-color="Events Button Text" align="left"
                                                                           border="0" cellpadding="0" cellspacing="0">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td style="padding: 0 12px 0 12px;border-collapse: collapse;"
                                                                                align="center" height="28">
                                                                                <a href="http://blissful.co.ke/dashboard?utm_source=newsletter&utm_medium=email&utm_campaign=Newsletter"
                                                                                   style="color: #0e2d3b;text-decoration: none;font-size: 14px;line-height: 28px;margin: 0;padding: 0;"
                                                                                   data-color="Events Button Text">View
                                                                                    all</a>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <!-- // END Button -->
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 1px;line-height: 1px;border-collapse: collapse;"
                                                        height="20" width="100%">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 1px;line-height: 1px;border-collapse: collapse;"
                                                        data-bgcolor="Events Headline Divider" height="2"
                                                        bgcolor="#0e2d3b" width="100%">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 1px;line-height: 1px;border-collapse: collapse;"
                                                        height="20" width="100%">&nbsp;</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <!-- // END Title -->
                                            <!-- Item -->
                                            <?php $serializedObject = unserialize($list_of_jobs); ?>
                                            @foreach($serializedObject as $_obj)
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                                       style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="font-size: 1px;line-height: 1px;border-collapse: collapse;"
                                                            height="0" width="100%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" width="100%"
                                                            style="border-collapse: collapse;">
                                                            <table class="columnContainer" align="left" border="0"
                                                                   cellpadding="0" cellspacing="0" width="100"
                                                                   style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                <tbody>
                                                                <tr class="eventDate">
                                                                    <td style="padding: 4px 10px 2px 0; font-family: Arial,Helvetica,sans-serif; font-size: 36px; line-height: 0.8em; color: #0e2d3b; font-weight: bold;" data-color="Events Date" align="center" width="45px">{{ date('j',strtotime($_obj->event_date)) }}</td>
                                                                    <td style="font-size: 1px; line-height: 1px;" data-bgcolor="Events Date Divider" bgcolor="#0e2d3b" width="2">&nbsp;</td>
                                                                    <td style="padding: 4px 0 2px 10px; font-family: Arial,Helvetica,sans-serif; font-size: 12px; line-height: 17px; color: #0e2d3b;" data-color="Events Date Mo/Year" align="left">{{ date('M',strtotime($_obj->event_date)) }}<br>{{ date('Y',strtotime($_obj->event_date)) }}</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <!-- Spacing -->
                                                            <table class="columnSpacing" align="left" border="0"
                                                                   cellpadding="0" cellspacing="0" width="1"
                                                                   style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                <tbody>
                                                                <tr>
                                                                    <td style="font-size: 1px;line-height: 1px;border-collapse: collapse;"
                                                                        height="15" width="100%">&nbsp;</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <!-- // END Spacing -->
                                                            <table class="columnContainer" align="right" border="0"
                                                                   cellpadding="0" cellspacing="0" width="500"
                                                                   style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                <tbody>
                                                                <tr>
                                                                    <td style="font-size: 18px;line-height: 28px;font-weight: bold;font-family: Arial,Helvetica,sans-serif;color: #0e2d3b;text-align: left;border-collapse: collapse;">
                                                                        <a href="http://www.blissful.co.ke/dashboard?utm_source=newsletter&utm_medium=email&utm_campaign=Newsletter"
                                                                           style="text-decoration: none;color: #0e2d3b;margin: 0;padding: 0;"
                                                                           data-color="Events Title">{{$_obj->job_title}}</a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-size: 14px;line-height: 24px;font-family: Arial,Helvetica,sans-serif;color: #81848d;text-align: left;padding-top: 5px;border-collapse: collapse;"
                                                                        data-color="Events Text">
                                                                        {{$_obj->description}}
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 1px;line-height: 1px;border-collapse: collapse;"
                                                            height="10" width="100%">&nbsp;</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                        @endforeach
                                        <!-- // END Item -->

                                            <table data-module="Footer"
                                                   data-thumb="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2016/02/13/Sc3QiaVMPIoeXk72yx6JOKFm/index-template/thumbnails/module-footer.jpg"
                                                   align="center" border="0" cellpadding="0" cellspacing="0"
                                                   width="100%"
                                                   style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                <tbody>
                                                <tr>
                                                    <td data-bgcolor="Body Background" align="center" bgcolor="#e2e5e7"
                                                        width="100%" style="border-collapse: collapse;">
                                                        <table style="max-width: 960px;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"
                                                               align="center" border="0" cellpadding="0" cellspacing="0"
                                                               width="100%">
                                                            <tbody>
                                                            <tr>
                                                                <td data-bgcolor="Footer Background"
                                                                    data-link-color="Footer Links"
                                                                    data-link-style="color: #2e2e61; text-decoration: underline;"
                                                                    bgcolor="#ffffff" valign="top"
                                                                    style="border-collapse: collapse;">
                                                                    <table class="container"
                                                                           style="width: 650px;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"
                                                                           align="center" border="0" cellpadding="0"
                                                                           cellspacing="0">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td style="padding: 60px 10px 60px 10px;border-collapse: collapse;"
                                                                                align="left" valign="top">
                                                                                <!-- Logo -->
                                                                                <table class="mobileHide" align="left"
                                                                                       border="0" cellpadding="0"
                                                                                       cellspacing="0" width="85"
                                                                                       style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td style="font-family: Arial,Helvetica,sans-serif;border-collapse: collapse;"
                                                                                            align="left" valign="top">
                                                                                            <a href="#"
                                                                                               style="font-weight: bold;color: #2e2e61;text-decoration: none;font-size: 22px;line-height: 0.75em;margin: 0;padding: 0;"><img
                                                                                                        src="https://gallery.mailchimp.com/2dda4c199c168004c3ee71cee/images/3e19b9c6-e91c-40fb-85a1-03d0c8104d03.png"
                                                                                                        alt="Blissful"
                                                                                                        style="display: block;border: none;outline: none;text-decoration: none;width: 85px !important;height: auto !important;-ms-interpolation-mode: bicubic;"
                                                                                                        border="0"></a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <!-- // END Logo -->
                                                                                <!-- Spacing -->
                                                                                <table class="mobileHide" align="left"
                                                                                       border="0" cellpadding="0"
                                                                                       cellspacing="0" width="1"
                                                                                       style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td style="font-size: 1px;line-height: 1px;border-collapse: collapse;"
                                                                                            height="30" width="100%">
                                                                                            &nbsp;</td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <!-- // END Spacing -->
                                                                                <!-- Content -->
                                                                                <table class="columnContainer"
                                                                                       align="right" border="0"
                                                                                       cellpadding="0" cellspacing="0"
                                                                                       width="505"
                                                                                       style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td style="font-size: 1px;line-height: 1px;border-collapse: collapse;"
                                                                                            class="mobileHide"
                                                                                            data-bgcolor="Footer Divider"
                                                                                            bgcolor="#2e2e61" width="2">
                                                                                            &nbsp;</td>
                                                                                        <td style="padding-left: 40px;border-collapse: collapse;"
                                                                                            class="footerContent"
                                                                                            align="left" valign="top">
                                                                                            <table align="left"
                                                                                                   border="0"
                                                                                                   cellpadding="0"
                                                                                                   cellspacing="0"
                                                                                                   width="100%"
                                                                                                   style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                                                <!-- Icons -->
                                                                                                <tbody>
                                                                                                <tr>
                                                                                                    <td style="padding-bottom: 25px;font-family: Arial,Helvetica,sans-serif;font-size: 14px;line-height: 22px;color: #91949b;border-collapse: collapse;"
                                                                                                        align="left"
                                                                                                        valign="top">
                                                                                                        <table class="smIcons"
                                                                                                               align="left"
                                                                                                               border="0"
                                                                                                               cellpadding="0"
                                                                                                               cellspacing="0"
                                                                                                               style="border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                                                            <tbody>
                                                                                                            <tr>
                                                                                                                <td style="padding-right: 10px;border-collapse: collapse;">
                                                                                                                    <a href="www.facebook.com/blissfulke"
                                                                                                                       style="text-decoration: none;margin: 0;padding: 0;"><img
                                                                                                                                src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2016/02/13/Sc3QiaVMPIoeXk72yx6JOKFm/index-template/images/icon-sm2-facebook.png"
                                                                                                                                alt="Facebook"
                                                                                                                                style="display: block;border: none;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;"
                                                                                                                                height="40"
                                                                                                                                border="0"
                                                                                                                                width="40"></a>
                                                                                                                </td>
                                                                                                                <td style="padding-right: 10px;border-collapse: collapse;">
                                                                                                                    <a href="www.twitter.com/blissfulke"
                                                                                                                       style="text-decoration: none;margin: 0;padding: 0;"><img
                                                                                                                                src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2016/02/13/Sc3QiaVMPIoeXk72yx6JOKFm/index-template/images/icon-sm2-twitter.png"
                                                                                                                                alt="Twitter"
                                                                                                                                style="display: block;border: none;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;"
                                                                                                                                height="40"
                                                                                                                                border="0"
                                                                                                                                width="40"></a>
                                                                                                                </td>

                                                                                                            </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <!-- // END Icons -->
                                                                                                <!-- Text -->
                                                                                                <tr>
                                                                                                    <td style="font-family: Arial,Helvetica,sans-serif;font-size: 14px;line-height: 22px;color: #91949b;border-collapse: collapse;"
                                                                                                        data-color="Footer Text"
                                                                                                        align="left"
                                                                                                        valign="top">
                                                                                                        <p style="padding: 0 0 10px 0;margin: 0;">
                                                                                                            © 2016
                                                                                                            Blissful.
                                                                                                            All Rights
                                                                                                            Reserved.<br>
                                                                                                        </p>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <!-- // END Text -->
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <!-- // END Content -->
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>


                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</head>
</html>