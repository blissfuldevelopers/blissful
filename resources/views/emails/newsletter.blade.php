<!doctype html>
<html xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraph.org/schema/">
<head>
    <meta property="og:title" content="Deals on chiavari chairs, tents and decor items">
    <meta property="fb:page_id" content="43929265776">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><!-- NAME: POP-UP -->
    <!--[if gte mso 15]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Deals on chiavari chairs, tents and decor items</title>
</head>
<body leftmargin="0" marginheight="0" marginwidth="0" offset="0"
      style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #f6f9fb;"
      topmargin="0">
<center>
    <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" id="bodyTable"
           style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #f6f9fb;"
           width="100%">
        <tbody>
        <tr>
            <td align="center" id="bodyCell"
                style="padding-bottom: 40px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;border-top: 0;"
                valign="top"><!-- BEGIN TEMPLATE // -->
                <table border="0" cellpadding="0" cellspacing="0"
                       style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                       width="100%">
                    <tbody>
                    <tr>
                        <td align="center"
                            style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                            valign="top"><!-- BEGIN PREHEADER // -->
                            <table border="0" cellpadding="0" cellspacing="0" id="templatePreheader"
                                   style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #f6f9fb;border-top: 0;border-bottom: 0;"
                                   width="100%">
                                <tbody>
                                <tr>
                                    <td align="center"
                                        style="padding-right: 10px;padding-left: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                        valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" class="templateContainer"
                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                               width="600">
                                            <tbody>
                                            <tr>
                                                <td align="center"
                                                    style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                    valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0"
                                                           id="preheaderBackground"
                                                           style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #f6f9fb;border-top: 0;border-bottom: 0;"
                                                           width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td class="preheaderContainer"
                                                                style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnTextBlock"
                                                                       style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                       width="100%">
                                                                    <tbody class="mcnTextBlockOuter">
                                                                    <tr>
                                                                        <td class="mcnTextBlockInner"
                                                                            style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                            valign="top"><!--[if mso]>
                                                                            <table align="left" border="0"
                                                                                   cellspacing="0" cellpadding="0"
                                                                                   width="100%" style="width:100%;">
                                                                                <tr>
                                                                            <![endif]--><!--[if mso]>
                                                                            <td valign="top" width="600"
                                                                                style="width:600px;">
                                                                            <![endif]-->
                                                                            <table align="left" border="0"
                                                                                   cellpadding="0" cellspacing="0"
                                                                                   class="mcnTextContentContainer"
                                                                                   style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td class="mcnTextContent"
                                                                                        style="padding: 0px 18px 9px;color: #9C9C9C;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;font-family: Helvetica;font-size: 10px;line-height: 125%;text-align: left;"
                                                                                        valign="top">
                                                                                        <div style="text-align: right;">
                                                                                            <span style="line-height:1.6"><a
                                                                                                        href="https://www.twitter.com/blissfulke"
                                                                                                        style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #c5c5c5;font-weight: normal;text-decoration: underline;"><span
                                                                                                            style="color:#808080">Follow us on Twitter</span></a> <span
                                                                                                        style="color:#808080"> |&nbsp;</span> </span><a
                                                                                                    href="https://www.facebook.com/blissfulke"
                                                                                                    style="line-height: 1.6;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #c5c5c5;font-weight: normal;text-decoration: underline;"
                                                                                                    target="_blank"><span
                                                                                                        style="color:#808080">Like Us on Facebook</span></a>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <!--[if mso]>
                                                                            </td>
                                                                            <![endif]--><!--[if mso]>
                                                                            </tr>
                                                                            </table>
                                                                            <![endif]--></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- // END PREHEADER --></td>
                    </tr>
                    <tr>
                        <td align="center"
                            style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                            valign="top"><!-- BEGIN HEADER // -->
                            <table border="0" cellpadding="0" cellspacing="0" id="templateHeader"
                                   style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #f6f9fb;border-top: 0;border-bottom: 0;"
                                   width="100%">
                                <tbody>
                                <tr>
                                    <td align="center"
                                        style="padding-right: 10px;padding-left: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                        valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" class="templateContainer"
                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                               width="600">
                                            <tbody>
                                            <tr>
                                                <td align="center"
                                                    style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                    valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0"
                                                           id="headerBackground"
                                                           style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #f6f9fb;border-top: 0;border-bottom: 0;"
                                                           width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td class="headerContainer"
                                                                style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnImageBlock"
                                                                       style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                       width="100%">
                                                                    <tbody class="mcnImageBlockOuter">
                                                                    <tr>
                                                                        <td class="mcnImageBlockInner"
                                                                            style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                            valign="top">
                                                                            <table align="left" border="0"
                                                                                   cellpadding="0" cellspacing="0"
                                                                                   class="mcnImageContentContainer"
                                                                                   style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td class="mcnImageContent"
                                                                                        style="padding-right: 9px;padding-left: 9px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                        valign="top"><span
                                                                                                class="sg-image"
                                                                                                data-imagelibrary="%7B%22width%22%3A%22150%22%2C%22height%22%3A%2262%22%2C%22alignment%22%3A%22%22%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/66d53de90122eae54d5762dcb82c98355564d8f38bd603476b518ba73fb73062ef6923bfc90c40aa43bb09aff6e220cf8819c83f4decf9c2f66bf221b0c7fa98.png%22%2C%22alt_text%22%3A%22%22%2C%22link%22%3A%22%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img
                                                                                                    height="62"
                                                                                                    src="https://marketing-image-production.s3.amazonaws.com/uploads/66d53de90122eae54d5762dcb82c98355564d8f38bd603476b518ba73fb73062ef6923bfc90c40aa43bb09aff6e220cf8819c83f4decf9c2f66bf221b0c7fa98.png"
                                                                                                    style="width: 150px;height: 62px;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;"
                                                                                                    width="150"></span>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnBoxedTextBlock"
                                                                       style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                       width="100%"><!--[if gte mso 9]>
                                                                    <table align="center" border="0" cellspacing="0"
                                                                           cellpadding="0" width="100%">
                                                                    <![endif]-->
                                                                    <tbody class="mcnBoxedTextBlockOuter">
                                                                    <tr>
                                                                        <td class="mcnBoxedTextBlockInner"
                                                                            style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                            valign="top"><!--[if gte mso 9]>
                                                                            <td align="center" valign="top" ">
                                                                            <![endif]-->
                                                                            <table align="left" border="0"
                                                                                   cellpadding="0" cellspacing="0"
                                                                                   class="mcnBoxedTextContentContainer"
                                                                                   style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td style="padding-top: 9px;padding-left: 18px;padding-bottom: 9px;padding-right: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                        <table border="0"
                                                                                               cellpadding="18"
                                                                                               cellspacing="0"
                                                                                               class="mcnTextContentContainer"
                                                                                               style="min-width: 100% !important;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                               width="100%">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td class="mcnTextContent"
                                                                                                    style="color: #727272;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align: left;"
                                                                                                    valign="top">
                                                                                                    <div style="text-align: center;">
                                                                                                        <span style="color:#1FB6FF"><span
                                                                                                                    style="font-size:25px"><font
                                                                                                                        face="helveticaneue-light, helvetica, sans-serif"><span
                                                                                                                            style="line-height:48px">Supplier Deals - Jan 2017</span></font></span></span>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <!--[if gte mso 9]>
                                                                            </td>
                                                                            <![endif]--><!--[if gte mso 9]>
                                                                            </tr>
                                                                            </table>
                                                                            <![endif]--></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- // END HEADER --></td>
                    </tr>
                    <tr>
                        <td align="center"
                            style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                            valign="top"><!-- BEGIN BODY // -->
                            <table border="0" cellpadding="0" cellspacing="0" id="templateBody"
                                   style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #f6f9fb;border-top: 0;border-bottom: 0;"
                                   width="100%">
                                <tbody>
                                <tr>
                                    <td align="center"
                                        style="padding-right: 10px;padding-left: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                        valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" class="templateContainer"
                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                               width="600">
                                            <tbody>
                                            <tr>
                                                <td align="center"
                                                    style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                    valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0"
                                                           id="bodyBackground"
                                                           style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;border-top: 0;border-bottom: 0;"
                                                           width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td class="bodyContainer"
                                                                style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnImageBlock"
                                                                       style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                       width="100%">
                                                                    <tbody class="mcnImageBlockOuter">
                                                                    <tr>
                                                                        <td class="mcnImageBlockInner"
                                                                            style="padding: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                            valign="top">
                                                                            <table align="left" border="0"
                                                                                   cellpadding="0" cellspacing="0"
                                                                                   class="mcnImageContentContainer"
                                                                                   style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td class="mcnImageContent"
                                                                                        style="padding-right: 0px;padding-left: 0px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                        valign="top"><span
                                                                                                class="sg-image"
                                                                                                data-imagelibrary="%7B%22width%22%3A%22600%22%2C%22height%22%3A%22400%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/192f6657a18d132577a53bb74c36fac9625e6d687718d550232ed1fad0f261458481d4810360f1b77d65d0ca21fe21445990f59784e213abeae27d94ea30a29d.jpg%22%2C%22link%22%3A%22%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a
                                                                                                    href="mailto:happy@blissful.co.ke?subject=Chiavari%20Chairs%20Booking"
                                                                                                    target="_blank"
                                                                                                    title="Chiavari Chairs Offer"
                                                                                                    style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img
                                                                                                        align="middle"
                                                                                                        alt="chiavari chairs"
                                                                                                        class="mcnImage"
                                                                                                        height="400"
                                                                                                        src="https://marketing-image-production.s3.amazonaws.com/uploads/192f6657a18d132577a53bb74c36fac9625e6d687718d550232ed1fad0f261458481d4810360f1b77d65d0ca21fe21445990f59784e213abeae27d94ea30a29d.jpg"
                                                                                                        style="border-style: none;height: 400px;outline-width: medium;max-width: 600px;padding-bottom: 0px;width: 600px;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;vertical-align: bottom;"
                                                                                                        width="600"></a></span>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnBoxedTextBlock"
                                                                       style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                       width="100%"><!--[if gte mso 9]>
                                                                    <table align="center" border="0" cellspacing="0"
                                                                           cellpadding="0" width="100%">
                                                                    <![endif]-->
                                                                    <tbody class="mcnBoxedTextBlockOuter">
                                                                    <tr>
                                                                        <td class="mcnBoxedTextBlockInner"
                                                                            style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                            valign="top"><!--[if gte mso 9]>
                                                                            <td align="center" valign="top" ">
                                                                            <![endif]-->
                                                                            <table align="left" border="0"
                                                                                   cellpadding="0" cellspacing="0"
                                                                                   class="mcnBoxedTextContentContainer"
                                                                                   style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td style="padding-top: 9px;padding-left: 18px;padding-bottom: 9px;padding-right: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                        <table border="0"
                                                                                               cellpadding="18"
                                                                                               cellspacing="0"
                                                                                               class="mcnTextContentContainer"
                                                                                               style="min-width: 100% !important;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                               width="100%">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td class="mcnTextContent"
                                                                                                    style="color: rgb(114, 114, 114);text-size-adjust: 100%;word-break: break-word;font-family: Helvetica;line-height: 150%;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-size: 18px;"
                                                                                                    valign="top">
                                                                                                    <div style="font-size: 18px; text-align: center;">
                                                                                                        <span style="color:#000000"><span
                                                                                                                    style="font-size:25px"><font
                                                                                                                        face="helveticaneue-light, helvetica, sans-serif"><span
                                                                                                                            style="line-height:48px">Chiavari Chairs For Sale</span></font></span></span>
                                                                                                    </div>

                                                                                                    <div style="text-align: center;">
                                                                                                        <span style="font-size: 16px;">Invest in chiavari chairs in 2017 and grow your business expontially. </span>
                                                                                                    </div>

                                                                                                    <div style="text-align: center;">
                                                                                                        <span style="color: rgb(114, 114, 114); font-family: Helvetica; font-size: 16px; text-align: center; background-color: rgb(255, 255, 255);">Available in white or&nbsp;gold</span><span
                                                                                                                style="font-size: 16px;"> </span>
                                                                                                    </div>

                                                                                                    <div style="text-align: center;">
                                                                                                        <div style="color: rgb(114, 114, 114); font-family: Helvetica; font-size: 18px; text-align: center; background-color: rgb(255, 255, 255);">
                                                                                                            <span style="font-size: 16px;">Book yours today for only&nbsp;<strong>KES. 500</strong></span>
                                                                                                        </div>

                                                                                                        <div style="color: rgb(114, 114, 114); font-family: Helvetica; font-size: 18px; text-align: center; background-color: rgb(255, 255, 255);">
                                                                                                            <span style="font-size: 12px;">Sold in multiples of 50 chairs with minimum order of 100 chairs.</span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <!--[if gte mso 9]>
                                                                            </td>
                                                                            <![endif]--><!--[if gte mso 9]>
                                                                            </tr>
                                                                            </table>
                                                                            <![endif]--></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnButtonBlock"
                                                                       style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                       width="100%">
                                                                    <tbody class="mcnButtonBlockOuter">
                                                                    <tr>
                                                                        <td align="center" class="mcnButtonBlockInner"
                                                                            style="padding-top: 0;padding-right: 18px;padding-bottom: 18px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                            valign="top">
                                                                            <table border="0" cellpadding="0"
                                                                                   cellspacing="0"
                                                                                   class="mcnButtonContentContainer"
                                                                                   style="border-collapse: separate !important;border-radius: 5px;background-color: #13CE66;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td align="center"
                                                                                        class="mcnButtonContent"
                                                                                        style="font-family: Arial;font-size: 16px;padding: 20px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                        valign="middle"><a
                                                                                                class="mcnButton "
                                                                                                href="mailto:happy@blissful.co.ke?subject=Chiavari%20Chairs%20Booking"
                                                                                                post=""
                                                                                                style="font-weight: normal;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: block;"
                                                                                                the="" title="“Read">Book
                                                                                            Yours Now</a></td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnDividerBlock"
                                                                       style="min-width: 100%;background-color: #F6F9FB;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;table-layout: fixed !important;"
                                                                       width="100%">
                                                                    <tbody class="mcnDividerBlockOuter">
                                                                    <tr>
                                                                        <td class="mcnDividerBlockInner"
                                                                            style="min-width: 100%;padding: 18px 18px 40px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                            <table border="0" cellpadding="0"
                                                                                   cellspacing="0"
                                                                                   class="mcnDividerContent"
                                                                                   style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                        &nbsp;</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <!--
                    <td class="mcnDividerBlockInner" style="padding: 18px;">
                    <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
    --></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnImageBlock"
                                                                       style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                       width="100%">
                                                                    <tbody class="mcnImageBlockOuter">
                                                                    <tr>
                                                                        <td class="mcnImageBlockInner"
                                                                            style="padding: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                            valign="top">
                                                                            <table align="left" border="0"
                                                                                   cellpadding="0" cellspacing="0"
                                                                                   class="mcnImageContentContainer"
                                                                                   style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td class="mcnImageContent"
                                                                                        style="padding-right: 0px;padding-left: 0px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                        valign="top"><span
                                                                                                class="sg-image"
                                                                                                data-imagelibrary="%7B%22width%22%3A%22600%22%2C%22height%22%3A%22400%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/0c95b34923ec5241e94c65a7b5be96531f5fc656f13196405a08ab8eb60a8684b62665381dafd672a661b2c9c82e7229dd535ed477b0365717ee8581a9d16e40.jpg%22%2C%22link%22%3A%22%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a
                                                                                                    href="mailto:happy@blissful.co.ke?subject=Hexagon%20Tents%20Booking"
                                                                                                    style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                                    target="_blank"
                                                                                                    title="Hexagon Tents Offer"><img
                                                                                                        align="middle"
                                                                                                        alt="hexagon / alpine tents offer"
                                                                                                        class="mcnImage"
                                                                                                        height="400"
                                                                                                        src="https://marketing-image-production.s3.amazonaws.com/uploads/0c95b34923ec5241e94c65a7b5be96531f5fc656f13196405a08ab8eb60a8684b62665381dafd672a661b2c9c82e7229dd535ed477b0365717ee8581a9d16e40.jpg"
                                                                                                        style="max-width: 600px;padding-bottom: 0px;vertical-align: bottom;border: 0px none;height: 400px;outline: medium none;text-decoration: none;width: 600px;-ms-interpolation-mode: bicubic;"
                                                                                                        width="600"></a></span>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnBoxedTextBlock"
                                                                       style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                       width="100%"><!--[if gte mso 9]>
                                                                    <table align="center" border="0" cellspacing="0"
                                                                           cellpadding="0" width="100%">
                                                                    <![endif]-->
                                                                    <tbody class="mcnBoxedTextBlockOuter">
                                                                    <tr>
                                                                        <td class="mcnBoxedTextBlockInner"
                                                                            style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                            valign="top"><!--[if gte mso 9]>
                                                                            <td align="center" valign="top" ">
                                                                            <![endif]-->
                                                                            <table align="left" border="0"
                                                                                   cellpadding="0" cellspacing="0"
                                                                                   class="mcnBoxedTextContentContainer"
                                                                                   style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td style="padding-top: 9px;padding-left: 18px;padding-bottom: 9px;padding-right: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                        <table border="0"
                                                                                               cellpadding="18"
                                                                                               cellspacing="0"
                                                                                               class="mcnTextContentContainer"
                                                                                               style="min-width: 100% !important;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                               width="100%">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td class="mcnTextContent"
                                                                                                    style="text-size-adjust: 100%;word-break: break-word;line-height: 150%;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #333333;font-family: Helvetica;font-size: 18px;"
                                                                                                    valign="top">
                                                                                                    <div style="text-align: center;">
                                                                                                        <font color="#000000"
                                                                                                              face="helveticaneue-light, helvetica, sans-serif"><span
                                                                                                                    style="font-size: 25px;">Hexagon / Alpine Offer</span></font>
                                                                                                    </div>

                                                                                                    <div style="color: rgb(114, 114, 114); font-family: Helvetica; text-align: center;">
                                                                                                        <span style="font-size: 16px;">Known for its characteristic majestic look the hexagon tent is the darling of most clients and its clean look guarantees repeat business.</span>
                                                                                                    </div>

                                                                                                    <div style="color: rgb(114, 114, 114); font-family: Helvetica; text-align: center;">
                                                                                                        <span style="font-size: 16px;">Book yours with <strong> KES. 100,000 </strong></span>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <!--[if gte mso 9]>
                                                                            </td>
                                                                            <![endif]--><!--[if gte mso 9]>
                                                                            </tr>
                                                                            </table>
                                                                            <![endif]--></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnButtonBlock"
                                                                       style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                       width="100%">
                                                                    <tbody class="mcnButtonBlockOuter">
                                                                    <tr>
                                                                        <td align="center" class="mcnButtonBlockInner"
                                                                            style="padding-top: 0;padding-right: 18px;padding-bottom: 18px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                            valign="top">
                                                                            <table border="0" cellpadding="0"
                                                                                   cellspacing="0"
                                                                                   class="mcnButtonContentContainer"
                                                                                   style="border-collapse: separate !important;border-radius: 5px;background-color: #13CE66;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td align="center"
                                                                                        class="mcnButtonContent"
                                                                                        style="font-family: Arial;font-size: 16px;padding: 20px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                        valign="middle"><a
                                                                                                class="mcnButton "
                                                                                                href="mailto:happy@blissful.co.ke?subject=Hexagon%20Tents%20Offer"
                                                                                                style="font-weight: normal;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: block;"
                                                                                                title="Read the post">Take
                                                                                            Up The Offer</a></td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnDividerBlock"
                                                                       style="min-width: 100%;background-color: #F6F9FB;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;table-layout: fixed !important;"
                                                                       width="100%">
                                                                    <tbody class="mcnDividerBlockOuter">
                                                                    <tr>
                                                                        <td class="mcnDividerBlockInner"
                                                                            style="min-width: 100%;padding: 18px 18px 40px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                            <table border="0" cellpadding="0"
                                                                                   cellspacing="0"
                                                                                   class="mcnDividerContent"
                                                                                   style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                        &nbsp;</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <!--
                    <td class="mcnDividerBlockInner" style="padding: 18px;">
                    <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
    --></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnImageBlock"
                                                                       style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                       width="100%">
                                                                    <tbody class="mcnImageBlockOuter">
                                                                    <tr>
                                                                        <td class="mcnImageBlockInner"
                                                                            style="padding: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                            valign="top">
                                                                            <table align="left" border="0"
                                                                                   cellpadding="0" cellspacing="0"
                                                                                   class="mcnImageContentContainer"
                                                                                   style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td class="mcnImageContent"
                                                                                        style="padding-right: 0px;padding-left: 0px;padding-top: 0;padding-bottom: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                        valign="top"><span
                                                                                                class="sg-image"
                                                                                                data-imagelibrary="%7B%22width%22%3A%22600%22%2C%22height%22%3A%22400%22%2C%22alt_text%22%3A%22parasol_sale%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/c64e587db4279534bd5e2179b107e17256c0ce47a16cc28dc2d1536851323f49365b93e0ec823784755bbd747f94a14a609b75529f7b2af0be19873bb76fb425.jpg%22%2C%22link%22%3A%22https%3A//www.blissful.co.ke/shop/product/paper-parasol-white%3Futm_source%3Demail%26utm_medium%3Dnewsletter%26utm_campaign%3Ddeals%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a
                                                                                                    for=""
                                                                                                    href="https://www.blissful.co.ke/shop/product/paper-parasol-white?utm_source=email&amp;utm_medium=newsletter&amp;utm_campaign=users"
                                                                                                    sale=""
                                                                                                    style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                                    target="_blank"
                                                                                                    title="“Parasols"><img
                                                                                                        align="left"
                                                                                                        alt="parasol_sale"
                                                                                                        class="mcnImage"
                                                                                                        height="400"
                                                                                                        src="https://marketing-image-production.s3.amazonaws.com/uploads/c64e587db4279534bd5e2179b107e17256c0ce47a16cc28dc2d1536851323f49365b93e0ec823784755bbd747f94a14a609b75529f7b2af0be19873bb76fb425.jpg"
                                                                                                        style="max-width: 600px;padding-bottom: 0px;vertical-align: bottom;border: 0px none;height: 400px;outline: medium none;text-decoration: none;width: 600px;-ms-interpolation-mode: bicubic;"
                                                                                                        width="600"></a></span>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnBoxedTextBlock"
                                                                       style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                       width="100%"><!--[if gte mso 9]>
                                                                    <table align="center" border="0" cellspacing="0"
                                                                           cellpadding="0" width="100%">
                                                                    <![endif]-->
                                                                    <tbody class="mcnBoxedTextBlockOuter">
                                                                    <tr>
                                                                        <td class="mcnBoxedTextBlockInner"
                                                                            style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                            valign="top"><!--[if gte mso 9]>
                                                                            <td align="center" valign="top" ">
                                                                            <![endif]-->
                                                                            <table align="left" border="0"
                                                                                   cellpadding="0" cellspacing="0"
                                                                                   class="mcnBoxedTextContentContainer"
                                                                                   style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td style="padding-top: 9px;padding-left: 18px;padding-bottom: 9px;padding-right: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                        <table border="0"
                                                                                               cellpadding="18"
                                                                                               cellspacing="0"
                                                                                               class="mcnTextContentContainer"
                                                                                               style="min-width: 100% !important;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                               width="100%">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td class="mcnTextContent"
                                                                                                    style="color: rgb(114, 114, 114);text-size-adjust: 100%;word-break: break-word;font-family: Helvetica;line-height: 150%;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-size: 18px;"
                                                                                                    valign="top">
                                                                                                    <div>
                                                                                                        <font color="#000000"
                                                                                                              face="helveticaneue-light, helvetica, sans-serif"
                                                                                                              style="font-size: 18px;"><span
                                                                                                                    style="font-size:25px; line-height:48px">18% Off Parasols</span></font><br>
                                                                                                        <span style="font-size: 16px;">These make for a fantastic and different decoration piece or photo prop.</span>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <!--[if gte mso 9]>
                                                                            </td>
                                                                            <![endif]--><!--[if gte mso 9]>
                                                                            </tr>
                                                                            </table>
                                                                            <![endif]--></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnButtonBlock"
                                                                       style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                       width="100%">
                                                                    <tbody class="mcnButtonBlockOuter">
                                                                    <tr>
                                                                        <td align="center" class="mcnButtonBlockInner"
                                                                            style="padding-top: 0;padding-right: 18px;padding-bottom: 18px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                            valign="top">
                                                                            <table border="0" cellpadding="0"
                                                                                   cellspacing="0"
                                                                                   class="mcnButtonContentContainer"
                                                                                   style="border-collapse: separate !important;border-radius: 5px;background-color: #13CE66;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td align="center"
                                                                                        class="mcnButtonContent"
                                                                                        style="font-family: Arial;font-size: 16px;padding: 20px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                        valign="middle"><a
                                                                                                class="mcnButton "
                                                                                                href="https://www.blissful.co.ke/shop/product/paper-parasol-white?utm_source=email&amp;utm_medium=newsletter&amp;utm_campaign=deals"
                                                                                                style="font-weight: normal;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: block;"
                                                                                                title="Download Now">Buy
                                                                                            Now</a></td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnDividerBlock"
                                                                       style="background-color: #F6F9FB;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;table-layout: fixed !important;"
                                                                       width="100%">
                                                                    <tbody class="mcnDividerBlockOuter">
                                                                    <tr>
                                                                        <td class="mcnDividerBlockInner"
                                                                            style="padding: 18px 18px 40px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                            <table border="0" cellpadding="0"
                                                                                   cellspacing="0"
                                                                                   class="mcnDividerContent"
                                                                                   style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                        &nbsp;</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnImageBlock"
                                                                       style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                       width="100%">
                                                                    <tbody class="mcnImageBlockOuter">
                                                                    <tr>
                                                                        <td class="mcnImageBlockInner"
                                                                            style="padding: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                            valign="top">
                                                                            <table align="left" border="0"
                                                                                   cellpadding="0" cellspacing="0"
                                                                                   class="mcnImageContentContainer"
                                                                                   style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td class="mcnImageContent"
                                                                                        style="padding-right: 0px;padding-left: 0px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                        valign="top"><span
                                                                                                class="sg-image"
                                                                                                data-imagelibrary="%7B%22width%22%3A%22600%22%2C%22height%22%3A%22400%22%2C%22alt_text%22%3A%22decor_shop%22%2C%22alignment%22%3A%22%22%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/22d9e99d7d65e12fc9a7443fc91e8cf0390fadaea3b3048a27a3af8e497c11e8f4e95358210dc2a0a70d2ef39d8279f0b8a20d81ddc39a240139b9070de698c8.jpg%22%2C%22link%22%3A%22https%3A//www.blissful.co.ke/shop/category/decor%3Futm_source%3Demail%26utm_medium%3Dnewsletter%26utm_campaign%3Dusers%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a
                                                                                                    href="https%3A//www.blissful.co.ke/blog/20-interesting-ideas-for-date-night-in-kenya%3Futm_source%3Demail%26utm_medium%3Dnewsletter%26utm_campaign%3Dusers%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"
                                                                                                    style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img
                                                                                                        align="left"
                                                                                                        alt="decor_shop"
                                                                                                        class="mcnImage"
                                                                                                        height="400"
                                                                                                        src="https://marketing-image-production.s3.amazonaws.com/uploads/22d9e99d7d65e12fc9a7443fc91e8cf0390fadaea3b3048a27a3af8e497c11e8f4e95358210dc2a0a70d2ef39d8279f0b8a20d81ddc39a240139b9070de698c8.jpg"
                                                                                                        style="max-width: 600px;padding-bottom: 0px;vertical-align: bottom;border: 0px none;height: 400px;outline: medium none;text-decoration: none;width: 600px;-ms-interpolation-mode: bicubic;"
                                                                                                        width="600"></a></span>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnBoxedTextBlock"
                                                                       style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                       width="100%"><!--[if gte mso 9]>
                                                                    <table align="center" border="0" cellspacing="0"
                                                                           cellpadding="0" width="100%">
                                                                    <![endif]-->
                                                                    <tbody class="mcnBoxedTextBlockOuter">
                                                                    <tr>
                                                                        <td class="mcnBoxedTextBlockInner"
                                                                            style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                            valign="top"><!--[if gte mso 9]>
                                                                            <td align="center" valign="top" ">
                                                                            <![endif]-->
                                                                            <table align="left" border="0"
                                                                                   cellpadding="0" cellspacing="0"
                                                                                   class="mcnBoxedTextContentContainer"
                                                                                   style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td style="padding-top: 9px;padding-left: 18px;padding-bottom: 9px;padding-right: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                        <table border="0"
                                                                                               cellpadding="18"
                                                                                               cellspacing="0"
                                                                                               class="mcnTextContentContainer"
                                                                                               style="min-width: 100% !important;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                               width="100%">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td class="mcnTextContent"
                                                                                                    style="text-size-adjust: 100%;word-break: break-word;line-height: 150%;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #333333;font-family: Helvetica;font-size: 18px;"
                                                                                                    valign="top"><font
                                                                                                            color="#000000"
                                                                                                            face="helveticaneue-light, helvetica, sans-serif"><span
                                                                                                                style="font-size: 25px;">Unique Decor Items</span></font><br>
                                                                                                    <span style="color: rgb(114, 114, 114); font-family: Helvetica; font-size: 16px;">From one of a kind hot-air balloon lanterns to unique butterfly sets and lots more. Step up your decor game in 2017 with these and stand out from the crowd</span>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <!--[if gte mso 9]>
                                                                            </td>
                                                                            <![endif]--><!--[if gte mso 9]>
                                                                            </tr>
                                                                            </table>
                                                                            <![endif]--></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnButtonBlock"
                                                                       style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                       width="100%">
                                                                    <tbody class="mcnButtonBlockOuter">
                                                                    <tr>
                                                                        <td align="center" class="mcnButtonBlockInner"
                                                                            style="padding-top: 0;padding-right: 18px;padding-bottom: 18px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                            valign="top">
                                                                            <table border="0" cellpadding="0"
                                                                                   cellspacing="0"
                                                                                   class="mcnButtonContentContainer"
                                                                                   style="border-collapse: separate !important;border-radius: 5px;background-color: #13CE66;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td align="center"
                                                                                        class="mcnButtonContent"
                                                                                        style="font-family: Arial;font-size: 16px;padding: 20px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                        valign="middle"><a
                                                                                                class="mcnButton "
                                                                                                href="https://www.blissful.co.ke/shop/category/decor?utm_source=email&amp;utm_medium=newsletter&amp;utm_campaign=deals"
                                                                                                style="font-weight: normal;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: block;"
                                                                                                title="Find Out More">Be
                                                                                            Unique</a></td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                                &nbsp;

                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnImageBlock"
                                                                       style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                       width="100%">
                                                                    <tbody class="mcnImageBlockOuter">
                                                                    <tr>
                                                                        <td class="mcnImageBlockInner"
                                                                            style="padding: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                            valign="top">
                                                                            <table align="left" border="0"
                                                                                   cellpadding="0" cellspacing="0"
                                                                                   class="mcnImageContentContainer"
                                                                                   style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td class="mcnImageContent"
                                                                                        style="padding-right: 0px;padding-left: 0px;padding-top: 0;padding-bottom: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                        valign="top">&nbsp;</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- // END BODY --></td>
                    </tr>
                    <tr>
                        <td align="center"
                            style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                            valign="top"><!-- BEGIN FOOTER // -->
                            <table border="0" cellpadding="0" cellspacing="0" id="templateFooter"
                                   style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #273444;border-top: 0;border-bottom: 0;"
                                   width="100%">
                                <tbody>
                                <tr>
                                    <td align="center"
                                        style="padding-right: 10px;padding-left: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                        valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" class="templateContainer"
                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                               width="600">
                                            <tbody>
                                            <tr>
                                                <td align="center"
                                                    style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                    valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0"
                                                           id="footerBackground"
                                                           style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #273444;border-top: 0;border-bottom: 0;"
                                                           width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td class="footerContainer"
                                                                style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnBoxedTextBlock"
                                                                       style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                       width="100%"><!--[if gte mso 9]>
                                                                    <table align="center" border="0" cellspacing="0"
                                                                           cellpadding="0" width="100%">
                                                                    <![endif]-->
                                                                    <tbody class="mcnBoxedTextBlockOuter">
                                                                    <tr>
                                                                        <td class="mcnBoxedTextBlockInner"
                                                                            style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                            valign="top"><!--[if gte mso 9]>
                                                                            <td align="center" valign="top" ">
                                                                            <![endif]-->
                                                                            <table align="left" border="0"
                                                                                   cellpadding="0" cellspacing="0"
                                                                                   class="mcnBoxedTextContentContainer"
                                                                                   style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td style="text-align: center;padding: 9px 18px;text-size-adjust: 100%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                        <font color="#ffffff"
                                                                                              face="helveticaneue-light, helvetica, sans-serif"><span
                                                                                                    style="font-size: 24px;">Need help? Call us on 0775 987555</span></font>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <!--[if gte mso 9]>
                                                                            </td>
                                                                            <![endif]--><!--[if gte mso 9]>
                                                                            </tr>
                                                                            </table>
                                                                            <![endif]--></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       class="mcnBoxedTextBlock"
                                                                       style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                       width="100%"><!--[if gte mso 9]>
                                                                    <table align="center" border="0" cellspacing="0"
                                                                           cellpadding="0" width="100%">
                                                                    <![endif]-->
                                                                    <tbody class="mcnBoxedTextBlockOuter">
                                                                    <tr>
                                                                        <td class="mcnBoxedTextBlockInner"
                                                                            style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                            valign="top"><!--[if gte mso 9]>
                                                                            <td align="center" valign="top" ">
                                                                            <![endif]-->
                                                                            <table align="left" border="0"
                                                                                   cellpadding="0" cellspacing="0"
                                                                                   class="mcnBoxedTextContentContainer"
                                                                                   style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td style="padding-top: 9px;padding-left: 18px;padding-bottom: 9px;padding-right: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                        <table border="0"
                                                                                               cellpadding="18"
                                                                                               cellspacing="0"
                                                                                               class="mcnTextContentContainer"
                                                                                               style="min-width: 100% !important;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                               width="100%">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td class="mcnTextContent"
                                                                                                    style="font-family: Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif;font-size: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;line-height: 150%;text-align: center;"
                                                                                                    valign="top"><span
                                                                                                            style="font-size:14px"><a
                                                                                                                href="[unsubscribe]"
                                                                                                                style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #606060;font-weight: normal;text-decoration: underline;"
                                                                                                                target="_blank"><span
                                                                                                                    style="color:#A9A9A9"><span
                                                                                                                        style="font-family:helveticaneue-light,helvetica,sans-serif">Unsubscribe from Blissful emails</span></span> </a> </span>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <!--[if gte mso 9]>
                                                                            </td>
                                                                            <![endif]--><!--[if gte mso 9]>
                                                                            </tr>
                                                                            </table>
                                                                            <![endif]--></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- // END FOOTER --></td>
                    </tr>
                    </tbody>
                </table>
                <!-- // END TEMPLATE --></td>
        </tr>
        </tbody>
    </table>
</center>
</body>
</html>
