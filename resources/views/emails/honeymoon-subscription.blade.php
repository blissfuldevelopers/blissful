<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <!-- NAME: 1 COLUMN -->
    <!--[if gte mso 15]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Package Enquiry</title>

</head>
<body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;">
<center>
    <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable"
           style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #FAFAFA;">
        <tr>
            <td align="center" valign="top" id="bodyCell"
                style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 10px;width: 100%;border-top: 0;">
                <!-- BEGIN TEMPLATE // -->
                <!--[if gte mso 9]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                    <tr>
                        <td align="center" valign="top" width="600" style="width:600px;">
                <![endif]-->
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer"
                       style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border: 0;max-width: 600px !important;">
                    <tr>
                        <td valign="top" id="templatePreheader"
                            style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 9px;"></td>
                    </tr>
                    <tr>
                        <td valign="top" id="templateHeader"
                            style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 0;">
                            <table class="mcnImageBlock"
                                   style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                   border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody class="mcnImageBlockOuter">
                                <tr>
                                    <td style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                        class="mcnImageBlockInner" valign="top">
                                        <table class="mcnImageContentContainer"
                                               style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                               align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td class="mcnImageContent"
                                                    style="padding-right: 9px;padding-left: 9px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                    valign="top">


                                                    <img alt=""
                                                         src="http://www.blissful.co.ke/img/blissful-logo.png"
                                                         style="max-width: 152px;padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;"
                                                         class="mcnImage" align="middle" width="152">


                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" id="templateBody"
                            style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 2px solid #EAEAEA;padding-top: 0;padding-bottom: 9px;">
                            <table class="mcnTextBlock"
                                   style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                   border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody class="mcnTextBlockOuter">
                                <tr>
                                    <td class="mcnTextBlockInner" valign="top"
                                        style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">

                                        <table style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                               class="mcnTextContentContainer" align="left" border="0" cellpadding="0"
                                               cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>

                                                <td class="mcnTextContent"
                                                    style="padding-top: 9px;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align: left;"
                                                    valign="top">

                                                    <span style="font-family:verdana,geneva,sans-serif"><span
                                                                style="font-size:24px">You have received a new {{$package_category}} enquiry</span></span><br>
                                                    <br>
                                                    <span style="font-size:18px">Here are the details:</span>

                                                    <ul>
                                                        <li style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <strong>{{ $package_name }}</strong></li>
                                                        <li style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <strong>{{ $user_name }}</strong></li>
                                                        <li style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <strong>{{ $email }}</strong></li>
                                                        <li style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <strong>{{ $number }}</strong></li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" id="templateFooter"
                            style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 9px;">
                            <table class="mcnFollowBlock"
                                   style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                   border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody class="mcnFollowBlockOuter">
                                <tr>
                                    <td style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                        class="mcnFollowBlockInner" align="center" valign="top">
                                        <table class="mcnFollowContentContainer"
                                               style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                               border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td style="padding-left: 9px;padding-right: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                    align="center">
                                                    <table style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                           class="mcnFollowContent" border="0" cellpadding="0"
                                                           cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td style="padding-top: 9px;padding-right: 9px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                align="center" valign="top">
                                                                <table align="center" border="0" cellpadding="0"
                                                                       cellspacing="0"
                                                                       style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td align="center" valign="top"
                                                                            style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                            <!--[if mso]>
                                                                            <table align="center" border="0"
                                                                                   cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                            <![endif]-->

                                                                            <!--[if mso]>
                                                                            <td align="center" valign="top">
                                                                            <![endif]-->


                                                                            <table style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   align="left" border="0"
                                                                                   cellpadding="0" cellspacing="0">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td style="padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                        class="mcnFollowContentItemContainer"
                                                                                        valign="top">
                                                                                        <table class="mcnFollowContentItem"
                                                                                               border="0"
                                                                                               cellpadding="0"
                                                                                               cellspacing="0"
                                                                                               width="100%"
                                                                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                                    align="left"
                                                                                                    valign="middle">
                                                                                                    <table align="left"
                                                                                                           border="0"
                                                                                                           cellpadding="0"
                                                                                                           cellspacing="0"
                                                                                                           width=""
                                                                                                           style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                        <tbody>
                                                                                                        <tr>

                                                                                                            <td class="mcnFollowIconContent"
                                                                                                                align="center"
                                                                                                                valign="middle"
                                                                                                                width="24"
                                                                                                                style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                                <a href="http://blissful.co.ke"
                                                                                                                   target="_blank"
                                                                                                                   style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img
                                                                                                                            src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png"
                                                                                                                            style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;"
                                                                                                                            class=""
                                                                                                                            height="24"
                                                                                                                            width="24"></a>
                                                                                                            </td>


                                                                                                        </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>

                                                                            <!--[if mso]>
                                                                            </td>
                                                                            <![endif]-->

                                                                            <!--[if mso]>
                                                                            <td align="center" valign="top">
                                                                            <![endif]-->


                                                                            <table style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   align="left" border="0"
                                                                                   cellpadding="0" cellspacing="0">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td style="padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                        class="mcnFollowContentItemContainer"
                                                                                        valign="top">
                                                                                        <table class="mcnFollowContentItem"
                                                                                               border="0"
                                                                                               cellpadding="0"
                                                                                               cellspacing="0"
                                                                                               width="100%"
                                                                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                                    align="left"
                                                                                                    valign="middle">
                                                                                                    <table align="left"
                                                                                                           border="0"
                                                                                                           cellpadding="0"
                                                                                                           cellspacing="0"
                                                                                                           width=""
                                                                                                           style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                        <tbody>
                                                                                                        <tr>

                                                                                                            <td class="mcnFollowIconContent"
                                                                                                                align="center"
                                                                                                                valign="middle"
                                                                                                                width="24"
                                                                                                                style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                                <a href="https://www.facebook.com/blissfulKE"
                                                                                                                   target="_blank"
                                                                                                                   style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img
                                                                                                                            src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png"
                                                                                                                            style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;"
                                                                                                                            class=""
                                                                                                                            height="24"
                                                                                                                            width="24"></a>
                                                                                                            </td>


                                                                                                        </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>

                                                                            <!--[if mso]>
                                                                            </td>
                                                                            <![endif]-->

                                                                            <!--[if mso]>
                                                                            <td align="center" valign="top">
                                                                            <![endif]-->


                                                                            <table style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   align="left" border="0"
                                                                                   cellpadding="0" cellspacing="0">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td style="padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                        class="mcnFollowContentItemContainer"
                                                                                        valign="top">
                                                                                        <table class="mcnFollowContentItem"
                                                                                               border="0"
                                                                                               cellpadding="0"
                                                                                               cellspacing="0"
                                                                                               width="100%"
                                                                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                                    align="left"
                                                                                                    valign="middle">
                                                                                                    <table align="left"
                                                                                                           border="0"
                                                                                                           cellpadding="0"
                                                                                                           cellspacing="0"
                                                                                                           width=""
                                                                                                           style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                        <tbody>
                                                                                                        <tr>

                                                                                                            <td class="mcnFollowIconContent"
                                                                                                                align="center"
                                                                                                                valign="middle"
                                                                                                                width="24"
                                                                                                                style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                                <a href="https://twitter.com/blissfulKE"
                                                                                                                   target="_blank"
                                                                                                                   style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img
                                                                                                                            src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png"
                                                                                                                            style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;"
                                                                                                                            class=""
                                                                                                                            height="24"
                                                                                                                            width="24"></a>
                                                                                                            </td>


                                                                                                        </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>

                                                                            <!--[if mso]>
                                                                            </td>
                                                                            <![endif]-->

                                                                            <!--[if mso]>
                                                                            <td align="center" valign="top">
                                                                            <![endif]-->


                                                                            <table style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                   align="left" border="0"
                                                                                   cellpadding="0" cellspacing="0">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                        class="mcnFollowContentItemContainer"
                                                                                        valign="top">
                                                                                        <table class="mcnFollowContentItem"
                                                                                               border="0"
                                                                                               cellpadding="0"
                                                                                               cellspacing="0"
                                                                                               width="100%"
                                                                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                                    align="left"
                                                                                                    valign="middle">
                                                                                                    <table align="left"
                                                                                                           border="0"
                                                                                                           cellpadding="0"
                                                                                                           cellspacing="0"
                                                                                                           width=""
                                                                                                           style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                        <tbody>
                                                                                                        <tr>

                                                                                                            <td class="mcnFollowIconContent"
                                                                                                                align="center"
                                                                                                                valign="middle"
                                                                                                                width="24"
                                                                                                                style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                                <a href="http://www.pinterest.com/blissfulKE"
                                                                                                                   target="_blank"
                                                                                                                   style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img
                                                                                                                            src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-pinterest-48.png"
                                                                                                                            style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;"
                                                                                                                            class=""
                                                                                                                            height="24"
                                                                                                                            width="24"></a>
                                                                                                            </td>


                                                                                                        </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>

                                                                            <!--[if mso]>
                                                                            </td>
                                                                            <![endif]-->

                                                                            <!--[if mso]>
                                                                            </tr>
                                                                            </table>
                                                                            <![endif]-->
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
                <!--[if gte mso 9]>
                </td>
                </tr>
                </table>
                <![endif]-->
                <!-- // END TEMPLATE -->
            </td>
        </tr>
    </table>
</center>
</body>
</html>