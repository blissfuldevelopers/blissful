@extends('page.zurb-layout')
@section('title')
Galleries
@stop
@section('content')
<div id="content">
<div class="container">
<div class="row">
	<div class="small-12 large-6 columns">
		<h1>Galleries</h1>
	</div>
</div>
<div class="row" id="container">
	<div class="small-12 large-6 columns">
		@if ($edits->count() > 0)
			<div class="grid">
				@foreach($edits as $edit)
				
				<div id="gallery" class="grid-item">
				  <a href="{{url('gallery/view/' .$edit->id)}}" id="thumb_name" class="pull-right">{{$edit->name}}</a>
				  <span id="image_count" class="pull-right">{{$edit->images()->count()}} Photos</span>	
				  <a id="gallery_delete" href="{{url('gallery/delete/' .$edit->id)}}" data-tooltip aria-haspopup="true" class="has-tip" title="Delete Gallery"><i class="fi-trash"></i></a>
				  <a id="gallery_edit" href="{{url('gallery/edit/' .$edit->id)}}" data-tooltip aria-haspopup="true" class="has-tip" title="Edit Gallery"><i class="fi-pencil"></i></a>
				  <a href="{{url('gallery/view/' .$edit->id)}}">
				  	<img id="gall_image" src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{url($edit->gallery_image)}}" alt="{{$edit->name}}">
				  </a>
			  	</div>

				@endforeach
			</div>
		@endif
	</div>
	<div class="small-12 large-6 columns">
		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		@foreach($edits as $edit)
		<form id="gallery-form" class="form-horizontal placard" method="post" action="{{('/gallery/update/'.$edit->id)}}" enctype="multipart/form-data">
	        <legend>Edit Gallery: {{$edit->name}}</legend>
			<input type="hidden" name="_token" value="{{csrf_token()}}">
					<label for="gallery_name" class="small-6 control-label">Name</label>
					<div class="small-12">
						<input type="text" name="gallery_name" id="gallery_name" placeholder="Name of Gallery?" class="floating-label
						 form-control" value="{{ $edit->name}}" style="background-color: rgb(250, 255, 189);" 
						@if(($edit->name) == 'Portfolio')
						disabled
						@endif />
					</div>
				<div class="form-group">
					<label for="gallery_description" class="small-6 control-label">Description</label>
					<div class="small-12">
						<textarea type="text" name="gallery_description" id="gallery_description" placeholder="A brief description of your gallery" class="form-control floating-label" style="background-color: rgb(250, 255, 189);"/>{{ $edit->description }}</textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="file" class="small-6 control-label">Gallery Cover Image<span class="mdi-image-photo sm"></span></label>
					<div class="small-12">
						<input type="file" name="file" id="file" value="{{$edit->gallery_image}}">
					</div>
				</div>
			
			<button type="submit" class="tiny right"><i class="fi-save"></i> Update Gallery.</button>
		@endforeach
		</form>

	</div>
</div>
</div>
</div>
@endsection
@section('scripts')
<script src="/js/masonry.pkgd.min.js"></script>
<script src="/js/imagesloaded.pkgd.min.js"></script>
<script>
$(document).ready(function() {
	var $container = $('.grid');
		// initialize Masonry after all images have loaded  
		$container.imagesLoaded(function() {
		     $container.masonry({
		     	itemSelector: '.grid-item'
		  		});
	});
});
</script>
@endsection