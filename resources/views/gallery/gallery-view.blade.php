@extends('page.home-layout')
@section('title')
    {{$gallery->name}}
@endsection
@section('styles')
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" />
@endsection

@section('header-content')
  <?php
    $header_class = 'vendorpg-intro';
  ?>
@endsection
@section('content')
<!-- main content  -->
<div class="contentpg-main">
  <div class="row">
    <div class="small-12 columns">
      <div class="wrapper">
        <div class="small-12 medium-7 small-centered columns">
          @include('includes.status')
        </div>
        <div id="gall" class="small-12 columns">
          @foreach ($gallery->images as $image)
            <div class="gallery-thumb">
              <a class="gallery_delete has-tip tip-right" href="{{url('image/delete/' .$image->id)}}" data-tooltip aria-haspopup="true" title="Delete Image">
                <i class="fi-trash"></i>
              </a>
                <img id="gall_image" src="{{url('https://s3.eu-central-1.amazonaws.com/blissful-ke/gallery/images/thumbs/' .$image->file_name)}}" alt="{{$gallery->name}}">
            </div>
          @endforeach
        </div>
        <div class="small-12 columns breaker">
          <form action="{{url('image/do-upload')}}" class="dropzone" id="addImages" enctype="multipart/form-data">
          {{ csrf_field() }}
          <input type="hidden" id="gallery_id" name="gallery_id" value="{{$gallery->id}}">
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script defer src="/js/dropzone.js" onLoad="initiate_dropzone();"></script>

<script type="text/javascript">
  function initiate_dropzone(){
    //////////////////////****************    DROPZONE *********************/////////////////////////

    Dropzone.options.addImages = {
        maxFilesize: 2,
        acceptedFiles: 'image/*',
        success: function(file, response) {
            if (file.status == 'success') {
                handleDropzoneFileUpload.handleSuccess(response);
            } else {
                handleDropzoneFileUpload.handleError(response);
            }
        }
    };
    // Dropzone.options.addVideos = {
    //     autoProcessQueue: false,
    //     maxFilesize:500,
    //     acceptedFiles:'video/*',


    //     success: function(file, response){
    //         if (file.status == 'success'){

    //         } else {
    //             handleDropzoneFileUpload.handleError(response);
    //         }
    //     }
    // };

    Dropzone.options.addPortfolio = { // The camelized version of the ID of the form element

        // The configuration we've talked about above
        autoProcessQueue: true,
        maxFilesize: 2,
        acceptedFiles: 'image/*',

        // The setting up of the dropzone
        init: function() {
            var myDropzone = this;

            // // First change the button to actually tell Dropzone to process the queue.
            // this.element.querySelector("#uploadPortfolio").addEventListener("click", function(e) {
            //   // Make sure that the form isn't actually being sent.
            //   e.preventDefault();
            //   e.stopPropagation();
            //   myDropzone.processQueue();
            // });

            // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
            // of the sending event because uploadMultiple is set to true.
            this.on("sendingmultiple", function() {
                console.log(response);
                // Gets triggered when the form is actually being sent.
                // Hide the success button or the complete form.
            });
            this.on("successmultiple", function(files, response) {
                console.log(response);
                // Gets triggered when the files have successfully been sent.
                // Redirect user or notify of success.
            });
            this.on("errormultiple", function(files, response) {
                console.log(response);
                // Gets triggered when there was an error sending the files.
                // Maybe show form again, and notify user of error
            });
        }

    }
    var handleDropzoneFileUpload = {
        handleError: function(response) {
            console.log(response);
        },
        handleSuccess: function(response) {
            console.log("it is hit");
            var bla = $('#gallery_id').val();
            $('#gall').load("/gallery/view/" + bla + " #gall > *");

        }
    };


  }
</script>
@endsection

