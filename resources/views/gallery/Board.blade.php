@extends('admin.layout')
@section('title')
Be Inspired
@stop
@section('content')
									<div id="content">
									<div class="card-body">  
                                    <form class="navbar-search" role="search" id="tag-search" action="{{url('/gallery/search')}}">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="tagSearch" placeholder="Enter your keyword">

                                        </div>
                                        <button type="submit" class="btn btn-icon-toggle ink-reaction"><i class="fa fa-search"></i></button>
                                    </form>                              
                                    <div class="my-gallery" itemscope itemtype="http://schema.org/ImageGallery">
                                    @foreach ($images as $img)
                                        @foreach ($img as $image)
                                            <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                            <a href="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{url($image->file_path)}}" itemprop="contentUrl" data-size="964x1024">
                                                <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{url($image->file_path)}}" itemprop="thumbnail" alt="Image description" />
                                            </a>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">          
                                            <input id="image_path" name="image_path" value="{{($image->file_path)}}">
                                            <figcaption itemprop="caption description">{{$image->caption}}</figcaption>
                                        </figure>
                                        @endforeach
                                    @endforeach
                                    </div>
                                   
                                    <!-- Root element of PhotoSwipe. Must have class pswp. -->
                                    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

                                        <!-- Background of PhotoSwipe.
                                             It's a separate element, as animating opacity is faster than rgba(). -->
                                        <div class="pswp__bg"></div>

                                        <!-- Slides wrapper with overflow:hidden. -->
                                        <div class="pswp__scroll-wrap">

                                            <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
                                            <!-- don't modify these 3 pswp__item elements, data is added later on. -->
                                            <div class="pswp__container">
                                                <div class="pswp__item"></div>
                                                <div class="pswp__item"></div>
                                                <div class="pswp__item"></div>
                                            </div>

                                            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                                            <div class="pswp__ui pswp__ui--hidden">

                                                <div class="pswp__top-bar">

                                                    <!--  Controls are self-explanatory. Order can be changed. -->

                                                    <div class="pswp__counter"></div>

                                                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                                                    <button class="pswp__button pswp__button--share" title="Share"></button>

                                                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                                                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                                                    <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                                                    <!-- element will get class pswp__preloader--active when preloader is running -->
                                                    <div class="pswp__preloader">
                                                        <div class="pswp__preloader__icn">
                                                            <div class="pswp__preloader__cut">
                                                                <div class="pswp__preloader__donut"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                                    <div class="pswp__share-tooltip"></div>
                                                </div>

                                                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                                                </button>

                                                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                                                </button>

                                                <div class="pswp__caption">
                                                    <div class="pswp__caption__center"></div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                  
                                </div>
                            </div>
@endsection