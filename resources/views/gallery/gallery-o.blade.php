@extends('page.home-layout')
@section('title')
    Galleries
@endsection

@section('header-content')
  <?php
    $header_class = 'vendorpg-intro';
  ?>
@endsection
@section('content')
<!-- main content  -->
<div class="contentpg-main">
    <div class="row">
      	<div class="small-12 columns">
        	<div class="wrapper">
        		<div class="small-12 medium-7 small-centered columns">
        			@include('includes.status')
        		</div>
        		<h4>Galleries</h4>
    			<div class="small-12 columns">
					@if ($galleries->count() > 0)
					<div class="small-12 medium-7 columns">
						@foreach ($galleries as $gallery)
						<div class="small-6 columns gallery-thumb">
						  	<a href="{{url('gallery/view/' .$gallery->id)}}" class="thumb_name">
						  		{{$gallery->name}}
						  	</a>
						  	<span class="image_count">{{$gallery->images()->count()}} Photos</span>	
						  	@if($gallery->name != 'Portfolio')
						  	<a class="gallery_delete has-tip tip-right" href="{{url('gallery/delete/' .$gallery->id)}}" data-tooltip aria-haspopup="true" title="Delete Gallery">
						  		<i class="fi-trash"></i>
						  	</a>
						  	@endif
						  	<a class="gallery_edit has-tip tip-right" href="{{url('gallery/edit/' .$gallery->id)}}" data-tooltip aria-haspopup="true" title="Edit Gallery">
						  		<i class="fi-pencil"></i>
						  	</a>
							 <a href="{{url('gallery/view/' .$gallery->id)}}">
							  	<img id="gall_image" src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$gallery->gallery_image}}" alt="{{$gallery->name}}">
							 </a>
					  	</div>
						@endforeach
					</div>
					@endif
					<div class="small-12 medium-5 columns">
						<form class="form-horizontal" method="post" action="{{('/gallery/save')}}" enctype="multipart/form-data">
					        <legend>Create Gallery</legend>
							<input type="hidden" name="_token" value="{{csrf_token()}}">	
							<div class="small-12 columns">
								<label for="gallery_name">Name
									<input type="text" name="gallery_name" id="gallery_name" placeholder="Name of Gallery?" class="floating-label form-control" value="{{ old('gallery_name')}}"/>
								</label>
							</div>
							<div class="small-12 columns">
								<label for="gallery_description">Description
									<textarea type="text" name="gallery_description" id="gallery_description" placeholder="A brief description of your gallery" class="form-control floating-label" value="{{ old('gallery_description')}}"/></textarea>
								</label>
							</div>
							<div class="small-12 columns">
								<label for="file" >Gallery Cover Image
									<input type="file" name="file" id="file">
								</label>
							</div>
							<div class="small-12 columns">
								<button type="submit" class="small buttons button-primary right"><i class="fi-save"></i> Create Gallery</button>
							</div>
						</form>
					</div>
				</div>
        	</div>
       	</div>
    </div>
</div>
@endsection
@section('scripts')

@endsection