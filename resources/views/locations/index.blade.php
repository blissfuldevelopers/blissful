<!DOCTYPE html>
<html>
  <head>
    <title>Add Location</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 80%;
        min-height: 500px;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #pac-input {
        background-color: #fff !important;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        margin-top: 11px;
        padding: 7px 10px;
        text-overflow: ellipsis;
        width: 300px;
        border: #fff;
        border-radius: 3px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }
      .gm-style-iw{
        text-align: center;
        max-height: 400px;
      }
      .gm-style-iw + div {
        display: none;
      }
      .info-row span{
        cursor: pointer;
        padding: 5px 6px;
        background: #1bb2a6;
        color: #fff;
        border-radius: 5px;
      }
      .info-row{
        min-height: 70px;
      }
      .info-loader{
        height: 40px;
        width: 70px;
        display: block;
        position: relative;
      }
      .info-loader >div{
        margin: auto;
        width: 30px;
        height: 30px;
        border-radius: 50%;
        border: 2px solid #f9a9a4;
        border-top: 2px solid #EC736C;
        background: transparent;
        -webkit-animation: loading 2s infinite linear;
      }
      @media only screen and (max-width: 768px) {
          /* For mobile phones: */
          .info-row{
            min-height: 90px;
          }
          #pac-input {
            width: 200px;
          }
      }
    </style>
  </head>
  <body>
    <h1>Please set your business location</h1>
    <input type="text" id="pac-input">
    <div id="map"></div>
    <script>



      // Note: This example requires that you consent to location sharing when
      // prompted by your browser. If you see the error "The Geolocation service
      // failed.", it means you probably did not give permission for the browser to
      // locate you.
      
      @if(isset($user) && isset($user->location->lat))
      var pos = {lat: {{$user->location->lat}}, lng: {{$user->location->lng}}};
      @else
      var pos = {lat: -1.2921, lng: 36.8219};
      @endif
      var marker = [];
      @if(isset($user))
      var user_id = {{$user->id}};
      @else
      var user_id = {{Auth::user()->id}}
      @endif
      //infotip content to be displayed with new marker
      var infoContent = '<div class="info-row"> <p>Drag marker, click on map, or use the search box to set location</p> <span onClick="save_marker()">Set location</span></div>';
      var loadingContent = '<div class="info-loader"><div></div></div><p>Saving</p>'
      function initMap() {

        var map = new google.maps.Map(document.getElementById('map'), {
          center: pos,
          zoom: 17, //zoom level, 0 = earth view to higher value
          clickableIcons: false
        });
        
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old marker.
          marker.setMap(null);

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
            pos = place.geometry.location;
          });
          map.fitBounds(bounds);
          handleLocationSetting(true,map, pos);
        });

        @if(isset($user) && isset($user->location->lat))
          handleLocationSetting(true,map, pos);
          return;
        @endif

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            map.setCenter(pos);
          });
        }

        google.maps.event.addListener(map, 'click', function(event) {
            marker.setMap(null);
            handleLocationSetting(true,map, event.latLng);
        });

        handleLocationSetting(true,map, pos);
      }

      function handleLocationSetting(browserHasGeolocation,map, pos) {
        //Drop a new Marker with our Edit Form
        create_marker(map,pos, 'New Marker', infoContent);
      }

      function create_marker(map,pos,title,infoContent){
        marker = new google.maps.Marker({
          position: pos,
          map: map,
          draggable:true,
          animation: google.maps.Animation.DROP,
          title:"Save Location",
        });
        //Create an infoWindow
        var infowindow = new google.maps.InfoWindow({
            draggable:true
        });
        //set the content of infoWindow
        infowindow.setContent(infoContent);
        infowindow.open(map,marker);
      }

      //############### Save Marker Function ##############
      function save_marker(){
        //Save new marker using jQuery Ajax

        //get marker positions
        var lat = marker.getPosition().lat();
        var lng = marker.getPosition().lng();
        var data = ""; 
        var address = "";
        var city_name = "";
        var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
        //fetch address
        var geocoder = new google.maps.Geocoder;

        geocoder.geocode({'latLng': latlng}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            // console.log(results)
            if (results[1]) {
             //formatted address
            address = results[0].formatted_address;

            //find country name
                 for (var i=0; i<results[0].address_components.length; i++) {
                for (var b=0;b<results[0].address_components[i].types.length;b++) {

                //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                    if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                        //this is the object you are looking for
                        city= results[0].address_components[i];
                        break;
                    }
                }
            }
            //city data
            city_name = city.long_name;
            data = {lat:lat,lng:lng,address:address,name:city_name,user_id:user_id};
            save_details(data,marker);
            } else {
              console.log("No results found")
            }
          } else {
            console.log("Geocoder failed due to: " + status)
          }
        });
        
      }

      function save_details(data,marker){
        $('.info-row').html(loadingContent);
        // console.log(data)
        $.ajax({
          type: "POST",
          url: "{{route('user-location.store')}}",
          data: data,
          success:function(data){
            console.log(data)
            marker.setDraggable(false); //set marker to fixed
            $('.info-row').css("min-height", "0").html('<p class="green">Location added successfully</p>');
            // place this within dom ready function
            function hidepanel() {     
              $('.locations-container').addClass('locations-container--hidden');
            }
            // use setTimeout() to execute
            setTimeout(hidepanel, 4000);
            
          },
          error:function (xhr, ajaxOptions, thrownError){
              alert(thrownError); //throw any errors
          }
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&libraries=places&callback=initMap">
    </script>
    <script data-cfasync="false" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  </body>
</html>