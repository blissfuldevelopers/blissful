@extends('layouts.kode')
@section('title')
    Dashboard
@endsection
@section('styles')
    <link rel="stylesheet" href="/css/dashboard/root.css"/>
    <link rel="stylesheet" href="/css/dashboard/dashes.css"/>
    @endsection
    @section('content')

    <?php

    $count = 0;
    $responses = 0;
    $arrayResponses= [];
    if (!empty( $threads )) {

        foreach ($threads as $thread) {
            $array [] = $thread->isUnread($currentUserId);
            $count = count(array_filter($array));
        }
    }

    $cssClass = $count == 0 ? 'hidden' : '';
    $number = $count == 0 ? 'no' : '';
    $Unreadmessage = $count == 1 ? 'message' : 'messages';
    ?>

    <?php
    foreach ($responseThreads as $responseThread) {
        $arrayResponses[] = $responseThread->isUnread($currentUserId);
    }
    if ($threads && $threads->count() > 0) {
        $responses = count(array_filter($arrayResponses));

    }


    $showHide = $responses == 0 ? 'hidden' : '';
    $responseNumber = $responses == 0 ? 'no' : '';
    $responseMessage = $responses == 1 ? 'response' : 'responses';
    ?>
            <!-- Start Page Loading -->
    <div class="loading"><img src="img/dashboard/loading.gif" alt="loading-img"></div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START TOP -->
    <div id="top" class="clearfix white_background materialize">

        <!-- Start App Logo -->
        <div class="applogo white_background">
            <a href="{{ route('home')  }}" class="logo"><img src="/img/blissful-logo.svg"></a>
        </div>
        <!-- End App Logo -->

        <!-- Start Sidebar Show Hide Button -->
        <a href="#" class="sidebar-open-button"><i class="fa fa-bars"></i></a>
        <a href="#" class="sidebar-open-button-mobile"><i class="fa fa-bars"></i></a>
        <!-- End Sidebar Show Hide Button -->

        <!-- Start Searchbox -->
        <form class="searchform" action="search" method="GET" enctype="multipart/form-data">
            <input type="text" name="find" class="searchbox" id="searchbox" placeholder="Find a Vendor">
            <span class="searchbutton"><i class="fa fa-search"></i></span>
        </form>
        <!-- End Searchbox -->

        <!-- Start Top Right -->
        <ul class="top-right">

            <!-- <li class="dropdown link">
              <a href="#" data-toggle="dropdown" class="dropdown-toggle hdbutton">Create New <span class="caret"></span></a>
                <ul class="dropdown-menu dropdown-menu-list"> -->
            <!-- <li><a href="#"><i class="fa falist fa-paper-plane-o"></i>Product or Item</a></li> -->
            <!-- <li><a href="#"><i class="fa falist fa-file-image-o"></i>Image Gallery</a></li>
          </ul>
      </li> -->
            <!--
                <li class="link">
                  <a href="#" class="notifications">6</a>
                </li> -->

            <li class="dropdown link">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle profilebox">
                    <img src="/{{Auth::user()->profile->profilePic}}" alt="img">
                    <b>{{Auth::user()->first_name}}</b>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-list dropdown-menu-right">
                    <li role="presentation" class="dropdown-header"><a href="
                @if( Auth::user()->hasRole('administrator'))
                                /admin
                                @elseif( Auth::user()->hasRole('vendor'))
                                /vndor
                                @elseif( Auth::user()->hasRole('user'))
                                /admin
                                @endif"><i class="fa falist fa-home"></i>Dashboard</a>
                    </li>
                    <li><a href="{{ url('profile/edit/'.Auth::user()->profile->user_id) }}"><i
                                    class="fa falist fa-wrench"></i>Profile</a></li>
                    <li><a href="#inbox" data-toggle="tab"><i class="fa falist fa-inbox"></i>Inbox<span
                                    class="badge label-danger {{$cssClass}}">{{$count}}</span></a></li>
                    @if( Auth::user()->hasRole('vendor'))
                        <li><a href="/vndor#jobs-board"><i class="fa falist fa-briefcase"></i>Jobs</a>
                    @elseif( Auth::user()->hasRole('user'))
                        <li><a href="/user#jobs-board"><i class="fa falist fa-briefcase"></i>Jobs</a>
                    @endif
                    <li class="divider"></li>
                    <li><a href="{{ route('authenticated.logout') }}"><i class="fa falist fa-power-off"></i> Logout</a>
                    </li>
                </ul>
            </li>

        </ul>
        <!-- End Top Right -->

    </div>
    <!-- END TOP -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->


    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START SIDEBAR -->
    <div class="sidebar clearfix">

        <ul class="sidebar-panel nav nav-tabs">
            <li class="sidetitle">MAIN</li>
            <li class="active">
                <a href="#main" data-toggle="tab">
        <span class="icon color5">
          <i class="fa fa-home"></i>
        </span>
                    Dashboard
                    <span class="label label-default"></span>
                </a>
            </li>
            <li>
                <a href="#inbox" data-toggle="tab">
        <span class="icon color6">
          <i class="fa fa-envelope-o"></i>
        </span>
                    Messages
                    <span class="label label-default {{$cssClass}}">{{$count}}</span>
                </a>
            </li>
            <li>
                <a href="#jobs-board" id='jobs-board-btn' data-toggle="tab">
                    <span class="icon color6">
                      <i class="fa fa-briefcase"></i>
                    </span>
                    Quotation Request
                </a>
            </li>
            <li>
                <a href="#responses" data-toggle="tab">
        <span class="icon color6">
          <i class="fa  fa-quote-left "></i>
        </span>
                    Quotation Responses
                    <span class="label label-default {{$showHide}}">{{$responses}}</span>
                </a>
            </li>
        </ul>

    </div>
    <!-- END SIDEBAR -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START CONTENT -->
    <div class="content tab-content">

        <!-- START OF MAIN TAB CONTENT -->
        <div class="tab-pane active" id="main">
            <!-- Start Page Header -->
            <div class="page-header">
                <h1 class="title">Dashboard</h1>
                <ol class="breadcrumb">
                    <li class="active">Quick look at how you are doing :)</li>
                </ol>
            </div>
            <!-- End Page Header -->

            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTAINER -->
            <div class="container-widget">

                <!-- Start First Row -->
                <div class="row">
                    <!-- Start Files -->
                    <div class="col-md-12 col-lg-3 contrast">
                        <div class="widget profile-widget" style="height:380px;">
                            <img src="/{{Auth::user()->profile->profilePic}}" class="profile-image" alt="img">
                            <h1>{{Auth::user()->first_name}}</h1>
                            <p><i class="fa fa-map-marker"></i> {{Auth::user()->profile->location}}</p>
                            <a href="{{ url('profile/edit/'.Auth::user()->profile->user_id) }}" class="btn btn-sm">Edit
                                Profile</a>
                        </div>
                    </div>
                    <!-- End Files -->
                    <!--  Start Job responses -->
                    <div class="col-md-12 col-lg-4">
                        <div class="panel panel-widget materialize">
                            <div class="panel-title">
                                Job Responses <span class="label label-danger {{$showHide}}">{{$responses}}</span>
                                <ul class="panel-tools">
                                    <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
                                    <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <ul class="mailbox-inbox">
                                    @if($responseThreads && $responseThreads->count() > 0)
                                        @foreach($responseThreads as $thread)
                                            @if($thread && $thread->isUnread($currentUserId))
                                                <li>
                                                    <a href="#" class="item clearfix">
                                                        <img src="{!! $thread->latestMessage->user->profile->profilePic!!}"alt="img" class="img">
                                                        <span class="from">{!! $thread->participantsString(Auth::id(), ['first_name']) !!}</span>
                                                        @if($thread->latestMessage->body)
                                                            {!! str_limit($thread->latestMessage->body, 75) !!}
                                                        @endif
                                                        <span class="date">{{$thread->created_at->diffforhumans()}}</span>
                                                    </a>
                                                </li>
                                            @endif
                                        @endforeach
                                        @if($responses == 0)
                                            <ul>
                                                <li><p> Sorry, no new response to display</p></li>
                                            </ul>
                                        @endif
                                    @else
                                        <ul>
                                            <li>
                                                <p> Sorry, no replies to your Quotation request, check again soon</p>
                                            </li>
                                        </ul>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-5">
                        <div class="panel panel-widget materialize">
                            <div class="panel-title">
                                Inbox <span class="label label-danger {{$cssClass}}">{{$count}}</span>
                                <ul class="panel-tools">
                                    <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
                                    <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <ul class="mailbox-inbox">
                                    @if($threads && $threads->count() > 0)
                                        @foreach($threads as $thread)
                                            @if($thread && $thread->isUnread($currentUserId))
                                                <li>
                                                    <a href="#" class="item clearfix">
                                                        <img src="{!! $thread->latestMessage->user->profile->profilePic!!}"alt="img" class="img">
                                                        <span class="from">{!! $thread->participantsString(Auth::id(), ['first_name']) !!}</span>
                                                        @if($thread->latestMessage->body)
                                                            {!! str_limit($thread->latestMessage->body, 75) !!}
                                                        @endif
                                                        <span class="date">{{$thread->created_at->diffforhumans()}}</span>
                                                    </a>
                                                </li>
                                            @endif
                                        @endforeach
                                        @if($count == 0)
                                            <ul>
                                                <li><p> Sorry, no new message to display</p></li>
                                            </ul>
                                        @endif
                                    @else
                                        <ul>
                                            <li><p> Sorry, no messages to display</p></li>
                                        </ul>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End First Row -->

                <!-- Start Second Row -->
                <div class="row">

                    <!-- Start Blog Post -->
                    @if($post && $post->count() >0)
                        <div class="col-md-6 col-lg-8">
                            <div class="panel no_pad panel-widget blog-post">
                                <div class="panel-body">
                                    <div class="image-div color10-bg">
                                        <img src="{{ page_image($post->page_image) }}" class="image" alt="img">
                                        <h1 class="title"><a href="{{$post->url()}}">{{ $post->title }}</a></h1>
                                    </div>
                                    <p class="text">{!! str_limit($post->content, 160) !!}<a href="{{$post->url()}}">Read
                                            More</a></p>
                                    <!-- <p class="author">
                                      <img src="img/dashboard/profileimg.png" alt="img">
                                      <span>Jonathan Doe</span>
                                      Designer
                                    </p> -->
                                </div>
                            </div>
                        </div>
                        @endif
                                <!-- End Blog Post -->

                        <!-- Start of Ad Section -->
                        <div class="col-md-6 col-lg-3">

                            <div class="panel panel-default  padding-0">
                                <img src="img/dashboard/2.jpg">
                            </div>

                        </div>
                        <!-- End of Ad Section -->

                </div>
                <!-- End Second Row -->

            </div>
            <!-- END CONTAINER -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
        </div>
        <!-- END OF MAIN TAB CONTENT -->

        <!-- START OF INBOX TAB CONTENT -->
        <div class="tab-pane" id="inbox">
            @include('panels.admin.partials.message-tab')
        </div>
        <!-- END OF INBOX TAB CONTENT -->

        <div class="tab-pane" id="jobs-board">
            @include('jobs.partials.jobs-container')
        </div>

        <!-- START OF RESPONSES TAB CONTENT -->
        <div class="tab-pane" id="responses">
            @include('panels.user.partials.response-tab')
        </div>
        <!-- END OF RESPONSES TAB CONTENT -->


    </div>
    <!-- End Content -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- start popup -->
    <div id="spopup">
        <!--close button-->
        <a style="position:absolute;top:5px;right:10px;color:#555;font-size:14px;font-weight:bold;" href="javascript:void(0);" onclick="return closeSPopup();">
            <span class="fa fa-close"></span>
        </a>
       <div class="spopup-text">
          <span>Need a quick quote?</span> &nbsp; <a href="/jobs/create" class="btn btn-danger">Get Started</a>
       </div> 
    </div>
    <!-- end popup -->
@endsection
@section('scripts')
    <script type="text/javascript">
        $(window).load(function() {
            $("#spopup").show(700);
        });
        function closeSPopup(){
            $('#spopup').hide(700);
        }
    </script>
    @include('panels.admin.partials.dash-scripts')
@stop