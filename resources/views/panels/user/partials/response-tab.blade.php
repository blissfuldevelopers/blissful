@if ($errors && $errors->any())
    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
    @endif
            <!-- Start Page Header -->
    <div class="page-header" id="jobs">
        <h1 class="title">Responses</h1>
        <ol class="breadcrumb">
            <li class="active">You have <span
                        class="label label-danger {{ @$showHide}}">{!! @$responses !!}</span>{!! @$responseNumber !!}
                unread {!! @$responseMessage !!}</li>
        </ol>
        <!-- Start Page Header Right Div -->

        <!-- End Page Header Right Div -->
    </div>
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START CONTAINER -->
    <div class="container-mail">
        <!-- Start Mailbox -->
        <div class="mailbox clearfix">
            <!-- Start Mailbox Container -->
            <div class="container-mailbox">
                <!-- Start Mailbox Inbox -->
                <div class="col-lg-3 col-md-4">
                    <ul class="mailbox-inbox nav nav-tabs">
                        @if($responseThreads)
                            @foreach($responseThreads as $responseThread)
                                @if($responseThread)
                                    <?php $class = $responseThread->isUnread($currentUserId) ? 'alert-info' : ''; ?>
                                    <li>
                                        <a href="#job_{{$responseThread->id}}" data-toggle="tab"
                                           class="item clearfix {{$class}}">
                                            <img src=@foreach($responseThread->participants as $participant)
                                            @if($participant->user_id != $currentUserId)
                                                    "/{{$participant->user->profile->profilePic}}"
                                                 @endif
                                                 @endforeach
                                                 alt="img" class="img">
                                            <span class="from">{!! $responseThread->participantsString(Auth::id(), ['first_name']) !!}</span>
                                            {!! str_limit($responseThread->latestMessage->body, 75) !!}
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        @else
                            <li>Sorry, no responses yet :(</li>
                        @endif
                    </ul>
                </div>
                <!-- End Mailbox Inbox -->

                <!-- Start Chat -->
                <div class="tab-content">
                    @if(Auth::user()->job && Auth::user()->job->count() > 0)
                        @foreach(Auth::user()->job as $job)
                            @foreach($job->bids as $bid)
                                @if($bid && $bid->thread)
                                    <div class="chat tab-pane fade col-lg-9 col-md-8" id="job_{{$bid->thread->id}}">
                                        <!-- Start Title -->
                                        <div class="title">
                                            <h1><strong>Job Title: </strong>{{$bid->job->title}}
                                                <small>{{$bid->user->email}}</small>
                                            </h1>
                                            <p><b>From:</b>{{$bid->user->first_name}}</p>
                                            <div class="btn-group" role="group" aria-label="...">
                                                {!! Form::open(['method' => 'PATCH','route' => ['bids.update', $bid->id]]) !!}
                                                {!! Form::hidden('bid_accepted', 1) !!}
                                                <button type="submit" class="btn btn-icon btn-sm btn-light"><i
                                                            class="fa fa-star-o"></i> Accept proposal
                                                </button>
                                                {!! Form::close() !!}
                                            </div>

                                        </div>
                                        <!-- End Title -->

                                        <!-- Start Conv -->
                                        <ul class="conv" id="job-conv{{$bid->thread->id}}">
                                            @foreach($bid->thread->messages()->get() as $message)
                                                @if($message)
                                                    <li>
                                                        <img src="/{{$message->user->profile->profilePic}}" alt="img"
                                                             class="img">
                                                        <p class="ballon {{  (($message->user->id) == (Auth::user()->id)) ? 'color2' : 'color1'}} ">{!! $message->body !!}
                                                            <br>
                                                            <i class="sm">{!! $message->created_at->diffForHumans() !!}</i>
                                                        </p>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                        <!-- End Conv -->
                                        <div class="write">

                                            {!! Form::open(['route' => ['messages.update', $bid->thread->id], 'method' => 'PUT', 'class'=>'margin-b-20', 'id'=>'form']) !!}
                                                    <!-- Message Form Input -->
                                            <div class="form-group">
                                                <div class="ajax_loader"
                                                     style="position: absolute; left: 45%; bottom: 160px; display:none">
                                                    <img src="/img/rolling.svg">
                                                </div>
                                                {!! Form::textarea('message', null, [
                                                'class' => 'form-control ',
                                                'id' => 'job-message'.$bid->thread->id,
                                                'required',
                                                'data-parsley-required-message' => 'Please write a message :)',
                                                'data-parsley-minlength'        => '2',
                                                'data-parsley-trigger'          => 'change focusout'
                                                ]) !!}
                                            </div>
                                            <input type="hidden" name="recipients" id="recipients"
                                                   value="{{@$message->user->profile->id}}">
                                            <!-- Submit Form Input -->
                                            <div class="form-group">
                                                <button id="send-job-btn{{@$bid->thread->id}}" class="btn btn-default">
                                                    Send
                                                </button>
                                                <button type="reset" class="btn margin-l-5">Clear</button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        @endforeach
                    @endif
                </div>
                <!-- End Chat -->
            </div>
            <!-- End Mailbox Container -->
        </div>
    </div>
    <!-- End Mailbox -->