@extends('page.zurb-layout')
@section('title')
Dashboard
@stop
@section('scripts')
<link rel="stylesheet" href="/css/default-skin.css"/>
<link rel="stylesheet" href="/css/photoswipe.css"/>
@endsection
@section('content')
<main role="main" id="dashboad" class="individuals">

    <!-- dashboard body -->
    <section class="dbody">
      <div class="row">
        <div class="small-12 columns">
          <ul class="tabs " data-tab>
            <li class="tab-title active"><a href="#panel1">Overview</a></li>
            <li class="tab-title"><a href="#panel2">Messages</a></li>
            <li class="tab-title"><a href="#panel3">Photos</a></li>
            <li class="tab-title"><a href="#panel4">Profile</a></li>
          </ul>
          <div class="tabs-content">
            <div class="content active" id="panel1">
              <div class="row">
                <div class="large-9 medium-9 small-12 columns">
                  <div class="row">
                    <!-- vendor list -->
                    <section class="v-list">
                      <h3>My Suppliers</h3>
                      <span class="link">&#10095;</span>
                      <ul>
                        <li>Magnolia Greens</li>
                        <li>KamFoods</li>
                        <li>Radical Tents</li>
                        <li>Windsor Club</li>
                        <li>Ben Kiruthi</li>
                        <li>Flock Limos</li>
                        <li>Daisy Decor</li>
                      </ul>
                    </section>
                    <!-- message inbox -->
                    <section class="inbox">
                      <h3>Inbox</h3>
                      <span class="link">&#10095;</span>
                      <ul>
                        @if($threads->count() > 0)
                            @foreach($threads as $thread)
                            @if($thread->isUnread($currentUserId))
                        <li>
                          <ul>
                            <!-- message avatar -->
                            <li class="avatar">
                              <img src="/img/anony.png">
                            </li>
                            <!-- message title -->
                            <li class="title">
                              <span>{!! link_to('messages/' . $thread->id, $thread->subject) !!}</span>
                            </li>
                            <!-- message date -->
                            <li class="date">
                              <span>{{$thread->created_at}}</span>
                            </li>
                          </ul>
                        </li>
                            @else
                            @endif
                            @endforeach
                            <p>Sorry, no new messages.</p>    
                        @endif
                        
                      </ul>
                    </section>
                    <!-- my idea book section -->
                    <section class="gallery">
                      <h3>My Idea Book</h3>
                      <span class="link">&#10095;</span>
                      <ul>
                        <li><img src="/img/gallery/1.jpg"></li>
                        <li><img src="/img/gallery/2.jpg"></li>
                        <li><img src="/img/gallery/3.jpg"></li>
                        <li><img src="/img/gallery/4.jpg"></li>
                        <li><img src="/img/gallery/5.jpg"></li>
                        <li><img src="/img/gallery/6.jpg"></li>
                        <li><img src="/img/gallery/7.jpg"></li>
                        <li><img src="/img/gallery/8.jpg"></li>
                      </ul>
                    </section>
                    <!-- featured article section -->
                    <section class="ftarticle">
                      <img src="/img/dashboard/1.jpg">
                      <div class="dets">
                        <h3>Sheria House: All you need to know</h3>
                        <span class="link">&#10095;</span>
                      </div>
                    </section>
                  </div>
                </div>
                <div class="large-3 medium-3 small-12 columns">
                  <div class="row">
                    <!-- dashboard ad -->
                    <section class="ad">
                      <img src="/img/dashboard/2.jpg">
                    </section>
                    <!-- my updates  -->
                    <section class="updates">
                      <h3>Updates</h3>
                      <span class="link">&#10095;</span>
                    </section>
                  </div>
                </div>
              </div>
            </div>
            <div class="content" id="panel2">
                  <section class="inbox">
                      @if (Session::has('error_message'))
                      <div class="alert alert-danger" role="alert">
                          {!! Session::get('error_message') !!}
                      </div>
                        @endif
                      
                      <ul>
                        @if($threads->count() > 0)
                            @foreach($threads as $thread)
                        <li>
                          <ul>

                            <?php $class = $thread->isUnread($currentUserId) ? 'alert-info' : ''; ?>
                            <!-- message avatar -->
                            <div id="thread_list_{{$thread->id}}" class="media alert {!!$class!!}">
                                <h4 class="media-heading">{!! link_to('messages/' . $thread->id, $thread->subject) !!}</h4>
                                <p id="thread_list_{{$thread->id}}_text">{!! $thread->latestMessage->body !!}</p>
                                <p><small><strong>Participants:</strong> {!! $thread->participantsString(Auth::id(), ['first_name']) !!}</small></p>
                            </div>
                          </ul>
                        </li>
                        @endforeach
                        @else
                            <p>Sorry, no new messages.</p>
                        @endif
                        
                      </ul>
                  </section>
            </div>
            <div class="content" id="panel3">
              <p>This is the third panel of the basic tab example. This is the third panel of the basic tab example.</p>
            </div>
            <div class="content" id="panel4">
              <div class="ripple placard profile">
                  <div class="image ">
                    <img src="{{Auth::user()->profile->profilePic}}">
                    <p>{{Auth::user()->profile->first_name}}</p>
                  </div>
              <div class="content">
                <a href="{{ url('profile/edit/'.Auth::user()->profile->user_id) }}">Edit Profile <a href="#" class="link hide-for-small-only">&#10095;</a></a> 
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


  </main>
  <!-- END OF MAIN SECTION -->
@stop