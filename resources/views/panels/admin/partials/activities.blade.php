@if(isset($activity) && count($activity))
    <div class="col-md-12 col-lg-3">
        <div class="panel panel-widget">
            <div class="panel-title">
                Page views
                <ul class="panel-tools panel-tools-hover">
                    <li><a class="icon"><i class="fa fa-refresh"></i></a></li>
                    <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
                    <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
                    <li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">

                @foreach($activity as $event)
                    <ul class="basic-list">
                        @if($event->name =='read')
                            {{$event->user ? $event->user->first_name : 'A Guest'}} viewed the <a
                                    href="#">{{$event->subject->name}}</a>
                            <strong>page</strong> {{$event->created_at->diffForHumans()}}

                        @endif
                    </ul>
                @endforeach


            </div>
        </div>
    </div>
    <!-- End of New  List -->
    <!-- Start of Popular Vendor List -->
    <div class="col-md-12 col-lg-3">
        <div class="panel panel-widget">
            <div class="panel-title">
                Vendor Views
                <ul class="panel-tools panel-tools-hover">
                    <li><a class="icon"><i class="fa fa-refresh"></i></a></li>
                    <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
                    <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
                    <li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">
                @foreach($activity as $event)
                    <ul class="basic-list">
                        @if($event->name =='viewed_vendor')
                            {{$event->user ? $event->user->first_name : 'A Guest'}} viewed the <a
                                    href="#">{{$event->subject ? $event->subject->user->first_name : 'a Deleted '}}</a>
                            <strong>page</strong> {{$event->created_at->diffForHumans()}}
                        @endif
                    </ul>
                @endforeach
            </div>
        </div>
    </div>
    <!-- End of Popular Vendor List -->
    <!-- Start of Popular Vendor List -->
    <div class="col-md-12 col-lg-3">
        <div class="panel panel-widget">
            <div class="panel-title">
                Logins
                <ul class="panel-tools panel-tools-hover">
                    <li><a class="icon"><i class="fa fa-refresh"></i></a></li>
                    <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
                    <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
                    <li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">
                @foreach($activity as $event)
                    <ul class="basic-list">
                        @if($event->name =='read')
                            <a href="/admin/users/{{$event->user ? $event->user->id : ''}}">{{$event->user ? $event->user->first_name : 'A deleted user/vendor had'}} {{$event->user ? $event->user->last_name :''}}</a>
                            <strong>Logged-in</strong> {{$event->created_at->diffForHumans()}}

                        @endif
                    </ul>
                @endforeach
            </div>
        </div>
    </div>
    <!-- End of Popular Vendor List -->
    <!-- Start of Popular Vendor List -->
    <div class="col-md-12 col-lg-3">
        <div class="panel panel-widget">
            <div class="panel-title">
                Vendor Categories
                <ul class="panel-tools panel-tools-hover">
                    <li><a class="icon"><i class="fa fa-refresh"></i></a></li>
                    <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
                    <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
                    <li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">
                @foreach($activity as $event)
                    <ul class="basic-list">
                        @if($event->name =='viewed_categories')
                            {{$event->user ? $event->user->first_name : 'A Guest'}} viewed the <a
                                    href="#">{{$event->subject->name}}</a>
                            <strong>category</strong> {{$event->created_at->diffForHumans()}}

                        @endif
                    </ul>
                @endforeach
            </div>
        </div>
    </div>
    <!-- End of Popular Vendor List -->
    <!-- Start of Popular Vendor List -->
    <div class="col-md-12 col-lg-3">
        <div class="panel panel-widget">
            <div class="panel-title">
                Vendor Registered
                <ul class="panel-tools panel-tools-hover">
                    <li><a class="icon"><i class="fa fa-refresh"></i></a></li>
                    <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
                    <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
                    <li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">
                @foreach($activity as $event)
                    <ul class="basic-list">
                        @if($event->name =='vendor_registered')
                            <a href="/reviews/{{$event->subject_id}}">{{$event->user ? $event->user->first_name : 'Deleted'}}</a>
                            registered as a <strong>Vendor</strong> for the following
                            services
                            @if(isset($event->user->classifications)
                            && $event->user->classifications)
                                @foreach($event->user->classifications as $classification )
                                    <strong>{{$classification->name}}</strong>,
                                @endforeach
                            @endif
                            {{$event->created_at->diffForHumans()}}

                        @endif
                    </ul>
                @endforeach
            </div>
        </div>
    </div>
@endif