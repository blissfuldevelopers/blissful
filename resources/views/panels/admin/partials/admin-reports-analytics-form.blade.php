{!! Form::open(['url' => route('activities-report'), 'method'=>'get', 'class' =>'analytics-report-form']) !!}
<div class="row">
    <div class="small-6 medium-3 columns">
       <label> From
        <?php $from = (isset($input['from']) && $input['from'] != '') ? date('Y-m-d', strtotime($input['from'])) : date('Y-m-d', strtotime('yesterday')); ?>
        {!! Form::text('from', $from, array('class' => 'datepicker small-8 columns form-control input-sm')) !!}
        </label>
    </div>
    <div class="small-6 medium-3 columns">
        <label> To
        <?php $to = (isset($input['to']) && $input['to'] != '') ? date('Y-m-d', strtotime($input['to'])) : date('Y-m-d', strtotime('today')); ?>
        {!! Form::text('to', $to, array('class' => 'datepicker small-8 columns form-control input-sm')) !!}
        </label>
    </div>


    <div class="small-6 medium-2 columns">
        <label> Limit
        <?php $limit = (isset($input['limit'])) ? $input['limit'] : 10; ?>
        {!! Form::text('limit', $limit, array('class' => 'small-8 columns form-control input-sm')) !!}
        </label>
    </div>


    <div class="small-6 medium-2 columns">
        <label> Sort by
        <?php $sort_by = (isset($input['sort_by'])) ? $input['sort_by'] : null; ?>
        {!! Form::select('sort_by', ['default' => 'Default', 'user_count' => 'User Count', 'activity_count' => 'Activity Count'], $sort_by, ['placeholder' => 'Sort By', 'class'=> 'form-control input-sm']) !!}
        </label>
    </div>


    <div class="small-6 medium-2 columns">
        <label> Sort Order
        <?php $sort_order = (isset($input['sort_order'])) ? $input['sort_order'] : null; ?>
        {!! Form::select('sort_order', ['dec' => 'Desc', 'Asc' => 'Asc'], $sort_order, ['placeholder' => 'Sort Order', 'class'=> 'small-12 columns form-control input-sm']) !!}
        </label>
    </div>


</div>
<div class="row">
    <div class="small-6 columns">

       <div class="small-12 columns">
           <strong>Activity Type:</strong>
       </div> 

        <?php
        $activity_type = (isset($input['activity_type'])) ? $input['activity_type'] : null;
        $activity_type = (is_array($activity_type)) ? array_flip($activity_type) : null;

        $activity_type_user = (isset($activity_type['user'])) ? true : false;
        $activity_type_category = (isset($activity_type['category'])) ? true : false;
        $activity_type_vendor = (isset($activity_type['vendor'])) ? true : false;
        $activity_type_page = (isset($activity_type['page'])) ? true : false;
        ?>

        <div class="small-3 columns">
            <label>
                {!! Form::checkbox('activity_type[]', 'user', $activity_type_user, ['class'=> 'form-control input-sm']) !!}  User
            </label>                 
        </div>
        <div class="small-3 columns">
            <label>
                {!! Form::checkbox('activity_type[]', 'category', $activity_type_category, ['class'=> 'form-control input-sm']) !!} Category
            </label>
        </div>
        <div class="small-3 columns">
                {!! Form::checkbox('activity_type[]', 'vendor', $activity_type_vendor, ['class'=> 'form-control input-sm']) !!} Vendor
        </div>
        <div class="small-3 columns">
            <label>
                {!! Form::checkbox('activity_type[]', 'page', $activity_type_page, ['class'=> 'form-control input-sm']) !!}  Page
            </label>
        </div>
       

    </div>

    <div class="small-4 columns">


        <?php
        $group_by = (isset($input['group_by'])) ? $input['group_by'] : null;
        $group_by = (is_array($group_by)) ? array_flip($group_by) : null;

        $group_by_user = (isset($group_by['user'])) ? true : false;
        $group_by_subject = (isset($group_by['subject'])) ? true : false;
        $group_by_type = (isset($group_by['activity_type'])) ? true : false;
        ?>
        <div class="small-12 columns">
            <strong>Group By:</strong>
        </div>
        <div class="small-4 columns">
            <label>
                {!! Form::checkbox('group_by[]', 'user', $group_by_user, ['class'=> 'form-control input-sm']) !!} 
                User
            </label>
        </div>
        <div class="small-4 columns">
            <label>
                {!! Form::checkbox('group_by[]', 'subject', $group_by_subject, ['class'=> 'form-control input-sm']) !!}  Subject 
            </label> 
        </div>
        <div class="small-4 columns">
            <label>
                {!! Form::checkbox('group_by[]', 'activity_type', $group_by_type, ['class'=> 'form-control input-sm']) !!}  Type
            </label>
        </div>
        


    </div>
    <div class="small-2 columns">
        <div class="small-12 columns"> &nbsp;</div>
        <div class="small-12 columns">
            <label>
                {!! Form::submit('Filter', ['class' => 'btn btn-default']) !!}
            </label>
        </div>
    </div>
</div>
{!! Form::close() !!}
