{!! HTML::style('/assets/css/parsley.css') !!}
{!! HTML::script('/assets/plugins/parsley.min.js') !!}
<script src="/js/foundation-datepicker.min.js"></script>
<script type="text/javascript">
    $('.form').parsley({
        errorsWrapper: '<div></div>',
        errorTemplate: ' <div class="alert alert-danger"></div>'
    });
    $("#front-message").click(function (e) {
        e.preventDefault()
        $("#inbox").tab('show')
    })

    $('[data-toggle="tabajax"]').click(function (e) {
        var $this = $(this),
                loadurl = $this.attr('href'),
                targ = $this.attr('data-target');

        $.get(loadurl, function (data) {
            $(targ).html(data);
        });

        $this.tab('show');
        return false;
    });
</script>

{!! HTML::script('/assets/js/ajaxify_bliss.script.js') !!}
<script>

    $('#jobs-board-btn').click(function (e) {
        e.preventDefault();
        var target_id = $(this).attr('href');
        //just for the jobs-container
        ajaxify_bliss({
            container: $(target_id + ' #jobs-container'),
            container_message: $(target_id + ' #jobs-container-message'),
            bindKeyboardBack: false,
            allowHistoryPushState: false
        });

        $(this).tab('show');
        return false;
    });
</script>
@if($threads && $threads->count() > 0)
    @foreach($threads as $thread)
        @if($thread)
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#send-btn{{ $thread->id }}').click(function (event) {
                        event.preventDefault();
                        if($.trim($('textarea[id=message{{ $thread->id }}]').val()) == '') {
                            $('textarea[id=message{{ $thread->id }}]').css('border', '1px solid red');
                            setTimeout(function() {
                            $('textarea[id=message{{ $thread->id }}]').css('border', '1px solid #ccc');                            
                            }, 700);
                        }
                        else{
                            $(this).attr('disabled', 'disabled');
                            $(".ajax_loader").show();
                            $.ajax({
                                url: '/messages/send/{{ $thread->id }}',
                                type: "PUT",
                                data: {
                                    'message': $('textarea[id=message{{ $thread->id }}]').val(),
                                    '_token': $('input[name=_token]').val()
                                },
                                success: function () {
                                    $('#send-btn{{ $thread->id }}').prop('disabled', false);
                                    $(".ajax_loader").hide();
                                    $("#message{{ $thread->id }}").val('');

                                    @if( Auth::user()->hasRole('administrator'))
                                      $("#conv{{ $thread->id }}").load("/admin #conv{{ $thread->id }}");
                                    $(".stats").load("/admin .stats");
                                    @elseif( Auth::user()->hasRole('vendor'))
                                      $("#conv{{ $thread->id }}").load("/vndor #conv{{ $thread->id }}");
                                    $(".stats").load("/vndor .stats");
                                    @elseif( Auth::user()->hasRole('user'))
                                      $("#conv{{ $thread->id }}").load("/user #conv{{ $thread->id }}");
                                    $(".stats").load("/user .stats");
                                    @endif
                                }
                            });
                            return false;      
                        }
                        
                    });
                });
            </script>
        @endif
    @endforeach
@endif
@if(Auth::user()->bid && Auth::user()->bid->count() > 0)
    @foreach(Auth::user()->bid as $bid)
        @if($bid->thread)
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#send-job-btn{{ $bid->thread->id }}').click(function (event) {
                        event.preventDefault();
                        if($.trim($('textarea[id=job-message{{ $bid->thread->id }}]').val()) == '') {
                                $('textarea[id=job-message{{ $bid->thread->id }}]').css('border', '1px solid red');
                                 setTimeout(function() {
                                $('textarea[id=job-message{{ $bid->thread->id }}]').css('border', '1px solid #ccc');                            
                                }, 700);
                            }
                        else{
                            $(this).attr('disabled', 'disabled');
                            $(".ajax_loader").show();
                            $.ajax({
                                url: '/messages/send/{{ $bid->thread->id }}',
                                type: "PUT",
                                data: {
                                    'message': $('textarea[id=job-message{{ $bid->thread->id }}]').val(),
                                    '_token': $('input[name=_token]').val()
                                },
                                success: function () {
                                    $('#send-job-btn{{ $bid->thread->id }}').prop('disabled', false);
                                    $(".ajax_loader").hide();
                                    $("#job-message{{ $bid->thread->id }}").val('');

                                    @if( Auth::user()->hasRole('administrator'))
                                      $("#job-conv{{ $bid->thread->id }}").load("/admin #job-conv{{ $bid->thread->id }}");
                                    $(".stats").load("/admin .stats");
                                    @elseif( Auth::user()->hasRole('vendor'))
                                      $("#job-conv{{ $bid->thread->id }}").load("/vndor #job-conv{{ $bid->thread->id }}");
                                    $(".stats").load("/vndor .stats");
                                    @elseif( Auth::user()->hasRole('user'))
                                      $("#job-conv{{ $bid->thread->id }}").load("/user #job-conv{{ $bid->thread->id }}");
                                    $(".stats").load("/user .stats");
                                    @endif
                                }
                            });
                            return false;
                        }
                    });
                });
            </script>
        @endif
    @endforeach
@endif
@if(Auth::user()->job && Auth::user()->job->count() > 0)
    @foreach(Auth::user()->job as $job)
        @foreach($job->bids as $bid)
            @if($bid->thread)
                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#send-job-btn{{ $bid->thread->id }}').click(function (event) {
                            event.preventDefault();
                        if($.trim($('textarea[id=job-message{{ $bid->thread->id }}]').val()) == '') {
                            $('textarea[id=job-message{{ $bid->thread->id }}]').css('border', '1px solid red');
                             setTimeout(function() {
                            $('textarea[id=job-message{{ $bid->thread->id }}]').css('border', '1px solid #ccc');                            
                            }, 700);
                        }
                        else{
                            $(this).attr('disabled', 'disabled');
                            $(".ajax_loader").show();
                            $.ajax({
                                url: '/messages/send/{{ $bid->thread->id }}',
                                type: "PUT",
                                data: {
                                    'message': $('textarea[id=job-message{{ $bid->thread->id }}]').val(),
                                    '_token': $('input[name=_token]').val()
                                },
                                success: function () {
                                    $('#send-job-btn{{ $bid->thread->id }}').prop('disabled', false);
                                    $(".ajax_loader").hide();
                                    $("#job-message{{ $bid->thread->id }}").val('');

                                    @if( Auth::user()->hasRole('administrator'))
                                      $("#job-conv{{ $bid->thread->id }}").load("/admin #job-conv{{ $bid->thread->id }}");
                                    $(".stats").load("/admin .stats");
                                    @elseif( Auth::user()->hasRole('vendor'))
                                      $("#job-conv{{ $bid->thread->id }}").load("/vndor #job-conv{{ $bid->thread->id }}");
                                    $(".stats").load("/vndor .stats");
                                    @elseif( Auth::user()->hasRole('user'))
                                      $("#job-conv{{ $bid->thread->id }}").load("/user #job-conv{{ $bid->thread->id }}");
                                    $(".stats").load("/user .stats");
                                    @endif
                                }
                            });
                            return false;
                        }
                        });
                    });
                </script>
            @endif
        @endforeach
    @endforeach
@endif
<script type="text/javascript">
    $('#send-advert').click(function (event) {
        event.preventDefault();
        $(".ajax_loader").show();
        $.ajax({
             type: "POST",
             url: 'send-advert',
             data: $('form[name=send-advert]').serialize(),
             success: function() {
                $(".flash").html('<div class="alert alert-success text-center">  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> Message sent successfully, we will get back to you soon</div>');
               $(".ajax_loader").hide();

             }
        });
    });
</script>
<script type="text/javascript">
    $('#review-request').click(function (event) {
        event.preventDefault();
        $(".ajax_loader").show();
        $.ajax({
             type: "POST",
             url: 'review-request',
             data: $('form[name=review-request]').serialize(),
             success: function() {
                $(".flash-r").html('<div class="alert alert-success text-center"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> Request for review has been sent successfully</div>');
               $(".ajax_loader").hide();

             }
        });
    });
</script>