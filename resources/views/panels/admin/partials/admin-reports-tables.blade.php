<div>
    <div>
        @include('panels.admin.partials.admin-reports-analytics-form')
    </div>

    <h4>{{ (isset($sub_title_report_name)) ? $sub_title_report_name : '' }}</h4>
</div>


@if(@$data)
    <table id="activity-reports-table" class="table table-striped table-bordered small-12 columns">
        <thead>
        <tr>
            @foreach($data as $row_data)
                @foreach($row_data as $column_title => $column_value)
                    <th>{{ ucwords(strtolower(str_replace('_', ' ', $column_title))) }}</th>
                @endforeach
                <?php break; ?>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($data as $row_data)
            <tr>
                @foreach($row_data as $column_value)
                    <td>{{$column_value}}</td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>

    @if(is_object($data))
        {!! $data->render(Foundation::paginate($data)) !!}
    @endif
@else
    <div>
        No Report data found
    </div>
@endif
