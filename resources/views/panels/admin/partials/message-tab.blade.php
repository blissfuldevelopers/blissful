<!-- Start Page Header -->
<div class="page-header" id="message-page-header">
    <h1 class="title">Messages</h1>
    <ol class="breadcrumb">
        <li class="active">You have <span class="label label-danger {{ @$cssClass }}">{!! @$count !!}</span>{!! @$number !!}
            unread {!! @$Unreadmessage !!}</li>
    </ol>
    <!-- Start Page Header Right Div -->
    <!-- <div class="right">
      <a href="#" class="btn btn-danger"><i class="fa fa-pencil"></i> COMPOSE</a>
    </div> -->
    <!-- End Page Header Right Div -->
</div>
<!-- End Page Header -->
<!-- //////////////////////////////////////////////////////////////////////////// -->
<!-- START CONTAINER -->
<div class="container-mail">
    <!-- Start Mailbox -->
    <div class="mailbox clearfix">
        <!-- Start Mailbox Container -->
        <div class="container-mailbox">
            <!-- Start Mailbox Inbox -->
            <div class="col-lg-3 col-md-4">
                <ul class="mailbox-inbox nav nav-tabs">
                    <li class="search">
                        <form>
                            <input type="text" class="mailbox-search" id="mailboxsearch" placeholder="Search">
                            <span class="searchbutton"><i class="fa fa-search"></i></span>
                        </form>
                    </li>
                    @if(@$threads)
                        @foreach($threads as $thread)
                            @if($thread)
                                <?php $class = $thread->isUnread($currentUserId) ? 'alert-info' : ''; ?>
                                <li>
                                    <a href="#thread_{{$thread->id}}" data-toggle="tab"
                                       class="item clearfix {{$class}}">
                                        <img src=@foreach($thread->participants as $participant)
                                        @if($participant->user_id != $currentUserId
                                        && $participant->user
                                        && @$participant->user->profile->profilePic)
                                                "/{{$participant->user->profile->profilePic}}"
                                             @endif
                                             @endforeach
                                             alt="img" class="img">
                                        <span class="from">{!! $thread->participantsString(Auth::id(), ['first_name']) !!}</span>
                                        {!! str_limit($thread->latestMessage->body, 75) !!}
                                    </a>
                                </li>
                            @endif
                        @endforeach
                    @else
                        <ul>
                            <li>
                                <p> Sorry, no messages to display</p>
                            </li>
                        </ul>
                    @endif
                </ul>
            </div>
            <!-- End Mailbox Inbox -->

            <!-- Start Chat -->
            <div class="tab-content">
                @if(@$threads)
                    @foreach($threads as $thread)
                        @if($thread)
                            <div class="chat tab-pane fade col-lg-9 col-md-8" id="thread_{{$thread->id}}">
                                <!-- Start Title -->
                                <div class="title">
                                    <h1>{!! $thread->subject !!} <!-- <small>( mail@jonathandoe.com )</small> --></h1>
                                    <p><b>To:</b>{!! $thread->participantsString(Auth::id(), ['first_name']) !!}</p>
                                    <!-- <div class="btn-group" role="group" aria-label="...">
                                      <button type="button" class="btn btn-icon btn-sm btn-light"><i class="fa fa-share"></i></button>
                                      <button type="button" class="btn btn-icon btn-sm btn-light"><i class="fa fa-star-o"></i></button>
                                      <button type="button" class="btn btn-icon btn-sm btn-light"><i class="fa fa-trash"></i></button>
                                    </div> -->
                                </div>
                                <!-- End Title -->

                                <!-- Start Conv -->
                                <ul class="conv" id="conv{{$thread->id}}">
                                    @foreach($thread->messages()->get() as $message)
                                        @if($message)
                                            <li>
                                                <img src="/{{$message->user->profile->profilePic}}" alt="img"
                                                     class="img">
                                                <p class="ballon {{ (($message->user->id) == (Auth::user()->id)) ? 'color2' : 'color1' }}"
                                                >
                                                    {!! $message->body !!}<br>
                                                    <i class="sm">{!! $message->created_at->diffForHumans() !!}</i>
                                                </p>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                                <!-- End Conv -->
                                <div class="write">

                                    {!! Form::open(['route' => ['messages.update', $thread->id], 'method' => 'PUT', 'class'=>'margin-b-20', 'id'=>'form']) !!}
                                            <!-- Message Form Input -->
                                    <div class="form-group">
                                        <div class="ajax_loader"
                                             style="position: absolute; left: 45%; bottom: 160px; display:none">
                                            <img src="/img/rolling.svg">
                                        </div>
                                        {!! Form::textarea('message', null, [
                                        'class' => 'form-control ',
                                        'id' => 'message'.$thread->id,
                                        'required',
                                        'data-parsley-required-message' => 'Please write a message :)',
                                        'data-parsley-minlength'        => '2',
                                        'data-parsley-trigger'          => 'change focusout'
                                        ]) !!}
                                    </div>
                                    <input type="hidden" name="recipients" id="recipients"
                                           value="{{$message->user->profile->id}}">
                                    <!-- Submit Form Input -->
                                    <div class="form-group">
                                        <button id="send-btn{{$thread->id}}" class="btn btn-default">Send</button>
                                        <button type="reset" class="btn margin-l-5">Clear</button>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
            <!-- End Chat -->
        </div>
        <!-- End Mailbox Container -->
    </div>
</div>
<!-- End Mailbox -->