@extends('layouts.kode')
@section('title')
    Dashboard
@endsection
@section('styles')
    <link rel="stylesheet" href="css/dataTables.bootstrap.min.css">
    <?php
          $count = Auth::user()->newMessagesCount();
          $cssClass = $count == 0 ? 'hidden' : '';
          $number = $count == 0 ? 'no' : '';
          $Unreadmessage = $count == 1 ? 'message' : 'messages';
          ?>
            <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START SIDEBAR -->
    <div class="sidebar clearfix">

        <ul class="sidebar-panel nav nav-tabs">
            <li class="sidetitle">MAIN</li>
            <li class="active">
                <a href="#main" data-toggle="tab">
                    <span class="icon color5">
                      <i class="fa fa-home"></i>
                    </span>
                    Dashboard
                    <span class="label label-default"></span>
                </a>
            </li>
            <li>
                <a href="#inbox" data-toggle="tab">
                    <span class="icon color6">
                      <i class="fa fa-envelope-o"></i>
                    </span>
                    Messages
                    <span class="label label-default {{$cssClass}}">{{$count}}</span>
                </a>
            </li>
            <li>
                <a href="#administration" data-toggle="tab">
                    <span class="icon color11">
                      <i class="fa fa-diamond"></i>
                    </span>
                    <span class="label label-default"></span>
                    Administration
                </a>
            </li>
            <li>
                <a href="#bundles" data-toggle="tab">
                    <span class="icon color6">
                      <i class="fa fa-bitcoin"></i>
                    </span>
                    Credit Bundles
                </a>
            </li>


            <li>
                <a href="#activity-reports" data-toggle="tab">
                    <span class="icon color6">
                      <i class="fa fa-table"></i>
                    </span>
                    Activity Reports
                </a>
            </li>

            <li>
                <a href="#messages" data-toggle="tab">
                    <span class="icon color6">
                      <i class="fa fa-folder"></i>
                    </span>
                    Messages Log
                </a>
            </li>

            <li>
                <a href="#activity" data-toggle="tab">
                    <span class="icon color6">
                      <i class="fa fa-tasks"></i>
                    </span>
                    Activity Log
                </a>
            </li>
        </ul>
    </div>
    <!-- END SIDEBAR -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START CONTENT -->
    <div class="content tab-content">

        <!-- START OF MAIN TAB CONTENT -->
        <div class="tab-pane active" id="main">
            <!-- Start Page Header -->
            <!-- Start Top Stats -->
            <div class="col-md-12">
                <div class="page-header">
                    <h1 class="title">Dashboard</h1>
                    <ol class="breadcrumb">
                        <li class="active">Quick look at how you are doing :)</li>
                    </ol>
                </div>
                <!-- End Page Header -->
                <ul class="topstats clearfix">
                    <li class="arrow"></li>
                    <li class="col-xs-6 col-lg-2">
                        <span class="title"><i class="fa fa-users"></i> New Vendors</span>

                        <h3>{{$vendor_no}}</h3>
                            <span class="diff"><b class="color-up"><i
                                            class="fa fa-caret-up"></i></b> from yesterday</span>
                    </li>
                    <!-- <li class="col-xs-6 col-lg-2">
                      <span class="title"><i class="fa fa-comments"></i> Comments</span>
                      <h3>960</h3>
                      <span class="diff"><b class="color-up"><i class="fa fa-caret-up"></i> 26%</b> from last yesterday</span>
                    </li> -->
                    {{--<li class="col-xs-6 col-lg-2">--}}
                    {{--<span class="title"><i class="fa fa-eye"></i> Page Views</span>--}}
                    {{--<h3 class="color-up">{{$vendor_view}}</h3>--}}
                    {{--</li>--}}
                    <li class="col-xs-6 col-lg-2">
                        <span class="title"><i class="fa fa-user"></i>Registered Vendors</span>

                        <h3 class="color-up">{{$vendors_count}}</h3>
                    </li>
                    <li class="col-xs-6 col-lg-2">
                        <span class="title"><i class="fa fa-user"></i>Registered Users</span>

                        <h3 class="color-up">{{$clients_count}}</h3>
                    </li>
                </ul>
            </div>
            <!-- End Top Stats -->

            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTAINER -->
            <div class="container-widget">

                <!-- Start First Row -->
                <div class="row">
                    <!-- Start Files -->
                    <div class="col-md-12 col-lg-3">
                        <div class="widget profile-widget"
                             style="height:380px;">
                            <img src="/{{Auth::user()->profile->profilePic}}" class="profile-image" alt="img">

                            <h1>{{Auth::user()->first_name}}</h1>

                            <p><i class="fa fa-map-marker"></i> {{Auth::user()->profile->location}}</p>
                            <a href="{{ url('profile/edit/'.Auth::user()->profile->user_id) }}" class="btn btn-sm">Edit
                                Profile</a>
                        </div>
                    </div>
                    <!-- Start of New Vendor List -->
                    <div class="col-md-12 col-lg-3">
                        <div class="panel panel-widget">
                            <div class="panel-title">
                                New Vendors
                                <ul class="panel-tools panel-tools-hover">
                                    <li><a class="icon"><i class="fa fa-refresh"></i></a></li>
                                    <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
                                    <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
                                    <li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <!-- start new vendors -->
                                <ul class="basic-list">
                                    @if(empty($vendor_new))
                                        <li>No new vendors</li>
                                    @else
                                        @foreach($vendor_new as $vendr)
                                            <li><a href="/vendors/{{$vendr->slug}}">{{$vendr->first_name}}</a></li>
                                        @endforeach
                                    @endif
                                </ul>
                                <!-- end new vendors list -->
                            </div>
                        </div>
                    </div>
                    <!-- End of New Vendor List -->
                    <!-- End Files -->
                    <div class="col-md-12 col-lg-6">
                        <div class="panel panel-widget materialize">
                            <div class="panel-title">
                                Inbox <span class="label label-danger {{$cssClass}}">{{$count}}</span>
                                <ul class="panel-tools">
                                    <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
                                    <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
                                    <li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <ul class="mailbox-inbox">
                                    @if($threads->count() > 0)
                                        @foreach($threads as $thread)
                                            @if($thread->isUnread($currentUserId))
                                                <li>
                                                    <a href="#" id="front-message" class="item clearfix">
                                                        <img src=@foreach($thread->messages()->get() as $message)
                                                        @if(($message->user->id) != (Auth::user()->id))
                                                                "/{{$message->user->profile->profilePic}}"
                                                             @endif
                                                             @endforeach
                                                             alt="img" class="img">
                                                        <span class="from">{!! $thread->participantsString(Auth::id(), ['first_name']) !!}</span>
                                                        {!! str_limit($thread->latestMessage->body, 75) !!}
                                                        <span class="date">{{$thread->created_at->diffforhumans()}}</span>
                                                    </a>
                                                </li>
                                            @else
                                                <ul class="noMessage">
                                                    <li><p> Sorry, no new messages to display</p></li>
                                                </ul>
                                            @endif
                                        @endforeach
                                    @else
                                        <ul>
                                            <li><p> Sorry, no messages to display</p></li>
                                        </ul>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End First Row -->

                <!-- Start Second Row -->
                <div class="row">

                    <!-- Start Blog Post -->
                    @if($post->count() >0)
                        <div class="col-md-6 col-lg-8">
                            <div class="panel no_pad panel-widget blog-post">
                                <div class="panel-body">
                                    <div class="image-div color10-bg">
                                        <img src="{{ page_image($post->page_image) }}" class="image" alt="img">

                                        <h1 class="title"><a href="{{$post->url()}}">{{ $post->title }}</a></h1>
                                    </div>
                                    <p class="text">{!! str_limit($post->content, 160) !!}<a href="{{$post->url()}}">Read
                                            More</a></p>
                                    <!-- <p class="author">
                                      <img src="img/dashboard/profileimg.png" alt="img">
                                      <span>Jonathan Doe</span>
                                      Designer
                                    </p> -->
                                </div>
                            </div>
                        </div>
                        @endif
                                <!-- End Blog Post -->

                        <!-- Start of Ad Section -->
                        <div class="col-md-6 col-lg-3">

                            <div class="panel panel-default  padding-0">
                                <img src="img/dashboard/2.jpg">
                            </div>

                        </div>
                        <!-- End of Ad Section -->

                </div>
                <!-- End Second Row -->

            </div>
            <!-- END CONTAINER -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
        </div>
        <!-- END OF MAIN TAB CONTENT -->

        <!-- START OF INBOX TAB CONTENT -->
        <div class="tab-pane" id="inbox">
            @include('panels.admin.partials.message-tab')
        </div>
        <!-- END OF INBOX TAB CONTENT -->

        <!-- START OF GALLERY TAB CONTENT -->
        <div class="tab-pane" id="administration">
            <!-- Start Page Header -->
            <div class="page-header">
                <h1 class="title">Administration</h1>
                <ol class="breadcrumb">
                    <li class="active">Site Administration</li>
                </ol>
            </div>
            <!-- End Page Header -->

            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTAINER -->
            <div class="container-widget">
                <div class="row">
                    <!-- Start Panel -->
                    <div class="col-md-6 col-lg-4">
                        <div class="panel panel-primary">
                            <div class="panel-title">
                                User Management
                                <ul class="panel-tools">
                                    <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
                                    <li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <li><a class="white" href="/users">Manage Users</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End Panel -->
                    <!-- Start Panel -->
                    <div class="col-md-6 col-lg-4">
                        <div class="panel panel-primary">
                            <div class="panel-title">
                                Site Content
                                <ul class="panel-tools">
                                    <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
                                    <li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <li><a class="white" href="/admin/post">Manage Blogs</a></li>
                                    <li><a class="white" href="/admin/category">Blog Categories</a></li>
                                    <li><a class="white" href="/classification">Vendor Categories</a></li>
                                    <li><a class="white" href="/locations">Manage Locations</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End Panel -->
                </div>
            </div>
            <!-- END CONTAINER -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
        </div>
        <!-- END OF VENDORS TAB CONTENT -->
        <!-- activity reports -->
        <div class="tab-pane" id="bundles">
            <!-- Start Page Header -->
            <div class="page-header">
                <h1 class="title">Bundles</h1>
            </div>
            <!-- End Page Header -->

            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTAINER -->
            <div class="container-widget">

                <!-- Start First Row -->
                <h1>Creditbundles <a href="{{ route('admin.bundles.create') }}" class="btn btn-primary pull-right btn-sm">Add New
                        Creditbundles</a></h1>
                <div class="table">

                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Credits</th>
                            <th>Unit Price</th>
                            <th>Cost</th>
                            <th>Expiration Time</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{-- */$x=0;/* --}}
                        @foreach($creditbundles as $item)
                            {{-- */$x++;/* --}}

                            <tr>
                                <td>{{ $x }}</td>
                                <td><a href="{{ url('admin/bundles', $item->id) }}">{{ @$item->name }}</a></td>
                                <td>{{ @$item->description }}</td>
                                <td>{{ $item->credits }}</td>
                                <td>{{ $item->unit_price }}</td>
                                <td>{{ @$item->cost }}</td>
                                <td>{{ $item->expiration_time }}</td>
                                <td>
                                    <a href="{{ route('admin.bundles.edit', $item->id) }}">
                                        <button type="submit" class="btn btn-primary btn-xs">Update</button>
                                    </a> /
                                    {!! Form::open([
                                        'method'=>'DELETE',
                                        'route' => ['admin.bundles.destroy', $item->id],
                                        'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination"> {!! $creditbundles->render() !!} </div>
                </div>
                <!-- End First Row -->

            </div>
            <!-- END CONTAINER -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
        </div>
        <!-- end of activity reports -->

        <!-- activity reports -->
        <div class="tab-pane" id="activity-reports">
            <!-- Start Page Header -->
            <div class="page-header">
                <h1 class="title">Activity Reports</h1>
                <ol class="breadcrumb">
                    <li class="active">Activity Reports</li>
                </ol>
            </div>
            <!-- End Page Header -->

            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTAINER -->
            <div class="container-widget">

                <!-- Start First Row -->
                <div class="row">

                    <div class="col-xs-12">
                        <div class="panel panel-widget">
                            <div class="panel-title">
                                Activities Report
                                <ul class="panel-tools panel-tools-hover">
                                    <li><a class="icon"><i class="fa fa-refresh"></i></a></li>
                                    <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
                                    <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
                                    <li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <div id="activity-reports-result"
                                     data-url="{{ $activities_report_url }}?from={{ date('Y-m-d', strtotime('yesterday')) }}">

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- End First Row -->

            </div>
            <!-- END CONTAINER -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
        </div>
        <!-- end of activity reports -->
        <!-- activity reports -->
        <div class="tab-pane" id="messages">
            <!-- Start Page Header -->
            <div class="page-header">
                <h1 class="title">Messages Log</h1>
            </div>
            <!-- End Page Header -->

            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTAINER -->
            <div class="container-widget">

                <!-- Start First Row -->
                <div class="container">
                    <ul class="list-group">
                    @foreach(@$messages as $message)

                            <li class="list-group-item">

                                    <p>
                                    <p><strong>From: </strong> <a href ="users/{{@$message->user->id}}">{{@$message->user->first_name}}</a></p>
                                    <p><small><strong>To: </strong> <a href ="users/{{@$message->thread->participantsString(@$message->user_id, ['id'])}}">{!! @$message->thread->participantsString(@$message->user_id, ['first_name']) !!}</a></small></p>
                                    <strong>Message: </strong>{{@$message->body}}
                                    <p>{{@$message->created_at->diffForHumans()}}</p>
                                    </p>

                            </li>

                    @endforeach
                    </ul>
                </div>
                {!! $messages->render()!!}
                <!-- End First Row -->

            </div>
            <!-- END CONTAINER -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
        </div>
        <!-- end of activity reports -->
        <!-- activity reports -->
        <div class="tab-pane" id="activity">
            <!-- Start Page Header -->
            <div class="page-header">
                <h1 class="title">Activity Log</h1>
            </div>
            <!-- End Page Header -->

            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTAINER -->
            <div class="container-widget">

                <!-- Start First Row -->
                <ul class="list-group">
                    @include('admin.activities.list')
                </ul>
                <!-- End First Row -->

            </div>
            <!-- END CONTAINER -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
        </div>
        <!-- end of activity reports -->
    </div>
    <!-- End Content -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
@endsection
@section('scripts')
    @include('panels.admin.partials.dash-scripts')
    <script src="/js/jquery.dataTables.min.js"></script>
    <script src="/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.1/js/bootstrap-datepicker.min.js"></script>
    <script>
        $(document).ready(function () {
            var activity_reports_container = $('#activity-reports-result'),
                    default_link = activity_reports_container.data('url');

            reload_table(default_link);

            function bindPaginationAction() {
                var activity_reports_form = $('form.analytics-report-form');
                activity_reports_container.off('click', 'a');

                activity_reports_container.on('click', 'a', function (e) {
                    e.preventDefault();
                    reload_table($(this).attr('href'));
                });

                activity_reports_form.submit(function (e) {
                    e.preventDefault();
                    var form_url = $(this).attr('action'),
                            form_data = $(this).serialize(),
                            url = form_url + '?' + form_data;
                    reload_table(url);
                });
            }

            function reload_table(link) {
                $.ajax({
                    url: link,
                    method: "GET"
                }).done(function (data) {
                    activity_reports_container.html(data);
                    bindPaginationAction();

                    $('input.datepicker').datepicker({
                        format: "yyyy-mm-dd",
                        todayBtn: "linked"
                    });
                });


            }
        });
        //


    </script>
@stop