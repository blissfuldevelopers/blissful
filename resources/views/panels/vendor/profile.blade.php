@extends('page.zurb-layout')

@section('head')

@stop

@section('content')
 <div class="container">
	 <div class="row"> 
		<div class="ripple placard profile">
              <div class="image ">
                <img src="{{Auth::user()->profile->profilePic}}">
                <p>{{Auth::user()->profile->description}}</p>
           		 @foreach(Auth::user()->classifications as $classification)
				 <li>{{$classification->name}}</li>
				 @endforeach
              </div>
              <div class="content">
                <a href="{{ url('profile/edit/'.Auth::user()->profile->user_id) }}">Edit Profile <a href="#" class="link hide-for-small-only">&#10095;</a></a> 
              </div>
        </div>
	 </div>
 </div>
@stop