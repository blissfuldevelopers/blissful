<!-- Start Page Header -->
<div class="page-header" id="jobs">
    <h1 class="title">Jobs</h1>
    <ol class="breadcrumb">
        <li class="active">You have <span
                    class="label label-danger {{ $showHide }}">{!! $responses !!}</span>{!!$responseNumber!!}
            unread {!!$responseMessage!!}</li>
    </ol>
    <!-- Start Page Header Right Div -->
    
    <!-- End Page Header Right Div -->
</div>
<!-- End Page Header -->
<!-- //////////////////////////////////////////////////////////////////////////// -->
<!-- START CONTAINER -->
<div class="container-mail">
    <!-- Start Mailbox -->
    <div class="mailbox clearfix">
        <!-- Start Mailbox Container -->
        <div class="container-mailbox">
            <!-- Start Mailbox Inbox -->
            <div class="col-lg-3 col-md-4">
                <ul class="mailbox-inbox nav nav-tabs">
                    @if(Auth::user()->bid->count() > 0)
                        @foreach(Auth::user()->bid as $bid)
                            @if(@$bid->thread)
                                <?php $class = ( $bid->thread && $bid->thread->isUnread($currentUserId) ) ? 'alert-info' : ''; ?>
                                <li>
                                    <a href="#job_{{ @$bid->thread->id }}" data-toggle="tab"
                                       class="item clearfix {{ @$class }}">
                                        <img src="{!! @$bid->thread->latestMessage->user->profile->profilePic!!}"alt="img" class="img">
                                        <span class="from">{!! @$bid->thread->participantsString(Auth::id(), ['first_name']) !!}</span>
                                        {!! str_limit(@$bid->thread->latestMessage->body, 75) !!}
                                                <!-- <span class="date">{{ $bid->thread->created_at->diffForHumans() }}</span> -->
                                    </a>
                                </li>
                            @endif
                        @endforeach
                    @else
                        <ul>
                            <li>
                                <p> Sorry, no Price Estimates to display</p>
                            </li>
                        </ul>
                    @endif
                </ul>
            </div>
            <!-- End Mailbox Inbox -->

            <!-- Start Chat -->
            <div class="tab-content">
                @if(Auth::user()->bid->count() > 0)
                    @foreach(Auth::user()->bid as $bid)
                        @if(@$bid->thread)
                            <div class="chat tab-pane fade col-lg-9 col-md-8" id="job_{{ $bid->thread->id }}">
                                <!-- Start Title -->
                                <div class="title">
                                    <h1>
                                        {!! @$bid->thread->subject !!} <!-- <small>( mail@jonathandoe.com )</small> --></h1>
                                    <p><b>To:</b>{!! @$bid->thread->participantsString(Auth::id(), ['first_name']) !!}
                                    </p>
                                    <!-- <div class="btn-group" role="group" aria-label="...">
                                      <button type="button" class="btn btn-icon btn-sm btn-light"><i class="fa fa-share"></i></button>
                                      <button type="button" class="btn btn-icon btn-sm btn-light"><i class="fa fa-star-o"></i></button>
                                      <button type="button" class="btn btn-icon btn-sm btn-light"><i class="fa fa-trash"></i></button>
                                    </div> -->
                                </div>
                                <!-- End Title -->

                                <!-- Start Conv -->
                                <ul class="conv" id="job-conv{{ $bid->thread->id }}">
                                    @foreach(@$bid->thread->messages()->get() as $message)
                                        <li>
                                            <img src="/{{ $message->user->profile->profilePic }}" alt="img" class="img">
                                            <p class="ballon {{ (($message->user->id) == (Auth::user()->id)) ? 'color2' : 'color1'  }} ">{!! $message->body !!}
                                                <br>
                                                <i class="sm">{!! $message->created_at->diffForHumans() !!}</i>
                                            </p>
                                        </li>
                                    @endforeach
                                </ul>
                                <!-- End Conv -->
                                <div class="write">

                                    {!! Form::open(['route' => ['messages.update', $bid->thread->id], 'method' => 'PUT', 'class'=>'margin-b-20 form', 'data-parsley-validate']) !!}
                                            <!-- Message Form Input -->
                                    <div class="form-group">
                                        <div class="ajax_loader"
                                             style="position: absolute; left: 45%; bottom: 160px; display:none">
                                            <img src="/img/rolling.svg">
                                        </div>
                                        {!! Form::textarea('message', null, [
                                        'class' => 'form-control ',
                                        'id' => 'job-message'.$bid->thread->id,
                                        'required',
                                        'data-parsley-required-message' => 'Please write a message :)',
                                        'data-parsley-minlength'        => '2',
                                        'data-parsley-trigger'          => 'change focusout'
                                        ]) !!}
                                    </div>
                                    <input type="hidden" name="recipients" id="recipients"
                                           value="{{ @$bid->thread->participantsString(Auth::id(), ['id']) }}">
                                    <!-- Submit Form Input -->
                                    <div class="form-group">
                                        <button id="send-job-btn{{ $bid->thread->id }}" class="btn btn-default">Send
                                        </button>
                                        <button type="reset" class="btn margin-l-5">Clear</button>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
            <!-- End Chat -->
        </div>
        <!-- End Mailbox Container -->
    </div>
</div>
<!-- End Mailbox -->