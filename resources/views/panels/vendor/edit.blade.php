
<div class="login-form edit-blade">
    @include('includes.status')
    {!! Form::model($user, ['route' => ['update.profile', $user->id],'method' => 'POST','enctype'=>'multipart/form-data','class' =>'submittor breaker','data-parsley-validate']) !!}
    <div class="row">
      
      <div class="small-12 medium-3 columns collapse">
        <p>Use this section to manage your profile information to help others find you and make it easier for them to get in touch.</p>
        <!-- <div class="profile-img-container small-6">
          <img class="edit-img" src="{{'/'.$user->profile->profilePic}}">
        </div>
        <p>{{$user->first_name}}</p>
        <p>{{$user->email}}</p> -->
      </div>

      <div class="small-12 medium-8 columns d-p-container">
        <div class="row">
          <div class="profile-dets small-12 columns">
              <img class="circle" src="{{'https://s3.eu-central-1.amazonaws.com/blissful-ke/'.$user->profile->profilePic}}">         
              <input type="file" name="file" id="file">
          </div>
        </div>
        <div class="small-12 columns">
          <span class="profile-title">
            {!! Form::label('first_name', 'First Name') !!}
          </span>
          <span class="profile-dets">
            {!! Form::text('first_name',null,[
            'required',
            'data-parsley-required-message' => 'first name is required',
            'data-parsley-trigger'          => 'change focusout'
            ]) !!}
          </span>
        </div>
        <div class="small-12 columns">
          <span class="profile-title">
            {!! Form::label('last_name', 'Last Name') !!}
          </span>
          <span class="profile-dets">
            {!! Form::text('last_name',null,[
            'required',
            'data-parsley-required-message' => 'last name is required',
            'data-parsley-trigger'          => 'change focusout'
            ]) !!}
          </span>
        </div>
        <div class="small-12 columns">
          <span class="profile-title">
            {!! Form::label('phone', 'Phone') !!}
          </span>
          <span class="profile-dets">
            {!! Form::text('phone',$user->profile->phone,[
            'required',
            'data-parsley-required-message' => 'Phone number is required',
            'data-parsley-trigger'          => 'change focusout'
            ]) !!}
          </span>
        </div>
        <div class="small-12 columns">
          <span class="profile-title">
            {!! Form::label('password_input', 'New Password') !!}
          </span>
          <span class="profile-dets">
            <input name="password_input" type="password" value="" id="password_input" class="small-9 columns"
                 data-parsley-trigger="change focusout"
                 data-parsley-minlength="6"
                 data-parsley-maxlength="20"
                  >
          </span>
        </div>
        <div class="small-12 columns">
          <span class="profile-title">
            {!! Form::label('password_confirm', 'Confirm Password') !!}
          </span>
          <span class="profile-dets">
            <input name="password" type="password" value="" id="password" class="small-9 columns"
                 data-parsley-trigger="change focusout"
                 data-parsley-equalto="#password_input"
                 data-parsley-equalto-message="Not same as Password"
                  >
          </span>
        </div>
        <div class="small-12 columns">
            <input type="submit" id="submittor" class="button button-primary withimg right" onclick="withimg(event);" value=" Update Profile">
        </div>
      </div>
    </div>
    {!! Form::close() !!}
</div>
