@extends('layouts.kode')
@section('title')
    Dashboard
@endsection
@section('styles')
    <link rel="stylesheet" href="/css/dashboard/root.css"/>
    <link rel="stylesheet" href="/css/dashboard/dashes.css"/>
    {!! HTML::style('/assets/css/parsley.css') !!}
@endsection
@section('content')
    <div class="stats">
        <?php
        foreach ($threads as $thread) {
            $array[] = $thread->isUnread($currentUserId);
        }
        if (isset($array)) {
            $count = count(array_filter($array));
        }
        else{
            $count=0;
        }
        $cssClass = $count == 0 ? 'hidden' : '';
        $number = $count == 0 ? 'no' : '';
        $Unreadmessage = $count == 1 ? 'message' : 'messages';
        ?>


        <?php
        if (Auth::user()->bid->count() == 0) {
            $responses = 0;
        }
        else {

            foreach (Auth::user()->bid as $bid) {
                $array = ( $bid->thread ) ? array( $bid->thread->isUnread($currentUserId) ) : [ ];
            }
            $responses = count(array_filter($array));
        }
        $showHide = $responses == 0 ? 'hidden' : '';
        $responseNumber = $responses == 0 ? 'no' : '';
        $responseMessage = $responses == 1 ? 'response' : 'responses';
        ?>
    </div>


    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START SIDEBAR -->
    <div class="sidebar clearfix">

        <ul class="sidebar-panel nav nav-tabs">
            <li class="sidetitle">MAIN</li>
            <li class="active">
                <a href="#main" data-toggle="tab">
                    <span class="icon color5">
                      <i class="fa fa-home"></i>
                    </span>
                    Dashboard
                    <span class="label label-default"></span>
                </a>
            </li>
            <li>
                <a href="#jobs-board" id='jobs-board-btn' data-toggle="tab">
                    <span class="icon color6">
                      <i class="fa fa-briefcase"></i>
                    </span>
                    Available Jobs
                </a>
            </li>
            <li>
                <a href="#inbox" data-toggle="tab">
                    <span class="icon color6">
                      <i class="fa fa-envelope-o"></i>
                    </span>
                    Messages
                    <span class="label label-default {{$cssClass}}">{{$count}}</span>
                </a>
            </li>
            <li>
                <a href="#jobs" data-toggle="tab">
            <span class="icon color6">
              <i class="fa fa-quote-right"></i>
            </span>
                    Jobs Responses
                    <span class="label label-default {{$showHide}}">{{$responses}}</span>
                </a>
            </li>
            <li>
                <a href="#gallery" data-toggle="tab">
                    <span class="icon color11">
                      <i class="fa fa-picture-o"></i>
                    </span>
                    <span class="label label-default"></span>
                    Gallery
                </a>
            <li>
                <a href="#advert" data-toggle="tab">
                <span class="icon color11">
                  <i class="fa fa-magic"></i>
                </span>
                    <span class="label label-default"></span>
                    Promote your Business
                </a>
            </li>
            <li>
                <a href="#review" data-toggle="tab">
                <span class="icon color11">
                  <i class="fa fa-pencil"></i>
                </span>
                    <span class="label label-default"></span>
                    Request for a review
                </a>
            </li>
        </ul>

    </div>
    <!-- END SIDEBAR -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START CONTENT -->
    <div class="content tab-content">

        <!-- START OF MAIN TAB CONTENT -->
        <div class="tab-pane active" id="main">
            <!-- Start Page Header -->
            <div class="page-header">
                <h1 class="title">Dashboard</h1>
                <ol class="breadcrumb">
                    <li class="active">Quick look at how you are doing :)</li>
                </ol>
            </div>
            <!-- End Page Header -->

            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTAINER -->
            <div class="container-widget">

                <!-- Start First Row -->
                <div class="row">
                    <!-- Start Files -->
                    <div class="col-sm-12 no_pad col-md-3 ">
                        <div class="widget profile-widget" style="height:380px;">
                            <img src="/{{Auth::user()->profile->profilePic}}" class="profile-image" alt="img">
                            <h1>{{Auth::user()->first_name}}</h1>
                            <p><i class="fa fa-map-marker"></i> {{Auth::user()->profile->location}}</p>
                            <a href="{{route('users.edit',array(Auth::user()->profile->user_id))}}" class="btn btn-sm">Edit
                                Profile</a>
                        </div>
                    </div>
                    <!-- End Files -->
                    <div class="col-sm-12 col-md-4">
                        <div class="panel panel-widget materialize">
                            <div class="panel-title">
                                Job Responses <span class="label label-danger {{$showHide}}">{{$responses}}</span>
                                <ul class="panel-tools">
                                    <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
                                    <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <ul class="mailbox-inbox">
                                    @if(Auth::user()->bid->count() > 0)
                                        @foreach(Auth::user()->bid as $bid)
                                            @if($bid->thread && $bid->thread->isUnread($currentUserId))
                                                <li>
                                                    <a href="#" class="item clearfix">
                                                        <img src="{!! $bid->thread->latestMessage->user->profile->profilePic!!}"alt="img" class="img">
                                                        <span class="from">{!! $bid->thread->participantsString(Auth::id(), ['first_name']) !!}</span>
                                                        @if($bid->thread->latestMessage->body)
                                                            {!! str_limit($bid->thread->latestMessage->body, 75) !!}
                                                        @endif
                                                        <span class="date">{{$bid->thread->latestMessage->created_at->diffforhumans()}}</span>
                                                    </a>
                                                </li>
                                                
                                            @endif
                                        @endforeach
                                        @if($responses == 0)
                                            <ul>
                                                <li><p> Sorry, no new Job responses to display</p></li>
                                            </ul>
                                        @endif
                                    @else
                                        <ul>
                                            <li><p> Looks like You haven't applied for a Job yet..</p></li>
                                        </ul>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-5">
                        <div class="panel panel-widget materialize">
                            <div class="panel-title">
                                Inbox <span class="label label-danger {{$cssClass}}">{{$count}}</span>
                                <ul class="panel-tools">
                                    <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
                                    <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <ul class="mailbox-inbox">
                                    @if(@$threads)
                                        @foreach($threads as $thread)
                                            @if($thread->isUnread($currentUserId))
                                                <li>
                                                    <a href="#" class="item clearfix">
                                                        <img src="{!! $thread->latestMessage->user->profile->profilePic!!}"alt="img" class="img">
                                                        <span class="from">{!! $thread->participantsString(Auth::id(), ['first_name']) !!}</span>
                                                        @if($thread->latestMessage->body)
                                                            {!! str_limit($thread->latestMessage->body, 75) !!}
                                                        @endif
                                                        <span class="date">{{$thread->latestMessage->created_at->diffforhumans()}}</span>
                                                    </a>
                                                </li>
                                            @endif
                                        @endforeach
                                        @if($count == 0)
                                            <ul>
                                                <li><p> Sorry, no new message to display</p></li>
                                            </ul>
                                        @endif
                                    @else
                                        <ul>
                                            <li><p> Sorry, no messages to display</p></li>
                                        </ul>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End First Row -->

                <!-- Start Second Row -->
                <div class="row">

                    <!-- Start Blog Post -->
                    @if($post->count() >0)
                        <div class="col-md-6 col-lg-8">
                            <div class="panel no_pad panel-widget blog-post">
                                <div class="panel-body">
                                    <div class="image-div color10-bg">
                                        <img src="{{ page_image($post->page_image) }}" class="image" alt="img">
                                        <h1 class="title"><a href="{{$post->url()}}">{{ $post->title }}</a></h1>
                                    </div>
                                    <p class="text">{!! str_limit($post->content, 160) !!}<a href="{{$post->url()}}">Read
                                            More</a></p>
                                    <!-- <p class="author">
                                      <img src="img/dashboard/profileimg.png" alt="img">
                                      <span>Jonathan Doe</span>
                                      Designer
                                    </p> -->
                                </div>
                            </div>
                        </div>
                        @endif
                                <!-- End Blog Post -->

                        <!-- Start of Ad Section -->
                        <div class="col-md-6 col-lg-3">

                            <div class="panel panel-default  padding-0">
                                <img src="img/dashboard/2.jpg">
                            </div>

                        </div>
                        <!-- End of Ad Section -->

                </div>
                <!-- End Second Row -->

            </div>
            <!-- END CONTAINER -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
        </div>
        <!-- END OF MAIN TAB CONTENT -->

        <!-- START OF INBOX TAB CONTENT -->
        <div class="tab-pane" id="inbox">
            @include('panels.admin.partials.message-tab')
        </div>
        <!-- END OF INBOX TAB CONTENT -->
        <!-- START OF JOBS TAB CONTENT -->
        <div class="tab-pane" id="jobs-board">
            @include('jobs.partials.jobs-container')
        </div>
        <!-- END OF JOBS TAB CONTENT -->
        <!-- START OF JOBS TAB CONTENT -->
        <div class="tab-pane" id="jobs">
            @include('panels.vendor.partials.jobs-tab')
        </div>
        <!-- END OF JOBS TAB CONTENT -->

        <!-- START OF GALLERY TAB CONTENT -->
        <div class="tab-pane" id="gallery">
            <!-- Start Page Header -->
            <div class="page-header">
                <h1 class="title">Gallery</h1>
                <ol class="breadcrumb">
                    <li class="active">Manage your photos and galleries</li>
                    <li><a href="/gallery/list"> Manage Galleries</a></li>
                </ol>
            </div>
            <!-- End Page Header -->

            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTAINER -->
            @if ($galleries->count() > 0)
                <div class="container-widget">
                    <!-- Start First Row -->
                    <div class="row">
                        <!-- Start Files -->

                        <div class="col-md-12">
                            <div class="panel panel-widget">
                                @foreach ($galleries as $gallery)
                                    <div class="col-xs-3 col-md-3">
                                        <div class="thumbnail">
                                            <a href="{{url('gallery/view/' .$gallery->id)}}"
                                               id="thumb_name">{{$gallery->name}}</a>
                                            <a href="{{url('gallery/edit/' .$gallery->id)}}" id="gallery_edit"><i
                                                        class="fa fa-pencil"></i></a>
                                            <a id="gallery_delete" class="pull-right"
                                               href="{{url('gallery/delete/' .$gallery->id)}}"><i
                                                        class="fa fa-trash"></i></a>
                                            <span id="image_count" class="pull-right">{{$gallery->images()->count()}}
                                                Photos</span>
                                            <img id="gall_img" src="{{url($gallery->gallery_image)}}">
                                        </div>
                                    </div>

                                @endforeach
                            </div>
                        </div>

                        <!-- End Files -->

                    </div>
                    <!-- End First Row -->
                </div>
                @endif
                        <!-- END CONTAINER -->
                <!-- //////////////////////////////////////////////////////////////////////////// -->
        </div>
        <!-- END OF GALLERY TAB CONTENT -->

        <!-- START OF ADVERT TAB CONTENT -->
        <div class="tab-pane" id="advert">
            <!-- Start Page Header -->
            <div class="page-header">
                <h1 class="title">Promote your business</h1>
                <ol class="breadcrumb">
                    <li class="active">Would you like to place an Ad with us?</li>
                </ol>
            </div>
            <!-- End Page Header -->

            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTAINER -->
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12">
                    <div class="col-sm-12 flash ">
                        
                    </div>
                    <p>Thousands of consumers use Blissful every day to make purchase & booking decisions. Blissful Ads
                        showcase your business to users looking for a business like yours.

                        <br>If you would like to find out more give us your details below.</p>

                    <form id="form_center" action="/send-advert" method="post" name="send-advert">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <div class="row control-group">
                            <div class="form-group small-12 medium-12 columns">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name"
                                       value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group small-12 medium-12 columns">
                                <label for="email">Email Address</label>
                                <input type="email" class="form-control" id="email" name="email"
                                       value="{{ old('email') }}">
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group small-12 medium-12 columns">
                                <label for="phone">Phone Number</label>
                                <input type="tel" class="form-control" id="phone" name="phone"
                                       value="{{ old('phone') }}">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="form-group small-12 medium-12 columns">
                                <button id="send-advert" type="submit" class="button tiny right">Send</button>
                            </div>
                        </div>
                    </form>
                    <div class="ajax_loader"
                         style="position: absolute; left: 45%; bottom: 160px; display:none">
                        <img src="/img/rolling.svg">
                    </div>
                </div>
            </div>
            <!-- END CONTAINER -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
        </div>
        <!-- END OF ADVERT TAB CONTENT -->

        <!-- START OF ADVERT TAB CONTENT -->
        <div class="tab-pane" id="review">
            <!-- Start Page Header -->
            <div class="page-header">
                <h1 class="title">Request for a review</h1>
                <ol class="breadcrumb">
                    <li class="active">Have people review your page...</li>
                </ol>
            </div>
            <!-- End Page Header -->

            <!-- //////////////////////////////////////////////////////////////////////////// -->
            <!-- START CONTAINER -->
            <div class="row">
                <div class="col-sm-12 flash-r ">
                        
                    </div>
                <div class="col-lg-6 col-md-6 col-sm-12"> 
                    {!! Form::open(['name'=>'review-request','route' => 'review-request']) !!}
                    <h4>Request for review</h4>
                    <div class="form-group">
                        {!! Form::label('Email', 'Please Enter Their Email', ['class' => 'control-label']) !!}
                        {!! Form::email('email', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Submit', ['class' => 'button tiny right form-control', 'id'=>'review-request']) !!}
                    </div>
                    {!! Form::close() !!}
                    <div class="ajax_loader"
                         style="position: absolute; left: 47%; bottom: 60px; display:none">
                        <img src="/img/rolling.svg">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 disabled">
                    <h3>Hi,</h3>
                    <p>
                        My business relies on recommendations from my clients. I would appreciate it if you would write
                        a brief review for me on www.blissful.co.ke, the largest and most influential directory for
                        wedding & events professional in Kenya.
                    </p>
                    <p>
                        Kindly click on the link below and share your experience when you worked with me.
                    </p>
                    <p>
                        Thanks.
                    </p>
                </div>

            </div>
            <!-- END CONTAINER -->
            <!-- //////////////////////////////////////////////////////////////////////////// -->
        </div>
        <!-- END OF REVIEW TAB CONTENT -->
    </div>
    <!-- End Content -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
@endsection
@section('scripts')
    @include('panels.admin.partials.dash-scripts')
@stop