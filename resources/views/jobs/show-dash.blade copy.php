
<!-- Start Page Header -->

<div class="page-header">

    <!-- <h1 class="title">Jobs Board</h1> -->
    <!-- <ol class="breadcrumb"> -->
        <!-- @if(isset($jobs) && count($jobs)) -->
            <!-- <li class="active">You have <span class="label label-danger">{{ @$total_jobs }}</span> jobs total
            </li> -->
        <!-- @else -->
            <!-- <li class="active">No Quotation requests available at the moment</li> -->
        <!-- @endif -->
        <!-- <li class="right"> -->
            <!-- <a class="btn btn-default btn-rounded" href="{{ route('jobs.create') }}"> -->
                <!-- Request for a quote -->
            <!-- </a> -->
        <!-- </li> -->
    <!-- </ol> -->

</div>
<div class="small-12">
    @include('flash::message')
</div>
<div class="small-12">
     <span class="right">
     <a class="button tiny warning" href="{{ route('jobs.create') }}">
     Request for a quote
     </a>
     </span>
</div>
<!-- End Page Header -->
<!-- //////////////////////////////////////////////////////////////////////////// -->
@if(isset($jobs) && count($jobs))
        <!-- START CONTAINER -->
<div class="container-widget">
    <!-- Start Job Cards -->
    <div class="row">
        <div class="small-12 columns">
            <!-- Start Files -->
            <!-- <div class="panel panel-widget trans-bg"> -->
                <!-- <div class="panel-body"> -->
                  <!-- <ul class="basic-list row hidden-xs">
                      <li class="basic-li col-sm-12 top-b">
                          <span class="col-sm-2 col-sm-offset-2">Title</span>
                          <span class="col-sm-2">Budget</span>
                          <span class="col-sm-2">Date</span>
                          <span class="col-sm-2">Location</span>
                          <span class="col-sm-2"></span>
                      </li>
                    </ul> -->
                    <ul class="basic-list">
                        <?php $count = $offset; ?>
                        @foreach($jobs as $job)
                        <li class="basic-li row white_background breaker-bottom materialize">
                            <span class="col-xs-12 col-sm-2">
                                @if($job->user->last_name)
                                <span class="avi color1-bg">{{str_limit(@$job->user->first_name, 1,"" )}}{{str_limit(@$job->user->last_name, 1,"")}}</span>
                                @else
                                <span class="avi_2 color1-bg">{{str_limit(@$job->user->first_name, 1,"" )}}</span>                @endif
                            </span>
                            <span class="col-xs-12 col-sm-2">
                                <span class="visible-xs">Title: </span> {{ $job->title }}
                            </span>
                            <span class="col-xs-12 col-sm-2">
                                <span class="visible-xs">Budget: </span>KSH: {{ number_format($job->budget) }}
                            </span>
                            <span class="col-xs-12 col-sm-2">
                            <span class="visible-xs">Event Date: </span> {{ date('d, M Y',strtotime($job->event_date)) }} </br>
                                @if($job->expired)
                                    <span class="color10"> Expired </span> <br/>
                                @else
                                    <span class="color8"> <small>{{ $job->datetime_to_event }}</small></span>
                                @endif
                            </span>
                            <span class="col-xs-12 col-sm-2">
                                <span class="visible-xs">Location: </span> {{$job->event_location}}
                            </span>
                            <span class="col-xs-12 col-sm-2">
                                <a class="btn btn-default btn-rounded" href="{{ route('jobs.show', [$job->id]) }}?bootstrap=true">
                                    View Request
                                </a>
                            </span>
                        </li>
                        @endforeach
                        {!! $jobs->render() !!}
                    </ul>
                <!-- </div> -->
            <!-- </div> -->
            <!-- End Files -->
        </div>
    </div>
    <!--  End Job Cards -->
</div>
@else
    @if(Auth::user()->vendor)
    <p>No Quotation Requests available :( check again soon...</p>
    @else
    <p>Looks like you haven't created a quotation request :( Create one here... &nbsp;
        <a class="btn btn-info btn-rounded" href="{{ route('jobs.create') }}">
                Request for a quote
        </a>
    </p>
    @endif
@endif


