@extends('page.home-layout')
@section('title')
    Quotations
@endsection
@section('styles')

@endsection
@section('header-content')
    <?php
    $header_class = 'vendorpg-intro';
    ?>
@endsection
@section('content')
    <!-- @include('flash::message') -->

    <div class="small-12 medium-10 columns small-centered show-jobs-section breaker">
        <div class="small-12 columns">
            <div class="small-12 medium-7 large-6 columns small-centered">
                @include('includes.status')
            </div>
            @if(isset($job) && $job->user_has_bid || Session::has('success'))
                <div class="reveal" id="user_success" data-reveal data-close-on-click="false"
                     data-animation-in="slide-in-right" data-animation-out="slide-out-right">
                    <h2 id="modalTitle">ThankYou.</h2>
                    <p class="lead">Your proposal was successfully sent.</p>
                    <p>If you would like to follow up with a call or email, below are their contacts.</p>
                    <div class="small-12 columns">
                        <div class="small-6 medium-4 columns pimg">
                            <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{@$job->user->profile->profilePic}}"
                                 class="circle" alt="{{@$bid->user->first_name}}">
                            <span>{{@$job->user->first_name.' '.@$job->user->last_name }}</span>
                        </div>
                        <div class="small-6 medium-8 columns dets">
                            <span><i class="fi-telephone large"></i> {{@$job->user->profile->phone}}</span>
                            <span><i class="fi-mail large"></i> {{@$job->user->email}}</span>
                        </div>
                    </div>
                    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                </div>
            @endif
        </div>
        <div class="small-12 columns breaker-bottom">
        <span class="float-right">
             @if(Auth::user()->hasUserRole('vendor'))
                <a class="button tiny warning secondary_background"
                   onclick="window.location='/shop/category/blissful-credits';">
                     Buy Credits
                 </a>
            @else
                <a class="button tiny warning non-ajax" href="{{ route('jobs.create') }}">
                     Request for a quote
                 </a>
            @endif
        </span>
        </div>
        <!-- START CONTAINER -->
    @if(!Auth::user()->hasUserRole('user'))
        <!-- Start of Filter Section -->
            <div class="row">
                <div class="small-12 columns collapse breaker-bottom">
                    <h5>Filter</h5>
                    <ul>
                        @if(Auth::user()->hasUserRole('administrator'))
                            <li class="small-12 columns">
                                <div class="small-8 columns">
                                    <form target="_blank" action="/admin/jobs/reports" method="GET" class="non-ajax">
                                        <div class="small-4 columns">
                                            <select name="category">
                                                <option value="all">All</option>
                                                @foreach(@$categories as $category)
                                                    @if($category->name == $selected_category)
                                                        <option selected
                                                                value="{{$category->name}}">{{$category->name}}</option>
                                                    @else
                                                        <option value="{{$category->name}}">{{$category->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="small-4 columns left">
                                            <input type="hidden" name="CSV" value="1">
                                            <button class="tiny button button-primary" type="submit">Export to csv</button>
                                        </div>


                                    </form>
                                </div>

                                <div class="small-4 columns">
                                    <form target="_blank" action="/admin/jobs-credits/reports" method="GET"
                                          class="non-ajax">
                                        <input type="hidden" name="csv">
                                        <button class="tiny success button" type="submit ">Jobs credit report</button>
                                    </form>
                                </div>

                            </li>
                        @endif
                        <li class="small-12 columns">
                            <form action="/jobs/all" method="GET" id="job-filters">
                                <div class="small-4 columns">
                                    <label> Category
                                        <select class="job_select" name="category">
                                            <option value="all">All</option>
                                            @foreach(@$categories as $category)
                                                @if($category->name == $selected_category)
                                                    <option selected
                                                            value="{{$category->id}}">{{$category->name}}</option>
                                                @else
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </label>
                                </div>
                                <div class="small-4 columns">
                                    <label> Location
                                        <select class="job_select" name="location">
                                            @foreach(@$locations as $location)
                                                @if($location->name == $selected_location)
                                                    <option selected="selected"
                                                            value="{{$location->name}}">{{$location->name}}</option>
                                                @else
                                                    <option value="{{$location->name}}">{{$location->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </label>
                                </div>
                                <div class="small-4 columns">
                                    <label> Date
                                        <select class="job_select" name="date">
                                            <option value="asc">Ascending</option>
                                            <option selected value="desc">Descending</option>
                                        </select>
                                    </label>
                                </div>
                                <button style="display:none" id="send_filter" type="submit">Send</button>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- End of Filter Section -->
    @endif
        <!-- Adsense Section 1-->
    <div class="row">
        <div class="small-12 columns breaker">
            <div class="hide-for-small-only">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Blissful_responsive -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-2847929148248604"
                     data-ad-slot="1315972373"
                     data-ad-format="auto"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
            <div class="show-for-small-only">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- mobile_horizontal banner -->
                <ins class="adsbygoogle"
                     style="display:inline-block;width:320px;height:100px"
                     data-ad-client="ca-pub-2847929148248604"
                     data-ad-slot="2792705573"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </div>
    </div>
    @if(isset($jobs) && count($jobs))
        <!-- Start Job Cards -->
            <div class="row">
                <div class="small-12 columns collapse">
                    <ul class="jobs-list">
                        <?php $count = $offset; ?>
                        @foreach($jobs as $job)
                            <li>
                                <div class="row">
                                    <div class="small-12 medium-4 columns">
                                        <h5>@if($job->trashed() && Auth::user()->hasuserRole('administrator'))<span class="fi-skull"></span> @endif{{ $job->title }}
                                        </h5>
                                        <h6>Category<span class="visible-xs"> - </span> {{@$job->category->name}}</h6>
                                        <h6>Location<span class="visible-xs"> - </span> {{$job->event_location}}</h6>
                                        @if( Auth::user()->hasuserRole('administrator'))
                                            <div class="small-12 columns">
                                                @if(!$job->trashed())
                                                <a href="{{ route('jobs.edit', $job->id) }}">
                                                    <button type="submit" class="button button-primary">Update</button>
                                                </a>
                                                {!! Form::open([
                                                        'method'=>'DELETE',
                                                        'route' => ['jobs.destroy', $job->id],
                                                        'style' => 'display:inline'
                                                    ]) !!}
                                                    {!! Form::submit('Delete', ['class' => 'button small']) !!}
                                                {!! Form::close() !!}
                                                @else
                                                    <a href="/jobs/restore/{{$job->id}}" class="btn btn-info btn-xs">
                                                        <span class="fi-refresh"></span> Restore deleted Request
                                                    </a>
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                    <div class="small-12 medium-4 columns">
                                        <div class="job-date">
                                            <span class="jtitle">Event Date:</span>
                                            <span class="edate">{{ date('d, M Y',strtotime($job->event_date)) }}</span>
                                        </div>
                                        <div class="job-budget">
                                            <span class="jtitle">Approx Budget (Kes):</span>
                                            <span class="jbudget">{{ number_format($job->budget) }}</span>
                                        </div>
                                        @if( Auth::user()->hasuserRole('administrator'))
                                            <p><strong>{{$job->user->first_name}} | {{$job->user->email}}</strong></p>
                                            <p><strong>Number of proposals: {{$job->bids->count()}}</strong></p>
                                        @endif
                                    </div>
                                    <div class="small-12 medium-4 columns job-actions">

                                        @if( $job->is_invited(Auth::user()->id))
                                            <h6 class="green">Invited</h6>
                                        @endif
                                        @if($job->countdown)
                                            <h6 class="green">{{$job->countdown}}</h6>
                                        @endif
                                        @if($job->expired)
                                            <h6 class="red">Expired</h6>
                                        @elseif(!$job->countdown)
                                            <h6 class="red">Job has been closed</h6>
                                        @elseif($job->user_can_view_job)
                                            @if($job->user_has_bid)
                                                <h6 class="green"><span class="fi-check"></span> Proposal Sent</h6>
                                            @endif
                                            <form action="{{ route('jobs.show', [$job->id]) }}" method="get">
                                                <input type="hidden" name="_method" value="get"/>
                                                <button class="button button-primary">
                                                    View More
                                                </button>
                                            </form>
                                        @else
                                            <h5 class="red"><span class="fi-lock"></span> Top-up credits to view job
                                                details</h5>
                                        @endif

                                    </div>
                                </div>
                            </li>
                        @endforeach
                        <div class="row">
                            <div class="pagination-centered small-12 columns breaker">
                                {!! $jobs->render(Foundation::paginate($jobs)) !!}
                            </div>
                        </div>
                    </ul>
                </div>
            </div>
            <!--  End Job Cards -->
            <!-- Adsense Section 1-->
            <div class="row">
                <div class="small-12 columns breaker breaker_bottom">
                    <div class="hide-for-small-only">
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Blissful_responsive -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-2847929148248604"
                             data-ad-slot="1315972373"
                             data-ad-format="auto"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                    <div class="show-for-small-only">
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- mobile_horizontal banner -->
                        <ins class="adsbygoogle"
                             style="display:inline-block;width:320px;height:100px"
                             data-ad-client="ca-pub-2847929148248604"
                             data-ad-slot="2792705573"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                </div>
            </div>
        @else
            @if(Auth::user()->vendor)
                <p class="hm">No Quotation Requests available <span style="font-size: 1.5rem;"> &#x1f615; </span> check
                    again
                    soon...</p>
            @else
                <p class="hm">Looks like you haven't created a quotation request&nbsp; <span style="font-size: 1.5rem;">&#x1f615;</span>
                    &nbsp;Create one here... &nbsp;
                </p>
            @endif
        @endif
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

        function load_page_scripts() {
            var cc = 0;
            $('#job-filters select').click(function () {

                cc++;
                if (cc == 2) {
                    $(this).change();
                    cc = 0;
                }
            }).change(function () {
                $(this).closest('form').submit();
            });
        }


    </script>
@endsection