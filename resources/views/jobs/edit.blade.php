@extends('page.home-layout')
@section('title')
    Quotations
@endsection
@section('styles')

@endsection
@section('header-content')
    <?php
    $header_class = 'vendorpg-intro';
    ?>
@endsection
@section('content')
<div class="small-12 columns breaker">
    <a href="/jobs/all" class="button button-primary tiny"> < Back to Jobs</a>
</div>
<div class="small-12 columns">
    <h4>Edit Job </h4>
    <hr/> 
</div>

<div class="row">
    <div class="small-12 medium-7 large-6 columns small-centered">
        @include('includes.status')
    </div>
    <div class="small-12 columns">
        <div class="small-6 columns">
            @if($job->countdown)
                <h6 class="green text-center">{{$job->countdown}}</h6>
            @endif
        </div>    
    </div>
    <div class="small-12 columns">
        {!! Form::model($job, [
            'method' => 'PATCH',
            'route' => ['jobs.update', $job->id],
            'class' => 'form-horizontal'
        ]) !!}
            @if($job->in_review == 0)
            <input type="hidden" name="in_review" value="1">
            <textarea rows="10" cols="6" name="mail_content" class="tinymce-text" placeholder="write mail message here"></textarea>
            {!! Form::submit('Request for more details', ['class' => 'open-close button small']) !!}
            @else                   
            <input type="hidden" name="in_review" value="0">
            {!! Form::submit('Remove from whitelist', ['class' => 'open-close  button small']) !!}
            @endif
        {!! Form::close() !!}

    </div>
    {!! Form::model($job, [
        'method' => 'PATCH',
        'route' => ['jobs.update', $job->id],
        'class' => 'form-horizontal'
    ]) !!}

    <div class="small-12 columns {{ $errors->has('title') ? 'has-error' : ''}}">
        {!! Form::label('title', 'Title: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group small-12 columns {{ $errors->has('description') ? 'has-error' : ''}}">
        {!! Form::label('description', 'Description: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group small-12 columns">
        {!! Form::label('classification', 'Category:') !!}
        <select name="classification_id" placeholder="Category" class="step2 has-tip tip-right"
                data-options="disable_for_touch:true" , data-tooltip="location" aria-haspopup="true"
                title="Select the city or town your business is located." required
                data-parsley-required-message="Please select where you are located"
                data-parsley-trigger="change focusout">
            <optgroup label="Event Location">
                @foreach($classifications as $classification)
                    @if($classification->id == $job->category->id)
                        <option selected="selected" value="{{$classification->id}}">{{$classification->name}}</option>
                    @else
                        <option value="{{$classification->id}}">{{$classification->name}}</option>
                    @endif
                @endforeach
            </optgroup>
        </select>
    </div>
    <div class="small-12 columns">
        @if(in_array($job->category->name,['Venue','Tents and Chairs','Catering','Mobile Toilets','Event Planner']))
        <span><label for="{{$classification_data->name}}-budget">How many guests are you planning to host?</label></span>
        <select  name="guest_number" >
            <option {{($job->guest_number == '5-50' ? 'selected' : '')}} value="5-50">5-50</option>
            <option {{($job->guest_number == '51-100' ? 'selected' : '')}} value="51-100">51-100</option>
            <option {{($job->guest_number == '101-200' ? 'selected' : '')}} value="101-200">101-200</option>
            <option {{($job->guest_number == '201-400' ? 'selected' : '')}} value="201-400">201-400</option>
            <option {{($job->guest_number == '401-600' ? 'selected' : '')}} value="401-600">401-600</option>
            <option {{($job->guest_number == '601-800' ? 'selected' : '')}} value="601-800">601-800</option>
            <option {{($job->guest_number == '800' ? 'selected' : '')}} value="800">800+</option>
        </select>
        @endif
    </div>
    <div class="form-group small-12 columns {{ $errors->has('budget') ? 'has-error' : ''}}">
        {!! Form::label('budget', 'Budget: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('budget', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('budget', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group small-12 columns {{ $errors->has('event_date') ? 'has-error' : ''}}">
        {!! Form::label('event_date', 'Event Date: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('event_date', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('event_date', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group small-12 columns">
        {!! Form::label('event_location', 'Event Location:') !!}
        <input type="text" name="event_location" id="pac-input" placeholder="Type in neighbourhood, street or town" data-parsley-group="first" required data-parsley-required-message = "You need to set a location" data-parsley-trigger="change focusout" value="{{isset($job->location->address)?$job->location->address:$job->event_location}}">
    </div>
    <div class="small-12 columns">
        @if(count($classification_data->children))
        <div class="small-12 columns">
        <p>Details</p>
            @foreach($classification_data->children as $subcategory)
                @if(in_array($subcategory->id ,$job_classifications))
                <div class="small-12 columns">
                     <input name="sub_classifications[]" checked value="{{$subcategory->id}}" type="checkbox"><label for="checkbox2">{{$subcategory->name}}</label>
                </div>
                @else
                <div class="small-12 columns">
                     <input name="sub_classifications[]" value="{{$subcategory->id}}" type="checkbox"><label for="checkbox2">{{$subcategory->name}}</label>
                </div>
                @endif
            @endforeach
        
        </div>
        
        <div class="small-12 columns">
        @foreach($classification_data->children as $subcategory)
            <div class="small-12 medium-4 columns">
                @if(count($subcategory->children))
                <p>{{$subcategory->name}} Options</p>
                @foreach($subcategory->children as $child)
                    @if(in_array($child->id ,$job_classifications))
                    <div class="small-12 columns">
                       <input name="sub_classifications[]" checked value="{{$child->id}}" type="checkbox"><label for="checkbox2">{{$child->name}}</label> 
                    </div>
                    
                    @else
                    <div class="small-12 columns">
                       <input name="sub_classifications[]" value="{{$child->id}}" type="checkbox"><label for="checkbox2">{{$child->name}}</label> 
                    </div>
                    @endif 
                @endforeach
                @endif
            </div>
        @endforeach
        </div>
        @endif
    </div>
    <div class="form-group small-12 columns {{ $errors->has('credits') ? 'has-error' : ''}}">
        {!! Form::label('credits', 'Credits: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('credits', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('credits', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    
    <div class="form-group small-12 medium-6 small-centered columns">
        <button type="submit" class="small grey_background button button-primary small-12">Update</button>
    </div>
    {!! Form::close() !!}
    <div class="small-12 columns">
        <div class="small-12 medium-6 columns">
            {!! Form::model($job, [
                'method' => 'PATCH',
                'route' => ['jobs.update', $job->id],
                'class' => 'form-horizontal'
            ]) !!}
                @if($job->completed == 0)
                <input type="hidden" name="completed" value="1">
                <button type="submit" class="open-close  button small float-left">Close Job</button>
                @else                 
                <input type="hidden" name="completed" value="0">
                <button type="submit" class="open-close  button small float-left">Reopen Job</button>
                @endif
            {!! Form::close() !!}
        </div>
        <div class="small-12 medium-6 columns">
            <a class="small button float-left" href="/jobs/update/extend_expiry/{{$job->id}}"> Repost Job</a>
        </div> 
    </div>

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
</div>
@endsection
@section('scripts')
<script src="/assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    $('.open-close').click(function(event){
        event.preventDefault();
        //check if tinymce exists
        if(tinymce.editors.length > 0){
            
            //move tinymce content to textbox
            tinyMCE.triggerSave();
            var editorContent = tinyMCE.activeEditor.getContent();
        }
        
        $(this).closest('form').submit();   
    });
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
    $( document ).ready(function() {
        if(!isMobile) {
            tinymce.init({
            selector: ".tinymce-text",
            theme: "modern",
            menubar: false,
            plugins: [
                "advlist autolink lists link charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime  nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern imagetools placeholder"
            ],
            toolbar: " bold italic alignleft aligncenter alignright alignjustify bullist numlist outdent indent preview",
            relative_urls: false,
            image_advtab: true,
            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ]
        });
        }

        else{
                tinymce.init({
                    selector: ".tinymce-text",
                    theme: "modern",
                    menubar: false,
                    toolbar: false,
                    relative_urls: false,
                    image_advtab: true,
                    templates: [
                        {title: 'Test template 1', content: 'Test 1'},
                        {title: 'Test template 2', content: 'Test 2'}
                    ]
                });
            } 
    });
</script>
<script type="text/javascript">
    function initMap(){
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                input.value = "";
                console.log("Returned place contains no ");
                return;
            }

            var address = "";
            var city_name = "";
            var latlng = "";
            var latitude;
            var longitude;
            places.forEach(function(place) {
                if (!place.geometry) {
                    input.value = "";
                    console.log("Returned place contains no geometry");
                    return;
                }

                latlng = place.geometry.location;
            });
            
            //fetch address
            var geocoder = new google.maps.Geocoder;

            geocoder.geocode({'latLng': latlng}, function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                // console.log(results)
                if (results[1]) {
                 //formatted address
                address = results[0].formatted_address;
                latitude = results[0].geometry.location.lat();
                longitude = results[0].geometry.location.lng();
                //find country name
                     for (var i=0; i<results[0].address_components.length; i++) {
                    for (var b=0;b<results[0].address_components[i].types.length;b++) {

                    //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                        if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                            //this is the object you are looking for
                            city= results[0].address_components[i];
                            break;
                        }
                    }
                }
                //city data
                city_name = city.long_name;
                $('<input name="city_name" value="'+city_name+'" type="hidden"/>').insertAfter(input);
                $('<input name="latitude" value="'+latitude+'" type="hidden"/>').insertAfter(input);
                $('<input name="longitude" value="'+longitude+'" type="hidden"/>').insertAfter(input);
                input.dataset.city = city_name;
                input.dataset.lat = latitude;
                input.dataset.lng = longitude;

                } else {
                  console.log("No results found")
                }
              } else {
                console.log("Geocoder failed due to: " + status)
              }
            });
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&libraries=places&callback=initMap"></script>
@endsection