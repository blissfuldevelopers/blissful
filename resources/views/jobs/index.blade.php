@extends('page.zurb-layout')
@section('title')
    Jobs
@endsection
@section('styles')

@endsection
@section('content')

    <h1>Jobs-index <a href="{{ route('user.jobs.create') }}" class="btn btn-primary pull-right btn-sm">Send Price Estimates</a></h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>No.</th><th>Title</th><th>Description</th><th>Category</th><th>Event Location</th><th>Event date</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($jobs as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $x }}</td>
                    <td><a href="{{ url('/user/jobs', $item->id) }}">{{ $item->title }}</a></td><td>{{ $item->description }}</td><td>{{ $item->classification }}</td><td>{{ $item->event_location }}</td><td>{{ $item->event_date }}</td>
                    <td>
                        <a href="{{ route('user.jobs.edit', $item->id) }}">
                            <button type="submit" class="btn btn-primary btn-xs">Update</button>
                        </a> /
                        {!! Form::open([
                            'method'=>'DELETE',
                            'route' => ['user.jobs.destroy', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination"> {!! $jobs->render() !!} </div>
    </div>

@endsection
