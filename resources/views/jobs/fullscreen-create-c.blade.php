<link rel="stylesheet" type="text/css" href="/assets/css/fullscreenform.css">
<div class="row">
    <div class="fs-form-wrap" id="fs-form-wrap">
        <div class="fs-title hm">
            <h3>Tell us what you need</h3>
        </div>
        {!! Form::open(['route' => 'jobs.store', 'class' => 'fs-form fs-form-full breaker']) !!}

        <ol class="fs-fields">
            <li data-input-trigger>
                {!! Form::label('classification', 'I need Price Estimates for:', ['class' => 'fs-field-label fs-anim-upper']) !!}

                <select name="classification" placeholder="category" class="cs-select fs-anim-lower form-control">
                    <optgroup label="Category">
                        @foreach($classifications as $classification)
                            <option value="{{$classification->name}}">{{$classification->name}}</option>
                        @endforeach
                    </optgroup>
                </select>
            </li>
            <li>
                {!! Form::label('event_date', 'For an event that will be held on', ['class' => 'fs-field-label fs-anim-upper']) !!}

                {!! Form::text('event_date', null, ['class' => 'fs-mark fs-anim-lower datepicker', 'required' => 'required', 'placeholder'=>date('Y-m-d',strtotime('tomorrow'))]) !!}
                {!! $errors->first('event_date', '<p class="help-block">:message</p>') !!}

            </li>
             <li>
                {!! Form::label('event_location', 'My event will be held in ', ['class' => 'fs-field-label fs-anim-upper']) !!}
                <select name="event_location" placeholder="Event Location" class="cs-select form-control fs-anim-lower">
                    <optgroup label="Event Location">
                        @foreach($locations as $location)
                            <option value="{{$location->name}}">{{$location->name}}</option>
                        @endforeach
                    </optgroup>
                </select>
            </li>
            <li>
                {!! Form::label('budget', 'My budget is approximately Kes.', ['class' => 'fs-field-label fs-anim-upper']) !!}
                {!! Form::number('budget', null, ['class' => 'fs-mark fs-anim-lower', 'required' => 'required', 'placeholder'=>'1000']) !!}
                {!! $errors->first('budget', '<p class="help-block">:message</p>') !!}

            </li>
            <li>
                {!! Form::label('description', ' Write a few things about the event that the supplier should know, this may include special requests / instructions', ['class' => 'fs-field-label fs-anim-upper']) !!}
                <textarea cols="40" rows="5" class="fs-anim-lower" id="description" name="description"
                          placeholder="Short description of the project" required="required"></textarea>
                {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
            </li>


        </ol>
    
        <!-- /fs-fields -->
        <button class="fs-submit" type="submit">Create</button>
        <a href="#" onclick="window.location='/'" class="fs-submit cancel-f"> Cancel </a>
        {!! Form::close() !!}
                <!-- /fs-form -->
    </div><!-- /fs-form-wrap -->
</div>

<script>
    if (!window.jQuery) {
        // Offline JS
        document.write('<script src="http://code.jquery.com/jquery-2.1.1.min.js"><\/script>');
    }
</script>

{!! HTML::script('/app/common/js/modernizr.custom.js') !!}
{!! HTML::script('/app/common/js/classie.js') !!}
{!! HTML::script('/assets/js/fullscreenForm.js') !!}

<script>
    (function () {
        var formWrap = document.getElementById('fs-form-wrap');

        new FForm(formWrap, {
            onReview: function () {
                classie.add(document.body, 'overview'); // for demo purposes only
            }
        });
        resize();

        function resize() {
            var docHeight = $(window).height();
            $('.fs-form-wrap').height(docHeight);
        }

        $(window).resize(function () {
            resize();
        });

    })();


</script>