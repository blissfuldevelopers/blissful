
<div  class="sub-classifs">
	<p class="lead-q">What type of {{$classification_data->name}} are you looking for?</p>
	@if(count($classification_data->children))
    <!-- <span><label for="mult-classifs">What type of {{$classification_data->name}} are you looking for</label></span> -->
    <div class="small-12 columns">
        <div class="thumbnail-container">
            @foreach($classification_data->children as $subcategory)
            <div data-classification-id="{{$classification_data->id}}" data-id="{{$subcategory->id}}" class="category-thumbnail unselected" @if( !empty($subcategory->thumbnail_path)) style="background:url('{{$subcategory->thumbnail_path}}')" @endif>
                <span>{{$subcategory->name}}</span>
            </div>
            @endforeach  
        </div> 
    </div>
    <select multiple="multiple" name="levelone-{{$classification_data->id}}[]" class="category-picker" required >
        <option value=""></option>
        @foreach($classification_data->children as $subcategory)
        <option  class="classif-opt"  data-slug="{{$subcategory->id}}" value="{{$subcategory->id}}">{{$subcategory->name}}</option>
        @endforeach
    </select>
    @endif
</div>
<p class="form-error"> You need to select an option </p>
<script type="text/javascript">
    @foreach($classification_data->children as $subcategory)
        @if(count($subcategory->children))
        var cat_id_{{$subcategory->id}} = '<div data-category = "{{$subcategory->id}}" class="sub-sub-classifs classification-{{$subcategory->id}}"><p class="lead-q">What type of {{$subcategory->name}} are you looking for?</p><div class="thumbnail-container">@foreach($subcategory->children as $child)<div data-id="{{$child->id}}" class="category-thumbnail unselected" @if( !empty($child->thumbnail_path)) style="background:url({{$child->thumbnail_path}})" @endif><span>{{$child->name}}</span></div>@endforeach</div><select  multiple="multiple" name="level{{str_shuffle("ajne")}}-{{$classification_data->id}}[]" required class="row category-picker show-labels show-html "><option value=""></option> @foreach($subcategory->children as $child)<option  class="classif-opt" data-slug="{{$child->slug}}" data-img-src="{{@$child->thumbnail_path}}" value="{{$child->id}}">{{$child->name}}</option>@endforeach</select></div><p class="form-error"> You need to select an option </p>';
        @endif
    @endforeach

</script>
