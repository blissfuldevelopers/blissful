<div class="row">
    <div class="small-8 medium-10 columns">
        <h1 class="km">Jobs Board</h1>
    </div>


    <div class="small-4 medium-2 columns">
        <a class="button tiny info" href="{{ route('jobs.create') }}">
            Create Job
        </a>
    </div>

</div>
<div class="row">
    @if(isset($jobs) && count($jobs))
        <div class="small-12 columns">
            <h2 class="ns">Showing {{ @$total_jobs }} jobs</h2>
        </div>
        <div class="small-12 columns">
            <?php $count = $offset; ?>
            @foreach($jobs as $job)
                <div class="row list-element white_background">
                    <?php $count++; ?>
                    <div class=" small-1 columns">
                        <div class="row">
                            <span class="job-number">
                            <i class="fi-torso"></i>
                            </span>

                        </div>
                        <div class="row">
                            <br/>
                            <p>{{ @$job->user->first_name }} {{ @$job->user->last_name }}</p>
                        </div>
                    </div>
                    <div class="small-9 medium-5 columns">

                        <h5>
                            {{ $job->title }}

                            <small class="text-right right">
                                <i class="fi-calendar medium"></i>
                                {{ date('d, M Y',strtotime($job->event_date)) }}
                            </small>
                        </h5>
                        <div class="ns">
                            <p>{{ @$job->category->name }} - {{ @$job->event_location }} </p>
                            <p>{{ $job->description }}</p>
                        </div>
                    </div>
                    <div class="small-12 medium-2 columns ns event-details">
                        <h5>Particulars</h5>
                        @if($job->expired)
                            <span class="red"> Expired </span> <br/>
                        @else
                            <span class="light_blue"> {{ $job->datetime_to_event }}</span> <br/>
                        @endif


                    </div>
                    <div class="small-12 medium-2 columns view-job">
                        <h5>Budget</h5>

                        Kes
                        <h4>{{ number_format($job->budget) }}</h4>
                    </div>
                    <div class="small-12 medium-2 columns view-job">
                        <a class="button tiny" href="{{ route('jobs.show', [$job->id]) }}">
                            View Job
                        </a>
                    </div>
                </div>
            @endforeach


        </div>

        @if(is_object($jobs))
            <div class="small-12">
                {!! $jobs->render(Foundation::paginate($jobs)) !!}
            </div>
        @endif
    @else
        <h2>No Jobs found</h2>
    @endif
</div>


