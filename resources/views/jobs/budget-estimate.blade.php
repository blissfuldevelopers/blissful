<div class="budget-estimate-block small-12 columns">
	<label for="estimate_budget-{{$classification->id}}">
		<div class="estimate-title small-12 columns hm">
			{{$classification->name}}
		</div>
		<div class="estimate-details small-12 columns breaker">
			<div class="estimate-suggestion">Blissful's suggestion {{number_format($estimate_budget)}} kes.</div>
			<input name="estimate_budget-{{$classification->id}}" class="estimate-budget-input" required data-parsley-group = "budget-estimates" data-parsley-trigger = "change focusout"  onchange="findTotal()" data-estimate-id="{{$classification->id}}" type="number" value="{{$estimate_budget}}" min="5000" />
			<button class="change-budget" data-input-name="estimate_budget-{{$classification->id}}">Change</button>
		</div>		
	</label>
</div>