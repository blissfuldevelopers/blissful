  @extends('page.zurb-layout')
@section('title')
    Jobs
@endsection
@section('styles')

@endsection
@section('content')

    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
                <th>Title</th><th>User</th><th>Event Location</th><th>Event date</th><th>Credits</th><th>Actions</th>
            </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($jobs as $item)
                {{-- */$x++;/* --}}
                <tr>

                    <td><a href="{{ url('/user/jobs', $item->id) }}">{{ $item->title }}</a></td><td>{{ $item->user->first_name }}</td><td>{{ $item->event_location }}</td><td>{{ $item->event_date }}</td><td>{{ $item->credits }}</td>
                    <td>
                        <a href="{{ route('job.bid', $item->id) }}">
                            <button type="submit" class="btn btn-primary btn-xs">Submit Proposal</button>
                        </a>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination"> {!! $jobs->render() !!} </div>
    </div>

@endsection
