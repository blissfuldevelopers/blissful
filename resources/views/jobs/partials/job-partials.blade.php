@if(Auth::user()->hasUserRole('vendor'))
  @foreach($jobs as $job)
  <tr>
    <td class="ven">{{$job->user->first_name}} {{$job->user->last_name}} 
    <span class="cat">- {{$job->category->name}} -</span>
    <span class="loc">{{isset($job->location->address)? $job->location->address : $job->event_location}}</span></td>
    <td class="quo">
       @if( $job->is_invited(Auth::user()->id))
            <h6 class="green">Invited</h6>
        @endif
        @if($job->countdown)
            <h6 class="green">{{$job->countdown}}</h6>
        @endif
        @if($job->expired)
            <h6 class="red">Expired</h6>
        @elseif(!$job->countdown)
            <h6 class="red">Job has been closed</h6>
        @elseif($job->user_can_view_job)
            @if($job->user_has_bid)
            <h6 class="green"><span class="fi-check"></span> Proposal Sent</h6>
            @endif
            <a href="{{route('jobs.show',[$job->id])}}" class="button">View Request</a>
        @else
            <h6 class="red"><span class="fi-lock"></span> Top-up credits to view job details</h6>
        @endif
    </td>
  </tr>
  @endforeach
@else
  @foreach($bids as $bid)
    @if(isset($bid->thread->id))
    <tr>
      <td class="ven">{{$bid->user->first_name}} <span class="loc">{{isset($bid->user->location->address)? $bid->user->location->address : $bid->user->location}}</span></td>
      <td class="quo">
        <a href="{{route('messages.show',[$bid->thread->id])}}" class="button">View Quotation</a>
      </td>
    </tr>
    @endif
  @endforeach
  @if($bids->count() == 0)
  <tr>
    <td><p class="text-center">No Quotations so far</p></td>
    <td> </td>
  </tr>
  @endif
@endif

<tr class="show-for-small-only"><td class="ven text-right"><a href="/jobs/all">View all</a></td></tr>