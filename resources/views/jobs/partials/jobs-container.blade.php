{!! HTML::style('/assets/css/jobsboard-custom.css') !!}

<div class="row">
<!--   <div class="dash-stats small-12 columns collapse hide-for-small-only">
      <ul>
          <li>
              <i class="fa fa-briefcase"></i>
              <span class="stats-title">New Jobs</span>
              <h5 class="new-jobs ns">0</h5>
          </li>
          <li>
              <i class="fa fa-bitcoin"></i>
              <span class="stats-title">Credits Balance</a></span>
              <h5 class="credits-balance ns">0</h5>
          </li>
          <li>
              <i class="fa fa-bookmark-o"></i>
              <span class="stats-title">Total Jobs</span>
              <h5 class="jobs-value">100</h5>
          </li>
      </ul>
  </div> -->
  <div id="jobs-container-back-action">
    <ul class="sub-nav">
      <li class="small-3 columns">
        <a href="#back" class="small-12">
          <i class="fi-arrow-left medium"></i>
          <span class="text">Back</span>
        </a>
      </li>

      <li class=" hm small-5 columns">
        <a href="#home" class="small-12 medium-9">
<!--           <span class="small-12 grey">{{$default_jobs_route}}</span>
 -->        </a>
      </li>

      <li class="small-3 columns">
        <a href="#reload" class="">
          <i class="fi-refresh medium right"></i>
        </a>
        <span id="curr-page-name" data-url=""></span>
      </li>
    </ul>
  </div>
  <!-- start of stats section -->
  <!-- <div class="dash-stats small-12 columns collapse">
    <ul>
      <li>
        <i class="fa fa-briefcase"></i>
        <span class="stats-title">New Jobs</span>
        <h5>20</h5>
      </li>
      <li>
        <i class="fa fa-flag-o"></i>
        <span class="stats-title">Ranking this Week</span>
        <h5>120</h5>
      </li>
    </ul>
  </div>  -->   
  <!-- end of stats section -->
</div>
<!-- Start of main jobs section -->

<div class="row">
  <div class="small-12 columns" id="jobs-container-data" data-url="{{ @$default_jobs_route }}" 
  data-requested-url="{{ @$requested_jobs_route }}">
  </div>
</div>