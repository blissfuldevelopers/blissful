<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Blissful | Introduction to Jobs</title>
  <link rel="icon" type="image/png" href="/img/dashboard/favicon.ico"/>
  <link rel="stylesheet" type="text/css" href="/assets/css/superslides.css?<?php echo date('l jS \of F Y h:i:s A'); ?>">
</head>
<body>
  <div id="slides">
    <ul class="slides-container">
      <li>
        <img src="/img/plain-white-background.png" alt="">
        <div class="container">
          <h1>
            <img class="preserve" src="/img/blissful-logo.png">
          </h1>
          <p>
            The Smarter way to find suppliers and request quotes
          </p>
          <div class="section group">
            <div class="col span_1_of_3">
              <div class="job-intro-icon icon-camera"></div>
            </div>
            <div class="col span_1_of_3 ">
                <div class="job-intro-icon icon-chair"></div>
            </div>
            <div class="col span_1_of_3">
              <div class="job-intro-icon icon-tent"></div>
            </div>
          </div>
          <p>
            Get <strong>4</strong> quotes in <strong>48</strong> hours.
          </p>
        </div>
      </li>

      <li>
        <img src="/img/plain-white-background.png" alt="">
        <div class="container">
          <h1>
            <div class='job-intro-icon icon-edit-2'></div>
          </h1>
          <p>
            Just tell us what you need and when.
          </p>
          <div class="detail-area">
            <div class="dets">Venue</div>
            <div class="dets">Date</div>
            <div class="dets">Location</div>
          </div>
        </div>
      </li>
      <li>
        <img src="/img/plain-white-background.png" alt="">
        <div class="container">
          <h1>
            <div class='job-intro-icon icon-edit'></div>
          </h1>
          <p>
            Describe what you need in <strong>Detail.</strong>
          </p>
          <div class="detail-area small-no-bg">
            <img class="preserve" src="/img/job-description.png">
          </div>
        </div>
      </li>
<!--       <li>
        <img src="/img/plain-white-background.png" alt="">
        <div class="container">
          <h1>
            <div class='job-intro-icon icon-chat'></div>
          </h1>
          <p>
            Talk to vendors who will send you quotes.
          </p>
          <div class="section group chat">
            <div class="col span_3_of_3 ">
                <div class="left job-intro-icon icon-user"></div>
                <div class="right chat-area breaker-bottom ">
                   <p>Hi,</p>
                  <p>I am looking for a wedding planner to help plan for my wedding to be held on the 5th of December.</p>
                </div>
            </div>
            <div class="col span_3_of_3 desktop-only">
                <div class="left chat-area">
                <p>Good afternoon,</p>
                <p> Congratulations on your upcoming nuptials. We have a package that we believe will be the best offer you can get. </p>
                </div>
                <div class="right job-intro-icon icon-user"></div>
            </div>
          </div>
        </div>
      </li> -->
<!--       <li>
        <img src="/img/plain-white-background.png" alt="">
        <div class="container">
          <h1>
            <div class='job-intro-icon icon-rating'></div>
          </h1>
          <p>
            Rate to help others pick the best
          </p>
          <div class="section group chat">
            <div class="section-rating breaker-bottom small-no-bg">
              <img class="preserve" src="/img/rating-example.png">
            </div>
            <div class="stars-area ">
              <div class="star-container">
                <div class="star job-intro-icon icon-selected-star"></div>
              </div>
              <div class="star-container">
                <div class="star job-intro-icon icon-selected-star"></div>
              </div>
              <div class="star-container">
                <div class="star job-intro-icon icon-selected-star"></div>
              </div>
              <div class="star-container">
                <div class="star job-intro-icon icon-selected-star"></div>
              </div>
              <div class="star-container">
                <div class="star job-intro-icon icon-empty-star"></div>
              </div>
            </div>
          </div>
        </div>
      </li> -->
    </ul>
    <nav class="desktop-navigation">
      <a href="#" class="desktop-only desktop-next next">&#10095;</a>
      <span class="desktop-only get-started" style="display:none;" onclick="window.location.href='/jobs/create'">Get Started</span>
    </nav>
    <nav class="slides-navigation">
      <a href="#" class="next"> <i>Next</i> &#10095;</a>
      <a href="#" class="prev">&#10094; <i>Prev                                  </i> </a>
      <span class="get-started" style="display:none;" onclick="window.location.href='/jobs/create'">Get Started</span>
    </nav>
  </div>
  <script type="text/javascript" src="/js/jquery-2.2.1.min.js"></script>
  <script type="text/javascript" src="/js/jquery.superslides.min.js"></script>
  <script type="text/javascript">
    $('#slides').superslides();

    $('.next').click( function(event){
      var slide = $('#slides').superslides('current');
      
      if (slide == 0) {
        $('.slides-navigation')
        .css('background-color', '')
        .css('background-color', '#00cbb0');
      }
      if (slide == 1) {
        $('.slides-navigation')
        .css('background-color', '')
        .css('background-color', '#fac500');
        $('.next').hide();
        $('.slides-navigation > .get-started').show();
        if (  $(window).width() > 739) {
           $('.desktop-navigation > .get-started').show();
        }
      }
      if (slide == 2) {
        $('.slides-navigation')
        .css('background-color', '')
        .css('background-color', '#6dc336');
       
      }
      if (slide == 3) {
        $('.slides-navigation')
        .css('background-color', '')
        .css('background-color', '#01c4e3');
        $('.next').hide();
        $('.slides-navigation > .get-started').show();
        if (  $(window).width() > 739) {
           $('.desktop-navigation > .get-started').show();
        }
       
      }
      if (slide == 4) {
        $('.slides-navigation')
        .css('background-color', '')
        .css('background-color', '#ee3149');
      }
      $('.prev').show();
      $('#slides').superslides('animate', 'next');
    });
    $('.prev').click( function(event){
      var slide = $('#slides').superslides('current');
      console.log(slide);
      if (slide == 0) {
        $('.slides-navigation')
        .css('background-color', '')
        .css('background-color', '#fac500');
      }
      if (slide == 1) {
        $('.slides-navigation')
        .css('background-color', '')
        .css('background-color', '#00cbb0');
       
      }
      if (slide == 2) {
        $('.slides-navigation')
        .css('background-color', '')
        .css('background-color', '#6dc336');
       
      }
      if (slide == 3) {
        $('.slides-navigation')
        .css('background-color', '')
        .css('background-color', '#01c4e3');
      }
      if (slide == 4) {
        $('.slides-navigation')
        .css('background-color', '')
        .css('background-color', '#ee3149');
      }
    });
    
  </script>
  <script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-50669449-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>