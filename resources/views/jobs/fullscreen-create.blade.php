<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Request for Quotation | Blissful</title>
    <link rel="icon" type="image/png" href="https://s3.eu-central-1.amazonaws.com/blissful-ke/img/dashboard/favicon.ico">
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/home-scripts.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/bliss-custom.css') }}">
  </head>
  <body>
    <!-- masthead -->
    <div class="jobpg-intro">
        <!-- main navigation -->
        <nav>
          <div class="row">
            <div class="medium-12 columns">
              <div class="small-6 medium-4 columns">
                <!-- logo -->
                <h1 class="logo">
                    <img src="/img/blissful-logo-w.svg">
                </h1>
              </div>
              <div class="small-6 medium-3 columns">
                <!-- nav links -->
                @if(Auth::check())
                <ul class="dropdown menu" data-dropdown-menu>
                  <li class="header-dropdown">
                    <a>
                      <img class="round" src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{Auth::user()->profile->profilePic}}"> {{Auth::user()->first_name}}
                    </a>
                  </li>
                </ul>
                @endif
              </div>  
            </div>
          </div>
        </nav>
        
    </div>
    <!-- main content  -->
    <div class="jobpg-main">
        <div class="row">
          <div class="small-12 columns">
            <div class="wrapper">
              <h1>Request for Quotation</h1>
              <div class="small-12 columns">
                <div class="small-12 columns text-right">
                  Step
                </div>
                <div class="small-12 columns text-right">
                  <span class="current-step"></span> of <span class="total-steps"></span>
                </div>
                <div class="small-12 columns steps-column float-left"></div>
              </div>
              <form class="jobs-form" action="{{route('jobs.store')}}" method="POST">          
                <div class="small-12 columns questions-container">
                  <div class="questions current">
                    <div class="small-12 columns">
                      <p class="lead-q">What service are you looking for?</p>
                      <div class="option-container">
                        @foreach($classifications as $classification)
                        <div class="option-select" data-name="{{$classification->name}}" data-id="{{$classification->id}}">
                          <span>
                            &#x029BE;
                          </span>
                          <div class="option-dets">
                            {{$classification->name}}
                          </div>
                        </div>
                        @endforeach
                        <select name="classification[]" class="classification-options" multiple required>
                          <option value=""></option>
                          @foreach($classifications as $classification)
                          <option id="option-{{$classification->id}}" value="{{$classification->slug}}">{{$classification->name}}</option>
                          @endforeach
                        </select>
                      </div>
                      <p class="form-error"> You need to select an option </p>
                    </div>
                  </div>
                  <div class="questions">
                    <p class="lead-q">Please give us details on your Event</p>
                    <div class="small-12 medium-6 small-centered columns">
                      <label>
                        Event Date
                        {!! Form::text('event_date', null, ['class' => 'dat','required', 'data-live-validate'])!!}
                      </label>         
                      <span class="form-error"> Please input the event date </span>
                    </div>
                    <div class="small-12 medium-6 small-centered columns">
                      <label>
                        Event Location
                        <input type="text" name="event_location" id="pac-input" placeholder="Type in neighbourhood, street or town" required  data-live-validate>
                      </label>  
                      <span class="form-error"> Please input the event location </span>
                    </div>
                    <div class="small-12 medium-6 columns small-centered guest-section">

                    </div>
                              
                  </div>
                  @if(!Auth::check())
                  <div class="questions">
                    <div class="small-12 columns">
                      <p class="lead-q">Please input your details below.</p>
                      <div class="small-12 medium-6 small-centered columns">
                        <label>
                          Name
                          <input type="text" name="first_name" placeholder="Name" data-live-validate>
                        </label>
                        <span class="form-error"> Please input Your name </span>
                      </div>
                      <div class="small-12 medium-6 small-centered columns">
                        <label>
                          Email
                          <input type="email" name="email" placeholder="Email" data-live-validate>
                        </label>
                        <span class="form-error"> Please input a valid input </span>
                      </div>
                      <div class="small-12 medium-6 small-centered columns">
                        <label>
                          Phone Number
                          <input type="number" name="phone" placeholder="Phone Number" data-live-validate>
                        </label>
                        <span class="form-error"> Please input a valid phone number </span>
                      </div>
                    </div>
                  </div>
                  @endif
                </div>
                <div class="small-12 columns">
                  <a class="prev-q button button-primary"><span>&#x02039;</span> &nbsp; Back</a>
                  <a class="next-q button button-primary">Next &nbsp; <span>&#x0203A;</span></a>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
    <!--Start of Crisp Live Chat Script-->
    <script type="text/javascript">
            
      CRISP_WEBSITE_ID = "aeb94ef1-11bf-4618-85b7-dce7f0ee2b92";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.im/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();
    </script>

    <!--End of Crisp Live Chat Script-->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-50669449-1', 'auto');
        ga('send', 'pageview');

    </script>
    <script type="text/javascript">
        adroll_adv_id = "TH4Q5IVG5FHXNNPGFG2JBA";
        adroll_pix_id = "342MRCCGYRABZFOVG5SIPM";
        /* OPTIONAL: provide email to improve user identification */
        @if(Auth::check())
                adroll_email = "{{Auth::user()->email}}";
        @endif
        (function () {
            var _onload = function(){
                if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
                if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
                var scr = document.createElement("script");
                var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
                scr.setAttribute('async', 'true');
                scr.type = "text/javascript";
                scr.src = host + "/j/roundtrip.js";
                ((document.getElementsByTagName('head') || [null])[0] ||
                    document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
            };
            if (window.addEventListener) {window.addEventListener('load', _onload, false);}
            else {window.attachEvent('onload', _onload)}
        }());
    </script>
    <script defer src="{{ elixir('js/home-scripts.js') }}" onLoad="start_scripts()"></script>
    <script type="text/javascript">
    var selected = [];
    var ajax_status = 1;
      function start_scripts() {
        count_steps();
        $.urlParam = function(name){
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results==null){
               return null;
            }
            else{
               return results[1] || 0;
            }
        }
        var option = $.urlParam('option');
        if (option) {
          var el = $('div[data-id="'+option+'"]');
          
          $('.option-container').scrollTop(el.position().top);
          add_section(el);
          
        }
        $('#pac-input').focusout(function(e){
            e.preventDefault();

            if (!$(this).attr('data-city')) {
                $(this).val('');
                $(this).attr("placeholder", "Please select a location from the suggestions");
            }
            else{
              var lat = $(this).attr('data-lat');
              var lng = $(this).attr('data-lng'); 
              var city = $(this).attr('data-city'); 
              $('<input type="hidden" name="city_name" value="'+city+'"> <input type="hidden" name="lat" value="'+lat+'"> <input type="hidden" name="lng" value="'+lng+'">').insertAfter('#pac-input');
            }
        });

        $('.next-q').click(function(e){
          e.preventDefault();
          var current = $('.current');
          var next = current.next();
          var height = next.height()+15;
          var validity = [];

          $('.current :input').each(function(index,data) {

            var value = $(this).val();
            $(this).parent().siblings('.form-error').hide();
            if ($(this).hasClass('capacity-input')) {
              
              value = "111";
            }
            if ($(this).attr('type') == 'email') {
              
              if (!validateEmail(value)) {
                validity.push(1);
                $(this).parent().siblings('.form-error').show();
              }

            }
            if ($(this).attr('name') == 'phone') {
              if (value.length < 8) {
                validity.push(1);
                $(this).parent().siblings('.form-error').show();
              }
            }
            if (value == "" || value === undefined || value === null) {

              validity.push(1);
              $(this).parent().siblings('.form-error').show();
            }
             
          });
          
          needle = 1,
          index = contains.call(validity, needle); // true

          if (index || current.find('.section-loader').length > 0) {
            return false;
          }

          

          if (next.hasClass('questions')) {
            current.removeClass('current');
            next.addClass('current');         
            $('.questions-container').height(height);
            count_steps();
          }
          else{
            $('form.jobs-form').submit();
          }
          
        });
        $('.prev-q').click(function(e){
          e.preventDefault();
          var current = $('.current');
          var prev = current.prev();
          var height = prev.height()+15;
          if (prev.hasClass('questions')) {
            current.removeClass('current');
            prev.addClass('current');
            $('.questions-container').height(height);
            count_steps();
          }
          
        });
        $('.option-select').click(function(e){

          e.preventDefault();
          var el = $(this);
          add_section(el);
        });

        $(document).on('click', '.category-thumbnail', function(e){

          e.preventDefault();
          var thumb = $(this);
          var id = thumb.attr('data-id');
          var parent_id = thumb.attr('data-classification-id');
          var parent_class = 'panel-number-'+parent_id;
          var current_class = 'child-question-'+id;

          var panel = '<div class="questions '+current_class+'"> '+window['cat_id_'+id]+'</div>';
          
          if (thumb.hasClass('unselected')) {

            thumb.removeClass('unselected');
            $('option[value="'+id+'"]').attr('selected','selected');

            if (window['cat_id_'+id]) {
                $(panel).insertAfter('.'+parent_class);
            }
            
          }
          else{
            thumb.addClass('unselected');

            $('option[value="'+id+'"]').removeAttr('selected');
            if (window['cat_id_'+id]) {
                $('.'+current_class).remove();
            }
          }
          count_steps();
        });

       $(document).on('moved.zf.slider', '.slider', function(e){ 
            var min = $('.slider-min').attr('aria-valuenow');
            var max = $('.slider-max').attr('aria-valuenow');
            var cap = min+'-'+max;
            var formatted_min = addCommas(min);
            var formatted_max = addCommas(max);
          
            $('.min-capacity').val(formatted_min);
            $('.max-capacity').val(formatted_max);
            $('#guest-number').val(cap);

            
        });

        $('.dat').datePicker({
            createButton: false,
            clickInput: true
        });

      }
      function addCommas(nStr){
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
          x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
      }
      function count_steps(){
        var questions = $('.questions').length;
        var current = $('.current');
        var index = $( ".questions" ).index(current);
        var current_step = index + 1;

        $('.current-step').text(current_step);
        $('.total-steps').text(questions);

        var percentage = (current_step/questions) * 100; 
        $(".steps-column").animate({
          width: percentage+"%"
        }, 1500 );
      }
      function fetch_child(){
        console.log(selected)
        if (selected[0] != null && ajax_status == 1) {
            ajax_status = 0;
            var current = selected[0];
            var current_class = 'panel-number-'+current;
            $.ajax({
                    type: 'get',
                    url: '/jobs/child-class',
                    data: 'option='+current,
                    success: function(data) {
                        var next = $("."+current_class).html(data);
                        var height = next.height()+15;
                        if (next.hasClass('current')) {
                            $('.questions-container').height(height);
                        }
                        
                        ajax_status = 1;
                        selected.shift();
                        fetch_child();
                    },
                    error: function(){
                      console.log('error')
                    }
                });
        }
      }
      function add_section(el) {
        
          var id = el.attr('data-id');
          var container = $('.option-container');
          var current_class = 'panel-number-'+id;
          var last_section_class = 'last-section-'+id;
          var classification = el.attr('data-name');
          var class_arrays = ["Event Set Up","Catering","Transport"];
          var panel = '<div class="questions child-class '+current_class+'"> <div class="section-loader"><div></div>';
          var guest_section = '<div class="small-12"><label>Number of guests</label></div><div class="slider" data-slider data-initial-start="50" data-initial-end="2500" data-options="start: 25;end:3000;"><span class="slider-handle slider-min" data-slider-handle role="slider" tabindex="1"></span><span class="slider-fill" data-slider-fill></span><span class="slider-handle slider-max" data-slider-handle role="slider" tabindex="1"></span><input name="min_capacity" class="capacity-input min-capacity" value=""><input name="max_capacity" class="capacity-input max-capacity" value=""><input type="hidden" id="guest-number" name="guest_number"></div>';

          var last_section = '<div class="questions child-class '+last_section_class+'" data-cat-last="'+id+'"> <p class="lead-q">Additional info</p> <div class="small-12 medium-6 small-centered columns"><label>What\'s Your budget for '+classification+'<input type="number" name="budget-'+id+'" placeholder="Input budget"></label><span class="form-error">Please set your budget</span></div><div class="small-12 medium-6 small-centered columns"><label>Please give a brief description of the service you\'d like <textarea rows="4" name="description-'+id+'"></textarea></label><span class="form-error">Please give a brief description</span></div></div>';

          var index = contains.call(class_arrays, classification); // true

          if (index && $('.slider').length == 0) {
            $('.guest-section').append(guest_section);
            var element = $('.slider');
            var options = {
              start:25,
              end:3000
            };
            var elem = new Foundation.Slider(element,options);
          }
          if (classification == "Venue") {
            $('.next-q').hide();
            window.location = "/venue";
          }

          if (el.hasClass('selected')) {

              el.removeClass('selected');
              el.find('span').html('&#x029BE;');              
              $("#option-"+id).removeAttr('selected');

              //remove from array of selected
              var index = selected.indexOf(id);
              if (index > -1) {
                  selected.splice(index, 1);
              }
              //remove slide
              $('.'+current_class).remove();
              $('.'+last_section_class).remove();


          }
          else{
            selected.push(id);
            el.addClass('selected');
            el.find('span').html('&checkmark;');
            $("#option-"+id).attr('selected','selected');

            var moveby = el.outerHeight(true);
            
            container.stop().animate({
                  'scrollTop': container.scrollTop()+moveby
                  }, 1300, 'swing');

            //check if any child classes have been added
            if (selected.length > 1) {
              $(panel).insertAfter('.questions-container div.child-class:last');
              $(last_section).insertAfter('.'+current_class);
            }
            else{
              $(panel).insertAfter('.questions-container div.questions:first');
              $(last_section).insertAfter('.'+current_class);
            }
            fetch_child();
          }
          count_steps();
      }

      function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
      } 

    var contains = function(needle) {
        // Per spec, the way to identify NaN is that it is not equal to itself
        var findNaN = needle !== needle;
        var indexOf;

        if(!findNaN && typeof Array.prototype.indexOf === 'function') {
            indexOf = Array.prototype.indexOf;
        } else {
            indexOf = function(needle) {
                var i = -1, index = -1;

                for(i = 0; i < this.length; i++) {
                    var item = this[i];

                    if((findNaN && item !== item) || item === needle) {
                        index = i;
                        break;
                    }
                }

                return index;
            };
        }

        return indexOf.call(this, needle) > -1;
    };
    </script>
    <script type="text/javascript">
        function initMap(){
            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    input.value = "";
                    console.log("Returned place contains no ");
                    return;
                }

                var address = "";
                var city_name = "";
                var latlng = "";
                var latitude;
                var longitude;
                places.forEach(function(place) {
                    if (!place.geometry) {
                        input.value = "";
                        console.log("Returned place contains no geometry");
                        return;
                    }

                    latlng = place.geometry.location;
                });
                
                //fetch address
                var geocoder = new google.maps.Geocoder;

                geocoder.geocode({'latLng': latlng}, function(results, status) {
                  if (status == google.maps.GeocoderStatus.OK) {
                    // console.log(results)
                    if (results[1]) {
                     //formatted address
                    address = results[0].formatted_address;
                    latitude = results[0].geometry.location.lat();
                    longitude = results[0].geometry.location.lng();
                    //find country name
                         for (var i=0; i<results[0].address_components.length; i++) {
                        for (var b=0;b<results[0].address_components[i].types.length;b++) {

                        //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                            if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                                //this is the object you are looking for
                                city= results[0].address_components[i];
                                break;
                            }
                        }
                    }
                    //city data
                    city_name = city.long_name;
                    input.dataset.city = city_name;
                    input.dataset.lat = latitude;
                    input.dataset.lng = longitude;

                    } else {
                      console.log("No results found")
                    }
                  } else {
                    console.log("Geocoder failed due to: " + status)
                  }
                  console.log(city_name)
                });
            });
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&libraries=places&callback=initMap"></script>

  </body>
</html>
