<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
        <meta name="title" content="Request for Quotation"/>
        <meta name="description" content="Get Price Estimates from multiple event service providers. Find event service providers in Kenya" >
		<title>Request for Quotation | Blissful</title>
		<link rel="icon" type="image/png" href="/img/dashboard/favicon.ico"/>
		<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" type="text/css" href="/assets/css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="/assets/css/job-create.css?v=fknfnuykf" />
		<link rel="stylesheet" type="text/css" href="/assets/css/component.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/widearea.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/bid-form.css">
		<script src="/assets/js/modernizr.custom.js"></script>
        <link rel="stylesheet" type="text/css" href="{{ elixir('css/image-picker.css') }}">
	</head>
	<body>
        <div style="display:none;" class="loading loading--fixed loading--double"></div>
		<div class="container job-container">
			<section class="job-section">
                @if(Session::has('jobcreated'))
                <div style="text-align:center; width:100%; height:100%;">
                   <span>ThankYou!</span>
                        <div class="row">
                            <p>
                                We are matching suitable suppliers for you.</br>
                                We will get in touch with you in the next 48 hours.
                            </p>
                        </div>
                        <div class="nav-wrap" >
                            <a href="#" class=" action-button" onclick="ga('send', 'event', 'button', 'click', 'jobs-close');" >Close &nbsp; &times;</a>
                        </div>
                </div>
                @else
				<form id="theForm" class="simform" autocomplete="off" method="POST" action="{{ route('jobs.store') }}">
					<div class="simform-inner">
						<ol class="questions">
							<li class="start-slide start-slide--visible">
                                <div class="budget-enquiry">
                                    <div class="budget-inner">
                                        <p>Would you like us to suggest budgets for you?</p>
                                        <div class="budget-buttons">
                                            <button class="budget-yes"><span>Yes</span></button>
                                            <button class="budget-no"> <span>No</span></button>
                                        </div>
                                    </div>
                                </div>
                                <h1>You can use blissful to source for quotes from multiple suppliers, for free.</h1>
								<span><label for="q1">Let's Start you off..</label></span>
                                <div class="simform-dets">
                                    <div class="row">
                                        <div class="desc">
                                            <span>What type of event are you planning for?</span>
                                        </div>
                                        <div class="det">
                                            <span>
                                                <input name="event_name" required data-parsley-group="first" class="pass" type="text" placeholder="eg. Wedding, Birthday" data-parsley-required-message="please include an event name"/>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="desc">
                                            <span>When will your event be held? </span>
                                        </div>
                                        <div class="det">
                                            <span>
                                                {!! Form::text('event_date', null,
                                                    [
                                                    'class' => 'getdate pass',
                                                    'tooltipText'=>'Click to choose the date on which your event is to be held',
                                                    'id'=>'event_date',
                                                    'required' => 'required',
                                                    'placeholder'=>'YYYY/MM/DD',
                                                    'data-format'=>'YYYY-MM-DD',
                                                    'data-template'=>'DD MMM YYYY',
                                                    'data-parsley-required-message' => 'You need to include an event date',
                                                    'data-parsley-group' =>'first',
                                                    'data-parsley-trigger' =>'change focusout'
                                                    ])
                                                !!}
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="desc">
                                            <span>Where will your event be held? </span>
                                        </div>
                                        <div class="det">
                                            <span>
                                            <input type="text" name="event_location" id="pac-input" placeholder="Type in neighbourhood, street or town" data-parsley-group="first" required data-parsley-required-message = "You need to set a location" data-parsley-trigger="change focusout">
                                                
                                            </span>
                                        </div>
                                    </div>
                                </div>
							<div class="row">

                                <div class="classification">
                                    <div class="classification-desc">
                                        <p class="multiple-class-label">Click to choose what you'd like Price Estimates for </p>
                                    </div>
                                    <select multiple="multiple" name="classification[]" id="mult-classifs" class="row image-picker show-labels show-html pass" data-parsley-group="first" required data-parsley-required-message = "You need to select an option">
                                        @foreach($classifications as $classification)
                                        @if(is_object($selected) && $selected->id == $classification->id)
                                        <option selected class="classif-opt" data-slug="{{$classification->slug}}" data-img-src="{{$classification->thumbnail_path}}" value="{{$classification->slug}}" >{{$classification->name}}</option>

                                        @else
                                        <option  class="classif-opt" data-slug="{{$classification->slug}}" data-img-src="{{$classification->thumbnail_path}}" value="{{$classification->slug}}">{{$classification->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

							</li>
							<li>

                            </li>
                            <li class="planning-panel">
                                <div class="planning-slide">
                                    <p>Would you like us to assist you in your planning process?</p>
                                    <div class="row">
                                        <div class="budget-buttons">
                                            <button class="planning-yes"><span>Yes</span></button>
                                            <button class="planning-no"> <span>No</span></button>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            @if(!Auth::check() || empty(Auth::user()->profile->phone))
                            <li> 
                                <div class="iframe-container">
                                    
                                </div>
                                <p>Tell us a bit about yourself.</p>
                                    <div class="info-container">      
                                        @if(!Auth::check())
                                       
                                        <div class="info-container-divider">
                                            OR
                                        </div>
                                        <div class="info-socials">
                                            <p>We know typing can be a bit boring sometimes :)</p>
                                            <a href="{{ route('social.redirect', ['provider' => 'facebook']) }}" data-title="Login with Facebook" class="facebook-signin social-signin" >
                                                Sign in with Facebook
                                            </a>
                                            <a href="{{ route('social.redirect', ['provider' => 'google']) }}" class="google-signin social-signin">
                                                Sign in with Google 
                                            </a>
                                        </div>
                                        <div class="info-form">
                                            <p>Fill your details below</p>
                                            <span><label for="first_name">My Name is</label></span>
                                            <div class="name-text">
                                                {!! Form::text('first_name', null,
                                                [
                                                'class'     =>  'pass',
                                                'required' => 'required',
                                                'placeholder'=>'First Name',
                                                'data-parsley-required-message' => 'Your first name is required',
                                                'data-parsley-trigger'          => 'change focusout',
                                                'data-parsley-pattern'          => '/^[a-zA-Z]*$/',
                                                'data-parsley-minlength'        => '2',
                                                'data-parsley-maxlength'        => '32',
                                                'data-parsley-group'            => 'second'

                                                ]
                                                ) !!}
                                                {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}

                                                {!! Form::text('last_name', null,
                                                [
                                                'class'     =>'pass',
                                                'required' => 'required',
                                                'placeholder'=>'Last Name',
                                                'data-parsley-required-message' => 'Your last name is required',
                                                'data-parsley-trigger'          => 'change focusout',
                                                'data-parsley-pattern'          => '/^[a-zA-Z]*$/',
                                                'data-parsley-minlength'        => '2',
                                                'data-parsley-maxlength'        => '32',
                                                'data-parsley-group'            =>'second',
                                                ]) !!}
                                                {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
                                            </div>
                                            <span><label for="phone">You can reach me on.</label></span>
                                                {!! Form::tel('phone', null,
                                                [
                                                'class' => 'pass',
                                                'required' => 'required',
                                                'placeholder'=>'Phone Number',
                                                'data-parsley-required-message' => 'Please input your phone number',
                                                'data-parsley-minlength'        => '6',
                                                'data-parsley-minlength-message' => 'Phone number invalid, please retype',
                                                'data-parsley-trigger'          => 'focusout',
                                                'data-parsley-group' =>'second'
                                                 ]) !!}
                                                {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                                            <span><label for="email">I would like quotes to be sent to</label></span>

                                                {!! Form::email('email', null,
                                                [
                                                'class'=> 'pass',
                                                'required' => 'required',
                                                 'placeholder'=>'Email Address',
                                                 'data-parsley-required-message' => 'Your contact is required',
                                                'data-parsley-trigger'          => 'change focusout',
                                                'data-parsley-type'             => 'email',
                                                'data-parsley-group'            =>'second'
                                                 ]) !!}
                                                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                            <span><label for="email">Set password for account</label></span>
                                                {!! Form::password('password', [
                                                        'class' => 'form-password', 
                                                        'placeholder' => 'Password', 
                                                        'required',  
                                                        'id' => 'inputPassword',
                                                        'data-parsley-required-message' => 'Password is required',
                                                        'data-parsley-trigger'          => 'change focusout',
                                                        'data-parsley-minlength'        => '6',
                                                        'data-parsley-maxlength'        => '20'
                                                        ]) !!}


                                                {!! Form::password('password_confirmation', [
                                                'class' => 'form-password', 
                                                'placeholder' => 'Confirm Password',
                                                'required',  
                                                'id' => 'inputPasswordConfirmation',
                                                'data-parsley-trigger'          => 'change focusout',
                                                'data-parsley-equalto'          => '#inputPassword',
                                                'data-parsley-equalto-message'  => 'Passwords do not match',
                                                 ]) !!}
                                        </div>
                                        
                                        @elseif(empty(Auth::user()->profile->phone))
                                        <span><label for="phone">You can reach me on</label></span>
                                                {!! Form::tel('phone', null,
                                                [
                                                'class' => 'pass',
                                                'required' => 'required',
                                                'placeholder'=>'Phone Number',
                                                'data-parsley-type'=>'number',
                                                'data-parsley-pattern'=>'^[\d\+\-\.\(\)\/\s]*$',
                                                'data-parsley-required-message' => 'Please input your phone number',
                                                'data-parsley-pattern-message' => 'Phone number invalid, please retype',
                                                'data-parsley-trigger'          => 'change focusout',
                                                'data-parsley-group'        =>'second'
                                                 ]) !!}
                                                {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}

                                        @endif
                                    </div>
                                @endif
                            </li>
						</ol><!-- /questions -->
					</div><!-- /simform-inner -->
                    <button class="submit" type="submit">Post</button>
                    <div class="controls">
                        <button class="prev">Back </button>
                        <button id="first-click" class="next">Next </button>
                        <div class="progress"></div>
                        <span class="error-message"></span>
                    </div><!-- / controls -->
                    <span class="number">
                                <span class="number-current"></span>
                                <span class="number-total"></span>
                    </span>
					<span class="final-message">
                    </span>
				</form><!-- /simform -->
                @endif
			</section>
		</div><!-- /container -->
        <script type="text/javascript" src="/js/jquery-2.2.1.min.js"></script>
        <script type="text/javascript" src="/assets/plugins/parsley.min.js"></script>
		<script src="/assets/js/classie.js"></script>
		<script src="/assets/js/stepsForm.js?f34=rd5tqwd443"></script>
        <script type="text/javascript" src="{{ elixir('js/image-picker.js') }}"></script>
        <script type="text/javascript" src="/assets/js/moment.min.js"></script>
        <script type="text/javascript" src="/assets/js/combodate.js"></script>
        <script type="text/javascript" src="/assets/js/widearea.min.js"></script>
        @if(is_object($selected))
            <script type="text/javascript">

                var current = '{{$selected->name}}';
                var current_class = current.replace(/[^A-Z0-9]+/ig, "_")+"  first-subclass child-class";
                $.ajax({
                            type: 'get',
                            url: '/jobs/child-class',
                            data: 'option='+current,
                            success: function(data) {
                                $( ".questions >li:first-child").next('li').addClass(current_class).html(data);
                                ajax_status = 1;
                            },
                            error: function(){
                                $("."+current_class > ".no-load").show();
                                $('.loading').hide();
                            }
                        });
            </script>
        @endif
		<script type="text/javascript">
            //initialize interactiveness
            function startStep(){

                var theForm = document.getElementById( 'theForm' );
                new stepsForm( theForm);

            }
            $( document ).ready(function() {

                    startStep();
                    if ($("body").height() > $(window).height()) {
                            $(window).scroll(function() {
                                if ($('body').height() <= ($(window).height() + $(window).scrollTop())) {
                                     $('.controls').show();
                                }
                            });
                    }
                    else{
                            $('.controls').show(1000,"swing");
                    }
                    checkContainer();

            });
            $('#pac-input').focusout(function(e){
                e.preventDefault();
                if (!$(this).attr('data-city')) {
                    $(this).val('');
                    $(this).attr("placeholder", "Please select a location from the suggestions");
                }
            });
            $('.planning-yes').click(function(e){
                e.preventDefault();
                var input = '<input type="hidden" id="planning_input" name="planning_requested" value = "1">';
                $(input).insertAfter(this);
                $('.next').click();

            });
            $('.planning-no').click(function(e){
                e.preventDefault();
                $('#planning_input').remove();
                $('.next').click();

            });
            $('.budget-yes').click(function(e){
                e.preventDefault();
                //remove all budget inputs
                $('.budget-input').remove();

                //Check if the budget list element exists
                if ($('.questions > li.budget-result').length == 0) {
                    
                    //add budget list element
                    $('ol').each(function(){
                        //breaking these elements down into 2 vars cause they're long

                        var budget_total = '<div class=\"budget-total\"> Total: <span id=\"calculated-total\"> 00 </span> Kes. </div>';
                        var element = '<li class=\"budget-result\"><span> <label>Budget Calculator</label> </span>'+budget_total+' </li>';
                        var last_child = $(this).find('li.child-class:last');
                        //check if last child exists 
                        if (last_child.length == 0) {
                            var insert_before = $(this).find('li.planning-panel');
                            $(element).insertBefore(insert_before);
                        }
                        else{
                            $(element).insertAfter(last_child);
                        }
                    });

                    //add new element to list
                    startStep();

                    //mark li element as toggled
                    $(this).closest('li.start-slide').addClass('is-toggled');
                }
                toggleClassMenu();
                remove_slide();
                // $('.next').click();
            });
            $('.budget-no').click(function(e){

                e.preventDefault();

                //mark li element as toggled
                $(this).closest('li.start-slide').addClass('is-toggled');
                toggleClassMenu();
                remove_slide();
            });

            //remove slide element
            function remove_slide(){
                $(".budget-enquiry").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){
                    $(this).remove();
                });
            }
            $("a.social-signin").on("click", function (e) {
                    e.preventDefault();
                    $('.info-container').addClass('info-container-loading');
                    var src = $(this).attr("href");
                    var temp = window.open(src); 

                    temp.addEventListener('load', function() { 
                        closeSocialLoginTab(temp);
                    } , false);

                    // fallback for event listener
                    setInterval(function(){
                        var url = temp.location.pathname; 
                        if (/dashboard/i.test(url)){
                            closeSocialLoginTab(temp);
                        }
                    }, 30);
            });
            
            $("#mult-classifs").imagepicker({
                show_label: true,
            });

           

            $('#event_date').combodate({
                firstItem: 'name',
                minYear: 2017,
                maxYear: 2019,
                smartDays:true,
                minDate: new Date()
            });

            var selection = [];
            var array_ajax = [];
            var array_selection = [];
             //since we're using the fallback, we need to ensure we don't run the close function twice
            var already_closed = 0; 
            var ajax_status = 1;
            function closeSocialLoginTab(temp){
                if (already_closed == 0) {
                        already_closed = 1;
                        temp.close(); 
                        $('.info-container').removeClass('info-container-loading');
                        $('.questions').hide();
                        $('.loading').show();
                        $.ajax({
                                type: 'get',
                                url: '/jobs/get/user_number_blade',
                                success: function(data) {
                                    $('.info-form').remove();
                                    $('.loading').hide();
                                    $('#theForm').append(data);
                                },
                                error: function(){
                                    $(".no-load").show();
                                    $('.loading').hide();
                                }
                        });
                    }
                
            }
            function compareArrays(arr1, arr2) {
                return $(arr1).not(arr2).length == 0 && $(arr2).not(arr1).length == 0
            };

             $('.thumbnail').click(function beginfetch(){
                var moveby = $(this).parent().outerHeight(true);
                var selected = $('#mult-classifs').val();

                var status = (compareArrays(selected, array_selection) ? 'true' : 'false');


                if (status==="false") {
                    array_selection = $('#mult-classifs').val();

                    $('.image_picker_selector').stop().animate({
                    'scrollTop': $('.image_picker_selector').scrollTop()+moveby
                    }, 780, 'swing');

                     $('.controls').show(1000,"swing");
                    var val_raw = $(this).children('p').text();
                    var value = encodeURIComponent(val_raw);
                    var arrayCheck = $.inArray( value, selection );
                    var newVal = value.replace(/[^A-Z0-9]+/ig, "_");
                    if (arrayCheck != -1) {

                        var index = selection.indexOf(value);
                        if (index >= 0) {
                          selection.splice( index, 1 );
                          $( "."+newVal ).remove();
                        }
                    }
                    else{

                        if (selection[0] != null) {
                            var lastEl = selection[selection.length-1];
                            var newEL = lastEl.replace(/[^A-Z0-9]+/ig, "_");

                            $("<li><span>" +val_raw+ "</span> <div class=\"loading loading--double\"></div> <p class=\" no-load \"> Error loading request <a href=\"#\" onclick = \"fetchChild();\" >Reload</a> </p></li>" ).insertAfter("."+newEL).addClass(newVal+ " child-class");
                        }
                        else {
                            if(!$( ".questions >li:first-child").next('li').hasClass("first-subclass")){
                                $( ".questions >li:first-child").next('li').remove();
                            }
                            
                           var firstly =  $( "<li><span>" +val_raw+ "</span><div class=\"loading loading--double\"></div> <p class=\" no-load \"> Error loading request <a href=\"#\" onclick = \"fetchChild();\" >Reload</a> </p> </li>" ).insertAfter( ".questions >li:first-child" ).addClass(newVal+ " child-class");


                        }
                        //add new element
                        startStep();

                        //add new category id to arrays
                        selection.push(value);
                        array_ajax.push(value);

                        //get child element
                        fetchChild();
                    }
                }

             });

            

            function fetchChild(){
                if (array_ajax[0] != null && ajax_status == 1) {
                    ajax_status = 0;
                    var current = array_ajax[0];
                    var current_class = current.replace(/[^A-Z0-9]+/ig, "_");
                    $.ajax({
                            type: 'get',
                            url: '/jobs/child-class',
                            data: 'option='+current,
                            success: function(data) {
                                $("."+current_class).html(data);
                                ajax_status = 1;
                                array_ajax.shift();


                            },
                            error: function(){
                                $("."+current_class > ".no-load").show();
                                $('.loading').hide();
                            }
                        });
                }
            }
            setInterval( fetchChild, 100);
            $( ".pass" ).focusin(function() {
                $('.controls').show(1000,"swing");
            });
            function forceSend(){
                $('.controls').hide();
                $('.loading').show();
                $.ajax({
                type: 'post',
                url: '{{ route('jobs.store') }}',
                data: $("form").serialize(),
                success: function(data) {
                    $('html').html(data);
                },
                error: function(){
                    $(".no-load").show();
                    $('.loading').hide();
                }
                });
            }
    // for budget estimate

    $( document ).on( 'click', '.change-budget', function(e) {
        e.preventDefault();
        //get name of the input
        var name = $(this).data('inputName');

        var input = $("input[name="+name+"]");

        var input_val = input.val();
        var id = input.data('estimateId');

        //change name to budget
        input.attr('name','budget-'+id+'').removeAttr('disabled');
        //insert hidden input with estimate value
        $('<input name=\"'+name+'\" type=\"number\" disabled required data-parsley-type=\"number\" style=\"display:none;\" value=\"'+input_val+'\" data-parsley-group = \"budget-estimates\" data-parsley-trigger = \"change focusout\" min=\"5000\"/>').insertAfter(input);

        //show suggestion
        input.siblings('div.estimate-suggestion').addClass('estimate-visible');
        //focus on the input
        input.focus();
        //hide change button
        $(this).hide();

    });
    
    //calculate total for budgets
    function findTotal(){
        var arr = document.getElementsByClassName('estimate-budget-input');
        var tot=0;
        for(var i=0;i<arr.length;i++){
            if(parseInt(arr[i].value))
                tot += parseInt(arr[i].value);
        }

        //append new total to input
        span = document.getElementById("calculated-total");
        txt = document.createTextNode(tot.toLocaleString());
        span.innerText = txt.textContent;
    }
</script>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-50669449-1', 'auto');
    ga('send', 'pageview');

</script>
<script type="text/javascript">
    adroll_adv_id = "TH4Q5IVG5FHXNNPGFG2JBA";
    adroll_pix_id = "342MRCCGYRABZFOVG5SIPM";
    /* OPTIONAL: provide email to improve user identification */
    @if(Auth::check())
     adroll_email = "{{Auth::user()->email}}";
    @endif
    (function () {
        var _onload = function(){
            if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
            if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
                document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
        };
        if (window.addEventListener) {window.addEventListener('load', _onload, false);}
        else {window.attachEvent('onload', _onload)}
    }());
</script>
<!--Start of Crisp Live Chat Script-->
<script type="text/javascript">
        CRISP_WEBSITE_ID = "aeb94ef1-11bf-4618-85b7-dce7f0ee2b92";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.im/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();
        
        function checkContainer () {
            if($('div.crisp-client > div > div > a').is(':visible')){ //if the container is visible on the page
                $('div.crisp-client > div > div > a').attr('style', 'right: auto !important; left: 50% !important; transform: translateX(-50%);');
    
            } else {
                setTimeout(checkContainer, 50); //wait 50 ms, then try again
                
            }
        }
</script>
<script type="text/javascript">
    function initMap(){
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                input.value = "";
                console.log("Returned place contains no ");
                return;
            }

            var address = "";
            var city_name = "";
            var latlng = "";
            var latitude;
            var longitude;
            places.forEach(function(place) {
                if (!place.geometry) {
                    input.value = "";
                    console.log("Returned place contains no geometry");
                    return;
                }

                latlng = place.geometry.location;
            });
            
            //fetch address
            var geocoder = new google.maps.Geocoder;

            geocoder.geocode({'latLng': latlng}, function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                // console.log(results)
                if (results[1]) {
                 //formatted address
                address = results[0].formatted_address;
                latitude = results[0].geometry.location.lat();
                longitude = results[0].geometry.location.lng();
                //find country name
                     for (var i=0; i<results[0].address_components.length; i++) {
                    for (var b=0;b<results[0].address_components[i].types.length;b++) {

                    //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                        if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                            //this is the object you are looking for
                            city= results[0].address_components[i];
                            break;
                        }
                    }
                }
                //city data
                city_name = city.long_name;
                input.dataset.city = city_name;
                input.dataset.lat = latitude;
                input.dataset.lng = longitude;

                } else {
                  console.log("No results found")
                }
              } else {
                console.log("Geocoder failed due to: " + status)
              }
              console.log(city_name)
            });
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&libraries=places&callback=initMap"></script>

<!--End of Crisp Live Chat Script-->
	</body>
</html>