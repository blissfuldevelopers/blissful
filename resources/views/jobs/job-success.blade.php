<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        <meta name="title" content="Quotation request created"/>
        <meta name="description" content="Get Price Estimates from multiple event service providers. Find event service providers in Kenya" >
        <title>Quotation request created | Blissful</title>
        <link rel="icon" type="image/png" href="/img/dashboard/favicon.ico"/>
        <link rel="shortcut icon" href="../favicon.ico">
        <link rel="stylesheet" type="text/css" href="/assets/css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/job-create.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/component.css" />
        <script src="/assets/js/modernizr.custom.js"></script>

    </head>
    <body>
        <div class="container">
            <section class="centered-section">
                <div>
                   <span>ThankYou!</span>
                        <div class="row">
                            <p>
                                We are matching suitable suppliers for you.</br>
                                You will receive proposals for your requests for the next 30 days.
                            </p>
                        </div>
                        <div class="nav-wrap" >
                            <a href="/dashboard" class=" action-button" onclick="ga('send', 'event', 'button', 'click', 'jobs-close');" >Close &nbsp; &times;</a>
                        </div>  
                </div>                                  
            </section>
        </div><!-- /container -->
        
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-50669449-1', 'auto');
    ga('send', 'pageview');

</script>
<script type="text/javascript">
    adroll_adv_id = "TH4Q5IVG5FHXNNPGFG2JBA";
    adroll_pix_id = "342MRCCGYRABZFOVG5SIPM";
    /* OPTIONAL: provide email to improve user identification */
    @if(Auth::check())
     adroll_email = "{{Auth::user()->email}}"; 
    @endif
    (function () {
        var _onload = function(){
            if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
            if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
                document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
        };
        if (window.addEventListener) {window.addEventListener('load', _onload, false);}
        else {window.attachEvent('onload', _onload)}
    }());
</script>
    </body>
</html>