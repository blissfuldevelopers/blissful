@if(empty($user->profile->phone))
<div class="container">
    <section class="new_user-section">
        <div class="new_user">
           <h3 class="fs-title">Final step, set phone number for your account</h3>
            <div class="row">
                <p class="fs-desc">
                    Hi, {{$user->first_name}}. You can kick back and relax <i>&#9786;</i>  We are currently matching suitable suppliers for you.</br>
                    But first, you will need to add a phone number which you'd like Price Estimates to be sent to .
                </p>
                <div class="intro-img ">
                    <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$user->profile->profilePic}}">
                </div>
            </div>
            <h4 class="form-signin-heading">{{$user->email}}</h4>
            @include('includes.errors')
            <div class="new_user-dets questions">
                {!! Form::tel('phone', null,
                    [
                    'class' => 'pass',
                    'required' => 'required',
                    'placeholder'=>'07XX123456',
                    'data-parsley-required-message' => 'Please input your phone number',
                    'data-parsley-minlength'        => '6',
                    'data-parsley-minlength-message' => 'Phone number invalid, please retype',
                    'data-parsley-trigger'          => 'focusout',
                    'data-parsley-group' =>'second'
                     ]
                )!!}
                {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="nav-wrap">
                <input type="submit" id="submit_dets" name="submit" class="action-button" onclick="ga('send', 'event', 'button', 'click', 'set-phonenumber');" value="Save Details" />
            </div>  
        </div>                                  
    </section>
    <script type="text/javascript">
        $('#submit_dets').click(function(e){
            e.preventDefault();
            $('.next').click();
        });
    </script>
</div><!-- /container -->
@else
<script type="text/javascript">
    $('.next').click();
</script>
@endif