<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        <title>Blissful | Complete Profile</title>
        <link rel="icon" type="image/png" href="/img/dashboard/favicon.ico"/>
        <link rel="shortcut icon" href="../favicon.ico">
        <link rel="stylesheet" type="text/css" href="/assets/css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/job-create.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/component.css" />
        {!! HTML::style('/assets/css/parsley.css') !!}
        <script src="/assets/js/modernizr.custom.js"></script>

    </head>
    <body>
        <div class="container">
            <section class="new_user-section">
                <div class="new_user">
                   <h3 class="fs-title">Final step, create a password for your account</h3>
                    <div class="row">
                        <p>
                            Hi, {{$user->first_name}}. We are matching suitable suppliers for you.</br>
                            But first, you will need to create a password, which you will use to login into your blissful account to view Price Estimates.
                        </p>
                        <div class="intro-img ">
                            <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$user->profile->profilePic}}">
                        </div>
                    </div>
                    <h4 class="form-signin-heading">{{$user->email}}</h4>
                    {!! Form::open(['name'=>'userReg','url' => route('auth.reset-post', ['token' => $token ]),'data-parsley-validate' ] ) !!}

                    @include('includes.errors')
                    <div class="new_user-dets questions">
                        {!! Form::password('password', [
                        'class' => 'form-password', 
                        'placeholder' => 'Password', 
                        'required',  
                        'id' => 'inputPassword', 
                        'autofocus',
                        'data-parsley-required-message' => 'Password is required',
                        'data-parsley-trigger'          => 'change focusout',
                        'data-parsley-minlength'        => '6',
                        'data-parsley-maxlength'        => '20'
                        ]) !!}


                        {!! Form::password('password_confirmation', [
                        'class' => 'form-password', 
                        'placeholder' => 'Password confirmation',
                        'required',  
                        'id' => 'inputPasswordConfirmation',
                        'data-parsley-trigger'          => 'change focusout',
                        'data-parsley-equalto'          => '#inputPassword',
                        'data-parsley-equalto-message'  => 'Not same as Password',
                         ]) !!}

                        <input type="hidden" name="from_jobs" value="1">

                    </div>

                    <div class="nav-wrap">
                        <input type="submit" id="submit_dets" name="submit" class="submit action-button" onclick="ga('send', 'event', 'button', 'click', 'set-password');" value="Set Password" />
                    </div>
                    {!! Form::close() !!}
                    <div class="nav-wrap success_login" style="display:none;">
                        <p class="green">Password successfully created, click below to login</p>
                        <a href="/login" class="action-button">Login</a>
                    </div>  
                </div>                                  
            </section>
        </div><!-- /container -->

        {!! HTML::script('/assets/plugins/parsley.min.js') !!}
        <script type="text/javascript">
             $( document ).ready(function() {
                $('form[name=userReg]').parsley({
                    errorsWrapper: '<div></div>',
                    errorTemplate: '<span style="color:#F75B4A"></span>'
                });
             });
            

            $('form').submit(function(event){
                event.preventDefault();
                $('.new_user-dets').hide();
                $('#submit_dets').hide();
                $('.new_user-dets').after('<div class=\" password-loader \"> Saving Password.</br> <div class=\"loading loading--double\"></div></div>');

              $.ajax({

                url : '/password/{{$token}}',
                type : 'POST',
                data : $('form').serialize(),
                
                success : function(data) {
                    $('html').html(data);

                },
                error : function() {
                    $('.password-loader').hide().remove();
                    alert("error ");
                }

                });
            });
        </script>

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-50669449-1', 'auto');
            ga('send', 'pageview');

        </script>
        <script type="text/javascript">
            adroll_adv_id = "TH4Q5IVG5FHXNNPGFG2JBA";
            adroll_pix_id = "342MRCCGYRABZFOVG5SIPM";
            /* OPTIONAL: provide email to improve user identification */
            @if(Auth::check())
             adroll_email = "{{Auth::user()->email}}"; 
            @endif
            (function () {
                var _onload = function(){
                    if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
                    if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
                    var scr = document.createElement("script");
                    var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
                    scr.setAttribute('async', 'true');
                    scr.type = "text/javascript";
                    scr.src = host + "/j/roundtrip.js";
                    ((document.getElementsByTagName('head') || [null])[0] ||
                        document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
                };
                if (window.addEventListener) {window.addEventListener('load', _onload, false);}
                else {window.attachEvent('onload', _onload)}
            }());
        </script>
    </body>
</html>

