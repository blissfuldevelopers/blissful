
<link rel="stylesheet" type="text/css" href="/css/multistep-form.css">
{!! HTML::style('/assets/css/parsley.css') !!}
<link rel="stylesheet" href="/css/form-tooltip.css" media="screen" type="text/css">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<!-- multistep form -->
<div class="row">

@if(Session::has('jobcreated'))
<div id="msform" class="fs-thanks">
	<fieldset>
		<h2 class="fs-title">Thankyou !</h2>
		<div class="row">
			<p>
				We are matching suitable suppliers for you.</br>
				We will get in touch with you in the next 48 hours.
			</p>
		</div>
		<div class="nav-wrap">
			<a href="{{ route('jobs.create') }}" class=" action-button" onclick="ga('send', 'event', 'button', 'click', 'jobs-close');" >Close &nbsp; &times;</a>
		</div>
	</fieldset>
	
</div>
@else
{!! Form::open(['route' => 'jobs.store', 'id'=>'msform','data-parsley-validate','onLoad'=>'functionLoadMCE();']) !!}
	<!-- progressbar -->
	<ul id="progressbar">
		<li class="active"></li>
		<li></li>
		<li></li>
	</ul>
	<!-- fieldsets -->
	<fieldset>
		<h2 class="fs-title">Tell us what you need.</h2>
<!-- 		<h3 class="fs-subtitle">This is step 1</h3>
 -->	<div class="row">
 			<p>I need Price Estimates for &nbsp;
		 		<select tooltipText="Choose the category which you want to post a job for"  name="classification">
		            <optgroup label="Category">
		                @foreach($classifications as $classification)
		                    <option value="{{$classification->name}}">{{$classification->name}}</option>
		                @endforeach
		            </optgroup>
		        </select>
			&nbsp; For an event to be held on &nbsp;
	        	{!! Form::text('event_date', null, 
	        	[
	        	'class' => 'getdate', 
	        	'tooltipText'=>'Click to choose the date on which your event is to be held',
	        	'id'=>'event_date', 
	        	'required' => 'required', 
	        	'placeholder'=>date('Y-m-d',strtotime('tomorrow')),
	        	'data-format'=>'YYYY-MM-DD',
	        	'data-template'=>'YYYY MM DD',
	        	'data-parsley-required-message' => 'You need to include an event date',
        		'data-parsley-trigger'          => 'change focusout'

	        	]) !!}
	        	{!! $errors->first('event_date', '<p class="help-block">:message</p>') !!}
	        
	        &nbsp; in &nbsp;
	        <select tooltipText="Where will the event be held ?" name="event_location" placeholder="Event Location">
                <optgroup label="Event Location">
                    @foreach($locations as $location)
                        <option value="{{$location->name}}">{{$location->name}}</option>
                    @endforeach
                </optgroup>
    		</select>
	        </p>
 		</div>
 			<input type="button" name="next" onclick="ga('send', 'event', 'button', 'click', 'job-next-1');" class="next action-button first-btn" value="Next &nbsp; &gt;" />
	</fieldset>
	<fieldset>
		<h2 class="fs-title">...A bit more...</h2>
		<div class="row">
			<p>
				My budget is about &nbsp;
				{!! Form::number('budget', null, 
				[
				'required' => 'required', 
				'placeholder'=>'1000',
				'tooltipText'=>'Make sure you add a reasonable amount for the job to be done',
				'data-parsley-required-message' => 'please include your budget',
        		'data-parsley-trigger' => 'change focusout'
				]) !!}
		        {!! $errors->first('budget', '<p class="help-block">:message</p>') !!}
		        &nbsp;Kes.	
			</p>
			<p>
				What other detail about the event do you think the supplier should know? This may include special requests / instructions.
				<textarea tooltipText="Please give a detailed description of your specifications"
						cols="40" rows="5" id="description" name="description"
                        placeholder="Type in here..."  class="tinymce-text"></textarea>
                {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
			</p>
		</div>
		<input type="button" name="previous" class="previous action-button second-btn" onclick="ga('send', 'event', 'button', 'click', 'job-previous-1');" value="&lt; &nbsp; Previous" />
		@if(!Auth::check() || empty(Auth::user()->profile->phone))
		<input type="button" name="next" class="next action-button" onclick="ga('send', 'event', 'button', 'click', 'job-next-2');" value="Next &nbsp; &gt;" />
		@else
		<input type="hidden" name="phone" value="Auth::check()->profile->phone"></input>
		<input type="submit" name="submit" class="submit action-button" value="Submit" />
		@endif
	</fieldset>
	@if(!Auth::check() || empty(Auth::user()->profile->phone))
	<fieldset>
		<h2 class="fs-title">Tell us a bit about yourself..</h2>
		<div class="row">
			@if(!Auth::check())
			<p>
				My name is &nbsp;
				{!! Form::text('first_name', null, 
				[
				'required' => 'required',
				'placeholder'=>'First Name',
				'data-parsley-required-message' => 'Your first name is required',
	            'data-parsley-trigger'          => 'change focusout',
	            'data-parsley-pattern'          => '/^[a-zA-Z]*$/',
	            'data-parsley-minlength'        => '2',
	            'data-parsley-maxlength'        => '32'
	            ]
	            ) !!}
	        	{!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
	        	&nbsp;
	        	{!! Form::text('last_name', null, 
	        	[
				'required' => 'required',
				'placeholder'=>'Last Name',
				'data-parsley-required-message' => 'Your last name is required',
	            'data-parsley-trigger'          => 'change focusout',
	            'data-parsley-pattern'          => '/^[a-zA-Z]*$/',
	            'data-parsley-minlength'        => '2',
	            'data-parsley-maxlength'        => '32'	        	]) !!}
	        	{!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
			</p>
			<p>
				You can reach me on &nbsp;
				{!! Form::tel('phone', null, 
				[
				'required' => 'required',
				'placeholder'=>'Phone Number',
                'data-parsley-required-message' => 'Please input your phone number',
                'data-parsley-minlength'        => '6',
                'data-parsley-minlength-message' => 'Phone number invalid, please retype',
                'data-parsley-trigger'          => 'focusout'
				 ]) !!}
	        	{!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
			</p>
			<p>
				I would like quotes to be sent to &nbsp;
				{!! Form::email('email', null, 
				[
				'required' => 'required',
				 'placeholder'=>'Email Address',
				 'data-parsley-required-message' => 'Your contact is required',
	            'data-parsley-trigger'          => 'change focusout',
	            'data-parsley-type'             => 'email'
				 ]) !!}
	        	{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
			</p>
			@elseif(empty(Auth::user()->profile->phone))
			<p>
				You can reach me on &nbsp;
				{!! Form::tel('phone', null, 
				[
				'required' => 'required',
				'placeholder'=>'Phone Number',
				'data-parsley-type'=>'number',
                'data-parsley-pattern'=>'^[\d\+\-\.\(\)\/\s]*$',
                'data-parsley-required-message' => 'Please input your phone number',
                'data-parsley-pattern-message' => 'Phone number invalid, please retype',
                'data-parsley-trigger'          => 'change focusout'
				 ]) !!}
	        	{!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
			</p>
			<p>
				{!! Form::password('password', [
                        'class' => 'form-password', 
                        'placeholder' => 'Password', 
                        'required',  
                        'id' => 'inputPassword', 
                        'autofocus',
                        'data-parsley-required-message' => 'Password is required',
                        'data-parsley-trigger'          => 'change focusout',
                        'data-parsley-minlength'        => '6',
                        'data-parsley-maxlength'        => '20'
                        ]) !!}


                {!! Form::password('password_confirmation', [
                'class' => 'form-password', 
                'placeholder' => 'Confirm Password',
                'required',  
                'id' => 'inputPasswordConfirmation',
                'data-parsley-trigger'          => 'change focusout',
                'data-parsley-equalto'          => '#inputPassword',
                'data-parsley-equalto-message'  => 'Passwords do not match',
                 ]) !!}
			</p>
			@endif
		</div>
		
		<input type="button" name="previous" class="previous action-button second-btn" onclick="ga('send', 'event', 'button', 'click', 'job-previous-2');" value="&lt; &nbsp; Previous" />
		<input type="submit" name="submit" class="submit action-button" onclick="ga('send', 'event', 'button', 'click', 'job-submit');" value="Submit" />
	</fieldset>
	@endif
    {!! Form::close() !!}
@endif
</div>
@include('pages.social-proof')
<script type="text/javascript" src="/js/rounded-corners.js"></script>
<script type="text/javascript" src="/js/form-field-tooltip.js"></script>
<script type="text/javascript" src="/js/dashboard/jquery.min.js"></script>
<script type="text/javascript" src="/js/dashboard/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/multistep-form.js"></script>
<script type="text/javascript" src="/assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/assets/js/moment.min.js"></script>
<script type="text/javascript" src="/assets/js/combodate.js"></script>
{!! HTML::script('/assets/plugins/parsley.min.js') !!}
<script type="text/javascript">
    $('#msform').parsley({
        errorsWrapper: '<div></div>',
        errorTemplate: '<span style="color:#F75B4A"></span>'
    });
</script>
<script type="text/javascript">
	$('#event_date').combodate({
		firstItem: 'name',
		minYear: 2015,
    	maxYear: 2020
	});
</script>
<script type="text/javascript">
var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
    functionLoadMCE();

      function functionLoadMCE() {
        if(!isMobile) {
            tinymce.init({
            selector: ".tinymce-text",
            theme: "modern",
            menubar: false,
            plugins: [
                "advlist autolink lists link charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime  nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern imagetools "
            ],
            toolbar: " bold italic alignleft aligncenter alignright alignjustify bullist numlist outdent indent preview",
            relative_urls: false,
            image_advtab: true,
            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ]
        });
        }

    else{
            tinymce.init({
            selector: ".tinymce-text",
            theme: "modern",
            menubar: false,
            toolbar: false,
            relative_urls: false,
            image_advtab: true,
            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ]
        });
        } 
      }
    
    
$('.submit-btn').click(function(event){
    event.preventDefault();
    tinyMCE.triggerSave();

      var editorContent = tinyMCE.activeEditor.getContent();
      if ( editorContent == '' || editorContent == null)
      {
        $(tinyMCE.activeEditor.getContainer())
          .css("background-color", '#ffeeee')
          .parent()
          .css({
            "background-color": '#ffeeee',
            "border": '1px solid #EC736C'
          });
        $('<span class="red">Please Type in your message</span>').insertAfter($(tinyMCE.activeEditor.getContainer()));

      }

    else{
        $(this).closest('form').submit();
    }

    $(this).delay(7000).queue(function (){

        $.ajax({
            type: "GET",
            url: "/socials/jobs_create/first/job_create_first",
            success : function(data) {
             
                  $(".first > .dets > .social_number").html(data.result.number);
                  $(".first > .dets > .social_message").html(data.result.message);
                  $(".first > .dets > .social_day_number").html(data.result.day_number);
                  $(".first").show();
                  $(".first").delay( 10000 ).fadeOut(2000);
            }
        });
      $(this).dequeue();

    });

  });
</script>
<script type="text/javascript">
var tooltipObj = new DHTMLgoodies_formTooltip();
tooltipObj.setTooltipPosition('right');
tooltipObj.setPageBgColor('#1BB2A6E');
tooltipObj.setCloseMessage('close');
tooltipObj.initFormFieldTooltip();
</script>

