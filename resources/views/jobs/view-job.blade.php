<h1>{{ $job->title }}</h1>
<div class="container">

    <p><strong>Job description:</strong> <br>{{ $job->description }}</p>

    <p><strong>Credits Required:</strong> <br>{{ $job->credits}}</p>

    <p><strong>Quatation Request From:</strong> <br>{{ $job->user->first_name}}</p>

    {!! Form::open(['route' => 'job.bid', 'class' => 'form-horizontal']) !!}

    <div class="form-group {{ $errors->has('bid_details') ? 'has-error' : ''}}">
        {!! Form::label('bid_details', 'Quotation: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::textarea('bid_details', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('bid_details', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <input type="hidden" name="job_id" value="{{$job->id}}">
    <input type="hidden" name="user_id" value="{{ $job->user->id}}">

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Send quotation', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}
</div>
@if ($errors->any())
    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif
