@extends('page.zurb-layout')
@section('title')
    Jobs
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="/css/foundation-datepicker.css">
@endsection
@section('content')
    <main role="main" id="contactpg">
        @include('includes.status')
        @include('jobs.partials.jobs-container')
    </main>
@endsection

@section('scripts')
    <script src="/js/foundation-datepicker.min.js"></script>
    {!! HTML::script('/assets/js/ajaxify_bliss.script.js') !!}
    <script>

        ajaxify_bliss({
            container: $('#jobs-container-data'),
            container_message: $('#jobs-container-message'),
            bindKeyboardBack: true
        });


    </script>
@endsection