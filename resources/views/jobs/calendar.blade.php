<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSS reset -->
	<link rel="stylesheet" href="/assets/css/timeline.css"> <!-- Resource style -->
</head>
<body>
<div class="row">
<div class="small-12">
     <span class="right">
             <a class="button tiny warning non-ajax " href="{{ route('jobs.create') }}">
                 Request for a quote
             </a>
     </span>
</div>
	<section id="cd-timeline" class="cd-container">
		<div class="cd-timeline-block">
			<a  href="{{ route('jobs.create') }}" class=" non-ajax cd-timeline-create cd-timeline-img cd-location" data-content="Create new Quotation Request">
					<img src="/assets/img/create.svg" alt="Picture">
			</a> <!-- cd-timeline-img -->
			<div class="cd-timeline-content" style="display: none;">
			</div>
		</div> <!-- cd-timeline-block -->
		@if($events->total() > 0)
			@foreach($events as $event)
			<div class="cd-timeline-block">
				<div class="cd-timeline-img cd-job">
					<img src="/assets/img/tl-job.svg" alt="Picture">
				</div> <!-- cd-timeline-img -->

				<div class="cd-timeline-content">
					<h2>{{$event->title}}</h2>
					<p>{{$event->jobs->count()}} Jobs created</p>
					<form action="/jobs/events/{{$event->id}}" method="get">
	                    <input type="hidden" name="_method" value="get"/>
	                    <a href="#0" class="cd-read-more" onclick="$(this).closest('form').submit()">View Jobs</a>
	                </form>
					<span class="cd-date">{{$event->start->formatLocalized('%B %d')}}</span>
				</div> <!-- cd-timeline-content -->
			</div> <!-- cd-timeline-block -->
			@endforeach
		@endif
		@if(is_object($events))
            <div class="small-12">
                {!! $events->render(Foundation::paginate($events)) !!}
            </div>
        @endif
		@if($jobs->total() > 0)
			@foreach($jobs as $job)
				<div class="cd-timeline-block">
					<div class="cd-timeline-img cd-job">
						<img src="/assets/img/tl-job.svg" alt="Picture">
					</div> <!-- cd-timeline-img -->

					<div class="cd-timeline-content">
						<h2>{{$job->title}}</h2>
						<p>{{strip_tags(str_limit($job->description,150))}}</p>
						<form action="{{ route('jobs.show', [$job->id]) }}" method="get">
		                    <input type="hidden" name="_method" value="get"/>
		                    <a href="#0" class="cd-read-more" onclick="$(this).closest('form').submit()">View Proposals</a>
		                </form>
						<span class="cd-date">{{$job->created_at->formatLocalized('%B %d')}}</span>
					</div> <!-- cd-timeline-content -->
				</div> <!-- cd-timeline-block -->
			@endforeach
		@endif
		@if(is_object($jobs))
            <div class="small-12">
                {!! $jobs->render(Foundation::paginate($jobs)) !!}
            </div>
        @endif
	</section> <!-- cd-timeline -->
</div>

<script src="/js/jquery-2.2.1.min.js"></script>
<script src="/assets/js/moment.min.js"></script>
<script src="/assets/js/timeline.js"></script> <!-- Resource jQuery -->
<script src="/assets/js/modernizr.js"></script> <!-- Modernizr -->
</body>
</html>