<div class="bs-component">
    <div class="row">
        <h3 class="col-xs-12 col-md-6 col-xs-offset-5">Create New Job</h3>
    </div>
    <div class="row">
        <hr/>
        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
    </div>
    {!! Form::open(['route' => 'jobs.store', 'class' => 'form-horizontal']) !!}

    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
        {!! Form::label('title', 'Title: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
        {!! Form::label('description', 'Description: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('classification', 'Venue: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <select name="classification" placeholder=category" class="form-control"
                    data-options="disable_for_touch:true" ,
                    data-tooltip="category" aria-haspopup="true"
                    title="Select the city or town your business is located."
                    required data-parsley-required-message="Please select where you are located"
                    data-parsley-trigger="change focusout">
                <optgroup label="Category">
                    @foreach($classifications as $classification)
                        <option value="{{$classification->id}}">{{$classification->name}}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>

    <div class="form-group {{ $errors->has('budget') ? 'has-error' : ''}}">
        {!! Form::label('budget', 'Budget: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6 input-group">
            <span class="input-group-addon">Ksh</span>
            {!! Form::text('budget', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('budget', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('event_date') ? 'has-error' : ''}}">
        {!! Form::label('event_date', 'Event Date: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('event_date', null, ['class' => 'form-control datepicker', 'required' => 'required']) !!}
            {!! $errors->first('event_date', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('event_location', 'Location: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            <select name="event_location" placeholder="Event Location" class="form-control"
                    data-options="disable_for_touch:true" , data-tooltip="location" aria-haspopup="true"
                    title="Select the city or town your business is located." required
                    data-parsley-required-message="Please select where you are located"
                    data-parsley-trigger="change focusout">
                <optgroup label="Event Location">
                    @foreach($locations as $location)
                        <option value="{{$location->id}}">{{$location->name}}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}
</div>

