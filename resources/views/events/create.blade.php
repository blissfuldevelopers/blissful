

<div class="container">

    <div class="row">
        <div class="col-sm-12">
            <h1 class="pull-left">Create New Event</h1>
        </div>
    </div>

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'events.store']) !!}

            @include('events.fields')

        {!! Form::close() !!}
    </div>
</div>
