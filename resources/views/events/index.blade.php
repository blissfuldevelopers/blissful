
    <div class="container">

        <h1 class="pull-left">Events</h1>
        <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('events.create') !!}">Add New</a>

        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        @if($events->isEmpty())
            <div class="well text-center">No Events found.</div>
        @else
            @include('events.table')
        @endif
    </div>
