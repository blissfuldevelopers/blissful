
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Edit Event</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($event, ['route' => ['events.update', $event->id], 'method' => 'patch']) !!}

            @include('events.fields')

            {!! Form::close() !!}
        </div>
    </div>
