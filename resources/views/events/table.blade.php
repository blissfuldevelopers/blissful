<style>
    .card {
        font-family: 'Roboto', sans-serif;
        overflow: hidden;
        box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        color: #272727;
        border-radius: 2px;
        padding: 10px;
        background: white;
    }

    .card .title {
        line-height: 3rem;
        font-size: 1.5rem;
        font-weight: 300;
    }

    .card .sub-title {
        line-height: 3rem;
        font-size: 1rem;
        font-weight: 300;
    }

    .card .content {
        padding: 1.3rem;
        font-weight: 300;
        border-radius: 0 0 2px 2px;
    }

    .card p {
        margin: 0;
    }

    .card .action {
        border-top: 1px solid rgba(160, 160, 160, 0.2);
        padding: 1.3rem;
    }

    .card a {
        color: #FF344E;
        margin-right: 1.3rem;
        transition: color 0.3s ease;
        text-transform: uppercase;
        text-decoration: none;
    }

    .card .image {
        position: relative;
    }

    .card .image .title {
        position: absolute;
        bottom: 0;
        left: 0;
        padding: 1.3rem;
        color: #fff;
    }

    .card .image img {
        border-radius: 2px 2px 0 0;
    }

</style>
<div class="row">
    <!-- Main Feed -->
    @foreach($events as $event)
        <div class="small-12 medium-6 large-4 columns">
            <div class="card">
                <div>
                    <span class="title">{!! $event->title !!}</span>
                </div>
                <div class="content">
                    <strong>
                        @if($event->fullday)
                            All Day |
                        @endif

                        {{ date('d-M-Y H:i:s', strtotime($event->start)) }}
                        --
                        {{ date('d-M-Y H:i:s', strtotime($event->end)) }}
                    </strong>
                    <p> {!! $event->desc !!} </p>
                </div>
                <div class="sub-title">
                    {{ $event->jobs->count() }} Jobs
                </div>
                <div class="action">


                    {!! Form::open(['route' => ['events.destroy', $event->id], 'method' => 'delete']) !!}
                    <ul class="inline-list">
                        <li>
                            <a href="{!! route('events.show', [$event->id]) !!}">
                                <i class="glyphicon glyphicon-eye-open"></i>
                                Open
                            </a>
                        </li>
                        <li>
                            <a href="{!! route('events.edit', [$event->id]) !!}">
                                <i class="glyphicon glyphicon-edit"></i>
                                Edit
                            </a>
                        </li>
                        <li>
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i> Delete', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </li>
                    </ul>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <!-- End Feed Entry -->
    @endforeach

</div>

<div class="small-12 columns">
    @if(is_object($events))

        {!! $events->render(Foundation::paginate($events)) !!}

    @endif
</div>