<style>


    .event {
        border: 1px solid #eee;
        box-shadow: 0 2px 0 rgba(204, 204, 204, 0.15);
        margin-bottom: 1rem;
        border-radius: 0.25rem;
        background: white;
        position: relative;
    }

    .event a {
        color: #FF344E;
        margin-right: 1.3rem;
        transition: color 0.3s ease;
        text-transform: uppercase;
        text-decoration: none;
    }

    .event .event-details {

    }

    .event .event-details:after {
        border-left: 22px solid #7D8D95;
        border-bottom: 15px solid transparent;
        position: absolute;
        right: -4%;
        top: 28px;
        content: "";
        border-top: 15px solid transparent;
    }

    .event .jobs-list {
        border-left: 1px solid #7D8D95;
        font-size: 1.25rem;
        font-weight: 300;
        line-height: 6.2rem;
        padding: 0 1.25rem 1.25rem 1.5rem;
        overflow: hidden;
        position: relative;
    }

    .event .jobs-list .add-job-call h2 {
        color: #0E2D3B;
        margin: 0;
        font-size: 1.8rem;
        -webkit-margin-before: 0.83em;
        -webkit-margin-after: 0.83em;
        -webkit-margin-start: 0px;
        -webkit-margin-end: 0px;
    }

    .event .event-icon {
        position: absolute;
        right: 1.25rem;
        top: 2.2rem;
        font-size: 1.7rem;
        line-height: 1em;
    }

    .event .event-icon-shadow {
        position: absolute;
        color: rgba(0, 0, 0, 0.05);
        font-size: 8rem;
        top: -1.25rem;
        right: -1.25rem;
        transform: rotate(-20deg);
    }

    .event ul {
        margin: 0;
        padding: 0;
        list-style: none;
    }

    .event .day {
        padding: 1.25rem 1.9rem;
        border-bottom: 1px solid #f2f2f2;
        color: #888888;
    }

    .event .day:last-child {
        border-bottom: none;
    }

    .event .day .image {
        display: block;
        float: left;
        margin-right: 0.75rem;
    }

    .event .day .day-title {
        text-transform: uppercase;
        color: #666666;
        font-weight: 600;
    }

    .event .day .day-icon {
        margin-top: -0.7rem;
        color: #666666;
        font-size: 1.25rem;
        float: right;
    }
</style>
<div class="container">
    <!-- Feed Entry -->
    <div class="row event">
        <div class="small-12 medium-6 columns event-details">
            <div class="large-2 columns small-3"><img src="http://placehold.it/80x80&text=[img]"/></div>

            <h4>{!! $event->title !!}</h4>
            <p>
                <strong>
                    @if($event->fullday)
                        All Day |
                    @endif

                    {{ date('d-M-Y H:i:s', strtotime($event->start)) }}
                    --
                    {{ date('d-M-Y H:i:s', strtotime($event->end)) }}
                </strong>
            </p>
            <p>
                {!! $event->desc !!}
            </p>


            {!! Form::open(['route' => ['events.destroy', $event->id], 'method' => 'delete']) !!}
            <ul class="inline-list">

                <li>
                    <a href="{!! route('events.edit', [$event->id]) !!}" class=''>
                        <i class="glyphicon glyphicon-edit"></i>
                        Edit
                    </a>
                </li>
                <li>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i> X', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </li>
            </ul>
            {!! Form::close() !!}
        </div>

        <div class="small-12 medium-6 columns jobs-list">
            <div class="add-job-call">
                <h2>
                    {{ $event->jobs->count() }} Jobs
                    <a href="{!! route('jobs.create', ['event' => $event->id]) !!}"
                       class='right'>
                        <i class="glyphicon glyphicon-edit"></i>
                        Request for a Job
                    </a>
                </h2>
            </div>

            <ul>
                @foreach($event->jobs as $job)
                    <li class="day {{ ($job->completed) ? 'completed' : 'incomplete' }}">
                            <span class="image">
                              <img src="http://placehold.it/80x80/f2f2f2"/>
                            </span>
                        <a href="{!! route('jobs.edit', [$job->id]) !!}" class="right">
                            <i class="glyphicon glyphicon-edit"></i>
                            Edit
                        </a>
                        <span class="day-title">{{ $job->title }}</span><br/>
                            <span class="temp">
                                <p>{{ $job->description }}</p>
                                <p>
                                    [ {{ $job->bids->count() }} proposals]
                                </p>


                            </span>
                    </li>
                @endforeach
                <li>
                    <hr/>
                </li>
            </ul>
        </div>

    </div>

    <!-- End Feed Entry -->
</div>

