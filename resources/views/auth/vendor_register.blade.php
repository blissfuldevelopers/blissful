@extends('layouts.main')

@section('head')
    {!! HTML::style('/assets/css/register.css') !!}
    {!! HTML::style('/assets/css/parsley.css') !!}
    {!! HTML::style('/css/select2.min.css') !!}
@stop
@section('title')
    Sign Up
@endsection
@section('content')

    {!! Form::open(['url' => route('auth.vendorRegister-post'), 'class' => 'form-signin', 'data-parsley-validate' ] ) !!}

    @include('includes.errors')

    <h2 class="form-signin-heading">Please Register Your Business</h2>

    <label for="inputEmail" class="sr-only">Email address</label>
    {!! Form::email('email', null, [
        'class'                         => 'form-control',
        'placeholder'                   => 'Email address',
        'required',
        'id'                            => 'inputEmail',
        'data-parsley-required-message' => 'Email is required',
        'data-parsley-trigger'          => 'change focusout',
        'data-parsley-type'             => 'email'
    ]) !!}

    <label for="inputFirstName" class="sr-only">Business name</label>
    {!! Form::text('first_name', null, [
        'class'                         => 'form-control',
        'placeholder'                   => 'Business name',
        'required',
        'id'                            => 'inputFirstName',
        'data-parsley-required-message' => 'Business Name is required',
        'data-parsley-trigger'          => 'change focusout',
        'data-parsley-pattern'          => '/^[a-zA-Z]*$/',
        'data-parsley-minlength'        => '5',
        'data-parsley-maxlength'        => '32'
    ]) !!}

    <div class="form-group">
        {!! Form::label('classification_list','Service Categories:') !!}
        {!! Form::select('classifications_list[]', $classifications, null, [
        'id'=>'classification_list',
        'class'                         => 'form-control step2',
        'multiple',
        'style'                         =>'width:100%;',
        'required',
        'data-parsley-required-message' => 'Category is required',
        'data-parsley-trigger'          => 'change focusout'
        ]) !!}
    </div>


    <label for="inputPassword" class="sr-only">Password</label>
    {!! Form::password('password', [
        'class'                         => 'form-control',
        'placeholder'                   => 'Password',
        'required',
        'id'                            => 'inputPassword',
        'data-parsley-required-message' => 'Password is required',
        'data-parsley-trigger'          => 'change focusout',
        'data-parsley-minlength'        => '6',
        'data-parsley-maxlength'        => '20'
    ]) !!}


    <label for="inputPasswordConfirm" class="sr-only has-warning">Confirm Password</label>
    {!! Form::password('password_confirmation', [
        'class'                         => 'form-control',
        'placeholder'                   => 'Password confirmation',
        'required',
        'id'                            => 'inputPasswordConfirm',
        'data-parsley-required-message' => 'Password confirmation is required',
        'data-parsley-trigger'          => 'change focusout',
        'data-parsley-equalto'          => '#inputPassword',
        'data-parsley-equalto-message'  => 'Not same as Password',
    ]) !!}

    <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>

    <button class="btn btn-sm btn-primary btn-block register-btn" type="submit">Register</button>

    {!! Form::close() !!}


@endsection

@section('scripts')
    <script type="text/javascript" defer src="/js/select2.full.min.js"></script>
    <script type="text/javascript" defer src="/assets/plugins/parsley.min.js"></script>
    <script defer src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript">
        function load_page_scripts(){
            window.ParsleyConfig = {
                errorsWrapper: '<div></div>',
                errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>'
            };
        }
        
    </script>
@stop