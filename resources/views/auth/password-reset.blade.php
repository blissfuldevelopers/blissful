@extends('page.home-layout')
@section('title')
    Reset your password
@endsection

@section('header-content')
  <?php
    $header_class = 'contentpg-intro';
  ?>
@endsection
@section('content')
    <!-- main content  -->
    <div class="contentpg-main">
        <div class="row">
          <div class="small-12 columns">
            <div class="wrapper">
                {!! Form::open(['url' => route('auth.password-post'),'data-abide','novalidate' ] ) !!}
                <div class="small-12 medium-7 large-6 columns small-centered">
                    @include('includes.status')
                    @include('includes.errors')
                </div>

                <div class="small-12 medium-7 large-6 columns small-centered">
                    <h2>Reset your Password</h2>
                </div>
                <div class="small-12 medium-7 large-6 columns small-centered">
                    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email address', 'required', 'autofocus', 'id' => 'inputEmail','data-live-validate']) !!}
                    <span class="form-error">
                      Please input a valid email
                    </span>
                </div>
                <div class="small-12 medium-7 large-6 columns small-centered">
                    <button class="button button-primary small-12" type="submit">Send me a reset link</button>
                </div>

                {!! Form::close() !!}
            </div>
          </div>
        </div>
    </div>
@endsection