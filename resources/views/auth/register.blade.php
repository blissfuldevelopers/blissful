@section('title')
    Register
@overwrite
@section('styles')
    {!! HTML::style('/assets/css/signin.css') !!}
    {!! HTML::style('/assets/css/register.css') !!}
    {!! HTML::style('/assets/css/parsley.css') !!}
@stop

@section('content')

    <!-- main content  -->
    <div class="contentpg-main">
        <div class="row">
            <div class="small-12 columns">
                <div class="wrapper">
                    <div class="small-12 medium-7 large-6 columns small-centered">
                        {!! Form::open(['url' => route('auth.register-post'), 'class' => 'form-signin', 'data-parsley-validate' ] ) !!}

                        @include('includes.errors')

                        <h2 class="form-signin-heading">Please register</h2>

                        {!! Form::email('email', null, [
                            'class'                         => 'form-control',
                            'placeholder'                   => 'Email address',
                            'required',
                            'id'                            => 'inputEmail',
                            'data-parsley-required-message' => 'Email is required',
                            'data-parsley-trigger'          => 'change focusout',
                            'data-parsley-type'             => 'email'
                        ]) !!}

                        {!! Form::text('first_name', null, [
                            'class'                         => 'form-control',
                            'placeholder'                   => 'First name',
                            'required',
                            'id'                            => 'inputFirstName',
                            'data-parsley-required-message' => 'First Name is required',
                            'data-parsley-trigger'          => 'change focusout',
                            'data-parsley-pattern'          => '/^[a-zA-Z]*$/',
                            'data-parsley-minlength'        => '2',
                            'data-parsley-maxlength'        => '32'
                        ]) !!}

                        {!! Form::text('last_name', null, [
                            'class'                         => 'form-control',
                            'placeholder'                   => 'Last name',
                            'required',
                            'id'                            => 'inputLastName',
                            'data-parsley-required-message' => 'Last Name is required',
                            'data-parsley-trigger'          => 'change focusout',
                            'data-parsley-pattern'          => '/^[a-zA-Z]*$/',
                            'data-parsley-minlength'        => '2',
                            'data-parsley-maxlength'        => '32'
                        ]) !!}
                        {!! Form::input('number', 'phone', null, [
                            'class'                         => 'form-control',
                            'placeholder'                   => 'Phone number 07xxxxxxxx',
                            'required',
                            'id'                            => 'inputPhone',
                            'data-parsley-required-message' => 'Please enter your phone number',
                            'data-parsley-trigger'          => 'change focusout',
                            'data-parsley-minlength'        => '8',
                            'data-parsley-maxlength'        => '32'
                        ]) !!}

                        {!! Form::password('password', [
                            'class'                         => 'form-control',
                            'placeholder'                   => 'Password',
                            'required',
                            'id'                            => 'inputPassword',
                            'data-parsley-required-message' => 'Password is required',
                            'data-parsley-trigger'          => 'change focusout',
                            'data-parsley-minlength'        => '6',
                            'data-parsley-maxlength'        => '20'
                        ]) !!}


                        {!! Form::password('password_confirmation', [
                            'class'                         => 'form-control',
                            'placeholder'                   => 'Password confirmation',
                            'required',
                            'id'                            => 'inputPasswordConfirm',
                            'data-parsley-required-message' => 'Password confirmation is required',
                            'data-parsley-trigger'          => 'change focusout',
                            'data-parsley-equalto'          => '#inputPassword',
                            'data-parsley-equalto-message'  => 'Not same as the Password above',
                        ]) !!}

                        
                        <div class="small-12 columns text-center">
                            <div id="formCaptcha" class="breaker"></div>
                            <button class="button small button-primary breaker" type="submit">Register</button>
                        </div>
                        

                        <p class="or-social">Or Use Social Login</p>

                        <a href="{{ route('social.redirect', ['provider' => 'facebook']) }}" class="button small small-12 facebook" onclick="ga('send', 'event', 'button', 'click', 'facebook-social-login');"> 
                            <i class="fi-social-facebook"></i>
                        </a>
                        <a href="{{ route('social.redirect', ['provider' => 'google']) }}" class="button small small-12 google" onclick="ga('send', 'event', 'button', 'click', 'google-social-login');">
                           <i class="fi-social-google-plus"></i> 
                        </a>

                        <p class="or-social">By signing up or signing in, I acknowledge and agree to Blissful's <a href="terms-and-conditions">Terms of Use</a> and <a href="privacy-policy">Privacy Policy</a>.</p>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
<script defer src="/assets/plugins/parsley.min.js" onLoad="load_scripts();"></script>
<script type="text/javascript">
    function load_scripts(){
        window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: ' <small class="error"></small>'
        };
    }
</script>