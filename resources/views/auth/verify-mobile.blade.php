
@extends('page.home-layout')
@section('title')
    Complete Profile 
@endsection
@section('header-content')
  <?php
    $header_class = 'contentpg-intro';
  ?>
@endsection
@section('content')
    <!-- main content  -->
    <div class="contentpg-main">
        <div class="row">
          <div class="small-12 columns">
            <div class="wrapper">
                <div class="small-12 medium-6 columns small-centered text-center new_user">
                    <h3>Verify mobile number</h3>
                    <div class="intro-img">
                        <img src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$user->profile->profilePic}}">
                    </div>
                    <p>{{$user->first_name}}</p>
                </div>
                <div class="small-12 medium-7 columns small-centered">
                    <p class="text-center">We sent a text message with a verification code to {{$user->profile->phone}}</p>
                </div>
                <div class="small-12 medium-6 columns small-centered">
                    @include('flash::message')
                    <form name="verifyMobile" method="POST" action="/verify/sms/{{$user->id}}" data-abide novalidate>
                        @include('includes.errors')
                        <div class="amall-12 columns">
                            <label>
                                Verification Code
                                {!! Form::text('token', null, [
                                'class' => 'form-password', 
                                'placeholder' => 'Enter Code here', 
                                'required', 
                                'autofocus',
                                'data-live-validate'
                                ]) !!}
                                <span class="form-error">Please input the verification code</span>
                            </label>
                        </div>
                        @if(!$user->hasUserRole('vendor'))
                        <div class="small-12 columns">
                            <label>
                                Password
                                {!! Form::password('password', null, [
                                'id'=>'password_input',
                                'class' => 'form-password', 
                                'placeholder' => 'Set Password', 
                                'required',
                                'pattern'=>'password',
                                'data-live-validate'
                                ]) !!}
                                <span class="form-error">Please set a valid password, Should be at least 6 characters</span>
                            </label>
                        </div>
                        <div class="small-12 columns">
                            <label>
                                Confirm Password
                                {!! Form::password('password_confirmation', null, [
                                'class' => 'form-password', 
                                'placeholder' => 'Confirm Password', 
                                'required', 
                                'data-equalto'=>'password_input',
                                'data-live-validate'
                                ]) !!}
                                <span class="form-error">Passwords should match</span>
                            </label>
                        </div>
                        @endif
                        <div class="small-12 columns text-center">
                            <button type="submit" id="submit_dets" name="submit" class="button button-primary" onclick="ga('send', 'event', 'button', 'click', 'verify-mobile');" > Verify </button>
                        </div>
                    </form>
                </div>
                
            </div>
          </div>
        </div>
    </div>
@endsection
@section('scripts')
<script type="text/javascript">
    function load_page_scripts(){
        Foundation.Abide.defaults.patterns['password'] = /^(.){6,}$/;
    }
</script>
@endsection