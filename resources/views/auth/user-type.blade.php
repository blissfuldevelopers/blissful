@section('title')
    Join Us
@overwrite
@section('content')
<!-- START OF MAIN SECTION -->
<div class="contentpg-main">
    <div class="row">
        <div class="small-12 medium-10 small-centered">
            <div class="wrapper">

                <div class="small-12 columns error-center header-title">
                    <h3> Welcome to the community!</h3>
                    <h5> First, tell us what you’re looking for.</h5>
                </div>
                <div class="small-12 columns join-section">
                    <div class="text-center small-12 medium-6 columns vendor-section">
                        <img class="join-img" src="/img/food-store.svg">
                        <p>I'm a service provider, who's looking to get more bookings and grow my business.</p>
                        <a class="button button-primary" href="{{ route('auth.vendor-register') }}">List Your Business</a>
                    </div>
                    <div class="line-divider hide-for-small-only"></div>
                    <div class="section-divider hide-for-small-only">OR</div>
                    <div class="text-center small-12 medium-6 columns user-section">
                        <img class="join-img" src="/img/user.svg">
                        <p>I'm a user who's searching for event service providers for my next event.</p>
                        <a class="button button-primary" href="/register">Find a vendor</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop