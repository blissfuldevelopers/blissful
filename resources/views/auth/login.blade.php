
@extends('page.home-layout')
@section('title')
    Register or Signup
@endsection

@section('header-content')
  <?php
    $header_class = 'contentpg-intro';
  ?>
@endsection
@section('content')
    <!-- main content  -->
    <div class="contentpg-main">
        <div class="row">
          <div class="small-12 columns">
            <div class="wrapper">
                {!! Form::open(['url' => route('auth.login-post'),'id'=>'form','data-abide','novalidate' ] ) !!}
                    <div class="small-12 medium-7 large-6 columns small-centered">
                        @include('includes.status')
                    </div>
                    
                    <div class="small-12 medium-7 large-6 columns small-centered">
                        <h2>Please sign in</h2>
                    </div>
                    
                    <div class="small-12 medium-6 large-5 columns small-centered">
                        <div>
                            {!! Form::email('email', null, [
                                'placeholder'                   => 'Email address',
                                'required',
                                'id'                            => 'inputEmail',
                                'data-live-validate'
                            ]) !!}
                            <span class="form-error">
                              Please input a valid email
                            </span>
                        </div>
                        <div>
                            {!! Form::password('password', [
                                'placeholder'                   => 'Password',
                                'required',
                                'id'                            => 'inputPassword',
                                'data-live-validate'
                            ]) !!}
                            <span class="form-error">
                              Please input a valid password
                            </span>
                        </div>
                        <div class="small-12">
                            <p>Remember me?</p>
                            <div class="switch login-switch small">
                              <input class="switch-input" id="yes-no" type="checkbox" name="remember"  checked value="1">
                              <label class="switch-paddle" for="yes-no">
                                <span class="show-for-sr">Remember me?</span>
                                <span class="switch-active" aria-hidden="true">Yes</span>
                                <span class="switch-inactive" aria-hidden="true">No</span>
                              </label>
                            </div>
                        </div>
                        <button class="button-primary login-button" onclick="ga('send', 'event', 'button', 'click', 'login');" type="submit">Sign in</button>
                        <p><a href="{{ route('auth.password') }}">Forgot password?</a> <a class="float-right" href="{{ route('auth.user-type') }}">or Register</a>
                        </p>

                        <p class="or-social">Or Use Social Login</p>
                        <a href="{{ route('social.redirect', ['provider' => 'facebook']) }}" class="button small small-12 facebook" onclick="ga('send', 'event', 'button', 'click', 'facebook-social-login');"> 
                            <i class="fi-social-facebook"></i>
                        </a>
                        <a href="{{ route('social.redirect', ['provider' => 'google']) }}" class="button small small-12 google" onclick="ga('send', 'event', 'button', 'click', 'google-social-login');">
                           <i class="fi-social-google-plus"></i> 
                        </a>
                    </div>
                {!! Form::close() !!}
            </div>
          </div>
        </div>
    </div>
@endsection
