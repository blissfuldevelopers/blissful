@extends('page.home-layout')
@section('title')
    Reset your password
@endsection

@section('header-content')
  <?php
    $header_class = 'contentpg-intro';
  ?>
@endsection
@section('content')
    <!-- main content  -->
    <div class="contentpg-main">
        <div class="row">
          <div class="small-12 columns">
            <div class="wrapper">
            <div class="small-12 medium-7 large-6 columns small-centered">
                {!! Form::open(['url' => route('auth.reset-post', ['token' => $token ]),'data-abide','novalidate'] ) !!}

                    <h2>Set New Password</h2>
                    @include('includes.errors')
                    <div>
                        {!! Form::password('password', [
                            'class' => 'form-control', 
                            'placeholder' => 'Password',  
                            'id' => 'inputPassword', 
                            'pattern'=>'.{6,}',
                            'autofocus',
                            'required', 
                            'data-live-validate'
                         ]) !!} 
                        <span class="form-error">
                          Invalid password, password should be 6 characters minimum
                        </span>
                    </div>
                    <div>
                        {!! Form::password('password_confirmation', [
                            'placeholder'       => 'Password confirmation',
                            'id'                => 'inputPasswordConfirmation',
                            'data-equalto'      => 'inputPassword',
                            'required',
                            'data-live-validate'
                         ]) !!} 
                        <span class="form-error">
                          Passwords should match.
                        </span>
                    </div>
                    <button class="button button-primary small small-12 " type="submit">Change</button>

                {!! Form::close() !!}
            </div>
                
            </div>
          </div>
        </div>
    </div>
@endsection
