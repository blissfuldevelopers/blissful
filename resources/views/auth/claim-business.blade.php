@section('title')
    Claim Business
@endsection
@section('content')
<main role="main" id="categories" class="lightblue claim-business">
    <div class="small-12 columns hm">
        <div class="row">
            <h1 class="breaker">Claim Business</h1>
            <div class="small-12 medium-5 columns small-centered">
                <div class="small-12 columns">
                    <div class="rimg-lg">
                        <img class="circle" src="https://s3.eu-central-1.amazonaws.com/blissful-ke/{{$user->profile->profilePic}}">
                    </div>
                </div>
                <div class="small-12 columns">
                    <h2 class="red">{{$user->first_name}}</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="small-12 medium-5 columns small-centered">
                @include('includes.status')
                @include('includes.errors')                        
            </div>
        </div>
        <div class="row">
            @if(!Session::has('message') || Session::get('status') != "success")
            <div class="small-10 medium-6 columns small-centered">
                <h3>Please confirm the email account associated with this account</h3>
                <form name="verifyMobile" method="POST" action="/password" >       
                    <div class="small-12 medium-8 small-centered columns">
                        {!! Form::text('email', null, [
                        'class' => 'form-password', 
                        'placeholder' => $email, 
                        'required', 
                        'type'  => 'email',
                        'autofocus',
                        'data-parsley-required-message' => 'please input a valid email address',
                        'data-parsley-trigger'          => 'change focusout',
                        ]) !!}
                        {!! Form::hidden('slug', $user->slug) !!}


                    </div>
                    <div class="small-12 columns">
                        <button type="submit" id="submit_dets" name="submit" class="tiny button alert" onclick="ga('send', 'event', 'button', 'click', 'claim-business');" > Send verification code</button>
                    </div>
                </form>
            </div>
            @endif            
        </div>
    </div>                                  
</main>
@endsection
@section('scripts')
<script type="text/javascript">
     $( document ).ready(function() {
        $('form[name=verifyMobile]').parsley({
            errorsWrapper: '<div></div>',
            errorTemplate: '<span style="color:#F75B4A"></span>'
        });
     });
    
</script>
@stop
