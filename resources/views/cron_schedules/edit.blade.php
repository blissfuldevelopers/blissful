<div class="row">
    <div class="col-sm-12">
        <h1 class="pull-left">Edit CronSchedule</h1>
    </div>
</div>

@include('core-templates::common.errors')

<div class="row">
    {!! Form::model($cronSchedule, ['route' => ['cron_schedules.update', $cronSchedule->id], 'method' => 'patch']) !!}

    @include('cron_schedules.fields')

    {!! Form::close() !!}
</div>
