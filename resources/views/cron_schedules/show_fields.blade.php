<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $cronSchedule->id !!}</p>
</div>

<!-- Cron Task Id Field -->
<div class="form-group">
    {!! Form::label('cron_task_id', 'Cron Task Id:') !!}
    <p>{!! $cronSchedule->cron_task_id !!}</p>
</div>

<!-- Repeat Start Field -->
<div class="form-group">
    {!! Form::label('repeat_start', 'Repeat Start:') !!}
    <p>{!! $cronSchedule->repeat_start !!}</p>
</div>

<!-- Repeat Stop Field -->
<div class="form-group">
    {!! Form::label('repeat_stop', 'Repeat Stop:') !!}
    <p>{!! $cronSchedule->repeat_stop !!}</p>
</div>

<!-- Cron Min Field -->
<div class="form-group">
    {!! Form::label('cron_min', 'Cron Min:') !!}
    <p>{!! $cronSchedule->cron_min !!}</p>
</div>

<!-- Cron Hour Field -->
<div class="form-group">
    {!! Form::label('cron_hour', 'Cron Hour:') !!}
    <p>{!! $cronSchedule->cron_hour !!}</p>
</div>

<!-- Cron Day Of Month Field -->
<div class="form-group">
    {!! Form::label('cron_day_of_month', 'Cron Day Of Month:') !!}
    <p>{!! $cronSchedule->cron_day_of_month !!}</p>
</div>

<!-- Cron Month Field -->
<div class="form-group">
    {!! Form::label('cron_month', 'Cron Month:') !!}
    <p>{!! $cronSchedule->cron_month !!}</p>
</div>

<!-- Cron Day Of Week Field -->
<div class="form-group">
    {!! Form::label('cron_day_of_week', 'Cron Day Of Week:') !!}
    <p>{!! $cronSchedule->cron_day_of_week !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $cronSchedule->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $cronSchedule->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $cronSchedule->updated_at !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $cronSchedule->created_by !!}</p>
</div>

