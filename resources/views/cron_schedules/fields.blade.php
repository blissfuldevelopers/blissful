<!-- Cron Task Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cron_task_id', 'Cron Task Id:') !!}
    {!! Form::number('cron_task_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Repeat Start Field -->
<div class="form-group col-sm-6">
    {!! Form::label('repeat_start', 'Repeat Start:') !!}
    {!! Form::date('repeat_start', null, ['class' => 'form-control']) !!}
</div>

<!-- Repeat Stop Field -->
<div class="form-group col-sm-6">
    {!! Form::label('repeat_stop', 'Repeat Stop:') !!}
    {!! Form::date('repeat_stop', null, ['class' => 'form-control']) !!}
</div>

<!-- Cron Min Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cron_min', 'Cron Min:') !!}
    {!! Form::text('cron_min', null, ['class' => 'form-control']) !!}
</div>

<!-- Cron Hour Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cron_hour', 'Cron Hour:') !!}
    {!! Form::text('cron_hour', null, ['class' => 'form-control']) !!}
</div>

<!-- Cron Day Of Month Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cron_day_of_month', 'Cron Day Of Month:') !!}
    {!! Form::text('cron_day_of_month', null, ['class' => 'form-control']) !!}
</div>

<!-- Cron Month Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cron_month', 'Cron Month:') !!}
    {!! Form::text('cron_month', null, ['class' => 'form-control']) !!}
</div>

<!-- Cron Day Of Week Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cron_day_of_week', 'Cron Day Of Week:') !!}
    {!! Form::text('cron_day_of_week', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::number('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('cron_schedules.index') !!}" class="btn btn-default">Cancel</a>
</div>
