<div class="row">
    <div class="col-sm-12">
        <h1 class="pull-left">Create New CronSchedule</h1>
    </div>
</div>

@include('core-templates::common.errors')

<div class="row">
    {!! Form::open(['route' => 'cron_schedules.store']) !!}

    @include('cron_schedules.fields')

    {!! Form::close() !!}
</div>
