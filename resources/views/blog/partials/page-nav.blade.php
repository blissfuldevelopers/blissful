{{-- Navigation --}}
<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
  <div class="container-fluid header_row">
    {{-- Brand and toggle get grouped for better mobile display --}}
    <div class="navbar-header page-scroll">
      <button type="button" class="navbar-toggle" data-toggle="collapse"
              data-target="#navbar-main">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ route('home')}}" style="width:170px;cursor:pointer;"><img class="img-responsive" src="/img/blissful-logo.png" alt="Blissful" style="cursor:pointer;" /></a>
    </div>

    {{-- Collect the nav links, forms, and other content for toggling --}}
    <div class="collapse navbar-collapse navbar-responsive-collapse" id="navbar-main">
            <ul class="nav navbar-nav navbar-left">
              <li>
                <form action="/search" method="GET" enctype="multipart/form-data">
                  <div class="warning">
                    <input name="find" id="find" type="text" class="@if(Session::has('searchErrors')) red_border @endif" placeholder="@if(Session::has('searchErrors'))Search not found @else Find a Vendor @endif" />
                  </div>
                </form>
              </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                @if(!Auth::check())

                    <li><a class="btn btn-sm white green_background " href="{{ route('auth.vendor-register') }}">List Your Business</a></li>
                    <li><a href="/">Help</a></li>
                    <li><a href="/login">Log In</a></li>

                @else
                <?php
                  $count = Auth::user()->newMessagesCount();
                  $cssClass = $count == 0 ? 'hidden' : '';
                  ?>
                <li class="dropdown">
                    <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle" data-toggle="dropdown"><img src="/{{Auth::user()->profile->profilePic}}" class="img-circle special-img" style="width:50px;height:50px;"> <span class="grey">{{Auth::user()->first_name}}</span> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a id="nav-text" href="@if( Auth::user()->hasRole('administrator'))
                        /admin
                        @elseif( Auth::user()->hasRole('vendor'))
                        /vndor
                        @elseif( Auth::user()->hasRole('user'))
                        /user
                        @endif"><i class="fa fa-home sm"></i> Dashboard</a></li>
                        <li><a href="@if( Auth::user()->hasRole('vendor'))
                            {{route('users.edit',array(Auth::user()->profile->user_id))}}
                            @else 
                            {{ url('profile/edit/'.Auth::user()->profile->user_id) }}
                            @endif
                            "><i class="fa falist fa-wrench"></i> Profile</a>
                        </li>
                        <li><a href="@if( Auth::user()->hasRole('administrator'))
                        /admin#inbox
                        @elseif( Auth::user()->hasRole('vendor'))
                        /vndor#inbox
                        @elseif( Auth::user()->hasRole('user'))
                        /user#inbox
                        @endif"><i class="fa falist fa-inbox" ></i> Inbox <span class="badge label-danger {{$cssClass}}">{{$count}}</span></a>
                        </li>
                        <li><a href="{{ route('authenticated.logout') }}"><i class="fa falist fa-power-off" style="color:red;"></i> Logout</a></li>
                    </ul>
                </li>
                @endif
            </ul>
    </div>
  </div>
</nav>