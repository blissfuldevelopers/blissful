<hr>
<div class="row">
  <div class="small-12 columns">
    @include('blog.partials.disqus')
  </div>
</div>
<hr>

<!-- START OF FOOTER SECTION -->
<footer>
    <div class="tft">
        <div class="row">
            <div class="small-12 medium-6 large-6 columns">
                <div class="row">
                    <div class="small-6 columns">
                        <h5>COMPANY</h5>
                        <ul>
                            <li><a href="/about">About Us</a></li>
                            <li><a href="/contact">Contact Us</a></li>
                            <li><a href="{{ route('blog') }}">Blog</a></li>
                        </ul>
                    </div>
                    <div class="small-6 columns">
                        <h5>PARTNERS</h5>
                        <ul>
                            <li><a href="{{ route('auth.user-type') }}">Join Us</a></li>
                            <li><a href="">Advertise</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="small-12 medium-6 large-6 columns">
                <div class="row">
                    <div class="small-12 columns">
                        <ul class="socials">
                            <li class="f"><a href="https://www.facebook.com/blissfulKE"></a></li>
                            <li class="t"><a href="https://twitter.com/blissfulKE"></a></li>
                            <li class="g"><a href="https://plus.google.com/112646574144413915809/posts"></a></li>
                            <li class="p"><a href="http://www.pinterest.com/blissfulKE"></a></li>
                        </ul>
                    </div>
                    <div class="small-12 columns">
                        <ul class="right">
                            <li><a href="/privacy-policy">Privacy Policy</a></li>
                            <li><a href="/terms-and-conditions">Terms and conditions</a></li>
                            <li><span class="fi-telephone tiny white"></span> <span
                                        class="white ns tiny"> 0775 987 555</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bft">
        <div class="row">
            <div class="small-12 columns">
                <span>© <?php echo date("Y"); ?> Blissful</span>
            </div>
        </div>
    </div>
</footer>