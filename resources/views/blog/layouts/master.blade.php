<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ $meta_description }}">
    <meta name="author" content="{{ config('blog.author') }}">
    @yield('metas')
    <link rel="icon" type="image/png" href="/img/dashboard/favicon.ico"/>
    <title>{{ $title or config('blog.title') }} | Blissful</title>
    <!-- google ads --> 
    <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
    <script>
      var googletag = googletag || {};
      googletag.cmd = googletag.cmd || [];
    </script>

    <script>
      googletag.cmd.push(function() {
        googletag.defineSlot('/189532048/leaderboard_top_home', [728, 90], 'div-gpt-ad-1474450779427-0').addService(googletag.pubads());
        googletag.defineSlot('/189532048/Square_1', [300, 250], 'div-gpt-ad-1474450779427-1').addService(googletag.pubads());
        googletag.defineSlot('/189532048/Half_Page_1', [300, 600], 'div-gpt-ad-1474450779427-2').addService(googletag.pubads());
        googletag.defineSlot('/189532048/Mobile_Leaderboard_Top', [320, 50], 'div-gpt-ad-1474450779427-3').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
      });
    </script>
    
    <link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="/assets/blog/css/foundation.css" />
    <link rel="stylesheet" href="/assets/blog/css/app.css" />
    <link rel="stylesheet" href="/fonts/foundation/foundation-icons.css"/>
    <link rel="stylesheet" type="text/css" href="/css/jquery.materialripple.css">
    @yield('styles')
  </head>
  <body class="blissful-blog">

  <!-- START OF HEADER -->
  <header>
    <!-- logo section -->
    <section class="top-header">
      <div class="row">
        <div class="small-6 columns">
          <h1>
          <a href="/">
            <img src="/assets/blog/img/blissful-shop.jpg" alt="Blissful Logo">
          </a>      
          </h1>
        </div>
        <div class="small-6 columns">
          <form action="/search" method="GET" enctype="multipart/form-data">
            <input name="find" id="find" type="text"
                                           class="@if(Session::has('searchErrors')) red_border @endif"
                                           placeholder="@if(Session::has('searchErrors'))Search not found @else Find a Vendor @endif"/>
          </form>
        </div>
      </div>
    </section>
    <!-- main navigation -->
    
  </header>
  <!-- END OF HEADER -->
  @yield('page-header')
  @yield('content')

  @include('blog.partials.page-footer')
  
  <!-- END OF BODY -->

    <script src="/assets/blog/js/vendor/jquery.min.js"></script>
    <script src="/assets/blog/js/vendor/what-input.min.js"></script>
    <script src="/assets/blog/js/foundation.min.js"></script>
    <script src="/assets/blog/js/app.js"></script>
    <script type="text/javascript" src="/js/jquery.materialripple.js"></script>
    <script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-50669449-1', 'auto');
    ga('send', 'pageview');

</script>
<!--Start of Crisp Live Chat Script-->
<script type="text/javascript">
    $(window).load(function () {
        $(this).delay(8000).queue(function () {
            CRISP_WEBSITE_ID = "aeb94ef1-11bf-4618-85b7-dce7f0ee2b92";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.im/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();
        });
    });

</script>
<script type="text/javascript">
    $(".blog-card").click(function () {
        window.location = $(this).find("a.main-ref").attr("href");
        return false;
    });
    $('.blog-card').materialripple();

    $('img').bind('error',function(ev){
        //error has been thrown
        var url = $(this).attr('src');
        var filename = url.match(/.*\/(.*)$/)[1];
        console.log(filename);
        $(this).attr('src','https://s3.eu-central-1.amazonaws.com/blissful-ke/gallery/images/'+filename);
    });
</script>
<!--End of Crisp Live Chat Script-->
  </body>
</html>


