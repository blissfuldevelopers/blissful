@extends('blog.layouts.master', [
  'title' => $post->title,
  'meta_description' => $post->meta_description ?: config('blog.description'),
])
@section('metas')
    <meta property="og:url" content="{{request()->fullUrl()}}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{$post->title}} | Blissful"/>
    <meta property="og:description" content="{{$post->subtitle}}"/>
    <meta property="og:image" content="{{page_image($post->page_image)}}"/>
    <meta property="fb:app_id" content="1615380565398173"/>
@endsection
@section('content')
    <!-- START OF BODY -->
    <main aria-role="main" class="blog-article">
        <!-- START OF SHOP ITEM HEADER SECTION -->
        <section class="masthead">
            <div class="row">
                <div class="small-12 columns">
                    <div class="hide-for-small-only">
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Blissful_responsive -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-2847929148248604"
                             data-ad-slot="1315972373"
                             data-ad-format="auto"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                    <div class="show-for-small-only">
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- mobile_horizontal banner -->
                        <ins class="adsbygoogle"
                             style="display:inline-block;width:320px;height:100px"
                             data-ad-client="ca-pub-2847929148248604"
                             data-ad-slot="2792705573"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                    <div class="mainft">
                        <img src="{{ page_image($post->page_image) }}">
                    </div>
                </div>
            </div>
        </section>
        <!-- END OF SHOP ITEM HEADER SECTION -->
        <!-- START OF SHOP CART SECTION -->
        <section class="article">
            <div class="row">
                <!-- Item Image -->
                <div class="small-12 medium-9 columns">
                    <div class="blog-article-wrap">
                        <div class="wrap title green">
                            <h6>
                                @if ($post->categories->count())
                                    {!! join(', ', $post->categoryLinks()) !!}
                                @endif
                            </h6>
                            <h2>{{ $post->title }}</h2>
                            <h4>{{ $post->subtitle }}</h4>
                            <h5>Posted on {{ $post->published_at->format('F j, Y') }}</h5>
                            @include('pages.share', [
                                            'url' => request()->fullUrl(),
                                            'description' => $post->subtitle,
                                            'image' => page_image($post->page_image)
                                        ])
                        </div>
                        {!! $post->content_html !!}
                    </div>
                    <!-- share section -->
                    <span class="share">
            {{--@include('pages.share', [--}}
                        {{--'url' => request()->fullUrl(),--}}
                        {{--'description' => $post->subtitle,--}}
                        {{--'image' => page_image($post->page_image)--}}
                        {{--])--}}
          </span>
                    {{-- The Pager --}}
                    <div class="row">
                        <ul class="pager small-10 medium-6 small-centered columns">
                            @if ($category && $category->reverse_direction)
                                @if ($post->olderPost($category))
                                    <li class="previous  small-6 columns">
                                        <a href="{!! $post->olderPost($category)->url($category) !!}"
                                           class="button secondary round">
                                            <i class="fi-arrow-left large"></i>
                                            Previous {{ $category->category }} Post
                                        </a>
                                    </li>
                                @endif
                                @if ($post->newerPost($category))
                                    <li class="next small-6 columns">
                                        <a href="{!! $post->newerPost($category)->url($category) !!}"
                                           class="button secondary round">
                                            Next {{ $category->category }} Post
                                            <i class="fi-arrow-right large"></i>
                                        </a>
                                    </li>
                                @endif
                            @else
                                @if ($post->newerPost($category))
                                    <li class="previous small-6 columns">
                                        <a href="{!! $post->newerPost($category)->url($category) !!}"
                                           class="button secondary round">
                                            <i class="fi-arrow-left large"></i>
                                            Newer {{ $category ? $category->category : '' }} Post
                                        </a>
                                    </li>
                                @endif
                                @if ($post->olderPost($category))
                                    <li class="next small-6 columns">
                                        <a href="{!! $post->olderPost($category)->url($category) !!}"
                                           class="button secondary round">
                                            Older {{ $category ? $category->category : '' }} Post
                                            <i class="fi-arrow-right large"></i>
                                        </a>
                                    </li>
                                @endif
                            @endif
                        </ul>
                    </div>
                </div>
                <!-- sidebar section -->
                <div class="medium-3 columns hide-for-small-only sidebar">
                    @if(@$related)
                        <h5>Related Stories</h5>
                        <ul>
                            @foreach($related as $item)
                                <li class="blog-card">
                                    <img src="{{page_image($item->page_image)}}">
                                    <h5><a class="main-ref" href="{{ $item->url($category) }}">{{ $item->title }}</a>
                                    </h5>
                                </li>
                            @endforeach
                            <li class="blog-card">
                                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <!-- large_rectangle_2 -->
                                <ins class="adsbygoogle"
                                     style="display:inline-block;width:270px;height:280px"
                                     data-ad-client="ca-pub-2847929148248604"
                                     data-ad-slot="1176371574"></ins>
                                <script>
                                    (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
                            </li>
                        </ul>
                    @endif
                </div>
                <!-- sidebar for small screens -->
                <div class="small-11 small-centered columns show-for-small-only sidebar">
                    @if(@$related)
                        <h5>Related Stories</h5>
                        <ul>
                            @foreach($related as $item)
                                <li>
                                    <img src="{{page_image($item->page_image)}}">
                                    <h3><a class="main-ref" href="{{ $item->url($category) }}">{{ $item->title }}</a>
                                    </h3>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>

        </section>

        <!-- END OF BLOG ARTICLE SECTION -->

    </main>
    <!-- END OF BODY -->
@endsection