@extends('blog.layouts.master')

<!-- START OF HOMEPG MASTHEAD -->
@section('page-header')

  <section class="masthead">
    <div class="row">
      <div class="small-12 columns">
        <div class="mainft">
          <img src="{{ page_image($posts->first()->page_image) }}">
          <div class="wrap title">
            <h6>
              @if ($posts->first()->categories->count())
                {!! join(', ', $posts->first()->categoryLinks()) !!}
              @endif
            </h6>
            <h2><a href="{{ $posts->first()->url($category) }}">{{ $posts->first()->title }}</a></h2>
            @if ($posts->first()->subtitle)
            <h4>{{$posts->first()->subtitle}}</h4>
            @endif
            <h5>Posted on {{ $posts->first()->published_at->format('F j, Y') }}</h5>
          </div>
          {{--<span class="share">--}}
            {{--<span class="icon ion-forward"></span>--}}
            {{--<span class="txt">Share</span>--}}
          {{--</span>--}}
        </div>
      </div>
    </div>
  </section>
  <!-- END OF HOMEPG MASTHEAD -->
@endsection
  <!-- START OF BODY -->

    
@section('content')
  <main aria-role="main">
    <div class="row">
      {{--<div class="small-10 medium-8 small-centered columns">--}}
        {{--<a href="http://www.superbtent.com/">--}}
          {{--<img class="hide-for-small-only" src="/img/SUPERB1-728X90.jpg">--}}
          {{--<img class="show-for-small-only" src="/img/SUPERB-320x100.jpg">--}}
        {{--</a>--}}
      {{--</div>--}}
      <div class="small-12 columns">
        <h3>Latest Posts</h3>
        <ul class="blogs-list">      
          {{-- The Posts --}}
          @foreach ($posts as $post)
          <li class="blog-card">
            <img src="{{ page_image($post->page_image)}}">
            <div class="wrap title green">
              <h6>
              @if ($post->categories->count())
                {!! join(', ', $post->categoryLinks()) !!}
              @endif 
              </h6>
              <h2> <a class="main-ref" href="{{ $post->url($category) }}">{{ $post->title }}</a></h2>
              @if ($post->subtitle)
              <h4>{{$post->subtitle}}</h4>
              @endif
              <h5>Posted on {{ $post->published_at->format('F j, Y') }}</h5>
            </div>
            {{--<span class="share">--}}
              {{--<span class="icon ion-forward"></span>--}}
              {{--<span class="txt">Share</span>--}}
            {{--</span>--}}
          </li>
          @endforeach
      </div>
    </div>
    <div class="row">
         {{-- The Pager --}}
        <ul class="pager small-10 medium-6 small-centered columns">

          {{-- Reverse direction --}}
          @if ($reverse_direction)

            @if ($posts->currentPage() > 1)
              <li class="previous small-6 columns">
                <a href="{!! $posts->url($posts->currentPage() - 1) !!}" class="button secondary round">                 
                  <i class="fi-arrow-left large"></i>
                  Previous {{ $category->category }} Posts
                </a>
              </li>
            @endif

            @if ($posts->hasMorePages())
              <li class="next small-6 columns">
                <a href="{!! $posts->nextPageUrl() !!}"  class="button secondary round">
                  Next {{ $category->category }} Posts
                  <i class="fi-arrow-right large"></i>
                </a>
              </li>
            @endif

            
          @else
            
            @if ($posts->hasMorePages())
              <li class="next small-6 columns">
                <a href="{!! $posts->nextPageUrl() !!}" class="button secondary round">
                  <i class="fi-arrow-left large"></i>
                  Older {{ $category ? $category->category : '' }} Posts
                  
                </a>
              </li>
            @endif

            @if ($posts->currentPage() > 1)
              <li class="previous small-6 columns">
                <a href="{!! $posts->url($posts->currentPage() - 1) !!}" class="button secondary round">
                  Newer {{ $category ? $category->category : '' }} Posts
                  <i class="fi-arrow-right large"></i>
                </a>
              </li>
            @endif
          @endif
        </ul>
    </div>
    <div class="row">
      {{--<div class="small-10 medium-8 small-centered columns breaker">--}}
          {{--<a href="http://www.superbtent.com/">--}}
            {{--<img class="hide-for-small-only" src="/img/SUPERB2-728X90.jpg">--}}
            {{--<img class="show-for-small-only" src="/img/SUPERB-320x100.jpg">--}}
          {{--</a>--}}
      {{--</div>--}}
    </div>
  </main>
@stop