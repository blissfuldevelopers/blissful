$.urlParam = function(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    } else {
        return results[1] || 0;
    }
}
var option = $.urlParam('date');
if (option) {

    option = replaceAll(option, '%2F', '/');

    function replaceAll(str, find, replace) {
        return str.replace(new RegExp(find, 'g'), replace);
    }
    $('.date-pick').val(option);

}
$('.date-pick').datePicker({
    createButton: false,
    clickInput: true
});
$('.gallery ul').slick({
    lazyLoad: 'ondemand',
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    variableWidth: true
});
$('.starrr').starrr();

$('.starrr').on('starrr:change', function(e, value) {
    $('.ratings-hidden').val(value);
});


$('button.submit-review').click(function(e) {

    var form = $('form#review-form');
    e.preventDefault();
    var rating = form.find('input.ratings-hidden').val();
    rating = rating.replace(/\s/g, '');

    var textarea = form.find('textarea');
    var comments = textarea.val();
    comments = comments.replace(/\s/g, '');

    var rating_result = check_rating(rating);
    var comments_result = check_comments(comments, textarea);

    if (rating_result === false || comments_result === false) {
        return false;
    } else {
        form.submit();
    }

});

$("form.booking-form").bind("submit", function(e) {
    e.preventDefault();
    return false;
});
$("form.booking-form").bind("formvalid.zf.abide", function(e, target) {

    var btn_container = $('#bookingModal form .button');
    var btn = $("#bookingModal form .button span");
    var loading_class = 'loader loader--double';

    btn.html('').addClass(loading_class);
    btn_container.off();

    var param = $("form.booking-form").serialize();

    $.ajax({
        type: "POST",
        url: "/bookings/save",
        data: param,
        success: function(data) {
            $("#bookingModal form").remove();
            $('#bookingModal').append(data);
            btn.removeClass(loading_class);
            btn_container.on();
        },
        error: function(data) {
            $('<p class="error"> Error while sending request</p>').insertAfter('#bookingModal p.lead');
            btn.removeClass(loading_class);
            btn.html('Place Booking');
            btn_container.on();
        }
    });
});

function check_rating(rating) {

    if (rating == '' || rating == null) {
        var star = $('.starrr');
        $('<p class="error rating-error">Please select your rating.</p>').insertAfter(star);
        return false;
    } else {
        $('.rating-error').remove();
    }

}

function check_comments(comments, textarea) {

    if (comments == '' || comments == null) {
        $('<p class="error comments-error">Please write a review.</p>').insertAfter(textarea);
        return false;
    } else {
        $('.comments-error').remove();
    }

}
