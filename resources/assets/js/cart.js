function notification(type, msg, dely) {
    // 'warning', 'info', 'success', 'error'
    Lobibox.notify(type, {
        size: 'normal', // normal, mini, large
        icon: false,
        closable: true,
        sound: false,
        position: "top right",
        delay: dely || 5000,
        msg: msg
    });


}
var instance = $('form.order-details').parsley({
    errorsWrapper: '<div></div>',
    errorTemplate: ' <small class="error"></small>'
});

function removeCartFn(id) {
    if (confirm("Are you sure want to remove this product from cart")) {
        $.ajax({
            url: "/shop/cart/" + id,
            type: 'DELETE',
            success: function(data) {
                if (data.flag) {
                    notification('success', 'Item removed from cart successfully');
                    location.reload();
                } else {
                    notification('error', 'Error while removing item from cart');
                }
            },
            error: function(e) {
                notification('error', 'Error while removing item from cart');
            }
        })
    }
}

function updateCartFn(id, val) {
    $.ajax({
        url: "/shop/cart/" + id,
        type: 'PUT',
        data: { qty: val },
        success: function(data) {
            if (data.flag) {
                notification('success', 'Cart Updated Successfully');
                location.reload();
            } else {
                notification('error', 'Error while updating cart');
            }
        },
        error: function(e) {
            notification('error', 'Error while updating cart');
        }
    })
}
$('form.order-details').submit(function(e) {
    e.preventDefault();
    $('div.details-card  button').attr('disabled', 'disabled');
    $('.loading').show();
    if (instance.isValid()) {
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            success: function(data) {
                $('div.details-card  button').removeAttr('disabled', 'disabled');
                $('.loading').hide();
                $('form.order-details').parent().html(data);

            },
            error: function(e) {
                notification('error', 'Error while saving details');
                $('div.details-card  button').removeAttr('disabled', 'disabled');
                $('.loading').hide();
            }
        });
    }
});
