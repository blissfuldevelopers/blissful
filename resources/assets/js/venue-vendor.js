$('#pac-input').focusout(function(e) {
    e.preventDefault();

    if (!$(this).attr('data-city')) {
        $(this).val('');
        $(this).attr("placeholder", "Please select a location from the suggestions");
    }
});
$('.typ').change(function() {
    if ($(this).val()) {
        var url = "/venues/search";
        load_results(url);
    }

});
$(document).on('mouseup', '.slider-handle', function() {
    var url = "/venues/search";
    load_results(url);

});

//start map
function initMap() {
    var options = {
        componentRestrictions: { country: "ke" }
    };


    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.Autocomplete(input, options);
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    google.maps.event.addListener(searchBox, 'place_changed', function() {
        var place = searchBox.getPlace();

        if (place.length == 0) {
            input.value = "";
            console.log("Returned place contains no ");
            return;
        }

        var address = "";
        var city_name = "";
        var latlng = "";
        var latitude;
        var longitude;

        if (!place.geometry) {
            input.value = "";
            console.log("Returned place contains no geometry");
            return;
        }

        latlng = place.geometry.location;

        //fetch address
        var geocoder = new google.maps.Geocoder;

        geocoder.geocode({ 'latLng': latlng }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                // console.log(results)
                if (results[1]) {
                    //formatted address
                    address = results[0].formatted_address;
                    latitude = results[0].geometry.location.lat();
                    longitude = results[0].geometry.location.lng();
                    //find country name
                    for (var i = 0; i < results[0].address_components.length; i++) {
                        for (var b = 0; b < results[0].address_components[i].types.length; b++) {

                            //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                            if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                                //this is the object you are looking for
                                city = results[0].address_components[i];
                                break;
                            }
                        }
                    }
                    //set map
                    var map = new google.maps.Map(document.getElementById('search-map'), {
                        center: latlng,
                        zoom: 11, //zoom level, 0 = earth view to higher value
                        clickableIcons: false
                    });
                    // Create marker 
                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        draggable: false,
                        animation: google.maps.Animation.DROP,
                        title: "Search Location",
                    });

                    // Add circle overlay and bind to marker
                    var circle = new google.maps.Circle({
                        map: map,
                        radius: 15000, // 10 miles in metres
                        fillColor: '#5eb8c1',
                        strokeColor: "#FFF",
                        strokeWeight: 0
                    });
                    circle.bindTo('center', marker, 'position');

                    //city data
                    city_name = city.long_name;
                    input.dataset.city = city_name;
                    input.dataset.lat = latitude;
                    input.dataset.lng = longitude;
                    var url = "/venues/search";
                    load_results(url);

                } else {
                    console.log("No results found")
                }
            } else {
                console.log("Geocoder failed due to: " + status)
            }
            console.log(city_name)
        });
    });
}

$('.dat').datePicker({
    createButton: false,
    clickInput: true
});
$('.venue-search').click(function(e) {
    e.preventDefault();
    var url = "/venues/search";
    load_results(url);


});

$(document).on('click', '.venue-search-results .pagination a', function(e) {
    e.preventDefault();
    var url = $(this).attr('href');
    load_results(url);
});

function load_results(url) {


    var btn_container = $('.venue-search');
    btn_container.text('Loading Results');

    var search_container = $('.vencatpg-list');
    $('form.filters').append('<span class="section-loader"></span><div class="section-loader-area"></div>');
    $('.vencatpg-list').append('<span class="section-loader"></span><div class="section-loader-area"></div>');

    var loader = $('.section-loader');
    var loader_area = $('.section-loader-area');
    var lat = $('#pac-input').attr('data-lat');
    var lng = $('#pac-input').attr('data-lng');

    var param = $("form.filters").serialize();
    if (lat && lng) {
        param = param + '&lat=' + lat + '&lng=' + lng;
    }


    $.ajax({
        type: "GET",
        url: url,
        data: param,
        success: function(data) {
            $(search_container).html(data);
            loader.remove();
            loader_area.remove();
            btn_container.text('Find a Venue');
            $('p.error').remove();
        },
        error: function(data) {
            $('<p class="error text-center"> Error while sending request</p>').insertAfter("form.filters");
            loader.remove();
            loader_area.remove();
            btn_container.text('Find a Venue');
        }
    });
}
