$(document).ready(function() {
    $('.lightslider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true
    });
});

function cartCount() {
    $.ajax({
        url: '/shop/cart-count',
        type: 'GET',
        success: function(data) {
            console.log(data);
            if (data > 0) {
                $(".showCart").show();
            }
            $("#CartCount").text(data);
        },
        error: function(e) {}
    });
}

function notification(type, msg, dely) {
    // 'warning', 'info', 'success', 'error'
    Lobibox.notify(type, {
        size: 'normal', // normal, mini, large
        icon: false,
        closable: true,
        sound: false,
        position: "top right",
        delay: dely || 5000,
        msg: msg
    });


}

function addToCartFn(id) {
    $.ajax({
        url: '/shop/cart/' + id,
        type: 'GET',
        success: function(data) {
            if (data.flag) {
                notification('success', 'Item added to cart successfully');
                window.location.href = '/shop/cart';
                cartCount();
            } else {
                notification('error', 'Error while adding item to cart');
            }
        },
        error: function(e) {
            console.log(e)
            notification('error', 'Error while adding item to cart');
        }
    });
}

function showForm() {
    $('.contact-form').show(
        function() {
            $('.contact-form').slideDown("slow");
            $('.enquire-more').slideUp(
                function() {
                    $('.enquire-more').hide();
                });
        });
}
//submit for contact form
$('.contact-form').submit(function(e) {
    e.preventDefault();

    var form = $(this);


    $('.loading').show();

    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: form.serialize(),
        success: function(data) {
            form.html(data);
            $('.loading').hide();
        },
        error: function(e) {
            console.log(e)
            notification('error', 'An error occoured, please try again');
            $('.loading').hide();
        }
    });
});

function saveSubscription() {
    $.ajax({
        url: '/shop/cart/' + id,
        type: 'GET',
        success: function(data) {
            if (data.flag) {
                notification('success', 'Item added to cart successfully');
                window.location.href = '/shop/cart';
                cartCount();
            } else {
                notification('error', 'Error while adding item to cart');
            }
        },
        error: function(e) {
            notification('error', 'Error while adding item to cart');
        }
    });
}
