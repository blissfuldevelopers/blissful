(function($) {

    $(window).load(function() {

        if (window.location.pathname.split('/')[1] == 'vendor') {
            var bStyle = $('.c-header > .row:first > div > .row:first').css('borderLeft');

            $('.cat-li li .s-wrap').css('borderLeft', bStyle);
        }

        $('.loader').remove();

        $('.ld').removeClass('ld');

    });

    $(document).ready(function() {
        // initialize foundation logic
        $(document).foundation();


        function loadlink() {
            var bla = $('#right').val();

            $('#notifs').load("/" + bla + " #notifs > *");

        }

        loadlink(); // This will run on page load

        setInterval(function() {
            loadlink() // this will run after every 5 seconds
        }, 1000);

        $(function() {
            $('.ripple').materialripple();
        });

        if (window.location.pathname == '/') {
            // auto size homepage slideshow
            var hSlides = $('section.slideshow'),
                docH = jQuery(document).height();

            function resize() {
                wW = $(window).width(), //width of window
                    wH = parseInt($(window).height()) - 70; //edited height of the window (minus header height)

                hSlides.css({
                    width: wW,
                    height: wH
                });
            }
            resize();

            $(window).resize(function() {
                resize();
            });
        }

        // floating contact button on vendor pages
        if (window.location.pathname.split('/')[1] == 'vendors') {
            if (jQuery(window).width() > 600) {
                $(window).scroll(function() {
                    if ($(".v-contact").offset().top < 547) {
                        //get the left position of the parent container
                        var rowL = parseInt($('.vendor-header > .row').offset().left) + 30;

                        $(".v-contact").addClass("bfxd").css('right', rowL);
                        // console.log('addfixed');
                    } else {
                        $(".v-contact").removeClass("bfxd");
                        // console.log('removefixed');
                    }
                });
            }

        }

        // show hide filters on category pages on small screens
        if (window.location.pathname.split('/')[2] == 'list') {

            var filterH = $('.cat-filters');

            $('.filter-icon a').click(function() {
                $(this).parent().parent().parent().parent().css('height', 'initial');
                console.log('clicked');
            });
        }

        $("#scrollr").click(function() {
            var offset = 10; //Offset of 20px

            $('html, body').animate({
                scrollTop: $("#shop-main").offset().top + offset
            }, 1600);
        });

        //hack for adjusting header height
        function header_height_adjust() {
            var header_height = $('.blissful-shop header').height() + 15;
            $('.blissful-shop .title-header').css('margin-top', header_height + "px");
        }

        header_height_adjust();
        $(window).resize(function() {
            header_height_adjust();
        });

        function notification(type, msg, dely) {
            // 'warning', 'info', 'success', 'error'
            Lobibox.notify(type, {
                size: 'normal', // normal, mini, large
                icon: false,
                closable: true,
                sound: false,
                position: "top right",
                delay: dely || 5000,
                msg: msg
            });


        }

        function cartCount() {
            $.ajax({
                url: '/shop/cart-count',
                type: 'GET',
                success: function(data) {
                    if (data > 0) {
                        $(".showCart").show();
                    }
                    $("#CartCount").text(data);
                },
                error: function(e) {}
            });
        }

        cartCount();

        objectFit.polyfill({
            selector: 'img', // this can be any CSS selector
            fittype: 'cover', // either contain, cover, fill or none
            disableCrossDomain: 'true' // either 'true' or 'false' to not parse external CSS files.
        });

        $(function() {
            var cc = 0;
            $('#locate').click(function() {

                cc++;
                if (cc == 2) {
                    $(this).change();
                    cc = 0;
                }
            }).change(function() {

                if ($('#locs').length) {
                    $('#locs').submit();
                } else {
                    var url = $('#locate').val();
                    window.location.href = url;
                }


            });
        });
        $(function() {
            var filterH = $('.cat-filters');

            $('.filter-icon a').click(function() {
                $(this).parent().parent().parent().parent().css('height', 'initial');
            });
        });

    });

})(jQuery);
