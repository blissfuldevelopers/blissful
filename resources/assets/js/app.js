(function($) {

    $(document).foundation();

    function cartCount() {
        $.ajax({
            url: '/shop/cart-count',
            type: 'GET',
            success: function(data) {
                if (data > 0) {
                    $(".show-cart").show();
                }
                $("#cart-count").text(data);
            },
            error: function(e) {}
        });
    }

    cartCount();

})(jQuery);
// side nav cntrls

var menu = document.getElementById("mobile-menu");
var menu_bg = document.getElementById("mobile-bg");
var open = document.getElementById("mobile-menu-open");
var close = document.getElementById("mobile-menu-close");

open.addEventListener("click", function() {
    menu.classList.add('opened');
    menu_bg.classList.add('opened');
});
close.addEventListener("click", function() {
    menu.classList.remove('opened');
    menu_bg.classList.remove('opened');
});
menu_bg.addEventListener("click", function() {
    menu.classList.remove('opened');
    menu_bg.classList.remove('opened');
});
