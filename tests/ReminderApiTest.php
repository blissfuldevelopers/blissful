<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReminderApiTest extends TestCase
{
    use MakeReminderTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateReminder()
    {
        $reminder = $this->fakeReminderData();
        $this->json('POST', '/api/v1/reminders', $reminder);

        $this->assertApiResponse($reminder);
    }

    /**
     * @test
     */
    public function testReadReminder()
    {
        $reminder = $this->makeReminder();
        $this->json('GET', '/api/v1/reminders/'.$reminder->id);

        $this->assertApiResponse($reminder->toArray());
    }

    /**
     * @test
     */
    public function testUpdateReminder()
    {
        $reminder = $this->makeReminder();
        $editedReminder = $this->fakeReminderData();

        $this->json('PUT', '/api/v1/reminders/'.$reminder->id, $editedReminder);

        $this->assertApiResponse($editedReminder);
    }

    /**
     * @test
     */
    public function testDeleteReminder()
    {
        $reminder = $this->makeReminder();
        $this->json('DELETE', '/api/v1/reminders/'.$reminder->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/reminders/'.$reminder->id);

        $this->assertResponseStatus(404);
    }
}
