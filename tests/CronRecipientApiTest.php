<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CronRecipientApiTest extends TestCase
{
    use MakeCronRecipientTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCronRecipient()
    {
        $cronRecipient = $this->fakeCronRecipientData();
        $this->json('POST', '/api/v1/cronRecipients', $cronRecipient);

        $this->assertApiResponse($cronRecipient);
    }

    /**
     * @test
     */
    public function testReadCronRecipient()
    {
        $cronRecipient = $this->makeCronRecipient();
        $this->json('GET', '/api/v1/cronRecipients/'.$cronRecipient->id);

        $this->assertApiResponse($cronRecipient->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCronRecipient()
    {
        $cronRecipient = $this->makeCronRecipient();
        $editedCronRecipient = $this->fakeCronRecipientData();

        $this->json('PUT', '/api/v1/cronRecipients/'.$cronRecipient->id, $editedCronRecipient);

        $this->assertApiResponse($editedCronRecipient);
    }

    /**
     * @test
     */
    public function testDeleteCronRecipient()
    {
        $cronRecipient = $this->makeCronRecipient();
        $this->json('DELETE', '/api/v1/cronRecipients/'.$cronRecipient->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/cronRecipients/'.$cronRecipient->id);

        $this->assertResponseStatus(404);
    }
}
