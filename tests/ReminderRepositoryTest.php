<?php

use App\Models\Reminder;
use App\Repositories\ReminderRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReminderRepositoryTest extends TestCase
{
    use MakeReminderTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ReminderRepository
     */
    protected $reminderRepo;

    public function setUp()
    {
        parent::setUp();
        $this->reminderRepo = App::make(ReminderRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateReminder()
    {
        $reminder = $this->fakeReminderData();
        $createdReminder = $this->reminderRepo->create($reminder);
        $createdReminder = $createdReminder->toArray();
        $this->assertArrayHasKey('id', $createdReminder);
        $this->assertNotNull($createdReminder['id'], 'Created Reminder must have id specified');
        $this->assertNotNull(Reminder::find($createdReminder['id']), 'Reminder with given id must be in DB');
        $this->assertModelData($reminder, $createdReminder);
    }

    /**
     * @test read
     */
    public function testReadReminder()
    {
        $reminder = $this->makeReminder();
        $dbReminder = $this->reminderRepo->find($reminder->id);
        $dbReminder = $dbReminder->toArray();
        $this->assertModelData($reminder->toArray(), $dbReminder);
    }

    /**
     * @test update
     */
    public function testUpdateReminder()
    {
        $reminder = $this->makeReminder();
        $fakeReminder = $this->fakeReminderData();
        $updatedReminder = $this->reminderRepo->update($fakeReminder, $reminder->id);
        $this->assertModelData($fakeReminder, $updatedReminder->toArray());
        $dbReminder = $this->reminderRepo->find($reminder->id);
        $this->assertModelData($fakeReminder, $dbReminder->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteReminder()
    {
        $reminder = $this->makeReminder();
        $resp = $this->reminderRepo->delete($reminder->id);
        $this->assertTrue($resp);
        $this->assertNull(Reminder::find($reminder->id), 'Reminder should not exist in DB');
    }
}
