<?php

use Faker\Factory as Faker;
use App\Models\CronRecipient;
use App\Repositories\CronRecipientRepository;

trait MakeCronRecipientTrait
{
    /**
     * Create fake instance of CronRecipient and save it in database
     *
     * @param array $cronRecipientFields
     * @return CronRecipient
     */
    public function makeCronRecipient($cronRecipientFields = [])
    {
        /** @var CronRecipientRepository $cronRecipientRepo */
        $cronRecipientRepo = App::make(CronRecipientRepository::class);
        $theme = $this->fakeCronRecipientData($cronRecipientFields);
        return $cronRecipientRepo->create($theme);
    }

    /**
     * Get fake instance of CronRecipient
     *
     * @param array $cronRecipientFields
     * @return CronRecipient
     */
    public function fakeCronRecipient($cronRecipientFields = [])
    {
        return new CronRecipient($this->fakeCronRecipientData($cronRecipientFields));
    }

    /**
     * Get fake data of CronRecipient
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCronRecipientData($cronRecipientFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'message_id' => $fake->randomDigitNotNull,
            'mobile_number' => $fake->randomDigitNotNull,
            'email' => $fake->word,
            'sent' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $cronRecipientFields);
    }
}
