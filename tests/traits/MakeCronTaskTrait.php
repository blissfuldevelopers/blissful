<?php

use Faker\Factory as Faker;
use App\Models\CronTask;
use App\Repositories\CronTaskRepository;

trait MakeCronTaskTrait
{
    /**
     * Create fake instance of CronTask and save it in database
     *
     * @param array $cronTaskFields
     * @return CronTask
     */
    public function makeCronTask($cronTaskFields = [])
    {
        /** @var CronTaskRepository $cronTaskRepo */
        $cronTaskRepo = App::make(CronTaskRepository::class);
        $theme = $this->fakeCronTaskData($cronTaskFields);
        return $cronTaskRepo->create($theme);
    }

    /**
     * Get fake instance of CronTask
     *
     * @param array $cronTaskFields
     * @return CronTask
     */
    public function fakeCronTask($cronTaskFields = [])
    {
        return new CronTask($this->fakeCronTaskData($cronTaskFields));
    }

    /**
     * Get fake data of CronTask
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCronTaskData($cronTaskFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'type' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $cronTaskFields);
    }
}
