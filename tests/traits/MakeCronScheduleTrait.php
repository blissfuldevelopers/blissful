<?php

use Faker\Factory as Faker;
use App\Models\CronSchedule;
use App\Repositories\CronScheduleRepository;

trait MakeCronScheduleTrait
{
    /**
     * Create fake instance of CronSchedule and save it in database
     *
     * @param array $cronScheduleFields
     * @return CronSchedule
     */
    public function makeCronSchedule($cronScheduleFields = [])
    {
        /** @var CronScheduleRepository $cronScheduleRepo */
        $cronScheduleRepo = App::make(CronScheduleRepository::class);
        $theme = $this->fakeCronScheduleData($cronScheduleFields);
        return $cronScheduleRepo->create($theme);
    }

    /**
     * Get fake instance of CronSchedule
     *
     * @param array $cronScheduleFields
     * @return CronSchedule
     */
    public function fakeCronSchedule($cronScheduleFields = [])
    {
        return new CronSchedule($this->fakeCronScheduleData($cronScheduleFields));
    }

    /**
     * Get fake data of CronSchedule
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCronScheduleData($cronScheduleFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'cron_task_id' => $fake->randomDigitNotNull,
            'repeat_start' => $fake->date('Y-m-d H:i:s'),
            'repeat_stop' => $fake->date('Y-m-d H:i:s'),
            'cron_min' => $fake->word,
            'cron_hour' => $fake->word,
            'cron_day_of_month' => $fake->word,
            'cron_month' => $fake->word,
            'cron_day_of_week' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $cronScheduleFields);
    }
}
