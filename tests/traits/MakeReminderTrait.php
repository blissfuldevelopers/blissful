<?php

use Faker\Factory as Faker;
use App\Models\Reminder;
use App\Repositories\ReminderRepository;

trait MakeReminderTrait
{
    /**
     * Create fake instance of Reminder and save it in database
     *
     * @param array $reminderFields
     * @return Reminder
     */
    public function makeReminder($reminderFields = [])
    {
        /** @var ReminderRepository $reminderRepo */
        $reminderRepo = App::make(ReminderRepository::class);
        $theme = $this->fakeReminderData($reminderFields);
        return $reminderRepo->create($theme);
    }

    /**
     * Get fake instance of Reminder
     *
     * @param array $reminderFields
     * @return Reminder
     */
    public function fakeReminder($reminderFields = [])
    {
        return new Reminder($this->fakeReminderData($reminderFields));
    }

    /**
     * Get fake data of Reminder
     *
     * @param array $postFields
     * @return array
     */
    public function fakeReminderData($reminderFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_id' => $fake->randomDigitNotNull,
            'task_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $reminderFields);
    }
}
