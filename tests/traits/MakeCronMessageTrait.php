<?php

use Faker\Factory as Faker;
use App\Models\CronMessage;
use App\Repositories\CronMessageRepository;

trait MakeCronMessageTrait
{
    /**
     * Create fake instance of CronMessage and save it in database
     *
     * @param array $cronMessageFields
     * @return CronMessage
     */
    public function makeCronMessage($cronMessageFields = [])
    {
        /** @var CronMessageRepository $cronMessageRepo */
        $cronMessageRepo = App::make(CronMessageRepository::class);
        $theme = $this->fakeCronMessageData($cronMessageFields);
        return $cronMessageRepo->create($theme);
    }

    /**
     * Get fake instance of CronMessage
     *
     * @param array $cronMessageFields
     * @return CronMessage
     */
    public function fakeCronMessage($cronMessageFields = [])
    {
        return new CronMessage($this->fakeCronMessageData($cronMessageFields));
    }

    /**
     * Get fake data of CronMessage
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCronMessageData($cronMessageFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'message' => $fake->text,
            'send_date' => $fake->date('Y-m-d H:i:s'),
            'sent' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $cronMessageFields);
    }
}
