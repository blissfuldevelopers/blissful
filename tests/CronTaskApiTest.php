<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CronTaskApiTest extends TestCase
{
    use MakeCronTaskTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCronTask()
    {
        $cronTask = $this->fakeCronTaskData();
        $this->json('POST', '/api/v1/cronTasks', $cronTask);

        $this->assertApiResponse($cronTask);
    }

    /**
     * @test
     */
    public function testReadCronTask()
    {
        $cronTask = $this->makeCronTask();
        $this->json('GET', '/api/v1/cronTasks/'.$cronTask->id);

        $this->assertApiResponse($cronTask->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCronTask()
    {
        $cronTask = $this->makeCronTask();
        $editedCronTask = $this->fakeCronTaskData();

        $this->json('PUT', '/api/v1/cronTasks/'.$cronTask->id, $editedCronTask);

        $this->assertApiResponse($editedCronTask);
    }

    /**
     * @test
     */
    public function testDeleteCronTask()
    {
        $cronTask = $this->makeCronTask();
        $this->json('DELETE', '/api/v1/cronTasks/'.$cronTask->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/cronTasks/'.$cronTask->id);

        $this->assertResponseStatus(404);
    }
}
