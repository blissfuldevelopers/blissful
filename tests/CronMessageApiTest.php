<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CronMessageApiTest extends TestCase
{
    use MakeCronMessageTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCronMessage()
    {
        $cronMessage = $this->fakeCronMessageData();
        $this->json('POST', '/api/v1/cronMessages', $cronMessage);

        $this->assertApiResponse($cronMessage);
    }

    /**
     * @test
     */
    public function testReadCronMessage()
    {
        $cronMessage = $this->makeCronMessage();
        $this->json('GET', '/api/v1/cronMessages/'.$cronMessage->id);

        $this->assertApiResponse($cronMessage->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCronMessage()
    {
        $cronMessage = $this->makeCronMessage();
        $editedCronMessage = $this->fakeCronMessageData();

        $this->json('PUT', '/api/v1/cronMessages/'.$cronMessage->id, $editedCronMessage);

        $this->assertApiResponse($editedCronMessage);
    }

    /**
     * @test
     */
    public function testDeleteCronMessage()
    {
        $cronMessage = $this->makeCronMessage();
        $this->json('DELETE', '/api/v1/cronMessages/'.$cronMessage->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/cronMessages/'.$cronMessage->id);

        $this->assertResponseStatus(404);
    }
}
