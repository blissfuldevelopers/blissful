<?php

use App\Models\CronTask;
use App\Repositories\CronTaskRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CronTaskRepositoryTest extends TestCase
{
    use MakeCronTaskTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CronTaskRepository
     */
    protected $cronTaskRepo;

    public function setUp()
    {
        parent::setUp();
        $this->cronTaskRepo = App::make(CronTaskRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCronTask()
    {
        $cronTask = $this->fakeCronTaskData();
        $createdCronTask = $this->cronTaskRepo->create($cronTask);
        $createdCronTask = $createdCronTask->toArray();
        $this->assertArrayHasKey('id', $createdCronTask);
        $this->assertNotNull($createdCronTask['id'], 'Created CronTask must have id specified');
        $this->assertNotNull(CronTask::find($createdCronTask['id']), 'CronTask with given id must be in DB');
        $this->assertModelData($cronTask, $createdCronTask);
    }

    /**
     * @test read
     */
    public function testReadCronTask()
    {
        $cronTask = $this->makeCronTask();
        $dbCronTask = $this->cronTaskRepo->find($cronTask->id);
        $dbCronTask = $dbCronTask->toArray();
        $this->assertModelData($cronTask->toArray(), $dbCronTask);
    }

    /**
     * @test update
     */
    public function testUpdateCronTask()
    {
        $cronTask = $this->makeCronTask();
        $fakeCronTask = $this->fakeCronTaskData();
        $updatedCronTask = $this->cronTaskRepo->update($fakeCronTask, $cronTask->id);
        $this->assertModelData($fakeCronTask, $updatedCronTask->toArray());
        $dbCronTask = $this->cronTaskRepo->find($cronTask->id);
        $this->assertModelData($fakeCronTask, $dbCronTask->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCronTask()
    {
        $cronTask = $this->makeCronTask();
        $resp = $this->cronTaskRepo->delete($cronTask->id);
        $this->assertTrue($resp);
        $this->assertNull(CronTask::find($cronTask->id), 'CronTask should not exist in DB');
    }
}
