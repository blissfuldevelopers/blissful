<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CronScheduleApiTest extends TestCase
{
    use MakeCronScheduleTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCronSchedule()
    {
        $cronSchedule = $this->fakeCronScheduleData();
        $this->json('POST', '/api/v1/cronSchedules', $cronSchedule);

        $this->assertApiResponse($cronSchedule);
    }

    /**
     * @test
     */
    public function testReadCronSchedule()
    {
        $cronSchedule = $this->makeCronSchedule();
        $this->json('GET', '/api/v1/cronSchedules/'.$cronSchedule->id);

        $this->assertApiResponse($cronSchedule->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCronSchedule()
    {
        $cronSchedule = $this->makeCronSchedule();
        $editedCronSchedule = $this->fakeCronScheduleData();

        $this->json('PUT', '/api/v1/cronSchedules/'.$cronSchedule->id, $editedCronSchedule);

        $this->assertApiResponse($editedCronSchedule);
    }

    /**
     * @test
     */
    public function testDeleteCronSchedule()
    {
        $cronSchedule = $this->makeCronSchedule();
        $this->json('DELETE', '/api/v1/cronSchedules/'.$cronSchedule->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/cronSchedules/'.$cronSchedule->id);

        $this->assertResponseStatus(404);
    }
}
