<?php

use App\Models\CronMessage;
use App\Repositories\CronMessageRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CronMessageRepositoryTest extends TestCase
{
    use MakeCronMessageTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CronMessageRepository
     */
    protected $cronMessageRepo;

    public function setUp()
    {
        parent::setUp();
        $this->cronMessageRepo = App::make(CronMessageRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCronMessage()
    {
        $cronMessage = $this->fakeCronMessageData();
        $createdCronMessage = $this->cronMessageRepo->create($cronMessage);
        $createdCronMessage = $createdCronMessage->toArray();
        $this->assertArrayHasKey('id', $createdCronMessage);
        $this->assertNotNull($createdCronMessage['id'], 'Created CronMessage must have id specified');
        $this->assertNotNull(CronMessage::find($createdCronMessage['id']), 'CronMessage with given id must be in DB');
        $this->assertModelData($cronMessage, $createdCronMessage);
    }

    /**
     * @test read
     */
    public function testReadCronMessage()
    {
        $cronMessage = $this->makeCronMessage();
        $dbCronMessage = $this->cronMessageRepo->find($cronMessage->id);
        $dbCronMessage = $dbCronMessage->toArray();
        $this->assertModelData($cronMessage->toArray(), $dbCronMessage);
    }

    /**
     * @test update
     */
    public function testUpdateCronMessage()
    {
        $cronMessage = $this->makeCronMessage();
        $fakeCronMessage = $this->fakeCronMessageData();
        $updatedCronMessage = $this->cronMessageRepo->update($fakeCronMessage, $cronMessage->id);
        $this->assertModelData($fakeCronMessage, $updatedCronMessage->toArray());
        $dbCronMessage = $this->cronMessageRepo->find($cronMessage->id);
        $this->assertModelData($fakeCronMessage, $dbCronMessage->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCronMessage()
    {
        $cronMessage = $this->makeCronMessage();
        $resp = $this->cronMessageRepo->delete($cronMessage->id);
        $this->assertTrue($resp);
        $this->assertNull(CronMessage::find($cronMessage->id), 'CronMessage should not exist in DB');
    }
}
