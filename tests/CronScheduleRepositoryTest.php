<?php

use App\Models\CronSchedule;
use App\Repositories\CronScheduleRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CronScheduleRepositoryTest extends TestCase
{
    use MakeCronScheduleTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CronScheduleRepository
     */
    protected $cronScheduleRepo;

    public function setUp()
    {
        parent::setUp();
        $this->cronScheduleRepo = App::make(CronScheduleRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCronSchedule()
    {
        $cronSchedule = $this->fakeCronScheduleData();
        $createdCronSchedule = $this->cronScheduleRepo->create($cronSchedule);
        $createdCronSchedule = $createdCronSchedule->toArray();
        $this->assertArrayHasKey('id', $createdCronSchedule);
        $this->assertNotNull($createdCronSchedule['id'], 'Created CronSchedule must have id specified');
        $this->assertNotNull(CronSchedule::find($createdCronSchedule['id']), 'CronSchedule with given id must be in DB');
        $this->assertModelData($cronSchedule, $createdCronSchedule);
    }

    /**
     * @test read
     */
    public function testReadCronSchedule()
    {
        $cronSchedule = $this->makeCronSchedule();
        $dbCronSchedule = $this->cronScheduleRepo->find($cronSchedule->id);
        $dbCronSchedule = $dbCronSchedule->toArray();
        $this->assertModelData($cronSchedule->toArray(), $dbCronSchedule);
    }

    /**
     * @test update
     */
    public function testUpdateCronSchedule()
    {
        $cronSchedule = $this->makeCronSchedule();
        $fakeCronSchedule = $this->fakeCronScheduleData();
        $updatedCronSchedule = $this->cronScheduleRepo->update($fakeCronSchedule, $cronSchedule->id);
        $this->assertModelData($fakeCronSchedule, $updatedCronSchedule->toArray());
        $dbCronSchedule = $this->cronScheduleRepo->find($cronSchedule->id);
        $this->assertModelData($fakeCronSchedule, $dbCronSchedule->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCronSchedule()
    {
        $cronSchedule = $this->makeCronSchedule();
        $resp = $this->cronScheduleRepo->delete($cronSchedule->id);
        $this->assertTrue($resp);
        $this->assertNull(CronSchedule::find($cronSchedule->id), 'CronSchedule should not exist in DB');
    }
}
