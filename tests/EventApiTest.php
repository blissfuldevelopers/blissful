<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EventApiTest extends TestCase
{
    use MakeEventTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    function it_can_create_events()
    {
        $event = $this->fakeEventData();
        $this->json('POST', "/api/v1/events", $event);

        $this->assertApiResponse($event);
    }

    /**
     * @test
     */
    function it_can_read_event()
    {
        $event = $this->makeEvent();
        $this->json("GET", "/api/v1/events/{$event->id}");

        $this->assertApiResponse($event->toArray());
    }

    /**
     * @test
     */
    function it_can_update_event()
    {
        $event = $this->makeEvent();
        $editedEvent = $this->fakeEventData();

        $this->json('PUT', "/api/v1/events/{$event->id}", $editedEvent);

        $this->assertApiResponse($editedEvent);
    }

    /**
     * @test
     */
    function it_can_delete_events()
    {
        $event = $this->makeEvent();
        $this->json("DELETE", "/api/v1/events/{$event->id}");

        $this->assertApiSuccess();
        $this->json("GET", "/api/v1/events/{$event->id}");

        $this->assertResponseStatus(404);
    }

}
