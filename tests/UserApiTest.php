<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserApiTest extends TestCase
{
    use MakeUserTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    function it_can_create_users()
    {
        $user = $this->fakeUserData();
        $this->json('POST', "/api/v1/users", $user);

        $this->assertApiResponse($user);
    }

    /**
     * @test
     */
    function it_can_read_user()
    {
        $user = $this->makeUser();
        $this->json("GET", "/api/v1/users/{$user->id}");

        $this->assertApiResponse($user->toArray());
    }

    /**
     * @test
     */
    function it_can_update_user()
    {
        $user = $this->makeUser();
        $editedUser = $this->fakeUserData();

        $this->json('PUT', "/api/v1/users/{$user->id}", $editedUser);

        $this->assertApiResponse($editedUser);
    }

    /**
     * @test
     */
    function it_can_delete_users()
    {
        $user = $this->makeUser();
        $this->json("DELETE", "/api/v1/users/{$user->id}");

        $this->assertApiSuccess();
        $this->json("GET", "/api/v1/users/{$user->id}");

        $this->assertResponseStatus(404);
    }

}
