<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ThreadApiTest extends TestCase
{
    use MakeThreadTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    function it_can_create_threads()
    {
        $thread = $this->fakeThreadData();
        $this->json('POST', "/api/v1/threads", $thread);

        $this->assertApiResponse($thread);
    }

    /**
     * @test
     */
    function it_can_read_thread()
    {
        $thread = $this->makeThread();
        $this->json("GET", "/api/v1/threads/{$thread->id}");

        $this->assertApiResponse($thread->toArray());
    }

    /**
     * @test
     */
    function it_can_update_thread()
    {
        $thread = $this->makeThread();
        $editedThread = $this->fakeThreadData();

        $this->json('PUT', "/api/v1/threads/{$thread->id}", $editedThread);

        $this->assertApiResponse($editedThread);
    }

    /**
     * @test
     */
    function it_can_delete_threads()
    {
        $thread = $this->makeThread();
        $this->json("DELETE", "/api/v1/threads/{$thread->id}");

        $this->assertApiSuccess();
        $this->json("GET", "/api/v1/threads/{$thread->id}");

        $this->assertResponseStatus(404);
    }

}
