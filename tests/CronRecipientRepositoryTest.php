<?php

use App\Models\CronRecipient;
use App\Repositories\CronRecipientRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CronRecipientRepositoryTest extends TestCase
{
    use MakeCronRecipientTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CronRecipientRepository
     */
    protected $cronRecipientRepo;

    public function setUp()
    {
        parent::setUp();
        $this->cronRecipientRepo = App::make(CronRecipientRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCronRecipient()
    {
        $cronRecipient = $this->fakeCronRecipientData();
        $createdCronRecipient = $this->cronRecipientRepo->create($cronRecipient);
        $createdCronRecipient = $createdCronRecipient->toArray();
        $this->assertArrayHasKey('id', $createdCronRecipient);
        $this->assertNotNull($createdCronRecipient['id'], 'Created CronRecipient must have id specified');
        $this->assertNotNull(CronRecipient::find($createdCronRecipient['id']), 'CronRecipient with given id must be in DB');
        $this->assertModelData($cronRecipient, $createdCronRecipient);
    }

    /**
     * @test read
     */
    public function testReadCronRecipient()
    {
        $cronRecipient = $this->makeCronRecipient();
        $dbCronRecipient = $this->cronRecipientRepo->find($cronRecipient->id);
        $dbCronRecipient = $dbCronRecipient->toArray();
        $this->assertModelData($cronRecipient->toArray(), $dbCronRecipient);
    }

    /**
     * @test update
     */
    public function testUpdateCronRecipient()
    {
        $cronRecipient = $this->makeCronRecipient();
        $fakeCronRecipient = $this->fakeCronRecipientData();
        $updatedCronRecipient = $this->cronRecipientRepo->update($fakeCronRecipient, $cronRecipient->id);
        $this->assertModelData($fakeCronRecipient, $updatedCronRecipient->toArray());
        $dbCronRecipient = $this->cronRecipientRepo->find($cronRecipient->id);
        $this->assertModelData($fakeCronRecipient, $dbCronRecipient->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCronRecipient()
    {
        $cronRecipient = $this->makeCronRecipient();
        $resp = $this->cronRecipientRepo->delete($cronRecipient->id);
        $this->assertTrue($resp);
        $this->assertNull(CronRecipient::find($cronRecipient->id), 'CronRecipient should not exist in DB');
    }
}
