<?php
return [
  'name' => "Blissful | Blog",
  'title' => "Wedding News, Trends & Advice in Kenya",
  'subtitle' => 'We cover everything from gorgeous real weddings to up and coming vendors and seasoned professionals in the wedding & events industry. We also provide advice and insights on ways to make your wedding a success',
  'description' => 'Blissful blog covers everything from gorgeous real weddings to up and coming vendors and seasoned professionals in the wedding & events industry. We also provide advice and insights on ways to make your wedding a success',
  'author' => 'Blissful.co.ke',
  'page_image' => 'blog-header.jpg',
  'posts_per_page' => 10,
  'uploads' => [
    'storage' => 's3',
    'webpath' => 'https://s3.eu-central-1.amazonaws.com/blissful-ke/uploads',
  ],
];
