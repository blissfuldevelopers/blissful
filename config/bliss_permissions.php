<?php

/**
 * Feel free to run 'php artisan db:seed --class=PermissionSeeder' you update the permissions
 * Try to tie permissions to roles, rather than users.
 *  It's easier to manage them that way...
 *
 * For Further reading on the roles functions being used
 * https://github.com/romanbican/roles
 */
return [
    'permissions' => [
        'Users',
        'Shop',
        'Orders',
        'Jobs',
        'Bids',
        'Gallery',
        'Blog',
        'Reviews',
        'Messages',
        'Leads',
        'Credits',
        'Credit_Bundles',
        'Activities',
        'Advertise',
        'Tracker',
    ],

    'crud' => [
        'view',
        'create',
        'edit',
        'delete',
    ],

    'user_role_permissions' => [
        'administrator' => [ 'all', ], //NOTE: Changing admin roles from here doesn't change their roles
        'user'          => [
            'users'    => [ 'view' ],
            'jobs'     => [ 'all' ],
            'shop'     => [ 'view' ],
            'orders'   => [ 'view', 'create', 'edit' ], //can't delete
            'bids'     => [ 'view' ],
            'gallery'  => [ 'all' ],
            'blog'     => [ 'view' ],
            'reviews'  => [ 'create', 'view' ],
            'messages' => [ 'all' ],
        ],
        'vendor'        => [
            'users'          => [ 'view' ],
            'jobs'           => [ 'all' ],
            'shop'           => [ 'view' ],
            'orders'         => [ 'view', 'create', 'edit' ], //can't delete
            'bids'           => [ 'all' ],
            'gallery'        => [ 'all' ],
            'blog'           => [ 'view' ],
            'reviews'        => [ 'view' ],
            'credits'        => [ 'view' ],
//            'credit_bundles' => [ 'view' ],
            'messages'       => [ 'all' ],
            'advertise'      => [ 'view', 'create', 'edit' ],
        ],
        'editor'        => [
            'users'    => [ 'view' ],
            'jobs'     => [ 'view' ],
            'shop'     => [ 'view' ],
//                'bids'    => ['view' ],
            'gallery'  => [ 'view' ],
            'blog'     => [ 'all' ], //all rights
            'reviews'  => [ 'view' ],
            'messages' => [ 'view' ],
            'leads'    => [ 'all' ],
        ],
    ],
];