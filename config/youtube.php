<?php

return [
	
	/**
	 * Application Name.
	 */
	'application_name' => 'Blissful',

	/**
	 * Client ID.
	 */
	'client_id' => '22111778214-d7m0pdus5otkhv17cas0rilrdonmfvlv.apps.googleusercontent.com',

	/**
	 * Client Secret.
	 */
	'client_secret' => 'jUNRnSlnmzjzeAzlTDgcbkyf',

	/**
	 * Route Base URI. You can use this to prefix all route URI's.
	 * Example: 'admin', would prefix the below routes with 'http://domain.com/admin/'
	 */
	'route_base_uri' => '',

	/**
	 * Redirect URI, this does not include your TLD.
	 * Example: 'callback' would be http://domain.com/callback
	 */
	'redirect_uri' => 'youtube-callback',

	/**
	 * The autentication URI in with you will require to first authorize with Google.
	 */
	'authentication_uri' => 'youtube-auth',

	/**
	 * Access Type
	 */
	'access_type' => 'offline',

	/**
	 * Approval Prompt
	 */
	'approval_prompt' => 'auto',

	/**
	 * Scopes.
	 */
	'scopes' => [
		'https://www.googleapis.com/auth/youtube',
		'https://www.googleapis.com/auth/youtube.upload',
		'https://www.googleapis.com/auth/youtube.readonly'
	],

	/**
	 * Developer key.
	 */
	'developer_key' => getenv('GOOGLE_DEVELOPER_KEY')

];